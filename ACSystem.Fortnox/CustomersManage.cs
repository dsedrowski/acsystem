﻿using FortnoxAPILibrary;
using FortnoxAPILibrary.Connectors;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACSystem.Fortnox
{
    public class CustomersManage : Customer
    {
        private readonly CustomerConnector _connector;

        public CustomersManage()
        {
            Auth.SetCredentials();

            _connector = new CustomerConnector();
        }

        public Customer Get(string _customerNumber)
        {
            var customer = _connector.Get(_customerNumber);

            if (!_connector.HasError)
                return customer;
            else
                throw new Exception("Błąd podczas pobierania klienta", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public List<Customer> List(Filter.Customer filter, string name, string city)
        {
            _connector.FilterBy = filter;
            _connector.Name = name;
            _connector.City = city;

            var customers = _connector.Find();

            var list = new List<Customer>();

            for (var i = 1; i <= int.Parse(customers.TotalPages); i++)
            {
                Console.WriteLine(customers.CustomerSubset[0].CustomerNumber);

                customers.CustomerSubset.ForEach(x =>
                {
                       list.Add(new Customer
                       {
                           Address1 = x.Address1,
                           Address2 = x.Address2,
                           CustomerNumber = x.CustomerNumber,
                           City = x.City,
                           Email = x.Email,
                           Name = x.Name,
                           OrganisationNumber = x.OrganisationNumber,
                           Phone1 = x.Phone,
                           url = x.url,
                           ZipCode = x.ZipCode
                       });
                });

                if (customers.CurrentPage != customers.TotalPages)
                {
                    _connector.Page = i+1;
                    customers = _connector.Find();
                }
            }

            if (!_connector.HasError)
                return list;
            else
                throw new Exception("Błąd podczas pobierania list klientów", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public Customer Create(string Name, string Address, string Zip, string City, string NIP, string Email)
        {
            _connector.Name = Name;
            var total = _connector.Find().TotalResources;

            if (!total.Equals("0"))
                throw new Exception("Błąd podczas tworzenia klienta", new Exception("Klient o podanej nazwie już istnieje"));

            var customer = new Customer
            {
                Name = Name,
                Address1 = Address,
                ZipCode = Zip,
                City = City,
                OrganisationNumber = NIP,
                Email = Email
            };

            customer = _connector.Create(customer);

            if (!_connector.HasError)
                return customer;
            else
                throw new Exception("Błąd podczas tworzenia klienta", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public Customer Update(Customer customer)
        {
            customer = _connector.Update(customer);

            if (!_connector.HasError)
                return customer;
            else
                throw new Exception("Błąd podczas aktualizacji klienta", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public bool Delete(string _customerNumber)
        {
            _connector.Delete(_customerNumber);

            if (!_connector.HasError)
                return true;
            else
                throw new Exception("Błąd podczas aktualizacji klienta", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }
    }
}
