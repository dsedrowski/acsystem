﻿using FortnoxAPILibrary;
using FortnoxAPILibrary.Connectors;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACSystem.Fortnox
{
    public class ArticlesManage : Article
    {
        private readonly ArticleConnector _connector;

        public ArticlesManage()
        {
            Auth.SetCredentials();

            _connector = new ArticleConnector();
        }

        public Article Get(string _articleNumber)
        {
            var article = _connector.Get(_articleNumber);

            if (!_connector.HasError)
                return article;
            else
                throw new Exception("Błąd podczas pobierania artykułu", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public Article Create(Article data)
        {
            data = _connector.Create(data);

            if (!_connector.HasError)
                return data;
            else
                throw new Exception("Błąd podczas tworzenia artykułu", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public Article Update(Article data)
        {
            data = _connector.Update(data);

            if (!_connector.HasError)
                return data;
            else
                throw new Exception("Błąd podczas aktualizacji artykułu", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public bool Delete(string _number)
        {
            _connector.Delete(_number);

            if (!_connector.HasError)
                return true;
            else
                throw new Exception("Błąd podczas usuwania artykułu", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }
    }
}
