﻿using ACSystem.Core;
using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using ACSystem.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskStatus = ACSystem.Core.TaskStatus;

namespace ACSystem.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly IApartmentRepository _apartmentRepository;
        private readonly IFeeRepository _feeRepository;
        private readonly IInventoryRepository _inventoryRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IProject_ProjectManagerRepository _project_ProjectManagerRepository;
        private readonly IEventRepository _eventRepository;
        private readonly IPayrollItemsRepository _payrollItemsRepository;

        public ProjectService(IProjectRepository projectRepository, ITaskRepository taskRepository,
                              IApartmentRepository apartmentRepository, IFeeRepository feeRepository,
                              IInventoryRepository inventoryRepository, IInvoiceRepository invoiceRepository,
                              IProject_ProjectManagerRepository project_ProjectManagerRepository, IEventRepository eventRepository,
                              IPayrollItemsRepository payrollItemsRepository)
        {
            _projectRepository = projectRepository;
            _taskRepository = taskRepository;
            _apartmentRepository = apartmentRepository;
            _feeRepository = feeRepository;
            _inventoryRepository = inventoryRepository;
            _invoiceRepository = invoiceRepository;
            _project_ProjectManagerRepository = project_ProjectManagerRepository;
            _eventRepository = eventRepository;
            _payrollItemsRepository = payrollItemsRepository;
        }

        #region Synchronous

        public Projects GetById(int id) => _projectRepository.Find(id);

        public ICollection<Project_ProjectManager> GetManagers(int id) =>
            _project_ProjectManagerRepository.Find(e => e.ProjectId.Equals(id));

        public Dictionary<string, bool> CanFinalizeProject(int id)
        {
            var tasksError = _taskRepository.Any(e => e.Apartment.ProjectId == id &&
                                                          (e.Status == TaskStatus.FinishedWithFaults ||
                                                           e.Status == TaskStatus.PlaceNotReady ||
                                                           e.Status == TaskStatus.DoWyjasnienia));
            var taskNotFinished = _taskRepository.Any(e => e.Apartment.ProjectId == id &&
                                                           (e.Status == TaskStatus.Free ||
                                                            e.Status == TaskStatus.Stoped ||
                                                            e.Status == TaskStatus.UnderWork ||
                                                            e.Status == TaskStatus.Planned));
            var taskNotInvoiced = _taskRepository.Any(e => e.Apartment.ProjectId == id &&
                                                                             e.Accepted && !e.Invoice);
            var taskNotPayroll = _taskRepository.ExecuteQuery<int>($"SELECT COUNT(*) FROM view_IsAnyToPayroll WHERE Id = {id}").FirstOrDefault() > 0;
            var taskNotFees = _taskRepository.Any(e => e.Apartment.ProjectId == id && e.Fees.Any(x => x.Status == FeeStatus.DoPotracenia));
            var apartmentNotFees = _apartmentRepository.Any(e => e.ProjectId == id && e.Fees.Any(x => x.Status == FeeStatus.DoPotracenia));
            var projectNotFees = _feeRepository.Any(e => e.ProjectID == id && e.Status == FeeStatus.DoPotracenia);
            var anyInventory = _inventoryRepository.Any(e => e.ProjectID == id);
            var NotFees = (taskNotFees || apartmentNotFees || projectNotFees);

            return new Dictionary<string, bool>
            {
                { "Projekt zawiera zadania z błędami", tasksError },
                { "Projekt zawiera niezakończone zadania", taskNotFinished },
                { "Projekt zawiera niezafakturowane zadania", taskNotFinished },
                { "Projekt zawiera zadania nie zaciągniete do list płac", taskNotPayroll },
                { "Projekt zawiera niepobrane szkody", NotFees },
                { "Do projektu przypisane są narzędzia", anyInventory }
            };
        }

        #endregion

        #region Asynchronous

        public async Task<Projects> GetByIdAsync(int id) => await _projectRepository.FindAsync(id);

        public async Task<Dictionary<string, bool>> CanFinalizeProjectAsync(int id)
        {
            var tasksError = await _taskRepository.AnyAsync(e => e.Apartment.ProjectId == id &&
                                                          (e.Status == TaskStatus.FinishedWithFaults ||
                                                           e.Status == TaskStatus.PlaceNotReady ||
                                                           e.Status == TaskStatus.DoWyjasnienia));
            var taskNotFinished = await _taskRepository.AnyAsync(e => e.Apartment.ProjectId == id &&
                                                           (e.Status == TaskStatus.Free ||
                                                            e.Status == TaskStatus.Stoped ||
                                                            e.Status == TaskStatus.UnderWork ||
                                                            e.Status == TaskStatus.Planned));
            var taskNotInvoiced = await _taskRepository.AnyAsync(e => e.Apartment.ProjectId == id &&
                                                                             e.Accepted && !e.Invoice);
            var taskNotPayroll = _taskRepository.ExecuteQuery<int>($"SELECT COUNT(*) FROM view_IsAnyToPayroll WHERE Id = {id}").FirstOrDefault() > 0;
            var taskNotFees = await _taskRepository.AnyAsync(e => e.Apartment.ProjectId == id && e.Fees.Any(x => x.Status == FeeStatus.DoPotracenia));
            var apartmentNotFees = await _apartmentRepository.AnyAsync(e => e.ProjectId == id && e.Fees.Any(x => x.Status == FeeStatus.DoPotracenia));
            var projectNotFees = await _feeRepository.AnyAsync(e => e.ProjectID == id && e.Status == FeeStatus.DoPotracenia);
            var anyInventory = await _inventoryRepository.AnyAsync(e => e.ProjectID == id);
            var NotFees = (taskNotFees || apartmentNotFees || projectNotFees);

            return new Dictionary<string, bool>
            {
                { "Projekt zawiera zadania z błędami", tasksError },
                { "Projekt zawiera niezakończone zadania", taskNotFinished },
                { "Projekt zawiera niezafakturowane zadania", taskNotFinished },
                { "Projekt zawiera zadania nie zaciągniete do list płac", taskNotPayroll },
                { "Projekt zawiera niepobrane szkody", NotFees },
                { "Do projektu przypisane są narzędzia", anyInventory }
            };
        }

        public async Task<ICollection<Project_ProjectManager>> GetManagersAsync(int id) =>
            await _project_ProjectManagerRepository.FindAsync(e => e.ProjectId.Equals(id));

        #endregion

        public async Task<List<EventsViewModel>> GetEventsForProject(int id)
        {
            var project = await _projectRepository.FindAsync(id);
            var events = (await _eventRepository.FindAsync(e => e.Type == EventType.Project && e.ReferenceTo == id))
                                                .Select(e => new EventsViewModel
                                                {
                                                    ID = e.Id,
                                                    Date = e.Start,
                                                    Project = project.ProjectNumber,
                                                    Title = e.Title,
                                                    Description = e.Description,
                                                    Type = "Project"
                                                }).ToList();

            return events;
        }

        public async Task<List<Invoice>> GetInvoices(int id) => (await _invoiceRepository.FindAsync(e => e.ProjectID == id && e.Complete == true)).ToList();

        public async Task<List<PayrollItems>> GetPayrolls(int id) => (await _payrollItemsRepository.FindAsync(e => e.ProjectID == id && e.Active == true)).ToList();

        public async Task<List<Fees>> GetFees(int id) => (await _feeRepository.FindAsync(e => e.ProjectID == id)).ToList();

        public async Task<List<view_CheckTaskPriceNorm>> CheckTaskPriceNorm(int id) =>
            await _projectRepository.GetContext().view_CheckTaskPriceNorm.Where(q => q.ProjectID == id).ToListAsync();

        public async Task<List<view_CheckPayrollRateNorm>> CheckPayrollRateNorm(int id) =>
            await _projectRepository.GetContext().view_CheckPayrollRateNorm.Where(q => q.ProjectID == id).ToListAsync();
    }
}
