﻿using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACSystem.Services
{
    public interface IProjectService
    {
        Projects GetById(int id);
        Task<Projects> GetByIdAsync(int id);

        ICollection<Project_ProjectManager> GetManagers(int id);
        Task<ICollection<Project_ProjectManager>> GetManagersAsync(int id);

        Dictionary<string, bool> CanFinalizeProject(int id);
        Task<Dictionary<string, bool>> CanFinalizeProjectAsync(int id);

        Task<List<EventsViewModel>> GetEventsForProject(int id);
        Task<List<Invoice>> GetInvoices(int id);
        Task<List<PayrollItems>> GetPayrolls(int id);
        Task<List<Fees>> GetFees(int id);
        Task<List<view_CheckTaskPriceNorm>> CheckTaskPriceNorm(int id);
        Task<List<view_CheckPayrollRateNorm>> CheckPayrollRateNorm(int id);
    }
}
