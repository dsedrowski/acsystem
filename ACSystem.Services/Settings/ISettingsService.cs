﻿using ACSystem.Core.Models;
using System.Threading.Tasks;

namespace ACSystem.Services
{
    public interface ISettingsService
    {
        Settings GetSingleSettings();
        Task<Settings> GetSingleSettingsAsync();
    }
}
