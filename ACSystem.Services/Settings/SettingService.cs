﻿using ACSystem.Repositories;
using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACSystem.Services
{
    public class SettingService : ISettingsService
    {
        private readonly ISettingsRepository _settingsRepository;

        public SettingService(ISettingsRepository settingsRepository)
        {
            _settingsRepository = settingsRepository;
        }

        #region Synchronous

        public Settings GetSingleSettings() =>
            _settingsRepository.First();

        #endregion

        #region Asynchronous

        public async Task<Settings> GetSingleSettingsAsync() =>
            await _settingsRepository.FirstAsync();

        #endregion
    }
}
