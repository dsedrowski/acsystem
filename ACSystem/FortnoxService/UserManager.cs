﻿using FortnoxAPILibrary;
using FortnoxAPILibrary.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.FortnoxService
{
    public class UserManager
    {
        public UserManager()
        {
            Auth.SetCredentials();
        }

        public CostCenter Get(string _ccNumber)
        {
            var connector = new CostCenterConnector();

            var cc = connector.Get(_ccNumber);

            if (!connector.HasError)
                return cc;
            else
                throw new Exception("Błąd podczas pobierania cost center", new Exception(connector.Error.Code + "#" + connector.Error.Message));
        }

        public CostCenter Create(CostCenter data)
        {
            var connector = new CostCenterConnector();

            data = connector.Create(data);

            if (!connector.HasError)
                return data;
            else
                throw new Exception("Błąd podczas tworzenia cost center", new Exception(connector.Error.Code + "#" + connector.Error.Message));
        }

        public CostCenter Update(CostCenter data)
        {
            var connector = new CostCenterConnector();

            data = connector.Update(data);

            if (!connector.HasError)
                return data;
            else
                throw new Exception("Błąd podczas edytowania cost center", new Exception(connector.Error.Code + "#" + connector.Error.Message));
        }

        public bool Delete(string _ccNumber)
        {
            var connector = new CostCenterConnector();

            connector.Delete(_ccNumber);

            if (!connector.HasError)
                return true;
            else
                throw new Exception("Błąd podczas usuwania cost center", new Exception(connector.Error.Code + "#" + connector.Error.Message));
        }

        public List<CostCenter> List()
        {
            var _connector = new CostCenterConnector();

            var ccs = _connector.Find();

            var list = new List<CostCenter>();

            for (var i = 1; i <= int.Parse(ccs.TotalPages); i++)
            {
                ccs.CostCenterSubset.ForEach(x =>
                {
                    list.Add(new CostCenter
                    {
                        Code = x.Code,
                        Description = x.Description,
                        Note = x.Note
                    });
                });

                if (ccs.CurrentPage != ccs.TotalPages)
                {
                    _connector.Page = i + 1;
                    ccs = _connector.Find();
                }
            }

            if (!_connector.HasError)
                return list;
            else
                throw new Exception("Błąd podczas pobierania list cost center", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }
    }
}