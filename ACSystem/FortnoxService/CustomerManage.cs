﻿using FortnoxAPILibrary;
using FortnoxAPILibrary.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.FortnoxService
{
    public class CustomerManage
    {
        public CustomerManage()
        {
            Auth.SetCredentials();
        }

        public Customer Get(string _customerNumber)
        {
            var _connector = new CustomerConnector();

            var customer = _connector.Get(_customerNumber);

            if (!_connector.HasError)
                return customer;
            else
                throw new Exception("Błąd podczas pobierania klienta", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public Customer Create(Customer customer)
        {
            customer.CountryCode = "SE";

            var _connector = new CustomerConnector();

            _connector.Name = customer.Name;
            var total = _connector.Find();

            if (!total.TotalResources.Equals("0"))
            {
                customer.CustomerNumber = total.CustomerSubset[0].CustomerNumber;
                return customer;
            }

            _connector.Name = string.Empty;
            customer = _connector.Create(customer);

            if (!_connector.HasError)
                return customer;
            else
                throw new Exception("Błąd podczas tworzenia klienta", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public Customer Update(Customer customer)
        {
            var _connector = new CustomerConnector();

            customer = _connector.Update(customer);

            if (!_connector.HasError)
                return customer;
            else
                throw new Exception("Błąd podczas aktualizacji klienta", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public bool Delete(string _customerNumber)
        {
            var _connector = new CustomerConnector();

            _connector.Delete(_customerNumber);

            if (!_connector.HasError)
                return true;
            else
                throw new Exception("Błąd podczas aktualizacji klienta", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public List<Customer> List()
        {
            var _connector = new CustomerConnector();

            _connector.FilterBy = Filter.Customer.Active;
            var customers = _connector.Find();

            var list = new List<Customer>();

            for (var i = 1; i <= int.Parse(customers.TotalPages); i++)
            {
                Console.WriteLine(customers.CustomerSubset[0].CustomerNumber);

                customers.CustomerSubset.ForEach(x =>
                {
                    var c = _connector.Get(x.CustomerNumber);
                    list.Add(new Customer
                    {
                        Address1 = c.Address1,
                        Address2 = c.Address2,
                        CustomerNumber = c.CustomerNumber,
                        City = c.City,
                        Email = c.Email,
                        Name = c.Name,
                        VATNumber = c.VATNumber,
                        Phone1 = c.Phone1,
                        url = c.url,
                        ZipCode = c.ZipCode
                    });
                });

                if (customers.CurrentPage != customers.TotalPages)
                {
                    _connector.Page = i + 1;
                    customers = _connector.Find();
                }
            }

            if (!_connector.HasError)
                return list;
            else
                throw new Exception("Błąd podczas pobierania list klientów", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }
    }
}