﻿using FortnoxAPILibrary;
using FortnoxAPILibrary.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.FortnoxService
{
    public class ProjectManager
    {
        public ProjectManager()
        {
            Auth.SetCredentials();
        }

        public Project Get(string _projectNumber)
        {
            try
            {
                ProjectConnector connector = new ProjectConnector();

                var project = connector.Get(_projectNumber);

                if (!connector.HasError)
                    return project;
                else
                    throw new Exception("Błąd podczas pobierania projektu", new Exception(connector.Error.Code + "#" + connector.Error.Message));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Project Create(Project _project, out string _errorCode)
        {
            try
            {
                ProjectConnector connector = new ProjectConnector();
                _project = connector.Create(_project);

                _errorCode = "";

                if (!connector.HasError)
                    return _project;
                else
                {
                    _errorCode = connector.Error.Code;
                    throw new Exception("Błąd podczas tworzenia projektu", new Exception(connector.Error.Code + "#" + connector.Error.Message + " Projekt: " + _project.Description));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Project Update(Project _project)
        {
            try
            {
                ProjectConnector connector = new ProjectConnector();

                _project = connector.Update(_project);

                if (!connector.HasError)
                    return _project;
                else
                    throw new Exception("Błąd podczas aktualizacji projektu", new Exception(connector.Error.Code + "#" + connector.Error.Message));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(string _number)
        {
            try
            {
                ProjectConnector connector = new ProjectConnector();

                connector.Delete(_number);

                if (!connector.HasError)
                    return true;
                else
                    throw new Exception("Błąd podczas usuwania projektu", new Exception(connector.Error.Code + "#" + connector.Error.Message));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Project> List()
        {
            try
            {
                ProjectConnector connector = new ProjectConnector();

                var projects = connector.Find();

                var list = new List<Project>();

                for (var i = 1; i <= int.Parse(projects.TotalPages); i++)
                {
                    projects.ProjectSubset.ForEach(x =>
                    {
                        list.Add(new Project
                        {
                            ProjectNumber = x.ProjectNumber,
                            Description = x.Description,
                            StartDate = x.StartDate,
                            EndDate = x.EndDate
                        });
                    });

                    if (projects.CurrentPage != projects.TotalPages)
                    {
                        connector.Page = i + 1;
                        projects = connector.Find();
                    }
                }

                if (!connector.HasError)
                    return list;
                else
                    throw new Exception("Błąd podczas pobierania list projektów", new Exception(connector.Error.Code + "#" + connector.Error.Message));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}