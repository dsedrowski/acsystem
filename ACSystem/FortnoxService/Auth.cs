﻿using FortnoxAPILibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ACSystem.FortnoxService
{
    public class Auth
    {
        public static readonly string AUTHORIZATION_CODE = "ec5add01-a02c-42a8-88c3-e36d43fbd9ea";
        public static readonly string CLIENT_SECRET = "NT2Y8grEA4";

#if DEBUG
        public static readonly string ACCESS_TOKEN = "0d7ffc88-0e6f-422a-aa23-be82d27ac545";
#else
        public static readonly string ACCESS_TOKEN = "53beaf60-fd15-4727-b649-ab6e13ae243d";
#endif

        public static void SetCredentials()
        {

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;
            ConnectionCredentials.AccessToken = ACCESS_TOKEN;
            ConnectionCredentials.ClientSecret = CLIENT_SECRET;
        }
    }
}