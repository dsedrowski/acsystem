﻿using FortnoxAPILibrary;
using FortnoxAPILibrary.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ACSystem.FortnoxService
{
    public class InvoiceManager
    {
        public InvoiceManager()
        {
            Auth.SetCredentials();
        }

        public Invoice Create(Invoice data, string customerEmail)
        {
            InvoiceConnector connector = new InvoiceConnector();

            data.EmailInformation = new InvoiceEmailInformation
            {
                EmailAddressTo = customerEmail,
                EmailAddressCC = "rapport@acbygg.com",
                EmailAddressFrom = "info@acbygg.com"
            };

            data = connector.Create(data);

            if (!connector.HasError)
                return data;
            else
                throw new Exception($"Błąd podczas tworzenia faktury. ID Klienta: {data.CustomerNumber}", new Exception(connector.Error.Code + "#" + connector.Error.Message));
        }
    }
}