﻿using ACSystem.Core.Models;
using FortnoxAPILibrary;
using FortnoxAPILibrary.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.FortnoxService
{
    public class ArticleManager
    {
        private readonly ArticleConnector _connector;

        public ArticleManager()
        {
            Auth.SetCredentials();

            _connector = new ArticleConnector();
        }

        public Article Get(string _articleNumber)
        {
            var article = _connector.Get(_articleNumber);

            if (!_connector.HasError)
                return article;
            else
                throw new Exception("Błąd podczas pobierania artykułu", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public Article Create(Article data, List<Products> list = null)
        {
            var productPrice = data.SalesPrice;
            _connector.Description = data.Description;
            var total = _connector.Find();

            if (!total.TotalResources.Equals("0"))
            {
                var prs = true;

                foreach (var s in total.ArticleSubset)
                {
                    if (data.Description.Equals(s.Description))
                    {
                        prs = (list == null) ?
                                        new ACSystemContext().Products.Any(q => q.InSystemID == s.ArticleNumber)
                                        :
                                        list.Any(q => q.InSystemID == s.ArticleNumber);
                    }
                }

                if (!prs)
                {
                    data.ArticleNumber = total.ArticleSubset[0].ArticleNumber;
                    return data;
                }

            }

            _connector.ArticleNumber = string.Empty;
            _connector.Description = string.Empty;

            data = _connector.Create(data);

            if (!_connector.HasError)
            {
                var price = new Price
                {
                    ArticleNumber = data.ArticleNumber,
                    PriceValue = productPrice.Replace(',', '.'),
                    PriceList = "A"
                };

                PriceConnector priceConnector = new PriceConnector();
                priceConnector.Create(price);

                if (!priceConnector.HasError)
                    return data;
                else
                    throw new Exception("Błąd podczas tworzenia ceny", new Exception(priceConnector.Error.Code + "#" + priceConnector.Error.Message));
            }
            else
                throw new Exception("Błąd podczas tworzenia artykułu", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public Article Update(Article data)
        {
            data = _connector.Update(data);

            if (!_connector.HasError)
                return data;
            else
                throw new Exception("Błąd podczas aktualizacji artykułu", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }

        public bool Delete(string _number)
        {
            _connector.Delete(_number);

            if (!_connector.HasError)
                return true;
            else
                throw new Exception("Błąd podczas usuwania artykułu", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }


        public List<Article> List()
        {
            var test = _connector.Get("64");

            _connector.FilterBy = Filter.Article.Active;
            var articles = _connector.Find();

            var list = new List<Article>();

            for (var i = 1; i <= int.Parse(articles.TotalPages); i++)
            {
                articles.ArticleSubset.ForEach(x =>
                {
                    list.Add(new Article
                    {
                        ArticleNumber = x.ArticleNumber,
                        Description = x.Description,
                        SalesPrice = x.SalesPrice,
                        Unit = x.Unit
                    });
                });

                if (articles.CurrentPage != articles.TotalPages)
                {
                    _connector.Page = i + 1;
                    articles = _connector.Find();
                }
            }

            if (!_connector.HasError)
                return list;
            else
                throw new Exception("Błąd podczas pobierania list klientów", new Exception(_connector.Error.Code + "#" + _connector.Error.Message));
        }
    }
}