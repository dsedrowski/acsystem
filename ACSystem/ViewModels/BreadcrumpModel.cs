﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.ViewModels
{
    public class BreadcrumpModel
    {
        public string Page { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public bool Active { get; set; }
    }
}