﻿using ACSystem.Core.Models;
using System;
using System.Linq;

namespace ACSystem {
    public static class Temp {
        public static Settings Settings { get; set; }

        public static void SetVariables() {
            using (var ctx = new ACSystemContext()) {
                Settings = ctx.Settings.FirstOrDefault();

                if (Settings == null)
                    throw new NullReferenceException("Settings are not implemented. Contact your administrator.");
            }
        }
    }
}