namespace ACSystem.Migrations
{
    using Core.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ACSystem.Core.Models.ACSystemContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "ACSystem.Models.ACSystemContext";
        }

        protected override void Seed(ACSystem.Core.Models.ACSystemContext context)
        {
            SeedRoles(context);
            SeedUsers(context);
        }

        private void SeedRoles(ACSystem.Core.Models.ACSystemContext context)
        {
            var roleManager = new RoleManager<Microsoft.AspNet.Identity.EntityFramework.IdentityRole>(new RoleStore<IdentityRole>());

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Admin";

                roleManager.Create(role);
            }
        }

        private void SeedUsers (Core.Models.ACSystemContext context)

        {
            var store = new UserStore<ACSystemUser>(context);
            var manager = new UserManager<ACSystemUser>(store);

            if (!context.Users.Any(u => u.UserName == "Admin"))
            {
                var user = new ACSystemUser { UserName = "admin@admin.pl"};
                var adminresult = manager.Create(user, "admin");

                if (adminresult.Succeeded)
                    manager.AddToRole(user.Id, "Admin");
            }
        }
    }
}
