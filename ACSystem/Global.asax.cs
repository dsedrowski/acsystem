﻿using ACSystem.Core.Models;
using ACSystem.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ACSystem
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error()
        {
            var db = new ACSystemContext();

            Exception ex = Server.GetLastError();

            var user = User.Identity.Name;

            var url = Context.Request.Url.ToString();

            var log = new ExceptionsLog
            {
                Message = ex.Message,
                Type = ex.GetType().ToString(),
                Source = ex.Source,
                StackTrace = ex.StackTrace,
                URL = url,
                LogDate = DateTime.Now,
                User = user
            };

            db.ExceptionsLog.Add(log);

            db.SaveChanges();

            return;
        }
    }
}
