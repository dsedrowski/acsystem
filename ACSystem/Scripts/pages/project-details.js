﻿$(document).ready(function () {
    $('.tab-button').click(LoadPartialView)
});

function LoadPartialView(event) {
    var tab = $(event.target);

    if ($(tab.attr("href")).length > 0)
        return;

    $.get(tab.data("url"), function (data) {
        $('.tab-content').append(data);
    })
    .done(function () {
        $('.tab-content .tab-pane').removeClass('active');
        $(tab.attr("href")).addClass('active');
    })
    .fail(function () {
        toastr.error("Wystąpił błąd podczas pobierania zawartości zakładki !");
    })
}