/// <autosync enabled="true" />
/// <reference path="bootstrap.js" />
/// <reference path="bootstrap.min.js" />
/// <reference path="bootstrap_old.js" />
/// <reference path="bootstrap-datepicker.min.js" />
/// <reference path="bootstrap-datetimepicker.js" />
/// <reference path="jquery.validate.min.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-3.1.1.min.js" />
/// <reference path="jquery-3.1.1.slim.min.js" />
/// <reference path="locales/bootstrap-datepicker.ar.min.js" />
/// <reference path="locales/bootstrap-datepicker.az.min.js" />
/// <reference path="locales/bootstrap-datepicker.bg.min.js" />
/// <reference path="locales/bootstrap-datepicker.bs.min.js" />
/// <reference path="locales/bootstrap-datepicker.ca.min.js" />
/// <reference path="locales/bootstrap-datepicker.cs.min.js" />
/// <reference path="locales/bootstrap-datepicker.cy.min.js" />
/// <reference path="locales/bootstrap-datepicker.da.min.js" />
/// <reference path="locales/bootstrap-datepicker.de.min.js" />
/// <reference path="locales/bootstrap-datepicker.el.min.js" />
/// <reference path="locales/bootstrap-datepicker.en-au.min.js" />
/// <reference path="locales/bootstrap-datepicker.en-gb.min.js" />
/// <reference path="locales/bootstrap-datepicker.eo.min.js" />
/// <reference path="locales/bootstrap-datepicker.es.min.js" />
/// <reference path="locales/bootstrap-datepicker.et.min.js" />
/// <reference path="locales/bootstrap-datepicker.eu.min.js" />
/// <reference path="locales/bootstrap-datepicker.fa.min.js" />
/// <reference path="locales/bootstrap-datepicker.fi.min.js" />
/// <reference path="locales/bootstrap-datepicker.fo.min.js" />
/// <reference path="locales/bootstrap-datepicker.fr.min.js" />
/// <reference path="locales/bootstrap-datepicker.fr-ch.min.js" />
/// <reference path="locales/bootstrap-datepicker.gl.min.js" />
/// <reference path="locales/bootstrap-datepicker.he.min.js" />
/// <reference path="locales/bootstrap-datepicker.hr.min.js" />
/// <reference path="locales/bootstrap-datepicker.hu.min.js" />
/// <reference path="locales/bootstrap-datepicker.hy.min.js" />
/// <reference path="locales/bootstrap-datepicker.id.min.js" />
/// <reference path="locales/bootstrap-datepicker.is.min.js" />
/// <reference path="locales/bootstrap-datepicker.it.min.js" />
/// <reference path="locales/bootstrap-datepicker.it-ch.min.js" />
/// <reference path="locales/bootstrap-datepicker.ja.min.js" />
/// <reference path="locales/bootstrap-datepicker.ka.min.js" />
/// <reference path="locales/bootstrap-datepicker.kh.min.js" />
/// <reference path="locales/bootstrap-datepicker.kk.min.js" />
/// <reference path="locales/bootstrap-datepicker.ko.min.js" />
/// <reference path="locales/bootstrap-datepicker.kr.min.js" />
/// <reference path="locales/bootstrap-datepicker.lt.min.js" />
/// <reference path="locales/bootstrap-datepicker.lv.min.js" />
/// <reference path="locales/bootstrap-datepicker.me.min.js" />
/// <reference path="locales/bootstrap-datepicker.mk.min.js" />
/// <reference path="locales/bootstrap-datepicker.mn.min.js" />
/// <reference path="locales/bootstrap-datepicker.ms.min.js" />
/// <reference path="locales/bootstrap-datepicker.nb.min.js" />
/// <reference path="locales/bootstrap-datepicker.nl.min.js" />
/// <reference path="locales/bootstrap-datepicker.nl-be.min.js" />
/// <reference path="locales/bootstrap-datepicker.no.min.js" />
/// <reference path="locales/bootstrap-datepicker.pl.min.js" />
/// <reference path="locales/bootstrap-datepicker.pt.min.js" />
/// <reference path="locales/bootstrap-datepicker.pt-br.min.js" />
/// <reference path="locales/bootstrap-datepicker.ro.min.js" />
/// <reference path="locales/bootstrap-datepicker.rs.min.js" />
/// <reference path="locales/bootstrap-datepicker.rs-latin.min.js" />
/// <reference path="locales/bootstrap-datepicker.ru.min.js" />
/// <reference path="locales/bootstrap-datepicker.sk.min.js" />
/// <reference path="locales/bootstrap-datepicker.sl.min.js" />
/// <reference path="locales/bootstrap-datepicker.sq.min.js" />
/// <reference path="locales/bootstrap-datepicker.sr.min.js" />
/// <reference path="locales/bootstrap-datepicker.sr-latin.min.js" />
/// <reference path="locales/bootstrap-datepicker.sv.min.js" />
/// <reference path="locales/bootstrap-datepicker.sw.min.js" />
/// <reference path="locales/bootstrap-datepicker.th.min.js" />
/// <reference path="locales/bootstrap-datepicker.tr.min.js" />
/// <reference path="locales/bootstrap-datepicker.uk.min.js" />
/// <reference path="locales/bootstrap-datepicker.vi.min.js" />
/// <reference path="locales/bootstrap-datepicker.zh-cn.min.js" />
/// <reference path="locales/bootstrap-datepicker.zh-tw.min.js" />
/// <reference path="modernizr-2.8.3.js" />
/// <reference path="moment.min.js" />
/// <reference path="moment-with-locales.min.js" />
/// <reference path="plugins/autosize/autosize.min.js" />
/// <reference path="plugins/colorselector/colorselector.min.js" />
/// <reference path="plugins/custombox/custombox.min.js" />
/// <reference path="plugins/datatables/datatables.bootstrap.min.js" />
/// <reference path="plugins/datatables/datatables.responsive.min.js" />
/// <reference path="plugins/datatables/jquery.datatables.min.js" />
/// <reference path="plugins/dropzone/dropzone.js" />
/// <reference path="plugins/fastclick/fastclick.min.js" />
/// <reference path="plugins/fullcalendar/fullcalendar.min.js" />
/// <reference path="plugins/fullcalendar/locale-all.js" />
/// <reference path="plugins/huebee/huebee-colorpicker.min.js" />
/// <reference path="plugins/icheck/icheck.min.js" />
/// <reference path="plugins/inputmask/jquery.inputmask.date.extensions.js" />
/// <reference path="plugins/inputmask/jquery.inputmask.extensions.js" />
/// <reference path="plugins/inputmask/jquery.inputmask.js" />
/// <reference path="plugins/inputmask/jquery.inputmask.numeric.extensions.js" />
/// <reference path="plugins/inputmask/jquery.inputmask.phone.extensions.js" />
/// <reference path="plugins/inputmask/jquery.inputmask.regex.extensions.js" />
/// <reference path="plugins/jeditable/jquery.jeditable.ajaxupload.js" />
/// <reference path="plugins/jeditable/jquery.jeditable.autogrow.js" />
/// <reference path="plugins/jeditable/jquery.jeditable.charcounter.js" />
/// <reference path="plugins/jeditable/jquery.jeditable.datepicker.js" />
/// <reference path="plugins/jeditable/jquery.jeditable.js" />
/// <reference path="plugins/jeditable/jquery.jeditable.masked.js" />
/// <reference path="plugins/jeditable/jquery.jeditable.time.js" />
/// <reference path="plugins/jeditable/jquery.jeditable.timepicker.js" />
/// <reference path="plugins/jqconfirm/jquery-confirm.min.js" />
/// <reference path="plugins/jqueryprint/print.js" />
/// <reference path="plugins/labelbetter/jquery.label_better.min.js" />
/// <reference path="plugins/loadingoverlay/loadingoverlay.min.js" />
/// <reference path="plugins/paging/paging.js" />
/// <reference path="plugins/printpage/print.js" />
/// <reference path="plugins/printurl/print.js" />
/// <reference path="plugins/select2/select2.full.min.js" />
/// <reference path="plugins/slimscroll/jquery.slimscroll.min.js" />
/// <reference path="plugins/toastr/toastr.min.js" />
/// <reference path="plugins/wysihtml5/advenced.js" />
/// <reference path="plugins/wysihtml5/wysihtml5.js" />
/// <reference path="respond.matchmedia.addlistener.js" />
/// <reference path="respond.matchmedia.addlistener.min.js" />
/// <reference path="respond.min.js" />
/// <reference path="site/account.js" />
/// <reference path="site/account-create.js" />
/// <reference path="site/apartments-manage.js" />
/// <reference path="site/app.js" />
/// <reference path="site/customers.js" />
/// <reference path="site/customers-details.js" />
/// <reference path="site/demo.js" />
/// <reference path="site/fees.js" />
/// <reference path="site/generals.js" />
/// <reference path="site/home.js" />
/// <reference path="site/inventory.js" />
/// <reference path="site/inventory-ext.js" />
/// <reference path="site/invoice.js" />
/// <reference path="site/invoice-create.js" />
/// <reference path="Site/invoice-percent.js" />
/// <reference path="site/parallel-content.js" />
/// <reference path="site/payroll.js" />
/// <reference path="site/products.js" />
/// <reference path="site/products-ext.js" />
/// <reference path="site/projects.js" />
/// <reference path="site/projects-ext.js" />
/// <reference path="site/rent-and-loan.js" />
/// <reference path="site/task-todo.js" />
