﻿$(document).ready(function () {
    $('.pytania-form').submit(function (e) {
        e.preventDefault();

        var form = $(this);

        var successFn = function (data) {
            $("#tab-pytania-" + data.QuestionTypeID).append(CompareRow(data.QuestionPol, data.QuestionTranslate, 'tr-clickable question-' + data.ID, 'question', data.ID, data.QuestionTypeID, '', false));
            form.find("input[type=text], textarea").val("");
        };

        var errorFn = function () {
            toastr.error("Wystąpił błąd podczas dodawania pytania !");
        };

        GetAjaxWithFunctions("/Question/AddQuestion", "POST", $(this).serialize(), successFn, errorFn, ".box-body");        
    });

    $('.odpowiedzi-form').submit(function (e) {
        e.preventDefault();

        var form = $(this);

        var successFn = function (data) {
            $('#tab-odpowiedzi[data-question-id="' + data.QuestionID + '"]').append(CompareRow(data.AnswerPol, data.AnswerTranslate, 'question-' + data.QuestionID + '  answer-' + data.ID, 'answer', data.ID, -1, '<td>' + data.ExtraField_Get + '</td><td>' + data.IsNotApplicable_Get + '</td><td>' + data.IsError_Get + "</td>", false));
            form.find("input[type=text], textarea").val("");
            form.find("select").val("none");
        }

        var errorFn = function () {
            toastr.error("Wystąpił błąd podczas dodawania odpowiedzi !");
        }

        GetAjaxWithFunctions("/Question/AddAnswer", "POST", $(this).serialize(), successFn, errorFn, ".box-body");
    });

    $('.pytania-form-for-connect').submit(function (e) {
        e.preventDefault();

        var form = $(this);

        var successFn = function (data) {
            $("#tab-pytania-" + data.QuestionTypeID).append(CompareRow(data.QuestionPol, data.QuestionTranslate, 'tr-clickable question-' + data.ID, 'question', data.ID, data.QuestionTypeID, '', true));
            form.find("input[type=text], textarea").val("");
        }

        var errorFn = function () {
            toastr.error("Wystąpił błąd podczas dodawania pytania !");
        }

        GetAjaxWithFunctions("/Question/AddQuestion", "POST", $(this).serialize(), successFn, errorFn, ".box-body");
    });

    $('.odpowiedzi-form-for-connect').submit(function (e) {
        e.preventDefault();

        var form = $(this);

        var successFn = function (data) {
            $('#tab-odpowiedzi[data-question-id="' + data.QuestionID + '"]').append(CompareRow(data.AnswerPol, data.AnswerTranslate, 'question-' + data.QuestionID + '  answer-' + data.ID, 'answer', data.ID, -1, '<td>' + data.ExtraField_Get + '</td><td>' + data.IsNotApplicable_Get + '</td><td>' + data.IsError_Get + "</td>", true));
            form.find("input[type=text], textarea").val("");
            form.find("select").val("none");
        }

        var errorFn = function () {
            toastr.error("Wystąpił błąd podczas dodawania odpowiedzi !");
        }

        GetAjaxWithFunctions("/Question/AddAnswer", "POST", $(this).serialize(), successFn, errorFn, ".box-body");
    });


    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue',
    });
});

$(document).on("click", ".delete-question", function () {
    var id = $(this).data("id");

    var data = {
        ID: id
    };

    var successFn = function () {
        $("tr.question-" + id).remove();

        toastr.success("Pomyślnie usunięto pytanie !");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania pytania !");
    }

    var fn = function () {
        GetAjaxWithFunctions("/Question/Delete", "POST", data, successFn, errorFn, ".box-body");
    }

    Confirm("Czy na pewno chcesz usunąć pytanie?", fn);
});

$(document).on("click", ".delete-answer", function () {
    var id = $(this).data("id");

    var data = {
        ID: id
    };

    var successFn = function () {
        $("tr.answer-" + id).remove();

        toastr.success("Pomyślnie usunięto odpowiedź !");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania odpowiedzi !");
    }

    var fn = function () {
        GetAjaxWithFunctions("/Question/DeleteAnswer", "POST", data, successFn, errorFn, ".box-body");
    }

    Confirm("Czy na pewno chcesz usunąć odpowiedź ?", fn);
});

$(document).on("click", ".tr-clickable", function () {
    var ID = $(this).data("id");
    var typeID = $(this).data("type-id");
    var isConnect = $(this).data("is-connect");

    $("#tab-pytania-" + typeID + " tr").removeClass("selected");
    $(this).addClass("selected");

    $("#" + typeID + " #tab-odpowiedzi tbody tr").remove();

    var data = {
        ID: ID
    };

    var successFn = function (data) {
        $("#" + typeID + ' #question-hidden-input').val(ID);
        $("#" + typeID + ' #tab-odpowiedzi').attr("data-question-id", ID);
        $.each(data, function (index, value) {
            $("#" + typeID + ' #tab-odpowiedzi').append(CompareRow(value.AnswerPol, value.AnswerTranslate, 'question-' + value.QuestionID + '  answer-' + value.ID, 'answer', value.ID, typeID, '<td>' + value.ExtraField_Get + '</td><td>' + value.IsNotApplicable + '</td><td>' + value.IsError + "</td>", isConnect));
        });
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas ładowania listy odpowiedzi !");
    };

    GetAjaxWithFunctions("/Question/AnswersForQuestion", "POST", data, successFn, errorFn, "#tab-odpowiedzi");
});

function CompareRow(first, second, idClass, type, id, typeID, extraField, forConnect) {
    var dataAttributes = '';

    if (type === "question")
        dataAttributes = ' data-id="' + id + '" data-type-id="' + typeID + '"';

    var row = '<tr class="' + idClass + '"' + dataAttributes + '> ';

    if (forConnect && extraField === '')
        row += '<td><input type="checkbox" class="minimal" onchange="ConnectQuestionToProductProject(' + id + ', ' + product_projectID + ')"/></td>';

    row += '<td>' + first + "</td> <td>" + second + '</td>' + extraField;

    if (!forConnect)
        row += '<td><a class="text-danger cursor-pointer delete-' + type + '" data-id="' + id + '" title="Usuń"><i class="fa fa-remove"></i></a></td>';

    row += '</tr>';

    return row;
}

function ConnectQuestionToProduct(questionID, productID) {
    var data = {
        QuestionID: questionID,
        ProductID: productID
    };

    $('.question').LoadingOverlay("show");

    var successFn = function (data) {
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas przypisywania pytania !");
    };

    GetAjaxWithFunctions("/Question/ConnectQuestionToProduct", "POST", data, successFn, errorFn, ".question");
}

function ConnectQuestionToProductProject(questionID, product_projectID) {
    var data = {
        QuestionID: questionID,
        Product_ProjectID: product_projectID
    };

    $('.question').LoadingOverlay("show");

    var successFn = function (data) {
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas przypisywania pytania !");
    };

    GetAjaxWithFunctions("/Question/ConnectQuestionToProductProject", "POST", data, successFn, errorFn, ".question");
}