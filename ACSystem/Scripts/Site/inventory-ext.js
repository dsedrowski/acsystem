﻿$(document).ready(function () {
    $(".select2").select2();

    $("#add-accessory").click(function () {
        var id = $(this).data("id");

        id = (id > 0) ? id : "";

        var popup = window.open("/Popup/AddChooseAccessory/" + id, "Popup", "width=600,height=1000");
        popup.focus();
    });
    
    $("#add-file").click(function () {
        var id = $(this).data("id");

        id = (id > 0) ? id : "";

        var popup = window.open("/Popup/UploadInventoryFile/" + id, "Popup", "width=600,height=350");
        popup.focus();
    });

    $("#choose-warehouse").click(function () {
        var popup = window.open("/Popup/AddChooseWarehouse/", "Popup", "width=600,height=1000");
        popup.focus();
    });

    $("#choose-type").click(function () {
        var popup = window.open("/Popup/AddChooseType/", "Popup", "width=600,height=1000");
        popup.focus();
    });

    $(document).on("click", ".delete-accessory", function () {
        var ID = $(this).data("connect-id");
        var tr = $(this).closest("tr");

        if (ID > 0) {
            var data = {
                ID: ID
            };

            var successFn = function (data) {
                if (data.success) {
                    if (data.remove == false)
                        tr.children("td:nth-child(-n+2)").addClass("row-disabled");
                    else
                        tr.remove();
                }
                else
                    toastr.error("Wystąpił błąd podczas kasowania akcesorium");
            }

            var errorFn = function () {
                toastr.error("Wystąpił błąd podczas kasowania akcesorium");
            }

            GetAjaxWithFunctions("/Inventory/AJAX_DisactiveAccessory", "POST", data, successFn, errorFn, tr);
        }
        else {
            $(this).closest("tr").remove();
        }
    })

    $(document).on("click", ".delete-file", function () {
        var id = $(this).data("id");
        var url = $(this).data("url");
        var tr = $(this).closest("tr");

        var data = {
            ID: id,
            URL: url
        };

        var successFn = function (data) {
            if (data.success)
                tr.remove();
            else
                toastr.error("Wystąpił błąd podczas usuwania pliku !");
        }

        var errorFn = function () {
            toastr.error("Wystąpił błąd podczas usuwania pliku !");
        } 

        GetAjaxWithFunctions("/Inventory/AJAX_DeleteFile", "POST", data, successFn, errorFn, tr);
    })
    
    $(document).on('click', '.doc-name-edit', function () {
        var id = $(this).data("id");
        var name = $(this).data("name");

        $("#edit-filename-modal #ID").val(id);
        $("#edit-filename-modal #Name").val(name);

        NewModal("#edit-filename-modal");
    })

    $(document).on('submit', '.edit-filename-form', function (e) {
        e.preventDefault();

        var data = $(this).serialize();

        var successFn = function (data) {
            $("tr.doc-row-" + data.ID + ' span.doc-name-edit').html(data.Name + ' <i class="fa fa-pencil"></i>');
            $("tr.doc-row-" + data.ID + ' span.doc-name-edit').closest("input").val("TEST");
            toastr.success("Zmiana nazwy pliku przebiegła prawidłowo");
        }

        var errorFn = function () {
            toastr.error("Wystąpił błąd podczas zmiany nazwy pliku!");
        }

        GetAjaxWithFunctions('/Inventory/AJAX_EditFileName', 'POST', data, successFn, errorFn, '#files-table');
    })
});