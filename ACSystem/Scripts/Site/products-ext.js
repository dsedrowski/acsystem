﻿$(document).ready(function () {
    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue',
    });

    $('.select2').select2();
})

$(document).on("change", ".percent-change", function () {
    CountPercents();
});

$(document).on("click", "#connect-product", function () {
    var productID = $(this).data("product-id");
    var projectID = $("#ProjectID").val();
    var price = $("#ProductPrice").val();
    var p1 = $("#PercentType1").val();
    var p2 = $("#PercentType2").val();

    if (price === undefined || price === '') {
        toastr.error("Proszę podać cenę produktu dla projektu !");
        return;
    }

    var data = { ProjectID: projectID, ProductID: productID, Price: price, PercentType1: p1, PercentType2: p2 };

    var successFn = function (data) {
        if (data.success) {
            var row = '<tr class="connect-' + data.ConnectID + '">' +
                '<td><a href="/Projects/Details/' + data.ProjectID + '">' + data.ProjectName + '</a></td>' +
                '<td  id="connect-price-' + data.ConnectID + '" data-price="' + data.Price + '">' + data.Price + ' kr</td>' +
                '<td id="connect-p1-' + data.ConnectID + '" data-p="' + data.PercentType1 + '">' + ((data.PercentType1 != null) ? data.PercentType1 + "%" : "") + '</td> ' +
                '<td id="connect-p2-' + data.ConnectID + '" data-p="' + data.PercentType2 + '">' + ((data.PercentType2 != null) ? data.PercentType2 + "%" : "") + '</td> ' +
                '<td>' +
                '<div class="crud-container">' +
                '<span data-toggle="tooltip" data-placement="bottom" title="Edytuj cenę" data-connect-id="' + data.ConnectID + '" class="crud crud-edit edit-connect-product"><i class="fa fa-pencil"></i></span>' +
                '<span data-toggle="tooltip" data-placement="bottom" title="Usuń" data-connect-id="' + data.ConnectID + '" class="crud crud-delete delete-connect-product"><i class="glyphicon glyphicon-remove"></i></span>' +
                '</div>' +
                '</td>' +
                '</tr>';

            $('.products-table').append(row);

            $('#ProductId option[value="' + data.ProductID + '"]').remove();

            toastr.success("Pomyślnie powiązano produkt z projektem");
        }
        else {
            toastr.error("Wystąpił błąd podczas wiązania produktu z projektem !");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas wiązania produktu z projektem !");
    }

    GetAjaxWithFunctions("/Projects/ConnectProjectWithProduct", "POST", data, successFn, errorFn, "");
});

$(document).on("click",
    ".edit-connect-product",
    function() {
        var connectID = $(this).data("connect-id");
        var price = $("#connect-price-" + connectID).data("price");
        var p1 = $("#connect-p1-" + connectID).data("p");
        var p2 = $("#connect-p2-" + connectID).data("p");

        $("#ToEditConnectID").val(connectID);
        $("#ToEditPrice").val(price);
        $("#ToEditPercentType1").val(p1);
        $("#ToEditPercentType2").val(p2);

        NewModal("#editprice-modal");
    });

$(document).on("click", ".delete-connect-product", function () {
    var connectID = $(this).data("connect-id");

    var data = { ConnectID: connectID };

    var successFn = function (data) {
        if (data.success) {
            $(".connect-" + connectID).remove();

            toastr.success("Pomyślnie usunięto powiązanie.");
        }
        else {
            toastr.error("Wystąpił błąd podczas usuwania powiązania !");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania powiązania !");
    }

    var fn = function () {
        GetAjaxWithFunctions("/Projects/DeleteConnectWithProjectAndProduct", "POST", data, successFn, errorFn, "");
    }

    Confirm("Czy na pewno chcesz usunąć powiązanie?", fn);
});

$(document).on("click",
    ".save-connect-product-price",
    function() {
        var connectID = $(".custombox-content #ToEditConnectID").val();
        var price = $(".custombox-content #ToEditPrice").val();
        var p1 = $(".custombox-content #ToEditPercentType1").val();
        var p2 = $(".custombox-content #ToEditPercentType2").val();

        var data = {
            ConnectID: connectID,
            Price: price,
            PercentType1: p1,
            PercentType2: p2
        };

        var successFn = function(data) {
            if (data.success === true) {
                $("#connect-price-" + connectID).html(price + " kr");
                $("#connect-price-" + connectID).data("price", price);

                $("#connect-p1-" + connectID).html(p1 + "%");
                $("#connect-p1-" + connectID).data("p", p1);

                $("#connect-p2-" + connectID).html(p2 + "%");
                $("#connect-p2-" + connectID).data("p", p2);

                Custombox.modal.close();
            } else {
                toastr.error("Wystąpił błąd podczas zapisywania ceny!");
                Custombox.modal.close();
            }
        }

        var errorFn = function() {
            toastr.error("Wystąpił błąd podczas zapisywania ceny!");
            Custombox.modal.close();
        }

        GetAjaxWithFunctions("/Projects/EditConnectProductPrice", "POST", data, successFn, errorFn, "");
    });

$(document).on("click", "#group-product", function () {
    var productID = $(this).data("product-id");
    var subID = $("#SubProductID").val();
    var count = $("#ProductCount").val();
    var createTask = $("#CreateTask").val();

    if (count === undefined || count === "") {
        toastr.error("Proszę podać ilosc produktu !");
        return;
    }

    var data = { ProductID: productID, SubProductID: subID, Quantity: count, CreateTask: (createTask == 1) ? true : false };

    var successFn = function (data) {
        if (data.success) {
            var row = '<tr class="group-' + data.Product_GroupID + '">' +
                '<td><a href="/Products/Details/' + data.ProductSubID + '">' + data.ProductName + "</a></td>" +
                '<td id="group-quantity-' + data.Product_GroupID + '" data-quantity="' + data.Quantity + '">' + data.Quantity + " " + data.Jm + "</td>" +
                '<td id="group-task-' + data.Product_GroupID + '" data-task="' + data.CreateTask + '">' + ((data.CreateTask) ? "TAK" : "NIE") + "</td>" +
                "<td>" +
                '<div class="crud-container">' +
                '<span data-toggle="tooltip" data-placement="bottom" title="Edytuj" data-group-id="' + data.Product_GroupID + '" class="crud crud-edit edit-group-product"><i class="fa fa-pencil"></i></span>' +
                '<span data-toggle="tooltip" data-placement="bottom" title="Usuń" data-group-id="' + data.Product_GroupID + '" class="crud crud-delete delete-group-product"><i class="glyphicon glyphicon-remove"></i></span>' +
                "</div>" +
                "</td>" +
                "</tr>";

            $(".group-table").append(row);

            $('#SubProductID option[value="' + data.ProductSubID + '"]').remove();

            toastr.success("Pomyślnie powiązano produkt");
        }
        else {
            toastr.error("Wystąpił błąd podczas wiązania produktu !");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas wiązania produktu !");
    }

    GetAjaxWithFunctions("/Products/GroupProduct", "POST", data, successFn, errorFn, "");
});

$(document).on("click",
    ".edit-group-product",
    function () {
        var groupID = $(this).data("group-id");
        var quantity = $("#group-quantity-" + groupID).data("quantity");
        var task = $("#group-task-" + groupID).data("task");

        $("#ToEditGroupID").val(groupID);
        $("#ToEditQuantity").val(quantity);
        $("#ToEditTask").attr("checked", (task === "True"));

        NewModal("#editgroup-modal");
    });

$(document).on("click",
    ".save-group-product",
    function () {

        var groupID = $(".custombox-content #ToEditGroupID").val();
        var quantity = $(".custombox-content #ToEditQuantity").val();
        var task = $(".custombox-content #ToEditTask").is(":checked");

        var data = {
            GroupID: groupID,
            Quantity: quantity,
            CreateTask: task
        };

        var successFn = function (data) {
            if (data.success === true) {
                $("#group-quantity-" + groupID).html(quantity);
                $("#group-quantity-" + groupID).data("quantity", quantity);

                $("#group-task-" + groupID).html((task) ? "TAK" : "NIE");
                $("#group-task-" + groupID).data("task", (task) ? "True" : "False");

                Custombox.modal.close();
            } else {
                toastr.error("Wystąpił błąd podczas zapisywania!");
                Custombox.modal.close();
            }
        }

        var errorFn = function () {
            toastr.error("Wystąpił błąd podczas zapisywania!");
            Custombox.modal.close();
        }

        GetAjaxWithFunctions("/Products/EditGroupProduct", "POST", data, successFn, errorFn, "");
    });

$(document).on("click",
    ".delete-group-product",
    function() {
        var id = $(this).data("group-id");

        var successFn = function(data) {
            $(".group-" + id).remove();

            toastr.success("Pomyślnie usunięto wpis !");
        }

        var errorFn = function() {
            toastr.error("Wystąpił błąd podczas usuwania wpisu !");
        }

        var fn = function() {
            GetAjaxWithFunctions("/Products/DeleteGroupProduct",
                "POST",
                { GroupID: id },
                successFn,
                errorFn,
                ".group-table");
        }

        Confirm("Czy na pewno chcesz usunąć wpis?", fn);
    });

function CountPercents() {
    var productPrice = $("#Price").val().replace(",", ".");
    var p1 = $("#Type1").val().replace(",", ".");
    var p2 = $("#Type2").val().replace(",", ".");
    var p3 = $("#Type3").val().replace(",", ".");
    var p4 = $("#Type4").val().replace(",", ".");
    var p5 = $("#Type5").val().replace(",", ".");
    var p6 = $("#Type6").val().replace(",", ".");

    var pt1 = Math.ceil(Number(productPrice) * (Number(p1) / 100));
    var pt2 = Math.ceil(Number(productPrice) * (Number(p2) / 100));
    var pt3 = Math.ceil(Number(productPrice) * (Number(p3) / 100));
    var pt4 = Math.ceil(Number(productPrice) * (Number(p4) / 100));
    var pt5 = Math.ceil(Number(productPrice) * (Number(p5) / 100));
    var pt6 = Math.ceil(Number(productPrice) * (Number(p6) / 100));

    $(".pt1").html(pt1 + " SEK");
    $(".pt2").html(pt2 + " SEK");
    $(".pt3").html(pt3 + " SEK");
    $(".pt4").html(pt4 + " SEK");
    $(".pt5").html(pt5 + " SEK");
    $(".pt6").html(pt6 + " SEK");
}