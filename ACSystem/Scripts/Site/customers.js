﻿var tableId = '#customers-list';
GetTableFilterWebStorage("customer", tableId);
var table = LoadTable(tableId);

var statusFilter = $('.status-filter');

$(document).ready(function () {
    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue'
    });

    statusFilter.on('ifChanged', function () {
        ChangeFilter();
        DefaultPropTable();
        LoadTable(tableId);
    });
});

$(document).on('click', '.customer-details', function () {
    location.assign('/Customers/Details/' + $(this).data('customer-id'));
});

$(document).on('click', '.customer-edit', function () {
    location.assign('/Customers/Edit/' + $(this).data('customer-id'));
});

$(document).on('click', '.customer-delete', function () {
    var id = $(this).data('customer-id');

    var fn = function () {
        $.ajax({
            url: '/Customers/Delete',
            dataType: 'json',
            type: 'POST',
            data: { id: id },
            success: function (data) {
                if (data.success === 'true') {
                    location.reload();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            }
        });

    };

    Confirm("Czy na pewno chcesz usunąć klienta?", fn);
});

$(document).on('click', '.pagination a', function () {
    var page = $(this).data('page');

    if (page !== null)
    {
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .prevBtn', function () {
    var page = $(tableId).data('page');

    if (page !== null) {
        page -= 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .nextBtn', function () {
    var page = $(tableId).data('page');

    if (page !== null) {
        page += 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '#search-table-btn', function () {
    DefaultPropTable();
    LoadTable(tableId);
});

$("#search-table-input").on("keyup", function (event) {
    if (event.keyCode === 13) {
        $(table).html("");
        DefaultPropTable();
        LoadTable(tableId);
    }
});

$(document).on('click', tableId + ' th.clickable', function () {
    DefaultPropTable();
    $(tableId).data('order-by', $(this).data('id'));

    var currentDest = $(this).data('dest');

    $(tableId + ' th.clickable').data('dest', 'desc');

    if (currentDest === 'asc')
        $(this).data('dest', 'desc');
    if (currentDest === 'desc') $(this).data('dest', 'asc');

    $(tableId).data('dest', $(this).data('dest'));

    LoadTable(tableId);
});

$(document).on("click", "#synchronize-customers", function () {
    $.LoadingOverlay("show");
    $.get("/Fortnox/Customer_Synchronize", function (data) {
        if (data.success)
            toastr.success(data.message);
        else
            toastr.error(data.message);

        if (data.reload)
            location.reload();

        $.LoadingOverlay("hide");
    });
});

function LoadTable(table) {
    var page = $(table).data('page');
    var dest = $(table).data('dest');
    var orderBy = $(table).data('order-by');
    var search = $('#search-table-input').val();
    var filter = $(table).data('filter');

    SetTableFilterWebStorage("customer", page, dest, orderBy, filter, search);
    CreateSortingIcon(dest, orderBy);

    $.ajax({
        url: '/Customers/CustomersList',
        dataType: 'json',
        type: 'GET',
        data: { page: page, dest: dest, orderBy: orderBy, search: search, filter: filter },
        success: function (data) {
            FillTable(data.data, table);
            GeneratePagination(data.pages, table);
        },
        error: function (XMLHttpRequest) {
            $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
        }
    });
}

function FillTable(data, table) {
    var row = "";

    $.each(data, function (index, value) {
        var lockout = "";

        if (value.IsLocked)
            lockout = '<span class="text-danger"> (ZABLOKOWANY)</span>';

        row += '<tr class="tr-clickable">' +
        '<td class="customer-details" data-customer-id="' + value.Id + '">' + value.Name + lockout + '</td>' +
        '<td class="customer-details" data-customer-id="' + value.Id + '">' + value.Address + '</td>' +
        '<td class="customer-details" data-customer-id="' + value.Id + '"><a href="mailto:' + value.Email + '" title="' + value.Email + '">' + value.Email + '</a></td>' +
        '<td>' + GetCrudHtml(value.Id) + '</td>' +
        '</tr>';
    });

    $(table + ' tbody').html(row);
}

function DefaultPropTable() {
    $(tableId).data('page', 1);
    $(tableId).data('dest', 'asc');
    $(tableId).data('order-by', 'name');
}

function GetCrudHtml(id) {
    return '<div class="crud-container">' +
                '<a class="crud crud-details" href="/Customers/Details/' + id + '" data-toggle="tooltip", data-placement="bottom" title="Szczegóły"><i class="glyphicon glyphicon-eye-open"></i></a>' +
                '<a class="crud crud-edit" href="/Customers/Edit/' + id + '" data-toggle="tooltip", data-placement="bottom" title="Edytuj"><i class="glyphicon glyphicon-pencil"></i></a>' +
                '<span class="crud crud-delete customer-delete" data-customer-id="' + id + '" data-toggle="tooltip", data-placement="bottom" title="Usuń"><i class="glyphicon glyphicon-remove"></i></span>' +
            "</div>";
}

function GeneratePagination(pages,table){
    var currentPage = $(table).data('page');

    var prevBtn = '<li><a href="#" class="prevBtn">«</a></li>';
    var nextBtn = '<li><a href="#" class="nextBtn">»</a></li>';

    var pagination = '';

    if (pages > 1 && currentPage > 1)
        pagination += prevBtn;

    for (var i = 1; i <= pages; i++) {
        if (i === currentPage)
            pagination += '<li class="active"><a href="#">' + i + '</a></li>';
        else
            pagination += '<li><a href="#" data-page="' + i + '">' + i + '</a></li>';
    }

    if (pages > 1 && currentPage < pages)
        pagination += nextBtn;

    $('.pagination').html(pagination);
}

function CreateSortingIcon(dest, orderBy) {
    var iconAsc = '<i class="glyphicon glyphicon-sort-by-alphabet sort-column-icon"></i>';
    var iconDesc = '<i class="glyphicon glyphicon-sort-by-alphabet-alt sort-column-icon"></i>';

    $(tableId + ' thead tr th i').remove();

    if (orderBy === 'name' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconAsc);
    else if (orderBy === 'name' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconDesc);
    else if (orderBy === 'address' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconAsc);
    else if (orderBy === 'address' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconDesc);
    else if (orderBy === 'email' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconAsc);
    else if (orderBy === 'email' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconDesc);
}

function ChangeFilter() {
    var isFirst = true;
    var filter = "";

    var checkbox1 = $('#fltr-chbx-1').prop('checked');

    if (checkbox1 === true) {
        filter += "0";
        isFirst = false;
    }

    $(tableId).data('filter', filter);
}