﻿var tableId = '#payroll-list';
var table = LoadTable(tableId);

var statusFilter = $('.status-filter');

$(document).ready(function () {


    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue',
    });

    statusFilter.on('ifChanged', function () {
        ChangeFilter();
        LoadTable(tableId);
    });

    if (typeof select2 !== "undefined")
        $('#FitterID').select2({ width: '100%' });

    $("#check-all").click(function () {
        $('input[type="checkbox"].minimal.all-element-active').iCheck("check");
    });

    $("#uncheck-all").click(function () {
        $('input[type="checkbox"].minimal.all-element-active').iCheck("uncheck");
    });

    $('input[type="checkbox"].minimal.all-element-active').on('ifChanged', function () {
        var elements = $(this).data('checkboxes');

        var isChecked = $(this).parent('[class*="icheckbox"]').hasClass("checked");

        if (!isChecked)
            $("." + elements).iCheck("check");
        else
            $("." + elements).iCheck("uncheck");
    });

    $('input[type="checkbox"].minimal.element-active').on('ifChanged', function () {
        var fitter = $(this).data("fitter");

        var rowPaySek = Number($(this).data("row-pay-sek"));
        //var rowPayPln = Number($(this).data("row-pay-pln").replace(',', '.'));
        var rowFeeSek = Number($(this).data("row-fee-sek"));
        //var rowFeePln = Number($(this).data("row-fee-pln").replace(',', '.'));
        var rowHourCostSek = Number($(this).data("row-hour-cost-sek"));
        //var rowHourCostPln = Number($(this).data("row-hour-cost-pln").replace(',','.'));


        var checked = $(this).parent('[class*="icheckbox"]').hasClass("checked");

        var fitterSekSum = Number($(".bg-fitter-" + fitter + " span.sek").html());
        var fitterPlnSum = Number($(".bg-fitter-" + fitter + " span.pln").html().replace(',', '.'));

        if (!checked) {
            $(".bg-fitter-" + fitter + " span.sek").html(fitterSekSum + (rowPaySek - rowHourCostSek) + rowFeeSek);
            //$(".bg-fitter-" + fitter + " span.pln").html((fitterPlnSum + (rowPayPln - rowHourCostPln) + rowFeePln).toFixed(2));
        }
        else {
            $(".bg-fitter-" + fitter + " span.sek").html(fitterSekSum - (rowPaySek - rowHourCostSek) - rowFeeSek);
            //$(".bg-fitter-" + fitter + " span.pln").html((fitterPlnSum - (rowPayPln - rowHourCostPln) - rowFeePln).toFixed(2));
        }

        fitterSekSum = Number($(".bg-fitter-" + fitter + " span.sek").html());
        //fitterPlnSum = Number($(".bg-fitter-" + fitter + " span.pln").html().replace(',', '.'));

        if (fitterSekSum < 0)
            $(".bg-fitter-" + fitter).css("background-color", "#953b39");
        else if (fitterSekSum > 0)
            $(".bg-fitter-" + fitter).css("background-color", "#468847");
        else
            $(".bg-fitter-" + fitter).css("background-color", "#777");

        CalculatePayrollSum(this);
        CheckFittersSum();
    });
});

$(document).on('change', '.all-element-active', function () {
    var elements = $(this).data('checkboxes');

    var isChecked = $(this).is(':checked');

    $('.' + elements).prop('checked', isChecked);
})

$(document).on('change', '.element-active', function () {
    var fitter = $(this).data("fitter");

    var rowPaySek = Number($(this).data("row-pay-sek"));
    var rowPayPln = Number($(this).data("row-pay-pln").replace(',', '.'));
    var rowFeeSek = Number($(this).data("row-fee-sek"));
    var rowFeePln = Number($(this).data("row-fee-pln").replace(',', '.'));

    var checked = $(this).is(":checked");

    var fitterSekSum = Number($(".bg-fitter-" + fitter + " span.sek").html());
    var fitterPlnSum = Number($(".bg-fitter-" + fitter + " span.pln").html().replace(',', '.'));

    if (checked) {
        $(".bg-fitter-" + fitter + " span.sek").html(fitterSekSum + rowPaySek + rowFeeSek);
        $(".bg-fitter-" + fitter + " span.pln").html((fitterPlnSum + rowPayPln + rowFeePln).toFixed(2));
    }
    else {
        $(".bg-fitter-" + fitter + " span.sek").html(fitterSekSum - rowPaySek - rowFeeSek);
        $(".bg-fitter-" + fitter + " span.pln").html((fitterPlnSum - rowPayPln - rowFeePln).toFixed(2));
    }
})

$(document).on('click', '.payroll-details', function () {
    location.assign('/Office/PayrollDetails/' + $(this).data('payroll-id'));
});

$(document).on('click', '.payroll-delete', function () {
    var id = $(this).data('payroll-id');

    var fn = function () {
        $.ajax({
            url: '/Office/PayrollDelete',
            dataType: 'json',
            type: 'POST',
            data: { id: id },
            success: function (data) {
                if (data.success === 'true') {
                    LoadTable(tableId);
                    toastr.success("Pomyślnie usunięto listę płacową");
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            }
        });
    };

    Confirm("Czy na pewno chcesz usunąć listę płacową?", fn);
});

$(document).on('click', '.create-payroll', function () {
    //LoadModalContent("#modal", "/Office/Modal_AddPayroll");
    //NewModal("#modal-add-payroll");
});

$(document).on('click', tableId + ' th.clickable', function () {
    DefaultPropTable();
    $(tableId).data('order-by', $(this).data('id'))

    var currentDest = $(this).data('dest');

    $(tableId + ' th.clickable').data('dest', 'desc');

    if (currentDest === 'asc')
        $(this).data('dest', 'desc');
    if (currentDest === 'desc') $(this).data('dest', 'asc');

    $(tableId).data('dest', $(this).data('dest'));

    LoadTable(tableId);
});

$(document).on('click', '#search-table-btn', function () {
    DefaultPropTable();
    LoadTable(tableId);
});

$("#search-table-input").on("keyup", function (event) {
    if (event.keyCode === 13) {
        $(table).html("");
        DefaultPropTable();
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination a', function () {
    var page = $(this).data('page');

    if (page !== null) {
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .prevBtn', function () {
    var page = $(tableId).data('page');;

    if (page !== null) {
        page -= 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .nextBtn', function () {
    var page = $(tableId).data('page');;

    if (page !== null) {
        page += 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.close-modal', function () {
    Custombox.close();
});

$(document).on('click', '.print-fitter-payroll', function () {
    var box = $(this).data('box-id');

    PrintURL(box);
});

$(document).on('change', '.for-fitter-check', function () {
    if ($(this).is(':checked')) {
        $('#FitterID').prop("disabled", false);
    }
    else {
        $('#FitterID').prop("disabled", true);
    }
});

$(document).on('click', '.payroll-to-pay', function () {
    var payrollID = $(this).data('payroll-id');
    var fitterID = $(this).data('fitter-id');

    var successFn = function (data) {
        if (data.success === true) {
            location.reload();
        }
        else {
            toastr.error("Wystąpił błąd podczas zmiany statusu.")
        }
    }

    var errorFn = function (data) {
        toastr.error("Wystąpił błąd podczas zmiany statusu");
    }

    var data = { ID: payrollID, FitterID: fitterID, Status: 0 }

    var fn = function () {
        GetAjaxWithFunctions("/Office/ChangePayrollStatus", "POST", data, successFn, errorFn, '');
    }

    Confirm("Czy napewno chcesz zmienić status?", fn);
});

$(document).on('click', '.payroll-stoped', function () {
    var payrollID = $(this).data('payroll-id');
    var fitterID = $(this).data('fitter-id');

    var successFn = function (data) {
        if (data.success === true) {
            location.reload();
        }
        else {
            toastr.error("Wystąpił błąd podczas zmiany statusu.")
        }
    }

    var errorFn = function (data) {
        toastr.error("Wystąpił błąd podczas zmiany statusu");
    }

    var data = { ID: payrollID, FitterID: fitterID, Status: 1 }

    var fn = function () {
        GetAjaxWithFunctions("/Office/ChangePayrollStatus", "POST", data, successFn, errorFn, '');
    }

    Confirm("Czy napewno chcesz zmienić status?", fn);
});

$(document).on('click', '.payroll-payed', function () {
    var payrollID = $(this).data('payroll-id');
    var fitterID = $(this).data('fitter-id');

    var successFn = function (data) {
        if (data.success === true) {
            location.reload();
        }
        else {
            toastr.error("Wystąpił błąd podczas zmiany statusu.")
        }
    }

    var errorFn = function (data) {
        toastr.error("Wystąpił błąd podczas zmiany statusu");
    }

    var data = { ID: payrollID, FitterID: fitterID, Status: 2 }

    var fn = function () {
        GetAjaxWithFunctions("/Office/ChangePayrollStatus", "POST", data, successFn, errorFn, '');
    }

    Confirm("Czy napewno chcesz zmienić status?", fn);
});


$(document).on('click', '.send-fitter-payroll', function () {
    var payrollID = $(this).data('payroll-id');
    var fitterID = $(this).data('fitter-id');

    var successFn = function (data) {
        if (data.success === true) {
            toastr.success("Pomyślnie wysłano listę płac.");
        }
        else {
            toastr.error("Wystąpił błąd podczas wysyłania listy płac.");
        }
    };

    var errorFn = function (data) {
        toastr.error("Wystąpił błąd podczas wysyłania listy płac.");
    };

    var data = { ID: payrollID, FitterID: fitterID };

    GetAjaxWithFunctions("/Office/SendPayrollToFitter", "POST", data, successFn, errorFn, '');
});

$(document).on('click', '.save-payroll-btn', function () {
    $(this).attr('disabled', true);
    $('#PayrollSaveForm').submit();
})

function LoadTable(table) {
    var page = $(table).data('page');
    var dest = $(table).data('dest');
    var orderBy = $(table).data('order-by');
    var filter = $(table).data('filter');
    var search = $('#search-table-input').val();

    CreateSortingIcon(dest, orderBy);

    var successFn = function (data) {
        FillTable(data.data, table);
        GeneratePagination(data.pages, table);
    };

    var errorFn = function () {
        $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.')
    };

    var ajaxData = { page: page, dest: dest, orderBy: orderBy, search: search, filter: filter }

    GetAjaxWithFunctions('/Office/PayrollList', "GET", ajaxData, successFn, errorFn, tableId);
}

function FillTable(data, table) {
    var row = "";

    if (data !== null) {
        $.each(data, function (index, value) {
            row += '<tr class="tr-clickable">' +
                '<td class="payroll-details" data-payroll-id="' + value.Id + '">' + value.No + '</td>' +
                '<td class="payroll-details" data-payroll-id="' + value.Id + '">' + value.Date + '</td>' +
                '<td class="payroll-details" data-payroll-id="' + value.Id + '">' + value.SumMinusHours + " / " + value.Sum + " SEK</td>" +
                '<td class="payroll-details" data-payroll-id="' + value.Id + '">' + value.PayLeft + "</td>" +
                '<td  class="payroll-details with-label" data-payroll-id="' + value.Id + '">' + CreateLabel(value.Status) + '</td>' +
                '<td>' + GetCrudHtml(value.Id) + '</td>' +
                '</tr>';
        });
    }

    $(table + ' tbody').html(row);
}

function DefaultPropTable() {
    $(tableId).data('page', 1);
    $(tableId).data('dest', 'desc');
    $(tableId).data('order-by', 'date');
}

function GetCrudHtml(id) {
    return '<div class="crud-container">' +
        '<a class="crud crud-details" href="/Office/PayrollDetails/' + id + '" data-toggle="tooltip", data-placement="bottom" title="Szczegóły"><i class="glyphicon glyphicon-eye-open"></i></a>' +
        '<span class="crud crud-delete payroll-delete" data-payroll-id="' + id + '" data-toggle="tooltip", data-placement="bottom" title="Usuń"><i class="glyphicon glyphicon-remove"></i></span>' +
        '</div>';
}

function GeneratePagination(pages, table) {
    var currentPage = $(table).data('page');

    var prevBtn = '<li><a href="#" class="prevBtn">«</a></li>';
    var nextBtn = '<li><a href="#" class="nextBtn">»</a></li>'

    var pagination = '';

    if (pages > 1 && currentPage > 1)
        pagination += prevBtn;

    for (var i = 1; i <= pages; i++) {
        if (i === currentPage)
            pagination += '<li class="active"><a href="#">' + i + '</a></li>';
        else
            pagination += '<li><a href="#" data-page="' + i + '">' + i + '</a></li>';
    }

    if (pages > 1 && currentPage < pages)
        pagination += nextBtn;

    $('.pagination').html(pagination);
}

function CreateSortingIcon(dest, orderBy) {
    var iconAsc = '<i class="glyphicon glyphicon-sort-by-alphabet sort-column-icon"></i>';
    var iconDesc = '<i class="glyphicon glyphicon-sort-by-alphabet-alt sort-column-icon"></i>'

    $(tableId + ' thead tr th i').remove();

    if (orderBy === 'number' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconAsc);
    else if (orderBy === 'number' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconDesc);
    else if (orderBy === 'date' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconAsc);
    else if (orderBy === 'date' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconDesc);
    else if (orderBy === 'sum' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconAsc);
    else if (orderBy === 'sum' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconDesc);
    else if (orderBy === 'status' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(4)').append(iconAsc);
    else if (orderBy === 'status' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(4)').append(iconDesc);
}

function CreateLabel(status) {
    if (status === 0)
        return '<span class="label label-default">Gotowa</span>';

    if (status === 1)
        return '<span class="label label-warning">Częściowo wypłacona</span>';

    if (status === 2)
        return '<span class="label label-success">Zamknięta</span>';
}

function ChangeFilter() {
    var isFirst = true;
    var filter = "";

    var checkbox1 = $('#fltr-chbx-1').prop('checked');
    var checkbox2 = $('#fltr-chbx-2').prop('checked');
    var checkbox3 = $('#fltr-chbx-3').prop('checked');

    if (checkbox1 === true) {
        filter += "0";
        isFirst = false;
    }

    if (checkbox2 === true) {
        if (isFirst !== true)
            filter += ",";

        filter += 1;
        isFirst = false;
    }

    if (checkbox3 === true) {
        if (isFirst !== true)
            filter += ",";

        filter += 2;
        isFirst = false;
    }

    $(tableId).data('filter', filter);
}

function CalculatePayrollSum(clicked) {
    var rowPaySek = Number($(clicked).data("row-pay-sek"));
    //var rowPayPln = Number($(clicked).data("row-pay-pln").replace(',', '.'));
    var rowFeeSek = Number($(clicked).data("row-fee-sek"));
    //var rowFeePln = Number($(clicked).data("row-fee-pln").replace(',', '.'));
    var rowHourCostSek = Number($(clicked).data("row-hour-cost-sek"));
    //var rowHourCostPln = Number($(clicked).data("row-hour-cost-pln").replace(',', '.'));

    var clickedChecked = $(clicked).parent('[class*="icheckbox"]').hasClass("checked");

    var paySek = (!clickedChecked) ? rowPaySek : 0.00;
    //var payPln = (!clickedChecked) ? rowPayPln : 0.00;
    var feeSek = (!clickedChecked) ? rowFeeSek : 0.00;
    //var feePln = (!clickedChecked) ? rowFeePln : 0.00;
    var hourCostSek = (!clickedChecked) ? rowHourCostSek : 0.00;
    //var hourCostPln = (!clickedChecked) ? rowHourCostPln : 0.00;

    $('input[type="checkbox"].minimal.element-active').each(function () {
        if (this !== clicked) {
            var checked = $(this).parent('[class*="icheckbox"]').hasClass("checked");

            if (checked) {

                var thisPaySek = Number($(this).data("row-pay-sek"));
                //var thisPayPln = Number($(this).data("row-pay-pln").replace(',', '.'));
                var thisFeeSek = Number($(this).data("row-fee-sek"));
                //var thisFeePln = Number($(this).data("row-fee-pln").replace(',', '.'));
                var thisHourCostSek = Number($(this).data("row-hour-cost-sek"));
                //var thisHourCostPln = Number($(this).data("row-hour-cost-pln").replace(',', '.'));

                paySek += thisPaySek;
                //payPln += thisPayPln;
                feeSek += thisFeeSek;
                //feePln += thisFeePln;
                hourCostSek += thisHourCostSek;
                //hourCostPln += thisHourCostPln;
            }
        }
    });

    var sumSek = (paySek - hourCostSek) + feeSek;
    //var sumPln = (payPln - hourCostPln) + feePln;

    $("#sum-sek").html(sumSek);
    //$("#sum-pln").html(sumPln.toFixed(2));
}

function CheckFittersSum() {
    var anyOnMinus = false;

    $(".sum-badge.sek").each(function () {
        var badgeSum = Number($(this).html());

        if (badgeSum < 0)
            anyOnMinus = true;
    });

    //$(".save-payroll-btn").attr("disabled", anyOnMinus);
}