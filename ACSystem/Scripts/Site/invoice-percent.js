﻿var tableID = "#table-list";
var table = LoadTable(tableID);

$(document).on('click', '.percent-details', function () {
    location.assign('/Parallel/InvoicePercentDetails/' + $(this).data('percent-id'));
});

$(document).on('click', '.pagination a', function () {
    var page = $(this).data('page');

    if (page != null) {
        $(tableID).data('page', page);
        LoadTable(tableID);
    }
});

$(document).on('click', '.pagination .prevBtn', function () {
    var page = $(tableID).data('page');;

    if (page != null) {
        page -= 1;
        $(tableID).data('page', page);
        LoadTable(tableID);
    }
});

$(document).on('click', '.pagination .nextBtn', function () {
    var page = $(tableID).data('page');;

    if (page != null) {
        page += 1;
        $(tableID).data('page', page);
        LoadTable(tableID);
    }
});

$(document).on('click', tableID + ' th.clickable', function () {
    DefaultPropTable();
    $(tableID).data('order-by', $(this).data('id'))

    var currentDest = $(this).data('dest');

    $(tableID + ' th.clickable').data('dest', 'desc');

    if (currentDest === 'asc')
        $(this).data('dest', 'desc');
    if (currentDest === 'desc') $(this).data('dest', 'asc');

    $(tableID).data('dest', $(this).data('dest'));

    LoadTable(tableID);
});

function LoadTable(table) {
    var page = $(table).data('page');
    var dest = $(table).data('dest');
    var orderBy = $(table).data('order-by');

    CreateSortingIcon(dest, orderBy);

    var successFn = function (data) {
        FillTable(data.data, table);
        GeneratePagination(data.pages, table);
    }

    var errorFn = function (XMLHttpRequest) {
        $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
    }

    var data = { page: page, dest: dest, orderBy: orderBy };

    GetAjaxWithFunctions("/Parallel/AJAX_InvoicePercentList", "GET", data, successFn, errorFn, table);
}

function FillTable(data, table) {
    var row = "";

    if (data != null) {
        $.each(data, function (index, value) {
            row += '<tr class="tr-clickable">' +
                '<td class="percent-details" data-percent-id="' + value.ID + '">' + value.GenerateDate_String + '</td>' +
                '<td class="percent-details" data-percent-id="' + value.ID + '">' + value.AmountSum_String + ' SEK</td>' +
                '<td class="percent-details" data-percent-id="' + value.ID + '">' + value.CalcPercent + '%</td>'           
                '</tr>';
        });
    }

    $(table + ' tbody').html(row);
}

function DefaultPropTable() {
    $(tableID).data('page', 1);
    $(tableID).data('dest', 'desc');
    $(tableID).data('order-by', 'date');
}

function GeneratePagination(pages, table) {
    var currentPage = $(table).data('page');

    var prevBtn = '<li><a href="#" class="prevBtn">«</a></li>';
    var nextBtn = '<li><a href="#" class="nextBtn">»</a></li>'

    var pagination = '';

    if (pages > 1 && currentPage > 1)
        pagination += prevBtn;

    for (var i = 1; i <= pages; i++) {
        if (i === currentPage)
            pagination += '<li class="active"><a href="#">' + i + '</a></li>';
        else
            pagination += '<li><a href="#" data-page="' + i + '">' + i + '</a></li>';
    }

    if (pages > 1 && currentPage < pages)
        pagination += nextBtn;

    $('.pagination').html(pagination);
}

function CreateSortingIcon(dest, orderBy) {
    var iconAsc = '<i class="glyphicon glyphicon-sort-by-alphabet sort-column-icon"></i>';
    var iconDesc = '<i class="glyphicon glyphicon-sort-by-alphabet-alt sort-column-icon"></i>'

    $(tableID + ' thead tr th i').remove();

    if (orderBy === 'date' && dest === 'asc')
        $(tableID + ' thead tr th:nth-child(1)').append(iconAsc);
    else if (orderBy === 'date' && dest === 'desc')
        $(tableID + ' thead tr th:nth-child(1)').append(iconDesc);
    else if (orderBy === 'amount' && dest === 'asc')
        $(tableID + ' thead tr th:nth-child(2)').append(iconAsc);
    else if (orderBy === 'amount' && dest === 'desc')
        $(tableID + ' thead tr th:nth-child(2)').append(iconDesc);
    else if (orderBy === 'percent' && dest === 'asc')
        $(tableID + ' thead tr th:nth-child(4)').append(iconAsc);
    else if (orderBy === 'percent' && dest === 'desc')
        $(tableID + ' thead tr th:nth-child(4)').append(iconDesc);
}