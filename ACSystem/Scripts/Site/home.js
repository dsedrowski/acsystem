﻿$(document).ready(function () {
    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue',
    });

    $('input[type="checkbox"].minimal').iCheck('check');

    $('#EventType').select2();

    $('.filter').on('ifChanged', function () {
        SaveCurrentData();

        $('#calendar').fullCalendar('destroy');
        CalendarInit();
    });

    CalendarInit();
});

$(document).on('click', '.close-modal', function () {
    Custombox.modal.close();
});

$(document).on('click', '#add-event', function () {
    var eventType = $('#EventType').val();

    $('#box-for-choose-type').hide('slow');

    switch (eventType) {
        case "0":
            $('#box-for-task-event').show('slow');
            break;
        case "1":
            $('#box-for-fitter-event').show('slow');
            break;
    }
});

$(document).on('click', '#add-task', function () {
    var id = $('#ApartmentsToTask').val();

    LoadModalContent("#modal", "/Projects/Modal_AddTask?ApartmentID=" + id);
});

$(document).on('click', '#ProjectEventCancel', function () {
    Custombox.modal.close();
});

$(document).on('click', '#TaskEventCancel', function () {
    Custombox.modal.close();
});

$(document).on('click', '#FitterEventCancel', function () {
    Custombox.modal.close();
});

$(document).on('click', '#TaskEventCreate', function () {
    var taskId = $('.custombox-content #Tasks').val();
    var title = $('.custombox-content #TaskEventTitle').val();
    var start = $('.custombox-content #TaskEventStart').val();
    var end = $('.custombox-content #TaskEventEnd').val();
    var desc = $('.custombox-content #TaskEventDescription').val();
    var color = $('.custombox-content .radios .active input').val();
    var allDay = $('.custombox-content #TaskEventAllDay').is(':checked');

    $.ajax({
        url: '/Projects/AddEvent',
        dataType: 'json',
        type: 'POST',
        data: { Type: 0, ReferenceTo: taskId, Title: title, Start: start, End: end, Description: desc, Color: color, Allday: allDay },
        success: function (data) {
            if (data.success = 'true') {
                toastr.success("Wydarzenie zostało utworzone!");
            }
            else {
                toastr.error("Wystąpił błąd podczas tworzenia wydarzenia!");
            }

            Custombox.modal.close();
            ClearModal();
        },
        error: function (XMLHttpRequest) {
            toastr.error("Wystąpił błąd podczas tworzenia wydarzenia!");
            Custombox.modal.close();
            ClearModal();
        }
    });
    SaveCurrentData();

    $('#calendar').fullCalendar('destroy');
    CalendarInit();
});

$(document).on('click', '#ProjectEventCreate', function () {
    var projectId = $('.custombox-content #Project4Project').val();
    var title = $('.custombox-content #ProjectEventTitle').val();
    var start = $('.custombox-content #ProjectEventStart').val();
    var end = $('.custombox-content #ProjectEventEnd').val();
    var desc = $('.custombox-content #ProjectEventDescription').val();
    var color = $('.custombox-content .radios .active input').val();
    var allDay = $('.custombox-content #ProjectEventAllDay').is(':checked');

    $.ajax({
        url: '/Projects/AddEvent',
        dataType: 'json',
        type: 'POST',
        data: { Type: 3, ReferenceTo: projectId, Title: title, Start: start, End: end, Description: desc, Color: color, Allday: allDay },
        success: function (data) {
            if (data.success = 'true') {
                toastr.success("Wydarzenie zostało utworzone!");
            }
            else {
                toastr.error("Wystąpił błąd podczas tworzenia wydarzenia!");
            }

            Custombox.modal.close();
            ClearModal();
        },
        error: function (XMLHttpRequest) {
            toastr.error("Wystąpił błąd podczas tworzenia wydarzenia!");
            Custombox.modal.close();
            ClearModal();
        }
    });
    SaveCurrentData();

    $('#calendar').fullCalendar('destroy');
    CalendarInit();
});

$(document).on('click', '#FitterEventCreate', function () {
    var fitterID = $('.custombox-content #Fitter').val();
    var title = $('.custombox-content #FitterEventTitle').val();
    var start = $('.custombox-content #FitterEventStart').val();
    var end = $('.custombox-content #FitterEventEnd').val();
    var desc = $('.custombox-content #FitterEventDescription').val();
    var color = $('.custombox-content .radios .active input').val();
    var allDay = $('.custombox-content #FitterEventAllDay').is(':checked');

    $.ajax({
        url: '/Projects/AddEvent',
        dataType: 'json',
        type: 'POST',
        data: { Type: 1, ReferenceToUser: fitterID, Title: title, Start: start, End: end, Description: desc, Color: color, Allday: allDay },
        success: function (data) {
            if (data.success = 'true') {
                toastr.success("Wydarzenie zostało utworzone!");
            }
            else {
                toastr.error("Wystąpił błąd podczas tworzenia wydarzenia!");
            }

            Custombox.modal.close();
            ClearModal();
        },
        error: function (XMLHttpRequest) {
            toastr.error("Wystąpił błąd podczas tworzenia wydarzenia!");
            
            Custombox.modal.close();
            ClearModal();
        }
    });
    SaveCurrentData();

    $('#calendar').fullCalendar('destroy');
    CalendarInit();
});

$(document).on('click', '#start-search', function () {
    SaveCurrentData();

    $('#calendar').fullCalendar('destroy');
    CalendarInit();
})

$(document).on('click', '#print-calendar', function () {
    PrintDiv('#calendar');
});

$(document).on('change', '#Project', function () {
    var projectId = $(this).val();

    $('.custombox-content #Apartments').html('');
    $('.custombox-content #Tasks').html('');

    $.getJSON('/Home/ApartmentsList?projectId=' + projectId, function (data) {
        var first = true;
        var apartmentId = null;

        $.each(data, function (index, value) {
            if (first === true) {
                apartmentId = value.Id;
                first = false;
            }

            var option = '<option value="' + value.Id + '">' + value.NumberOfApartment + '</option>';

            $('.custombox-content #Apartments').append(option);
        });

        if (apartmentId != null) {
            $.getJSON('/Home/TasksList?apartmentId=' + apartmentId, function (data) {

                if (data.length == 0) $('.custombox-content #TaskEventCreate').prop('disabled', true);
                else $('.custombox-content #TaskEventCreate').prop('disabled', false);

                $.each(data, function (index, value) {
                    var option = '<option value="' + value.Id + '">' + value.ProductName;

                    if (value.Sufix != null)
                        option += ' ' + value.Sufix;

                    option += '</option>';
                    $('.custombox-content #Tasks').append(option);
                });
            });
        }
        else {
            $('#TaskEventCreate').prop('disabled', true);
        }
    });
});

$(document).on('change', '#Apartments', function () {
    var apartmentId = $(this).val();

    $('.custombox-content #Tasks').html('');
    
    $.getJSON('/Home/TasksList?apartmentId=' + apartmentId, function (data) {

        if (data.length == 0) $('.custombox-content #TaskEventCreate').prop('disabled', true);
        else $('.custombox-content #TaskEventCreate').prop('disabled', false);

        $.each(data, function (index, value) {
            var option = '<option value="' + value.Id + '">' + value.ProductName;

            if (value.Sufix != null)
                option += ' ' + value.Sufix;

            option += '</option>';

            $('.custombox-content #Tasks').append(option);
        });
    });
});

$(document).on('change', '#ProjectToTask', function () {
    var projectId = $(this).val();

    $('#ApartmentsToTask').html('');

    $.getJSON('/Home/ApartmentsList?projectId=' + projectId, function (data) {
        if (data.length == 0) $('#add-task').prop('disabled', true);
        else $('#add-task').prop('disabled', false);

        $.each(data, function (index, value) {
            var option = '<option value="' + value.Id + '">' + value.NumberOfApartment + '</option>'

            $('#ApartmentsToTask').append(option);
        });
    });
});

$(document).on('change', '#ProductId', function () {
    GetProduct($(this).val(), false);
});

$(document).on('change', '#EditProductId', function () {
    GetProduct($(this).val(), true);
});

$(document).on('dp.change', '#TaskEventStart', function () {
    var date = $(this).val();

    var newMoment = moment(date, 'YYYY-MM-DD HH:mm').add(4, 'hours').format('YYYY-MM-DD HH:mm');

    $('#TaskEventEnd').val(newMoment);
});

$(document).on('dp.change', '#FitterEventStart', function () {
    var date = $(this).val();

    var newMoment = moment(date, 'YYYY-MM-DD HH:mm').add(4, 'hours').format('YYYY-MM-DD HH:mm');

    $('#FitterEventEnd').val(newMoment);
});

$(document).on('dp.change', '.custombox-content #StartDate', function () {
    var date = $(this).val();

    var newMoment = moment(date, 'YYYY-MM-DD HH:mm').add(4, 'hours').format('YYYY-MM-DD HH:mm');

    $('.custombox-content #EndDate').val(newMoment);
});

$(document).on('click', '.delete-event', function () {
    var ID = $(this).data('event-id');

    var successFn = function (data) {
        if (data.result === "true")
        {
        SaveCurrentData();

        $('#calendar').fullCalendar('destroy');
        CalendarInit();
        }
        else {
            toastr.error("Wystąpił błąd podczas usuwania wydarzenia")
        }

        Custombox.modal.close();
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania wydarzenia");

        Custombox.modal.close();
    }

    GetAjaxWithFunctions("/Home/DeleteEvent", "POST", { ID: ID }, successFn, errorFn, "");
})

$(document).on("click", "#add-calendar", function () {
    NewModal("#add-calendar-modal");
})

$(document).on("click", ".edit-calendar", function () {
    var ID = $(this).data("calendar-id");

    $("#edit-calendar-modal #CalendarID").val(ID);

    NewModal("#edit-calendar-modal");
})

$(document).on("click", ".calendar-check", function () {
    if ($(this).hasClass("active"))
        $(this).removeClass("active");
    else
        $(this).addClass("active");

    SaveCurrentData();

    $('#calendar').fullCalendar('destroy');
    CalendarInit();
})

$(document).on("click", ".add-event-modal", function () {
    var modal = $(this).data("modal");

    var html = $(modal).html();
    $("#modal").html(html);

    NewModal(modal);
});

$(document).on("dp.change", ".autodate", function () {
    var date = $(this).val();

    var newMoment = moment(date, "YYYY-MM-DD HH:mm").add(4, "hours").format("YYYY-MM-DD HH:mm");

    $(".autodate-change").val(newMoment);
});

$(document).on("click", ".save-event", function () {
    var eventID = $(this).data("event-id");
    var title = $(".custombox-content #Title").val();
    var description = $(".custombox-content #Description").val();
    var start = $(".custombox-content #Start").val();
    var end = $(".custombox-content #End").val();
    var allDay = $(".custombox-content #AllDay").is(":checked");

    var data = {
        ID: eventID,
        Title: title,
        Description: description,
        Start: start,
        End: end,
        AllDay: allDay
    };

    var successFn = function (data) {
        if (data.success)
            toastr.success("Wydarzenie zostało zapisane !");
        else
            toastr.error("Wystąpił błąd podczas zapisywania wydarzenia !");


        SaveCurrentData();

        $('#calendar').fullCalendar('destroy');
        CalendarInit();
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas zapisywania wydarzenia !")
    }

    GetAjaxWithFunctions("/Home/AJAX_EditEvent", "POST", data, successFn, errorFn, ".custombox-content");
})

var draggedEventAllDay = false;
var draggedEventStartDate = "";


function CalendarInit() {
    var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();
    
    var myCurrentDateLocal = localStorage.getItem("calendar-date");
    var currentViewLocal = localStorage.getItem("calendar-view");

    if (myCurrentDateLocal != null)
        myCurrentDate = myCurrentDateLocal;

    if (currentViewLocal != null)
        currentView = currentViewLocal;

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },
        buttonText: {
            today: 'Dzisiaj',
            month: 'Miesiąc',
            week: 'Tydzień',
            day: 'Dzień',
            list: 'Lista'
        },
        events: {
            url: '/Home/Calendar',
            type: 'GET',
            data: {
                types: GetFilter(),
                calendars: GetCalendars(),
                search: $('#search').val()
            }
        },
        views: {
            listMonth: {
                listDayFormat: '[Tydzień] WW, DD MMMM'
            }
        },
        viewRender: function(view, element) {
            var currentView = view.name;

            if (currentView === 'listMonth')
                $('#print-calendar').show();
            else
                $('#print-calendar').hide();

            var startDate = new Date(view.intervalStart);

            var pad = "00";
            var year = startDate.getFullYear().toString();
            var month = (startDate.getMonth() + 1).toString();
            var day = startDate.getDate().toString();

            var dateString = year + "-" + pad.substring(0, pad.length - month.length) + month + "-" + pad.substring(0, pad.length - day.length) + day + " 07:00";
            
            $('.paste-date-begin').val(dateString);
            SaveCurrentData();
        },
        noEventMessage: "Brak wydarzeń w podanym okresie czasu",
        editable: true,
        dropable: true,
        weekNumbers: true,
        weekNumberCalculation: "local",
        locale: "pl",
        defaultView: currentView,
        defaultDate: myCurrentDate,
        monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwieceń', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
        monthNamesShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
        dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Niedziela'],
        dayNamesShort: ['Nd', 'Pon', 'Wt', 'Śr', 'Czw', 'Pt', 'Sb'],
        weekNumberTitle: 'T',
        eventLimit: true,
        eventLimitText: 'więcej...',
        allDayText: 'całodniowe',
        timeFormat: 'HH:mm',
        axisFormat: 'HH:mm',
        columnFormat: 'ddd D/M',
        eventClick: function (calEvent, jsEvent, view) {
            OpenEventDetails(calEvent.id);
        },
        eventDragStart: function (event, jsEvent, ui, view) {
            draggedEventAllDay = event.allDay;
            draggedEventStartDate = event.start.format();
        },
        eventDragStop: onExternalEventDrop,
        eventDrop: function (event, delta, revertFunc) {
            var start = event.start;
            var _start = event._start;
            var end = event.end;
            var type = event.id.split('-')[0];
            var id = event.id.split('-')[1];

            if (end === null) end = start;
            
            $.get('/Home/UpdateEvent?type=' + type + '&id=' + id + '&start=' + start.format() + '&end=' + end.format() + '&allDay=' + event.allDay + "&startString=" + draggedEventStartDate, function (data) {
                if (data.success === false)
                    revertFunc();
            });
        },
        eventResizeStart: function(event, jsEvent, ui, view){
            draggedEventStartDate = event.start.format();
        },
        eventResize: function (event, delta, revertFunc) {
            var start = event.start;
            var end = event.end;
            var type = event.id.split('-')[0];
            var id = event.id.split('-')[1];

            if (end === null) end = start;

            $.get('/Home/UpdateEvent?type=' + type + '&id=' + id + '&start=' + start.format() + '&end=' + end.format() + '&allDay=' + event.allDay + "&startString=" + draggedEventStartDate, function (data) {
                if (data.success === false)
                    revertFunc();
            });
        }
    })
}

function onExternalEventDrop(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
    if (!allDay && draggedEventAllDay) {
        revertFunc();
    }    
}

function SaveCurrentData() {

    var viewObject = $('#calendar').fullCalendar('getView');

    if (viewObject && viewObject.type)
        currentView = viewObject.type;

    if (currentView === 'listMonth')
        $('#print-calendar').show();
    else
       $('#print-calendar').hide();

    var moment = $('#calendar').fullCalendar('getDate');

    if (moment)
        myCurrentDate = moment.format();

    localStorage.setItem("calendar-date", moment.format());
    localStorage.setItem("calendar-view", currentView);
}

function OpenEventDetails(id) {
    var type = id.split('-')[0];
    var eventId = id.split('-')[1];
    
    var url = "";

    switch (type) {
        case 'task':
            url = "/Home/TaskEvent/" + eventId;  
            LoadModalContent("#modal", url);          
            break;
        case 'event4task':
            url = "/Home/EventForTask/" + eventId;
            LoadModalContent("#modal", url);
            break;
        case 'event4fitter':
            url = "/Home/EventForFitter/" + eventId;
            LoadModalContent("#modal", url);
            break;
        case 'event4project':
            url = "/Home/EventForProject/" + eventId;
            LoadModalContent("#modal", url);
            break;
        case 'project':
            location.assign("/Projects/Details/" + eventId);
            break;
        case 'group':
            location.assign("/Projects/Details/" + eventId);
            break;
        default:
            $.alert('Brak podanego typu wydarzenia');
    }
    
    //OpenModal('#modal', '.modal-dialog', url);
}

function OpenModal(element, content, url) {
    new Custombox.modal({
        content: {
            effect: 'fadein',
            target: element,
            animateFrom: 'top',
            animateTo: 'bottom',
            positionX: 'center',
            positionY: 'center',
            fullscreen: false
        },
        overlay: {
            active: 0.5
        },
        onComplete: LoadModalContent(content, url),
    }).open();
}

//function LoadModalContent(element, url) {
//    $.get(url, function (data) {
//                $(element).html(data);
//    });
//}

function ClearModal(element) {
    $(element).html("");
}

function GetFilter() {
        var isFirst = true;
        var filter = "";

        var checkbox1 = $('#fltr-chbx-1').prop('checked');
        var checkbox2 = $('#fltr-chbx-2').prop('checked');
        var checkbox3 = $('#fltr-chbx-3').prop('checked');
        var checkbox4 = $('#fltr-chbx-4').prop('checked');
        var checkbox5 = $('#fltr-chbx-5').prop('checked');

        if (checkbox1 === true) {
            filter += "0";
            isFirst = false;
        }

        if (checkbox2 === true) {
            if (isFirst != true)
                filter += ",";

            filter += 1;
            isFirst = false;
        }

        if (checkbox3 === true) {
            if (isFirst != true)
                filter += ",";

            filter += 2;
            isFirst = false;
        }

        if (checkbox4 === true) {
            if (isFirst != true)
                filter += ",";

            filter += 3;
            isFirst = false;
        }


        if (checkbox5 === true) {
            if (isFirst != true)
                filter += ",";

            filter += 4;
            isFirst = false;
        }

        return filter;
    
}

function GetCalendars() {
    var isFirst = true;
    var calendars = "";

    $(".calendar-check.active input").each(function () {
        if (isFirst === false)
            calendars += ",";

        calendars += $(this).val();

        isFirst = false;
    });

    return calendars;
}

function TaskEventInputClear() {
    $('#box-for-task-event input').val('');
    $('#TaskEventAllDay').iCheck('check');
}

function FitterEventInputClear() {
    $('#box-for-fitter-event input').val('');
    $('#FitterEventAllDay').iCheck('check');
}

function GetProduct(id) {
    ClearProductValues();

    var successFn = function (data) {
        $('#ProductPrice').val(data.Price);
        $('#PercentType1').val(data.Type1);
        $('#PercentType2').val(data.Type2);
        $('#PercentType3').val(data.Type3);
        $('#PercentType4').val(data.Type4);
        $('#PercentType5').val(data.Type5);
        $('#PercentType6').val(data.Type6);
        $('#product-jm').append(data.Jm);
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas aktualizacji danych o produkcie!!");
    }

    GetAjaxWithFunctions('/Products/GetProduct?id=' + id, 'GET', null, successFn, errorFn, '.product-load');
}

function ClearProductValues() {
    $('#ProductPrice').val('');
    $('#PercentType1').val('');
    $('#PercentType2').val('');
    $('#PercentType3').val('');
    $('#PercentType4').val('');
    $('#PercentType5').val('');
    $('#PercentType6').val('');
    $('#product-jm').html('');
}
