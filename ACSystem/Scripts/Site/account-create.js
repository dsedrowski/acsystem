﻿$(document).ready(function () {
    $("#profile-img").change(function () {
        var formData = new FormData();
        var totalFiles = document.getElementById("profile-img").files.length;
        for (var i = 0; i < totalFiles; i++) {
            var file = document.getElementById("profile-img").files[i];

            formData.append("profile-img", file);
        }
        $.ajax({
            type: "POST",
            url: '/Account/UploadImage',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.result === 'true') {
                    toastr.success("Zdjęcie zostało dodane!");
                    $('.profile-user-img').attr('src', '../../dist/Profile_Images/' + response.message);
                    $('#Image').val(response.message);
                }
                else {
                    toastr.error(response.message);
                }
            },
            error: function (error) {
                toastr.error(error);
            }
        });
    });

    $("#podpis-img").change(function () {
        var formData = new FormData();
        var totalFiles = document.getElementById("podpis-img").files.length;
        for (var i = 0; i < totalFiles; i++) {
            var file = document.getElementById("podpis-img").files[i];

            formData.append("podpis-img", file);
        }
        $.ajax({
            type: "POST",
            url: '/Account/UploadImagePodpis',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.result === 'true') {
                    toastr.success("Zdjęcie zostało dodane!");
                    $('.podpis-user-img').attr('src', '/Monter/dist/Podpisy/' + response.message);
                    $('#Podpis').val(response.message);
                }
                else {
                    toastr.error(response.message);
                }
            },
            error: function (error) {
                toastr.error(error);
            }
        });
    });

    $("#DateOfBirth").inputmask("yyyy-mm-dd", { "placeholder": "yyyy-mm-dd" });
    //$("#PhoneNumber").inputmask("999-999-999", { "placeholder": "___-___-___" });

    $('#Role').select2();


    if (typeof Huebee != "undefined" && $('.color-picker').length > 0) {
        var hueb = new Huebee('.color-picker', {
            setText: false
        });

        hueb.on('change', function (color, hue, sat, lum) {
            $('#ColorHex').val(color);
        })

        var color = $('.color-picker').data('color');

        if (color) hueb.setColor(color);
    }
});


$(document).on("click", "#add-calendar", function () {
    NewModal("#add-calendar-modal");
})

$(document).on("submit", "#add-calendar-form", function (e) {
    e.preventDefault();

    var successFn = function (data) {
        if (data.success === true) {
            toastr.success("Pomyślnie dodano nowy kalendarz");
            $("#CalendarID").append('<option value="' + data.id + '" style="background: ' + data.color + '; color: white; padding: 20px;">' + data.name + "</option>");
            Custombox.modal.close();
        }
        else {
            toastr.error("Wystąpił błąd podczas dodawania nowego kalendarza");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas dodawania nowego kalendarza");
    }

    GetAjaxWithFunctions("/Account/AddCalendar", "POST", $(this).serialize(), successFn, errorFn, "#CalendarID");
});