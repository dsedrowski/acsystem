﻿var selectedApartments = [];
var selectedTasks = [];

$(document).ready(function() {

    $(".operation-modal").click(function() {
        NewModal($(this).data("modal"));
    });

    $(".apartment-check").on("ifChanged", function () {
        var checked = $(this).parent('[class*="icheckbox_square-blue"]').hasClass("checked");

        SelectedApartments($(this).val(), checked);
    });

    $(".task-check").on("ifChanged", function () {
        var checked = $(this).parent('[class*="icheckbox_square-blue"]').hasClass("checked");

        SelectedTasks($(this).val(), checked);
    });

    $(".delete-selected-tasks").click(function() {
        DeleteTasks();
    });

    $(".delete-selected-apartments").click(function () {
        DeleteApartments();
    });

    $(".report-btn").click(function () {
        var type = $(this).data("type");
        var what = $(this).data("what");

        GenerateReport(type, what);
    });

    $("#search-reports-grouped-table-btn").click(function () {
        var lgh = $(this).data("apartment-id");
        var search = $("#search-reports-grouped-table-input").val();

        GetReportList(search, lgh);
    });

    $("#search-project-reports-grouped-table-btn").click(function () {
        var project = $(this).data("project-id");
        var search = $("#search-project-reports-grouped-table-input").val();

        GetProjectReportList(search, project);
    });

    $(".details-grouped-raport").click(function () {
        var reportID = $(this).data("report-id");

        ShowDetailsGroupedReport(reportID);
    });

    $(".send-grouped-raport").click(function () {
        var id = $(this).data("report-id");

        SendApartmentGroupReport(id);
    });

    $("#raport-service-with-code").click(function () {
        var projectID = $(this).data("project-id");

        $.LoadingOverlay('show');
        $.get("/Print/ServiceTasksWithCode/" + projectID, function (data) {
            if (data.success)
                PrintURLPDF("/dist/RaportPDF/" + data.pdfName);
            else
                toastr.error("Wystąpił błąd podczas drukowania raportu.");
        });
    });

    $("#raport-kitchen-zip").click(function () {
        var successFn = function (data) {
            if (data.success)
                window.location = "/dist/RaportPDF/" + data.file;
            else
                toastr.error("Wystąpił błąd podczas generowania paczki!");
        }

        var errorFn = function () {
            toastr.error("Wystąpił błąd podczas generowania paczki!");
        }

        var data = {
            Apartamenty: $(".apartments-array").val()
        };

        GetAjaxWithFunctions("/Projects/RaportyZip", "POST", data, successFn, errorFn, "");
    });

    $(".move-task").click(function () {
        $(".tasks-array").val($(this).data("task-id"));
        NewModal("#move-one-task-modal");
    });

    $("#search-apartment-input").keyup(function () {

        SearchApartment($(this).val(), $("#ProductId_search").val());
    });

    $("#ProductId_search").change(function () {
        SearchApartment($("#search-apartment-input").val(), $(this).val());
    });
});

$(document).on("change", ".move-one-task-project-id", function () {
    var id = $(this).val();
    $.get("/Home/ApartmentsList?projectId=" + id, null, function (data) {
        $(".custombox-content #MoveApartmentID").html("");
        $.each(data, function (index, value) {
            $(".custombox-content #MoveApartmentID").append('<option value="' + value.Id + '">' + value.NumberOfApartment + '</option>');
        });
    });
});

$(document).on("click", ".move-one-task-submit-button", function () {
    var fn = function () {
        $(".custombox-content form").submit();
    };

    Confirm("Czy na pewno chcesz przenieść zadanie?", fn);
});

$(document).on("click", ".move-multiple-tasks-submit-button", function () {
    var fn = function () {
        $(".custombox-content form").submit();
    };

    Confirm("Czy na pewno chcesz przenieść zadania?", fn);
});


function SelectedApartments(newVal, checked) {
    selectedApartments = [];

    if (!checked)
        selectedApartments.push(newVal);

    $(".apartment-check-div .icheckbox_square-blue.checked input").each(function () {
        var id = $(this).val();

        if ((checked && newVal === id) === false)
            selectedApartments.push($(this).val());
    });

    $(".apartments-array").val(selectedApartments);

    if (selectedApartments.length > 0) {
        $("#add-tasks-btn").removeClass("hidden");
        $("#add-tasks-service-kitchen-btn").removeClass("hidden");
        $("#add-tasks-service-construction-btn").removeClass("hidden");
        $("#move-apartments-btn").removeClass("hidden");
        $("#delete-apartments-btn").removeClass("hidden");
        $("#change-floor-btn").removeClass("hidden");
        $("#change-cage-btn").removeClass("hidden");
        $(".reports-btn").removeClass("hidden");
        $("#raport-service-with-code").removeClass("hidden");
        $("#raport-kitchen-zip").removeClass("hidden");
    }
    else {
        $("#add-tasks-btn").addClass("hidden");
        $("#add-tasks-service-kitchen-btn").addClass("hidden");
        $("#add-tasks-service-construction-btn").addClass("hidden");
        $("#move-apartments-btn").addClass("hidden");
        $("#delete-apartments-btn").addClass("hidden");
        $("#change-floor-btn").addClass("hidden");
        $("#change-cage-btn").addClass("hidden");
        $(".reports-btn").addClass("hidden");
        $("#raport-service-with-code").addClass("hidden");
        $("#raport-kitchen-zip").addClass("hidden");
    }
}

function SelectedTasks(newVal, checked) {
    selectedTasks = [];

    if (!checked)
        selectedTasks.push(newVal);

    $(".tasks-table .icheckbox_square-blue.checked input").each(function () {
        var id = $(this).val();

        if ((checked && newVal === id) === false)
            selectedTasks.push($(this).val());
    });

    $("#TasksArray").val(selectedTasks);
    $(".tasks-array").val(selectedTasks);

    if (selectedTasks.length > 0) {
        $("#edit-tasks-btn").removeClass("hidden");
        $("#delete-tasks-btn").removeClass("hidden");

        if (selectedTasks.length > 1) {
            $("#move-one-task-btn").removeClass("hidden").addClass("hidden");
            $("#move-multiple-tasks-btn").removeClass("hidden");
        }
        else {
            $("#move-one-task-btn").removeClass("hidden");
            $("#move-multiple-tasks-btn").removeClass("hidden").addClass("hidden");
        }
    } else {
        $("#edit-tasks-btn").addClass("hidden");
        $("#delete-tasks-btn").addClass("hidden");
        $("#move-one-task-btn").removeClass("hidden").addClass("hidden");
        $("#move-multiple-tasks-btn").removeClass("hidden").addClass("hidden");
    }
}

function DeleteTasks() {
    var successFn = function (data) {
        if (data.success === true) {
            location.reload();
        } else {
            toastr.error("Wystąpił błąd podczas usuwania zadań");
        }
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania zadań");
    };

    var fn = function () {
        GetAjaxWithFunctions("/Projects/DeleteTaskGroup", "POST", { tasks: selectedTasks }, successFn, errorFn, "");
    };

    Confirm("Czy na pewno chcesz usunąć zaznaczone zadania?", fn);
}

function DeleteApartments() {
    var successFn = function (data) {
        if (data.success === true) {
            location.reload();
        } else {
            toastr.error("Wystąpił błąd podczas usuwania mieszkań.");
        }
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania mieszkań.");
    };

    var fn = function () {
        GetAjaxWithFunctions("/Projects/DeleteApartmentGroup", "POST", { apartments: selectedApartments }, successFn, errorFn, "");
    };

    Confirm("Czy na pewno chcesz usunąć zaznaczone mieszkania?", fn);
}

function GetCheckedApartments() {
    selectedApartments = [];

    $(".apartment-check-div .icheckbox_square-blue.checked input").each(function () {
        var id = $(this).val();

        selectedApartments.push($(this).val());
    });

    return selectedApartments;
}

function GenerateReport(type, what) {
    var apartments = GetCheckedApartments();

    var data = {
        apartments: apartments,
        type: type,
        what: what
    };

    var successFn = function (data) {
        if (data.success === true) {
            toastr.success("Raport dla wybranych mieszkań został wygenerowany pomyślnie !");
        }
        else {
            toastr.error("Wystąpił błąd podczas generowania raportu !");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas generowania raportu !");
    }

    GetAjaxWithFunctions("/Projects/MakeGroupReport/", "POST", data, successFn, errorFn, "");
}

function GetReportList(search, apartmentID) {
    var data = {
        search: search,
        lgh: apartmentID
    };

    var successFn = function (data) {
        var rows = "";

        $.each(data.list, function (index, value) {
            var reportNumber = value.LGH + value.Number;
            var precurosorNumber = (value.Precursor > 0) ? value.LGH + value.Precursor : "";

            var row = "<tr>";
                row += "<td>" + reportNumber + "</td>";
                switch (value.Type) {
                    case 1:
                        row += "<td>Kuchnie</td>";
                        break;
                    case 2:
                        row += "<td>Budowlanka</td>";
                        break;
                    case 3:
                        row += "<td>Serwis</td>";
                        break;
                }
                row += "<td>" + value.Data + "</td>";
                row += "<td>" + value.ProjectInSystemID + "</td>";
                row += "<td>" + value.ProjectNumber + "</td>";
                row += "<td>";
                switch (value.Status) {
                    case 1:
                        row += '<span class="label label-default">Nie rozpoczęte</span>';
                        break;
                    case 0:
                        row += '<span class="label label-warning">W trakcie</span>';
                        break;
                    case 3:
                        row += '<span class="label label-success">Zakończone</span>';
                        break;
                    case 2:
                        row += '<span class="label label-danger">Zakończone z błędami</span>';
                        break;
                }
                row += "</td>";
                row += "<td>" + precurosorNumber + "</td>";
                row += "<td>";
                row += '<div class="crud-container">';
                row += '<span data-toggle="tooltip" data-placement="bottom" title="Szczegóły" data-report-id="' + value.ID + '" class="crud crud-details details-raport"><i class="glyphicon glyphicon-eye-open"></i></span>';
                row += '<span data-toggle="tooltip" data-placement="bottom" title="Drukuj" data-report-id="' + value.ID + '" class="crud crud-print print-raport"  onclick="PrintURL(\'/Print/ApartmentRaportGrouped?ID=' + value.ID + '&Print=true\')"><i class="glyphicon glyphicon-print"></i></span>';
                row += '<span data-toggle="tooltip" data-placement="bottom" title="Wyślij" data-report-id="' + value.ID + '" class="crud crud-mail send-raport"><i class="glyphicon glyphicon-envelope"></i></span>'
                row += "</div>";
                row += "</td>";
                row += "</tr>";


                rows += row;
        });

        $("table#reports-grouped-table tbody").html(rows);

        $('.table-pagination').paging({ limit: 16 });
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas ładowania treści.");
    }

    GetAjaxWithFunctions("/Projects/AJAX_GroupReportsList/", "POST", data, successFn, errorFn, "#reports-grouped-table");
}

function GetProjectReportList(search, projectID) {
    var data = {
        search: search,
        id: projectID
    };

    var successFn = function (data) {
        var rows = "";

        $.each(data.list, function (index, value) {
            var reportNumber = value.LGH + value.Number;
            var precurosorNumber = (value.Precursor > 0) ? value.LGH + value.Precursor : "";

            var row = "<tr>";
            row += "<td>" + reportNumber + "</td>";
            row += "<td>" + value.LGHNumber + "</td>";
            switch (value.Type) {
                case 1:
                    row += "<td>Kuchnie</td>";
                    break;
                case 2:
                    row += "<td>Budowlanka</td>";
                    break;
                case 3:
                    row += "<td>Serwis</td>";
                    break;
            }
            row += "<td>" + value.Data + "</td>";
            row += "<td>";
            switch (value.Status) {
                case 1:
                    row += '<span class="label label-default">Nie rozpoczęte</span>';
                    break;
                case 0:
                    row += '<span class="label label-warning">W trakcie</span>';
                    break;
                case 3:
                    row += '<span class="label label-success">Zakończone</span>';
                    break;
                case 2:
                    row += '<span class="label label-danger">Zakończone z błędami</span>';
                    break;
            }
            row += "</td>";
            row += "<td>" + precurosorNumber + "</td>";
            row += "<td>";
            row += '<div class="crud-container">';
            row += '<span data-toggle="tooltip" data-placement="bottom" title="Szczegóły" data-report-id="' + value.ID + '" class="crud crud-details details-raport"><i class="glyphicon glyphicon-eye-open"></i></span>';
            row += '<span data-toggle="tooltip" data-placement="bottom" title="Drukuj" data-report-id="' + value.ID + '" class="crud crud-print print-raport"  onclick="PrintURL(\'/Print/ApartmentRaportGrouped?ID=' + value.ID + '&Print=true\')"><i class="glyphicon glyphicon-print"></i></span>';
            row += '<span data-toggle="tooltip" data-placement="bottom" title="Wyślij" data-report-id="' + value.ID + '" class="crud crud-mail send-raport"><i class="glyphicon glyphicon-envelope"></i></span>'
            row += "</div>";
            row += "</td>";
            row += "</tr>";


            rows += row;
        });

        $("table#reports-grouped-table tbody").html(rows);

        $('.table-pagination').paging({ limit: 16 });
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas ładowania treści.");
    }

    GetAjaxWithFunctions("/Projects/AJAX_GroupProjectReportsList/", "POST", data, successFn, errorFn, "#reports-grouped-table");
}

function ShowDetailsGroupedReport(reportID) {
    LoadModalContent("#modal", "/Projects/Modal_ReportDetails/" + reportID);
}

function SendApartmentGroupReport(id) {
    var successFn = function (data) {
        if (data.success === true) {
            toastr.success("Pomyślnie wysłano raport");
            //location.reload();
        }
        else {
            toastr.error(data.message);
        }
    }

    var errorFn = function () {
        data.error("Wystąpił błąd podczas wysyłania raportu");
    }

    GetAjaxWithFunctions("/Projects/SendApartmentGroupRaport", "GET", { ID: id }, successFn, errorFn, "");
}

function GetReportTasksWithCode(id) {
    var successFn = function (data) {
        if (data.success) {
            window.location = '/dist/RaportPDF/' + data.file + '.pdf';
        }
        else {
            toastr.error("Wystąpił błąd podczas generowania pliku raportu!");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas generowania pliku raportu!");
    }

    var data = {
        ProjectID: id
    }

    GetAjaxWithFunctions("/Projects/GetReport_ServiceTasksWithCode", "POST", data, successFn, errorFn, "");
}

function SearchApartment(searchText, productId) {
    if (searchText)
        searchText = searchText.toLowerCase();

    if ($('.search-apartment').length > 0) {
        $(".search-apartment").each(function () {
            try {
                var data = ($(this).data("lgh") + "").toLowerCase();
                var products = ($(this).data("products") + "").toLowerCase().split(',');

                if (data.indexOf(searchText) >= 0 && (products.includes(productId) || productId === "-1")) {
                    $(this).removeClass("tr-hidden").removeClass("tr-visible").addClass("tr-visible");
                }
                else {
                    $(this).removeClass("tr-hidden").removeClass("tr-visible").addClass("tr-hidden");
                }
            }
            catch (error) {
                console.log("Błąd: " + $(this).data("lgh"));
                console.error(error);
            }
        });
    }

    if ($('.tasks-in-apartment-table .group-hide tr, .grouped-table .labels').length > 0) {
        $(".tasks-in-apartment-table .group-hide tr, .grouped-table .labels").each(function () {
            if ($(this).hasClass('pr-' + productId)) {
                $(this).removeClass("tr-hidden").removeClass("tr-visible").addClass("tr-visible");
                $('[data-toggle="tooltip"]:visible').removeAttr('data-toggle').attr('data-toggle', 'tooltip');
            }
            else {
                $(this).removeClass("tr-hidden").removeClass("tr-visible").addClass("tr-hidden");
                $('[data-toggle="tooltip"]:hidden').removeAttr('data-toggle');
            }
        });
    }

    if (productId == -1) {
        $('.tasks-in-apartment-table .group-hide tr, .grouped-table .labels').removeClass("tr-hidden").removeClass("tr-visible").addClass("tr-visible");
    }

    if ($('.cage-row').length > 0) {
        $(".cage-row").each(function () {
            var data = $(this).data("cage");

            var count = $('[data-apartment-cage="' + data + '"].tr-visible').length;
            var dostepne = $('[data-cage-dostepne="' + data + '"].tr-visible').length;
            var sukces = $('[data-cage-sukces="' + data + '"].tr-visible').length;
            var wtrakcie = $('[data-cage-wtrakcie="' + data + '"].tr-visible').length;
            var zatrzymane = $('[data-cage-zatrzymane="' + data + '"].tr-visible').length;
            var blad = $('[data-cage-blad="' + data + '"].tr-visible').length;
            var other = $('[data-cage-other="' + data + '"].tr-visible').length;

            var timeNeeded = 0;
            var timeLeft = 0;

            $('[data-task-cage="' + data + '"].task-row.tr-visible').each(function () {
                timeNeeded += Number($(this).data("time-needed"));
                timeLeft += Number($(this).data("time-left"));
            });

            $(this).find(".cage-count").html(count);
            $(this).find(".cage-time").html(`Czas: ${timeLeft.toFixed(2)} / ${timeNeeded.toFixed(2)} h`);
            $(this).find(".label-default").html(dostepne);
            $(this).find(".label-success").html(sukces);
            $(this).find(".label-warning").html(wtrakcie);
            $(this).find(".label-info").html(zatrzymane);
            $(this).find(".label-danger").html(blad);
            $(this).find(".label-primary").html(other);
        });
    }

    if ($('.floor-row').length > 0) {
        $(".floor-row").each(function () {
            var data = $(this).data("floor");

            var count = $('[data-apartment-floor="' + data + '"].tr-visible').length;
            var dostepne = $('[data-floor-dostepne="' + data + '"].tr-visible').length;
            var sukces = $('[data-floor-sukces="' + data + '"].tr-visible').length;
            var wtrakcie = $('[data-floor-wtrakcie="' + data + '"].tr-visible').length;
            var zatrzymane = $('[data-floor-zatrzymane="' + data + '"].tr-visible').length;
            var blad = $('[data-floor-blad="' + data + '"].tr-visible').length;
            var other = $('[data-floor-other="' + data + '"].tr-visible').length;

            var timeNeeded = 0;
            var timeLeft = 0;

            $('[data-task-floor="' + data + '"].task-row.tr-visible').each(function () {
                timeNeeded += Number($(this).data("time-needed"));
                timeLeft += Number($(this).data("time-left"));
            })

            $(this).find(".floor-count").html(count);
            $(this).find(".floor-time").html(`Czas: ${timeLeft.toFixed(2)} / ${timeNeeded.toFixed(2)} h`);
            $(this).find(".label-default").html(dostepne);
            $(this).find(".label-success").html(sukces);
            $(this).find(".label-warning").html(wtrakcie);
            $(this).find(".label-info").html(zatrzymane);
            $(this).find(".label-danger").html(blad);
            $(this).find(".label-primary").html(other);
        });
    }

    if ($('.apartment-row').length > 0) {
        $('.apartment-row').each(function () {
            var data = $(this).data('lgh');

            var timeNeeded = 0;
            var timeLeft = 0;

            $('[data-lgh="' + data + '"].task-row.tr-visible').each(function () {
                console.log($(this).data("lgh"), $(this).data("time-needed"), $(this).data("time-left"))
                timeNeeded += Number($(this).data("time-needed"));
                timeLeft += Number($(this).data("time-left"));
            });

            $(this).find('.lgh-time').html(`Czas: ${timeLeft.toFixed(2)} / ${timeNeeded.toFixed(2)} h`);
        })
    }
}