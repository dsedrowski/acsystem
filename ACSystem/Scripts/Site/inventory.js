﻿var tableId = '#inventory-list';
GetTableFilterWebStorage("inventory", tableId);
GetTableSearchWebStorage();
var table = LoadTable(tableId);

var statusFilter = $('.status-filter');

$(document).ready(function () {
    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue'
    });

    //$('input[type="checkbox"].minimal').iCheck('check');

    statusFilter.on('ifChanged', function () {
        ChangeFilter();
        DefaultPropTable();
        LoadTable(tableId);
    });
});

$(document).on('click', '.inventory-details', function () {
    location.assign('/Inventory/Details/' + $(this).data('inventory-id'));
});

$(document).on('click', '.inventory-delete', function () {
    var id = $(this).data('inventory-id');

    var fn = function () {
        $.ajax({
            url: '/Inventory/Delete',
            dataType: 'json',
            type: 'POST',
            data: { id: id },
            success: function (data) {
                if (data.success = 'true') {
                    location.reload();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            }
        });
    }

    Confirm("Czy na pewno chcesz usunąć narzędzie?", fn);
});

$(document).on('click', '.pagination a', function () {
    var page = $(this).data('page');

    if (page != null) {
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .prevBtn', function () {
    var page = $(tableId).data('page');;

    if (page != null) {
        page -= 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .nextBtn', function () {
    var page = $(tableId).data('page');;

    if (page != null) {
        page += 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '#search-table-btn', function () {
    DefaultPropTable();
    LoadTable(tableId);
});

$("#search-table-input").on("keyup", function (event) {
    if (event.keyCode == 13) {
        $(table).html("");
        DefaultPropTable();
        LoadTable(tableId);
    }
});

$(document).on('click', tableId + ' th.clickable', function () {
    DefaultPropTable();
    $(tableId).data('order-by', $(this).data('id'))

    var currentDest = $(this).data('dest');

    $(tableId + ' th.clickable').data('dest', 'desc');

    if (currentDest === 'asc')
        $(this).data('dest', 'desc');
    if (currentDest === 'desc') $(this).data('dest', 'asc');

    $(tableId).data('dest', $(this).data('dest'));

    LoadTable(tableId);
});

$(document).on('click', '.close-modal', function () {
    Custombox.close();
});

$(document).on('click', '.give-inventory', function () {
    var inventoryID = $(this).data('inventory-id');

    LoadModalContent("#modal", "/Inventory/Modal_GiveInventory?InventoryID=" + inventoryID);
});

$(document).on('click', '.free-inventory', function () {
    var inventoryID = $(this).data('inventory-id');

    LoadModalContent("#modal", "/Inventory/Modal_FreeInventory?InventoryID=" + inventoryID);
});

$(document).on('click', '.sell-inventory', function () {
    var inventoryID = $(this).data('inventory-id');

    LoadModalContent("#modal", "/Inventory/Modal_SellInventory?InventoryID=" + inventoryID);
});

$(document).on("click", ".check-condition", function () {
    var id = $(this).data("inventory-id");

    id = (id > 0) ? id : "";

    var popup = window.open("/Popup/CheckCondition/" + id, "Popup", "width=600,height=1000");
    popup.focus();
});

$(document).on("keypress", ".checkEnterClick", function (event) {
    if (event.which === 13 || event.keyCode === 13) {
        DefaultPropTable();
        LoadTable(tableId);
    }
});

function LoadTable(table) {
    var page = $(table).data('page');
    var dest = $(table).data('dest');
    var orderBy = $(table).data('order-by');
    var filter = $(table).data('filter');
    var search = $('#search-table-input').val();
    var numer_ew_s = $("#search_numer_ew_s").val();
    var numer_ew_t = $("#search_numer_ew_t").val();
    var typ = $("#search_typ").val();
    var marka = $("#search_marka").val();
    var data_search = $("#search_data").val();
    var magazyn = $("#search_magazyn").val();
    var posiada = $("#search_posiada").val();
    var akcesoria = $("#search_accessories_no").val();

    SetTableFilterWebStorage("inventory", page, dest, orderBy, filter, search);
    SetTableSearchWebStorage();
    CreateSortingIcon(dest, orderBy);

    var data = { page: page, dest: dest, orderBy: orderBy, search: search, filter: filter, numer_ew_s, numer_ew_t, typ, marka, data: data_search, magazyn, posiada, akcesoria };

    var successFn = function (data) {
        FillTable(data.data, table);
        GeneratePagination(data.pages, table);
    };

    var errorFn = function (XMLHttpRequest) {
        $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
    }

    GetAjaxWithFunctions("/Inventory/InventoryList", "GET", data, successFn, errorFn, tableId);
}

function FillTable(data, table) {
    var row = "";

    if (data != null) {
        $.each(data, function (index, value) {
            var trBg = '';

            if (value.IsVitalityEnd)
                trBg = ' style="background-color: #ff7675;"';
            else if (value.IsVitalityComeToEnd)
                trBg = ' style="background-color: #fdcb6e;"';
            else if (value.ReviewIsComing)
                trBg = ' style="background-color: #74b9ff;"';

            row += '<tr class="tr-clickable" ' + trBg + '>' +
            '<td class="inventory-details" data-inventory-id="' + value.Id + '">' + value.NumberOfMachine + '</td>' +
            '<td class="inventory-details" data-inventory-id="' + value.Id + '">' + value.NumberOfMachine2 + '</td>' +
            '<td class="inventory-details" data-inventory-id="' + value.Id + '">' + value.Type + '</td>' +
            '<td class="inventory-details" data-inventory-id="' + value.Id + '">' + value.Mark + '</td>' +
            '<td class="inventory-details" data-inventory-id="' + value.Id + '">' + value.BuyDateString + '</td>' +
            '<td class="inventory-details with-label" data-inventory-id="' + value.Id + '">' + CreateLabel(value.Status) + '</td>' +
            '<td class="inventory-details with-label" data-inventory-id="' + value.Id + '">' + value.Warehouse + '</td>' +
            '<td class="inventory-details with-label" data-inventory-id="' + value.Id + '">' + value.Owner + '</td>' +
            '<td>' + GetCrudHtml(value.Id, value.Status, value.IsPrivate) + '</td>' +
            '</tr>';
        });
    }

    $(table + ' tbody').html(row);
}

function DefaultPropTable() {
    $(tableId).data('page', 1);
    $(tableId).data('dest', 'asc');
    $(tableId).data('order-by', 'numberOfMachine');
}

function GetCrudHtml(id, status, isPrivate) {
    var crud = '<div class="crud-container">' +
                    '<a class="crud crud-details" href="/Inventory/Details/' + id + '" data-toggle="tooltip", data-placement="bottom" title="Szczegóły"><i class="glyphicon glyphicon-eye-open"></i></a>';

    if (!isPrivate) {
        if (status === 1)
            crud += '<span data-toggle="tooltip" data-placement="bottom" title="Zwolnij" data-inventory-id="' + id + '" class="crud crud-delete-fitter free-inventory"><i class="fa fa-user-times"></i></span>';

        if (status === 0)
            crud += '<span data-toggle="tooltip" data-placement="bottom" title="Przypisz montera" data-inventory-id="' + id + '" class="crud crud-add-fitter give-inventory"><i class="fa fa-user-plus"></i></span>';
    }

    if (status != 2)
        crud += '<span data-toggle="tooltip" data-placement="bottom" title="Wykonaj przegląd" data-inventory-id="' + id + '" class="crud crud-add-fitter check-condition"><i class="fa fa-cogs"></i></span>';

    if (status != 3 && status != 1)
        crud += '<span data-toggle="tooltip" data-placement="bottom" title="Sprzedaj" data-inventory-id="' + id + '" class="crud crud-add-fitter sell-inventory"><i class="fa fa-dollar"></i></span>';

    crud +=     '<a class="crud crud-edit" href="/Inventory/Edit/' + id + '" data-toggle="tooltip", data-placement="bottom" title="Edytuj"><i class="glyphicon glyphicon-pencil"></i></a>' +
                '<span class="crud crud-delete inventory-delete" data-inventory-id="' + id + '" data-toggle="tooltip", data-placement="bottom" title="Usuń"><i class="glyphicon glyphicon-remove"></i></span>' +
            '</div>';

    return crud;
}

function GeneratePagination(pages, table) {
    var currentPage = $(table).data('page');

    var prevBtn = '<li><a href="#" class="prevBtn">«</a></li>';
    var nextBtn = '<li><a href="#" class="nextBtn">»</a></li>'

    var pagination = '';

    if (pages > 1 && currentPage > 1)
        pagination += prevBtn;

    for (var i = 1; i <= pages; i++) {
        if (i === currentPage)
            pagination += '<li class="active"><a href="#">' + i + '</a></li>';
        else
            pagination += '<li><a href="#" data-page="' + i + '">' + i + '</a></li>';
    }

    if (pages > 1 && currentPage < pages)
        pagination += nextBtn;

    $('.pagination').html(pagination);
}

function CreateSortingIcon(dest, orderBy) {
    var iconAsc = '<i class="glyphicon glyphicon-sort-by-alphabet sort-column-icon"></i>';
    var iconDesc = '<i class="glyphicon glyphicon-sort-by-alphabet-alt sort-column-icon"></i>'

    $(tableId + ' thead tr th i').remove();

    if (orderBy === 'numberOfMachine' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconAsc);
    else if (orderBy === 'numberOfMachine' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconDesc);
    if (orderBy === 'numberOfMachine2' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconAsc);
    else if (orderBy === 'numberOfMachine2' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconDesc);
    else if (orderBy === 'type' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconAsc);
    else if (orderBy === 'type' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconDesc);
    else if (orderBy === 'mark' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(4)').append(iconAsc);
    else if (orderBy === 'mark' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(4)').append(iconDesc);
    else if (orderBy === 'buydate' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(5)').append(iconAsc);
    else if (orderBy === 'buydate' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(5)').append(iconDesc);
    else if (orderBy === 'status' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(6)').append(iconAsc);
    else if (orderBy === 'status' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(6)').append(iconDesc);
    else if (orderBy === 'magazyn' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(7)').append(iconAsc);
    else if (orderBy === 'magazyn' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(7)').append(iconDesc);
    else if (orderBy === 'owner' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(8)').append(iconAsc);
    else if (orderBy === 'owner' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(8)').append(iconDesc);
}

function CreateLabel(status) {
    if (status === 1)
        return '<span class="label label-warning">Zajęte</span>';

    if (status === 0)
        return '<span class="label label-success">Dostępne</span>';

    if (status === 2)
        return '<span class="label label-danger">Skasowane</span>';

    if (status === 3)
        return '<span class="label label-default">Sprzedane</span>';
}

function ChangeFilter() {
    var isFirst = true;
    var filter = "";

    var checkbox1 = $('#fltr-chbx-1').prop('checked');
    var checkbox2 = $('#fltr-chbx-2').prop('checked');
    var checkbox3 = $('#fltr-chbx-3').prop('checked');
    var checkbox4 = $('#fltr-chbx-4').prop('checked');

    if (checkbox1 === true) {
        filter += "0";
        isFirst = false;
    }

    if (checkbox2 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 1;
        isFirst = false;
    }

    if (checkbox3 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 2;
        isFirst = false;
    }

    if (checkbox4 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 3;
        isFirst = false;
    }

    $(tableId).data('filter', filter);
}

function SetTableSearchWebStorage() {
    var numer_ew_s = $("#search_numer_ew_s").val();
    var numer_ew_t = $("#search_numer_ew_t").val();
    var typ = $("#search_typ").val();
    var marka = $("#search_marka").val();
    var data_search = $("#search_data").val();
    var magazyn = $("#search_magazyn").val();
    var posiada = $("#search_posiada").val();
    var akcesoria = $("#search_accessories_no").val();

    localStorage.setItem("inventory_search_numer_ew_s", numer_ew_s);
    localStorage.setItem("inventory_search_numer_ew_t", numer_ew_t);
    localStorage.setItem("inventory_search_typ", typ);
    localStorage.setItem("inventory_search_marka", marka);
    localStorage.setItem("inventory_search_data", data_search);
    localStorage.setItem("inventory_search_magazyn", magazyn);
    localStorage.setItem("inventory_search_posiada", posiada);
    localStorage.setItem("inventory_search_akcesoria", akcesoria);
}

function GetTableSearchWebStorage() {
    var numer_ew_s = localStorage.getItem("inventory_search_numer_ew_s");
    var numer_ew_t = localStorage.getItem("inventory_search_numer_ew_t");
    var typ = localStorage.getItem("inventory_search_typ");
    var marka = localStorage.getItem("inventory_search_marka");
    var data_search = localStorage.getItem("inventory_search_data");
    var magazyn = localStorage.getItem("inventory_search_magazyn");
    var posiada = localStorage.getItem("inventory_search_posiada");
    var akcesoria = localStorage.getItem("inventory_search_akcesoria");

    $("#search_numer_ew_s").val(numer_ew_s);
    $("#search_numer_ew_t").val(numer_ew_t);
    $("#search_typ").val(typ);
    $("#search_marka").val(marka);
    $("#search_data").val(data_search);
    $("#search_magazyn").val(magazyn);
    $("#search_posiada").val(posiada);
    $("#search_accessories_no").val(akcesoria);
}