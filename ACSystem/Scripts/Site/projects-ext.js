﻿var customerId = $("#CustomerId");
var projectManagerId = $("#ProjectManagers");
var street = $("#Street");
var city = $("#City");
var product = $("#ProductId");



$(document).ready(function () {
    if (customerId.val() === null) {
        projectManagerId.html("");
    }

    if (street.val() !== "" && city.val() !== "")
        GenerateMap();

    DefineICheck();



    street.on("change", function () {
        if (street.val() !== "" && city.val() !== "")
            GenerateMap();
    });

    city.on("change", function () {
        if (street.val() !== "" && city.val() !== "")
            GenerateMap();
    });

    customerId.on("change", function () {
        GetProjectManagers(customerId.val());
    });

    product.on("change", function () {
        GetProduct($(this).val());
    });

    if (typeof Huebee !== "undefined" && $(".color-picker").length > 0) {
        var hueb = new Huebee(".color-picker", {
            setText: false
        });

        hueb.on("change", function (color, hue, sat, lum) {
            $("#ColorHex").val(color);
        });

        var color = $(".color-picker").data("color");

        if (color) hueb.setColor(color);
    }

    $(".dd-customer-name").on("click", function () {
        location.assign("/Customers/Details/" + $(this).data("customer-id"));
    });

    $(".dd-project-manager-name").on("click", function () {
        LoadProjectManagerData($(this).data("project-manager-id"));

        Custombox.open({
            target: "#modal-project-manager",
            effect: "slide",
            position: ["center", "center"],
            escKey: true,
        });
    });

    $(".edit-status").val("stoped");

    $('input[type="checkbox"].minimal.apartment-check').on('ifChanged', function () {
        var apartmentID = $(this).val();

        var checked = $(this).parent('[class*="icheckbox"]').hasClass("checked");

        $(".apartment-" + apartmentID).each(function () {
            if (!checked && $(this).closest('tr').hasClass('tr-visible'))
                $(this).iCheck('check');
            else
                $(this).iCheck('uncheck');
        })

        //if (!checked)
        //    $(".apartment-" + apartmentID).iCheck("check");
        //else
        //    $(".apartment-" + apartmentID).iCheck("uncheck");

    });


    $('input[type="checkbox"].minimal.cage-check').on('ifChanged', function () {
        var id = $(this).val();

        var checked = $(this).parent('[class*="icheckbox"]').hasClass("checked");

        if (!checked)
            $("." + id).iCheck("check");
        else
            $("." + id).iCheck("uncheck");

    });

    $('input[type="checkbox"].minimal.floor-check').on('ifChanged', function () {
        var id = $(this).val();

        var checked = $(this).parent('[class*="icheckbox"]').hasClass("checked");

        if (!checked)
            $("." + id).iCheck("check");
        else
            $("." + id).iCheck("uncheck");

    });

    $('input[type="checkbox"].minimal.check-all-apartments').on('ifChanged', function () {
        var checked = $(this).parent('[class*="icheckbox"]').hasClass("checked");

        if (!checked) {
            $(".cage-check").iCheck("check");
            $(".floor-check").iCheck("check");
            $(".apartment-check").iCheck("check");
        }
        else {
            $(".cage-check").iCheck("uncheck");
            $(".floor-check").iCheck("uncheck");
            $(".apartment-check").iCheck("uncheck");
        }

    });


    $('input[type="checkbox"].minimal#project-product-check-all').on('ifChanged', function () {
        if ($(this).is(":checked")) {
            $('.project-product-check').iCheck("check");
        }
        else {
            $('.project-product-check').iCheck("uncheck");
        }
    });

    $(".dont-show").click(function () {
        var ID = $(this).data("task-id");

        var data = { ID };

        var successFn = function () {
            $(".tr-diff-task-" + ID).remove();
            toastr.success("Pomyślnie oznaczono zadanie");
        }
        var errorFn = function () {
            toastr.error("Wystąpił błąd podczas oznaczania zadania !");
        }

        GetAjaxWithFunctions("/Parallel/DontShowInCheckTaskPriceNorm", "POST", data, successFn, errorFn, "");
    });

    $(".dont-show-payroll").click(function () {
        var ID = $(this).data("payroll-item-id");

        var data = { ID };

        var successFn = function () {
            $(".tr-diff-payroll-" + ID).remove();
            toastr.success("Pomyślnie oznaczono element listy płac");
        }
        var errorFn = function () {
            toastr.error("Wystąpił błąd podczas oznaczania elementu listy płac !");
        }

        GetAjaxWithFunctions("/Parallel/DontShowInCheckPayrollRateNorm", "POST", data, successFn, errorFn, "");
    });

    $(".generate-fee").click(function () {
        var ID = $(this).data("payroll-item-id");
        var Diff = $(this).data("diff");

        var data = { ID, Diff };

        var successFn = function () {
            $(".tr-diff-payroll-" + ID).remove();
            toastr.success("Pomyślnie wygenerowano szkodę dla elementu listy płac");
        }
        var errorFn = function () {
            toastr.error("Wystąpił błąd podczas generowania szkody dla elementu listy płac !");
        }

        GetAjaxWithFunctions("/Parallel/GenerateFeeFromCheckPayrollRateNorm", "POST", data, successFn, errorFn, "");
    });

    var dropzone = $(".file-dropzone.for-project").dropzone({
        sending: function (file, xhr, formData) {
            var projectID = $(".file-dropzone.for-project").data("project-id");
            // Will send the filesize along with the file as POST data.
            formData.append("ProjectID", projectID);
        },
        success: function (first, response) {
            if (response.result === "true") {
                toastr.success("Dokument został dodany!");

                var docRow = "<tr>";
                docRow += "<td>" + response.fileName + "</td>";
                docRow += '<td class="text-center"><a href="/Monter/dist/Documents/' + response.newFileName + '" class="btn btn-sm btn-primary text-center">Pobierz</a></td>';
                docRow += "</tr>";

                $(".project-documents-table tbody").append(docRow);
            }
        },
        successMultiple: function (first, response) {
            if (response.result === "true") {
                toastr.success("Dokument został dodany!");

                var docRow = "<tr>";
                docRow += "<td>" + response.fileName + "</td>";
                docRow += '<td class="text-center"><a href="/Monter/dist/Documents/' + response.newFileName + '" class="btn btn-sm btn-primary text-center">Pobierz</a></td>';
                docRow += "</tr>";

                $(".project-documents-table tbody").append(docRow);
            }
        }
    });
    ActivityWorkDoneEdit();

});

$(document).on('click', '.project-details', function () {
    location.assign('/Projects/Details/' + $(this).data('project-id'));
});

$(document).on('click', '.project-edit', function () {
    location.assign('/Projects/Edit/' + $(this).data('project-id'));
});

$(document).on('click', '.project-delete', function () {
    var id = $(this).data('project-id');

    var fn = function () {
        $.ajax({
            url: '/Projects/Delete',
            dataType: 'json',
            type: 'POST',
            data: { id: id },
            success: function (data) {
                if (data.success === 'true') {
                    location.reload();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            }
        });
    };

    Confirm("Czy na pewno chcesz usunąć projekt?", fn);
});

$(document).on('click', '.copy-project', function () {
    var projectID = $(this).data('project-id');

    LoadModalContent("#modal", "/Projects/Modal_CopyProject?ProjectID=" + projectID);
});

$(document).on("click", ".dd-map", function () {
    //NewModal("#modal-map");
    //var address = street.val() + ", " + city.val();
    //var link = "https://www.google.com/maps/dir/?api=1&origin=VALLDA, Szwecja&destination=" + address;

    //var win = window.open(link, '_blank');
    //win.focus();
});

$(document).on("click", ".apartment-details", function () {
    location.assign("/Projects/ApartmentDetails/" + $(this).data("apartment-id"));
});

$(document).on("click", ".close-modal", function () {
    Custombox.modal.close();
    ClearModal("#modal");
});

$(document).on("click", ".add-apartment", function () {
    var ProjectID = $(this).data("project-id");
    LoadModalContent("#modal", "/Projects/Modal_AddApartment?ProjectID=" + ProjectID);
});

$(document).on("click", ".edit-apartment", function () {
    var id = $(this).data("apartment-id");
    var projectID = $(this).data("project-id");

    LoadModalContent("#modal", "/Projects/Modal_EditApartment/"+id);
});

$(document).on("click", ".delete-apartment", function () {
    var id = $(this).data("apartment-id");
    var projectId = $(this).data("project-id");

    var fn = function () {
        $.ajax({
            url: "/Projects/ApartmentDelete",
            dataType: "json",
            type: "GET",
            data: { ProjectId: projectId, ApartmentId: id },
            success: function (data) {
                if (data.success === "true") {
                    location.reload();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert("Błąd " + XMLHttpRequest.status + ". Proszę spróbować jeszcze raz.");
            }
        });
    }

    Confirm("Czy na pewno chcesz usunąć mieszkanie?", fn);
});

$(document).on("click", ".copy-apartment", function () {
    var id = $(this).data("apartment-id");
    var projectID = $(this).data("project-id");

    LoadModalContent("#modal", "/Projects/Modal_CopyApartment?ProjectID=" + projectID + "&ApartmentIDToCopy=" + id);
});

$(document).on("click", ".add-task", function () {
    var id = $(this).data("apartment-id");

    LoadModalContent("#modal", "/Projects/Modal_AddTask?ApartmentID=" + id, true);

});

$(document).on("click", ".add-kitchen-service-task", function () {
    var id = $(this).data("apartment-id");

    LoadModalContent("#modal", "/Projects/Modal_AddKitchenServiceTask/" + id, true);

});

$(document).on("click", ".add-construction-service-task", function () {
    var id = $(this).data("apartment-id");

    LoadModalContent("#modal", "/Projects/Modal_AddConstructionServiceTask/" + id, true);

});

$(document).on("click", ".handy-finish-task", function () {
    var id = $(this).data("task-id");
    var backTo = $(this).data("back");

    LoadModalContent("#modal", "/Projects/Modal_HandyFinishTask?ID=" + id + "&backTo=" + backTo);
});

$(document).on("click", ".edit-task", function () {
    var id = $(this).data("task-id");
    var type = $(this).data("task-type");

    if (type === "Developer")
        LoadModalContent("#modal", "/Projects/Modal_EditTask/" + id);
    else
        LoadModalContent("#modal", "/Projects/Modal_EditTaskSerwis/" + id);
});

$(document).on("click", ".details-task", function () {
    var id = $(this).data("task-id");

    LoadModalContent("#modal", "/Projects/Modal_DetailsTask/" + id);
});

$(document).on("click", ".delete-task", function () {
    var id = $(this).data("task-id");

    var fn = function () {
        $.ajax({
            url: "/Projects/TaskDelete",
            dataType: "json",
            type: "GET",
            data: { id: id },
            success: function (data) {
                if (data.success === "true") {
                    location.reload();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert("Błąd " + XMLHttpRequest.status + ". Proszę spróbować jeszcze raz.");
            }
        });
    }

    Confirm("Czy na pewno chcesz usunąć zadanie?", fn);
});

$(document).on("click", ".serwis-task", function () {
    var id = $(this).data("task-id");
    var backTo = $(this).data("back");

    LoadModalContent("#modal", "/Projects/Modal_AddTaskSerwis?id=" + id + "&backTo=" + backTo);
});

$(document).on("click", ".add-fitter-task", function () {
    var taskID = $(this).data("task-id");
    var ApartmentID = $(this).data("apartment-id");

    LoadModalContent("#modal", "/Projects/Modal_AddFitterToTask?ApartmentID=" + ApartmentID + "&TaskID=" + taskID);
});

$(document).on("click", ".plan-fitter-task", function () {
    var taskID = $(this).data("task-id");
    var ApartmentID = $(this).data("apartment-id");

    LoadModalContent("#modal", "/Projects/Modal_PlanFitterToTask?ApartmentID=" + ApartmentID + "&TaskID=" + taskID);
});

$(document).on("click", ".access-grant-task", function () {
    var taskID = $(this).data("task-id");
    var ApartmentID = $(this).data("apartment-id");

    LoadModalContent("#modal", "/Projects/Modal_GrantAccessToTask?ApartmentID=" + ApartmentID + "&TaskID=" + taskID);
});

$(document).on("click", ".delete-grant-fitter", function() {
    var fitterId = $(this).data("fitter-id");
    var taskId = $(this).data("task-id");

    var successFn = function(data) {
        if (data.success === true) {
            $(".grant-fitter-" + fitterId).remove();
        } else {
            toastr.error("Wystąpił błąd podczas usuwania dostępu.");
        }
    }

    var errorFn = function() {
        toastr.error("Wystąpił błąd podczas usuwania dostępu.");
    }

    var ajaxFn = function() {
        GetAjaxWithFunctions(
            "/Projects/DeleteAccess",
            "POST",
            { fitterId: fitterId, taskId: taskId },
            successFn,
            errorFn,
            ".modal-dialog"
        );
    }

    Confirm("Czy napewno chcesz usunąć dostęp dla tego użytkownika?", ajaxFn);
});

$(document).on("click", ".delete-planned-task", function () {
    var eventID = $(this).data("event-id");

    var successFn = function (data) {
        if (data.success === true) {
            $(".plan-" + eventID).remove();
        } else {
            toastr.error("Wystąpił błąd podczas usuwania dostępu.");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania dostępu.");
    }

    var ajaxFn = function () {
        GetAjaxWithFunctions(
            "/Projects/DeletePlan",
            "POST",
            { eventID: eventID },
            successFn,
            errorFn,
            ".modal-dialog"
        );
    }

    Confirm("Czy napewno chcesz usunąć plan dla tego użytkownika?", ajaxFn);
});

$(document).on("click", "[data-index]", function () {
    var index = $(this).data("index");

    new Custombox.modal({
        content: {
            effect: "fadein",
            target: "#modal-photos-" + index,
            animateFrom: "top",
            animateTo: "bottom",
            positionX: "center",
            positionY: "center",
        },
        overlay: {
            active: 0.5
        }
    }).open();
});

$(document).on("click", ".activity-photos", function () {
    var id = $(this).data("activity-id");

    $(".modal-photos #photos").html("");

    $.getJSON("/Projects/ActivityPhotos?id=" + id, function (data) {
        $.each(data.photos, function (index, value) {
            var photo = '<div class="col-md-4 col-sm-6 co-xs-12 gal-item photo-' + value.ID + '">' +
                            '<div class="box">' +
                                '<a href="#" data-index="' + index + '">' +
                                    '<img src="' + value.Photo + '">' +
                                "</a>" +
                                "<h4>" + value.Desc + "</h4>" +
                                '<div id="modal-photos-' + index + '" class="modal-photos modal-photos-multiple">' +
                                    '<a href="javascript:void(0);" onclick="Custombox.modal.close();" class="demo-close"><i class="fa fa-times"></i></a>' +
                                    '<button type="button" class="btn btn-outline btn-info demo-close-btn" onclick="Custombox.modal.close();">Close</button>' +
                                    '<div class="modal-content">' +
                                        '<div class="modal-body">' +
                                            '<img src="' + value.Photo + '" class="img-responsive">' +
                                        "</div>" +
                                        '<div class="col-md-12 description">' +
                                            "<h4>" + value.Desc + "</h4>" +
                                            '<button type="button" data-photo-id="' + value.ID + '" class="btn btn-danger delete-photo" onclick="Custombox.modal.close();">Usuń</button>' +
                                        "</div>" +
                                    "</div>" +
                                "</div>" +
                                //'<div class="modal fade" id="2" tabindex="-1" role="dialog">' +
                                //    '<div class="modal-dialog">' +
                                //        '<div class="modal-content">' +
                                //            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>' +
                                //            '<div class="modal-body">' +
                                //                '<img src="' + value.Photo + '">' +
                                //            '</div>' +
                                //            '<div class="col-md-12 description">' +
                                //                '<h4>'+ value.Desc +'</h4>' +
                                //            '</div>' +
                                //        '</div>' +
                                //    '</div>' +
                                //'</div>' +
                            "</div>" +
                        "</div>";

            $(".modal-photos #photos").append(photo);
        });
    });

    new Custombox.modal({
        content: {
            effect: "fadein",
            target: "#modal-photos",
            animateFrom: "top",
            animateTo: "bottom",
            positionX: "center",
            positionY: "top",
            fullscreen: true
        },
        overlay: {
            active: 0.5
        }
    }).open();
});

$(document).on("click", ".delete-photo", function () {
    var photoID = $(this).data("photo-id");

    $.getJSON("/Projects/DeleteActivityPhoto?id=" + photoID, function (data) {
        if (data.success === true) {
            $(".photo-" + photoID).remove();
        }
        else {
            toastr.error("Wystąpił błąd podczas usuwania zdjęcia");
        }
    })
});

$(document).on("click", ".question-photo", function () {
    var id = $(this).data("task-id");
    var question = $(this).data("question-type");

    $(".modal-photos #photos").html("");

    $.getJSON("/Projects/QuestionPhoto?id=" + id + "&question=" + question, function (data) {
        $.each(data.photos, function (index, value) {
            var translated = (value.Translated !== null) ? value.Translated : "";

            var photo = '<div class="col-md-4 col-sm-6 co-xs-12 gal-item">' +
                            '<div class="box">' +
                                '<a href="#" data-index="' + index + '">' +
                                    '<img src="' + value.Photo + '">' +
                                "</a>" +
                                '<div id="modal-photos-' + index + '" class="modal-photos modal-photos-multiple">' +
                                    '<a href="javascript:void(0);" onclick="Custombox.modal.close();" class="demo-close"><i class="fa fa-times"></i></a>' +
                                    '<button type="button" class="btn btn-outline btn-info demo-close-btn" onclick="Custombox.modal.close();">Close</button>' +
                                    '<div class="modal-content">' +
                                        '<div class="modal-body">' +
                                            '<img src="' + value.Photo + '" class="img-responsive">' +
                                        "</div>" +
                                        '<div class="col-md-12 description">' +
                                            "<h4>" + value.Desc + "</h4>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>" +
                            "</div>" +
                "<h4>" + value.Desc + "</h4>" +
                '<div class="input-group">' +
                '<input type="text" class="question-photo-' + value.ID + ' form-control" value="' + translated + '" />' +
                    '<span class="input-group-btn">' +
                        '<button class="btn btn-primary save-question-translate" type="button" data-photo-id="' + value.ID + '">Zapisz</button>' +
                    "</span>" +
                "</div>" +
                        "</div>";

            $(".modal-photos #photos").append(photo);
        });
    });

    new Custombox.modal({
        content: {
            effect: "fadein",
            target: "#modal-photos",
            animateFrom: "top",
            animateTo: "bottom",
            positionX: "center",
            positionY: "top",
            fullscreen: true
        },
        overlay: {
            active: 0.5
        }
    }).open();
});

$(document).on("click", ".save-question-translate", function () {
    var photoID = $(this).data("photo-id");

    var translated = $(".custombox-content .question-photo-" + photoID).val();

    $.getJSON("/Projects/SaveQuestionTranslate?ID=" + photoID + "&translated=" + translated, function (data) {
        if (data.success === true) {
            toastr.success("Zapisano tłumaczenie opisu zdjęcia");
        }
        else {
            toastr.error("Wystąpił błąd podczas zapisywania tłumaczenia opisu zdjęcia");
        }
    })
});

$(document).on("click", ".add-task-event", function () {
    var id = $(this).data("task-id");

    LoadModalContent("#modal", "/Projects/Modal_AddTaskEvent/" + id);
});

$(document).on("click", ".details-raport", function () {
    var id = $(this).data("task-id");

    LoadModalContent("#modal", "/Print/Raport?ID=" + id + "&Print=false");
});

$(document).on("submit", ".add-task-event-form", function () {
    var taskId = $("#ReferenceTo").val();
    var title = $("#Title").val();
    var start = $("#Start").val();
    var end = $("#End").val();
    var desc = $("#Description").val();
    var color = $(".radios .active input").val();
    var allDay = $("#AllDay").is(":checked");

    $.ajax({
        url: "/Projects/AddEvent",
        dataType: "json",
        type: "POST",
        data: { Type: 0, ReferenceTo: taskId, Title: title, Start: start, End: end, Description: desc, Color: color, AllDay: allDay },
        success: function (data) {
            if (data.success === "true") {
                toastr.success("Wydarzenie zostało utworzone!");
            }
            else {
                toastr.error("Wystąpił błąd podczas tworzenia wydarzenia!");
            }
        },
        error: function (XMLHttpRequest) {
            toastr.error("Wystąpił błąd podczas tworzenia wydarzenia!");
        }
    });
});

$(document).on("change", ".question-select", function () {
    var type = $(this).data("question-type");
    var val = $(this).val();
    var taskId = $("#selected-task-id").val();

    $(this).removeClass("Nie").removeClass("Tak").removeClass("Nie_Dotyczy").addClass(val);
    $(".td-" + type).removeClass("Nie").removeClass("Tak").removeClass("Nie_Dotyczy").addClass(val);

    var ajaxData = { TaskId: taskId, Type: type, Value: val }

    GetAjaxWithSuccess("/Projects/EditTaskQuestion", "POST", ajaxData, "Odpowiedź na pytanie została zmieniona!", "Wystąpił błąd podczas edycji odpowiedzi!", ".question-select");
});

$(document).on("change", "#ProductId", function () {
    GetProduct($(this).val(), false);
    GetProduct_Questions($(this).val());
});

$(document).on("change", "#EditProductId", function () {
    GetProduct($(this).val(), true);
});

$(document).on("change", ".patch-checkbox", function () {

    var id = $(this).data("patch-id");
    var status = $(this).is(":checked");

    var ajaxData = { PatchID: id, PatchStatus: status }

    var success = function (data) {
        if (data.success === "true") {
            toastr.success("Element zadania został zaktualizowany pomyślnie");

            if (status === true)
                $(".task-element-" + id).css({ 'background': "green", 'color': "white" });
            else
                $(".task-element-" + id).css({ 'background': "none", 'color': "#333" });
        }
        else {
            toastr.error("Wystąpił błąd podczas aktualizacji elementu zadania!");
        }
    }

    var error = function (xhr, status) {
            toastr.error("Wystąpił błąd podczas aktualizacji elementu zadania!");
    }

    GetAjaxWithFunctions("/Projects/EditTaskPatch?PatchID="+id+"&PatchStatus="+status, "GET", null, success, error, this);
});

$(document).on("change", "#Street", function () {
    if ($("#Street").val() !== "" && $("#City").val() !== "")
        GenerateMap();
});

$(document).on("change", "#CustomerId", function () {
    var CustomerID = $(this).val();

    GetProjectManagers(CustomerID);
});

$(document).on("click", "#add-pm", function () {
    var customerID = $("#CustomerId").val();

    LoadModalContent("#modal", "/Projects/Modal_AddProjectManager?CustomerID=" + customerID);
});

$(document).on("click", ".save-pm", function() {
    var customerID = $(".custombox-content #HiddenCustomerID").val();
    var name = $(".custombox-content #PMName").val();
    var email = $(".custombox-content #PMEmail").val();
    var phonenumber = $(".custombox-content #PMPhoneNumber").val();

    var successFn = function(data) {
        if (data.success === true) {
            toastr.success("Pomyślnie dodano menedżera projektu.");
            $("#ProjectManagers").append('<option value="' + data.pmID + '" selected>' + data.pmName + "</option>");
        } else {
            toastr.error("Wystąpił błąd podczas dodawania menedżera projektu.");
        }
    }

    var errorFn = function() {
        toastr.error("Wystąpił błąd podczas dodawania menedżera projektu.");
    }

    var data = { customerID: customerID, name: name, email: email, phonenumber: phonenumber };

    GetAjaxWithFunctions("/Projects/AddProjectManager", "POST", data, successFn, errorFn, "");
});

$(document).on("change", "#City", function () {
    if ($("#Street").val() !== "" && $("#City").val() !== "")
        GenerateMap();
});

$(document).on("change", "#Status", function() {
    var status = $(this).val();
    var prevStatus = $(".custombox-content #FirstState").val();

    if (status !== prevStatus) {
        $(".change-reason").show();
        $(".change-reason-box").attr("required", true);
    } else {
        $(".change-reason-box").val("");
        $(".change-reason-box").attr("required", false);
        $(".change-reason").hide();
    }
});

$(document).on("dp.change", ".custombox-content #StartDate.autodate", function () {
    var date = $(this).val();

    var newMoment = moment(date, "YYYY-MM-DD HH:mm").add(4, "hours").format("YYYY-MM-DD HH:mm");

    $(".custombox-content #EndDate").val(newMoment);
});

$(document).on("dp.change", ".custombox-content #Start.autodate", function () {
    var date = $(this).val();

    var newMoment = moment(date, "YYYY-MM-DD HH:mm").add(4, "hours").format("YYYY-MM-DD HH:mm");

    $(".custombox-content #End").val(newMoment);
});

$(document).on("click", ".block-task", function () {
    var taskID = $(this).data("task-id");
    var blocked = $(this).data("blocked");

    var fn = function () {
        $.ajax({
            url: "/Projects/LockTask",
            dataType: "json",
            type: "GET",
            data: { id: taskID },
            success: function (data) {
                if (data.success === true) {
                    if (blocked === 1) {
                        $(".icon-column-" + taskID).html("");
                        $(".lock-" + taskID).data("blocked", 0);
                        $(".lock-" + taskID).removeClass("crud-accept").addClass("crud-delete");
                    }
                    else {
                        $(".icon-column-" + taskID).html('<span style="color: red;"><i class="glyphicon glyphicon-lock"></i></span>');
                        $(".lock-" + taskID).data("blocked", 1);
                        $(".lock-" + taskID).removeClass("crud-delete").addClass("crud-accept");
                    }
                }
                else {
                    toastr.error("Wystąpił błąd podczas blokowania zadania.");
                }
            },
            error: function (XMLHttpRequest) {
                $.alert("Błąd " + XMLHttpRequest.status + ". Proszę spróbować jeszcze raz.");
            }
        });
    }

    var question = "Czy na pewno chcesz zablokować zadanie?";

    if (blocked === 1)
        question = "Czy na pewno chcesz odblokować zadanie?";

    Confirm(question, fn);
});

$(document).on("click", ".force-invoice", function () {
    var taskID = $(this).data("task-id");
    var set = $(this).data("set");

    var fn = function () {
        $.ajax({
            url: "/Projects/SetTaskToForceInvoice",
            dataType: "json",
            type: "GET",
            data: { ID: taskID, set: set },
            success: function (data) {
                if (data.success === true) {
                    if (set === "True") {
                        $(".icon-column-" + taskID).html('<span style="color: red;">FV</span>');
                        $(".force-" + taskID).data("blocked", 0);
                        $(".force-" + taskID).removeClass("crud-accept").addClass("crud-delete");
                    }
                    else {
                        $(".icon-column-" + taskID).html('');
                        $(".force-" + taskID).data("blocked", 1);
                        $(".force-" + taskID).removeClass("crud-delete").addClass("crud-accept");
                    }
                }
                else {
                    toastr.error("Wystąpił błąd podczas wymuszania fakturowania.");
                }
            },
            error: function (XMLHttpRequest) {
                $.alert("Błąd " + XMLHttpRequest.status + ". Proszę spróbować jeszcze raz.");
            }
        });
    }

    fn();
});

$(document).on("click", ".fill-task", function () {
    var taskID = $(this).data("task-id");

    var successFn = function (data) {

        if (data.success === true) {
            toastr.success("Pomyślnie wypełniono zadanie !");

            $(".task-count-" + taskID).html(data.left);
        }
        else {
            toastr.error("Wystąpił błąd podczas dopełniania zadania");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas dopełniania zadania");
    }

    GetAjaxWithFunctions("/Projects/FillTask", "POST", { ID: taskID }, successFn, errorFn, "");
});

$(document).on("click", ".send-raport", function () {
    var id = $(this).data("task-id");

    var successFn = function (data) {
        if (data.success === true) {
            toastr.success("Pomyślnie wysłano raport");
            location.reload();
        }
        else {
            toastr.error(data.message);
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas wysyłania raportu");
    }

    GetAjaxWithFunctions("/Projects/SendRaport", "GET", { ID: id }, successFn, errorFn, "");
});

$(document).on("click", ".add-service-element", function() {
    var desc = $(".description-question").last().val();
    var question = $(".select-question-name").last().val();
    var code = $(".task-service-error-code").last().val();
    var room = $(".task-service-room").last().val();

    var container = '<div class="question-container">';
    container += '<div class="col-xs-9">';
    container += '<input type="hidden" name="questionDescription[]" value="' + desc + '" />';
    container += '<input type="hidden" name="questionSelect[]" value="' + question + '" />';
    container += '<input type="hidden" name="questionCode[]" value="' + code + '" />';
    container += '<input type="hidden" name="questionRoom[]" value="' + room + '" />';
    container += "<strong>#" + code + " / " + question + "(" + room + ")</strong> <br />";
    container += "<span>" + desc + "</span>";
    container += "</div>";
    container += '<div class="col-xs-3">';
    container += '<button type="button" class="btn btn-danger delete-added-question"><i class="fa fa-remove"></i></button>';
    container += "</div>";
    container += "</div>";

    var row = '<tr>';
    row += '<td><input type="text" class="form-control" name="questionCode[]" value="' + code + '" /><input type="hidden" name="questionSelect[]" value="' + question + '" /></td>';
    row += '<td><input type="text" class="form-control" name="questionRoom[]" value="' + room + '" /></td>';
    row += '<th>' + question + '</th>';
    row += '<td><input type="text" class="form-control" name="questionDescription[]" value="' + desc + '" /></td>';
    row += '<td><button type="button" class="btn btn-danger delete-added-question"><i class="fa fa-remove"></i></button></td>';
    row += '</tr>';

    $(".added-questions tbody").append(row);

    $(".description-question").last().val("");
});

$(document).on("click", ".add-service-element-from-questions", function() {
    var desc = $(this).data("description");
    var question = $(this).data("question");
    var id = $(this).data("id");

    $("#description-id-" + id + " a").addClass("hidden");

    $.get("/Projects/ChangeQuestionDescriptionState/" + id);

    var row = '<tr>';
    row += '<td><input type="text" class="form-control" name="questionCode[]" value="" /><input type="hidden" name="questionSelect[]" value="' + question + '" /></td>';
    row += '<td><input type="text" class="form-control" name="questionRoom[]" value="" /></td>';
    row += '<th>' + question + '</th>';
    row += '<td><input type="text" class="form-control" name="questionDescription[]" value="' + desc + '" /></td>';
    row += '<td><button type="button" class="btn btn-danger delete-added-question"><i class="fa fa-remove"></i></button></td>';
    row += '</tr>';

    $(".added-questions tbody").append(row);

    $(".description-question").last().val("");
});

$(document).on("click", ".add-service-element-in-edit", function () {
    var desc = $(".description-question").last().val();
    var question = $(".name-question").last().val();
    var code = $(".task-service-error-code").last().val();
    var room = $(".task-service-room").last().val();

    var row = '<tr>';
    row += '<td><input type="text" class="form-control" name="questionCode[]" value="' + code + '" /><input type="hidden" name="questionSelect[]" value="' + question + '" /></td>';
    row += '<td><input type="text" class="form-control" name="questionRoom[]" value="' + room + '" /></td>';
    row += '<th>' + question + '</th>';
    row += '<td><input type="text" class="form-control" name="questionDescription[]" value="' + desc + '" /></td>';
    row += '<td><button type="button" class="btn btn-danger delete-added-question"><i class="fa fa-remove"></i></button></td>';
    row += '</tr>';

    $(".questions-table tbody").append(row);

    $(".name-question").last().val("");
    $(".description-question").last().val("");
    $(".task-service-error-code").last().val("");
    $(".task-service-room").last().val("");
});

$(document).on("click", ".delete-added-question", function () {
    var id = $(this).data("id");

    if (id !== undefined) {
        $("#description-id-" + id + " a").removeClass("hidden");
        $.get("/Projects/ChangeQuestionDescriptionState/" + id);
    }

    $(this).parent().parent().remove();
});

$(document).on("click", "#save-project-description", function() {
    var projectID = $(this).data("project-id");
    var description = $(".textarea-wysiwg").val();

    var successFn = function(data) {
        if (data.success === true)
            toastr.success("Pomyślnie zapisano opis projektu.");
        else
            toastr.error("Wystąpił błąd podczas zapisywania opisu projektu.");
    }

    var errorFn = function() {
        toastr.error("Wystąpił błąd podczas zapisywania opisu projektu.");
    }

    GetAjaxWithFunctions("/Projects/ChangeProjectDescription", "POST", {projectID: projectID, description: description}, successFn, errorFn, "");
});

$(document).on("click", "#save-project-invoice-description", function () {
    var projectID = $(this).data("project-id");
    var description = $("#InvoiceDescription").val();

    var successFn = function (data) {
        if (data.success === true)
            toastr.success("Pomyślnie zapisano opis dla faktury.");
        else
            toastr.error("Wystąpił błąd podczas zapisywania opisu dla faktury.");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas zapisywania opisu faktury.");
    }

    GetAjaxWithFunctions("/Projects/ChangeProjectInvoiceDescription", "POST", { projectID: projectID, description: description }, successFn, errorFn, "");
});

$(document).on("click", "#save-apartment-description", function () {
    var apartmentID = $(this).data("apartment-id");
    var description = $(".textarea-wysiwg").val();

    var successFn = function (data) {
        if (data.success === true)
            toastr.success("Pomyślnie zapisano opis mieszkania.");
        else
            toastr.error("Wystąpił błąd podczas zapisywania opisu mieszkania.");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas zapisywania opisu projektu.");
    }

    GetAjaxWithFunctions("/Projects/ChangeApartmentDescription", "POST", { apartmentID: apartmentID, description: description }, successFn, errorFn, "");
});

$(document).on("click", "#save-apartment-invoice-description", function () {
    var apartmentID = $(this).data("apartment-id");
    var description = $("#InvoiceDescription").val();

    var successFn = function (data) {
        if (data.success === true)
            toastr.success("Pomyślnie zapisano opis dla faktury.");
        else
            toastr.error("Wystąpił błąd podczas zapisywania opisu dla faktury.");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas zapisywania opisu dla faktury.");
    }

    GetAjaxWithFunctions("/Projects/ChangeApartmentInvoiceDescription", "POST", { apartmentID: apartmentID, description: description }, successFn, errorFn, "");
});

$(document).on("change", ".percent-change", function() {
    CountPercents($(this).data("count-directly"));
});

$(document).on("change", ".product-price", function () {
    CountPercents($(this).data("count-directly"));
});

$(document).on("click", ".details-order", function () {
    var id = $(this).data("order-id");

    LoadModalContent("#modal", "/Projects/ShowOrder/" + id);
});

$(document).on("click", ".translate-order", function () {
    var id = $(this).data("order-id");
    var redirect = $(this).data("redirect");

    LoadModalContent("#modal", "/Projects/TranslateOrder?ID=" + id + "&Redirect=" + redirect);
});

$(document).on("click", ".delete-order", function () {
    var id = $(this).data("order-id");

    var fn = function () {
        $.ajax({
            url: "/Projects/DeleteOrder",
            dataType: "json",
            type: "GET",
            data: { ID: id },
            success: function (data) {
                if (data.success === true) {
                    location.reload();
                }
                else {
                    toastr.error("Wystąpił błąd podczas usuwania zamówienia.");
                }
            },
            error: function (XMLHttpRequest) {
                $.alert("Błąd " + XMLHttpRequest.status + ". Proszę spróbować jeszcze raz.");
            }
        });
    }

    Confirm("Czy na pewno chcesz usunąć zamówienie?", fn);
});

$(document).on("click", ".finish-order", function () {
    var id = $(this).data("order-id");

    var fn = function () {
        $.ajax({
            url: "/Projects/FinishOrder",
            dataType: "json",
            type: "GET",
            data: { ID: id },
            success: function (data) {
                if (data.success === true) {
                    location.reload();
                }
                else {
                    toastr.error("Wystąpił błąd podczas zakańczania zamówienia.");
                }
            },
            error: function (XMLHttpRequest) {
                $.alert("Błąd " + XMLHttpRequest.status + ". Proszę spróbować jeszcze raz.");
            }
        });
    }

    Confirm("Czy na pewno chcesz zakończyć zamówienie?", fn);
});

$(document).on("click", ".send-order", function () {
    var orderID = $(this).data("order-id");
    var projectID = $(this).data("project-id");

    LoadModalContent("#modal", "/Projects/Modal_SendOrder?ProjectID=" + projectID + "&OrderID=" + orderID);
});

$(document).on("click", ".send-pm-mail", function() {
    var orderID = $(".custombox-content #OrderID").val();
    var email = $(".custombox-content #pm-mail").val();

    var successFn = function(data) {
        if (data.success === true) {
            location.reload();
        } else {
            toastr.error("Wystąpił błąd podczas wysyłania zamówienia.");
        }
    }

    var errorFn = function() {
        toastr.error("Wystąpił błąd podczas wysyłania zamówienia.");
    }

    GetAjaxWithFunctions("/Projects/SendOrder", "POST", { OrderID: orderID, Email: email }, successFn, errorFn, "");
});

$(document).on("click", ".send-handy-mail", function () {
    var orderID = $(".custombox-content #OrderID").val();
    var email = $(".custombox-content #handy-mail").val();

    var successFn = function (data) {
        if (data.success === true) {
            location.reload();
        } else {
            toastr.error("Wystąpił błąd podczas wysyłania zamówienia.");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas wysyłania zamówienia.");
    }

    GetAjaxWithFunctions("/Projects/SendOrder", "POST", { OrderID: orderID, Email: email }, successFn, errorFn, "");
});

$(document).on("change", "#task-document", function () {
    var taskID = $(this).data("task-id");

    var formData = new FormData();
    var totalFiles = this.files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = this.files[i];

        formData.append("task-img", file);
    }

    formData.append("TaskID", taskID);

    $.ajax({
        type: "POST",
        url: "/Projects/UploadTaskDocument",
        data: formData,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.result === "true") {
                toastr.success("Dokument został dodany!");

                var docRow = "<tr>";
                docRow += "<td>" + response.fileName + "</td>";
                docRow += '<td class="text-center"><a href="/Monter/dist/Documents/' + response.newFileName + '" class="btn btn-sm btn-primary text-center">Pobierz</a></td>';
                docRow += "</tr>";

                $(".documents-table").append(docRow);
            }
            else {
                toastr.error("Wystąpił błąd podczas dodawania dokumentu.");
            }
        },
        error: function (error) {
            toastr.error("Wystąpił błąd podczas dodawania dokumentu.");
        }
    });
});

$(document).on("click", "#task-comments-tab", function () {
    var id = $(this).data("apartment-id");

    var successFn = function (data) {
        if (data.success === true) {
            $("#task-comment-alert").remove();
        }
    }

    var errorFn = function () { }

    GetAjaxWithFunctions("/Projects/TaskCommentsSetViewed", "POST", { ID: id }, successFn, errorFn, "#task-comments-tab");
});

$(document).on("click", "#new-order-apartment", function () {
    var projectID = $(this).data("project-id");
    var apartmentID = $(this).data("apartment-id");

    LoadModalContent("#modal", "/Projects/Modal_NewOrderInApartment?ProjectID=" + projectID + "&ApartmentID=" + apartmentID);
});

$(document).on("click", "#new-order-project", function () {
    var projectID = $(this).data("project-id");

    LoadModalContent("#modal", "/Projects/Modal_NewOrderInProject?ProjectID=" + projectID);
});

$(document).on("click", "#add-project-access", function () {
    var projectID = $(this).data("project-id");

    LoadModalContent("#modal", "/Projects/Modal_ProjectAccess?ProjectID=" + projectID);
});

$(document).on("click", "#add-apartment-access", function () {
    var ID = $(this).data("apartment-id");

    LoadModalContent("#modal", "/Projects/Modal_ApartmentAccess?ApartmentID=" + ID);
});

$(document).on("click", "#add-task-access", function () {
    var ID = $(this).data("task-id");

    LoadModalContent("#modal", "/Projects/Modal_TaskAccess?TaskID=" + ID);
});

$(document).on("click", ".delete-project-access", function () {
    var ID = $(this).data("access-id");

    var data = {
        ID: ID
    };

    var successFn = function (data) {
        if (data.success) {
            $("#tr-project-access-" + ID).remove();

            toastr.success("Pomyślnie usunięto dostęp !");
        }
        else
            toastr.error("Wystąpił błąd podczas usuwania dostępu !");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania dostępu !");
    }

    GetAjaxWithFunctions("/Projects/ProjectAccess_Delete", "POST", data, successFn, errorFn, "");
});

$(document).on("click", ".delete-apartment-access", function () {
    var ID = $(this).data("access-id");

    var data = {
        ID: ID
    };

    var successFn = function (data) {
        if (data.success) {
            $("#tr-apartment-access-" + ID).remove();

            toastr.success("Pomyślnie usunięto dostęp !");
        }
        else
            toastr.error("Wystąpił błąd podczas usuwania dostępu !");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania dostępu !");
    }

    GetAjaxWithFunctions("/Projects/ApartmentAccess_Delete", "POST", data, successFn, errorFn, "");
});

$(document).on("click", ".delete-task-access", function () {
    var ID = $(this).data("access-id");

    var data = {
        ID: ID
    };

    var successFn = function (data) {
        if (data.success) {
            $("#tr-task-access-" + ID).remove();

            toastr.success("Pomyślnie usunięto dostęp !");
        }
        else
            toastr.error("Wystąpił błąd podczas usuwania dostępu !");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania dostępu !");
    }

    GetAjaxWithFunctions("/Projects/TaskAccess_Delete", "POST", data, successFn, errorFn, "");
});

var nextLp = 0;

$(document).on("click", "#add-order-item", function () {
    var item = $(".custombox-content #order-item").last().val();

    $(".custombox-content #order-item").last().val("");

    var row = '<tr class="row-' + (nextLp + 1) + '">';
    row += "<td>" + item + "</td>";
    row += '<td style="text-align: center;"><button type="button" data-row-number="' + (nextLp + 1) + '" class="btn btn-danger delete-order-item">USUŃ</button>';
    row += "</tr>";

    $(".order-list tbody").append(row);

    var hidden = '<input type="hidden" name="orderItem[]" value="' + item + '" class="row-' + (nextLp + 1) + '" />';

    $(".hidden-order-items").append(hidden);

    nextLp++;
});

$(document).on("click", ".delete-order-item", function () {
    var lp = $(this).data("row-number");

    $(".row-" + lp).remove();
});


$(document).on('change', '#Apartments', function () {
    var apartmentId = $(this).val();

    $('.custombox-content #Tasks').html('');

    $.getJSON('/Projects/TasksList?apartmentId=' + apartmentId, function (data) {

        if (data.length === 0) $('.custombox-content #TaskEventCreate').prop('disabled', true);
        else $('.custombox-content #TaskEventCreate').prop('disabled', false);

        $('.custombox-content #Tasks').append("<option></option>");

        $.each(data, function (index, value) {
            var option = '<option value="' + value.Id + '">' + value.ProductName;

            if (value.Sufix !== null)
                option += ' ' + value.Sufix;

            option += '</option>';

            $('.custombox-content #Tasks').append(option);
        });
    });
});

$(document).on('click', '.delete-activity', function () {
    var activityID = $(this).data('activity-id');

    var successFn = function () {
        $('tr.activity-' + activityID).addClass('row-disabled');
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas dezaktywacji wpisu");
    }

    var fn = function () {
        GetAjaxWithFunctions("/Projects/ActivityDelete", "GET", { id: activityID }, successFn, errorFn, 'tr.activity-' + activityID);
    }

    Confirm("Czy na pewno chcesz dezaktywować pozycję?", fn);
});

$(document).on('click', '.add-fee', function () {
    var projectID = $(this).data("project-id");
    var apartmentID = $(this).data("apartment-id");

    LoadModalContent("#modal", "/Office/Modal_AddFee?projectID=" + projectID + "&apartmentID=" + apartmentID);
});

$(document).on("change", "#InSystemID", function () {
    var val = $(this).val();
    var projectID = $(this).data("project-id");

    var success = function (data) {
        if (data.exist) {
            $(".form-group-for-id").addClass("has-warning");
            $(".form-group-for-id .fa, .form-group-for-id .help-block").show();
        }
        else {
            $(".form-group-for-id").removeClass("has-warning");
            $(".form-group-for-id .fa, .form-group-for-id .help-block").hide();
        }
    }

    var error = function () {
        toastr.error("Wystąpił błąd podczas sprawdzania ID!");
    }

    GetAjaxWithFunctions("/Projects/CheckProjectID", "POST", { ID: val, ProjectID: projectID }, success, error, "#InSystemID");
});

$(document).on("click", "#set-project-check", function () {
    var projectID = $(this).data("project-id");
    var isChecked = $(this).data("is-checked");

    var success = function (data) {
        if (data.success === true) {
            toastr.success("Oznaczenie projektu 'SPRAWDZONY' zostało zmienione");

            if (isChecked === "True")
            {
                $("#set-project-check").removeClass("btn-danger").addClass("btn-success");
                $("#set-project-check").data("is-checked", "False").text("PROJEKT SPRAWDZONY");
            }
            else
            {
                $("#set-project-check").removeClass("btn-success").addClass("btn-danger");
                $("#set-project-check").data("is-checked", "True").text("PROJEKT NIESPRAWDZONY");
            }
        }
        else {
            toastr.error("Wystąpił błąd podczas zmiany oznaczenia projektu.");
        }
    }

    var error = function () {
        toastr.error("Wystąpił błąd podczas zmiany oznaczenia projektu.");
    }

    GetAjaxWithFunctions("/Projects/ChangeProjectCheck", "POST", { ProjectID: projectID, IsChecked: isChecked }, success, error, "");
});

$(document).on("click", ".block-photo", function () {
    var source = $(this).data("source");
    var id = $(this).data("id");
    var isBlocked = $(this).data("blocked");

    var button = $(this);

    var successFn = function (data) {
        if (data.success === true) {
            toastr.success("Stan zdjęcia zmieniony poprawnie.");

            if (isBlocked === "True") {
                $("." + source + "-" + id).removeClass("is-blocked");
                button.data("blocked", "False").removeClass("btn-success").addClass("btn-warning").text("Zablokuj");
            }
            else {
                $("." + source + "-" + id).addClass("is-blocked");
                button.data("blocked", "True").removeClass("btn-warning").addClass("btn-success").text("Odblokuj");
            }
        }
        else {
            toastr.error("Wystąpił błąd podczas zmiany stanu zdjęcia!");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas zmiany stanu zdjęcia!");
    }

    var data = {
        isBlocked: isBlocked,
        source: source,
        id: id
    };

    GetAjaxWithFunctions("/Projects/BlockPhoto", "POST", data, successFn, errorFn, "." + source + "-" + id);
});

$(document).on("click", ".delete-photo", function () {
    var source = $(this).data("source");
    var id = $(this).data("id");

    var button = $(this);

    var successFn = function (data) {
        if (data.success === true) {
            toastr.success("Zdjęcie usunięto pozytywnie.");

            $("." + source + "-" + id).remove();
        }
        else {
            toastr.error("Wystąpił błąd podczas usuwania zdjęcia!");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania zdjęcia!");
    }

    var data = {
        source: source,
        id: id
    };

    var fn = function () {
        GetAjaxWithFunctions("/Projects/DeletePhoto", "POST", data, successFn, errorFn, "." + source + "-" + id);
    }

    Confirm("Czy na pewno chcesz usunąć to zdjęcie?", fn);
});

$(document).on("click", ".show-translate-btn", function () {
    var elem = $(this).data("to-collapse");

    $(elem).toggle();
});

$(document).on("click", ".save-translate", function () {
    var question = $(this).data("question");
    var descID = $(this).data("id");
    var taskID = $(this).data("task-id");

    var descPL = $('.custombox-content input[name="' + question + '_DescriptionPL_' + descID + '"]').val();
    var descSE = $('.custombox-content input[name="' + question + '_DescriptionSE_' + descID + '"]').val();

    if (descPL === undefined || descSE === undefined) {
        descPL = $('input[name="' + question + '_DescriptionPL_' + descID + '"]').val();
        descSE = $('input[name="' + question + '_DescriptionSE_' + descID + '"]').val();
    }

    var successFn = function (data) {
        if (data.success === true)
            toastr.success("Pomyślnie zapisano opisy i tłumaczenia !");
        else
            toastr.error("Wystąpił błąd podczas zapisywania opisów i tłumaczeń!");
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas zapisywania opisów i tłumaczeń!");
    }

    var data = {
        taskID: taskID,
        descID: descID,
        question: question,
        descPL: descPL,
        descSE: descSE
    };

    GetAjaxWithFunctions("/Projects/SaveQuestionTranslation", "POST", data, successFn, errorFn, ".custombox-content");
});

$(document).on("change", ".work-done-field", function () {
    var toGoField = $("#countToGo").val().replace(',','.');
    var toGo = Number(toGoField);

    $(".work-done-field").each(function (i, obj) {
        var workDoneField = $(this).val().replace(',','.');
        var workDone = Number(workDoneField);

        if (workDone !== 'NaN')
            toGo -= workDone;

        toGo = toGo.toFixed(2);
    });

    $(".submit-handy-finish").attr("disabled", toGo < 0);


    $(".count-to-go").html(toGo);
});


$(document).on("click", "#connect-product", function () {
    var projectID = $(this).data("project-id");
    var productID = $("#ProductId").val();
    var price = $("#ProductPrice").val();
    var p1 = $("#PercentType1").val();
    var p2 = $("#PercentType2").val();
    var noExtra = $("#NoExtra").prop("checked");

    var productName = $("#product-name").val();
    var productInvoice = $("#product-invoice").val();

    if (price === undefined || price === '') {
        toastr.error("Proszę podać cenę produktu dla projektu !");
        return;
    }



    var data = { ProjectID: projectID, ProductID: productID, Price: price, productName: productName, PercentType1: p1, PercentType2: p2, invoice: productInvoice, NoExtra: noExtra };

    var successFn = function (data) {
        if (data.success) {
            var displayName = data.ProductName;
            displayName += data.OnInvoiceName && data.OnInvoiceName !== "" ? " (" + data.OnInvoiceName + ")" : "";
            displayName += ' <small style="color: darkgray;">' + data.ProductType + '</small>';

            var percentType1_price = Math.round(data.Price * (data.PercentType1 / 100));
            var percentType2_price = Math.round(data.Price * (data.PercentType2 / 100));

            var row = '<tr class="connect-' + data.ConnectID + '" data-name="' + productName + '" data-invoice= "' + data.OnInvoiceName + '"> ' +
                '<td><input type="checkbox" class="minimal project-product-check" value="' + data.ConnectID + '" /></td>' +
                '<td><a href="/Products/Edit/' + data.ProductID + '">' + displayName + '</a></td>' +
                '<td  id="connect-price-' + data.ConnectID + '" data-price="' + data.Price + '">' + data.Price + ' kr</td>' +
                '<td id="connect-p1-' + data.ConnectID + '" data-p="' + data.PercentType1 + '">' + (data.PercentType1 !== null ? data.PercentType1 + "% (" + percentType1_price + "kr)" : "") + '</td> ' +
                '<td id="connect-p2-' + data.ConnectID + '" data-p="' + data.PercentType2 + '">' + (data.PercentType2 !== null ? data.PercentType2 + "% (" + percentType2_price + "kr)" : "") + '</td> ' +
                '<td id="no-extra-' + data.ConnectID + '" data-no-extra="' + data.NoExtra + '">' + (data.NoExtra === true ? 'TAK' : 'NIE') + '</td>' +
                '<td>' +
                '<div class="crud-container">' +
                '<a href="/Question/QuestionsForProductProject/' + data.ConnectID + '" class="crud crud-edit" data-toggle="tooltip" data-placement="bottom" title="Pytania do produktu"><i class="fa fa-question"></i></a>' +
                '<span data-toggle="tooltip" data-placement="bottom" title="Edytuj cenę" data-connect-id="' + data.ConnectID + '" class="crud crud-edit edit-connect-product"><i class="fa fa-pencil"></i></span>' +
                '<span data-toggle="tooltip" data-placement="bottom" title="Usuń" data-connect-id="' + data.ConnectID + '" class="crud crud-delete delete-connect-product"><i class="glyphicon glyphicon-remove"></i></span>' +
                '</div>' +
                '</td>' +
                '</tr>';

            $('.products-table').append(row);

            $('#ProductId option[value="' + data.ProductID + '"]').remove();

            toastr.success("Pomyślnie powiązano produkt z projektem");
            DefineICheck();
        }
        else {
            toastr.error("Wystąpił błąd podczas wiązania produktu z projektem !");
        }
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas wiązania produktu z projektem !");
    }

    GetAjaxWithFunctions("/Projects/ConnectProjectWithProduct", "POST", data, successFn, errorFn, "");
});

$(document).on("click", "#plan-product", function () {
    var projectID = $(this).data("project-id");
    var productCount = $("#PlanProductCount").val();
    var productID = $("#PlanProductId").val();

    var data = { ProjectID: projectID, ProductID: productID, Count: productCount };

    var successFn = function (data) {
        if (data.success) {
            var row = '<tr class="plan-' + data.PlanID + '">' +
                '<td><a href="/Products/Edit/' + data.ProductID + '">' + data.ProductName + '</a></td>' +
                '<td>' + data.Jm + '</td>' +
                '<td>' + data.PlanCount + '</td>' +
                '<td>' + data.PlanDone + '</td>' +
                '<td>' + data.PlanLeft + '</td>' +
                '<td>' +
                '<div class="crud-container">' +
                '<span data-toggle="tooltip" data-placement="bottom" title="Usuń" data-plan-id="' + data.PlanID + '" class="crud crud-delete delete-plan-product"><i class="fa fa-remove"></i></span>' +
                '</div>' +
                '</td>' +
                '</tr>';

            $('.plan-table').append(row);

            $('#PlanProductId option[value="' + data.ProductID + '"]').remove();

            toastr.success("Pomyślnie zaplanowano ilości dla produktu !");
        }
        else {
            toastr.error("Wystąpił błąd podczas planowania ilości dla produktu !");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas wiązania produktu z projektem !");
    }

    GetAjaxWithFunctions("/Projects/PlanProductProject", "POST", data, successFn, errorFn, "");
});

$(document).on("click", ".delete-connect-product", function () {
    var connectID = $(this).data("connect-id");

    var data = { ConnectID: connectID };

    var successFn = function (data) {
        if (data.success) {
            $(".connect-" + connectID).remove();

            toastr.success("Pomyślnie usunięto powiązanie.");
        }
        else {
            toastr.error("Wystąpił błąd podczas usuwania powiązania !");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania powiązania !");
    }

    var fn = function () {
        GetAjaxWithFunctions("/Projects/DeleteConnectWithProjectAndProduct", "POST", data, successFn, errorFn, "");
    }

    Confirm("Czy na pewno chcesz usunąć powiązanie?", fn);
});

$(document).on("click", ".delete-plan-product", function () {
    var planID = $(this).data("plan-id");

    var data = { PlanID: planID };

    var successFn = function (data) {
        if (data.success) {
            $(".plan-" + planID).remove();

            toastr.success("Pomyślnie usunięto planowanie.");
        }
        else {
            toastr.error("Wystąpił błąd podczas usuwania planowania !");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania planowania !");
    }

    var fn = function () {
        GetAjaxWithFunctions("/Projects/DeletePlanProjectProduct", "POST", data, successFn, errorFn, "");
    }

    Confirm("Czy na pewno chcesz usunąć planowanie?", fn);
});

$(document).on("click", ".delete-file", function () {
    var id = $(this).data("id");
    var url = $(this).data("url");
    var from = $(this).data("from");
    var tr = $(this).closest("tr");

    var data = {
        ID: id,
        URL: url,
        From: from
    };

    var successFn = function (data) {
        if (data.success)
            tr.remove();
        else
            toastr.error("Wystąpił błąd podczas usuwania pliku !");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania pliku !");
    }

    GetAjaxWithFunctions("/Projects/AJAX_DeleteFile", "POST", data, successFn, errorFn, tr);
});

$(document).on("click", ".show-event", function () {
    var type = $(this).data("type");
    var eventId = $(this).data("id");

    var url = "";

    switch (type) {
        case 'Task':
            url = "/Home/EventForTask/" + eventId;
            LoadModalContent("#event-modal", url);
            break;
        case 'Project':
            url = "/Home/EventForProject/" + eventId;
            LoadModalContent("#event-modal", url);
            break;
        default:
            $.alert('Brak podanego typu wydarzenia');
    }
});


$(document).on("click", ".save-event", function () {
    var eventID = $(this).data("event-id");
    var title = $(".custombox-content #Title").val();
    var description = $(".custombox-content #Description").val();
    var start = $(".custombox-content #Start").val();
    var end = $(".custombox-content #End").val();
    var allDay = $(".custombox-content #AllDay").is(":checked");

    var data = {
        ID: eventID,
        Title: title,
        Description: description,
        Start: start,
        End: end,
        AllDay: allDay
    };

    var successFn = function (data) {
        if (data.success)
            toastr.success("Wydarzenie zostało zapisane !");
        else
            toastr.error("Wystąpił błąd podczas zapisywania wydarzenia !");

        location.reload();
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas zapisywania wydarzenia !")
    }

    GetAjaxWithFunctions("/Home/AJAX_EditEvent", "POST", data, successFn, errorFn, ".custombox-content");
});

$(document).on('click', '.delete-event', function () {
    var ID = $(this).data('event-id');

    if (ID === undefined)
        ID = $(this).data("id");

    var successFn = function (data) {
        if (data.result === "true") {
            location.reload();
        }
        else {
            toastr.error("Wystąpił błąd podczas usuwania wydarzenia")
        }

    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania wydarzenia");

    }

    GetAjaxWithFunctions("/Home/DeleteEvent", "POST", { ID: ID }, successFn, errorFn, "");
});

$(document).on("click", "#add-event", function () {
    var id = $(this).data("id");

    LoadModalContent("#event-modal", "/Projects/Modal_AddEvent/" + id);
});

$(document).on('change', '#Apartments', function () {
    var apartmentId = $(this).val();

    $('.custombox-content #Tasks').html('');

    $.getJSON('/Home/TasksList?apartmentId=' + apartmentId, function (data) {

        if (data.length === 0) $('.custombox-content #TaskEventCreate').prop('disabled', true);
        else $('.custombox-content #TaskEventCreate').prop('disabled', false);

        $.each(data, function (index, value) {
            var option = '<option value="' + value.Id + '">' + value.ProductName;

            if (value.Sufix !== null)
                option += ' ' + value.Sufix;

            option += '</option>';

            $('.custombox-content #Tasks').append(option);
        });
    });
});

$(document).on('click', '#TaskEventCreate', function () {
    var taskId = $('.custombox-content #Tasks').val();
    var title = $('.custombox-content #TaskEventTitle').val();
    var start = $('.custombox-content #TaskEventStart').val();
    var end = $('.custombox-content #TaskEventEnd').val();
    var desc = $('.custombox-content #TaskEventDescription').val();
    var color = $('.custombox-content .radios .active input').val();
    var allDay = $('.custombox-content #TaskEventAllDay').is(':checked');
    var projectID = $(".custombox-content #ProjectID").val();

    var type = -1;
    var referenceTo = -1;

    if (taskId !== null) {
        type = 0;
        referenceTo = taskId;
    }
    else {
        type = 3;
        referenceTo = projectID;
    }

    $.ajax({
        url: '/Projects/AddEvent',
        dataType: 'json',
        type: 'POST',
        data: { Type: type, ReferenceTo: referenceTo, Title: title, Start: start, End: end, Description: desc, Color: color, Allday: allDay, ProjectID: projectID },
        success: function (data) {
            if (data.success === 'true') {
                toastr.success("Wydarzenie zostało utworzone!");
                location.reload();
            }
            else {
                toastr.error("Wystąpił błąd podczas tworzenia wydarzenia!");
            }

            Custombox.modal.close();
            ClearModal();
        },
        error: function (XMLHttpRequest) {
            toastr.error("Wystąpił błąd podczas tworzenia wydarzenia!");
            Custombox.modal.close();
            ClearModal();
        }
    });
});

$(document).on("change", ".comment-todo", function () {
    var id = $(this).data("id");

    var data = {
        ID: id
    };

    var successFn = function (data) {
        if (data.success)
            toastr.success("Pomyślnie oznaczono TODO");
        else
            toastr.error("Wystąpił błąd podczas oznaczania TODO");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas oznaczania TODO");
    }

    GetAjaxWithFunctions("/Projects/SetCommentToDo", "POST", data, successFn, errorFn, "");
});

$(document).on("click", "#generuj-xml-aktywnosci", function () {
    var successFn = function (data) {
        if (data.success)
            window.location = "/dist/AcitivityExcel/" + data.file;
        else
            toastr.error("Wystąpił błąd podczas generowania pliku excel!");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas generowania pliku excel!");
    }

    var id = $(this).data("apartment-id");
    var projectID = $(this).data("project-id");

    var data = {
        ApartmentID: id,
        ProjectID: projectID
    };

    GetAjaxWithFunctions("/Projects/ActivityExcel", "POST", data, successFn, errorFn, "");
});

$(document).on("change", "#tab-products #ProductId", function () {
    var price = Math.round(parseFloat($(this).find(":selected").data("price")));
    var p1 = Math.round(parseFloat($(this).find(":selected").data("p1")));
    var p2 = Math.round(parseFloat($(this).find(":selected").data("p2")));
    var name = $(this).find(":selected").data("name");
    var invoice = $(this).find(":selected").data("invoice");


    $("#ProductPrice").val(price);
    $("#PercentType1").val(p1);
    $("#PercentType2").val(p2);
    $("#product-name").val(name);
    $("#product-invoice").val(invoice);
});

$(document).on("click", ".edit-connect-product", function () {
    var connectID = $(this).data("connect-id");
    var price = $("#connect-price-" + connectID).data("price");
    var p1 = $("#connect-p1-" + connectID).data("p");
    var p2 = $("#connect-p2-" + connectID).data("p");
    const name = $(".connect-" + connectID).data("name");
    const invoice = $(".connect-" + connectID).data("invoice");
    const noExtra = $("#no-extra-" + connectID).data("no-extra");

    $("#ToEditConnectID").val(connectID);
    $("#ToEditPrice").val(price);
    $("#ToEditPercentType1").val(p1);
    $("#ToEditPercentType2").val(p2);
    $('#ToEditName').val(name);
    $('#ToEditInvoice').val(invoice);
    $('#ToEditNoExtra').prop('checked', noExtra);

    NewModal("#editprice-modal");
});;

$(document).on("click", ".save-connect-product-price", function () {
    var connectID = $(".custombox-content #ToEditConnectID").val();
    var price = $(".custombox-content #ToEditPrice").val();
    var p1 = $(".custombox-content #ToEditPercentType1").val();
    var p2 = $(".custombox-content #ToEditPercentType2").val();
    var name = $(".custombox-content #ToEditName").val();
    var invoice = $(".custombox-content #ToEditInvoice").val();
    var noExtra = $('.custombox-content #ToEditNoExtra').prop('checked');
    var type = $(".connect-" + connectID).data("type");

    var data = {
        ConnectID: connectID,
        Price: price,
        PercentType1: p1,
        PercentType2: p2,
        Name: name,
        OnInvoiceName: invoice,
        NoExtra: noExtra
    };

    var successFn = function (data) {
        if (data.success === true) {
            $("#connect-price-" + connectID).html(price + " kr");
            $("#connect-price-" + connectID).data("price", price);

            $("#connect-p1-" + connectID).html(p1 + "%");
            $("#connect-p1-" + connectID).data("p", p1);

            $("#connect-p2-" + connectID).html(p2 + "%");
            $("#connect-p2-" + connectID).data("p", p2);

            $("#no-extra-" + connectID).html(noExtra ? 'TAK' : 'NIE');
            $("#no-extra-" + connectID).data('no-extra', noExtra);

            console.log($("#connect-" + connectID).find(".product-name a").html());
            $(".connect-" + connectID).find(".product-name a").html(name + '(' + invoice + ') <small style="color: darkgray;">' + type + '</small>');
            $(".connect-" + connectID).data("name", name);

            Custombox.modal.close();
        }
        else {
            toastr.error("Wystąpił błąd podczas zapisywania ceny!");
            Custombox.modal.close();
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas zapisywania ceny!");
        Custombox.modal.close();
    }

    GetAjaxWithFunctions("/Projects/EditConnectProductPrice", "POST", data, successFn, errorFn, "");
});

$(document).on('click', '.add-place-error', function () {
    var projectID = $(this).data("project-id");
    var apartmentID = $(this).data("apartment-id");
    var taskID = $(this).data("task-id");
    var place = $(this).data("place");

    switch (place) {
        case "project":
            apartmentID = null;
            taskID = null;
            break;
        case "apartment":
            taskID = null;
            break;
        default:
            break;
    }

    LoadModalContent("#modal", "/Projects/Modal_AddPlaceError?projectID=" + projectID + "&apartmentID=" + apartmentID + "&taskID=" + taskID + "&place=" + place);
});

$(document).on("click", ".send-place-error-report", function () {
    var ID = $(this).data("id");

    $.get("/Projects/SendPlaceErrorReport/" + ID, function () { location.reload(); });
});

$(document).on("click", ".translate-place-error", function () {
    var id = $(this).data("id");

    var popup = window.open("/Popup/PlaceError?PlaceErrorID=" + id, "Popup", "width=600, height=600"
    );
    popup.focus();
});

$(document).on("click", ".delete-place-error", function () {
    var id = $(this).data("id");

    var successFn = function (data) {
        if (data.success) {
            $(".row-place-error-" + id).remove();
            toastr.success("Pomyślnie usunięto błąd.");
        }
        else
            toastr.error("Wystąpił błąd podczas usuwania błędu !");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania błędu !");
    }

    GetAjaxWithFunctions("/Projects/DeletePlaceError", "POST", { id }, successFn, errorFn, "#tab-place-errors");
});

$(document).on("click", ".row-place-error-expand", function () {
    var id = $(this).data("id");
    var expandIcon = $(this).children("i.fa");
    var expanded = expandIcon.hasClass("fa-minus-square");

    $(".row-place-error-details").hide();
    $(".row-place-error-expand i.fa").removeClass("fa-minus-square").addClass("fa-plus-square");

    if (expanded) {
        expandIcon.removeClass("fa-minus-square").addClass("fa-plus-square");
        $(".row-place-error-details-" + id).hide(300);
    }
    else {
        expandIcon.removeClass("fa-plus-square").addClass("fa-minus-square");
        $(".row-place-error-details-" + id).show(300);
    }
});

$(document).on("change", ".error-img", function () {
    var formData = new FormData();
    var totalFiles = this.files.length;
    var data = {};
    for (var i = 0; i < totalFiles; i++) {
        var file = this.files[i];
        formData.append("error-img", file);
    }

    $.ajax({
        type: "POST",
        url: "/Inventory/AJAX_AddInventoryFile",
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        beforeSend: function () {
            $("body").LoadingOverlay("show");
        },
        success: function (data) {
            if (data.result === "true") {
                var row = "<tr>";
                row += "<td>" + data.FileName + '<input type="hidden" name="_photos[]" value="' + data.URL + '" /></td>';
                row += '<td class="text-center"><button type="button" class="btn btn-xs btn-flat btn-danger delete-file" data-url="' + data.URL + '" data-id="' + data.ID + '"><i class="fa fa-trash"></i></button></td>'
                row += "</tr>";

                $(".error-files-table").append(row);

                toastr.success("Pomyślnie dodano plik !");
            }
            else
                toastr.error("Wystąpił błąd podczas wgrywania pliku");
        },
        error: function (error) {
            toastr.error("Wystąpił błąd podczas wgrywania pliku");
        },
        complete: function () {
            $("body").LoadingOverlay("hide", true);
        }
    });
});

$(document).on("click", ".delete-file-inventory", function () {
    var id = $(this).data("id");
    var url = $(this).data("url");
    var tr = $(this).closest("tr");

    var data = {
        ID: id,
        URL: url
    };

    var successFn = function (data) {
        if (data.success)
            tr.remove();
        else
            toastr.error("Wystąpił błąd podczas usuwania pliku !");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania pliku !");
    }

    GetAjaxWithFunctions("/Inventory/AJAX_DeleteFile", "POST", data, successFn, errorFn, tr);
});

$(document).on("click", "#delete-selected-project-product", function () {
    var ProjectProductIDs = [];
    $(".project-product-check:checked").each(function () {
        ProjectProductIDs.push($(this).val());
    });

    $.post("/Projects/AJAX_DeleteSelectedProjectProducts", { ProjectProductIDs }, function (data) {
        if (data === true) {
            location.reload();
        }
    });
});


function GetProjectManagers(customerId) {
    projectManagerId.html("");
    projectManagerId.prop("disabled", false);

    $.getJSON("/Projects/ProjectManagersList?customerId=" + customerId, function (data) {
        if (data.data.length === 0) {
            projectManagerId.prop("disabled", true);
        }

        $.each(data.data, function (index, value) {
            var option = '<option value="' + value.Id + '">' + value.Name + "</option>";
            projectManagerId.append(option);
        });
    });
}

function LoadProjectManagerData(id) {
    var modal = "#modal-project-manager";

    $.getJSON("/Projects/ProjectManagerById?id=" + id, function (data) {
        $(modal + " #pm-name").html(data.name);
        $(modal + " #pm-email").html(data.email);
        $(modal + " #pm-phonenumber").html(data.phonenumber);
    });
}

function CreateSerwisElementRow(elementName, element, elementStatus) {
    return '<tr class="' + elementStatus + '"><th>' + elementName + "</th>" +
                '<td><input type="text" class="form-control" name="' + element + '-desc" value=""/></td>'+
                '<td><input type="checkbox" name="'+element+'-check"/></td>' +
            "</tr>";
}

function GenerateMap() {
    var address = street.val() + ", " + city.val();

    //var link = "<iframe frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?&amp;q=" + address + "&amp;output=embed'></iframe>";

    //$("#apartment-map").html(link);

    //var link = "https://www.google.com/maps/dir/?api=1&origin=VALLDA, Szwecja&destination=" + address;

    //var win = window.open(link, '_blank');
    //win.focus();
}

function GetProduct(id) {
    ClearProductValues();

    var ProjectID = $("#ProjectID").val();

    var successFn = function (data) {

        var type1 = data.Type1 + "";
        var type2 = data.Type2 + "";
        var type3 = data.Type3 + "";
        var type4 = data.Type4 + "";
        var type5 = data.Type5 + "";
        var type6 = data.Type6 + "";
        $(".custombox-content #ProductPrice").val(data.Price);
        $(".custombox-content #PercentType1").val((type1 !== "null") ? type1.replace(".", ",") : 0);
        $(".custombox-content #PercentType2").val((type2 !== "null") ? type2.replace(".", ",") : 0);
        $(".custombox-content #PercentType3").val((type3 !== "null") ? type3.replace(".", ",") : 0);
        $(".custombox-content #PercentType4").val((type4 !== "null") ? type4.replace(".", ",") : 0);
        $(".custombox-content #PercentType5").val((type5 !== "null") ? type5.replace(".", ",") : 0);
        $(".custombox-content #PercentType6").val((type6 !== "null") ? type6.replace(".", ",") : 0);
        $(".custombox-content #product-jm").append(data.Jm);


        var productName = data.productName;

        CountPercents($(this).data("count-directly"));
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas aktualizacji danych o produkcie!!");
    }

    GetAjaxWithFunctions("/Products/GetProduct?id=" + id + "&projectID=" + ProjectID, "GET", null, successFn, errorFn, ".product-load");
}

function GetProduct_Questions(id) {
    $(".custombox-content #questions-accordion-container").html('');

    var ProjectID = $("#ProjectID").val();
    var TaskID = $(".custombox-content #Id").val();

    var data = {
        ProductID: id,
        ProjectID: ProjectID,
        TaskID: TaskID
    };

    var successFn = function (data) {
        $(".custombox-content #questions-accordion-container").html(data);
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas ładowania pytań dla wybranego produktu !");
    };

    GetAjaxWithFunctionsHtml("/Projects/GetPartial_Modal_AddTask_Questions", "POST", data, successFn, errorFn, ".custombox-content");
}

function ClearProductValues() {
    $(".custombox-content #ProductPrice").val("");
    $(".custombox-content #PercentType1").val("");
    $(".custombox-content #PercentType2").val("");
    $(".custombox-content #PercentType3").val("");
    $(".custombox-content #PercentType4").val("");
    $(".custombox-content #PercentType5").val("");
    $(".custombox-content #PercentType6").val("");
    $(".custombox-content #product-jm").html("");
}

function CountPercents(directly) {
    var custombox = (directly) ? "" : ".custombox-content ";

    var productPriceVal = $(custombox + "#ProductPrice").val();

    var productPrice = productPriceVal.replace(",", ".");
    var p1 = $(custombox + "#PercentType1").val().replace(",", ".");
    var p2 = $(custombox + "#PercentType2").val().replace(",", ".");
    var p3 = $(custombox + "#PercentType3").val().replace(",", ".");
    var p4 = $(custombox + "#PercentType4").val().replace(",", ".");
    var p5 = $(custombox + "#PercentType5").val().replace(",", ".");
    var p6 = $(custombox + "#PercentType6").val().replace(",", ".");

    var pt1 = Math.ceil(Number(productPrice) * (Number(p1) / 100));
    var pt2 = Math.ceil(Number(productPrice) * (Number(p2) / 100));
    var pt3 = Math.ceil(Number(productPrice) * (Number(p3) / 100));
    var pt4 = Math.ceil(Number(productPrice) * (Number(p4) / 100));
    var pt5 = Math.ceil(Number(productPrice) * (Number(p5) / 100));
    var pt6 = Math.ceil(Number(productPrice) * (Number(p6) / 100));

    $(".pt1").html(pt1 + " SEK");
    $(".pt2").html(pt2 + " SEK");
    $(".pt3").html(pt3 + " SEK");
    $(".pt4").html(pt4 + " SEK");
    $(".pt5").html(pt5 + " SEK");
    $(".pt6").html(pt6 + " SEK");
}

function ActivityWorkDoneEdit() {
    var settings = {
        id: 'field',
        name: 'value',
        type: 'text',
        cssclass: 'text-box single-line',
        tooltip: 'Edytuj',
        placeholder: 'Edytuj',
        indicator: 'Zapisywanie...',
        callback: function (value, settings) {
            var result = value.split(';')[0];
            var back = value.split(';')[1]

            $(this).html(back);

            if (result === 'true')
                toastr.success('Pomyślnie zmieniono ilość wykonanej pracy');
            else
                toastr.error(back);
        }
    }

    //$(".editable-activity").jeditable("/Projects/AJAX_EditableActivityWorkDone", settings);
}

function ConnectedProductPriceEdit() {
    //var settings = {
    //    id: 'field',
    //    name: 'value',
    //    type: 'text',
    //    cssclass: 'text-box single-line',
    //    tooltip: 'Edytuj',
    //    placeholder: 'Edytuj',
    //    indicator: 'Zapisywanie...',
    //    callback: function (value, settings) {
    //        var result = value.split(';')[0];
    //        var back = value.split(';')[1]

    //        $(this).html(back);

    //        if (result == 'true')
    //            toastr.success('Pomyślnie zmieniono cenę produktu.');
    //        else
    //            toastr.error(back);
    //    }
    //}

    //$(".editable-product-price").editable("/Projects/AJAX_EditableProductPrice", settings);

    var settings = {
        type: 'text',
        pk: $(this).data('id'),
        name: 'price',
        url: '/Projects/AJAX_EditableProductPrice',
        title: 'Wprowadź nową cenę'
    };

    $(".editable-product-price").editable(settings);
}


function DefineICheck() {


        $('input[type="checkbox"].minimal').iCheck({
            checkboxClass: "icheckbox_square-blue"
        });

        //$('input[type="checkbox"].minimal').iCheck("check");

}