﻿$(document).ready(function() {
    $(".select2").select2();
});

$(document).on("change", "#ProjectID", function() {
    var projectId = $(this).val();

    var successFn = function(data) {
        $("#project-content").html(data);
    }

    var errorFn = function() {
        toastr.error("Wystąpił błąd podczas generowania zestawienia.");
    }

    GetAjaxWithFunctionsHtml("/Parallel/Partial_ContentElements", "POST", { projectId: projectId }, successFn, errorFn, "");
})