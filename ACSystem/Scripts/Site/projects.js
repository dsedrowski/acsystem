﻿var tableId = '#projects-list';
GetTableFilterWebStorage("project", tableId);
var table = LoadTable(tableId);

var statusFilter = $('.status-filter');

$(document).ready(function () {
    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue',
    });

    //$('input[type="checkbox"].filter').iCheck('check');
    //$('input[type="checkbox"].is-checked').iCheck("uncheck");

    statusFilter.on('ifChanged', function () {
        ChangeFilter();
        DefaultPropTable();
        LoadTable(tableId);
    });
});

$(document).on('click', '.project-details', function () {
    location.assign('/Projects/Details/' + $(this).data('project-id'));
});

$(document).on('click', '.project-edit', function () {
    location.assign('/Projects/Edit/' + $(this).data('project-id'));
});

$(document).on('click', '.project-delete', function () {
    var id = $(this).data('project-id');

    var fn = function () {
        $.ajax({
            url: '/Projects/Delete',
            dataType: 'json',
            type: 'POST',
            data: { id: id },
            success: function (data) {
                if (data.success === 'true') {
                    location.reload();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            }
        });
    };

    Confirm("Czy na pewno chcesz usunąć projekt?", fn);
});

$(document).on('click', '.pagination a', function () {
    var page = $(this).data('page');

    if (page != null) {
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .prevBtn', function () {
    var page = $(tableId).data('page');;

    if (page != null) {
        page -= 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .nextBtn', function () {
    var page = $(tableId).data('page');;

    if (page != null) {
        page += 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '#search-table-btn', function () {
    $(table).html("");
    DefaultPropTable();
    LoadTable(tableId);
});

$("#search-table-input").on("keyup", function (event) {
    if (event.keyCode == 13) {
        $(table).html("");
        DefaultPropTable();
        LoadTable(tableId);
    }
});

$(document).on('click', tableId + ' th.clickable', function () {
    DefaultPropTable();
    $(tableId).data('order-by', $(this).data('id'))

    var currentDest = $(this).data('dest');

    $(tableId + ' th.clickable').data('dest', 'desc');

    if (currentDest === 'asc')
        $(this).data('dest', 'desc');
    if (currentDest === 'desc') $(this).data('dest', 'asc');

    $(tableId).data('dest', $(this).data('dest'));

    LoadTable(tableId);
});

$(document).on('click', '.go-to-customer', function () {
    var id = $(this).data('customer-id');

    location.assign('/Customers/Details/' + id);
});

$(document).on('click', '.close-modal', function () {
    Custombox.close();
})

$(document).on('click', '.copy-project', function () {
    var projectID = $(this).data('project-id');

    LoadModalContent("#modal", "/Projects/Modal_CopyProject?ProjectID=" + projectID);
});

$(document).on("click", "#synchronize-projects", function () {
    $.LoadingOverlay("show");
    $.get("/Fortnox/Project_Synchronize", function (data) {
        if (data.success)
            toastr.success(data.message);
        else
            toastr.error(data.message);

        if (data.reload)
            location.reload();

        $.LoadingOverlay("hide");
    });
});

function LoadTable(table) {
    var page = $(table).data('page');
    var dest = $(table).data('dest');
    var orderBy = $(table).data('order-by');
    var filter = $(table).data('filter');
    var search = $('#search-table-input').val();

    SetTableFilterWebStorage("project", page, dest, orderBy, filter, search);
    CreateSortingIcon(dest, orderBy);

    var successFn = function (data) {
        FillTable(data.data, table);
        GeneratePagination(data.pages, table);
    }

    var errorFn = function (XMLHttpRequest) {
        $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
    }

    var data = { page: page, dest: dest, orderBy: orderBy, search: search, filter: filter };

    GetAjaxWithFunctions("/Projects/ProjectsList", "GET", data, successFn, errorFn, table);
}

function FillTable(data, table) {
    var row = "";

    if (data != null)
    {
        $.each(data, function (index, value) {
            var systemID = (value.InSystemID != null) ? value.InSystemID : "";
            var isChecked = (value.IsChecked === true) ? '<span class="text-success"><i class="fa fa-check"></i></span>' : "";
            var background = 'style="background-color: #FFF"';

            if (value.ProjectEnd === true)
                background = 'style="background-color: #D46A6A; color: #FFF;"';
            else if (value.RemindBeforeEnd === true)
                background = 'style="background-color: #DC995F; color: #FFF;"';
            else if (value.IsChecked === true)
                background = 'style="background-color: #BAF9AA"';

            row += '<tr class="tr-clickable" ' + background + '> ' +
            '<td class="project-details" data-project-id="' + value.Id + '"><div style="width: 20px; height: 20px; background-color: ' + value.ColorHex +'"></div></td>' +
            '<td class="project-details" data-project-id="' + value.Id + '">' + systemID + '</td>' +
            '<td class="project-details" data-project-id="' + value.Id + '">' + value.ProjectNumber + ' ' + isChecked + '</td>' +
            '<td class="project-details" data-project-id="' + value.Id + '">' + ((value.Street != null) ? value.Street : "") + '</td>' +
            '<td class="project-details" data-project-id="' + value.Id + '">' + value.City + '</td>' +
            '<td class="project-details" data-project-id="' + value.Id + '">' + value.ApartmentsCount + '</td>' +
            '<td  class="project-details with-label" data-project-id="' + value.Id + '">' + CreateLabel(value.Status) + '</td>' +
            '<td class="project-details" data-project-id="' + value.Id + '">' + GoToCustomer(value.CustomerId, value.Customer) + '</td>' +
            '<td>' + GetCrudHtml(value.Id, value.CanDelete) + '</td>' +
            '</tr>';
        });
    }

    $(table + ' tbody').html(row);
}

function DefaultPropTable() {
    $(tableId).data('page', 1);
    $(tableId).data('dest', 'asc');
    $(tableId).data('order-by', 'projectNumber');
}

function GetCrudHtml(id, status) {
    var toret = '<div class="crud-container">' +
                '<a class="crud crud-details" href="/Projects/Details/' + id + '" data-toggle="tooltip", data-placement="bottom" title="Szczegóły"><i class="glyphicon glyphicon-eye-open"></i></a>' +
        '<a class="crud crud-edit" href="/Projects/Edit/' + id + '" data-toggle="tooltip", data-placement="bottom" title="Edytuj"><i class="glyphicon glyphicon-pencil"></i></a>' +
        '<span data-toggle="tooltip" data-placement="bottom" title="Kopiuj" data-project-id="' + id + '" class="crud crud-copy copy-project"><i class="glyphicon glyphicon-copy"></i></span>';
    if (status === true){
        toret += '<span class="crud crud-delete project-delete" data-project-id="' + id + '" data-toggle="tooltip", data-placement="bottom" title="Usuń"><i class="glyphicon glyphicon-remove"></i></span>' ;
    }
        toret += '</div>';

    return toret;
}

function GeneratePagination(pages, table) {
    var currentPage = $(table).data('page');

    var prevBtn = '<li><a href="#" class="prevBtn">«</a></li>';
    var nextBtn = '<li><a href="#" class="nextBtn">»</a></li>';

    var pagination = '';

    if (pages > 1 && currentPage > 1)
        pagination += prevBtn;

    for (var i = 1; i <= pages; i++) {
        if (i == currentPage)
            pagination += '<li class="active"><a href="#">' + i + '</a></li>';
        else
            pagination += '<li><a href="#" data-page="' + i + '">' + i + '</a></li>';
    }

    if (pages > 1 && currentPage < pages)
        pagination += nextBtn;

    $('.pagination').html(pagination);
}

function CreateSortingIcon(dest, orderBy) {
    var iconAsc = '<i class="glyphicon glyphicon-sort-by-alphabet sort-column-icon"></i>';
    var iconDesc = '<i class="glyphicon glyphicon-sort-by-alphabet-alt sort-column-icon"></i>'

    $(tableId + ' thead tr th i').remove();

    if (orderBy === 'inSystemID' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconAsc);
    else if (orderBy === 'inSystemID' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconDesc);
    if (orderBy === 'projectNumber' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconAsc);
    else if (orderBy === 'projectNumber' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconDesc);
    else if (orderBy === 'street' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(4)').append(iconAsc);
    else if (orderBy === 'street' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(4)').append(iconDesc);
    else if (orderBy === 'city' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(6)').append(iconAsc);
    else if (orderBy === 'city' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(6)').append(iconDesc);
    else if (orderBy === 'status' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(7)').append(iconAsc);
    else if (orderBy === 'status' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(7)').append(iconDesc);
    else if (orderBy === 'customer' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(8)').append(iconAsc);
    else if (orderBy === 'customer' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(8)').append(iconDesc);
}

function CreateLabel(status) {
    if (status === 1)
        return '<span class="label label-default">Nie rozpoczęte</span>';

    if (status === 0)
        return '<span class="label label-warning">W trakcie</span>';

    if (status === 3)
        return '<span class="label label-success">Zakończone</span>';

    if (status === 2)
        return '<span class="label label-danger">Zakończone z błędami</span>';
}

function ChangeFilter() {
    var isFirst = true;
    var filter = "";

    var checkbox1 = $('#fltr-chbx-1').prop('checked');
    var checkbox2 = $('#fltr-chbx-2').prop('checked');
    var checkbox3 = $('#fltr-chbx-3').prop('checked');
    var checkbox4 = $('#fltr-chbx-4').prop('checked');
    var checkbox5 = $('#fltr-chbx-5').prop('checked');

    if (checkbox1 === true) {
        filter += "1";
        isFirst = false;
    }

    if (checkbox2 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 0;
        isFirst = false;
    }

    if (checkbox3 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 3;
        isFirst = false;
    }

    if (checkbox4 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 2;
        isFirst = false;
    }

    if (checkbox5 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 4;
        isFirst = false;
    }

    $(tableId).data('filter', filter);
}

function GoToCustomer(id, name) {
    return '<span class="td-clickable go-to-customer" data-customer-id="' + id + '">' + name + '</span>';
}

function Projects_SetWebStorage(page, dest, orderby, filter, search) {

}
