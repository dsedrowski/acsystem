﻿$(document).ready(function () {
    $("#PostalCode").inputmask("99-999", { "placeholder": "___ __" });
    //$("#VATNumber").inputmask("999-999-99-99", { "placeholder": "___-___-__-__" });
    //$("#PhoneNumber").inputmask("999-999-999", { "placeholder": "___-___-___" });


    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue'
    });

    $('input[type="checkbox"].minimal').iCheck('check');
});

$(document).on('click', '.project-details', function () {
    location.assign('/Projects/Details/' + $(this).data('project-id'));
});

$(document).on('click', '.manager-add', function () {
    new Custombox.modal({
        content: {
            effect: 'fadein',
            target: '#modal',
            animateFrom: 'top',
            animateTo: 'bottom',
            positionX: 'center',
            positionY: 'center',
            fullscreen: false
        },
        overlay: {
            active: 0.5
        }
    }).open();
});

$(document).on('click', '.manager-edit', function () {

    var id = $(this).data('manager-id');


    new Custombox.modal({
        content: {
            effect: 'fadein',
            target: '#modal-edit',
            animateFrom: 'top',
            animateTo: 'bottom',
            positionX: 'center',
            positionY: 'center',
            fullscreen: false
        },
        overlay: {
            active: 0.5
        },
        onClose: ClearFields()
    }).open();


    $.getJSON('/Customers/ProjectManagerById/' + id, function (data) {
        $('.custombox-content #Id').val(data.Id);
        $('.custombox-content #Name').val(data.Name);
        $('.custombox-content #Email').val(data.Email);
        $('.custombox-content #PhoneNumber').val(data.PhoneNumber);
    });

});

$(document).on('click', '.manager-delete', function () {
    var id = $(this).data('manager-id');

    var fn = function () {
        $.ajax({
            url: '/Customers/ProjectManagerDelete',
            dataType: 'json',
            type: 'GET',
            data: { id: id },
            success: function (data) {
                if (data.success === 'true') {
                    location.reload();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            }
        });
    };

    Confirm("Czy na pewno chcesz usunąć menedżera projektu?", fn);
});

$(document).on('click', '.close-modal', function () {
    CustomBoxClose();
});

$(document).on('change', '.block-pm', function () {
    var pmID = $(this).data("pm-id");

    var locked = $(this).is(":checked");

    var successFn = function (data) {
        if (data.success === true)
            toastr.success("Status menedżera został poprawnie zmieniony !");
        else
            toastr.error("Wystąpił błąd podczas zmiany statusu menedżera");
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas zmiany statusu menedżera");
    };

    var data = { ID: pmID, Locked: locked };

    GetAjaxWithFunctions("/Customers/LockManager", "POST", data, successFn, errorFn, "");
});

function CustomBoxClose() {
    ClearFields();

    Custombox.close();
}

function ClearFields() {
    $('#modal #Name').val('');
    $('#modal #Email').val('');
    $('#modal #PhoneNumber').val('');

    $('#modal-edit #Id').val('');
    $('#modal-edit #Name').val('');
    $('#modal-edit #Email').val('');
    $('#modal-edit #PhoneNumber').val('');
}