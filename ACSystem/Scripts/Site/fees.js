﻿var tableId = '#fees-list';
var table = LoadTable(tableId);

var statusFilter = $('.status-filter');

$(document).ready(function () {
    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue',
    });

    $('input[type="checkbox"].minimal').iCheck('check');

    statusFilter.on('ifChanged', function () {
        ChangeFilter();
        LoadTable(tableId);
    });
});

$(document).on('click', '.add-fee', function () {
    LoadModalContent("#modal", "/Office/Modal_AddFee");
});

$(document).on('click', '.edit-fee', function () {
    var id = $(this).data('fee-id');

    LoadModalContent("#modal", "/Office/Modal_EditFee/" + id);
});

$(document).on('click', '.fee-details', function () {
    var id = $(this).data('fee-id');

    LoadModalContent("#modal", "/Office/Modal_DetailsFee/" + id);
});

$(document).on('click', '.delete-fee', function () {
    var id = $(this).data('fee-id');

    var fn = function () {
        $.ajax({
            url: '/Office/FeeDelete',
            dataType: 'json',
            type: 'GET',
            data: { id: id },
            success: function (data) {
                if (data.success === 'true') {
                    location.reload();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            }
        });
    };

    Confirm("Czy na pewno chcesz usunąć szkodę?", fn);
});

$(document).on('change', '.project-select', function () {
    var id = $(this).val();

    LoadApartments(id);
});

$(document).on('change', '.apartment-select', function () {
    var id = $(this).val();

    LoadTasks(id);
});

$(document).on('change', '#profile-img', function () {
        var formData = new FormData();
        var totalFiles = this.files.length;
        for (var i = 0; i < totalFiles; i++) {
            var file = this.files[i];

            formData.append("profile-img", file);
        }

        $.ajax({
            type: "POST",
            url: '/Office/UploadImage',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.result === 'true') {
                    toastr.success("Dokument zostało dodany!");
                    $('.custombox-content #Images').append('<input type="hidden" name="FeeFile[]" value="' + response.newFileName + '" />');
                    $('.photo-names').append('<h4 class="photo-name">' + response.fileName + '</h4>');
                }
                else {
                    toastr.error(response.message);
                }
            },
            error: function (error) {
                toastr.error(error);
            }
        });
    });

$(document).on('click', '#search-table-btn', function () {
    DefaultPropTable();
    LoadTable(tableId);
});

$("#search-table-input").on("keyup", function (event) {
    if (event.keyCode == 13) {
        $(table).html("");
        DefaultPropTable();
        LoadTable(tableId);
    }
});

$(document).on('click', tableId + ' th.clickable', function () {
    DefaultPropTable();
    $(tableId).data('order-by', $(this).data('id'))

    var currentDest = $(this).data('dest');

    $(tableId + ' th.clickable').data('dest', 'desc');

    if (currentDest === 'asc')
        $(this).data('dest', 'desc');
    if (currentDest === 'desc') $(this).data('dest', 'asc');

    $(tableId).data('dest', $(this).data('dest'));

    LoadTable(tableId);
});

$(document).on('click', '.pagination a', function () {
    var page = $(this).data('page');

    if (page != null) {
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .prevBtn', function () {
    var page = $(tableId).data('page');;

    if (page != null) {
        page -= 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .nextBtn', function () {
    var page = $(tableId).data('page');;

    if (page != null) {
        page += 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

function LoadApartments(id) {
    $('.apartment-select').html("");
    $('.task-select').html("");

    var apartmentsFn = function (data) {

        var options = "<option></option>";
        var first = true;

        $.each(data.data, function (key, value) {
            if (first === true)
                LoadTasks(value.ID);

            options += '<option value="' + value.ID + '">' + value.ApartmentNumber + '</option>';

            first = false;
        });

        $('.apartment-select').html(options);
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas ładowania apartamentów!");
    }

    GetAjaxWithFunctions('/Office/GetApartments/' + id, "GET", null, apartmentsFn, errorFn, '.changeable');
}

function LoadTasks(id) {
    $('.task-select').html("");

    var tasksFn = function (data) {

        var options = "<option></option>";

        $.each(data.data, function (key, value) {
            options += '<option value="' + value.ID + '">' + value.ProductName + '</option>';
        });

        $('.task-select').html(options);
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas ładowania zadań!");
    }

    GetAjaxWithFunctions('/Office/GetTasks/' + id, "GET", null, tasksFn, errorFn, '.changeable');
}

function LoadTable(table) {
    var page = $(table).data('page');
    var dest = $(table).data('dest');
    var orderBy = $(table).data('order-by');
    var filter = $(table).data('filter');
    var search = $('#search-table-input').val();

    CreateSortingIcon(dest, orderBy);

    var successFn = function (data) {
        FillTable(data.data, table);
        GeneratePagination(data.pages, table);
    };

    var errorFn = function () {
        $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.')
    };

    var ajaxData = { page: page, dest: dest, orderBy: orderBy, search: search, filter: filter }

    GetAjaxWithFunctions('/Office/FeesList', "GET", ajaxData, successFn, errorFn, tableId);
}

function FillTable(data, table) {
    var row = "";

    if (data != null) {
        $.each(data, function (index, value) {
            var apartment = (value.ApartmentNumber != null) ? value.ApartmentNumber : "";
            var task = (value.ProductName != null) ? value.ProductName : "";
            var project = (value.ProjectNumber != null) ? value.ProjectNumber : "";

            row += '<tr class="tr-clickable" data-toggle="tooltip" data-placement="bottom" title="Dodaj">' +
            '<td class="fee-details" data-fee-id="' + value.ID + '">' + value.FeeNo + '</td>' +
            '<td class="fee-details" data-fee-id="' + value.ID + '">' + value.FitterName + '</td>' +
                '<td class="fee-details" data-fee-id="' + value.ID + '">' + project + '</td>' +
                '<td class="fee-details" data-fee-id="' + value.ID + '">' + apartment + '</td>' +
                '<td class="fee-details" data-fee-id="' + value.ID + '">' + task + '</td>' +
            '<td class="fee-details" data-fee-id="' + value.ID + '">' + value.FeeDateConverted + '</td>' +
            '<td class="fee-details" data-fee-id="' + value.ID + '">' + value.TakeDateConverted + '</td>' +
            '<td class="fee-details" data-fee-id="' + value.ID + '">' + value.FeeAmount + '</td>' +
            '<td  class="fee-details with-label" data-fee-id="' + value.ID + '">' + CreateLabel(value.Status) + '</td>' +
            '<td class="fee-details" data-fee-id="' + value.ID + '"><a href="/Office/PayrollDetails/' + value.PayrollID + '">' + value.PayrolNo + '</a></td>' +
            '<td>' + GetCrudHtml(value.ID) + '</td>' +
            '</tr>';
        });
    }

    $(table + ' tbody').html(row);
}

function CreateLabel(status) {
    if (status === 0)
        return '<span class="label label-warning">Do potrącenia</span>';

    if (status === 1)
        return '<span class="label label-success">Potrącone</span>';
}

function GetCrudHtml(id) {
    return '<div class="crud-container">' +
                '<span class="crud crud-details fee-details" data-fee-id="' + id + '" data-toggle="tooltip", data-placement="bottom" title="Szczegóły"><i class="glyphicon glyphicon-eye-open"></i></span>' +
                '<span class="crud crud-edit edit-fee" data-fee-id="' + id + '" data-toggle="tooltip", data-placement="bottom" title="Edytuj"><i class="glyphicon glyphicon-pencil"></i></span>' +
                '<span class="crud crud-delete delete-fee" data-fee-id="' + id + '" data-toggle="tooltip", data-placement="bottom" title="Usuń"><i class="glyphicon glyphicon-remove"></i></span>' +
            '</div>';
}

function GeneratePagination(pages, table) {
    var currentPage = $(table).data('page');

    var prevBtn = '<li><a href="#" class="prevBtn">«</a></li>';
    var nextBtn = '<li><a href="#" class="nextBtn">»</a></li>'

    var pagination = '';

    if (pages > 1 && currentPage > 1)
        pagination += prevBtn;

    for (var i = 1; i <= pages; i++) {
        if (i === currentPage)
            pagination += '<li class="active"><a href="#">' + i + '</a></li>';
        else
            pagination += '<li><a href="#" data-page="' + i + '">' + i + '</a></li>';
    }

    if (pages > 1 && currentPage < pages)
        pagination += nextBtn;

    $('.pagination').html(pagination);
}

function CreateSortingIcon(dest, orderBy) {
    var iconAsc = '<i class="glyphicon glyphicon-sort-by-alphabet sort-column-icon"></i>';
    var iconDesc = '<i class="glyphicon glyphicon-sort-by-alphabet-alt sort-column-icon"></i>'

    $(tableId + ' thead tr th i').remove();

    if (orderBy === 'number' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconAsc);
    else if (orderBy === 'number' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconDesc);
    else if (orderBy === 'fitter' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconAsc);
    else if (orderBy === 'fitter' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconDesc);
    else if (orderBy === 'project' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconAsc);
    else if (orderBy === 'project' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconDesc);
    else if (orderBy === 'apartment' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(4)').append(iconAsc);
    else if (orderBy === 'apartment' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(4)').append(iconDesc);
    else if (orderBy === 'product' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(5)').append(iconAsc);
    else if (orderBy === 'product' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(5)').append(iconDesc);
    else if (orderBy === 'feedate' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(6)').append(iconAsc);
    else if (orderBy === 'feedate' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(6)').append(iconDesc);
    else if (orderBy === 'offdate' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(7)').append(iconAsc);
    else if (orderBy === 'offdate' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(7)').append(iconDesc);
    else if (orderBy === 'amount' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(8)').append(iconAsc);
    else if (orderBy === 'amount' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(8)').append(iconDesc);
    else if (orderBy === 'status' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(9)').append(iconAsc);
    else if (orderBy === 'status' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(9)').append(iconDesc);
    else if (orderBy === 'payroll' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(10)').append(iconAsc);
    else if (orderBy === 'payroll' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(10)').append(iconDesc);
}

function ChangeFilter() {
    var isFirst = true;
    var filter = "";

    var checkbox1 = $('#fltr-chbx-1').prop('checked');
    var checkbox2 = $('#fltr-chbx-2').prop('checked');

    if (checkbox1 === true) {
        filter += "0";
        isFirst = false;
    }

    if (checkbox2 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 1;
        isFirst = false;
    }

    $(tableId).data('filter', filter);
}

function DefaultPropTable() {
    $(tableId).data('page', 1);
    $(tableId).data('dest', 'desc');
    $(tableId).data('order-by', 'feedate');
}