﻿var tableId = '#projects-list';
var table = LoadTable(tableId);

var statusFilter = $('.status-filter');

$(document).ready(function () {
    var filter = localStorage.getItem("todo-filter");
    $("#filter-type").val(filter);
    var href = "/projects/tasktodo?type=" + filter;

    if (window.location.href.includes(href) === false)
        window.location.href = href;
})

$(document).on('click', '.todo-action-Wiadomosc', function () {
    var ID = $(this).data('id');

    GetAjaxLoadModal("/Projects/ShowTodoMessage?ID=" + ID, '#modal-todo-messaage');
});

$(document).on('click', '.assignTodo', function () {
    var ID = $(this).data('id');

    GetAjaxLoadModal("/Projects/Modal_AssignTodoAdmin?ID=" + ID, '#modal-todo-assign');
});

$('#usersList').click(function () {
    console.log($('.users-list'));

})

$(document).on('click', '.saveAssign', function (e) {
    var adminId = $('.custombox-content .users-list').val();
    var todo = $('#todoValue').data('todoid');
    //data = new {
    //    todoId: todo,
    //    adminId: adminId
    //};
    //var succFn = function () {
    //    toastr.success("Success!");
    //}
    //var errorFn = function () {
    //    toastr.error('Wystąpił błąd');
    //}

        //GetAjax("/Projects/AssignTodoAdmin", 'json', data, succFn, errorFn, "");

    $.ajax({
        url: "/Projects/AssignTodoAdmin",
        type: 'GET',
        data: {
            todoId: todo,
            adminId: adminId
        },
        success: function () {
            toastr.success("Pomyślnie przypisano użytkownika.");
            setTimeout(function () {
                window.location.reload();
            });
            },
        error: function (XMLHttpRequest) {
            $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
        }
    });

});

$(document).on('click', '.newTodo', function () {
    GetAjaxLoadModal("/Projects/Modal_NewTodo", '#modal-add-new-todo');
});

$(document).on("click", ".saveNewTodo", function () {
    var assignedUserid = $('.custombox-content .todo-users').val();
    var message = $('.custombox-content .todo-message').val();

    $.ajax({
        url: "/Projects/NewTodo",
        type: 'get',
        data: {
            assignedUserid: assignedUserid,
            message: message
        },
        success: function () {
                toastr.success("pomyślnie dodano todo");
            setTimeout(function () {
                window.location.reload();
            });

        },
        error: function (xmlhttprequest) {
            $.alert('błąd ' + xmlhttprequest.status + '. proszę spróbować jeszcze raz.');
        }
    });
})

$(document).on("click", ".check-todo", function () {
    var todoID = $(this).data('id');

    $.ajax({
        url: "/Projects/CheckTodo",
        type: 'get',
        data: {
            todoID: todoID
        },
        success: function (data) {
            console.log('data' + data);
            toastr.success("pomyślnie dodano todo");
            location.reload();

        },
        error: function (xmlhttprequest) {
            $.alert('błąd ' + xmlhttprequest.status + '. proszę spróbować jeszcze raz.');
        }
    });
})

$(document).on("click", "#save-todo-message", function () {
    console.log($('.custombox-content #message-text'));
    var todoID = $('.custombox-content #message-text').data('id');
    var message = $('.custombox-content #message-text').val();

    $.ajax({
        url: "/Projects/SaveTodoMessage",
        type: 'get',
        data: {
            todoID: todoID,
            MessageContent: message
        },
        success: function (data) {
            console.log('data' + data);
            toastr.success("Pomyślnie dodano todo");
            setTimeout(function () {
                window.location.reload();
            });
        },
        error: function (xmlhttprequest) {
            $.alert('błąd ' + xmlhttprequest.status + '. proszę spróbować jeszcze raz.');
        }
    });
})


$(document).on("click", ".todo-action-Akceptacja", function () {
    var id = $(this).data("id");
    var tr = $(this).closest("tr");

    $.ajax({
        url: '/Projects/TaskAcceptFromTodo',
        dataType: 'json',
        type: 'POST',
        data: { ID: id },
        success: function (data) {
            if (data.success === true) {
                //LoadTable(tableId);
                tr.remove();
                toastr.success("Pomyślnie zaakceptowano zadanie.");
                $(".tooltip").remove();
            }
        },
        error: function (XMLHttpRequest) {
            $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
        }
    });
})


$(document).on('change', '#filter-type', function () {
    type = $("#filter-type").find(":selected").data("type");
    localStorage.setItem("todo-filter", type);
    window.location.href = "/projects/tasktodo?type=" + type;
});

$(document).on('click', '.filter-by', function () {
    const arg = $(this).data('col');
    var dest;
    console.log($(this).find("i").hasClass("fa-caret-down"));
    if ($(this).find("i").hasClass("fa-caret-down")) {
        dest = "asc";
    } else {
        dest = "desc";
    }
    window.location.href = "/Projects/TaskTodo?filter=" + arg + "&dest=" + dest;
})

$(document).ready(function () {
    $('.users-list').select2();

    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue',
    });
    $('input[type="checkbox"].minimal').iCheck('check');
    statusFilter.on('ifChanged', function () {
        ChangeFilter();
        LoadTable(tableId);
    });

    console.log($('#filter-type').data('last-selected'));
    const lastType = $('#filter-type').data('last-selected') != undefined ? $('#filter-type').data('last-selected') : "";
    console.log(lastType);
    $('#filter-type option[data-type~="' + lastType + '"]').attr('selected', true);
});

$(document).on('click', '.pagination a', function () {
    var page = $(this).data('page');

    if (page != null) {
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .prevBtn', function () {
    var page = $(tableId).data('page');;

    if (page != null) {
        page -= 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .nextBtn', function () {
    var page = $(tableId).data('page');;

    if (page != null) {
        page += 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '#search-table-btn', function () {
    DefaultPropTable();
    LoadTable(tableId);
});

$(document).on('click', tableId + ' th.clickable', function () {
    DefaultPropTable();
    $(tableId).data('order-by', $(this).data('id'))

    var currentDest = $(this).data('dest');

    $(tableId + ' th.clickable').data('dest', 'desc');

    if (currentDest === 'asc')
        $(this).data('dest', 'desc');
    if (currentDest === 'desc') $(this).data('dest', 'asc');

    $(tableId).data('dest', $(this).data('dest'));

    LoadTable(tableId);
});

$(document).on('click', '.todo-check', function () {
    var id = $(this).data('id');
    var tr = $(this).closest("tr");

        $.ajax({
            url: '/Projects/TodoCheck',
            dataType: 'json',
            type: 'POST',
            data: { ID: id },
            success: function (data) {
                if (data.success === true) {
                    //LoadTable(tableId);
                    tr.remove();
                    console.log(tr);
                    toastr.success("Pomyślnie odznaczono powiadomienie.");
                    $(".tooltip").remove();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            }
        });
});

$(document).on("click", ".todo-action-Akceptacja", function () {
    var id = $(this).data("id");
    var tr = $(this).closest("tr");

    $.ajax({
        url: '/Projects/TaskAcceptFromTodo',
        dataType: 'json',
        type: 'POST',
        data: { ID: id },
        success: function (data) {
            if (data.success === 'true') {
                tr.remove();
                toastr.success("Pomyślnie zaakceptowano zadanie.");
                $(".tooltip").remove();
            }
        },
        error: function (XMLHttpRequest) {
            $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
        }
    });
});

$(document).on('click', '.todo-action-ZatrzymaneZadanie', function () {
    var id = $(this).data('id');
    var tr = $(this).closest('tr');

    $.ajax({
        url: '/Projects/MakeFillTaskFromTodo',
        dataType: 'json',
        type: 'POST',
        data: { ID: id },
        success: function (data) {
            if (data.success === true) {
                tr.remove();
                toastr.success("Pomyślnie utworzono zadanie uzupełniające.");
                $(".tooltip").remove();
            }
        },
        error: function (XMLHttpRequest) {
            $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
        }
    });
});

$(document).on('click', ".assign-user-option", function () {
    var id = $(this).data("id");
    var user = $(this).data("user");

    $.ajax({
        url: '/Projects/TaskAcceptFromTodo',
        dataType: 'json',
        type: 'GET',
        data: {
            ID: id,
            User: user
        },
        success: function (data) {
            if (data.success == 'true') {
                LoadTable(tableId);
                toastr.success("Pomyślnie zaakceptowano zadanie.");
            }
        },
        error: function (XMLHttpRequest) {
            $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
        }
    });
})

$(document).on('click', '.task-details', function () {
    var id = $(this).data('task-id');

    LoadModalContent("#modal", "/Projects/Modal_DetailsTask/" + id);
});

$(document).on("click", ".send-raport", function () {
    var id = $(this).data("task-id");

    var successFn = function (data) {
        if (data.success === true) {
            toastr.success("Pomyślnie wysłano raport");
            LoadTable(tableId);
            $(".tooltip").remove();
        }
        else {
            toastr.error("Wystąpił błąd podczas wysyłania raportu");
        }
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas wysyłania raportu");
    }

    GetAjaxWithFunctions("/Projects/SendRaport", "GET", { ID: id }, successFn, errorFn, '');
})

$(document).on('click', '[data-index]', function () {
    var index = $(this).data('index');

    new Custombox.modal({
        content: {
            effect: 'fadein',
            target: '#modal-photos-' + index,
            animateFrom: 'top',
            animateTo: 'bottom',
            positionX: 'center',
            positionY: 'center',
        },
        overlay: {
            active: 0.5
        }
    }).open();
})

$(document).on('click', '.activity-photos', function () {
    var id = $(this).data('activity-id');

    $('.modal-photos #photos').html('');

    $.getJSON('/Projects/ActivityPhotos?id=' + id, function (data) {
        $.each(data.photos, function (index, value) {
            var photo = '<div class="col-md-4 col-sm-6 co-xs-12 gal-item photo-' + value.ID + '">' +
                '<div class="box">' +
                '<a href="#" data-index="' + index + '">' +
                '<img src="' + value.Photo + '">' +
                '</a>' +
                '<div id="modal-photos-' + index + '" class="modal-photos modal-photos-multiple">' +
                '<a href="javascript:void(0);" onclick="Custombox.modal.close();" class="demo-close"><i class="fa fa-times"></i></a>' +
                '<button type="button" class="btn btn-outline btn-info demo-close-btn" onclick="Custombox.modal.close();">Close</button>' +
                '<div class="modal-content">' +
                '<div class="modal-body">' +
                '<img src="' + value.Photo + '" class="img-responsive">' +
                '</div>' +
                '<div class="col-md-12 description">' +
                '<h4>' + value.Desc + '</h4>' +
                '<button type="button" data-photo-id="' + value.ID + '" class="btn btn-danger delete-photo" onclick="Custombox.modal.close();">Usuń</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                //'<div class="modal fade" id="2" tabindex="-1" role="dialog">' +
                //    '<div class="modal-dialog">' +
                //        '<div class="modal-content">' +
                //            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>' +
                //            '<div class="modal-body">' +
                //                '<img src="' + value.Photo + '">' +
                //            '</div>' +
                //            '<div class="col-md-12 description">' +
                //                '<h4>'+ value.Desc +'</h4>' +
                //            '</div>' +
                //        '</div>' +
                //    '</div>' +
                //'</div>' +
                '</div>' +
                '</div>';

            $('.modal-photos #photos').append(photo);
        });
    });

    new Custombox.modal({
        content: {
            effect: 'fadein',
            target: '#modal-photos',
            animateFrom: 'top',
            animateTo: 'bottom',
            positionX: 'center',
            positionY: 'top',
            fullscreen: true
        },
        overlay: {
            active: 0.5
        }
    }).open();
});

$(document).on('click', '.delete-photo', function () {
    var photoID = $(this).data('photo-id');

    $.getJSON('/Projects/DeleteActivityPhoto?id=' + photoID, function (data) {
        if (data.success === true) {
            $('.photo-' + photoID).remove();
        }
        else {
            toastr.error("Wystąpił błąd podczas usuwania zdjęcia");
        }
    })
})

$(document).on('click', '.question-photo', function () {
    var id = $(this).data('task-id');
    var question = $(this).data('question-type');

    $('.modal-photos #photos').html('');

    $.getJSON('/Projects/QuestionPhoto?id=' + id + '&question=' + question, function (data) {
        $.each(data.photos, function (index, value) {
            var translated = (value.Translated != null) ? value.Translated : "";
            var photo = '<div class="col-md-4 col-sm-6 co-xs-12 gal-item">' +
                '<div class="box">' +
                '<a href="#" data-index="' + index + '">' +
                '<img src="' + value.Photo + '">' +
                '</a>' +
                '<div id="modal-photos-' + index + '" class="modal-photos modal-photos-multiple">' +
                '<a href="javascript:void(0);" onclick="Custombox.modal.close();" class="demo-close"><i class="fa fa-times"></i></a>' +
                '<button type="button" class="btn btn-outline btn-info demo-close-btn" onclick="Custombox.modal.close();">Close</button>' +
                '<div class="modal-content">' +
                '<div class="modal-body">' +
                '<img src="' + value.Photo + '" class="img-responsive">' +
                '</div>' +
                '<div class="col-md-12 description">' +
                '<h4>' + value.Desc + '</h4>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<h4>' + value.Desc + '</h4>' +
                '<div class="input-group">' +
                '<input type="text" class="question-photo-' + value.ID + ' form-control" value="' + translated + '" />' +
                '<span class="input-group-btn">' +
                '<button class="btn btn-primary save-question-translate" type="button" data-photo-id="' + value.ID + '">Zapisz</button>' +
                '</span>' +
                '</div>' +
                '</div>';

            $('.modal-photos #photos').append(photo);
        });
    });

    new Custombox.modal({
        content: {
            effect: 'fadein',
            target: '#modal-photos',
            animateFrom: 'top',
            animateTo: 'bottom',
            positionX: 'center',
            positionY: 'top',
            fullscreen: true
        },
        overlay: {
            active: 0.5
        }
    }).open();
});

$(document).on('click', '.save-question-translate', function () {
    var photoID = $(this).data('photo-id');

    var translated = $('.custombox-content .question-photo-' + photoID).val();

    $.getJSON('/Projects/SaveQuestionTranslate?ID=' + photoID + '&translated=' + translated,
        function(data) {
            if (data.success === true) {
                toastr.success("Zapisano tłumaczenie opisu zdjęcia");
            } else {
                toastr.error("Wystąpił błąd podczas zapisywania tłumaczenia opisu zdjęcia");
            }
        });
});

$(document).on('click', '.task-photos', function () {
    var id = $(this).data("task-id");

    LoadModalContent("#modal-all-photos", "/Projects/Modal_TaskPhotosAll?ID=" + id);
});

$(document).on("click", ".todo-action-TlumaczeniaPytanKontrolnych", function () {
    var id = $(this).data("id");

    var popup = window.open("/Popup/Translates/" + id, "Popup", "width=600,height=600");
    popup.focus();
})

$(document).on("click", ".todo-action-NowyKomentarz", function () {
    var id = $(this).data("id");

    var popup = window.open("/Popup/Comments/" + id, "Popup", "width=600,height=600");
    popup.focus();
})

$(document).on("click", ".todo-action-NowyBlad", function () {
    var id = $(this).data("place-error-id");
    var todoID = $(this).data("id");

    var popup = window.open("/Popup/PlaceError?PlaceErrorID=" + id + "&TodoID=" + todoID, "Popup", "width=600, height=600");
    popup.focus();
})

$("#add-file").click(function () {
    var id = $(this).data("id");

    id = (id > 0) ? id : "";

    var popup = window.open("/Popup/UploadInventoryFile/" + id, "Popup", "width=600,height=350");
    popup.focus();
});

function LoadTable(table) {
    return;

    var page = $(table).data('page');
    var dest = $(table).data('dest');
    var orderBy = $(table).data('order-by');
    var filter = $(table).data('filter');
    var search = $('#search-table-input').val();

    CreateSortingIcon(dest, orderBy);

    $.ajax({
        url: '/Projects/TaskTodoJson',
        dataType: 'json',
        type: 'GET',
        data: { page: page, dest: dest, orderBy: orderBy, search: search, status: filter },
        success: function (data) {
            FillTable(data.data, table);
            GeneratePagination(data.pages, table);
        },
        error: function (XMLHttpRequest) {
            $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
        }
    });
}

function FillTable(data, table) {
    var row = "";

    if (data != null) {
        $.each(data, function (index, value) {
            var fitter = (value.FitterFinished != null) ? value.FitterFinished : "";

            row += '<tr data-task-id="' + value.Id + '" class="tr-clickable">' +
                '<td class="task-details" data-task-id="' + value.Id + '">' + value.ProductName + '</td>' +
                '<td><a href="/Projects/Details/' + value.ProjectId + '">' + value.ProjectNumber + '</a></td>' +
                '<td><a href="/Projects/ApartmentDetails/' + value.ApartmentId + '">' + value.ApartmentNumber + '</td>' +
                '<td>' + value.ClosedDateString + '</td>' +
                '<td>' + fitter  + '</td>' +
            '<td  class="task-details with-label" data-task-id="' + value.Id + '">' + CreateLabel(value.Status) + '</td>' +
            '<td>' + GetCrudHtml(value.Id, value.ApartmentId) + '</td>' +
            '</tr>';
        });
    }

    $(table + ' tbody').html(row);
}

function DefaultPropTable() {
    $(tableId).data('page', 1);
    $(tableId).data('dest', 'asc');
    $(tableId).data('order-by', 'name');
}

function GetCrudHtml(id, apartmentId) {
    return '<div class="crud-container">' +
        '<span data-toggle="tooltip" data-placement="bottom" title="Szczegóły" data-task-id="' + id + '" class="crud crud-details task-details"><i class="glyphicon glyphicon-eye-open"></i></span>' +
        '<span data-toggle="tooltip" data-placement="bottom" title="Zdjęcia" data-task-id="' + id + '" class="crud crud-photos task-photos"><i class="glyphicon glyphicon-camera"></i></span>' +
        '<a href= "/Projects/ApartmentDetails/' + apartmentId + '" class="crud crud-details" data-placement="bottom" data-toggle="tooltip" title="Pokaż apartament" > <i class="fa fa-building"></i></a>' +
        '<span data-toggle="tooltip" data-placement="bottom" title="Wyślij" data-task-id="' + id + '" class="crud crud-mail send-raport"><i class="glyphicon glyphicon-envelope"></i></span >' +
        '<span class="crud crud-delete todo-check" data-task-id="' + id + '" data-toggle="tooltip", data-placement="bottom" title="Odznacz"><i class="glyphicon glyphicon-check"></i></span>' +
            "</div>";
}

function GeneratePagination(pages, table) {
    var currentPage = $(table).data('page');

    var prevBtn = '<li><a href="#" class="prevBtn">«</a></li>';
    var nextBtn = '<li><a href="#" class="nextBtn">»</a></li>'

    var pagination = '';

    if (pages > 1 && currentPage > 1)
        pagination += prevBtn;

    for (var i = 1; i <= pages; i++) {
        if (i === currentPage)
            pagination += '<li class="active"><a href="#">' + i + '</a></li>';
        else
            pagination += '<li><a href="#" data-page="' + i + '">' + i + '</a></li>';
    }

    if (pages > 1 && currentPage < pages)
        pagination += nextBtn;

    $('.pagination').html(pagination);
}

function CreateSortingIcon(dest, orderBy) {
    var iconAsc = '<i class="glyphicon glyphicon-sort-by-alphabet sort-column-icon"></i>';
    var iconDesc = '<i class="glyphicon glyphicon-sort-by-alphabet-alt sort-column-icon"></i>'

    $(tableId + ' thead tr th i').remove();

    if (orderBy === 'name' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconAsc);
    else if (orderBy === 'name' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconDesc);
    else if (orderBy === 'project' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconAsc);
    else if (orderBy === 'project' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconDesc);
    else if (orderBy === 'apartment' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconAsc);
    else if (orderBy === 'apartment' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconDesc);
    else if (orderBy === 'status' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(4)').append(iconAsc);
    else if (orderBy === 'status' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(4)').append(iconDesc);
}

function CreateLabel(status) {
    if (status === 3)
        return '<span class="label label-success">Zakończone</span>';

    if (status === 4)
        return '<span class="label label-danger">Zakończone z błędami</span>';
}

function ChangeFilter() {
    var isFirst = true;
    var filter = "";

    var checkbox1 = $('#fltr-chbx-3').prop('checked');
    var checkbox2 = $('#fltr-chbx-4').prop('checked');

    if (checkbox1 === true) {
        filter += "3";
        isFirst = false;
    }

    if (checkbox2 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 4;
        isFirst = false;
    }


    $(tableId).data('filter', filter);
}


