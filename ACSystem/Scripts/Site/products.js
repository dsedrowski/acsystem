﻿var tableId = '#products-list';
GetTableFilterWebStorage("product", tableId);
var table = LoadTable(tableId);

var statusFilter = $('.status-filter');

$(document).ready(function () {
    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue'
    });

    //$('input[type="checkbox"].filter').iCheck('check');
    //$('input[type="checkbox"].is-checked').iCheck("uncheck");

    statusFilter.on('ifChanged', function () {
        ChangeFilter();
        DefaultPropTable();
        LoadTable(tableId);
    });
});

$(document).on('click', '.product-details', function () {
    location.assign('/Products/Details/' + $(this).data('product-id'));
});

$(document).on('click', '.product-delete', function () {
    var id = $(this).data('product-id');

    var fn = function () {
        $.ajax({
            url: '/Products/Delete',
            dataType: 'json',
            type: 'POST',
            data: { id: id },
            success: function (data) {
                if (data.success === 'true') {
                    location.reload();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            }
        });
    };

    Confirm("Czy na pewno chcesz usunąć produkt?", fn);
});

$(document).on('click', '.pagination a', function () {
    var page = $(this).data('page');

    if (page !== null) {
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .prevBtn', function () {
    var page = $(tableId).data('page');

    if (page !== null) {
        page -= 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .nextBtn', function () {
    var page = $(tableId).data('page');

    if (page !== null) {
        page += 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '#search-table-btn', function () {
    DefaultPropTable();
    LoadTable(tableId);
});

$("#search-table-input").on("keyup", function (event) {
    if (event.keyCode === 13) {
        $(table).html("");
        DefaultPropTable();
        LoadTable(tableId);
    }
});

$(document).on('click', tableId + ' th.clickable', function () {
    DefaultPropTable();
    $(tableId).data('order-by', $(this).data('id'));

    var currentDest = $(this).data('dest');

    $(tableId + ' th.clickable').data('dest', 'desc');

    if (currentDest === 'asc')
        $(this).data('dest', 'desc');
    if (currentDest === 'desc') $(this).data('dest', 'asc');

    $(tableId).data('dest', $(this).data('dest'));

    LoadTable(tableId);
});

$(document).on('click', '.close-modal', function () {
    Custombox.close();
});

$(document).on('click', '.product-active', function () {
    var productID = $(this).data("product-id");
    var isActive = $(this).data("block-product");

    var successFn = function (data) {
        if (data.success === true) {
            if (isActive === false) {
                $(".pr-" + productID).addClass("disactive");
                $(".pr-active-btn-" + productID).data("block-product", true);
                $(".pr-active-btn-" + productID + " i").removeClass("fa-lock").addClass("fa-unlock");
            }
            else {
                $(".pr-" + productID).removeClass("disactive");
                $(".pr-active-btn-" + productID).data("block-product", false);
                $(".pr-active-btn-" + productID + " i").removeClass("fa-unlock").addClass("fa-lock");
            }

            toastr.success("Pomyślnie zmieniono aktywność produktu");
        }
        else {
            toastr.error("Wystąpił błąd podczas zmiany aktywności produktu");
        }
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas zmiany aktywności produktu");
    };

    GetAjaxWithFunctions("/Products/ChangeActive", "POST", { ID: productID, IsActive: isActive }, successFn, errorFn, tableId);
});

$(document).on("click", "#synchronize-products", function () {
    $.LoadingOverlay("show");
    $.get("/Fortnox/Product_Synchronize", function (data) {
        if (data.success)
            toastr.success(data.message);
        else
            toastr.error(data.message);

        if (data.reload)
            location.reload();

        $.LoadingOverlay("hide");
    });
});

function LoadTable(table) {
    var page = $(table).data('page');
    var dest = $(table).data('dest');
    var orderBy = $(table).data('order-by');
    var filter = $(table).data('filter');
    var search = $('#search-table-input').val();

    SetTableFilterWebStorage("product", page, dest, orderBy, filter, search);
    CreateSortingIcon(dest, orderBy);

    $.ajax({
        url: '/Products/ProductsList',
        dataType: 'json',
        type: 'GET',
        data: { page: page, dest: dest, orderBy: orderBy, search: search, filter: filter },
        success: function (data) {
            FillTable(data.data, table);
            GeneratePagination(data.pages, table);
        },
        error: function (XMLHttpRequest) {
            $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
        }
    });
}

function FillTable(data, table) {
    var row = "";

    if (data !== null) {
        $.each(data, function (index, value) {
            var isActive = value.IsActive === true ? "" : "disactive";

            row += '<tr class="tr-clickable ' + isActive + ' pr-' + value.Id + '">' +
            '<td class="product-details" data-product-id="' + value.Id + '">' + value.Name + '</td>' +
            '<td class="product-details" data-product-id="' + value.Id + '">' + value.TypeString + '</td>' +
            '<td class="product-details" data-product-id="' + value.Id + '">' + value.DescriptionPL + '</td>' +
            '<td class="product-details" data-product-id="' + value.Id + '">' + value.Price + ' kr</td>' +
            '<td class="product-details" data-product-id="' + value.Id + '">' + value.Jm + '</td>' +
            '<td>' + GetCrudHtml(value.Id, value.IsActive) + '</td>' +
            '</tr>';
        });
    }

    $(table + ' tbody').html(row);
}

function DefaultPropTable() {
    $(tableId).data('page', 1);
    $(tableId).data('dest', 'asc');
    $(tableId).data('order-by', 'name');
}

function GetCrudHtml(id, isActive) {
    var blockIcon = isActive ? "lock" : "unlock";
    var blockText = isActive ? "Zablokuj" : "Odblokuj";

    return '<div class="crud-container">' +
        '<span class="crud crud-block product-active pr-active-btn-' + id + '" data-product-id="' + id + '" data-block-product="' + !isActive + '" data-toggle="tooltip" data-placement="bottom" title="' + blockText + '"><i class="fa fa-' + blockIcon + '"></i></span>' +
                '<a class="crud crud-details" href="/Products/Details/' + id + '" data-toggle="tooltip", data-placement="bottom" title="Szczegóły"><i class="glyphicon glyphicon-eye-open"></i></a>' +
                '<a class="crud crud-edit" href="/Products/Edit/' + id + '" data-toggle="tooltip", data-placement="bottom" title="Edytuj"><i class="glyphicon glyphicon-pencil"></i></a>' +
                '<span class="crud crud-delete product-delete" data-product-id="' + id + '" data-toggle="tooltip" data-placement="bottom" title="Usuń"><i class="glyphicon glyphicon-remove"></i></span>' +
            '</div>';
}

function GeneratePagination(pages, table) {
    var currentPage = $(table).data('page');

    var prevBtn = '<li><a href="#" class="prevBtn">«</a></li>';
    var nextBtn = '<li><a href="#" class="nextBtn">»</a></li>';

    var pagination = '';

    if (pages > 1 && currentPage > 1)
        pagination += prevBtn;

    for (var i = 1; i <= pages; i++) {
        if (i === currentPage)
            pagination += '<li class="active"><a href="#">' + i + '</a></li>';
        else
            pagination += '<li><a href="#" data-page="' + i + '">' + i + '</a></li>';
    }

    if (pages > 1 && currentPage < pages)
        pagination += nextBtn;

    $('.pagination').html(pagination);
}

function CreateSortingIcon(dest, orderBy) {
    var iconAsc = '<i class="glyphicon glyphicon-sort-by-alphabet sort-column-icon"></i>';
    var iconDesc = '<i class="glyphicon glyphicon-sort-by-alphabet-alt sort-column-icon"></i>';

    $(tableId + ' thead tr th i').remove();

    if (orderBy === 'name' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconAsc);
    else if (orderBy === 'name' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconDesc);
    else if (orderBy === 'price' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconAsc);
    else if (orderBy === 'price' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconDesc);
}

function ChangeFilter() {
    var isFirst = true;
    var filter = "";

    var checkbox1 = $('#fltr-chbx-1').prop('checked');

    if (checkbox1 === true) {
        filter += "0";
        isFirst = false;
    }

    $(tableId).data('filter', filter);
}