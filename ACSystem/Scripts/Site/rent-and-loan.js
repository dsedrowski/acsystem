﻿var tableID = "#table-list";
var table = LoadTable(tableID);

$(document).ready(function () {
    $(".select2").select2();
    
    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue',
    });

    $('input[type="checkbox"].minimal').iCheck('check');

    $(".status-filter").on('ifChanged', function () {
        ChangeFilter();
        LoadTable(tableID);
    });
})

$(document).on('click', '.rent-details', function () {
    location.assign('/RentAndLoan/Edit/' + $(this).data('rent-id'));
});

$(document).on("click", ".rent-delete", function () {
    var ID = $(this).data("rent-id");

    var successFn = function (data) {
        if (data.success) {
            $(".rent-" + ID).remove();
            toastr.success("Pomyślnie usunięto pozycję.");
        }
        else
            toastr.error("Wystąpił błąd podczas usuwania pozycji");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas usuwania pozycji");
    }

    var fn = function () {
        GetAjaxWithFunctions("/RentAndLoan/Delete", "GET", { ID: ID }, successFn, errorFn, tableID);
    }

    Confirm("Czy chcesz usunąć wybraną pozycję?", fn);
})

$(document).on('click', '.pagination a', function () {
    var page = $(this).data('page');

    if (page != null) {
        $(tableID).data('page', page);
        LoadTable(tableID);
    }
});

$(document).on('click', '.pagination .prevBtn', function () {
    var page = $(tableID).data('page');;

    if (page != null) {
        page -= 1;
        $(tableID).data('page', page);
        LoadTable(tableID);
    }
});

$(document).on('click', '.pagination .nextBtn', function () {
    var page = $(tableID).data('page');;

    if (page != null) {
        page += 1;
        $(tableID).data('page', page);
        LoadTable(tableID);
    }
});

$(document).on('click', '#search-table-btn', function () {
    $(table).html("");
    DefaultPropTable();
    LoadTable(tableID);
});

$("#search-table-input").on("keyup", function (event) {
    if (event.keyCode == 13) {
        $(table).html("");
        DefaultPropTable();
        LoadTable(tableId);
    }
});

$("#search-table-input").on("keyup", function (event) {
    if (event.keyCode == 13) {
        $(table).html("");
        DefaultPropTable();
        LoadTable(tableID);
    }
});

$(document).on('click', tableID + ' th.clickable', function () {
    DefaultPropTable();
    $(tableID).data('order-by', $(this).data('id'))

    var currentDest = $(this).data('dest');

    $(tableID + ' th.clickable').data('dest', 'desc');

    if (currentDest === 'asc')
        $(this).data('dest', 'desc');
    if (currentDest === 'desc') $(this).data('dest', 'asc');

    $(tableID).data('dest', $(this).data('dest'));

    LoadTable(tableID);
});

function LoadTable(table) {
    var page = $(table).data('page');
    var dest = $(table).data('dest');
    var orderBy = $(table).data('order-by');
    var filter = $(table).data('filter');
    var search = $('#search-table-input').val();

    CreateSortingIcon(dest, orderBy);

    var successFn = function (data) {
        FillTable(data.data, table);
        GeneratePagination(data.pages, table);
    }

    var errorFn = function (XMLHttpRequest) {
        $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
    }

    var data = { page: page, dest: dest, orderBy: orderBy, search: search, filter: filter };

    GetAjaxWithFunctions("/RentAndLoan/List", "GET", data, successFn, errorFn, table);
}

function FillTable(data, table) {
    var row = "";

    if (data != null) {
        $.each(data, function (index, value) {
            row += '<tr class="tr-clickable rent-' + value.ID +'">' +
            '<td class="rent-details" data-rent-id="' + value.ID + '">' + value.ID + '</td>' +
            '<td class="rent-details" data-rent-id="' + value.ID + '"><a href="/Account/Details/' + value.FitterID + '">' + value.FitterFullName + '</a></td>' +
            '<td class="rent-details" data-rent-id="' + value.ID + '">' + value.Description + '</td>' +
                '<td class="rent-details" data-rent-id="' + value.ID + '">' + value.FullAmount + ' ' + value.CurrencyString + '</td>' +
            '<td class="rent-details" data-rent-id="' + value.ID + '">' + value.InstallmentAmount + ' ' + value.CurrencyString + '</td>' +
                '<td class="rent-details" data-rent-id="' + value.ID + '">' + value.LeftAmount + ' ' + value.CurrencyString + '</td>' +
            '<td  class="rent-details with-label" data-rent-id="' + value.ID + '">' + CreateLabel(value.Status) + '</td>' +
            '<td>' + GetCrudHtml(value.ID) + '</td>' +
            '</tr>';
        });
    }

    $(table + ' tbody').html(row);
}

function DefaultPropTable() {
    $(tableID).data('page', 1);
    $(tableID).data('dest', 'desc');
    $(tableID).data('order-by', 'number');
}

function GetCrudHtml(id, status) {
    var toret = '<div class="crud-container">';
                    
    toret += (status != 2) ? '<a class="crud crud-accept" href="/RentAndLoan/HandyRepayment/' + id + '" data-toggle="tooltip" data-placement="bottom" title="Spłać całość"><i class="fa fa-check"></i></a>' : "";
    toret +=    '<a class="crud crud-edit" href="/RentAndLoan/Edit/' + id + '" data-toggle="tooltip" data-placement="bottom" title="Edytuj"><i class="glyphicon glyphicon-pencil"></i></a>' +
                '<span class="crud crud-delete rent-delete" data-rent-id="' + id + '" data-toggle="tooltip" data-placement="bottom" title="Usuń"><i class="glyphicon glyphicon-remove"></i></span>' +

                '</div>';

    return toret;
}

function GeneratePagination(pages, table) {
    var currentPage = $(table).data('page');

    var prevBtn = '<li><a href="#" class="prevBtn">«</a></li>';
    var nextBtn = '<li><a href="#" class="nextBtn">»</a></li>'

    var pagination = '';

    if (pages > 1 && currentPage > 1)
        pagination += prevBtn;

    for (var i = 1; i <= pages; i++) {
        if (i === currentPage)
            pagination += '<li class="active"><a href="#">' + i + '</a></li>';
        else
            pagination += '<li><a href="#" data-page="' + i + '">' + i + '</a></li>';
    }

    if (pages > 1 && currentPage < pages)
        pagination += nextBtn;

    $('.pagination').html(pagination);
}

function CreateSortingIcon(dest, orderBy) {
    var iconAsc = '<i class="glyphicon glyphicon-sort-by-alphabet sort-column-icon"></i>';
    var iconDesc = '<i class="glyphicon glyphicon-sort-by-alphabet-alt sort-column-icon"></i>'

    $(tableID + ' thead tr th i').remove();

    if (orderBy === 'number' && dest === 'asc')
        $(tableID + ' thead tr th:nth-child(1)').append(iconAsc);
    else if (orderBy === 'number' && dest === 'desc')
        $(tableID + ' thead tr th:nth-child(1)').append(iconDesc);
    else if (orderBy === 'fitter' && dest === 'asc')
        $(tableID + ' thead tr th:nth-child(2)').append(iconAsc);
    else if (orderBy === 'fitter' && dest === 'desc')
        $(tableID + ' thead tr th:nth-child(2)').append(iconDesc);
    else if (orderBy === 'full-amount' && dest === 'asc')
        $(tableID + ' thead tr th:nth-child(4)').append(iconAsc);
    else if (orderBy === 'full-amount' && dest === 'desc')
        $(tableID + ' thead tr th:nth-child(4)').append(iconDesc);
    else if (orderBy === 'installment-amount' && dest === 'asc')
        $(tableID + ' thead tr th:nth-child(5)').append(iconAsc);
    else if (orderBy === 'installment-amount' && dest === 'desc')
        $(tableID + ' thead tr th:nth-child(5)').append(iconDesc);
    else if (orderBy === 'left-amount' && dest === 'asc')
        $(tableID + ' thead tr th:nth-child(6)').append(iconAsc);
    else if (orderBy === 'left-amount' && dest === 'desc')
        $(tableID + ' thead tr th:nth-child(6)').append(iconDesc);
    else if (orderBy === 'status' && dest === 'asc')
        $(tableID + ' thead tr th:nth-child(7)').append(iconAsc);
    else if (orderBy === 'status' && dest === 'desc')
        $(tableID + ' thead tr th:nth-child(7)').append(iconDesc);
}

function CreateLabel(status) {
    if (status === 0)
        return '<span class="label label-default">Gotowe</span>';

    if (status === 1)
        return '<span class="label label-warning">Częściowo potrącone</span>';

    if (status === 2)
        return '<span class="label label-success">Zamknięte</span>';
}

function ChangeFilter() {
    var isFirst = true;
    var filter = "";

    var checkbox1 = $('#fltr-chbx-1').prop('checked');
    var checkbox2 = $('#fltr-chbx-2').prop('checked');
    var checkbox3 = $('#fltr-chbx-3').prop('checked');

    if (checkbox1 === true) {
        filter += "0";
        isFirst = false;
    }

    if (checkbox2 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 1;
        isFirst = false;
    }

    if (checkbox3 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 2;
        isFirst = false;
    }

    $(tableID).data('filter', filter);
}
