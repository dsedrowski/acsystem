﻿toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "2000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

var momentDate = moment(DefaultDate, 'YYYY-MM-DD HH:mm');

$('[data-toggle="tooltip"]').tooltip();

$(document).ready(function () {

    $('.select2-from-started').select2();

    $('.date-only').datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'pl',
        showTodayButton: true,
        useCurrent: true,
        calendarWeeks: true,
        showClear: true,
        showClose: true,
        defaultDate: momentDate,
        tooltips: {
            today: 'Dzisiaj',
            clear: 'Wyczyść',
            close: 'Zamknij',
            selectMonth: 'Wybierz miesiąc',
            prevMonth: 'Poprzedni miesiąc',
            nextMonth: 'Następny miesiąc',
            selectYear: 'Wybierz rok',
            prevYear: 'Poprzedni rok',
            nextYear: 'Następny rok',
            selectDecade: 'Wybierz dekadę',
            prevDecade: 'Poprzednia dekada',
            nextDecade: 'Następna dekada',
            prevCentury: 'Poprzedni wiek',
            nextCentury: 'Następny wiek'
        }
    });

    $('.date-only-first-null').datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'pl',
        showTodayButton: true,
        useCurrent: true,
        calendarWeeks: true,
        showClear: true,
        showClose: true,
        tooltips: {
            today: 'Dzisiaj',
            clear: 'Wyczyść',
            close: 'Zamknij',
            selectMonth: 'Wybierz miesiąc',
            prevMonth: 'Poprzedni miesiąc',
            nextMonth: 'Następny miesiąc',
            selectYear: 'Wybierz rok',
            prevYear: 'Poprzedni rok',
            nextYear: 'Następny rok',
            selectDecade: 'Wybierz dekadę',
            prevDecade: 'Poprzednia dekada',
            nextDecade: 'Następna dekada',
            prevCentury: 'Poprzedni wiek',
            nextCentury: 'Następny wiek'
        }
    });

    $('.date-time').datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        locale: 'pl',
        showTodayButton: true,
        useCurrent: true,
        calendarWeeks: true,
        showClear: true,
        showClose: true,
        sideBySide: true,
        stepping: 30,
        defaultDate: momentDate,
        tooltips: {
            today: 'Dzisiaj',
            clear: 'Wyczyść',
            close: 'Zamknij',
            selectMonth: 'Wybierz miesiąc',
            prevMonth: 'Poprzedni miesiąc',
            nextMonth: 'Następny miesiąc',
            selectYear: 'Wybierz rok',
            prevYear: 'Poprzedni rok',
            nextYear: 'Następny rok',
            selectDecade: 'Wybierz dekadę',
            prevDecade: 'Poprzednia dekada',
            nextDecade: 'Następna dekada',
            prevCentury: 'Poprzedni wiek',
            nextCentury: 'Następny wiek'
        }
    });

    $('.grouped-table [data-toggle="toggle"]').change(function () {
        $(this).parents().next('.group-hide').toggle();
    });
});


document.addEventListener('custombox:content:complete', function () {
    $('.date-only').datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'pl',
        showTodayButton: true,
        useCurrent: true,
        calendarWeeks: true,
        showClear: true,
        showClose: true,
        defaultDate: momentDate,
        tooltips: {
            today: 'Dzisiaj',
            clear: 'Wyczyść',
            close: 'Zamknij',
            selectMonth: 'Wybierz miesiąc',
            prevMonth: 'Poprzedni miesiąc',
            nextMonth: 'Następny miesiąc',
            selectYear: 'Wybierz rok',
            prevYear: 'Poprzedni rok',
            nextYear: 'Następny rok',
            selectDecade: 'Wybierz dekadę',
            prevDecade: 'Poprzednia dekada',
            nextDecade: 'Następna dekada',
            prevCentury: 'Poprzedni wiek',
            nextCentury: 'Następny wiek'
        }
    });

    $('.date-time').datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        locale: 'pl',
        showTodayButton: true,
        useCurrent: true,
        calendarWeeks: true,
        showClear: true,
        showClose: true,
        sideBySide: true,
        stepping: 5,
        defaultDate: momentDate,
        tooltips: {
            today: 'Dzisiaj',
            clear: 'Wyczyść',
            close: 'Zamknij',
            selectMonth: 'Wybierz miesiąc',
            prevMonth: 'Poprzedni miesiąc',
            nextMonth: 'Następny miesiąc',
            selectYear: 'Wybierz rok',
            prevYear: 'Poprzedni rok',
            nextYear: 'Następny rok',
            selectDecade: 'Wybierz dekadę',
            prevDecade: 'Poprzednia dekada',
            nextDecade: 'Następna dekada',
            prevCentury: 'Poprzedni wiek',
            nextCentury: 'Następny wiek'
        }
    });

    $('.custombox-content .select2').select2();
    $('.custombox-content .form-group').each(function () {
        var howMuch = $(this + ' .select2-container').length;

        if (howMuch > 1) {
            $(this + ' .select2-container:last-child').remove();
        }
    });


    //if (typeof Huebee != "undefined") {
    //    var hueb = new Huebee('.custombox-content .color-picker', {
    //        setText: false
    //    });

    //    hueb.on('change', function (color, hue, sat, lum) {
    //        $('.custombox-content #ColorHex').val(color);
    //    })

    //    var color = $('.custombox-content .color-picker').data('color');

    //    if (color) hueb.setColor(color);
    //}

    var dropzone = $(".custombox-content .file-dropzone").dropzone({
        sending: function (file, xhr, formData) {
            var taskID = $(".custombox-content .file-dropzone").data("task-id");
            // Will send the filesize along with the file as POST data.
            formData.append("TaskID", taskID);
        },
        success: function (first, response) {
            if (response.result === "true") {
                toastr.success("Dokument został dodany!");

                var docRow = "<tr>";
                docRow += "<td>" + response.fileName + "</td>";
                docRow += '<td class="text-center"><a href="/Monter/dist/Documents/' + response.newFileName + '" class="btn btn-sm btn-primary text-center">Pobierz</a></td>';
                docRow += "</tr>";

                $(".documents-table").append(docRow);
            }
        },
        successMultiple: function (first, response) {
            if (response.result === "true") {
                toastr.success("Dokument został dodany!");

                var docRow = "<tr>";
                docRow += "<td>" + response.fileName + "</td>";
                docRow += '<td class="text-center"><a href="/Monter/dist/Documents/' + response.newFileName + '" class="btn btn-sm btn-primary text-center">Pobierz</a></td>';
                docRow += "</tr>";

                $(".documents-table").append(docRow);
            }
        }
    });

    var dropzoneTask = $(".for-task1231").dropzone({
        sending: function (file, xhr, formData) {
            var taskID = $(".for-task").data("task-id");
            // Will send the filesize along with the file as POST data.
            formData.append("TaskID", taskID);
        },
        success: function (first, response) {
            if (response.result === "true") {
                toastr.success("Dokument został dodany!");

                var docRow = "<tr>";
                docRow += "<td>" + response.fileName + "</td>";
                docRow += '<td class="text-center"><a href="/Monter/dist/Documents/' + response.newFileName + '" class="btn btn-sm btn-primary text-center">Pobierz</a></td>';
                docRow += "</tr>";

                $(".documents-table").append(docRow);
            }
        },
        successMultiple: function (first, response) {
            if (response.result === "true") {
                toastr.success("Dokument został dodany!");

                var docRow = "<tr>";
                docRow += "<td>" + response.fileName + "</td>";
                docRow += '<td class="text-center"><a href="/Monter/dist/Documents/' + response.newFileName + '" class="btn btn-sm btn-primary text-center">Pobierz</a></td>';
                docRow += "</tr>";

                $(".documents-table").append(docRow);
            }
        }
    });

    //if ($(".custombox-content .file-dropzone").hasClass("for-task")) {
    //    var taskID = $(".custombox-content .file-dropzone").data("task-id");

    //    dropzone.on("sending", function (file, xhr, formData) {
    //        // Will send the filesize along with the file as POST data.
    //        formData.append("TaskID", taskID);
    //    });
    //}
});

$(document).on('submit', '#search-task-form', function (e) {
    e.preventDefault();

    var successFn = function (data) {
        if (data != null) {
            location.href = '/Task/Details/' + data;
        }
        else {
            toastr.error('Nie znaleziono zadania o podanym numerze', 'Błąd!');
        }
    }

    var errorFn = function () {
        toastr.error('Nie znaleziono zadania o podanym numerze', 'Błąd!');
    }

    GetAjaxWithFunctions('/Task/IsTaskExist', 'POST', $(this).serialize(), successFn, errorFn, '');
});

$(document).on('click', '.radios .check', function () {
    $('.radios .check').removeClass('active');
    $('.radios .check').children("input").attr("name", "");
    $(this).addClass('active');
    $(this).children('input').attr("name", "ColorHex");
});

$(document).on('click', '.close-modal', function () {
    Custombox.modal.close();
    ClearModal('#modal');
});

$(document).on('click', '.my-own-accordion a', function () {
    var id = $(this).attr('href');

    var accordionBody = $('.custombox-content ' + id);

    if (accordionBody.hasClass('collapse'))
        accordionBody.removeClass('collapse');
    else
        accordionBody.addClass('collapse');
});

$(document).on("change", ".check-all", function () {
    var type = $(this).data("type");
    var isChecked = $(this).prop("checked");

    $("." + type + "-checkbox").prop("checked", isChecked);
});

function LoadModalContent(element, url, withColor) {
    if (withColor){
        GetAjaxLoadModalWithColorPicker(url, element);
    }
    else {
        GetAjaxLoadModal(url, element);
    }

}

function ClearModal(element) {
    $(element).html("");
}

function NewModal(element) {
    new Custombox.modal({
        content: {
            effect: 'fadein',
            target: element,
            animateFrom: 'top',
            animateTo: 'bottom',
            positionX: 'center',
            positionY: 'center',
            fullscreen: false
        },
        overlay: {
            active: 0.5
        }
    }).open();
}

function GetAjax(url, type, data, successMessage, errorMessage, loadingElement) {
    var result = null;

    $.ajax({
        url: url,
        data: data,
        type: type,
        dataType: 'json',
        beforeSend: function () {
            if (loadingElement === "")
                $.LoadingOverlay("show");
            else
                $(loadingElement).LoadingOverlay("show");
        },
        success: function (data) {
            result = data;
            if (successMessage !== "")
                toastr.success(successMessage);
        },
        error: function (xhr, status) {
            if (errorMessage === "")
                toastr.error("Wystąpił błąd podczas ładowania zawartości!");
            else
                toastr.error(errorMessage);
        },
        complete: function () {
            if (loadingElement === "")
                $.LoadingOverlay("hide");
            else
                $(loadingElement).LoadingOverlay("hide", true);
        }
    });
}

function GetAjaxWithSuccess(url, type, data, successMessage, errorMessage, loadingElement) {
    var result = null;

    $.ajax({
        url: url,
        data: data,
        type: type,
        dataType: 'json',
        beforeSend: function () {
            if (loadingElement === "")
                $.LoadingOverlay("show");
            else
                $(loadingElement).LoadingOverlay("show");
        },
        success: function (data) {
            result = data;
            if (data.success === 'true') {
                if (successMessage !== "")
                    toastr.success(successMessage);
            }
            else {
                if (errorMessage === "")
                    toastr.error("Wystąpił błąd podczas ładowania zawartości!");
                else
                    toastr.error(errorMessage);
            }
        },
        error: function (xhr, status) {
            if (errorMessage === "")
                toastr.error("Wystąpił błąd podczas ładowania zawartości!");
            else
                toastr.error(errorMessage);
        },
        complete: function () {
            if (loadingElement === "")
                $.LoadingOverlay("hide");
            else
                $(loadingElement).LoadingOverlay("hide", true);
        }
    });

    return result;
}

var ajaxRequest;
function GetAjaxWithFunctions(url, type, data, successFunc, errorFunc, loadingElement) {
    var result = null;

    clearTimeout(ajaxRequest);
    ajaxRequest = setTimeout(function () {
        $.ajax({
            url: url,
            data: data,
            type: type,
            dataType: 'json',
            beforeSend: function () {
                if (loadingElement === "")
                    $.LoadingOverlay("show");
                else
                    $(loadingElement).LoadingOverlay("show");
            },
            success: function (data) { result = data; successFunc(data); },
            error: function (xhr, status) { errorFunc(xhr, status); },
            complete: function () {
                if (loadingElement === "")
                    $.LoadingOverlay("hide");
                else
                    $(loadingElement).LoadingOverlay("hide", true);

                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    }, 1000);

    return result;
}

function GetAjaxWithFunctionsHtml(url, type, data, successFunc, errorFunc, loadingElement) {
    var result = null;

    $.ajax({
        url: url,
        data: data,
        type: type,
        dataType: 'html',
        beforeSend: function () {
            if (loadingElement === "")
                $.LoadingOverlay("show");
            else
                $(loadingElement).LoadingOverlay("show");
        },
        success: function (data) { result = data; successFunc(data); },
        error: function (xhr, status) { errorFunc(xhr, status); },
        complete: function () {
            if (loadingElement === "")
                $.LoadingOverlay("hide");
            else
                $(loadingElement).LoadingOverlay("hide", true);
        }
    });

    return result;
}

function GetAjaxLoadModal(url, element) {
    $.ajax({
        url: url,
        async: true,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        success: function (data) {
            $(element).html(data);
            NewModal(element);
        },
        error: function (xhr, status) {
            toastr.error("Wystąpił błąd podczas ładowania zawartości!");
        },
        complete: function () {
            $.LoadingOverlay("hide");
        }
    });
}

function GetAjaxLoadModalWithColorPicker(url, element) {
    $.ajax({
        url: url,
        async: true,
        type: 'GET',
        dataType: 'html',
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        success: function (data) {
            $(element).html(data);
            NewModal(element);

        },
        error: function (xhr, status) {
            toastr.error("Wystąpił błąd podczas ładowania zawartości!");
        },
        complete: function () {
            $.LoadingOverlay("hide");
        }
    });
}

function ArrayToObject(array) {
    var returnArray = {};
    for (var i = 0; i < array.length; i++) {
        returnArray[array[i]['name']] = array[i]['value'];
    }
    return returnArray;
}

function PrintDiv(divID) {
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('<html><head><title>' + document.title + '</title>');
    mywindow.document.write('<link href="/Content/Site/home.css" rel="stylesheet">');
    mywindow.document.write('<link href="/Content/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet">');
    mywindow.document.write('<style>.fc-left { opacity: 0; } .fc-right { opacity: 0; }</style>');
    mywindow.document.write('</head><body onload="window.print()">');
    mywindow.document.write($(divID).html());
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    //mywindow.print();
    //mywindow.close();

    return true;
}

function Confirm(message, func) {
    var pin = Math.floor(Math.random() * 9999 + 1000);

    var content = '<p class="text-center">' + message + "</div>";
    content += "<h5>Przepisz PIN by potwierdzić</h5>";
    content += "<h3>" + pin + "</h3>";
    content += '<div class="form-group">';
    content += '<input type="text" id="confirmation-pin" maxlength="5" size"5" style="color: black; font-size: 30px; padding: 10px; width: 97px; text-align: center;"/>';
    content += "</div>";

    $.confirm({
        title: "Uwaga!",
        content: content,
        buttons: {
            tak: {
                text: "TAK",
                action: function () {
                    var cpin = $("#confirmation-pin").val();
                    var dpin = pin.toString();

                    if (cpin === dpin)
                        func();
                    else
                        alert("Błędny pin");
                }
            },
            nie: {
                text: "NIE",
                action: function () { return;}
            }
        },
        theme: "supervan"
    });
}

function GetStatusLabel(status) {
    var label = "";

    switch (status) {
        case 2:
            label = '<span class="label label-info">Przerwane</span>';
            break;
        case 3:
            label = '<span class="label label-primary">Miejsce nieprzygotowane</span>';
            break;
        case 9:
            label = '<span class="label label-success">Zakończone</span>';
            break;
        case 7:
            label = '<span class="label label-danger">Zakończone z błędami</span>';
            break;
    }

    return label;
}

function SetTableFilterWebStorage(prefix, page, dest, orderby, filter, search) {
    localStorage.setItem(prefix + "-page", page);
    localStorage.setItem(prefix + "-dest", dest);
    localStorage.setItem(prefix + "-orderby", orderby);
    localStorage.setItem(prefix + "-filter", filter);
    localStorage.setItem(prefix + "-search", search);
}

function GetTableFilterWebStorage(prefix, tableID) {
    var page = localStorage.getItem(prefix + "-page");
    var dest = localStorage.getItem(prefix + "-dest");
    var orderby = localStorage.getItem(prefix + "-orderby");
    var filter = localStorage.getItem(prefix + "-filter");
    var search = localStorage.getItem(prefix + "-search");

    if (page === null)
        return;

    $(tableID).data("page", page);
    $(tableID).data("dest", dest);
    $(tableID).data("order-by", orderby);
    $(tableID).data("filter", filter);
    $('#search-table-input').val(search);

    var filterArray = filter.split(',');

    $('input[type="checkbox"].filter').iCheck('uncheck');
    for (var i = 0; i < filterArray.length; i++) {
        var checkID = '.value-' + filterArray[i];
        $(checkID).iCheck('check');
    }
}
