﻿$(document).ready(function () {
    $("#check-unsended").click(function () {
        $(".unsended-report").prop("checked", true);
    });

    $("#check-unprinted").click(function () {
        $(".unprintend-report").prop("checked", true);
    });

    $("#check-all").click(function () {
        $(".report-check").prop("checked", true);
    });

    $("#uncheck-all").click(function () {
        $(".report-check").prop("checked", false);
    });

    $("#send-checked").click(SendChecked);

    $("#print-checked").click(PrintChecked);
});

function SendChecked() {
    var checked = GetCheckedList();
    var date = "";
    $.LoadingOverlay('show');
    $.get("/Reports/Project_GroupedReport?Reports=" + checked + "&Send=True", function (data) {
        if (data.success) {
            toastr.success("Pomyślnie wysłano raporty");
            date = data.date;

            $.get("/Reports/Project_CheckReportsAsSended?Reports=" + checked, function (data) {
                $(".report-check:checked").each(function () {
                    var id = $(this).val();
                    $(".td-sended-" + id).html(date);
                    $(".report-check-" + id).removeClass(".unsended-report").prop("checked", false);
                });
            });
        }
        else
            toastr.error("Wystąpił błąd podczas wysyłania raportu.");
        $.LoadingOverlay('hide');
    });
}

function PrintChecked() {
    var checked = GetCheckedList();

    $.get("/Reports/Project_GroupedReport?Reports=" + checked, function (data) {
        if (data.success) {
            PrintURLPDF("/dist/GroupReports/" + data.pdfName);

            $.get("/Reports/Project_CheckReportsAsPrinted?Reports=" + checked, function (data) {
                $(".report-check:checked").each(function () {
                    var id = $(this).val();
                    $(".td-printed-" + id).html("TAK");
                    $(".report-check-" + id).removeClass(".unprintend-report").prop("checked", false);
                });
            });
        }
        else
            toastr.error("Wystąpił błąd podczas drukowania raportu.");
    });
}

function GetCheckedList() {
    var idArr = new Array();

    $(".report-check:checked").each(function (index, elem) {
        idArr.push($(this).val());
    });
    
    return idArr.join(",");
}