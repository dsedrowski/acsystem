﻿var tableId = '#invoice-list';
var table = LoadTable(tableId);

var statusFilter = $('.status-filter');

$(document).ready(function () {
    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue'
    });

    $('input[type="checkbox"].minimal').iCheck('check');

    statusFilter.on('ifChanged', function () {
        ChangeFilter();
        LoadTable(tableId);
    });

});

$(document).on('click', '.add-invoice', function () {
    location.assign('/Office/InvoiceCreate');
});

$(document).on('click', '.edit-invoice', function () {
    var id = $(this).data('invoice-id');

    location.assign('/Office/EditInvoice/' + id);
});

$(document).on('click', '.invoice-details', function () {
    var id = $(this).data('invoice-id');

    location.assign('/Office/DetailsInvoice/' + id);
});

$(document).on('click', '.delete-invoice', function () {
    var id = $(this).data('invoice-id');

    var fn = function () {
        $.ajax({
            url: '/Office/InvoiceDelete',
            dataType: 'json',
            type: 'GET',
            data: { id: id },
            success: function (data) {
                if (data.success === 'true') {
                    location.reload();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            }
        });
    };

    Confirm("Czy na pewno chcesz usunąć fakturę?", fn);
});

$(document).on('click', '.send-invoice', function () {
    var id = $(this).data('invoice-id');

    var fn = function () {
        $.ajax({
            url: '/Office/SendInvoice',
            dataType: 'json',
            type: 'GET',
            data: { id: id },
            beforeSend: function () {
                    $.LoadingOverlay("show");
            },
            success: function (data) {
                if (data.success === 'true') {
                    location.reload();
                    toastr.success("Pomyślnie wysłano fakturę");
                }
                else {
                    toastr.error("Wystąpił błąd podczas wysyłania faktury. " + data.exception);
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            },
            complete: function () {
                    $.LoadingOverlay("hide");
            }
        });
    };

    Confirm("Czy na pewno chcesz wysłać fakturę?", fn);
});

$(document).on('click', '.print-invoice', function () {
    var id = $(this).data('invoice-id');

    var successFn = function (data) {
        if (data.success === true)
            PrintURLPDF('/dist/Invoice/' + id + '.pdf', id);
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas drukowania faktury.");
    };

    GetAjaxWithFunctions('/Print/InvoiceExist/' + id, "GET", null, successFn, errorFn, '');
});

$(document).on('click', '#search-table-btn', function () {
    DefaultPropTable();
    LoadTable(tableId);
});

$("#search-table-input").on("keyup", function (event) {
    if (event.keyCode === 13) {
        $(table).html("");
        DefaultPropTable();
        LoadTable(tableId);
    }
});

$(document).on('click', tableId + ' th.clickable', function () {
    DefaultPropTable();
    $(tableId).data('order-by', $(this).data('id'));

    var currentDest = $(this).data('dest');

    $(tableId + ' th.clickable').data('dest', 'desc');

    if (currentDest === 'asc')
        $(this).data('dest', 'desc');
    if (currentDest === 'desc') $(this).data('dest', 'asc');

    $(tableId).data('dest', $(this).data('dest'));

    LoadTable(tableId);
});

$(document).on('click', '.invoice-is-send', function () {
    var invoiceID = $(this).data('invoice-id');

    var successFn = function (data) {
        if (data.success === true) {
            location.reload();
        }
        else {
            toastr.error("Wystąpił błąd podczas zmiany statusu.");
        }
    };

    var errorFn = function (data) {
        toastr.error("Wystąpił błąd podczas zmiany statusu");
    };

    var data = { ID: invoiceID, Status: 1 };

    GetAjaxWithFunctions("/Office/ChangeInvoiceStatus", "POST", data, successFn, errorFn, '');
});

$(document).on('click', '.invoice-is-payed', function () {
    var invoiceID = $(this).data('invoice-id');

    var successFn = function (data) {
        if (data.success === true) {
            location.reload();
        }
        else {
            toastr.error("Wystąpił błąd podczas zmiany statusu.");
        }
    };

    var errorFn = function (data) {
        toastr.error("Wystąpił błąd podczas zmiany statusu");
    };

    var data = { ID: invoiceID, Status: 2 };

    GetAjaxWithFunctions("/Office/ChangeInvoiceStatus", "POST", data, successFn, errorFn, '');
});

$(document).on("click", ".xml-invoice", function () {
    var id = $(this).data("invoice-id");
    $('.with-label[data-invoice-id="' + id + '"]').html('<span class="label label-primary">Wygenerowany XML</span>');
});

$(document).on("click", ".xml-invoices", function () {
    $(".with-label").html('<span class="label label-primary">Wygenerowany XML</span>');
});


$(document).on('click', '.pagination a', function () {
    var page = $(this).data('page');

    if (page !== null) {
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .prevBtn', function () {
    var page = $(tableId).data('page');

    if (page !== null) {
        page -= 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on('click', '.pagination .nextBtn', function () {
    var page = $(tableId).data('page');

    if (page !== null) {
        page += 1;
        $(tableId).data('page', page);
        LoadTable(tableId);
    }
});

$(document).on("click", ".fortnox-invoice", function () {
    var id = $(this).data("invoice-id");

    $.LoadingOverlay("show");
    $.get("/Fortnox/Send_Invoice/" + id, function (data) {
        if (data.success)
            toastr.success(data.message);
        else
            toastr.error(data.message);

        if (data.reload)
            location.reload();

        $.LoadingOverlay("hide");
    });
});

function LoadTable(table) {
    var page = $(table).data('page');
    var dest = $(table).data('dest');
    var orderBy = $(table).data('order-by');
    var filter = $(table).data('filter');
    var search = $('#search-table-input').val();

    CreateSortingIcon(dest, orderBy);

    var successFn = function (data) {
        FillTable(data.data, table);
        GeneratePagination(data.pages, table);
    };

    var errorFn = function () {
        $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
    };

    var ajaxData = { page: page, dest: dest, orderBy: orderBy, search: search, filter: filter };

    GetAjaxWithFunctions('/Office/InvoicesList', "GET", ajaxData, successFn, errorFn, tableId);
}

function FillTable(data, table) {
    var row = "";

    if (data !== null) {
        $.each(data, function (index, value) {
            row += '<tr class="tr-clickable">' +
            '<td class="invoice-details" data-invoice-id="' + value.ID + '">' + value.InvoiceNo + '</td>' +
            '<td class="invoice-details" data-invoice-id="' + value.ID + '">' + ((value.URL !== null) ? value.URL : "") + '</td>' +
            '<td class="invoice-details" data-invoice-id="' + value.ID + '">' + value.ProjectNumber + '</td>' +
            '<td class="invoice-details" data-invoice-id="' + value.ID + '">' + value.CustomerName + '</td>' +
            '<td class="invoice-details" data-invoice-id="' + value.ID + '">' + value.Total + '</td>' +
            '<td class="invoice-details" data-invoice-id="' + value.ID + '">' + value.InvoiceDateString + '</td>' +
            '<td class="invoice-details" data-invoice-id="' + value.ID + '">' + value.PaymentTermString + '</td>' +
            '<td  class="invoice-details with-label" data-invoice-id="' + value.ID + '">' + CreateLabel(value.Status) + '</td>' +
            '<td>' + GetCrudHtml(value.ID, value.Status) + '</td>' +
            '</tr>';
        });
    }

    $(table + ' tbody').html(row);
}

function CreateLabel(status) {
    if (status === 0)
        return '<span class="label label-default">Gotowe</span>';

    if (status === 1)
        return '<span class="label label-warning">Wysłana</span>';

    if (status === 2)
        return '<span class="label label-success">Zapłacona</span>';

    if (status === 3)
        return '<span class="label label-primary">Wygenerowany XML</span>';

    if (status === 4)
        return '<span class="label label-primary">Fortnox</span>';
}

function GetCrudHtml(id, status) {
    var ret = '<div class="crud-container">' +
        '<span class="crud crud-details invoice-details" data-invoice-id="' + id + '" data-toggle="tooltip", data-placement="bottom" title="Szczegóły"><i class="glyphicon glyphicon-eye-open"></i></span>';

                if (status !== 4) {
                    ret += '<span class="crud crud-print fortnox-invoice" data-invoice-id="' + id + '"  data-toggle="tooltip" data-placement="bottom" title="Wyślij do Fortnox"><i class="fa fa-cloud-upload"></i></a>';
                }

                ret += '<a class="crud crud-print xml-invoice" href="/Office/GenerateInvoiceXml/' + id + '" data-invoice-id="' + id + '"  data-toggle="tooltip" data-placement="bottom" title="Generuj XML" onclick="location.reload();"><i class="fa fa-file-text"></i></a>' +
                '<span class="crud crud-delete delete-invoice" data-invoice-id="' + id + '" data-toggle="tooltip", data-placement="bottom" title="Usuń"><i class="glyphicon glyphicon-remove"></i></span>' +
        '</div>';

    return ret;
}

function GeneratePagination(pages, table) {
    var currentPage = $(table).data('page');

    var prevBtn = '<li><a href="#" class="prevBtn">«</a></li>';
    var nextBtn = '<li><a href="#" class="nextBtn">»</a></li>';

    var pagination = '';

    if (pages > 1 && currentPage > 1)
        pagination += prevBtn;

    for (var i = 1; i <= pages; i++) {
        if (i === currentPage)
            pagination += '<li class="active"><a href="#">' + i + '</a></li>';
        else
            pagination += '<li><a href="#" data-page="' + i + '">' + i + '</a></li>';
    }

    if (pages > 1 && currentPage < pages)
        pagination += nextBtn;

    $('.pagination').html(pagination);
}

function CreateSortingIcon(dest, orderBy) {
    var iconAsc = '<i class="glyphicon glyphicon-sort-by-alphabet sort-column-icon"></i>';
    var iconDesc = '<i class="glyphicon glyphicon-sort-by-alphabet-alt sort-column-icon"></i>';

    $(tableId + ' thead tr th i').remove();

    if (orderBy === 'number' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconAsc);
    else if (orderBy === 'number' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(1)').append(iconDesc);
    else if (orderBy === 'fortnox' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconAsc);
    else if (orderBy === 'fortnox' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(2)').append(iconDesc);
    else if (orderBy === 'project' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconAsc);
    else if (orderBy === 'project' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(3)').append(iconDesc);
    else if (orderBy === 'customer' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(4)').append(iconAsc);
    else if (orderBy === 'customer' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(4)').append(iconDesc);
    else if (orderBy === 'total' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(5)').append(iconAsc);
    else if (orderBy === 'total' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(5)').append(iconDesc);
    else if (orderBy === 'invoicedate' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(6)').append(iconAsc);
    else if (orderBy === 'invoicedate' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(6)').append(iconDesc);
    else if (orderBy === 'paymentterm' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(7)').append(iconAsc);
    else if (orderBy === 'paymentterm' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(7)').append(iconDesc);
    else if (orderBy === 'status' && dest === 'asc')
        $(tableId + ' thead tr th:nth-child(8)').append(iconAsc);
    else if (orderBy === 'status' && dest === 'desc')
        $(tableId + ' thead tr th:nth-child(8)').append(iconDesc);
}

function ChangeFilter() {
    var isFirst = true;
    var filter = "";

    var checkbox1 = $('#fltr-chbx-1').prop('checked');
    var checkbox2 = $('#fltr-chbx-2').prop('checked');
    var checkbox3 = $('#fltr-chbx-3').prop('checked');
    var checkbox4 = $('#fltr-chbx-4').prop('checked');

    if (checkbox1 === true) {
        filter += "0";
        isFirst = false;
    }

    if (checkbox2 === true) {
        if (isFirst !== true)
            filter += ",";

        filter += 1;
        isFirst = false;
    }

    if (checkbox3 === true) {
        if (isFirst !== true)
            filter += ",";

        filter += 2;
        isFirst = false;
    }

    if (checkbox4 === true) {
        if (isFirst !== true)
            filter += ",";

        filter += 3;
        isFirst = false;
    }

    $(tableId).data("filter", filter);
}

function DefaultPropTable() {
    $(tableId).data('page', 1);
    $(tableId).data('dest', 'desc');
    $(tableId).data('order-by', 'invoicedate');
}