﻿$(document).ready(function () {
    $("input:text").label_better({
        easing: "bounce"
    });

    $("#PenaltyInterest").label_better({
        easing: "bounce"
    });

    $('.select2').select2();

    $(".input-currency").inputmask("AAA", { "placeholder": "___" });
    $(".input-account").inputmask("AA 99 9999 9999 9999 9999 9999 9999", { "placeholder": "__ __ ____ ____ ____ ____ ____ ____" });

});

$(document).on('change', '.customer-select', function () {
    var id = $(this).val();

    LoadProjects(id);
    ChangePaymentTerm(id);
});

$(document).on('dp.change', '#InvoiceDate', function () {
    ChangePaymentTermFromHidden();
})

$(document).on('submit', '#InvoiceCreateForm', function (e) {
    e.preventDefault();

    var fields = $(this).serializeArray();
    var object = ArrayToObject(fields)

    var successFn = function(data){
        $('#InvoiceElements').html(data);
        $('.can-disable').prop('disabled', true);
        $('#SaveInvoice').removeClass('hidden');
        $('#CancelInvoice').removeClass('hidden');
        $('#CreateInvoicePositions').addClass('hidden');

        $('input[type="checkbox"].minimal').iCheck({
            checkboxClass: 'icheckbox_square-blue',
        });

        $('input[type="checkbox"].minimal.all-element-active').on('ifChanged', function () {
            var toCheck = $(this).data("checkboxes");

            var checked = $(this).parent('[class*="icheckbox"]').hasClass("checked");

            if (!checked)
                $("." + toCheck).iCheck("check");
            else
                $("." + toCheck).iCheck("uncheck");

        });

        $('input[type="checkbox"].minimal.element-active').on('ifChanged', function () {
            var value = Number($(this).data("row-value"));
            var projectNumber = $(this).data("project-number");

            var checked = $(this).parent('[class*="icheckbox"]').hasClass("checked");

            var projectSum = Number($(".bg-project-" + projectNumber + " span").html());

            if (!checked)
                $(".bg-project-" + projectNumber + " span").html(projectSum + value);
            else
                $(".bg-project-" + projectNumber + " span").html(projectSum - value);
        });
    }
    
    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas generowania pozycji faktury!");
    }

    GetAjaxWithFunctionsHtml('/Office/InvoiceElementsGenerate', 'POST', object, successFn, errorFn, this);
})

$(document).on('click', '#SaveInvoice', function () {
    //var id = $('#InvoiceID').val();

    var elemsCount = $('.invoice-element').length;
    
    var elemsValue = $('.invoice-element-value');

    var sumValue = 0;

    elemsValue.each(function () {
        var value = parseFloat($(this).text());
        sumValue += value;
    })

    var ids = "";
    var isFirstId = true;

    $('.invoiceIDs').each(function () {

        if (isFirstId === false) {
            ids += ';';
        } else {
            isFirstId = false;
        }

        ids += $(this).val();
    });

    var fn = function () {
        //$.ajax({
        //    url: '/Office/InvoiceSave',
        //    dataType: 'json',
        //    type: 'GET',
        //    data: { id: ids },
        //    beforeSend: function () {
        //        $('#InvoiceCreateForm').LoadingOverlay('show');
        //    },
        //    success: function (data) {
        //        if (data.success === 'true') {
        //            location.assign('/Office/Invoice');
        //        }
        //        else {
        //            $.alert('Błąd! \n' + data.message);
        //            location.reload();
        //        }
        //    },
        //    error: function (XMLHttpRequest) {
        //        $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
        //    },
        //    complete: function () {
        //        $('#InvoiceCreateForm').LoadingOverlay('hide', true);
        //    }
        //});
        $("#InvoiceSaveForm").submit();
    }
    if (elemsCount === 0)
        $.alert("Błąd! \n Nie można zapisać pustej faktury!");
    else if (sumValue === 0)
        $.alert("Błąd! \n Suma pozycji nie może być zerowa!")
    else
        Confirm("Czy na pewno chcesz zapisać fakturę i jej pozycje?", fn);
        
})

$(document).on('click', '#CancelInvoice', function () {
    var ids = "";
    var isFirstId = true;

    $('.invoiceIDs').each(function() {

        if (isFirstId === false) {
            ids += ';';
        } else {
            isFirstId = false;
        }

        ids += $(this).val();
    });
    
    var fn = function () {
        $.ajax({
            url: '/Office/CancelInvoice',
            dataType: 'json',
            type: 'GET',
            data: { id: ids },
            beforeSend: function() {
                $('#InvoiceCreateForm').LoadingOverlay('show');
            },
            success: function (data) {
                if (data.success === 'true') {
                    $('.can-disable').prop('disabled', false);
                    $('#InvoiceElements').html("");
                    $('#SaveInvoice').addClass('hidden');
                    $('#CancelInvoice').addClass('hidden');
                    $('#CreateInvoicePositions').removeClass('hidden');
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            },
            complete: function () {
                $('#InvoiceCreateForm').LoadingOverlay('hide', true);
            }
        });
    }

    Confirm("Czy na pewno chcesz anulować fakturę i jej pozycje?", fn);
})

$(document).on('click', '.add-bank-account', function () {
    NewModal('#bank-account-modal');
});

$(document).on('click', '.bank-account-create', function () {
    var currency = $('.custombox-content #input-currency').val();
    var account = $('.custombox-content #input-account').val();

    var data = { Currency: currency, Account: account };

    var successFn = function (data) {
        Custombox.modal.close();
        ClearModal('#modal');

        var accountOption = "";

        $.each(data.accounts, function (key, value) {
            accountOption += '<option value="' + value.ID + '">' + value.AccountNumber + '</option>';
        });

        $('#BankAccountID').html(accountOption);
    }

    var errorFn = function () {
        Custombox.modal.close();
        ClearModal('#modal');
        toastr.error("Wystąpił błąd podczas dodawania numeru konta!");
    }

    GetAjaxWithFunctions('/Office/AddBankAccount', 'GET', data, successFn, errorFn, '#BankAccountID');
})

$(document).on('click', '.delete-element', function () {
    var ID = $(this).data('element-id');

    var fn = function () {
        $.ajax({
            url: '/Office/InvoiceElementDelete',
            dataType: 'json',
            type: 'GET',
            data: { id: ID },
            beforeSend: function () {
                $('.element-' + ID).LoadingOverlay('show');
            },
            success: function (data) {
                if (data.success === 'true') {
                    $('.element-' + ID).hide();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            },
            complete: function () {
                $('.element-' + ID).LoadingOverlay('hide', true);
            }
        });
    }

    Confirm("Czy na pewno chcesz usunąć pozycję?", fn);
})

$(document).on('click', '.delete-invoice-apartment', function () {
    var apartmentNumber = $(this).data('apartment-number');
    var invoiceID = $(this).data('invoice-id');

    var fn = function () {
        $.ajax({
            url: '/Office/InvoiceElementDeleteByApartment',
            dataType: 'json',
            type: 'GET',
                data: { apartmentNumber : apartmentNumber, invoiceID: invoiceID },
                beforeSend : function () {
                    $('.apartment-' + apartmentNumber).LoadingOverlay('show');
                    },
            success: function (data) {
                if(data.success === 'true') {
                    $('.apartment-' + apartmentNumber).hide();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            },
            complete: function () {
                $('.apartment-' + apartmentNumber).LoadingOverlay('hide', true);
            }
        });
    }

    Confirm("Czy na pewno chcesz usunąć pozycję dla całego mieszkania?", fn);
});

$(document).on('click', '.delete-invoice-project', function () {
    var projectID = $(this).data('project-id');
    var invoiceID = $(this).data('invoice-id');

    var fn = function () {
        $.ajax({
            url: '/Office/InvoiceElementDeleteByProject',
            dataType: 'json',
            type: 'GET',
            data: { projectID: projectID, invoiceID: invoiceID },
            beforeSend: function () {
                $('.project-' + projectID).LoadingOverlay('show');
            },
            success: function (data) {
                if (data.success === 'true') {
                    $('.project-' + projectID).remove();
                }
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            },
            complete: function () {
                $('.project-' + projectID).LoadingOverlay('hide', true);
            }
        });
    }

    Confirm("Czy na pewno chcesz usunąć pozycję dla całego mieszkania?", fn);
});

$(document).on('click', '.AddTaskBtn', function () {
    var apartmentNumber = $(this).data('apartment-number');
    var invoiceID = $(this).data('invoice-id');
    var taskID = $('#AddTask-' + apartmentNumber).val();

    var succesFn = function(data){
        if (data.success === true) {
            var row = CreateInvoiceElement(data.element);

            $('.apartment-' + apartmentNumber + ' tbody').append(row);

            $('#AddTask-' + apartmentNumber + ' option[value="' + taskID + '"]').remove();

            toastr.success("Pomyślnie dodano pozycję do faktury");
        }
        else {
            toastr.error("Wystąpił błąd podczas dodawania zadania!");
        }
    }

    var errorFn = function(){
        toastr.error("Wystąpił błąd podczas dodawania zadania!");
    }

    GetAjaxWithFunctions('/Office/AddTaskToInvoice', 'POST', { apartmentNumber: apartmentNumber, invoiceID: invoiceID, taskID: taskID }, succesFn, errorFn, 'apartment-' + apartmentNumber)
});

$(document).on('click', '.AddApartmentBtn', function () {
    var apartmentNumber = $('#AddApartment').val();
    var invoiceID = $(this).data('invoice-id');

    var successFn = function (data) {
        var box = CreateApartmentBox(apartmentNumber, invoiceID, data.options);

        $('#apartments-boxes').append(box);

        $('#AddApartment option[value="' + apartmentNumber + '"]').remove();

        toastr.success("Pomyślnie dodano apartament");
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas dodawania apartamentu!");
    }

    GetAjaxWithFunctions('/Office/GetTasksForApartment', 'POST', { apartmentNumber: apartmentNumber, invoiceID: invoiceID }, successFn, errorFn, '');
});

$(document).on('click', '.doc-name-edit', function () {

})

function LoadProjects(id) {
    $('.project-select').html("");

    var succesFn = function (data) {

        var options = "";

        $.each(data.data, function (key, value) {
            options += '<option value="' + value.ID + '">' + value.ProjectNumber + '</option>';
        });

        $('.project-select').html(options);
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas ładowania projektów!");
    }

    GetAjaxWithFunctions('/Office/GetProjects/' + id, "GET", null, succesFn, errorFn, '.changeable');
}

function ChangePaymentTerm(id) {
    var currentDate = $('#InvoiceDate').val();

    var succesFn = function (data) {

        $('#PaymentTermDays').val(data.paymentTerm);

        var newMoment = moment(currentDate, 'YYYY-MM-DD').add(data.paymentTerm, 'days').format('YYYY-MM-DD');

        $('#PaymentTerm').val(newMoment);
    }

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas ładowania apartamentów!");
    }

    GetAjaxWithFunctions('/Office/GetCustomerPaymentTerm/' + id, "GET", null, succesFn, errorFn, '.changeable');
}

function ChangePaymentTermFromHidden() {
    var currentDate = $('#InvoiceDate').val();
    var days = $('#PaymentTermDays').val();

    var newMoment = moment(currentDate, 'YYYY-MM-DD').add(days, 'days').format('YYYY-MM-DD');

    $('#PaymentTerm').val(newMoment);
}

function CreateInvoiceElement(data) {
    var row = '<tr data-element-id="' + data.ID + '" class="element-' + data.ID + ' invoice-element">';
    row += '<td>' + data.ProductName + '</td>';
    row += '<td>' + data.Quantity + '</td>';
    row += '<td>' + data.Unit + '</td>';
    row += '<td>' + data.UnitPrice + '</td>';
    row += '<td class="invoice-element-value">' + data.Sum + '</td>';
    row += '<td><span data-toggle="tooltip" data-placement="bottom" title="Usuń" data-element-id="' + data.ID + '" class="crud crud-delete delete-element"><i class="glyphicon glyphicon-remove"></i></span></td>';

    return row;
}

function CreateApartmentBox(no, inv, elms) {
    var box = '<div class="box box-primary collapsed-box apartment-' + no + '">';
    box += '<div class="box-header with-border">';
    box += '<h3 class="box-title">Apartament ' + no + '</h3>';
    box += '<div class="box-tools pull-right">';
    box += '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>';
    box += '<button type="button" class="btn btn-box-tool delete-invoice-apartment" data-apartment-number="' + no + '" data-invoice-id="' + inv + '"><i class="fa fa-trash"></i></button>';
    box += '</div></div>';
    box += '<div class="box-body table-responsive">';
    box += '<table class="table table-hover">';
    box += '<thead>';
    box += '<tr>';
    box += '<th>Produkt</th><th>Ilość</th><th>Jednostka</th><th>Wartość</th><th>Kwota</th><th></th>';
    box += '</tr>';
    box += '</thead>';
    box += '<tbody></tbody>';
    box += '</table>';
    box += '</div>';
    box += '<div class="box-footer">';
    box += '<div class="input-group col-md-5 col-md-offset-7">';
    box += '<select class="form-control select2" name="AddTask" id="AddTask-' + no + '" data-placeholder="Wybierz zadanie">';
    box += elms;
    box += '</select>';
    box += '<div class="input-group-btn">';
    box += '<button type="submit" class="btn btn-default AddTaskBtn" data-apartment-number="' + no + '" data-invoice-id="' + inv + '"  title="Dodaj"><i class="fa fa-plus"></i></button>';
    box += '</div>';
    box += '</div>';
    box += '</div>';
    box += '</div>';

    return box;
}