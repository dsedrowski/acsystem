﻿function PrintURL(url) {
    $.LoadingOverlay('show');
    AddIFrameToPage(url);
}

function PrintURLPDF(url, id) {
    $.LoadingOverlay('show');
    AddIFrameToPage(url, id);

    document.getElementById('PrintPageIFrame' + id).focus();
    document.getElementById('PrintPageIFrame' + id).contentWindow.print();
}

function AddIFrameToPage(url, id) {
    var sufix = (id !== null) ? id : "";
    if (!$('#PrintPageIFrame' + sufix)[0]) {
        $("body").append(WriteIFrame(url, sufix));
        $('#PrintPageIFrame' + sufix).on("load", function () {
            PrintInit();
            $.LoadingOverlay('hide');
        });
    }
    else {
        $('#PrintPageIFrame').attr("src", url);
    }
}

function WriteIFrame(url, id) {
    return '<iframe id="PrintPageIFrame' +  id +'" name="PrintPageIFrame" src=' + url + ' style="position: absolute; top: -1000px; @media print { display: block; }"></iframe>';
}

function PrintInit() {
    var selector = "#PrintPageIFrame";
}