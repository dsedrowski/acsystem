﻿using ACSystem.Reports;
using ACSystem.Reports.ReportsDataSetTableAdapters;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ACSystem.ReportingService
{
    public class PlaceError
    {
        public static bool ClientReport(int PlaceErrorID, string userSign, DataTable Photos, Assembly assembly, string savePath, out Warning[] warnings, out string errorMessage)
        {
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            try
            {
                LocalReport report = new LocalReport();
                report.EnableExternalImages = true;
                report.DataSources.Clear();

                ReportDataSource photos = new ReportDataSource();
                photos.Name = "ErrorImages";
                photos.Value = Photos;
                report.DataSources.Add(photos);

                ReportsDataSet dataSet = new ReportsDataSet();
                dataSet.EnforceConstraints = false;
                dataSet.BeginInit();

                ReportDataSource error = new ReportDataSource();
                error.Name = "ReportsDataSet";
                error.Value = dataSet.PlaceError;
                report.DataSources.Add(error);

                dataSet.EndInit();

                Stream stream = assembly.GetManifestResourceStream("ACSystem.Reports.PlaceError.ClientReport.rdlc");
                report.LoadReportDefinition(stream);

                PlaceErrorTableAdapter tableAdapter = new PlaceErrorTableAdapter();
                tableAdapter.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                tableAdapter.Fill(dataSet.PlaceError, PlaceErrorID);

                ReportParameter[] parameters = new ReportParameter[1];
                parameters[0] = new ReportParameter("FitterSign", userSign);
                report.SetParameters(parameters);

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                System.IO.File.WriteAllBytes(savePath, bytes);

                errorMessage = "";
                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "ClientReport", "");
                errorMessage = ex.Message;
                warnings = null;

                return false;
            }
        }

    }
}