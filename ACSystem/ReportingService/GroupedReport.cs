﻿using ACSystem.Reports;
using ACSystem.Reports.ReportsDataSetTableAdapters;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ACSystem.ReportingService
{
    public class GroupedReport
    {
        public static bool Report(string IDs, Assembly assembly, string savePath, out Warning[] warnings, out string errorMessage)
        {
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            try
            {
                LocalReport report = new LocalReport();
                report.EnableExternalImages = true;
                report.DataSources.Clear();

                ReportsDataSet dataSet = new ReportsDataSet();
                dataSet.EnforceConstraints = false;
                dataSet.BeginInit();

                ReportDataSource grouped = new ReportDataSource();
                grouped.Name = "ReportsDataSet";
                grouped.Value = dataSet.GroupReportData;
                report.DataSources.Add(grouped);

                //dataSet.EndInit();

                Stream stream = assembly.GetManifestResourceStream("ACSystem.Reports.GroupReports.GroupedReport.rdlc");
                report.LoadReportDefinition(stream);

                GroupReportDataTableAdapter tableAdapter = new GroupReportDataTableAdapter();
                tableAdapter.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                tableAdapter.Fill(dataSet.GroupReportData, IDs);

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                System.IO.File.WriteAllBytes(savePath, bytes);

                errorMessage = "";
                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "ClientReport", "");
                errorMessage = ex.Message;
                warnings = null;

                return false;
            }
        }

        public static bool ServicesWithCode(int ProjectID, Assembly assembly, string savePath, out Warning[] warnings, out string errorMessage) {
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            try {
                LocalReport report = new LocalReport();
                report.EnableExternalImages = true;
                report.DataSources.Clear();

                ReportsDataSet dataSet = new ReportsDataSet();
                dataSet.EnforceConstraints = false;
                dataSet.BeginInit();

                ReportDataSource dataSource = new ReportDataSource();
                dataSource.Name = "ReportsDataSet";
                dataSource.Value = dataSet.GetServicesWithCodeForProject;
                report.DataSources.Add(dataSource);

                Stream stream = assembly.GetManifestResourceStream("ACSystem.Reports.ServicesWithCode.ServicesWithCode.rdlc");
                report.LoadReportDefinition(stream);

                GetServicesWithCodeForProjectTableAdapter tableAdapter = new GetServicesWithCodeForProjectTableAdapter();
                tableAdapter.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                tableAdapter.Fill(dataSet.GetServicesWithCodeForProject, ProjectID);

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                System.IO.File.WriteAllBytes(savePath, bytes);

                errorMessage = "";
                return true;
            } catch (Exception ex) {
                Core.Service.Helpers.Log(ex, "ServicesWithCode", "");
                errorMessage = ex.Message;
                warnings = null;

                return false;
            }
        }
    }
}