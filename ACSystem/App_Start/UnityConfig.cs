using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using ACSystem.Controllers;
using ACSystem.Core.IRepo;
using ACSystem.Core.Repo;
using ACSystem.Core.Models;
using ACSystem.Services;
using ACSystem.Repositories;

namespace ACSystem.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion
         
        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<AccountController>(new InjectionConstructor());
            container.RegisterType<ManageController>(new InjectionConstructor());
            container.RegisterType<IACSystemContext, ACSystemContext>(new PerRequestLifetimeManager());
            container.RegisterType<IAccountRepo, AccountRepo>(new PerRequestLifetimeManager());
            container.RegisterType<ICustomersRepo, CustomersRepo>(new PerRequestLifetimeManager());
            container.RegisterType<IProjectsRepo, ProjectsRepo>(new PerRequestLifetimeManager());
            container.RegisterType<IInventoryRepo, InventoryRepo>(new PerRequestLifetimeManager());
            container.RegisterType<IProductsRepo, ProductsRepo>(new PerRequestLifetimeManager());
            container.RegisterType<ITasksRepo, TasksRepo>(new PerRequestLifetimeManager());
            container.RegisterType<IHomeRepo, HomeRepo>(new PerRequestLifetimeManager());
            container.RegisterType<IOfficeRepo, OfficeRepo>(new PerRequestLifetimeManager());

            //nowe serwisy i repo

            #region Services

            container.RegisterType<IProjectService, ProjectService>(new PerRequestLifetimeManager());
            container.RegisterType<ISettingsService, SettingService>(new PerRequestLifetimeManager());

            #endregion

            #region Repositories

            container.RegisterType<IApartmentRepository, ApartmentRepository>(new PerRequestLifetimeManager());
            container.RegisterType<IEventRepository, EventRepository>(new PerRequestLifetimeManager());
            container.RegisterType<IFeeRepository, FeeRepository>(new PerRequestLifetimeManager());
            container.RegisterType<IInventoryRepository, InventoryRepository>(new PerRequestLifetimeManager());
            container.RegisterType<IInvoiceRepository, InvoiceRepository>(new PerRequestLifetimeManager());
            container.RegisterType<IPayrollItemsRepository, PayrollItemsRepository>(new PerRequestLifetimeManager());
            container.RegisterType<IProjectRepository, ProjectRepository>(new PerRequestLifetimeManager());
            container.RegisterType<IProject_ProjectManagerRepository, Project_ProjectManagerRepository>(new PerRequestLifetimeManager());
            container.RegisterType<ISettingsRepository, SettingsRepository>(new PerRequestLifetimeManager());
            container.RegisterType<ITaskRepository, TaskRepository>(new PerRequestLifetimeManager());

            #endregion
        }
    }
}
