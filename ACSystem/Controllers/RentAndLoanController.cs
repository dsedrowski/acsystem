﻿using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels.Select;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ACSystem.Controllers
{
    public class RentAndLoanController : Controller
    {
        // GET: RentAndLoan
        public ActionResult Index()
        {
            return View();
        }

        #region CREATE
        public ActionResult Create()
        {
            using (var db = new ACSystemContext())
            {
                #region LISTA MONTERÓW
                var fitters = db.Fitters.Select(x => new FittersSelect { ID = x.Id, Name = x.User.Name, SurName = x.User.Surname, IsLocked = x.User.LockoutEnabled }).ToList();
                ViewBag.Fitters = fitters;
                #endregion

                return View();
            }
        }

        [HttpPost]
        public ActionResult Create(RentAndLoan _model)
        {
            using (var db = new ACSystemContext())
            {

                if (ModelState.IsValid)
                {
                    try
                    {
                        _model.LeftAmount = _model.FullAmount;
                        db.RentAndLoan.Add(_model);

                        db.SaveChanges();

                        return Redirect("/RentAndLoan");
                    }
                    #region CATCH
                    catch (DbEntityValidationException ex)
                    {
                        foreach (var validationErrors in ex.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                ModelState.AddModelError(validationError.PropertyName, validationError.ErrorMessage);
                            }
                        }

                        return View();
                    }
                    catch (Exception ex)
                    {
                        var log = new ExceptionsLog
                        {
                            Message = ex.Message,
                            Type = ex.GetType().ToString(),
                            Source = ex.Source,
                            StackTrace = ex.StackTrace,
                            URL = "CreateProject",
                            LogDate = DateTime.Now,
                            User = User.Identity.Name
                        };

                        db.ExceptionsLog.Add(log);

                        db.SaveChanges();
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    #endregion
                }
                else
                {
                    #region LISTA MONTERÓW
                    var fitters = db.Fitters.Select(x => new FittersSelect { ID = x.Id, Name = x.User.Name, SurName = x.User.Surname, IsLocked = x.User.LockoutEnabled }).ToList();
                    ViewBag.Fitters = fitters;
                    #endregion

                    return View();
                }
            }
        }

        #endregion

        #region EDIT

        public ActionResult Edit(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();
            
            var model = db.RentAndLoan.Find(ID.Value);

            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            model.Installments = db.Installment.Where(q => q.RentAndLoanID == ID.Value).ToList();

            ViewBag.Fitter = model.Fitter.User.FullName;

            var all = db.RentAndLoan.OrderBy(q => q.Fitter.User.Surname).ToList();
            var currentIndex = all.FindIndex(q => q.ID == ID.Value);

            int? prev = null, next = null;

            if (currentIndex > 0)
                prev = all[currentIndex - 1].ID;

            if (currentIndex < all.Count - 1)
                next = all[currentIndex + 1].ID;

            ViewBag.Prev = prev;
            ViewBag.Next = next;

            return View(model);            
        }

        [HttpPost]
        public ActionResult Edit(RentAndLoan _model)
        {
            using (var db = new ACSystemContext())
            {
                var model = db.RentAndLoan.Find(_model.ID);

                if (ModelState.IsValid)
                {
                    model.Description = _model.Description;
                    model.Frequency = _model.Frequency;
                    model.FullAmount = model.FullAmount + _model.AddAmount;
                    model.InstallmentAmount = _model.InstallmentAmount;
                    model.LeftAmount = model.LeftAmount + _model.AddAmount;
                    model.InterestRate = _model.InterestRate;

                    db.Entry(model).State = EntityState.Modified;

                    try
                    {
                        db.SaveChanges();

                        return Redirect("/RentAndLoan");
                    }
                    #region CATCH
                    catch (DbEntityValidationException ex)
                    {
                        foreach (var validationErrors in ex.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                ModelState.AddModelError(validationError.PropertyName, validationError.ErrorMessage);
                            }
                        }

                        return View(_model);
                    }
                    catch (Exception ex)
                    {
                        var log = new ExceptionsLog
                        {
                            Message = ex.Message,
                            Type = ex.GetType().ToString(),
                            Source = ex.Source,
                            StackTrace = ex.StackTrace,
                            URL = "EditRentAndLoan",
                            LogDate = DateTime.Now,
                            User = User.Identity.Name
                        };

                        db.ExceptionsLog.Add(log);

                        db.SaveChanges();
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    #endregion
                }
                else
                {                    
                    ViewBag.Fitter = model.Fitter.User.FullName;

                    return View(_model);
                }
            }            
        }

        #endregion

        #region DELETE

        public ActionResult Delete(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            using (var db = new ACSystemContext())
            {
                var model = db.RentAndLoan.Find(ID.Value);

                if (model == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                var elements = db.Installment.Where(q => q.RentAndLoanID == ID.Value);

                db.Installment.RemoveRange(elements);
                db.RentAndLoan.Remove(model);

                try
                {
                    db.SaveChanges();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                #region CATCH
                catch (DbEntityValidationException ex)
                {
                    foreach (var validationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            ModelState.AddModelError(validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }

                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var log = new ExceptionsLog
                    {
                        Message = ex.Message,
                        Type = ex.GetType().ToString(),
                        Source = ex.Source,
                        StackTrace = ex.StackTrace,
                        URL = "EditRentAndLoan",
                        LogDate = DateTime.Now,
                        User = User.Identity.Name
                    };

                    db.ExceptionsLog.Add(log);

                    db.SaveChanges();
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                #endregion
            }
        }

        #endregion

        #region AJAX 

        [HttpGet]
        public ActionResult List(int? page, string orderBy = "number", string dest = "desc", string search = "", string filter = "0,1,2")
        {
            using (var db = new ACSystemContext())
            {
                int pageSize = 20, 
                    rowsCount = db.RentAndLoan.Count(), 
                    pagesCount = rowsCount / pageSize;

                if (rowsCount <= pageSize || page <= 0 || !page.HasValue)
                    page = 1;

                int excludeRows = (page.Value - 1) * pageSize,
                    rows = 0;

                var list = GetList(db, out rows, excludeRows, pageSize, orderBy, dest, search, filter);

                pagesCount = (rows % pageSize == 0) ? rows / pageSize : (rows / pageSize) + 1;

                if (pagesCount <= 0) pagesCount = 1;

                return Json(new { data = list, pages = pagesCount }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public ActionResult HandyRepayment(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var model = db.RentAndLoan.Find(ID.Value);

            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var installment = new Installment
            {
                RentAndLoanID = model.ID,
                PayrollID = null,
                ChargeAmount = model.LeftAmount,
                Timestamp = DateTime.Now
            };

            db.Installment.Add(installment);

            model.LeftAmount = 0;

            db.Entry(model).State = EntityState.Modified;

            try
            {
                db.SaveChanges();

                return Redirect("/RentAndLoan");
            }
            #region CATCH
            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ModelState.AddModelError(validationError.PropertyName, validationError.ErrorMessage);
                    }
                }

                return Redirect("/RentAndLoan");
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "HandyRepayment",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            #endregion
        }

        #endregion

        #region SERVICES 

        private List<RentAndLoanViewModel> GetList(ACSystemContext db, out int rows, int skip, int take, string orderBy, string dest, string search, string filters)
        {
            rows = 0;

            if (string.IsNullOrEmpty(filters)) return null;

            List<int> filter = new List<int>();

            foreach (var fl in filters.Split(','))
            {
                filter.Add(int.Parse(fl));
            }

            List<RentAndLoanViewModel> query = db.RentAndLoan.Select(
                                                                            q => new RentAndLoanViewModel
                                                                            {
                                                                                ID = q.ID,
                                                                                FitterID = q.FitterID,
                                                                                FitterName = q.Fitter.User.Name,
                                                                                FitterSurname = q.Fitter.User.Surname,
                                                                                Frequency = q.Frequency,
                                                                                Description = q.Description,
                                                                                FullAmount = q.FullAmount,
                                                                                InstallmentAmount = q.InstallmentAmount,
                                                                                LeftAmount = q.LeftAmount,
                                                                                Currency = q.Currency
                                                                            }).ToList();

            switch (orderBy)
            {
                case "number":
                    query = (dest == "asc") ? query.OrderBy(q => q.ID).ToList() : query.OrderByDescending(q => q.ID).ToList();
                    break;
                case "fitter":
                    query = (dest == "asc") ? query.OrderBy(q => q.FitterFullName).ToList() : query.OrderByDescending(q => q.FitterFullName).ToList();
                    break;
                case "full-amount":
                    query = (dest == "asc") ? query.OrderBy(q => q.FullAmount).ToList() : query.OrderByDescending(q => q.FullAmount).ToList();
                    break;
                case "installment-amount":
                    query = (dest == "asc") ? query.OrderBy(q => q.InstallmentAmount).ToList() : query.OrderByDescending(q => q.InstallmentAmount).ToList();
                    break;
                case "left-amount":
                    query = (dest == "asc") ? query.OrderBy(q => q.LeftAmount).ToList() : query.OrderByDescending(q => q.LeftAmount).ToList();
                    break;
                case "currency":
                    query = (dest == "asc") ? query.OrderBy(q => q.Currency).ToList() : query.OrderByDescending(q => q.Currency).ToList();
                    break;
                case "status":
                    query = (dest == "asc") ? query.OrderBy(q => q.Status).ToList() : query.OrderByDescending(q => q.Status).ToList();
                    break;
                default:
                    query = (dest == "asc") ? query.OrderBy(q => q.ID).ToList() : query.OrderByDescending(q => q.ID).ToList();
                    break;
            }

            query = query.Where(q => (q.FitterName.Contains(search)|| q.Description.Contains(search)) && filter.Contains(q.Status)).ToList();

            rows = query.Count();

            query = query.Skip(skip).Take(take).ToList();

            return query;     
        }

        #endregion
    }
}