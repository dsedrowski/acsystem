﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ACSystem.Core.Models;
using ACSystem.ViewModels;
using System.Collections.Generic;
using ACSystem.Core.IRepo;
using ACSystem.Core.Repo;
using System.Net;
using System.IO;
using System;
using ACSystem.Core.Service;
using ACSystem.Core.Filters;
using ACSystem.Core;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Security;

namespace ACSystem.Controllers
{

    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ACSystemUserManager _userManager;

        /// <summary>
        /// Klasa repozytorium, zawierająca część logiczną aplikacji
        /// </summary>
        private readonly IAccountRepo _repo;

        public AccountController()
        {
            _repo = new AccountRepo();
        }

        public AccountController(IAccountRepo repo)
        {
            _repo = new AccountRepo();
        }

        public AccountController(ACSystemUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ACSystemUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ACSystemUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// Stronga główna modułu, zawierająca liste użytkowników
        /// </summary>
        [AuthLog(Roles = "Admin")]
        public ActionResult List()
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista użytkowników", Url = "Account/List", Icon = "glyphicon glyphicon-user", Active = true });

            return View();
        }

        /// <summary>
        /// Funkcja wykorzystywana przez AJAX do przetworzenia listy użytkowników
        /// </summary>
        /// <param name="page">Aktualna strona tabeli</param>
        /// <param name="orderBy">Pole, po którym tabela ma być sortowana</param>
        /// <param name="dest">Kierunek sortowania</param>
        /// <param name="search">Fraza, po której mają zostać wyszukane rekordy</param>
        /// <returns>JSON z listą użytkowników, liczbę stron tabeli</returns>
        [HttpGet]
        [AuthLog(Roles = "Admin")]
        public ActionResult UsersList(int? page, string orderBy = "name", string dest = "asc", string search = "", int filter = -1)
        {

            var db = new ACSystemContext();

            var pageSize = 20;

            var rowsCount = db.ACSystemUser.Count();

            var pagesCount = rowsCount / pageSize;

            if (rowsCount <= pageSize || page <= 0 || page == null) page = 1;

            var excludeRows = (page - 1) * pageSize;

            var rows = 0;

            IQueryable<UsersListViewModel> query = db.ACSystemUser.Select(p => new UsersListViewModel { Id = p.Id, Name = p.Name, Surname = p.Surname, Email = p.Email, PhoneNumber = p.PhoneNumber, LockoutEnabled = p.LockoutEnabled });
            IQueryable<UsersListViewModel> query_where = null;

            switch (orderBy)
            {
                case "surname":
                    if (dest == "asc")
                        query_where = query.OrderBy(c => c.Surname)
                                    .Where(c => c.Name.Contains(search) || c.Surname.Contains(search) || c.Email.Contains(search) || c.PhoneNumber.Contains(search));
                    else
                        query_where = query.OrderByDescending(c => c.Surname)
                                    .Where(c => c.Name.Contains(search) || c.Surname.Contains(search) || c.Email.Contains(search) || c.PhoneNumber.Contains(search));
                    break;
                case "name":
                    if (dest == "asc")
                        query_where = query.OrderBy(c => c.Name)
                                    .Where(c => c.Name.Contains(search) || c.Surname.Contains(search) || c.Email.Contains(search) || c.PhoneNumber.Contains(search));
                    else
                        query_where = query.OrderByDescending(c => c.Name)
                                    .Where(c => c.Name.Contains(search) || c.Surname.Contains(search) || c.Email.Contains(search) || c.PhoneNumber.Contains(search));
                    break;
                case "email":
                    if (dest == "asc")
                        query_where = query.OrderBy(c => c.Email)
                                    .Where(c => c.Name.Contains(search) || c.Surname.Contains(search) || c.Email.Contains(search) || c.PhoneNumber.Contains(search));
                    else
                        query_where = query.OrderByDescending(c => c.Email)
                                    .Where(c => c.Name.Contains(search) || c.Surname.Contains(search) || c.Email.Contains(search) || c.PhoneNumber.Contains(search));
                    break;
                case "phonenumber":
                    if (dest == "asc")
                        query_where = query.OrderBy(c => c.PhoneNumber)
                                    .Where(c => c.Name.Contains(search) || c.Surname.Contains(search) || c.Email.Contains(search) || c.PhoneNumber.Contains(search));
                    else
                        query_where = query.OrderByDescending(c => c.PhoneNumber)
                                    .Where(c => c.Name.Contains(search) || c.Surname.Contains(search) || c.Email.Contains(search) || c.PhoneNumber.Contains(search));
                    break;
                default:
                    if (dest == "asc")
                        query_where = query.OrderBy(c => c.Surname)
                                    .Where(c => c.Name.Contains(search) || c.Surname.Contains(search) || c.Email.Contains(search) || c.PhoneNumber.Contains(search));
                    else
                        query_where = query.OrderByDescending(c => c.Surname)
                                    .Where(c => c.Name.Contains(search) || c.Surname.Contains(search) || c.Email.Contains(search) || c.PhoneNumber.Contains(search));
                    break;
            }

            if (filter != -1)
                query_where = query_where.Where(q => q.LockoutEnabled || !q.LockoutEnabled);
            else
                query_where = query_where.Where(q => !q.LockoutEnabled);

            rows = query_where.Count();

            var result = query_where.Skip((int)excludeRows).Take(pageSize);

            pagesCount = (rows % pageSize == 0) ? rows / pageSize : (rows / pageSize) + 1;

            if (pagesCount <= 0) pagesCount = 1;

            return Json(new { data = result.ToList(), pages = pagesCount }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }

            var appContext = new ACSystemContext();
            var userManager = new UserManager<ACSystemUser>(new UserStore<ACSystemUser>(appContext));

            var userApp = userManager.FindByEmail(model.Email);

            if (userApp.LockoutEnabled)
            {
                ModelState.AddModelError("", "Twoje konto zostało zablokowane.");
                return PartialView();
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return PartialView(model);
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes.
            // If a user enters incorrect codes for a specified amount of time then the user account
            // will be locked out for a specified amount of time.
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        /// <summary>
        /// Funkcja inicjująca formularz tworzenia użytkownika
        /// </summary>

        [AllowAnonymous]
        public ActionResult Create()
        {
            var db = new ACSystemContext();

            var roles = new List<string>();

            //foreach (var role in db.Roles)
            //{
            //    roles.Add(role.Name);
            //}

            ViewBag.Roles = roles;

            var positions = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem { Text = "Biuro", Value = "0" },
                    new SelectListItem { Text = "Monter", Value = "1" },
                },
                "Value", "Text", 0
            );

            var userTypes = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem {Text = "Wewnętrzny", Value = "0"},
                    new SelectListItem {Text = "Zewnętrzny", Value = "1"}
                },
                "Value", "Text", "0"
            );

            var percentTypes = new SelectList(new List<SelectListItem>
            {
                new SelectListItem {Text = "Stawka 1", Value = "1" },
                new SelectListItem {Text = "Stawka 2", Value = "2" },
                new SelectListItem {Text = "Stawka 3", Value = "3" },
                new SelectListItem {Text = "Stawka 4", Value = "4" },
                new SelectListItem {Text = "Stawka 5", Value = "5" },
                new SelectListItem {Text = "Stawka 6", Value = "6" },
            }, "Value", "Text", "1");

            ViewBag.Positions = positions;
            ViewBag.PercentTypes = percentTypes;
            ViewBag.UserTypes = userTypes;

            var calendars = db.Calendar.ToList();
            ViewBag.Calendars = calendars;

            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista użytkowników", Url = "/Account/List", Icon = "glyphicon glyphicon-user", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Dodaj użytkownika", Url = "/Account/Create", Icon = "", Active = true });

            return View(new RegisterViewModel());
        }

        /// <summary>
        /// Przetwarza wysłane metodą POST dane, po poprawnym zwalidowaniu tworzy użytkownika
        /// </summary>
        /// <param name="model">Model zawierający dane użytkownika potrzebne do rejestracji</param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> Create(RegisterViewModel model)
        {
            var db = new ACSystemContext();

            var roles = new List<string>();
            var userRoles = new List<string>();

            ViewBag.Roles = roles;

            var positions = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem { Text = "Biuro", Value = "0" },
                    new SelectListItem { Text = "Monter", Value = "1" },
                },
                "Value", "Text", 0
            );

            var userTypes = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem {Text = "Wewnętrzny", Value = "0"},
                    new SelectListItem {Text = "Zewnętrzny", Value = "1"}
                },
                "Value", "Text", "0"
            );

            var percentTypes = new SelectList(new List<SelectListItem>
            {
                new SelectListItem {Text = "Stawka 1", Value = "1" },
                new SelectListItem {Text = "Stawka 2", Value = "2" },
                new SelectListItem {Text = "Stawka 3", Value = "3" },
                new SelectListItem {Text = "Stawka 4", Value = "4" },
                new SelectListItem {Text = "Stawka 5", Value = "5" },
                new SelectListItem {Text = "Stawka 6", Value = "6" },
            }, "Value", "Text", "1");

            ViewBag.Positions = positions;
            ViewBag.PercentTypes = percentTypes;
            ViewBag.UserTypes = userTypes;

            var calendars = db.Calendar.ToList();
            ViewBag.Calendars = calendars;

            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista użytkowników", Url = "/Account/List", Icon = "glyphicon glyphicon-user", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Dodaj użytkownika", Url = "/Account/Create", Icon = "", Active = true });

            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(model.Image))
                    model.Image = "no-photo.png";

                DateTime? lockTimestamp = null;

                if (model.LockoutEnabled)
                    lockTimestamp = DateTime.Now;

                if (string.IsNullOrEmpty(model.Password))
                {
                    try
                    {
                        model.Password = Membership.GeneratePassword(12, 1);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }

                var user = new ACSystemUser {
                    UserName = model.Email,
                    Name = model.Name,
                    Surname = model.Surname,
                    DateOfBirth = model.DateOfBirth,
                    Position = model.Position,
                    Email = model.Email,
                    PhoneNumber = model.PhoneNumber,
                    Image = model.Image,
                    CalendarID = model.CalendarID,
                    UserType = model.UserType,
                    Podpis = model.Podpis,
                    InSystemUserID = model.InSystemUserID,
                    LockoutEnabled = model.LockoutEnabled,
                    LockTimestamp = lockTimestamp,
                    CanSeeAll = model.CanSeeAll,
                    FirstDoService = model.FirstDoService,
                    HireDate = model.HireDate,
                    ReleaseDate = model.ReleaseDate
                };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    if (user.Position == Position.Worker)
                        await UserManager.AddToRoleAsync(user.Id, "Admin");
                    else
                        await UserManager.AddToRoleAsync(user.Id, "Użytkownicy");

                    if (user.Position == Position.Fitter && user.Fitter == null)
                    {
                        var fitter = new Fitters
                        {
                            Id = user.Id,
                            PercentType = (int)model.PercentType,
                            HourlyRate = model.HourlyRate
                        };

                        _repo.AddFitter(fitter);

                    }
                    else
                    {
                        if (user.Fitter != null)
                            _repo.DeleteFitter(user.Fitter);
                    }

                    //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    //string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    //await UserManager.SendEmailAsync(user.Id, "ACService - Potwierdź email", "Potwierdź swój email klikając <a href=\"" + callbackUrl + "\">tutaj</a>");

                    var _exception = "";
                    MailService.SendPassword(model.Email, model.Name, model.Password, out _exception);

                    return RedirectToAction("List", "Account");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        /// <summary>
        /// Funkcja inicjująca formularz edycji użytkownika
        /// </summary>
        /// <param name="id">Id użytkownika</param>
        /// <returns>Zwraca model z danymi dla użytkownika o podanym ID</returns>
        [AuthLog(Roles = "Admin")]
        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var editUserViewModel = _repo.UserById(id);

            var db = new ACSystemContext();

            var roles = new List<string>();
            var userRoles = new List<string>();

            ViewBag.Roles = roles;


            var positions = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem { Text = "Biuro", Value = "0" },
                    new SelectListItem { Text = "Monter", Value = "1" },
                },
                "Value", "Text", 0
            );

            var userTypes = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem {Text = "Wewnętrzny", Value = "0"},
                    new SelectListItem {Text = "Zewnętrzny", Value = "1"}
                },
                "Value", "Text", "0"
            );

            var percentTypes = new SelectList(new List<SelectListItem>
            {
                new SelectListItem {Text = "Stawka 1", Value = "1" },
                new SelectListItem {Text = "Stawka 2", Value = "2" },
                new SelectListItem {Text = "Stawka 3", Value = "3" },
                new SelectListItem {Text = "Stawka 4", Value = "4" },
                new SelectListItem {Text = "Stawka 5", Value = "5" },
                new SelectListItem {Text = "Stawka 6", Value = "6" },
            }, "Value", "Text", editUserViewModel.PercentType?.ToString());

            percentTypes.Where(q => q.Value == editUserViewModel.PercentType?.ToString()).FirstOrDefault().Selected = true;

            ViewBag.Positions = positions;
            ViewBag.PercentTypes = percentTypes;
            ViewBag.UserTypes = userTypes;

            var calendars = db.Calendar.ToList();
            ViewBag.Calendars = calendars;

            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista użytkowników", Url = "/Account/List", Icon = "glyphicon glyphicon-user", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Edytuj użytkownika", Url = "/Account/Edit", Icon = "", Active = true });

            return View(editUserViewModel);
        }

        /// <summary>
        /// Przetwarza wyslane metodą POST dane, po poprawnej walidacji zapisuje zmiany użytkownika
        /// </summary>
        /// <param name="model">Model zawierający dane użytkownika potrzebne do jego edycji</param>
        [HttpPost]
        [AuthLog(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditUserViewModel model)
        {
            var editUserViewModel = _repo.UserById(model.Id);

            var db = new ACSystemContext();

            var roles = new List<string>();
            var currentRoles = new List<string>();
            var userRoles = new List<string>();

            //foreach (var role in model.Role)
            //{
            //    userRoles.Add(db.Roles.Where(r => r.Name == role).FirstOrDefault().Name);
            //}

            foreach (var role in editUserViewModel.Role)
            {
                currentRoles.Add(db.Roles.Where(r => r.Id == role).FirstOrDefault().Name);
            }

            //foreach (var role in db.Roles)
            //{
            //    roles.Add(role.Name);
            //}

            //model.Role = userRoles;

            ViewBag.Roles = roles;


            var positions = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem { Text = "Biuro", Value = "0" },
                    new SelectListItem { Text = "Monter", Value = "1" },
                },
                "Value", "Text", editUserViewModel.Position
            );

            var userTypes = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem {Text = "Wewnętrzny", Value = "0"},
                    new SelectListItem {Text = "Zewnętrzny", Value = "1"}
                },
                "Value", "Text", "0"
            );

            var percentTypes = new SelectList(new List<SelectListItem>
            {
                new SelectListItem {Text = "Stawka 1", Value = "1" },
                new SelectListItem {Text = "Stawka 2", Value = "2" },
                new SelectListItem {Text = "Stawka 3", Value = "3" },
                new SelectListItem {Text = "Stawka 4", Value = "4" },
                new SelectListItem {Text = "Stawka 5", Value = "5" },
                new SelectListItem {Text = "Stawka 6", Value = "6" },
            }, "Value", "Text", editUserViewModel.PercentType?.ToString());

            ViewBag.Positions = positions;
            ViewBag.PercentTypes = percentTypes;
            ViewBag.UserTypes = userTypes;

            var calendars = db.Calendar.ToList();
            ViewBag.Calendars = calendars;

            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista użytkowników", Url = "/Account/List", Icon = "glyphicon glyphicon-user", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Dodaj użytkownika", Url = "/Account/Create", Icon = "", Active = true });


            if (ModelState.IsValid)
            {
                var user = (ACSystemUser)UserManager.FindById(model.Id);

                if (!user.LockoutEnabled && model.LockoutEnabled)
                    user.LockTimestamp = DateTime.Now;
                else if (user.LockoutEnabled && !model.LockoutEnabled)
                    user.LockTimestamp = null;

                user.UserName = model.Email;
                user.Name = model.Name;
                user.Surname = model.Surname;
                user.DateOfBirth = model.DateOfBirth;
                user.Position = model.Position;
                user.Email = model.Email;
                user.PhoneNumber = model.PhoneNumber;
                user.Image = model.Image;
                user.CalendarID = model.CalendarID;
                user.UserType = model.UserType;
                user.Podpis = model.Podpis;
                user.InSystemUserID = model.InSystemUserID;
                user.LockoutEnabled = model.LockoutEnabled;
                user.CanSeeAll = model.CanSeeAll;
                user.FirstDoService = model.FirstDoService;
                user.HireDate = model.HireDate;
                user.ReleaseDate = model.ReleaseDate;


                var result = await UserManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    await UserManager.RemoveFromRolesAsync(model.Id, currentRoles.ToArray());

                    if (user.Position == Position.Worker)
                        await UserManager.AddToRoleAsync(user.Id, "Admin");
                    else
                        await UserManager.AddToRoleAsync(user.Id, "Użytkownicy");


                    var fitter = new Fitters
                    {
                        Id = user.Id,
                        PercentType = (int)model.PercentType,
                        HourlyRate = model.HourlyRate
                    };

                    if (user.Position == Position.Fitter && user.Fitter != null)
                    {
                        _repo.EditFitter(fitter, User.Identity.GetUserId());
                    }
                    else
                    {
                        if (user.Fitter == null)
                            _repo.AddFitter(fitter);
                    }
                    return RedirectToAction("List", "Account");
                }
                AddErrors(result);
            }

            return View(model);
        }

        /// <summary>
        /// Generuje widok szczegółów użytkownika
        /// </summary>
        /// <param name="id">Id użytkownika</param>
        /// <returns>Zwraca model z danymi użytkownika</returns>
        [AuthLog(Roles = "Admin")]
        public ActionResult Details(string id)
        {
            if (string.IsNullOrEmpty(id))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var partUser = _repo.UserById(id);
            var user = _repo.FullUserById(id);

            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista użytkowników", Url = "/Account/List", Icon = "glyphicon glyphicon-user", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Edytuj użytkownika", Url = "/Account/Edit", Icon = "", Active = true });

            using (var db = new ACSystemContext())
            {
                var allAccounts = db.ACSystemUser.OrderBy(q => q.Surname).ToList();
                var currentIndex = allAccounts.FindIndex(q => q.Id == id);

                string prevAccount = null, nextAccount = null;

                if (currentIndex > 0)
                    prevAccount = allAccounts[currentIndex - 1].Id;

                if (currentIndex < allAccounts.Count - 1)
                    nextAccount = allAccounts[currentIndex + 1].Id;

                var fitters = db.ACSystemUser.Where(q => q.Crew_Members.Any() == false && q.LockoutEnabled == false).ToList();

                ViewBag.PrevAccount = prevAccount;
                ViewBag.NextAccount = nextAccount;
                ViewBag.Fitters = fitters;
            }

            return View(user);
        }

        public ActionResult SupervisorPercents(string SupervisorID, decimal? CompanyPercent, decimal? FitterPercent)
        {
            using (var db = new ACSystemContext())
            {
                var user = db.ACSystemUser.Find(SupervisorID);

                if (user == null)
                    return Json(new { success = false, error = "Nie znaleziono użytkownika o podanym ID." });

                user.CompanyPercent = CompanyPercent;
                user.FitterPercent = FitterPercent;

                try
                {
                    db.SaveChanges();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "", "");
                    return Json(new { success = false, error = "Wystąpił błąd podczas zapisywania stawek procentowych." });
                }
            }
        }

        public ActionResult GetPrevNextID(string current, string dest, string orderby, string search, string filter)
        {
            var db = new ACSystemContext();

            var showDisabled = filter.Contains("0");

            var users = db.ACSystemUser.Where(c => c.Name.Contains(search) || c.Surname.Contains(search) || c.Email.Contains(search) || c.PhoneNumber.Contains(search));

            if (filter.Contains("0"))
                users = users.Where(q => q.LockoutEnabled || !q.LockoutEnabled);
            else
                users = users.Where(q => !q.LockoutEnabled);

            switch (orderby)
            {
                case "surname":
                    if (dest == "asc")
                        users = users.OrderBy(c => c.Surname);
                    else
                        users = users.OrderByDescending(c => c.Surname);
                    break;
                case "name":
                    if (dest == "asc")
                        users = users.OrderBy(c => c.Name);
                    else
                        users = users.OrderByDescending(c => c.Name);
                    break;
                case "email":
                    if (dest == "asc")
                        users = users.OrderBy(c => c.Email);
                    else
                        users = users.OrderByDescending(c => c.Email);
                    break;
                case "phonenumber":
                    if (dest == "asc")
                        users = users.OrderBy(c => c.PhoneNumber);
                    else
                        users = users.OrderByDescending(c => c.PhoneNumber);
                    break;
                default:
                    if (dest == "asc")
                        users = users.OrderBy(c => c.Surname);
                    else
                        users = users.OrderByDescending(c => c.Surname);
                    break;
            }

            var usersList = users.ToList();

            var currentIndex = usersList.FindIndex(q => q.Id == current);

            string prevAccount = null, nextAccount = null;

            if (currentIndex > 0)
                prevAccount = usersList[currentIndex - 1].Id;

            if (currentIndex < usersList.Count - 1)
                nextAccount = usersList[currentIndex + 1].Id;

            return Json(new { prev = prevAccount, next = nextAccount }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Usuwa użytkownika o podanym numerze ID
        /// </summary>
        /// <param name="id">ID użytkownika</param>
        [HttpPost]
        [AuthLog(Roles = "Admin")]
        public async Task<ActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user = await UserManager.FindByIdAsync(id);
            var logins = user.Logins;
            var rolesForUser = await UserManager.GetRolesAsync(id);

            var context = new ACSystemContext();

            using (var transaction = context.Database.BeginTransaction())
            {
                foreach (var login in logins.ToList())
                {
                    await UserManager.RemoveLoginAsync(login.UserId, new UserLoginInfo(login.LoginProvider, login.ProviderKey));
                }

                if (rolesForUser.Count() > 0)
                {
                    foreach (var item in rolesForUser.ToList())
                    {
                        // item should be the name of the role
                        var result = await UserManager.RemoveFromRoleAsync(user.Id, item);
                    }
                }

                if (user.Fitter != null)
                    _repo.DeleteFitter(user.Fitter);

                if (user.Inventory_Fitter.Count > 0)
                    _repo.DeleteFitterInventory(user.Id);

                await UserManager.DeleteAsync(user);
                transaction.Commit();

                _repo.SaveChanges();
            }

            return Json(new { message = user.FullName }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Przetwarza i umieszcza na serwerze zdjęcie profilowe użytkownika
        /// </summary>
        /// <returns>Nazwę przetworzonego zdjęcia</returns>
        [HttpPost]
        [AuthLog(Roles = "Admin")]
        public ActionResult UploadImage()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var extension = new FileInfo(fileName);

                        newFileName += extension.Extension;

                        var filePath = Path.Combine(Server.MapPath("~/dist/Profile_Images/"), newFileName);
                        file.SaveAs(filePath);
                    }

                    return Json(new { result = "true", message = newFileName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = "false", message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        [AuthLog(Roles = "Admin")]
        public ActionResult UploadImagePodpis()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var extension = new FileInfo(fileName);

                        newFileName += extension.Extension;

                        var filePath = Path.Combine(Server.MapPath("~/Monter/dist/Podpisy/"), newFileName);
                        file.SaveAs(filePath);
                    }

                    return Json(new { result = "true", message = newFileName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = "false", message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadDocument(string UserID) {
            if (Request.Files.Count > 0) {
                using (ACSystemContext dtx = new ACSystemContext()) {
                    try {
                        HttpFileCollectionBase files = Request.Files;
                        string fileName = "";
                        string newFileName = Strings.RandomString(24);
                        List<UserDocuments> docs = new List<UserDocuments>();

                        for (int i = 0; i < files.Count; i++) {
                            HttpPostedFileBase file = files[i];

                            if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER") {
                                string[] testFiles = file.FileName.Split(new char[] { '\\' });
                                fileName = testFiles[testFiles.Length - 1];
                            } else {
                                fileName = file.FileName;
                            }

                            var fi = new FileInfo(fileName);
                            var ext = fi.Extension.ToUpper();
                            newFileName += fi.Extension;

                            var filePath = Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName);
                            file.SaveAs(filePath);

                            UserDocuments doc = new UserDocuments {
                                UserID = UserID,
                                URL = Path.Combine("/Monter/dist/Documents/", newFileName),
                                DocumentDescription = fileName,
                                ShowToUser = true
                            };
                            docs.Add(doc);
                        }

                        dtx.UserDocuments.AddRange(docs);
                        dtx.SaveChanges();

                        return Json(new { result = "true", docs = docs.ToArray() }, JsonRequestBehavior.AllowGet);
                    } catch (Exception ex) {
                        return Json(new { result = "false", message = ex.Message }, JsonRequestBehavior.AllowGet);
                    }
                }
            } else {
                return Json(new { result = "false", message = "Nie wybrano pliku." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost] public ActionResult UpdateDocument (int ID, string DocumentDescription, bool ShowToUser) {
            using (ACSystemContext dtx = new ACSystemContext()) {
                UserDocuments doc = dtx.UserDocuments.Find(ID);

                if (doc == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                doc.DocumentDescription = DocumentDescription;
                doc.ShowToUser = ShowToUser;
                dtx.Entry(doc).State = System.Data.Entity.EntityState.Modified;

                try {
                    dtx.SaveChanges();
                    return Json(true, JsonRequestBehavior.AllowGet);
                } catch (Exception ex) {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, HttpContext.User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        [HttpPost] public ActionResult DeleteDocument (int ID) {
            using (ACSystemContext dtx = new ACSystemContext()) {
                UserDocuments doc = dtx.UserDocuments.Find(ID);

                if (doc == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                dtx.UserDocuments.Remove(doc);

                try {
                    dtx.SaveChanges();
                    return Json(true, JsonRequestBehavior.AllowGet);
                } catch (Exception ex) {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, HttpContext.User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return PartialView();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    var _exception = "";
                    bool isSended = MailService.SendResetPasswordCode(user.Email, "", callbackUrl, out _exception);

                    if (!isSended)
                        return Json(new { message = _exception }, JsonRequestBehavior.AllowGet);

                    return RedirectToAction("ForgotPasswordConfirmation", "Account");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return PartialView(model);
        }

        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return PartialView();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? PartialView("Error") : PartialView();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return PartialView();
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return PartialView();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ACSystemUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }


        [HttpPost]
        public ActionResult AddCalendar(string Name, string ColorHex)
        {
            var db = new ACSystemContext();

            var calendar = new Calendar
            {
                Name = Name,
                ColorHex = ColorHex
            };

            db.Calendar.Add(calendar);

            try
            {
                db.SaveChanges();

                return Json(new { success = true, id = calendar.ID, name = calendar.Name, color = calendar.ColorHex }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddCrewMember(string SupervisorID, string MemberID)
        {
            if (string.IsNullOrEmpty(SupervisorID) || string.IsNullOrEmpty(MemberID))
                return Json(new { success = false, error = "Nie podano identyfikatora kierownika lub członka ekipy" }, JsonRequestBehavior.AllowGet);

            using (var db = new ACSystemContext())
            {
                var data = new Fitter_Crew
                {
                    SupervisorID = SupervisorID,
                    MemberID = MemberID
                };

                db.Fitter_Crew.Add(data);

                try
                {
                    db.SaveChanges();

                    return Json(new { success = true, MemberID, DataID = data.ID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "", "");
                    return Json(new { success = false, error = "Wystąpił błąd podczas dodawania członka ekipy." });
                }
            }
        }

        [HttpPost]
        public ActionResult DeleteCrewMember(int ID)
        {
            using (var db = new ACSystemContext())
            {
                var data = db.Fitter_Crew.Find(ID);

                if (data == null)
                    return Json(new { success = false, error = "Nie znaleziono wybranego wpisu." }, JsonRequestBehavior.AllowGet);

                db.Fitter_Crew.Remove(data);

                try
                {
                    db.SaveChanges();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "", "");
                    return Json(new { success = false, error = "Wystąpił błąd podczas usuwania członka." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        private void SeedRoles()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());

            if (!roleManager.RoleExists("Klienci"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Klienci";

                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Projekty"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Projekty";

                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Inwentarz"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Inwentarz";

                roleManager.Create(role);
            }
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}