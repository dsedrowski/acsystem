﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using ACSystem.Core.Models;
using ACSystem.Core.IRepo;
using ACSystem.ViewModels;
using System.Collections.Generic;
using ACSystem.Core.Models.ViewModels;
using System;
using ACSystem.Core.Filters;
using System.Linq;
using ACSystem.Core.Models.ViewModels.Select;
using ACSystem.FortnoxService;
using Microsoft.AspNet.Identity;

namespace ACSystem.Controllers
{
    [AuthLog(Roles = "Admin")]
    public class ProductsController : Controller
    {
        private readonly IProductsRepo _repo;

        public ProductsController(IProductsRepo repo)
        {
            _repo = repo;
        }

        // GET: Products
        public ActionResult Index()
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista produktów", Url = "/Products", Icon = "glyphicon glyphicon-list-alt", Active = true });

            return View();
        }

        [HttpGet]
        public ActionResult ProductsList(int? page, string orderBy = "numberOfMachine", string dest = "asc", string search = "", string filter = "")
        {
            var pageSize = 20;

            var rowsCount = _repo.Count();

            var pagesCount = rowsCount / pageSize;

            if (rowsCount <= pageSize || page <= 0 || page == null) page = 1;

            var excludeRows = (page - 1) * pageSize;

            var rows = 0;

            var list = _repo.List(out rows, excludeRows, pageSize, orderBy, dest, search, filter);

            pagesCount = (rows % pageSize == 0) ? rows / pageSize : (rows / pageSize) + 1;

            if (pagesCount <= 0) pagesCount = 1;

            return Json(new { data = list, pages = pagesCount }, JsonRequestBehavior.AllowGet);
        }

        // GET: Products/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista produktów", Url = "/Products", Icon = "glyphicon glyphicon-list-alt", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Szczegóły produktu", Url = "/Products/Details", Icon = "", Active = true });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products products = await _repo.GetProduct((int)id);
            if (products == null)
            {
                return HttpNotFound();
            }

            var db = new ACSystemContext();

            var all = db.Products.OrderBy(q => q.Name).ToList();
            var currentIndex = all.FindIndex(q => q.Id == id.Value);

            int? prev = null, next = null;

            if (currentIndex > 0)
                prev = all[currentIndex - 1].Id;

            if (currentIndex < all.Count - 1)
                next = all[currentIndex + 1].Id;

            ViewBag.Prev = prev;
            ViewBag.Next = next;

            return View(products);
        }

        public ActionResult GetPrevNextID(int current, string dest, string orderby, string search, string filter)
        {
            var db = new ACSystemContext();

            var showDisabled = filter.Contains("0");

            var query = db.Products.Where(p => p.Name.Contains(search));

            if (filter.Contains("0") == false)
                query = query.Where(q => q.IsActive);

            switch (orderby)
            {
                case "name":
                    if (dest == "asc")
                        query = query.OrderBy(p => p.Name);
                    else
                        query = query.OrderByDescending(p => p.Name);
                    break;
                case "price":
                    if (dest == "asc")
                        query = query.OrderBy(p => p.Price);
                    else
                        query = query.OrderByDescending(p => p.Price);
                    break;
                default:
                    if (dest == "asc")
                        query = query.OrderBy(p => p.Name);
                    else
                        query = query.OrderByDescending(p => p.Name);
                    break;
            }

            var list = query.ToList();

            var currentIndex = list.FindIndex(q => q.Id == current);

            int? prev = null, next = null;

            if (currentIndex > 0)
                prev = list[currentIndex - 1].Id;

            if (currentIndex < list.Count - 1)
                next = list[currentIndex + 1].Id;

            return Json(new { prev = prev, next = next }, JsonRequestBehavior.AllowGet);
        }


        // GET: Products/Create
        public ActionResult Create()
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista produktów", Url = "/Products", Icon = "glyphicon glyphicon-list-alt", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Dodaj produkt", Url = "/Products/Create", Icon = "", Active = true });

            var db = new ACSystemContext();

            ViewBag.Projects = db.Projects.Select(q => new ProjectsSelect { ID = q.Id, ProjectNumber = q.ProjectNumber }).ToList();

            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductsViewModel products)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista produktów", Url = "/Products", Icon = "glyphicon glyphicon-list-alt", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Dodaj produkt", Url = "/Products/Create", Icon = "", Active = true });

            if (ModelState.IsValid)
            {
                try
                {
                    var pr = _repo.Create(products);

#if !DEBUG
                    ArticleManager manager = new ArticleManager();
                    var inSystemID = manager.Create(new FortnoxAPILibrary.Article
                    {
                        Description = pr.DescriptionSE,
                        SalesPrice = pr.Price.ToString(),
                        Unit = pr.JmSwe.ToLower(),
                        Active = pr.IsActive.ToString().ToLower(),
                        Type = FortnoxAPILibrary.Connectors.ArticleConnector.ArticleType.SERVICE,
                        PurchasePrice = (pr.Price * (pr.PercentHeight.Type1 / 100)).ToString().Replace(',', '.')
                    });

                    pr.InSystemID = inSystemID.ArticleNumber;
                    _repo.Edit(pr, null);
#endif

                    _repo.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                    var db = new ACSystemContext();

                    ViewBag.Projects = db.Projects.Select(q => new ProjectsSelect { ID = q.Id, ProjectNumber = q.ProjectNumber }).ToList();

                    return View(products);
                }
            }
            else
            {
                var db = new ACSystemContext();

                ViewBag.Projects = db.Projects.Select(q => new ProjectsSelect { ID = q.Id, ProjectNumber = q.ProjectNumber }).ToList();

                return View(products);
            }
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista produktów", Url = "/Products", Icon = "glyphicon glyphicon-list-alt", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Edytuj produkt", Url = "/Products/Edit", Icon = "", Active = true });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductsViewModel products = _repo.GetProductViewModel((int)id);
            if (products == null)
            {
                return HttpNotFound();
            }

            var db = new ACSystemContext();

            ViewBag.Projects = db.Projects.Select(q => new ProjectsSelect { ID = q.Id, ProjectNumber = q.ProjectNumber }).ToList();
            ViewBag.Products = db.Products.Where(q => q.IsActive).Select(q => new ProductsSelect {ID = q.Id, Name = q.Name}).ToList();

            var all = db.Products.OrderBy(q => q.Name).ToList();
            var currentIndex = all.FindIndex(q => q.Id == id.Value);

            int? prev = null, next = null;

            if (currentIndex > 0)
                prev = all[currentIndex - 1].Id;

            if (currentIndex < all.Count - 1)
                next = all[currentIndex + 1].Id;

            ViewBag.Prev = prev;
            ViewBag.Next = next;

            return View(products);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductsViewModel productsVM)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista produktów", Url = "/Products", Icon = "glyphicon glyphicon-list-alt", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Edytuj produkt", Url = "/Products/Edit", Icon = "", Active = true });

            using (var db = new ACSystemContext())
            {

#if !DEBUG
                var manager = new ArticleManager();

                if (!string.IsNullOrEmpty(productsVM.InSystemID))
                {
                    var fortnox_article = manager.Get(productsVM.InSystemID);

                    fortnox_article.Description = productsVM.DescriptionSE;
                    fortnox_article.SalesPrice = productsVM.Price.ToString();
                    fortnox_article.Unit = productsVM.JmSwe.ToLower();
                    fortnox_article.Active = productsVM.IsActive.ToString().ToLower();
                    fortnox_article.PurchasePrice = (productsVM.Price * (productsVM.Type1 / 100)).ToString().Replace(',', '.');

                    manager.Update(fortnox_article);
                }
                else
                {
                    var inSystemID = manager.Create(new FortnoxAPILibrary.Article
                    {
                        Description = productsVM.DescriptionSE,
                        SalesPrice = productsVM.Price.ToString(),
                        Unit = productsVM.JmSwe.ToLower(),
                        Active = productsVM.IsActive.ToString().ToLower()
                    });

                    productsVM.InSystemID = inSystemID.ArticleNumber;
                }
#endif

                if (ModelState.IsValid)
                {

                    var pr = db.Products.Find(productsVM.Id);
                    var ph = db.PercentHeight.Find(productsVM.Id);

                    pr.Name = productsVM.Name;
                    pr.ProductType = productsVM.ProductType;
                    pr.DescriptionPL = productsVM.DescriptionPL;
                    pr.DescriptionEN = productsVM.DescriptionEN;
                    pr.DescriptionSE = productsVM.DescriptionSE;
                    pr.Price = productsVM.Price;
                    pr.Jm = productsVM.Jm;
                    pr.JmEng = productsVM.JmEng;
                    pr.JmSwe = productsVM.JmSwe;
                    pr.IsActive = productsVM.IsActive;
                    pr.ForAllProjects = productsVM.ForAllProjects;
                    pr.InSystemID = productsVM.InSystemID;
                    pr.NoExtra = productsVM.NoExtra;
                    pr.ExpectedTime = productsVM.ExpectedTime;
                    pr.TimePercent = productsVM.TimePercent;

                    if (ph == null)
                        ph = new PercentHeight
                        {
                            Id = productsVM.Id
                        };

                    ph.Type1 = productsVM.Type1;
                    ph.Type2 = productsVM.Type2;
                    ph.Type3 = productsVM.Type3;
                    ph.Type4 = productsVM.Type4;
                    ph.Type5 = productsVM.Type5;
                    ph.Type6 = productsVM.Type6;

                    db.Entry(pr).State = EntityState.Modified;
                    db.Entry(ph).State = EntityState.Modified;
                    
                    try
                    {
                        db.SaveChanges();

                        _repo.AddDefaultProductToProjects(pr, ph);
                    }
                    catch (Exception ex)
                    {
                        Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }

#region old
                    //_repo.Edit(product, productsVM.ConnectedProjects);
                    //_repo.Edit(ph);

                    //try
                    //{
                    //    _repo.SaveChanges();
                    //}
                    //catch(Exception ex)
                    //{
                    //    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                    //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    //}
#endregion

                    return RedirectToAction("Index");
                }

                ViewBag.Projects = db.Projects.Select(q => new ProjectsSelect { ID = q.Id, ProjectNumber = q.ProjectNumber }).ToList();


                var all = db.Products.OrderBy(q => q.Name).ToList();
                var currentIndex = all.FindIndex(q => q.Id == productsVM.Id);

                int? prev = null, next = null;

                if (currentIndex > 0)
                    prev = all[currentIndex - 1].Id;

                if (currentIndex < all.Count - 1)
                    next = all[currentIndex + 1].Id;

                ViewBag.Prev = prev;
                ViewBag.Next = next;

                return View(productsVM);
            }
        }

        // GET: Products/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = "";
            var pr = await _repo.GetProduct(id.Value);
            for (int i = 0; i < 3; i++)
            {
                result = await _repo.Delete((int)id);

                if (result.Split(';')[0] == "true")
                {
                    if (pr != null && !string.IsNullOrEmpty(pr.InSystemID))
                    {
                        var manager = new ArticleManager();
                        manager.Delete(pr.InSystemID);
                    }

                    break;
                }
            }

            var rsSplit = result.Split(';');

            if (rsSplit.Length > 1)
                return Json(new { success = rsSplit[0], message = rsSplit[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Nieoczekiwany błąd." }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> GetProduct(string productNewName, int? id, int? projectID)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var product = await _repo.GetProduct((int)id);


            var productName = product.Name;
            var productProject = product.Product_Project.FirstOrDefault(q => q.ProjectID == projectID);
            var price = (productProject != null) ? productProject.Price : product.Price;

            return Json(new
            {
                Price = price,
                Jm = product.Jm,
                ProductName = productName,
                Type1 = (productProject == null || !productProject.PercentType1.HasValue) ? product.PercentHeight.Type1 : productProject.PercentType1.Value,
                Type2 = (productProject == null || !productProject.PercentType2.HasValue) ? product.PercentHeight.Type2 : productProject.PercentType2.Value,
                Type3 = product.PercentHeight.Type3,
                Type4 = product.PercentHeight.Type4,
                Type5 = product.PercentHeight.Type5,
                Type6 = product.PercentHeight.Type6
            }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult ChangeProductName(string newProductName, int ID)
        //{
        //    var db = new ACSystemContext();
        //    var product = db.Products.FirstOrDefault(q => q.Id == ID);

        //    if (product.Name != newProductName) {
        //        product.Name = newProductName;
        //        db.SaveChanges();
        //    }
        //    return Json(new { newProductName = newProductName }, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult ChangeActive(int ID, bool IsActive)
        {
            var db = new ACSystemContext();

            var pr = db.Products.Find(ID);

            if (pr == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            pr.IsActive = IsActive;

            db.Entry(pr).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GroupProduct(int ProductID, int SubProductID, decimal Quantity, bool CreateTask)
        {
            var db = new ACSystemContext();

            var gp = new Product_Group
            {
                ProductMainID = ProductID,
                ProductSubID = SubProductID,
                Quantity = Quantity,
                CreateTask = CreateTask
            };

            var subProduct = db.Products.Find(SubProductID);

            db.Product_Group.Add(gp);

            try
            {
                db.SaveChanges();

                return Json(new { success = true, Product_GroupID = gp.Product_GroupID, ProductSubID = SubProductID, ProductID = ProductID, ProductName = subProduct.Name, Quantity = Quantity, Jm = subProduct.Jm, CreateTask = CreateTask }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "GroupProduct",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteGroupProduct(int GroupID)
        {
            var db = new ACSystemContext();

            var group = db.Product_Group.Find(GroupID);

            db.Product_Group.Remove(group);

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "DeleteGroupProduct",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult EditGroupProduct(int GroupID, decimal Quantity, bool CreateTask)
        {
            using (var db = new ACSystemContext())
            {
                var group = db.Product_Group.Find(GroupID);

                if (group == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                group.Quantity = Quantity;
                group.CreateTask = CreateTask;

                db.Entry(group).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {

            }
            base.Dispose(disposing);
        }
    }
}
