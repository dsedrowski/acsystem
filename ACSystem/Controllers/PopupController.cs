﻿using ACSystem.Core;
using ACSystem.Core.Extensions;
using ACSystem.Core.Models;
using ACSystem.Core.Service;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ACSystem.Controllers
{
    public class PopupController : Controller
    {
        #region EditActivity

        [HttpGet]
        public ActionResult EditActivity(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var activity = db.Task_Fitter.Find(ID.Value);

            if (activity == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            return View(activity);
        }

        [HttpPost]
        public ActionResult EditActivity(int Id, decimal WorkDone, decimal WorkTimeDecimal, TaskStatus Status, DateTime HandDateTime, bool IsActive)
        {
            var db = new ACSystemContext();

            var activity = db.Task_Fitter.Find(Id);

            activity.WorkDone = WorkDone;
            activity.WorkTime = (int)(WorkTimeDecimal * 60);
            activity.Status = Status;
            activity.HandDateTime = HandDateTime;
            activity.IsActive = IsActive;

            db.Entry(activity).State = EntityState.Modified;

            try
            {
                db.SaveChanges();

                ViewBag.Success = true;

                return View(activity);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "EdityActivity", User.Identity.Name);
                ViewBag.Success = false;

                return View(activity);
            }
        }

        #endregion

        #region Inventory

        public ActionResult AddChooseAccessory(int? ID)
        {
            var db = new ACSystemContext();

            var accessories = db.Inventory_Accessories.ToList();

            ViewBag.InventoryID = ID;

            return View("Inventory/AddChooseAccessory", accessories);
        }

        public ActionResult AddChooseWarehouse(int? ID)
        {
            var db = new ACSystemContext();

            var warehouses = db.InventoryWarehouse.ToList();

            ViewBag.InventoryID = ID;

            return View("Inventory/AddChooseWarehouse", warehouses);
        }

        public ActionResult AddChooseType()
        {
            var db = new ACSystemContext();

            var types = db.InventoryType.ToList();

            return View("Inventory/AddChooseType", types);
        }

        public ActionResult UploadInventoryFile(int? ID)
        {
            ViewBag.InventoryID = ID;

            return View("Inventory/UploadInventoryFile");
        }

        public ActionResult CheckCondition(int? ID)
        {
            ViewBag.InventoryID = ID;

            return View("Inventory/CheckCondition");
        }

        public ActionResult Report_InventoryFitterList()
        {
            var db = new ACSystemContext();

            var fitterList = db.Inventory_Fitter.Where(q => q.PutDate.HasValue == false && q.Inventory.Status == InventoryStatus.Busy).GroupBy(q => q.User).ToList();

            return View("Inventory/Report_InventoryFitterList", fitterList);
        }

        public ActionResult Report_InventoryProjectList()
        {
            var db = new ACSystemContext();

            var projectList = db.Inventory.Where(q => q.ProjectID.HasValue).GroupBy(q => q.Project).ToList();

            return View("Inventory/Report_InventoryProjectList", projectList);
        }

        public ActionResult Report_InventoryWarehouseList()
        {
            var db = new ACSystemContext();

            var warehouseList = db.Inventory.Where(q => q.WarehouseID.HasValue).GroupBy(q => q.Warehouse).ToList();

            return View("Inventory/Report_InventoryWarehouseList", warehouseList);
        }

        public ActionResult Report_InventoryAllList()
        {
            var db = new ACSystemContext();

            var allList = db.Inventory.GroupBy(q => q.Status).ToList();

            return View("Inventory/Report_InventoryAllList", allList);
        }

        public ActionResult Report_InventoryLifetimeList()
        {
            var db = new ACSystemContext();

            var lifetimeList = db.Inventory.Where(q => q.Status != InventoryStatus.Deleted).ToList();

            return View("Inventory/Report_InventoryLifetimeList", lifetimeList);
        }

        public ActionResult Report_InventoryTypeList()
        {
            var db = new ACSystemContext();

            var typeList = db.Inventory.Where(q => q.Status != InventoryStatus.Deleted).GroupBy(q => q.Type).ToList();

            return View("Inventory/Report_InventoryTypeList", typeList);
        }

        #region POST

        [HttpPost]
        public ActionResult CheckCondition(Inventory_Check check)
        {
            var db = new ACSystemContext();
            var inventory = db.Inventory.Find(check.ID);

            ViewBag.InventoryID = check.ID;

            var photo = new Inventory_Check_Photo
            {
                URL = Request.Form["check-photo"]
            };

            check.CheckDate = DateTime.Now;
            check.UserID = User.Identity.GetUserId();
            check.Photos.Add(photo);

            inventory.ConditionStatus = check.ConditionStatus;

            db.Inventory_Check.Add(check);
            db.Entry(inventory).State = EntityState.Modified;

            try
            {
                db.SaveChanges();

                ViewBag.Success = true;

                return View("Inventory/CheckCondition");
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "CheckCondition", User.Identity.Name);

                ViewBag.Success = false;

                return View("Inventory/CheckCondition", check);
            }
        }

        #endregion

        #region AJAX

        public ActionResult AJAX_CreateAccessory(string EvidenceNumber, string Name, int InventoryID, string URL)
        {
            var db = new ACSystemContext();

            var connect = new Inventory_Accessories_Connect();

            if (InventoryID > 0)
                connect = new Inventory_Accessories_Connect { InventoryID = InventoryID, Active = true };

            var accessory = new Inventory_Accessories
            {
                EvidenceNumber = EvidenceNumber,
                Name = Name
            };

            if (InventoryID > 0)
                accessory.Inventory_Accessories_Connect.Add(connect);

            var photo = new Inventory_Accessories_Photo
            {
                URL = URL
            };

            accessory.Photos.Add(photo);

            db.Inventory_Accessories.Add(accessory);

            try
            {
                db.SaveChanges();

                return Json(new { success = true, ID = accessory.ID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "AJAX_CreateAccessory", User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AJAX_CreateAccessoryConnect(int InventoryID, int AccessoryID)
        {
            var db = new ACSystemContext();

            var connect = new Inventory_Accessories_Connect
            {
                InventoryID = InventoryID,
                AccessoryID = AccessoryID,
                Active = true
            };

            db.Inventory_Accessories_Connect.Add(connect);

            try
            {
                db.SaveChanges();

                return Json(new { success = true, connectID = connect.ID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "AJAX_CreateAccessoryConnect", User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AJAX_AddInventoryFile(int InventoryID)
        {
            if (Request.Files.Count > 0)
            {
                var db = new ACSystemContext();

                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    Inventory_Photo inventoryPhoto = null;

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var fi = new FileInfo(fileName);
                        var ext = fi.Extension.ToUpper();
                        newFileName += fi.Extension;


                        var filePath = Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName);
                        file.SaveAs(filePath);

                        if (ext == ".JPG" || ext == ".JPEG" || ext == ".PNG" || ext == ".BMP")
                        {
                            newFileName = "resized_" + newFileName;

                            using (var image = Image.FromFile(filePath))
                            using (var newImage = Images.ScaleImage(image, 1200, 1600))
                            {
                                image.Dispose();
                                switch (ext)
                                {
                                    case ".JPG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".JPEG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".PNG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Png);
                                        break;
                                    case ".BMP":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Bmp);
                                        break;
                                }
                            }

                            System.IO.File.Delete(filePath);
                        }

                        if (InventoryID > 0)
                        {
                            inventoryPhoto = new Inventory_Photo
                            {
                                InventoryID = InventoryID,
                                Name = fi.Name,
                                URL = "/Monter/dist/Documents/" + newFileName
                            };

                            db.Inventory_Photo.Add(inventoryPhoto);

                            db.SaveChanges();
                        }
                    }

                    return Json(new { result = "true", FileName = fileName, URL = "/Monter/dist/Documents/" + newFileName, ID = (inventoryPhoto != null) ? inventoryPhoto.ID : -1 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = "false", message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AJAX_CreateWarehouse(string Street, string City, string Country)
        {
            var db = new ACSystemContext();

            var warehouse = new InventoryWarehouse
            {
                Street = Street,
                City = City,
                Country = Country
            };

            db.InventoryWarehouse.Add(warehouse);

            try
            {
                db.SaveChanges();

                return Json(new { success = true, ID = warehouse.ID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "AJAX_CreateWarehouse", User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AJAX_CreateType(string Name)
        {
            var db = new ACSystemContext();

            var type = new InventoryType
            {
                Name = Name
            };

            db.InventoryType.Add(type);

            try
            {
                db.SaveChanges();

                return Json(new { success = true, ID = type.ID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "AJAX_CreateType", User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AJAX_DeleteWarehouse(int ID)
        {
            var db = new ACSystemContext();

            var warehouse = db.InventoryWarehouse.Find(ID);

            db.InventoryWarehouse.Remove(warehouse);

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                Core.Service.Helpers.Log(ex, "AJAX_DeleteWarehouse", User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AJAX_UploadCheckConditionImage()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var fi = new FileInfo(fileName);
                        var ext = fi.Extension.ToUpper();
                        newFileName += fi.Extension;

                        var filePath = Path.Combine(Server.MapPath("~/Monter/dist/Documents/"), newFileName);
                        file.SaveAs(filePath);

                        if (ext == ".JPG" || ext == ".JPEG" || ext == ".PNG" || ext == ".BMP")
                        {
                            newFileName = "resized_" + newFileName;

                            using (var image = Image.FromFile(filePath))
                            using (var newImage = Images.ScaleImage(image, 600, 800))
                            {
                                image.Dispose();
                                switch (ext)
                                {
                                    case ".JPG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".JPEG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".PNG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Png);
                                        break;
                                    case ".BMP":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Bmp);
                                        break;
                                }
                            }

                            System.IO.File.Delete(filePath);
                        }
                    }

                    return Json(new { success = true, URL = "/Monter/dist/Documents/" + newFileName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

        #region TODO

        public ActionResult Translates(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var todo = db.Todo.Find(ID.Value);

            return View("Todo/Translates", todo);
        }

        public ActionResult Comments(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var todo = db.Todo.Find(ID.Value);

            return View("Todo/Comments", todo);
        }

        public ActionResult PlaceError(int? PlaceErrorID, int? TodoID)
        {
            if (!PlaceErrorID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();
            
            var placeError = db.PlaceError.Find(PlaceErrorID);

            if (placeError == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var task = db.Tasks.Find(placeError.TaskID);

            if (task != null)
                ViewBag.ProductName = task.GetTaskName(task.Apartment.ProjectId);

            ViewBag.TodoID = TodoID;

            return View("Todo/PlaceError", placeError);
        }

        #region POST

        [HttpPost]
        public ActionResult Translates(int ID, int[] QuestionPhotosID, string[] QuestionPhotosDesc, string[] QuestionPhotosTranslate)
        {
            var db = new ACSystemContext();

            var todo = db.Todo.Find(ID);
            todo.Checked = true;

            db.Entry(todo).State = EntityState.Modified;

            for (var i = 0; i < QuestionPhotosID.Length; i++)
            {
                var question = db.TaskQuestionPhoto.Find(QuestionPhotosID[i]);

                question.Description = QuestionPhotosDesc[i];
                question.TranslatedDescription = QuestionPhotosTranslate[i];

                db.Entry(question).State = EntityState.Modified;
            }

            try
            {
                db.SaveChanges();

                ViewBag.Success = true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                ViewBag.Success = false;
            }

            return View("Todo/Translates", todo);
        }

        [HttpPost]
        public ActionResult PlaceError(int ID, int? TodoID, string Description, string Translated, bool Send)
        {
            var db = new ACSystemContext();
            
            var todo = db.Todo.Find(TodoID);
            var error = db.PlaceError.Find(ID);

            error.Description = Description;
            error.Translated = Translated;

            if (todo == null)
                todo = db.Todo.FirstOrDefault(q => q.PlaceErrorID == ID && !q.Checked);

            if (todo != null)
            {
                todo.Checked = true;
                db.Entry(todo).State = EntityState.Modified;
            }

            db.Entry(error).State = EntityState.Modified;

            try
            {
                db.SaveChanges();

                error.Fitter = db.ACSystemUser.Find(error.FitterID);
                error.Project = db.Projects.Find(error.ProjectID);
                error.Apartment = db.Apartments.Find(error.ApartmentID);
                error.Task = db.Tasks.Find(error.TaskID);

                var _exception = "";
                var send = true;

                if (Send)
                {
                    var uri = new Uri(Request.Url.AbsoluteUri).GetServerUrl();
                    var pdfCreated = HttpRequests.RequestUrl_WithSuccess(uri + "/Reports/PlaceError_ClientReport?_placeErrorID=" + ID);

                    if (pdfCreated)
                    {

                        var emailTitle = "";
                        var emails = error.Project.Project_ProjectManager.Select(q => q.ProjectManager.Email).ToList();
                        var pdfPath = Path.Combine(Server.MapPath("~/dist/PlaceErrors"), $"{ID}.pdf");

                        if (error.Task != null)
                            emailTitle = $"OBS! Uppgift {error.Task.Name} Eventuellt Problem!";
                        else if (error.Apartment != null)
                            emailTitle = $"OBS! LGH {error.Apartment.Letter}{error.Apartment.NumberOfApartment} Eventuellt Problem!";
                        else
                            emailTitle = $"OBS! Projekt {error.Project.ProjectNumber} Eventuellt Problem!";

                        send = MailService.PlaceError_MailReport(emailTitle, emails, pdfPath, out _exception);

                        if (send)
                        {
                            error.SendDate = DateTime.Now;
                            db.Entry(error).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        send = false;
                        _exception = "Błąd generowania dokumentu PDF";
                    }
                }

                ViewBag.TodoID = TodoID;
                ViewBag.Success = send;
                ViewBag.Exception = _exception;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                ViewBag.Success = false;
                ViewBag.Exception = ex.Message;
            }

            return View("Todo/PlaceError", error);            
        }

        #endregion

        #endregion
    }
}