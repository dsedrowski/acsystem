﻿using ACSystem.Core;
using ACSystem.Core.Filters;
using ACSystem.Core.IRepo;
using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels.Select;
using ACSystem.Core.Repo;
using ACSystem.Core.Service;
using ACSystem.Reports;
using ACSystem.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.Reporting.WinForms;
using PdfSharp;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using TheArtOfDev.HtmlRenderer.Core;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace ACSystem.Controllers
{
    [AuthLog(Roles = "Admin")]
    public class OfficeController : Controller
    {
        private readonly IOfficeRepo _repo;

        public OfficeController(IOfficeRepo repo)
        {
            _repo = repo;
        }


        #region PAYROLL
        public ActionResult Payroll()
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista płac", Url = "/Office/Payroll", Icon = "fa fa-money", Active = true });

            ViewBag.LastNumber = _repo.LastPayrollNumber();
            ViewBag.Fitters = _repo.GetFittersList();

            return View();
        }

        public ActionResult PayrollList(int? page, string orderBy = "date", string dest = "desc", string search = "", string filter = "0,1,2")
        {
            var pageSize = 20;

            var rowsCount = _repo.Count();

            var pagesCount = rowsCount / pageSize;

            if (rowsCount <= pageSize || page <= 0 || page == null) page = 1;

            var excludeRows = (page - 1) * pageSize;

            var rows = 0;

            var list = _repo.List(out rows, excludeRows, pageSize, orderBy, dest, search, filter);

            pagesCount = (rows % pageSize == 0) ? rows / pageSize : (rows / pageSize) + 1;

            if (pagesCount <= 0) pagesCount = 1;

            return Json(new { data = list, pages = pagesCount }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> PayrollDetails(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Listy płacowe", Url = "/Office", Icon = "fa fa-money", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Szczegóły listy płacowej", Url = "/Office/PayrollDetails", Icon = "", Active = true });

            Payroll payroll = await _repo.GetPayroll(id.Value);
            if (payroll == null)
                return HttpNotFound();

            var fitters = _repo.GetContext().Fitters.ToList();

            ViewBag.Fitters = fitters;

            var db = new ACSystemContext();

            var all = db.Payroll.Where(q => q.Active).OrderBy(q => q.Number).ToList();
            var currentIndex = all.FindIndex(q => q.Id == id.Value);

            int? prev = null, next = null;

            if (currentIndex > 0)
                prev = all[currentIndex - 1].Id;

            if (currentIndex < all.Count - 1)
                next = all[currentIndex + 1].Id;

            ViewBag.Prev = prev;
            ViewBag.Next = next;

            return View(payroll);
        }

        [HttpGet]
        public async Task<ActionResult> PayrollSave(int? id, string toDate)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (string.IsNullOrEmpty(toDate))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Nie przekazano daty końcowej listy płac.");

            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Listy płacowe", Url = "/Office", Icon = "fa fa-money", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Zapisywanie listy płac", Url = "/Office/PayrollSave", Icon = "", Active = true });

            Payroll payroll = await _repo.GetPayroll(id.Value);
            if (payroll == null)
                return HttpNotFound();

            var fittersHourlyRate = new Dictionary<string, int>();
            var hoursNotReported = new Dictionary<string, string>();

            _repo.GetContext().Fitters.ToList().ForEach(x =>
            {
                fittersHourlyRate.Add(x.Id, x.HourlyRate);

                var fiveDaysPast = DateTime.Now.AddDays(-5);
                var fitterHours = _repo.GetContext().Fitter_Hours.Where(q => q.FitterID == x.Id && q.Date <= fiveDaysPast && q.Fitter_Hours_Project.Any() == false && !q.Fitter.LockoutEnabled);

                if (fitterHours.Any() && hoursNotReported.Any(q => q.Value.Equals(fitterHours.FirstOrDefault().Fitter.FullName)) == false)
                {
                    hoursNotReported.Add(fitterHours.FirstOrDefault().FitterID, fitterHours.FirstOrDefault().Fitter.FullName);
                }
            });

            ViewBag.FitterHourlyRate = fittersHourlyRate;
            ViewBag.HoursNotReported = hoursNotReported;
            ViewBag.ToDate = toDate;

            return View(payroll);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PayrollSave(int ID, string HoursNotReported, string ToDate)
        {
            var context = _repo.GetContext();

            var rq = Request.Form;

            var payroll = context.Payroll.Find(ID);

            var toDelete = new List<PayrollItems>();

            var allFees = new List<Fees>();

            var projects = new List<Projects>();
            var supervisorAdds = new List<SupervisorAdds>();
            var fitters = context.Fitters.ToList();

            foreach (var e in payroll.Items)
            {
                var isChecked = rq["element-" + e.Id];

                if (isChecked != null)
                {
                    var elm = e;
                    elm.ToPayPLN = (decimal)((double)elm.ToPay * payroll.CurrencyRate2);
                    elm.FeesPLN = (decimal)((double)elm.Fees * payroll.CurrencyRate2);
                    elm.HourlyRate = fitters.FirstOrDefault(q => q.Id == elm.FitterID)?.HourlyRate ?? 0;
                    elm.Active = true;

                    var allActivities = context.Task_Fitter.Where(q => q.TaskId == e.TaskID && q.FitterId == e.FitterID && q.IsActive).ToList();
                    //var fees = context.Fees.Where(q => q.TaskID == e.TaskID && q.FitterID == e.FitterID && q.Status == FeeStatus.DoPotracenia).ToList();
                    var fees = context.Fees.Where(q => q.ID == e.FeeID && q.Status == FeeStatus.DoPotracenia).ToList();

                    if (allActivities.Any() != false)
                    {
                        var project = allActivities.FirstOrDefault().Task.Apartment.Project;
                        var task = allActivities.FirstOrDefault().Task;
                        var fitter = allActivities.FirstOrDefault().Fitter;

                        _repo.SupervisorAddsService(supervisorAdds, projects, e, fitter, task);

                        if (project != null && project.IsChecked)
                        {
                            project.IsChecked = false;

                            context.Entry(project).State = EntityState.Modified;

                            var user = _repo.GetContext().ACSystemUser.Find(User.Identity.GetUserId());

                            var activity = new Project_Activity
                            {
                                ProjectID = project.Id,
                                UserID = User.Identity.GetUserId(),
                                Message = $"Projekt zmieniony na NIE SPRAWDZONY, użytkownik {user.FullName} dodał do projektu listę płac.",
                                Timestamp = DateTime.Now
                            };

                            _repo.GetContext().Project_Activity.Add(activity);
                        }

                        foreach (var aa in allActivities)
                        {
                            aa.InPayroll = true;
                            context.Entry(aa).State = System.Data.Entity.EntityState.Modified;
                        }
                    }

                    foreach (var fee in fees)
                    {
                        fee.Status = FeeStatus.Potracone;
                        fee.PayrollID = payroll.Id;
                        fee.TakeDate = DateTime.Now;

                        context.Entry(fee).State = System.Data.Entity.EntityState.Modified;
                    }

                    var rentAndLoan = context.RentAndLoan.Find(e.RentAndLoanID);

                    if (rentAndLoan != null)
                    {
                        if (rentAndLoan.Currency == Currency.SEK)
                            rentAndLoan.LeftAmount = rentAndLoan.LeftAmount - e.Fees;
                        else
                            rentAndLoan.LeftAmount = rentAndLoan.LeftAmount - e.FeesPLN;

                        context.Entry(rentAndLoan).State = EntityState.Modified;

                        var installment = new Installment
                        {
                            RentAndLoanID = rentAndLoan.ID,
                            PayrollID = payroll.Id,
                            ChargeAmount = (rentAndLoan.Currency == Currency.SEK) ? e.Fees : e.FeesPLN,
                            Timestamp = DateTime.Now
                        };

                        context.Installment.Add(installment);
                    }

                    e.ToPayPLN = e.ToPay * payroll.CurrencyRate;
                    context.Entry(e).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    toDelete.Add(e);
                    //payroll.Items.Remove(e);
                }
            }

            var feeNumber = context.Fees.Max(q => q.FeeNumber);

            foreach (var s in supervisorAdds)
            {
                if (s.ProjectID > 0 == false)
                    continue;

                feeNumber++;

                var newFee = _repo.GetContext().Fees.Add(new Fees
                {
                    FeeNumber = feeNumber,
                    FitterID = s.SupervisorID,
                    ProjectID = s.ProjectID,
                    FeeDate = DateTime.Now,
                    TakeDate = DateTime.Now,
                    Description = "Dodatek za kierownika",
                    FeeAmount = s.Amount,
                    Status = FeeStatus.Potracone,
                    PayrollID = payroll.Id
                });

                payroll.Items.Add(new PayrollItems
                {
                    FitterID = s.SupervisorID,
                    FitterName = s.Supervisor.FullName,
                    ProjectID = s.ProjectID,
                    ProjectNumber = projects.FirstOrDefault(q => q.Id == s.ProjectID).ProjectNumber,
                    TaskEnd = DateTime.Now,
                    WorkDone = 0,
                    WorkDoneUnit = "szt.",
                    WorkTime = 0,
                    IsHourlyPay = false,
                    ToPay = 0,
                    ToPayPLN = 0,
                    Fees = s.Amount * -1,
                    FeesPLN = (decimal)((double)s.Amount * payroll.CurrencyRate2) * -1,
                    Status = PayItemStatus.DoWyplaty,
                    Active = true,
                    FeeDescription = "Dodatek za kierownika",
                    FeeID = newFee.ID
                });
            }

            toDelete.ForEach(x => payroll.Items.Remove(x));
            context.PayrollItems.RemoveRange(toDelete);

            payroll.Active = true;
            payroll.Sum = (payroll.Items.Sum(q => q.ToPay)) - (payroll.Items.Sum(q => q.Fees));
            payroll.SumPLN = (payroll.Items.Sum(q => (decimal)((double)q.ToPay * payroll.CurrencyRate2))) - (payroll.Items.Sum(q => (decimal)((double)q.Fees * payroll.CurrencyRate2)));
            context.Entry(payroll).State = EntityState.Modified;

            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Helpers.Log(ex, "SavePayroll", User.Identity.Name);

                var fittersHourlyRate = new Dictionary<string, int>();

                _repo.GetContext().Fitters.ToList().ForEach(x =>
                {
                    fittersHourlyRate.Add(x.Id, x.HourlyRate);
                });

                ViewBag.FitterHourlyRate = fittersHourlyRate;

                return View(payroll);
            }

            var toDate = DateTime.Parse(ToDate);
            payroll.Items.Where(e => e.WorkTime > 0).GroupBy(e => e.FitterID).ToList().ForEach(x =>
            {
                context.Fitter_Hours.Where(e => e.FitterID == x.Key && !e.PayrollID.HasValue && !e.FreeDay && e.Date <= toDate).ToList().ForEach(y =>
                {
                    y.PayrollID = payroll.Id;
                    context.Entry(y).State = EntityState.Modified;
                });
            });

            List<IGrouping<string, PayrollItems>> payrollItemsGroupedByUser = payroll.Items.GroupBy(q => q.FitterID).ToList();
            payrollItemsGroupedByUser.ForEach(x =>
            {
                var hourlyRate = x.FirstOrDefault()?.HourlyRate ?? 0;
                var hours = x.Sum(q => q.WorkTime);
                var hourCost = hours * hourlyRate;
                var hourCostPLN = (double)hourCost * payroll.CurrencyRate2;
                decimal sum = x.Sum(q => q.ToPay) - x.Sum(q => q.Fees) - hourCost;
                if (sum < 0)
                {
                    RentAndLoan rentAndLoan = new RentAndLoan
                    {
                        FitterID = x.Key,
                        Frequency = InstallmentFrequency.PerPayroll,
                        Description = "Minusowa lista płac nr. " + payroll.Number.ToString().PadLeft(5, '0'),
                        FullAmount = sum * -1,
                        InstallmentAmount = sum * -1,
                        LeftAmount = sum * -1,
                        Currency = Currency.SEK,
                        StartDate = DateTime.Now,
                        InterestRate = 0
                    };

                    context.RentAndLoan.Add(rentAndLoan);

                    x.ToList().ForEach(y =>
                    {
                        y.Status = PayItemStatus.Wyplacone;
                        context.Entry(y).State = EntityState.Modified;
                    });
                }
            });

            try
            {
                context.SaveChanges();

                #region MAILE DLA PRACOWNIKÓW

                var fittersGroup = payroll.Items.GroupBy(q => q.FitterID);

                foreach (var f in fittersGroup)
                {
                    var fit = context.Fitters.Find(f.Key);

                    if (fit == null)
                        continue;

                    CreatePDFPayrollFromHTML(payroll, f.Key, fit.User.FullName, fit.User.Email);
                    Thread.Sleep(3000);
                }

                #endregion

                return Redirect("/Office/PayrollDetails/" + ID);
            }
            catch (Exception ex)
            {
                Helpers.Log(ex, "SavePayroll", User.Identity.Name);

                var fittersHourlyRate = new Dictionary<string, int>();

                _repo.GetContext().Fitters.ToList().ForEach(x =>
                {
                    fittersHourlyRate.Add(x.Id, x.HourlyRate);
                });

                ViewBag.FitterHourlyRate = fittersHourlyRate;

                return View(payroll);
            }

            return null;
        }

        public ActionResult PayrollCancel(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var context = _repo.GetContext();
            var payroll = context.Payroll.Find(id.Value);

            if (payroll == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            context.Payroll.Remove(payroll);

            context.SaveChanges();

            return Redirect("/Office/Payroll");
        }

        public ActionResult PayrollElementStatus(int? id, bool isChecked)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var context = _repo.GetContext();

            var element = context.PayrollItems.Find(id.Value);

            element.Active = isChecked;

            context.Entry(element).State = System.Data.Entity.EntityState.Modified;

            try
            {
                context.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "CreateProject",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                context.ExceptionsLog.Add(log);

                context.SaveChanges();

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PayrollCreate(CreatePayrollViewModel model)
        {
            int? payrollID = null;
            var hoursNotReported = new Dictionary<string, string>();

            if (model.ForFitter)
                payrollID = _repo.CreatePayroll(out hoursNotReported, model.Number, model.CreateDate, model.FitterID);
            else
                payrollID = _repo.CreatePayroll(out hoursNotReported, model.Number, model.CreateDate);

            if (payrollID == null)
                return Redirect("/Office/Payroll");

            ViewBag.HoursNotReported = hoursNotReported;

            return Redirect($"/Office/PayrollSave?id={payrollID}&toDate={model.CreateDate:yyyy-MM-dd}");
        }

        [HttpGet]
        public ActionResult PayrollCreate(int ProjectID)
        {
            var lastNumber = _repo.LastPayrollNumber();
            var hoursNotReported = new Dictionary<string, string>();

            var payrollID = _repo.CreatePayroll(out hoursNotReported, lastNumber + 1, DateTime.Now, "", ProjectID);

            if (payrollID == null)
                return Redirect("/Office/Payroll");

            ViewBag.HoursNotReported = hoursNotReported;

            return Redirect($"/Office/PayrollSave/{payrollID}");
        }

        public async Task<ActionResult> PayrollDelete(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = "";

            for (int i = 0; i < 3; i++)
            {
                result = await _repo.DeletePayroll((int)id);

                if (result.Split(';')[0] == "true")
                    break;
            }

            var rsSplit = result.Split(';');

            if (rsSplit.Length > 1)
                return Json(new { success = rsSplit[0], message = rsSplit[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Nieoczekiwany błąd." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangePayrollStatus(int ID, string FitterID, PayItemStatus Status)
        {
            var context = _repo.GetContext();

            var payrollItems = context.PayrollItems.Where(q => q.PayrollID == ID && q.FitterID == FitterID).ToList();

            foreach (var item in payrollItems)
            {
                item.Status = Status;

                if (Status == PayItemStatus.Wyplacone)
                    item.InvoiceTimestamp = DateTime.Now;
                else
                    item.InvoiceTimestamp = null;

                context.Entry(item).State = System.Data.Entity.EntityState.Modified;
            }

            try
            {
                context.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "mail",
                    LogDate = DateTime.Now,
                    User = "admin"
                };

                context.ExceptionsLog.Add(log);

                context.SaveChanges();

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "mail",
                    LogDate = DateTime.Now,
                    User = "admin"
                };

                context.ExceptionsLog.Add(log);

                context.SaveChanges();

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ChangeFitterPayrollInfo(int payrollID, string fitter, string InvoiceNumber, DateTime? InvoiceDate, string InvoiceOwner)
        {
            using (var db = new ACSystemContext())
            {
                var items = db.PayrollItems.Where(q => q.PayrollID == payrollID && q.FitterName == fitter);

                foreach (var item in items)
                {
                    item.InvoiceNumber = InvoiceNumber;
                    item.InvoiceDate = InvoiceDate;
                    item.InvoiceOwner = InvoiceOwner;

                    db.Entry(item).State = EntityState.Modified;
                }

                try
                {
                    db.SaveChanges();

                    return Redirect("/Office/PayrollDetails/" + payrollID);
                }
                catch (Exception ex)
                {
                    var log = new ExceptionsLog
                    {
                        Message = ex.Message,
                        Type = ex.GetType().ToString(),
                        Source = ex.Source,
                        StackTrace = ex.StackTrace,
                        URL = "ChangeFitterPayrollInfo",
                        LogDate = DateTime.Now,
                        User = User.Identity.Name
                    };

                    db.ExceptionsLog.Add(log);

                    db.SaveChanges();

                    return Redirect("/Office/PayrollDetails/" + payrollID);
                }
            }
        }

        #region PRINT
        [AllowAnonymous]
        public async Task<ActionResult> PayrollForFitter(int? PayrollID, string FitterName, string FitterID)
        {
            if (!PayrollID.HasValue || string.IsNullOrEmpty(FitterName) || string.IsNullOrEmpty(FitterID))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.FitterName = FitterName;

            Payroll payroll = await _repo.GetPayroll(PayrollID.Value);

            var context = _repo.GetContext();

            var fitter = context.Fitters.Find(FitterID);

            var fitterHourlyRate = fitter.HourlyRate;

            ViewBag.FitterHourlyRate = fitterHourlyRate;

            if (payroll == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return PartialView("Print/PayrollForFitter", payroll);
        }
        #endregion

        public ActionResult SendPayrollToFitter(int ID, string FitterID)
        {
            try
            {
                using (var context = new ACSystemContext())
                {
                    var fit = context.Fitters.Find(FitterID);

                    if (fit == null)
                        return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                    var payroll = context.Payroll.Find(ID);

                    if (payroll == null)
                        return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                    CreatePDFPayrollFromHTML(payroll, FitterID, fit.User.FullName, fit.User.Email);
                }

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }


        #region MODALS
        public ActionResult Modal_AddPayroll()
        {
            ViewBag.LastNumber = _repo.LastPayrollNumber();
            ViewBag.Fitters = _repo.GetFittersList();

            return PartialView();
        }
        #endregion

        #endregion

        #region INVOICE
        public ActionResult Invoice()
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista płac", Url = "/Office/Invoice", Icon = "fa fa-money", Active = true });


            return View();
        }

        public ActionResult InvoicesList(int? page, string orderBy = "date", string dest = "desc", string search = "", string filter = "0,1,2,3")
        {
            var pageSize = 20;

            var rowsCount = _repo.InvoicesCount();

            var pagesCount = rowsCount / pageSize;

            if (rowsCount <= pageSize || page <= 0 || page == null) page = 1;

            var excludeRows = (page - 1) * pageSize;

            var rows = 0;

            var list = _repo.InvoicesList(out rows, excludeRows, pageSize, orderBy, dest, search, filter);

            pagesCount = (rows % pageSize == 0) ? rows / pageSize : (rows / pageSize) + 1;

            if (pagesCount <= 0) pagesCount = 1;

            return Json(new { data = list, pages = pagesCount }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InvoiceCreate(int? projectID)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista płac", Url = "/Office/Invoice", Icon = "fa fa-money", Active = true });

            var lastNumber = _repo.LastInvoiceNumber();

            var customersList = _repo.GetCustomersList();
            var projectsList = _repo.GetProjectsList(customersList.FirstOrDefault().ID);
            var bankAccountsList = _repo.GetBankAccountsList();
            var firstCustomerPaymentTerm = _repo.CustomerPaymentTerm(customersList.FirstOrDefault().ID);
            //var LastInvoice = _repo.LastInvoice(customersList.FirstOrDefault().ID, projectsList.FirstOrDefault().ID);

            ViewBag.CustomersList = customersList;
            ViewBag.ProjectsList = projectsList;
            ViewBag.BankAccountsList = bankAccountsList;
            ViewBag.FirstCustomerPaymentTerm = firstCustomerPaymentTerm;
            //ViewBag.LastInvoice = LastInvoice;

            var model = new Invoice
            {
                InvoiceNumber = lastNumber + 1,
                InvoiceDate = DateTime.Now,
                DeliveryDate = DateTime.Now,
                PaymentTerm = DateTime.Now.AddDays(firstCustomerPaymentTerm),
                ProjectID = projectID ?? -1
            };

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> InvoiceElementsGenerate(Invoice invoice)
        {
            invoice.CreateDate = DateTime.Now;

            //var invoiceID = await _repo.CreateInvoice(invoice);

            //if (invoiceID == null)
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var elementsResult = await _repo.GenerateInvoiceElements(invoice.InvoiceDateTo, invoice);

            //if (elementsResult == false)
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var invoiceElements = _repo.GetInvoiceElements(elementsResult);

            ViewBag.InvoiceIDs = elementsResult;

            var notInvoicedTasks = _repo.GetNotInvoicedTasks(invoice.ProjectID);

            ViewBag.NotInvoicedTasks = notInvoicedTasks;

            var apartments = _repo.GetApartmentsList(invoice.ProjectID);

            ViewBag.Apartments = apartments;

            return PartialView($"Partial_InvoiceElementsGenerateNew", invoiceElements);
        }

        public ActionResult Partial_InvoiceElementsGenerate(int id)
        {
            var invoiceElements = _repo.GetInvoiceElements(id);

            return View(invoiceElements);
        }

        [HttpPost]
        public ActionResult InvoiceSave(int[] InvoiceIDs)
        {
            var db = _repo.GetContext();

            var lastInvoice = db.Invoice.Where(q => q.Complete == true).OrderByDescending(q => q.InvoiceNumber).FirstOrDefault();
            var lastId = lastInvoice?.InvoiceNumber + 1 ?? 1;

            foreach (var InvoiceID in InvoiceIDs)
            {
                var invoice = db.Invoice.Find(InvoiceID);

                var anyInInvoice = false;
                decimal invoiceAmount = 0;

                if (invoice == null)
                    return Redirect("/Office/InvoiceCreate");

                var rq = Request.Form;
                var toDelete = new List<InvoiceElements>();

                foreach (var e in invoice.Elements)
                {
                    var isChecked = rq["element-" + e.ID];

                    if (isChecked != null)
                    {
                        anyInInvoice = true;
                        invoiceAmount += (e.Invoice_AddsID.HasValue) ? e.Total ?? 0 : e.Sum;
                        e.Complete = true;
                        db.Entry(e).State = EntityState.Modified;

                        if (e.Invoice_AddsID.HasValue)
                        {
                            var add = db.Invoice_Adds.Find(e.Invoice_AddsID);
                            add.InvoiceID = e.InvoiceID;
                            db.Entry(add).State = EntityState.Modified;
                        }
                        else
                        {
                            var task = db.Tasks.Find(e.TaskID);
                            task.Invoice = true;
                            db.Entry(task).State = EntityState.Modified;
                        }

                        var project = db.Projects.Find(e.ProjectID);
                        project.IsChecked = false;
                        db.Entry(project).State = EntityState.Modified;

                        //var user = db.ACSystemUser.Find(User.Identity.GetUserId());

                        //var activity = new Project_Activity
                        //{
                        //    ProjectID = project.Id,
                        //    UserID = User.Identity.GetUserId(),
                        //    Message = $"Projekt zmieniony na NIE SPRAWDZONY, użytkownik {user.FullName} wygenerował fakturę dla projektu.",
                        //    Timestamp = DateTime.Now
                        //};

                        //db.Project_Activity.Add(activity);
                    }
                    else
                        toDelete.Add(e);
                }

                db.InvoiceElements.RemoveRange(toDelete);

                if (anyInInvoice)
                {
                    invoice.Complete = true;
                    invoice.InvoiceNumber = lastId;
                    invoice.Amount = invoiceAmount;
                    invoice.CreateDate = DateTime.Now;
                    db.Entry(invoice).State = EntityState.Modified;
                }
                else
                {
                    db.Invoice.Remove(invoice);
                }

                lastId++;
            }

            try
            {
                db.SaveChanges();

                return Redirect("/Office/Invoice");
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "SaveInvoice",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                return Redirect("/Office/InvoiceCreate");
            }

            //if (string.IsNullOrEmpty(id))
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            //if (await _repo.SaveInvoice(id))
            //{
            //    //var result = CreatePDF(id.Value, Server.MapPath($"~/dist/Invoice/{id.Value}.pdf"));
            //    ////var result = String.Empty;
            //    //if (string.IsNullOrEmpty(result))
            //    //    return Json(new { success = "true" }, JsonRequestBehavior.AllowGet);
            //    //else
            //    //{
            //    //    await _repo.DeleteInvoice(id.Value);

            //    //    return Json(new { success = "false", message = result }, JsonRequestBehavior.AllowGet);
            //    //}

            //    return Json(new { success = "true" }, JsonRequestBehavior.AllowGet);
            //}
            //else
            //    return Json(new { success = "false" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> InvoiceDelete(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = "";

            for (int i = 0; i < 3; i++)
            {
                result = await _repo.DeleteInvoice((int)id);

                if (result.Split(';')[0] == "true")
                    break;
            }

            var rsSplit = result.Split(';');

            if (rsSplit.Length > 1)
                return Json(new { success = rsSplit[0], message = rsSplit[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Nieoczekiwany błąd." }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> CancelInvoice(string id)
        {
            if (string.IsNullOrEmpty(id))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var idSplit = id.Split(';');

            var allResult = "";

            foreach (var i in idSplit)
            {
                var idInt = int.Parse(i);

                var result = "";

                for (int j = 0; j < 3; j++)
                {
                    result = await _repo.DeleteInvoice(idInt);

                    if (result.Split(';')[0] == "true")
                        break;
                }

                var rsSplit = result.Split(';');

                if (rsSplit[0] == "false")
                    return Json(new { success = rsSplit[0], message = rsSplit[1] }, JsonRequestBehavior.AllowGet);
                else
                    allResult = result;
            }

            var rsaSplit = allResult.Split(';');

            if (rsaSplit.Length > 1)
                return Json(new { success = rsaSplit[0], message = rsaSplit[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Nieoczekiwany błąd." }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> InvoiceElementDelete(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = "";

            for (int i = 0; i < 3; i++)
            {
                result = await _repo.DeleteInvoiceElement(id.Value);

                if (result.Split(';')[0] == "true")
                    break;
            }

            var rsSplit = result.Split(';');

            if (rsSplit.Length > 1)
                return Json(new { success = rsSplit[0], message = rsSplit[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Nieoczekiwany błąd." }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> InvoiceElementDeleteByApartment(string apartmentNumber, int? invoiceID)
        {
            if (string.IsNullOrEmpty(apartmentNumber))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!invoiceID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = "";

            for (int i = 0; i < 3; i++)
            {
                result = await _repo.DeleteInvoiceElementByApartment(apartmentNumber, invoiceID.Value);

                if (result.Split(';')[0] == "true")
                    break;
            }

            var rsSplit = result.Split(';');

            if (rsSplit.Length > 1)
                return Json(new { success = rsSplit[0], message = rsSplit[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Nieoczekiwany błąd." }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> InvoiceElementDeleteByProject(int? projectID, int? invoiceID)
        {
            if (!projectID.HasValue || !invoiceID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = await _repo.DeleteInvoice(invoiceID.Value);

            var rsSplit = result.Split(';');

            if (rsSplit.Length > 1)
                return Json(new { success = rsSplit[0], message = rsSplit[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Nieoczekiwany błąd." }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> DetailsInvoice(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var model = await _repo.GetInvoice(id.Value);

            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var all = db.Invoice.Where(q => q.Complete).OrderBy(q => q.InvoiceNumber).ToList();
            var currentIndex = all.FindIndex(q => q.ID == id.Value);

            int? prev = null, next = null;

            if (currentIndex > 0)
                prev = all[currentIndex - 1].ID;

            if (currentIndex < all.Count - 1)
                next = all[currentIndex + 1].ID;

            ViewBag.Prev = prev;
            ViewBag.Next = next;

            return View(model);
        }

        public async Task<ActionResult> SendInvoice(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var invoice = await _repo.GetInvoice(id.Value);

            if (invoice == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var pdfPath = Server.MapPath($"~/dist/Invoice/{id.Value}.pdf");

            if (!System.IO.File.Exists(pdfPath))
            {
                CreatePDF(id.Value, pdfPath);
            }

            var _exception = "";

            if (MailService.Invoice(invoice, pdfPath, out _exception))
            {
                var context = _repo.GetContext();
                invoice.Status = Core.InvoiceStatus.Wyslana;
                context.Entry(invoice).State = System.Data.Entity.EntityState.Modified;

                try
                {
                    context.SaveChanges();
                }
                catch (DbUpdateException ex)
                {
                    var log = new ExceptionsLog
                    {
                        Message = ex.Message,
                        Type = ex.GetType().ToString(),
                        Source = ex.Source,
                        StackTrace = ex.StackTrace,
                        URL = "mail",
                        LogDate = DateTime.Now,
                        User = "admin"
                    };

                    context.ExceptionsLog.Add(log);

                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    var log = new ExceptionsLog
                    {
                        Message = ex.Message,
                        Type = ex.GetType().ToString(),
                        Source = ex.Source,
                        StackTrace = ex.StackTrace,
                        URL = "mail",
                        LogDate = DateTime.Now,
                        User = "admin"
                    };

                    context.ExceptionsLog.Add(log);

                    context.SaveChanges();
                }

                return Json(new { success = "true" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = "false", exception = _exception }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddBankAccount(string Currency, string Account)
        {
            var accounts = _repo.AddBankAccount(Currency, Account);

            return Json(new { accounts }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddTaskToInvoice(string apartmentNumber, int? invoiceID, int? taskID)
        {
            if (string.IsNullOrEmpty(apartmentNumber) || !invoiceID.HasValue || !taskID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var newElement = _repo.CreateSingleInvoiceElement(invoiceID.Value, taskID.Value);

            if (newElement != null)
                return Json(new { success = true, element = newElement }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, element = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetTasksForApartment(string apartmentNumber, int? invoiceID)
        {
            if (string.IsNullOrEmpty(apartmentNumber) || !invoiceID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var tasks = _repo.GetApartmentTasksList(apartmentNumber, invoiceID.Value);

            var tasksOptions = String.Empty;

            if (tasks.Count() > 0)
            {
                foreach (var task in tasks)
                {
                    tasksOptions += $"<option value='{task.ID}'>{task.NameWithSufix}</option>";
                }
            }

            return Json(new { options = tasksOptions }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangeInvoiceStatus(int ID, InvoiceStatus Status)
        {
            var context = _repo.GetContext();
            var invoice = context.Invoice.Find(ID);

            if (invoice == null)
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);

            invoice.Status = Status;

            context.Entry(invoice).State = System.Data.Entity.EntityState.Modified;

            try
            {
                context.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "mail",
                    LogDate = DateTime.Now,
                    User = "admin"
                };

                context.ExceptionsLog.Add(log);

                context.SaveChanges();

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "mail",
                    LogDate = DateTime.Now,
                    User = "admin"
                };

                context.ExceptionsLog.Add(log);

                context.SaveChanges();

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public void GenerateInvoiceXml(int? ID)
        {
            if (!ID.HasValue)
                return;

            var db = _repo.GetContext();

            var invoice = db.Invoice.Find(ID.Value);

            if (invoice == null)
                return;

            invoice.Status = InvoiceStatus.Xml;
            db.Entry(invoice).State = EntityState.Modified;
            db.SaveChanges();

            using (var ms = new MemoryStream())
            {
                var xml = new XmlTextWriter(ms, Encoding.UTF8)
                {
                    Formatting = Formatting.Indented,
                    Indentation = 4
                };

                xml.WriteStartDocument();
                xml.WriteStartElement("xmlimexport");

                xml.WriteStartElement("customers");

                xml.WriteStartElement("atKunder");
                xml.WriteAttributeString("key", invoice.Customer.VATNumber);
                xml.WriteAttributeString("option", "edit");

                xml.WriteElementString("CUSTOMER_NUMBER", invoice.Customer.VATNumber);
                xml.WriteElementString("CUSTOMER_NAME", invoice.Customer.Name);
                xml.WriteElementString("CUSTOMER_MAILING_ADDRESS", invoice.Customer.Street);
                xml.WriteElementString("CUSTOMER_MAILING_ADDRESS2", "");
                xml.WriteElementString("CUSTOMER_ZIPCODE", invoice.Customer.PostalCode);
                xml.WriteElementString("CUSTOMER_CITY", invoice.Customer.City);


                xml.WriteEndElement(); // atKunder

                xml.WriteEndElement(); // customers

                xml.WriteStartElement("articles");

                var currArticles = new List<int>();
                foreach (var a in invoice.Elements)
                {
                    if (currArticles.Contains(a.Task.ProductId)) continue;

                    currArticles.Add(a.Task.ProductId);

                    xml.WriteStartElement("atArtiklar");
                    xml.WriteAttributeString("key", a.Task.ProductId.ToString());

                    xml.WriteElementString("ARTICLE_NUMBER", a.Task.ProductId.ToString());
                    xml.WriteElementString("ARTICLE_NAME", a.ProductName);

                    xml.WriteEndElement(); //atArtiklar
                }

                xml.WriteEndElement(); // articles

                xml.WriteStartElement("projects");
                xml.WriteStartElement("atProjekt");

                xml.WriteAttributeString("key", invoice.Project.InSystemID);

                xml.WriteElementString("PROJECT_CODE_OF_PROJECT", invoice.Project.InSystemID);
                xml.WriteElementString("PROJECT_NAME", invoice.Project.ProjectNumber);
                xml.WriteElementString("PROJECT_DATE_OF_BEGINNING", invoice.Project.StartDate.ToString("yyyy-MM-dd"));
                xml.WriteElementString("PROJECT_DATE_OF_END", invoice.Project.EndDate.ToString("yyyy-MM-dd"));

                xml.WriteEndElement(); // atProjekt;
                xml.WriteEndElement(); // projects;

                xml.WriteStartElement("orders");

                xml.WriteStartElement("order");

                xml.WriteStartElement("atOrder");
                xml.WriteAttributeString("customerkey", invoice.Customer.VATNumber);
                xml.WriteAttributeString("option", "edit");

                xml.WriteElementString("OOI_HEAD_CUSTOMER_ORDER_NUMBER", invoice.Project.ProjectNumber);
                xml.WriteElementString("OOI_HEAD_CONTRACTNR", invoice.Project.InSystemID);
                xml.WriteElementString("OOI_HEAD_CARGO_AMOUNT", "0");
                xml.WriteElementString("OOI_HEAD_DOCUMENT_DATE1", invoice.InvoiceDate.ToString("yyyy-MM-dd"));
                xml.WriteElementString("OOI_HEAD_DOCUMENT_DATE2", invoice.PaymentTerm.ToString("yyyy-MM-dd"));

                xml.WriteEndElement(); // atOrder

                xml.WriteStartElement("rows");

                if (invoice.Elements.FirstOrDefault().Task.Apartment.Project.IsGroupedInvoice)
                {
                    var groupedElements = new List<InvoiceElements>();

                    foreach (var elem in invoice.Elements)
                    {
                        if (groupedElements.Any(q => q.ProductName == elem.ProductName && q.UnitPrice == elem.UnitPrice))
                        {
                            var current = groupedElements.FirstOrDefault(q => q.ProductName == elem.ProductName && q.UnitPrice == elem.UnitPrice);

                            current.Quantity += elem.Quantity;
                        }
                        else
                        {
                            groupedElements.Add(elem);
                        }
                    }

                    var firstApartment = invoice.Elements.OrderBy(q => q.ApartmentNumber).FirstOrDefault();
                    var lastApartment = invoice.Elements.OrderByDescending(q => q.ApartmentNumber).FirstOrDefault();
                    var firstFinishDate = invoice.Elements.OrderBy(q => q.Task.ClosedDate).FirstOrDefault();
                    var lastFinishDate = invoice.Elements.OrderByDescending(q => q.Task.ClosedDate).FirstOrDefault();

                    xml.WriteStartElement("row");
                    xml.WriteElementString("OOI_ROW_TYPE_OF_ROW", "T");
                    xml.WriteElementString("OOI_ROW_TEXT", $"LGH {firstApartment.ApartmentNumber} - {lastApartment.ApartmentNumber}");
                    xml.WriteEndElement();

                    xml.WriteStartElement("row");
                    xml.WriteElementString("OOI_ROW_TYPE_OF_ROW", "T");
                    xml.WriteElementString("OOI_ROW_TEXT", $"{firstFinishDate.Task.EndDate.ToString("yyyy-MM-dd")} - {lastFinishDate.Task.EndDate.ToString("yyyy-MM-dd")}");
                    xml.WriteEndElement();

                    foreach (var e in groupedElements)
                    {
                        xml.WriteStartElement("row");

                        var product = _repo.GetContext().Products.Find(e.Task.ProductId);
                        var taskFitter =
                            e.Task.Task_Fitter.FirstOrDefault(q => q.Status == ACSystem.Core.TaskStatus.Finished ||
                                                                    q.Status == ACSystem.Core.TaskStatus
                                                                        .FinishedWithFaults);

                        xml.WriteElementString("OOI_ROW_ARTICLE_NUMBER", (e.Task.ProductId.ToString()).ToString());
                        xml.WriteElementString("OOI_ROW_TEXT", product?.DescriptionSE ?? e.ProductName);

                        xml.WriteElementString("OOI_ROW_QUANTITY1", e.Quantity.ToString(CultureInfo.InvariantCulture).Replace('.', ','));
                        xml.WriteElementString("OOI_ROW_QUANTITY2", e.Quantity.ToString(CultureInfo.InvariantCulture).Replace('.', ','));
                        xml.WriteElementString("OOI_ROW_PRICE_EACH_CURRENT_CURRENCY",
                            e.UnitPrice.ToString(CultureInfo.InvariantCulture).Replace('.', ','));
                        xml.WriteElementString("OOI_ROW_PROJECT", e.Project.InSystemID);
                        xml.WriteElementString("OOI_ROW_PROFIT_CENTRE",
                            taskFitter?.Fitter?.User?.InSystemUserID?.ToString());
                        xml.WriteElementString("OOI_ROW_DATE2", e.Task.ClosedDate?.ToString("yyyy-MM-dd") ?? DateTime.Now.ToString("yyyy-MM-dd"));

                        xml.WriteEndElement(); // row

                        if (!string.IsNullOrEmpty(e.Task.InvoiceDescription))
                        {
                            var wrapedText = Core.Service.Strings.WordWrap(e.Task.InvoiceDescription, 60);

                            foreach (var w in wrapedText)
                            {
                                xml.WriteStartElement("row");
                                xml.WriteElementString("OOI_ROW_TYPE_OF_ROW", "T");
                                xml.WriteElementString("OOI_ROW_TEXT", $"{w}");
                                xml.WriteEndElement();
                            }
                        }
                    }
                }
                else
                {
                    var apartments = invoice.Elements.GroupBy(q => q.ApartmentNumber);

                    if (!string.IsNullOrEmpty(invoice.Elements.FirstOrDefault().Task.Apartment.Project.InvoiceDescription))
                    {
                        var wrapedText = Core.Service.Strings.WordWrap(invoice.Elements.FirstOrDefault().Task.Apartment.Project.InvoiceDescription, 60);

                        foreach (var w in wrapedText)
                        {
                            xml.WriteStartElement("row");
                            xml.WriteElementString("OOI_ROW_TYPE_OF_ROW", "T");
                            xml.WriteElementString("OOI_ROW_TEXT", $"{w}");
                            xml.WriteEndElement();
                        }
                    }

                    foreach (var a in apartments)
                    {
                        xml.WriteStartElement("row");
                        xml.WriteElementString("OOI_ROW_TYPE_OF_ROW", "T");
                        xml.WriteElementString("OOI_ROW_TEXT", $"LGH {a.Key}");
                        xml.WriteEndElement();

                        if (!string.IsNullOrEmpty(a.FirstOrDefault().Task.Apartment.InvoiceDescription))
                        {
                            var wrapedText = Core.Service.Strings.WordWrap(a.FirstOrDefault().Task.Apartment.InvoiceDescription, 60);

                            foreach (var w in wrapedText)
                            {
                                xml.WriteStartElement("row");
                                xml.WriteElementString("OOI_ROW_TYPE_OF_ROW", "T");
                                xml.WriteElementString("OOI_ROW_TEXT", $"{w}");
                                xml.WriteEndElement();
                            }
                        }

                        foreach (var e in a)
                        {
                            xml.WriteStartElement("row");

                            var product = _repo.GetContext().Products.Find(e.Task.ProductId);
                            var taskFitter =
                                e.Task.Task_Fitter.FirstOrDefault(q => q.Status == ACSystem.Core.TaskStatus.Finished ||
                                                                       q.Status == ACSystem.Core.TaskStatus
                                                                           .FinishedWithFaults);

                            xml.WriteElementString("OOI_ROW_ARTICLE_NUMBER", (e.Task.ProductId.ToString()).ToString());
                            xml.WriteElementString("OOI_ROW_TEXT", product?.DescriptionSE ?? e.ProductName);

                            xml.WriteElementString("OOI_ROW_QUANTITY1", e.Quantity.ToString(CultureInfo.InvariantCulture).Replace('.', ','));
                            xml.WriteElementString("OOI_ROW_QUANTITY2", e.Quantity.ToString(CultureInfo.InvariantCulture).Replace('.', ','));
                            xml.WriteElementString("OOI_ROW_PRICE_EACH_CURRENT_CURRENCY",
                                e.UnitPrice.ToString(CultureInfo.InvariantCulture).Replace('.', ','));
                            xml.WriteElementString("OOI_ROW_PROJECT", e.Project.InSystemID);
                            xml.WriteElementString("OOI_ROW_PROFIT_CENTRE",
                                taskFitter?.Fitter?.User?.InSystemUserID?.ToString());
                            xml.WriteElementString("OOI_ROW_DATE2", e.Task.ClosedDate?.ToString("yyyy-MM-dd") ?? DateTime.Now.ToString("yyyy-MM-dd"));

                            xml.WriteEndElement(); // row

                            if (!string.IsNullOrEmpty(e.Task.InvoiceDescription))
                            {
                                var wrapedText = Core.Service.Strings.WordWrap(e.Task.InvoiceDescription, 60);

                                foreach (var w in wrapedText)
                                {
                                    xml.WriteStartElement("row");
                                    xml.WriteElementString("OOI_ROW_TYPE_OF_ROW", "T");
                                    xml.WriteElementString("OOI_ROW_TEXT", $"{w}");
                                    xml.WriteEndElement();
                                }
                            }
                        }
                    }


                }
                xml.WriteEndElement(); // rows

                xml.WriteEndElement(); // order

                xml.WriteEndElement(); // orders

                xml.WriteEndElement(); // xmlimexport

                xml.Flush();

                byte[] bytes = ms.ToArray();

                Response.Clear();
                Response.AppendHeader("Content-Disposition", $"filename=invoice-{invoice.InvoiceNo}.xml");
                Response.AppendHeader("Content-Length", bytes.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(bytes);
                xml.Close();
            }
        }

        public void GenereteInvoiceXmlForAll()
        {
            var db = _repo.GetContext();

            var invoices = db.Invoice.Where(q => q.Status != InvoiceStatus.Xml && q.Complete == true);

            if (!invoices.Any()) return;

            using (var ms = new MemoryStream())
            {
                var xml = new XmlTextWriter(ms, Encoding.UTF8)
                {
                    Formatting = Formatting.Indented,
                    Indentation = 4
                };

                xml.WriteStartDocument();
                xml.WriteStartElement("xmlimexport");

                xml.WriteStartElement("customers");

                var currCustomers = new List<int>();
                foreach (var c in invoices)
                {
                    if (currCustomers.Contains(c.CustomerID)) continue;

                    currCustomers.Add(c.CustomerID);

                    xml.WriteStartElement("atKunder");
                    xml.WriteAttributeString("key", c.Customer.VATNumber);
                    xml.WriteAttributeString("option", "edit");

                    xml.WriteElementString("CUSTOMER_NUMBER", c.Customer.VATNumber);
                    xml.WriteElementString("CUSTOMER_NAME", c.Customer.Name);
                    xml.WriteElementString("CUSTOMER_MAILING_ADDRESS", c.Customer.Street);
                    xml.WriteElementString("CUSTOMER_MAILING_ADDRESS2", "");
                    xml.WriteElementString("CUSTOMER_ZIPCODE", c.Customer.PostalCode);
                    xml.WriteElementString("CUSTOMER_CITY", c.Customer.City);

                    xml.WriteEndElement(); // atKunder

                }

                xml.WriteEndElement(); // customers

                xml.WriteStartElement("articles");
                var currArticles = new List<int>();
                foreach (var invoice in invoices)
                {
                    foreach (var a in invoice.Elements)
                    {
                        if (currArticles.Contains(a.Task.ProductId)) continue;

                        currArticles.Add(a.Task.ProductId);

                        xml.WriteStartElement("atArtiklar");
                        xml.WriteAttributeString("key", a.Task.ProductId.ToString());

                        xml.WriteElementString("ARTICLE_NUMBER", a.Task.ProductId.ToString());
                        xml.WriteElementString("ARTICLE_NAME", a.ProductName);

                        xml.WriteEndElement(); //atArtiklar
                    }
                }
                xml.WriteEndElement(); // articles

                xml.WriteStartElement("projects");
                foreach (var invoice in invoices)
                {
                    xml.WriteStartElement("atProjekt");

                    xml.WriteAttributeString("key", invoice.ProjectID.ToString());

                    xml.WriteElementString("PROJECT_CODE_OF_PROJECT", invoice.ProjectID.ToString());
                    xml.WriteElementString("PROJECT_NAME", invoice.Project.ProjectNumber);
                    xml.WriteElementString("PROJECT_DATE_OF_BEGINNING", invoice.Project.StartDate.ToString("yyyy-MM-dd"));
                    xml.WriteElementString("PROJECT_DATE_OF_END", invoice.Project.EndDate.ToString("yyyy-MM-dd"));

                    xml.WriteEndElement(); // atProjekt;
                }
                xml.WriteEndElement(); // projects;

                xml.WriteStartElement("orders");

                foreach (var i in invoices)
                {
                    i.Status = InvoiceStatus.Xml;
                    db.Entry(i).State = EntityState.Modified;

                    xml.WriteStartElement("order");

                    xml.WriteStartElement("atOrder");
                    xml.WriteAttributeString("customerkey", i.Customer.VATNumber);
                    xml.WriteAttributeString("option", "edit");

                    xml.WriteElementString("OOI_HEAD_CUSTOMER_ORDER_NUMBER", i.Project.ProjectNumber);
                    xml.WriteElementString("OOI_HEAD_CARGO_AMOUNT", "0");
                    xml.WriteElementString("OOI_HEAD_DOCUMENT_DATE1", i.InvoiceDate.ToString("yyyy-MM-dd"));
                    xml.WriteElementString("OOI_HEAD_DOCUMENT_DATE2", i.PaymentTerm.ToString("yyyy-MM-dd"));

                    xml.WriteEndElement(); // atOrder

                    xml.WriteStartElement("rows");

                    var apartments = i.Elements.GroupBy(q => q.ApartmentNumber);

                    foreach (var a in apartments)
                    {
                        xml.WriteStartElement("row");

                        xml.WriteElementString("OOI_ROW_TEXT", $"LGH {a.Key}");

                        xml.WriteEndElement(); // row

                        foreach (var e in a)
                        {
                            xml.WriteStartElement("row");

                            var product = _repo.GetContext().Products.FirstOrDefault(q => q.Name == e.ProductName);
                            var taskFitter = e.Task.Task_Fitter.FirstOrDefault(q => q.Status == ACSystem.Core.TaskStatus.Finished ||
                                                                  q.Status == ACSystem.Core.TaskStatus
                                                                      .FinishedWithFaults);

                            xml.WriteElementString("OOI_ROW_ARTICLE_NUMBER", (product?.Id ?? 0).ToString());
                            xml.WriteElementString("OOI_ROW_TEXT", product?.DescriptionSE ?? e.ProductName);
                            xml.WriteElementString("OOI_ROW_QUANTITY1",
                                e.Quantity.ToString(CultureInfo.InvariantCulture).Replace('.', ','));
                            xml.WriteElementString("OOI_ROW_QUANTITY2",
                                e.Quantity.ToString(CultureInfo.InvariantCulture).Replace('.', ','));
                            xml.WriteElementString("OOI_ROW_PRICE_EACH_CURRENT_CURRENCY",
                                e.UnitPrice.ToString(CultureInfo.InvariantCulture).Replace('.', ','));
                            xml.WriteElementString("OOI_ROW_PROJECT", e.ProjectID.ToString());
                            xml.WriteElementString("OOI_ROW_PROFIT_CENTRE",
                                taskFitter?.Fitter?.User?.InSystemUserID?.ToString());
                            xml.WriteElementString("OOI_ROW_DATE2", e.Task.ClosedDate?.ToString("yyyy-MM-dd") ?? DateTime.Now.ToString("yyyy-MM-dd"));

                            xml.WriteEndElement(); // row
                        }
                    }
                    xml.WriteEndElement(); // rows

                    xml.WriteEndElement(); // order
                }

                xml.WriteEndElement(); // orders

                xml.WriteEndElement(); // xmlimexport

                xml.Flush();

                byte[] bytes = ms.ToArray();

                Response.Clear();
                Response.AppendHeader("Content-Disposition", $"filename=invoices-{DateTime.Now:yyyy-MM-dd-HH:mm:ss}.xml");
                Response.AppendHeader("Content-Length", bytes.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(bytes);
                xml.Close();

                db.SaveChanges();
            }
        }
        #endregion

        #region FEES
        public ActionResult Fees()
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista szkód", Url = "/Office/Fees", Icon = "fa fa-money", Active = true });

            ViewBag.LastNumber = _repo.LastPayrollNumber();

            return View();
        }

        public ActionResult FeesList(int? page, string orderBy = "feedate", string dest = "desc", string search = "", string filter = "0,1")
        {
            var pageSize = 20;

            var rowsCount = _repo.FeesCount();

            var pagesCount = rowsCount / pageSize;

            if (rowsCount <= pageSize || page <= 0 || page == null) page = 1;

            var excludeRows = (page - 1) * pageSize;

            var rows = 0;

            var list = _repo.FeesList(out rows, excludeRows, pageSize, orderBy, dest, search, filter);

            pagesCount = (rows % pageSize == 0) ? rows / pageSize : (rows / pageSize) + 1;

            if (pagesCount <= 0) pagesCount = 1;

            return Json(new { data = list, pages = pagesCount }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> FeeDetails(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Listy szkód", Url = "/Office", Icon = "fa fa-money", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Szczegóły listy szkód", Url = "/Office/FeeDetails", Icon = "", Active = true });

            Fees fee = await _repo.GetFee(id.Value);
            if (fee == null)
                return HttpNotFound();

            return View(fee);
        }

        public ActionResult FeeCreate(Fees fee, string CallbackURL, string[] FeeFile)
        {
            var feeID = _repo.CreateFee(fee, User.Identity.GetUserId());

            if (FeeFile != null)
            {
                using (var db = new ACSystemContext())
                {
                    foreach (var file in FeeFile)
                    {
                        var ff = new FeeFiles
                        {
                            FeeID = feeID ?? 0,
                            FileName = file
                        };

                        db.FeeFiles.Add(ff);
                    }

                    db.SaveChanges();
                }
            }

            if (string.IsNullOrEmpty(CallbackURL))
                return Redirect($"/Office/Fees");
            else
                return Redirect(CallbackURL);
        }

        public async Task<ActionResult> FeeEdit(Fees fee, string[] FeeFile)
        {
            var feeID = await _repo.EditFee(fee);

            if (FeeFile != null)
            {
                using (var db = new ACSystemContext())
                {
                    foreach (var file in FeeFile)
                    {
                        var ff = new FeeFiles
                        {
                            FeeID = feeID ?? 0,
                            FileName = file
                        };

                        db.FeeFiles.Add(ff);
                    }

                    db.SaveChanges();
                }
            }

            return Redirect($"/Office/Fees");
        }

        public async Task<ActionResult> FeeDelete(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = "";

            for (int i = 0; i < 3; i++)
            {
                result = await _repo.DeleteFee((int)id);

                if (result.Split(';')[0] == "true")
                    break;
            }

            var rsSplit = result.Split(';');

            if (rsSplit.Length > 1)
                return Json(new { success = rsSplit[0], message = rsSplit[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Nieoczekiwany błąd." }, JsonRequestBehavior.AllowGet);
        }

        #region MODALS
        public ActionResult Modal_AddFee(int? projectID, int? apartmentID)
        {
            var Fitters = _repo.GetFittersList();

            var Projects = _repo.GetProjectsList();
            var Apartments = (Projects != null && Projects.Count > 0) ? _repo.GetApartmentsList(Projects.FirstOrDefault().ID) : new List<ApartmentsSelect>();

            if (!projectID.HasValue || projectID.Value == 0)
                ViewBag.Projects = (Projects != null && Projects.Count > 0) ? Projects : new List<ProjectsSelect>();
            else
            {
                ViewBag.ProjectID = projectID;
                ViewBag.CallbackURL = "/Projects/Details/" + projectID;
            }

            var Tasks = new List<TasksSelect>();

            if (!apartmentID.HasValue || apartmentID.Value == 0)
            {
                ViewBag.Apartments = Apartments;
                Tasks = (Apartments != null && Apartments.Count > 0) ? _repo.GetTasksList(Apartments.FirstOrDefault().ID) : new List<TasksSelect>();
            }
            else
            {
                ViewBag.ApartmentID = apartmentID;
                Tasks = _repo.GetTasksList(apartmentID.Value);
                ViewBag.CallbackURL = "/Projects/ApartmentDetails/" + apartmentID;
            }

            ViewBag.Fitters = (Fitters != null && Fitters.Count > 0) ? Fitters : new List<FittersSelect>();

            ViewBag.Tasks = Tasks;

            ViewBag.LastNumber = _repo.LastFeeNumber();

            return PartialView();
        }

        public async Task<ActionResult> Modal_EditFee(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var fee = await _repo.GetFee(id.Value);

            if (fee == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var projectId = fee.ProjectID;
            var apartmentId = fee.ApartmentID;

            var Fitters = _repo.GetFittersList();
            var Projects = _repo.GetProjectsList();
            var Apartments = (Projects != null && Projects.Count > 0) ? _repo.GetApartmentsList(projectId ?? 0) : new List<ApartmentsSelect>();
            var Tasks = (Apartments != null && Apartments.Count > 0) ? _repo.GetTasksList(apartmentId ?? 0) : new List<TasksSelect>();

            ViewBag.Fitters = (Fitters != null && Fitters.Count > 0) ? Fitters : new List<FittersSelect>();
            ViewBag.Projects = (Projects != null && Projects.Count > 0) ? Projects : new List<ProjectsSelect>();
            ViewBag.Apartments = Apartments;
            ViewBag.Tasks = Tasks;

            return PartialView(fee);
        }

        public async Task<ActionResult> Modal_DetailsFee(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var fee = await _repo.GetFee(id.Value);

            if (fee == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return PartialView(fee);
        }
        #endregion


        #endregion

        #region AJAX
        [HttpGet]
        public ActionResult GetProjects(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return Json(new { data = _repo.GetProjectsList(id.Value) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetApartments(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return Json(new { data = _repo.GetApartmentsList(id.Value) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetTasks(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return Json(new { data = _repo.GetTasksList(id.Value) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCustomerPaymentTerm(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return Json(new { paymentTerm = _repo.CustomerPaymentTerm(id.Value) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetLastInvoiceDate(int customerID, int projectID)
        {
            return Json(new { lastDate = _repo.LastInvoice(customerID, projectID).ToString("dd.MM.yyyy") }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SERVICES
        private string CreatePDF(int ID, string path)
        {
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            LocalReport report = new LocalReport();

            ReportDataSource reportDataSource = new ReportDataSource();
            ACSystemDataSet dataSet1 = new ACSystemDataSet();
            DataSet1 dataset2 = new DataSet1();
            dataSet1.EnforceConstraints = false;
            dataSet1.BeginInit();

            reportDataSource.Name = "DataSet1";

            reportDataSource.Value = dataset2.Apartments;


            report.DataSources.Clear();
            report.DataSources.Add(reportDataSource);

            var readRDLC = System.IO.File.Open(Server.MapPath("~/Reports/Invoice.rdlc"), FileMode.Open);

            report.LoadReportDefinition(readRDLC);

            readRDLC.Dispose();
            readRDLC.Close();

            dataSet1.EndInit();

            Reports.ACSystemDataSetTableAdapters.Invoice1TableAdapter tableAdapter = new Reports.ACSystemDataSetTableAdapters.Invoice1TableAdapter();
            tableAdapter.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            tableAdapter.ClearBeforeFill = true;

            tableAdapter.Fill(dataSet1.Invoice1, ID);


            //ReportViewer rv = new ReportViewer();
            //rv.ProcessingMode = ProcessingMode.Local;
            //rv.LocalReport.DataSources.Add(reportDataSource);
            //rv.LocalReport.ReportPath = Server.MapPath("~/Reports/Invoice.rdlc");

            byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            System.IO.File.WriteAllBytes(path, bytes);


            //Response.Buffer = true;
            //Response.Clear();
            //Response.ContentType = mimeType;
            //Response.AddHeader("content-disposition", "attachment; filename=" + "test" + "." + "pdf");
            //Response.BinaryWrite(bytes);
            //Response.Flush();

            if (warnings.Count() > 0)
            {
                var msg = "";

                foreach (var warning in warnings)
                {
                    msg += $"{warning.Code} | {warning.Message} | {warning.ObjectName} | {warning.ObjectType} | {warning.Severity}{Environment.NewLine}";
                }

                return msg;
            }

            return String.Empty;
            //var url = "http://www.acbygg.pl/Print/Invoice/" + ID;

            //#if DEBUG
            //url = "http://localhost:64228/Print/Invoice/" + ID;
            //#endif

            //var pdfConvert = new PdfConvert(new PdfConvertEnvironment
            //{
            //    TempFolderPath = Path.GetTempPath(),
            //    WkHtmlToPdfPath = Server.MapPath("~/dist/wkhtmltopdf.exe"),
            //    Timeout = 60000
            //});

            //try
            //{
            //    pdfConvert.ConvertHtmlToPdf(
            //            new PdfDocument
            //            {
            //                Url = url,
            //                FooterCenter = "Page [page] of [topage]"
            //            },
            //            new PdfOutput
            //            {
            //                OutputFilePath = path
            //            }
            //        );
            //}
            //catch (PdfConvertException ex)
            //{
            //    return ex.Message;
            //}
            //catch (Exception ex)
            //{
            //    return ex.Message;
            //}

            //return String.Empty;
        }

        private string CreatePDFPayrollFromHTML(Payroll payroll, string FitterID, string FitterName, string FitterEmail)
        {
            var url = $"http://{HttpContext.Request.Url.Host}:{HttpContext.Request.Url.Port}/Office/PayrollForFitter?PayrollID={payroll.Id}&FitterName={FitterName.Replace(' ', '_')}&FitterID={FitterID}";

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

            var fileName = $"Lista-plac-{FitterName.Replace(' ', '_')}-{DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss")}";

            var path = Server.MapPath($"~/dist/PayrollPDF/{fileName}.pdf");

            using (var sr = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
            {
                var fileContent = sr.ReadToEnd();

                PdfDocument pdf = PdfGenerator.GeneratePdf(fileContent, new PdfGenerateConfig { PageSize = PageSize.A4, PageOrientation = PageOrientation.Landscape });

                try
                {
                    pdf.Save(path);
                    MailService.Payroll(payroll, FitterName, FitterEmail, path);
                }
                catch (Exception ex)
                {
                    Helpers.Log(ex, "CreatePDFPayrollFromHTML", User.Identity.Name);
                }


                //using (var sw = new StreamWriter(Server.MapPath($"~/dist/RaportHTML/{fileName}.html")))
                //{
                //    sw.Write(fileContent);
                //    sw.Flush();
                //    sw.Close();
                //}
            }

            return String.Empty;
        }

        [HttpPost]
        public ActionResult UploadImage()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var fi = new FileInfo(fileName);
                        var ext = fi.Extension.ToUpper();
                        newFileName += fi.Extension;

                        var filePath = Path.Combine(Server.MapPath(FEES_DIR), newFileName);
                        file.SaveAs(filePath);

                        if (ext == ".JPG" || ext == ".JPEG" || ext == ".PNG" || ext == ".BMP")
                        {
                            using (var image = Image.FromFile(filePath))
                            using (var newImage = Images.ScaleImage(image, 600, 800))
                            {
                                newFileName = RESIZED_IMAGE_PREFIX + newFileName;

                                image.Dispose();
                                switch (ext)
                                {
                                    case ".JPG":
                                        newImage.Save(Path.Combine(Server.MapPath(FEES_DIR), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".JPEG":
                                        newImage.Save(Path.Combine(Server.MapPath(FEES_DIR), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".PNG":
                                        newImage.Save(Path.Combine(Server.MapPath(FEES_DIR), newFileName), ImageFormat.Png);
                                        break;
                                    case ".BMP":
                                        newImage.Save(Path.Combine(Server.MapPath(FEES_DIR), newFileName), ImageFormat.Bmp);
                                        break;
                                }
                            }

                            System.IO.File.Delete(filePath);
                        }
                    }

                    return Json(new { result = "true", fileName = fileName, newFileName = newFileName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = "false", message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        private decimal FinalPayrollSum(int payrollId)
        {
            var db = _repo.GetContext();

            var payroll = db.Payroll.Find(payrollId);
            decimal hourlySum = 0;

            foreach (var p in payroll.Items)
            {
                var fitter = db.Fitters.Find(p.FitterID);
                hourlySum += p.WorkTime * fitter.HourlyRate;
            }

            return payroll.Sum - hourlySum;
        }

        #region CONST
        private const string FEES_DIR = "~/dist/Fees/";
        private const string RESIZED_IMAGE_PREFIX = "resized_";
        #endregion
    }
}