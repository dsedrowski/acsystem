﻿using ACSystem.Core;
using ACSystem.Core.Filters;
using ACSystem.Core.IRepo;
using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using ACSystem.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ACSystem.Controllers
{
    [AuthLog(Roles = "Admin")]
    public class HomeController : Controller
    {
        private readonly IHomeRepo _repo;

        public HomeController(IHomeRepo repo)
        {
            _repo = repo;
        }

        public ActionResult Index()
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Pulpit", Url = "/", Icon = "dashboard", Active = true });
            //ViewBag.Roles = Roles.GetRolesForUser();
            
            var projects = _repo.ProjectsList();
            var apartments = _repo.ApartmentsList((projects.Count() > 0) ? projects.FirstOrDefault().Id : 0);
            var tasks = _repo.TasksList((apartments.Count() > 0) ? apartments.FirstOrDefault().Id : 0);
            var fitters = _repo.GetContext().Fitters.ToList();
            var calendars = _repo.GetContext().Calendar.ToList();
            

            ViewBag.ProjectsSelect =  (projects.Count() > 0) ? projects.ToList() : null;
            ViewBag.ApartmentsSelect = (apartments.Count() > 0) ? apartments.ToList() : null;
            ViewBag.TasksSelect = (tasks.Count() > 0) ? tasks.ToList() : null;
            ViewBag.Fitters = (fitters.Count() > 0) ? fitters : null;
            ViewBag.Calendars = calendars;
            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Calendar(DateTime start, DateTime end, string types, string calendars, string search)
        {
            var calListString = calendars.Split(',');

            var calListInt = (from c in calListString where !string.IsNullOrEmpty(c) select int.Parse(c)).Select(num => (int?) num).ToList();


            var projects = _repo.GetContext().Projects.Where(q => (
                                                        (q.StartDate >= start && q.EndDate <= end) ||
                                                        (q.StartDate <= start && q.EndDate >= end) ||
                                                        (q.StartDate <= start && q.EndDate <= end && q.EndDate >= start) ||
                                                        (q.StartDate >= start && q.EndDate >= end && q.StartDate <= end)
                                                        )
                                                        &&
                                                        (q.ProjectNumber.Contains(search)) &&
                                                        (q.Street.Contains(search)) &&
                                                        (q.City.Contains(search))
                                                        &&
                                                        (calListInt.Contains(q.CalendarID) || q.CalendarID == null)
                                                        &&
                                                        (q.ShowInCalendar)
                                                        );

            var tasks = _repo.TasksList().Where(q =>
                                                    (
                                                        (q.StartDate >= start && q.EndDate <= end) ||
                                                        (q.StartDate <= start && q.EndDate >= end) ||
                                                        (q.StartDate <= start && q.EndDate <= end && q.EndDate >= start) ||
                                                        (q.StartDate >= start && q.EndDate >= end && q.StartDate <= end)
                                                    ) 
                                                    && 
                                                        (q.Apartment.NumberOfApartment.Contains(search)) &&
                                                        (q.Apartment.Project.ProjectNumber.Contains(search)) &&
                                                        (q.Apartment.Project.Street.Contains(search)) &&
                                                        (q.Apartment.Project.City.Contains(search))
                                                    &&
                                                        (calListInt.Contains(q.Apartment.Project.CalendarID) || q.Apartment.Project.CalendarID == null)
                                                );
            var events = _repo.EventsList().Where(q =>
                                                      (
                                                        (q.Start >= start && q.End <= end) ||
                                                        (q.Start <= start && q.End >= end) ||
                                                        (q.Start <= start && q.End <= end && q.End >= start) ||
                                                        (q.Start >= start && q.End >= end && q.Start <= end)
                                                      )
                                                      );

            List<CalendarJson> json = new List<CalendarJson>();

            if (projects != null && types.Contains("3"))
            {
                foreach (var t in projects)
                {
                    var allDay = ((t.StartDate - t.EndDate).Days < 0);

                    if ((t.StartDate.Day == t.EndDate.Day) && (t.StartDate.Month == t.EndDate.Month) && (t.StartDate.Year == t.EndDate.Year))
                    {
                        allDay = false;
                    }

                    var color = "#333333";

                    if (t.Calendar != null)
                        color = t.Calendar.ColorHex;

                    json.Add(new CalendarJson
                    {
                        id = "project-" + t.Id,
                        title = $"Projekt {t.ProjectNumber}",
                        allDay = allDay,
                        start = t.StartDate.ToString("yyyy-MM-dd HH:mm"),
                        end = (allDay) ? t.EndDate.AddDays(1).ToString("yyyy-MM-dd HH:mm") : t.EndDate.ToString("yyyy-MM-dd HH:mm"),
                        editable = true,
                        color = color
                    });
                }
            }

            if (tasks != null && types.Contains("0"))
            {
                foreach (var t in tasks)
                {
                    if (calListInt.Contains(t.Apartment.Project.CalendarID) == false && t.Apartment.Project.CalendarID != null)
                        continue;

                    var allDay = ((t.StartDate - t.EndDate).Days < 0);

                    if ((t.StartDate.Day == t.EndDate.Day) && (t.StartDate.Month == t.EndDate.Month) && (t.StartDate.Year == t.EndDate.Year))
                    {
                        allDay = false;
                    }

                    var color = "#333333";

                    if (t.Apartment.Project.Calendar != null)
                        color = t.Apartment.Project.Calendar.ColorHex;

                    //if (t.Production == TaskProduction.Complaint) color = "#FFDA00";
                    //else if (t.Production == TaskProduction.Warranty) color = "#FF4900";

                    var id = $"group-{t.Apartment.ProjectId}";

                    if (json.Any(q => q.id == id && q.start.Split(' ')[0] == t.StartDate.ToString("yyyy-MM-dd"))) continue;

                    json.Add(new CalendarJson
                    {
                        id = id,
                        title = $"Projekt {t.Apartment.Project.ProjectNumber}, {t.Apartment.Project.City} {t.Apartment.Project.Street}",
                        allDay = allDay,
                        start = t.StartDate.ToString("yyyy-MM-dd HH:mm"),
                        end = (allDay) ? t.EndDate.AddDays(1).ToString("yyyy-MM-dd HH:mm") : t.EndDate.ToString("yyyy-MM-dd HH:mm"),
                        editable = true,
                        color = color
                        //id = "task-" + t.Id,
                        //title = $"Projekt {t.Apartment.Project.ProjectNumber}, LGH {t.Apartment.NumberOfApartment}, Produkt {t.GetTaskName(q.Apartment.ProjectId)} {t.TaskSufix}",
                        //allDay = allDay,
                        //start = t.StartDate.ToString("yyyy-MM-dd HH:mm"),
                        //end = (allDay) ? t.EndDate.AddDays(1).ToString("yyyy-MM-dd HH:mm") : t.EndDate.ToString("yyyy-MM-dd HH:mm"),
                        //editable = true,
                        //color = color
                    });
                }
            }

            if (events != null)
            {
                foreach (var e in events)
                {
                    var title = String.Empty;
                    var id = String.Empty;
                    DateTime? startHand = null, endHand = null;
                    bool? allDayHand = null;

                    var skip = false;

                    var color = e.Calendar?.ColorHex ?? "#333333";

                    if (e.ReferenceTo.HasValue || !string.IsNullOrEmpty(e.ReferenceToUser))
                    {
                        switch (e.Type)
                        {
                            case EventType.Project:
                                if (types.Contains("4") == false) continue;

                                var project = _repo.GetContext().Projects.Find(e.ReferenceTo.Value);

                                if (project == null)
                                {
                                    skip = true;
                                    break;
                                }

                                if (calListInt.Contains(project.CalendarID) == false && project.CalendarID != null)
                                    skip = true;

                                title = $"{project.ProjectNumber} {e.Title}";
                                id = $"event4project-{e.Id}";

                                if (project.Calendar != null)
                                    color = project.Calendar.ColorHex;

                                break;
                            case EventType.Task:
                                if (types.Contains("1") == false)
                                    continue;

                                var task = await _repo.GetTask(e.ReferenceTo.Value);

                                if (task == null)
                                {
                                    skip = true;
                                    break;
                                }

                                if (calListInt.Contains(task.Apartment.Project.CalendarID) == false && task.Apartment.Project.CalendarID != null)
                                    skip = true;
                                
                                title = $"{task.Apartment.Project.ProjectNumber}.{task.Apartment.Letter}{task.Apartment.NumberOfApartment} {task.GetTaskName(task.Apartment.ProjectId)} {task.TaskSufix} {e.Title}";
                                id = $"event4task-{e.Id}";

                                if (task.Apartment.Project.Calendar != null)
                                    color = task.Apartment.Project.Calendar.ColorHex;

                                break;
                            case EventType.User:
                                if (types.Contains("2") == false)
                                    continue;
                                var fitter = _repo.GetContext().Fitters.FirstOrDefault(q => q.Id == e.ReferenceToUser);
                                if (fitter == null)
                                {
                                    skip = true;
                                    break;
                                }

                                if (calListInt.Contains(fitter.User.CalendarID) == false && fitter.User.CalendarID != null)
                                    skip = true;

                                title = $"{fitter.User.FullName} {e.Title}";
                                id = $"event4fitter-{e.Id}";

                                if (fitter.User.Calendar != null)
                                    color = fitter.User.Calendar.ColorHex;

                                break;

                            case EventType.Plan:
                                if (types.Contains("2") == false)
                                    continue;

                                fitter = _repo.GetContext().Fitters.FirstOrDefault(q => q.Id == e.ReferenceToUser);
                                task = _repo.GetContext().Tasks.FirstOrDefault(q => q.Id == e.ReferenceTo);

                                if (fitter == null || task == null)
                                {
                                    skip = true;
                                    break;
                                }

                                if (calListInt.Contains(fitter.User.CalendarID) == false && fitter.User.CalendarID != null)
                                    skip = true;

                                title = $"{fitter.User.FullName} {e.Title}";
                                id = $"event4fitter-{e.Id}";
                                //startHand = task.StartDate;
                                //endHand = task.EndDate;
                                allDayHand = false;

                                if (fitter.User.Calendar != null)
                                    color = fitter.User.Calendar.ColorHex;

                                break;
                        }
                    }

                    if (skip == false)
                    {
                        var defAllDay = (allDayHand.HasValue) ? allDayHand.Value : e.AllDay;
                        var defEnd = (endHand.HasValue) ? endHand.Value : e.End;

                        json.Add(new CalendarJson
                        {
                            id = id,
                            title = title,
                            allDay = defAllDay,
                            start = (startHand.HasValue) ? startHand.Value.ToString("yyyy-MM-dd HH:mm") : e.Start.ToString("yyyy-MM-dd HH:mm"),
                            end = (defAllDay) ? defEnd.AddDays(1).ToString("yyyy-MM-dd HH:mm") : defEnd.ToString("yyyy-MM-dd HH:mm"),
                            editable = true,
                            color = color
                        });
                    }
                }
            }

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> TaskEvent(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var t = await _repo.GetTask((int)id);

            return PartialView(t);
        }

        [HttpGet]
        public async Task<ActionResult> EventForTask(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var event4Task = await _repo.GetEvent(id.Value);

            if (event4Task.ReferenceTo.HasValue)
            {
                var t = await _repo.GetTask(event4Task.ReferenceTo.Value);
                var task = new TaskFullViewModel
                {
                    TaskID = t.Id,
                    ProjectId = t.Apartment.ProjectId,
                    ProjectNumber = t.Apartment.Project.ProjectNumber,
                    ApartmentId = t.ApartmentId,
                    ApartmentNumber = t.Apartment.Letter + t.Apartment.NumberOfApartment,
                    CurrentFitterId = t.Fitters.FirstOrDefault()?.Id ?? null,
                    CurrentFitterName = t.Fitters.FirstOrDefault()?.User.FullName ?? null,
                    ProductName = t.GetTaskName(t.Apartment.ProjectId)
                };
                ViewBag.TaskData = task;
            }


            return PartialView(event4Task);
        }

        [HttpGet]
        public async Task<ActionResult> EventForProject(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var event4Project = await _repo.GetEvent(id.Value);

            if (event4Project.ReferenceTo.HasValue)
            {
                var t = _repo.GetContext().Projects.Find(event4Project.ReferenceTo.Value);
                var project = new ProjectFullViewModel()
                {
                    ProjectId = t.Id,
                    ProjectNumber = t.ProjectNumber
                };

                ViewBag.ProjectData = project;
            }


            return PartialView(event4Project);
        }

        [HttpGet]
        public async Task<ActionResult> EventForFitter(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var event4Fitter = await _repo.GetEvent(id.Value);
            
            return PartialView(event4Fitter);
        }

        [HttpGet]
        public async Task<ActionResult> UpdateEvent(string type, int id, DateTime start, DateTime? end, bool allDay, string startString)
        {
            var startDate = DateTime.Parse(startString);

            if (end == start && !allDay)
                end = start.AddHours(2);

            if (end == null)
                end = start.AddHours(2);

            if (allDay)
                end = end.Value.AddDays(-1);

            switch (type)
            {
                case "task":
                    var task = await _repo.GetTask(id);
                    task.StartDate = start;
                    task.EndDate = (DateTime)end;
                    _repo.TaskEdit(task);
                    break;
                case "event4task":
                    var events = await _repo.GetEvent(id);
                    events.Start = start;
                    events.End = (DateTime)end;
                    events.AllDay = allDay;
                    _repo.EventEdit(events);
                    break;
                case "event4fitter":
                    var eFitter = await _repo.GetEvent(id);
                    eFitter.Start = start;
                    eFitter.End = (DateTime)end;
                    eFitter.AllDay = allDay;
                    _repo.EventEdit(eFitter);
                    break;
                case "event4project":
                    var eProject = await _repo.GetEvent(id);
                    eProject.Start = start;
                    eProject.End = (DateTime)end;
                    eProject.AllDay = allDay;
                    _repo.EventEdit(eProject);
                    break;
                case "group":
                    var context = _repo.GetContext();
                    var project = context.Projects.Find(id);

                    if (project == null)
                        break;

                    foreach (var apartment in project.Apartments)
                    {
                        foreach (var t in apartment.Tasks)
                        {
                            if (t.StartDate.Date == startDate.Date)
                            {
                                t.StartDate = start;
                                t.EndDate = end.Value;

                                context.Entry(t).State = System.Data.Entity.EntityState.Modified;
                            }
                        }
                    }

                    context.SaveChanges();
                    break;
            }

            try
            {
                _repo.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Json(new { succes = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ApartmentsList(int projectId)
        {
            List<object> json = new List<object>();

            var list = _repo.ApartmentsList(projectId).Where(q => q.ProjectId == projectId).Select(q => new { Id = q.Id, NumberOfApartment = q.Letter + q.NumberOfApartment }).ToList();

            foreach (var l in list)
                json.Add(Json(new { Id = l.Id, NumberOfApartment = l.NumberOfApartment }).Data);

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult TasksList(int apartmentId)
        {
            List<object> json = new List<object>();

            var list = _repo.TasksList(apartmentId).Where(q => q.ApartmentId == apartmentId).Select(q => new { Id = q.Id, ProductName = q.GetTaskName(q.Apartment.ProjectId), Sufix = q.TaskSufix }).ToList();

            foreach (var l in list)
                json.Add(Json(new { Id = l.Id, ProductName = l.ProductName, Sufix = l.Sufix }).Data);

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteEvent(int ID)
        {
            var db = _repo.GetContext();

            var e = db.Events.Find(ID);

            if (e == null)
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);

            db.Events.Remove(e);
            db.SaveChanges();

            return Json(new { result = "true"}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddCalendar(string Name, string ColorHex)
        {
            var db = _repo.GetContext();

            var calendar = new Calendar
            {
                Name = Name,
                ColorHex = ColorHex
            };

            db.Calendar.Add(calendar);

            try
            {
                db.SaveChanges();

                return Redirect("/");
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult EditCalendar(int ID, string Name)
        {
            var db = _repo.GetContext();

            var calendar = db.Calendar.Find(ID);

            if (calendar == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            calendar.Name = Name;

            db.Entry(calendar).State = System.Data.Entity.EntityState.Modified;

            db.SaveChanges();

            return Redirect("/");
        }

        [HttpPost]
        public ActionResult AJAX_EditEvent(int ID, string Title, string Description, DateTime Start, DateTime End, bool AllDay)
        {
            var db = new ACSystemContext();

            var data = db.Events.Find(ID);

            data.Title = Title;
            data.Description = Description;
            data.Start = Start;
            data.End = End;
            data.AllDay = AllDay;

            db.Entry(data).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        private class CalendarJson
        {
            public string id { get; set; }
            public string title { get; set; }
            public bool allDay { get; set; }
            public string start { get; set; }
            public string end { get; set; }
            public bool editable { get; set; }
            public string color { get; set; }
        }
    }
}