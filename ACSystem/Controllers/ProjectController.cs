﻿using ACSystem.Core;
using ACSystem.Core.Filters;
using ACSystem.Core.Models;
using ACSystem.Models.View;
using ACSystem.Services;
using ACSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ACSystem.Controllers
{
    [AuthLog(Roles = "Admin")]
    public class ProjectController : Controller
    {
        private readonly IProjectService _projectService;
        private readonly ISettingsService _settingsService;

        public ProjectController(IProjectService projectService, ISettingsService settingsService)
        {
            _projectService = projectService;
            _settingsService = settingsService;
        }

        /// <summary>
        /// Pobiera i wyświetla szczegółowe dane o projekcie
        /// </summary>
        /// <param name="id">ID Projektu</param>
        [HttpGet]
        public async Task<ActionResult> Get(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Parametr ID Projektu jest wymagany do obsługi tego żądania.");

            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista projektów", Url = "/Projects", Icon = "fa fa-archive", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Szczegóły projektu", Url = "/Projects/Details", Icon = "", Active = true });

            var data = await _projectService.GetByIdAsync(id.Value);

            if (data == null)
                return HttpNotFound($"Nie znaleziono projektu o podanym numerze ID: {id}");

            var settings = await _settingsService.GetSingleSettingsAsync();
            var canFinalize = await _projectService.CanFinalizeProjectAsync(id.Value);

            return View("Details", new ProjectDetailsViewModel
            {
                Id = data.Id,
                InSystemId = data.InSystemID,
                ProjectName = data.ProjectNumber,
                Status = data.Status,
                StartDate = data.StartDate,
                EndDate = data.EndDate,
                Address = $"{data.Street}, {data.City}",
                Supervisor = data.Supervisor?.FullName,
                InvoiceDescription = data.InvoiceDescription,

                ParentProjectID = data.ParentProjectID,
                ParentProjectName = data.ParentProject?.ProjectNumber,

                WorkTimeDone = Math.Round(
                    data.Apartments.Sum(q => q.Tasks.Sum(
                                             x => x.Task_Fitter.Where(y => Tasks.StatusesToCalculateTimes.Contains(y.Status) && y.IsActive)
                                             .Sum(y => y.WorkDone * ((x.Production == TaskProduction.Developer) ?
                                                                                                        x.Product.ExpectedTime :
                                                                                                        Temp.Settings.ServiceTaskTime)))), 2),
                WorkTimeRequired = Math.Round(
                    data.Apartments.Sum(q => q.Tasks.Sum(
                                             x => x.ProductCount * ((x.Production == TaskProduction.Developer) ?
                                                                                                        x.Product.ExpectedTime :
                                                                                                        Temp.Settings.ServiceTaskTime))),2),

                InvoicesQuantity = data.InvoiceList.Count,
                PlaceErrorsQuantity = data.PlaceErrors.Count,
                FeesQuantity = data.Fees.Count,
                InventoryQuantity = data.InventoryList.Count,

                FirstDoService = data.FirstDoService ?? false,
                Remind = DateTime.Now >= data.EndDate.AddDays((settings?.ProjectEndReminder ?? 0) * -1) &&
                         data.Status != ProjectStatus.Finished &&
                         data.Status != ProjectStatus.FinishedWithFaults,
                IsChild = data.ChildProject,
                IsChecked = data.IsChecked,
                HasChilds = data.ChildProjects.Any(),

                Customer = new Customers
                {
                    Id = data.Customer.Id,
                    Name = data.Customer.Name,
                },
                Calendar = (data.Calendar == null) ? null : new Calendar
                                                            {
                                                                ColorHex = data.Calendar.ColorHex,
                                                                Name = data.Calendar.Name,
                                                            },
                Settings = (settings != null) ? null : new Settings
                                                        {
                                                            ServiceTaskTime = settings.ServiceTaskTime,
                                                            InvoicePercent = settings.InvoicePercent,
                                                            ProjectEndReminder = settings.ProjectEndReminder,
                                                            HoursPerDay = settings.HoursPerDay
                                                        },
                BeforeCheckValidationMessages = canFinalize
            });
        }

        #region Partial views

        [HttpGet]
        public async Task<ActionResult> Details_ProjectManagers(int id)
        {
            var projectManagers = await _projectService.GetManagersAsync(id);

            return PartialView("Partials/Details_ProjectManagers", projectManagers);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Events(int id)
        {
            var events = await _projectService.GetEventsForProject(id);

            return PartialView("Partials/Details_Events", events);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Apartments(int id)
        {
            var project = await _projectService.GetByIdAsync(id);

            return PartialView("Partials/Details_Apartments", project);
        }

        [HttpGet]
        public async Task<ActionResult> Details_ChildProjects(int id)
        {
            var project = await _projectService.GetByIdAsync(id);

            return PartialView("Partials/Details_ChildProjects", project);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Products(int id)
        {
            var project = await _projectService.GetByIdAsync(id);

            return PartialView("Partials/Details_Products", project);
        }

        [HttpGet]
        public async Task<ActionResult> Details_ProductPlan(int id)
        {
            var project = await _projectService.GetByIdAsync(id);

            return PartialView("Partials/Details_ProductPlan", project);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Invoices(int id)
        {
            var invoices = await _projectService.GetInvoices(id);

            return PartialView("Partials/Details_Invoices", invoices);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Payrolls(int id)
        {
            var payrolls = await _projectService.GetPayrolls(id);

            return PartialView("Partials/Details_Payrolls", payrolls);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Fees(int id)
        {
            var fees = await _projectService.GetFees(id);

            return PartialView("Partials/Details_Fees", fees);
        }

        [HttpGet]
        public async Task<ActionResult> Details_PlaceErrors(int id)
        {
            var project = await _projectService.GetByIdAsync(id);

            return PartialView("Partials/Details_PlaceErrors", project.PlaceErrors);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Content(int id)
        {
            var project = await _projectService.GetByIdAsync(id);

            return PartialView("Partials/Details_Content", project);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Orders(int id)
        {
            var project = await _projectService.GetByIdAsync(id);

            return PartialView("Partials/Details_Orders", project);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Documents(int id)
        {
            var project = await _projectService.GetByIdAsync(id);

            return PartialView("Partials/Details_Documents", project.Documents);
        }

        [HttpGet]
        public async Task<ActionResult> Details_GroupReports(int id)
        {
            var project = await _projectService.GetByIdAsync(id);

            return PartialView("Partials/Details_GroupReports", project.ReportNags);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Access(int id)
        {
            var project = await _projectService.GetByIdAsync(id);

            return PartialView("Partials/Details_Access", project);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Activities(int id)
        {
            var project = await _projectService.GetByIdAsync(id);

            return PartialView("Partials/Details_Activities", project.Activities);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Inventory(int id)
        {
            var project = await _projectService.GetByIdAsync(id);

            return PartialView("Partials/Details_Inventory", project.InventoryList);
        }

        [HttpGet]
        public async Task<ActionResult> Details_Diff(int id)
        {
            ViewBag.CheckTaskPriceNorm = await _projectService.CheckTaskPriceNorm(id);
            ViewBag.CheckPayrollRateNorm = await _projectService.CheckPayrollRateNorm(id);

            return PartialView("Partials/Details_Diff");
        }

        #endregion
    }
}