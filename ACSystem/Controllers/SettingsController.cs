﻿using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACSystem.Controllers
{
    public class SettingsController : Controller
    {
        // GET: Settings
        public ActionResult Index()
        {
            using (var ctx = new ACSystemContext()) {
                var model = ctx.Settings.FirstOrDefault();

                if (model == null)
                    return new HttpNotFoundResult();

                return View(model);
            }
            //var SETTINGS_PATH = Server.MapPath("~/dist/settings.xml");

            //ViewBag.PercentToGet = Settings.ReadSetting("PercentToGet", SETTINGS_PATH);
            //ViewBag.ProjectEndDaysRemind = Settings.ReadSetting("ProjectEndDaysRemind", SETTINGS_PATH);

        }       

        [HttpPost]
        public ActionResult Index(Settings _model)
        {
            //var SETTINGS_PATH = Server.MapPath("~/dist/settings.xml");

            //ViewBag.PercentToGet = _model.PercentToGet;
            //ViewBag.ProjectEndDaysRemind = _model.ProjectEndDaysRemind;

            //if (!Settings.WriteSetting("PercentToGet", _model.PercentToGet, SETTINGS_PATH))
            //    ViewBag.PercentToGet = Settings.ReadSetting("PercentToGet", SETTINGS_PATH);
            
            //if (!Settings.WriteSetting("ProjectEndDaysRemind", _model.ProjectEndDaysRemind, SETTINGS_PATH))
            //    ViewBag.ProjectEndDaysRemind = Settings.ReadSetting("ProjectEndDaysRemind", SETTINGS_PATH);

            return View();
        }
    }

    public class SettingsModel
    {
        public string PercentToGet { get; set; }
        public string ProjectEndDaysRemind { get; set; }
    }
}