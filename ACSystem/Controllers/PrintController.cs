﻿using ACSystem.Core.Filters;
using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ACSystem.Reports;
using Microsoft.Reporting.WinForms;
using ACSystem.Core.Extensions;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Core.Objects;
using System.Reflection;

namespace ACSystem.Controllers
{
    public class PrintController : Controller
    {
        private ACSystemContext _db;

        public PrintController()
        {
            _db = new ACSystemContext();
        }

        // GET: Print
        public ActionResult Invoice(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var invoice = _db.Invoice.Find(id.Value);

            if (invoice == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var pdfPath = Server.MapPath($"~/dist/Invoice/{id.Value}.pdf");

            if (!System.IO.File.Exists(pdfPath))
            {
                CreatePDF(id.Value, pdfPath);
            }

            return PartialView(invoice);
        }

        public ActionResult InvoiceExist(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var invoice = _db.Invoice.Find(id.Value);

            if (invoice == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var pdfPath = Server.MapPath($"~/dist/Invoice/{id.Value}.pdf");

            if (!System.IO.File.Exists(pdfPath))
            {
                CreatePDF(id.Value, pdfPath);
            }

            return Json(new {success = true}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Raport(int? ID, bool Print = false)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var task = _db.Tasks.Find(ID.Value);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.Print = Print;

            return PartialView(task);
        }

        [Obsolete]
        public ActionResult ApartmentRaportGrouped(int? ID, bool Print = false)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var report = _db.ReportNag.Find(ID.Value);

            if (report == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.Print = Print;

            var finalReport = _db.ReportElem.Where(q => q.ReportNagID == ID.Value).ToList();

            var secondReport = new List<ReportElem>();

            if (report.Type == Core.TaskReportType.Dev_Construction)
            {
                var data = _db.ReportNag.FirstOrDefault(q => q.ApartmentID == report.ApartmentID && EntityFunctions.TruncateTime(q.Date) == EntityFunctions.TruncateTime(report.Date) && q.Type == Core.TaskReportType.Dev_Kitchen);

                if (data != null)
                    secondReport.AddRange(_db.ReportElem.Where(q => q.ReportNagID == data.ID).ToList());
            }
            //else if (report.Type == Core.TaskReportType.Dev_Kitchen)
            //{
            //    var data = _db.ReportNag.FirstOrDefault(q => q.ApartmentID == report.ApartmentID && EntityFunctions.TruncateTime(q.Date) == EntityFunctions.TruncateTime(report.Date) && q.Type == Core.TaskReportType.Dev_Construction);

            //    if (data != null)
            //        secondReport.AddRange(_db.ReportElem.Where(q => q.ReportNagID == data.ID).ToList());
            //}

            finalReport.AddRange(secondReport);

            report.Elements = finalReport;

            var user = _db.ACSystemUser.FirstOrDefault(q => q.Email == User.Identity.Name);
            ViewBag.Podpis = user?.Podpis;

            return PartialView(report);
        }

        [Obsolete]
        public ActionResult MassivePrintGroupReports(string Reports, bool Print)
        {
            var array = Reports.ToIntArray(',');

            //_db.Database.Log = Console.Write;
            var reports = _db.ReportNag.Where(q => array.Contains(q.ID)).ToList();

            foreach (var r in reports)
            {
                var finalReport = _db.ReportElem.Where(q => q.ReportNagID == r.ID).ToList();

                var secondReport = new List<ReportElem>();

                if (r.Type == Core.TaskReportType.Dev_Construction)
                {
                    var data = _db.ReportNag.FirstOrDefault(q => q.ApartmentID == r.ApartmentID && EntityFunctions.TruncateTime(q.Date) == EntityFunctions.TruncateTime(r.Date) && q.Type == Core.TaskReportType.Dev_Kitchen);

                    if (data != null)
                        secondReport.AddRange(_db.ReportElem.Where(q => q.ReportNagID == data.ID).ToList());
                }

                finalReport.AddRange(secondReport);

                r.Elements = finalReport;
            }

            ViewBag.Print = Print;

            return PartialView(reports);
        }

        public ActionResult ServiceTasksWithCode(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var project = _db.Projects.Find(ID.Value);

            if (project == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Assembly assembly = Assembly.LoadFrom(Server.MapPath("~/bin/ACSystem.Reports.dll"));

            Warning[] warnings;
            string error = "";

            string pdfName = $"Services-{project.InSystemID.Replace('/', '-').Replace('\\', '-')}-{DateTime.Now:yyyy-MM-dd-HH-mm}.pdf";
            string pdfPath = Path.Combine(Server.MapPath("~/dist/RaportPDF"), pdfName);

            bool success = ReportingService.GroupedReport.ServicesWithCode(ID.Value, assembly, pdfPath, out warnings, out error);

            return Json(new { success, error, warnings, pdfName, date = DateTime.Now.ToString("yyyy-MM-dd") }, JsonRequestBehavior.AllowGet);
        }

        private string CreatePDF(int ID, string path)
        {
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            LocalReport report = new LocalReport();

            ReportDataSource reportDataSource = new ReportDataSource();
            ACSystemDataSet dataSet1 = new ACSystemDataSet();
            dataSet1.EnforceConstraints = false;
            dataSet1.BeginInit();

            reportDataSource.Name = "DataSet1";

            reportDataSource.Value = dataSet1.Invoice1;


            report.DataSources.Clear();
            report.DataSources.Add(reportDataSource);

            var readRDLC = System.IO.File.Open(Server.MapPath("~/Reports/Invoice.rdlc"), FileMode.Open);

            report.LoadReportDefinition(readRDLC);

            readRDLC.Dispose();
            readRDLC.Close();

            dataSet1.EndInit();

            Reports.ACSystemDataSetTableAdapters.Invoice1TableAdapter tableAdapter = new Reports.ACSystemDataSetTableAdapters.Invoice1TableAdapter();
            tableAdapter.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            tableAdapter.ClearBeforeFill = true;

            tableAdapter.Fill(dataSet1.Invoice1, ID);

            byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            System.IO.File.WriteAllBytes(path, bytes);

            if (warnings.Count() > 0)
            {
                var msg = "";

                foreach (var warning in warnings)
                {
                    msg += $"{warning.Code} | {warning.Message} | {warning.ObjectName} | {warning.ObjectType} | {warning.Severity}{Environment.NewLine}";
                }

                return msg;
            }

            return String.Empty;
        }
    }
}