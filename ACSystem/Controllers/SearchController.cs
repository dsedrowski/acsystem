﻿using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACSystem.Controllers
{
    public class SearchController : Controller
    {
        private readonly string queryString = $@"SELECT        
                                            p.InSystemID, 
                                            p.ProjectNumber, 
                                            ISNULL(p.Street + ', ' + p.City, N'') AS Adres, 
                                            ISNULL(ISNULL(a.Letter, N'') + a.NumberOfApartment, N'') AS LGH, 
                                            ISNULL(pr.Name + ' ' + ISNULL(t.TaskSufix, N''), N'') AS Task, 
                                            ISNULL(usr.Name + ' ' + usr.Surname, N'') AS Fitter, 
                                            ISNULL(CONVERT(char(10), tf.Timestamp,126), '') AS TimeStamp, 
                                            p.Id AS ProjectID, 
                                            a.Id AS ApartmentID, 
                                            t.Id AS TaskID, 
                                            usr.Id AS FitterID,
                                            inv.ID AS InvoiceID,
											CONVERT(varchar, ISNULL(inv.InvoiceNumber, '')) AS InvoiceNumber,
                                            inv.FortnoxUrl,
											CONVERT(varchar, Payroll.P_id) AS P_id,
											CONVERT(varchar, ISNULL(Payroll.P_no, '')) as P_no
                                    FROM dbo.Projects AS p LEFT OUTER JOIN
                                            dbo.Apartments AS a ON a.ProjectId = p.Id LEFT OUTER JOIN
                                            dbo.Tasks AS t ON t.ApartmentId = a.Id INNER JOIN
                                            dbo.Products AS pr ON t.ProductId = pr.Id LEFT OUTER JOIN
                                            dbo.Task_Fitter AS tf ON tf.TaskId = t.Id AND tf.Status <> 1 LEFT OUTER JOIN
                                            dbo.AspNetUsers AS usr ON usr.Id = tf.FitterId LEFT OUTER JOIN
											dbo.InvoiceElements AS inve ON t.Id = inve.TaskID LEFT OUTER JOIN
											dbo.Invoice AS inv ON inve.InvoiceID = inv.ID LEFT OUTER JOIN
											(SELECT TaskID, 
                        SUBSTRING (( SELECT ';' + CONVERT(varchar, PayrollID) FROM PayrollItems WHERE TaskID = result.TaskID AND Active = 1 GROUP BY PayrollID FOR XML path('')), 2, 9999) AS P_id,
                        SUBSTRING (( SELECT ';' + CONVERT(varchar, pa.Number) FROM PayrollItems pai JOIN Payroll pa ON pai.PayrollID = pa.ID WHERE TaskID = result.TaskID AND pai.Active = 1 GROUP BY pa.Number FOR XML path('')), 2, 9999) AS P_no
                        FROM PayrollItems AS result 
                        WHERE result.TaskID IS NOT NULL AND result.TaskID <> 0
                        Group BY TaskID) AS Payroll ON Payroll.TaskID = t.Id";

        // GET: Search
        public ActionResult Global(string query, string id = "", string projekt = "", string adres = "", string lgh = "", string zadanie = "", string monter = "", string date = "", string faktura = "", string payroll = "", int? page = 1, string orderby = "Project", string dest = "asc")
        {
            var db = new ACSystemContext();

            ViewBag.Query = query;
            ViewBag.Destination = dest;
            ViewBag.OrderBy = orderby;

            if (string.IsNullOrEmpty(query))
                return View(new List<view_GlobalSearch>());

            query = query.ToLower();

            var table = db.Database.SqlQuery<view_GlobalSearch>(queryString).ToList();
            table = table.Where(q =>
                                    (q.InSystemID != null && q.InSystemID.Contains(query)) ||
                                    (q.ProjectNumber != null && q.ProjectNumber.ToLower().Contains(query)) ||
                                    (q.Adres != null && q.Adres.Contains(query)) ||
                                    (q.LGH != null && q.LGH.Contains(query)) ||
                                    (q.Task != null && q.Task.Contains(query)) ||
                                    (q.Fitter != null && q.Fitter.Contains(query)) ||
                                    (q.InvoiceNumber != null && q.InvoiceNumber.Contains(query)) ||
                                    (q.P_no != null && q.P_no.Contains(query))).ToList();

            table = table.Where(q =>
                                    q.InSystemID.ToLower().Contains(id.ToLower()) &&
                                    q.ProjectNumber.ToLower().Contains(projekt.ToLower()) &&
                                    q.Adres.ToLower().Contains(adres.ToLower()) &&
                                    q.LGH.ToLower().Contains(lgh.ToLower()) &&
                                    q.Task.ToLower().Contains(zadanie.ToLower()) &&
                                    q.Fitter.ToLower().Contains(monter.ToLower()) &&
                                    q.TimeStamp.StartsWith(date) &&
                                    q.InvoiceNumber.Contains(faktura) &&
                                    q.P_no.Contains(payroll)).ToList();

            var pageSize = 20;
            var excludeRow = (page - 1) * pageSize;
            var rowsCount = table.Count();
            var pagesCount = (rowsCount % pageSize == 0) ? rowsCount / pageSize : (rowsCount / pageSize) + 1;

            if (page > pagesCount)
            {
                page = 1;
                excludeRow = 0;
            }

            switch (orderby)
            {
                case "ID":
                    table = (dest == "asc") ? table.OrderBy(q => q.InSystemID).ToList() : table.OrderByDescending(q => q.InSystemID).ToList();
                    break;
                case "Project":
                    table = (dest == "asc") ? table.OrderBy(q => q.ProjectNumber).ToList() : table.OrderByDescending(q => q.ProjectNumber).ToList();
                    break;
                case "Address":
                    table = (dest == "asc") ? table.OrderBy(q => q.Adres).ToList() : table.OrderByDescending(q => q.Adres).ToList();
                    break;
                case "LGH":
                    table = (dest == "asc") ? table.OrderBy(q => q.LGH).ToList() : table.OrderByDescending(q => q.LGH).ToList();
                    break;
                case "Task":
                    table = (dest == "asc") ? table.OrderBy(q => q.Task).ToList() : table.OrderByDescending(q => q.Task).ToList();
                    break;
                case "Invoice":
                    table = (dest == "asc") ? table.OrderBy(q => q.InvoiceNumber).ToList() : table.OrderByDescending(q => q.InvoiceNumber).ToList();
                    break;
                case "Payroll":
                    table = (dest == "asc") ? table.OrderBy(q => q.P_no).ToList() : table.OrderByDescending(q => q.P_no).ToList();
                    break;
                case "Fitter":
                    table = (dest == "asc") ? table.OrderBy(q => q.Fitter).ToList() : table.OrderByDescending(q => q.Fitter).ToList();
                    break;
                case "Timestamp":
                    table = (dest == "asc") ? table.OrderBy(q => q.TimeStamp).ToList() : table.OrderByDescending(q => q.TimeStamp).ToList();
                    break;
            }

            table = table.AsQueryable().Skip(excludeRow ?? 0).Take(pageSize).ToList();
            
            ViewBag.Pages = pagesCount;
            ViewBag.CurrentPage = page;
            ViewBag.IDSearch = id;
            ViewBag.ProjektSearch = projekt;
            ViewBag.AdresSearch = adres;
            ViewBag.LGHSearch = lgh;
            ViewBag.ZadanieSearch = zadanie;
            ViewBag.MonterSearch = monter;
            ViewBag.DateSearch = date;
            ViewBag.InvoiceSearch = faktura;
            ViewBag.PayrollSearch = payroll;

            return View(table.ToList());
        }
    }
}