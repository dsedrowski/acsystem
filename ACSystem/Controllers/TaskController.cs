﻿using ACSystem.Core;
using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using ACSystem.Core.Repo;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ACSystem.Controllers
{
    [Authorize]
    public class TaskController : Controller
    {
        public ActionResult Details(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var task = db.Tasks.Find(ID);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var rooms = db.Rooms.ToList();

            var plan = new List<PlannedTaskListForTask>();
            var events = db.Events.Where(q => q.Type == EventType.Plan && q.ReferenceTo == task.Id);

            foreach (var e in events)
            {
                var fitter = db.Fitters.Find(e.ReferenceToUser);

                var p = new PlannedTaskListForTask
                {
                    EventID = e.Id,
                    FitterID = fitter.Id,
                    FitterName = fitter.User.FullName,
                    PlannedDate = e.Start,
                    Description = e.Description
                };

                plan.Add(p);
            }


            ViewBag.Plan = plan;
            ViewBag.Rooms = rooms;

            var _repo = new ProjectsRepo(db);

            var productsList = _repo.ProductsList();

            ViewBag.ProductsList = productsList;
            ViewBag.IsSetProductsList = productsList.Count > 0;
            ViewBag.Projects = _repo.GetContext().Projects.Where(q => q.IsChecked == false).ToList();

            return View(task);
        }

        [HttpPost]
        public ActionResult Details(Tasks form)
        {
            var db = new ACSystemContext();

            var task = db.Tasks.Find(form.Id);

            if (task.Production == TaskProduction.Developer)
                task.ProductId = form.ProductId;

            task.ProductCount = form.ProductCount;
            task.ProductPrice = form.ProductPrice;
            task.Status = form.Status;
            task.StartDate = form.StartDate;
            task.EndDate = form.EndDate;
            task.IsHourlyPay = form.IsHourlyPay;
            task.HourlyPrice = form.HourlyPrice;
            task.PercentType1 = form.PercentType1;
            task.PercentType2 = form.PercentType2;
            task.PercentType3 = form.PercentType3;
            task.PercentType4 = form.PercentType4;
            task.PercentType5 = form.PercentType5;
            task.PercentType6 = form.PercentType6;
            task.Description = form.Description;
            task.InvoiceDescription = form.InvoiceDescription;

            if (!string.IsNullOrEmpty(form.ChangeReason))
                task.ChangeReason = form.ChangeReason;

            if (task.Production != Core.TaskProduction.Developer)
            {
                task.Production = form.Production;
                task.RoomID = form.RoomID;
                task.ErrorCode = form.ErrorCode;
                task.DistancePrice = form.DistancePrice;
            }

            db.Entry(task).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();

                var rooms = db.Rooms.ToList();

                var plan = new List<PlannedTaskListForTask>();
                var events = db.Events.Where(q => q.Type == EventType.Plan && q.ReferenceTo == task.Id);

                foreach (var e in events)
                {
                    var fitter = db.Fitters.Find(e.ReferenceToUser);

                    var p = new PlannedTaskListForTask
                    {
                        EventID = e.Id,
                        FitterID = fitter.Id,
                        FitterName = fitter.User.FullName,
                        PlannedDate = e.Start
                    };

                    plan.Add(p);
                }


                ViewBag.Plan = plan;
                ViewBag.Rooms = rooms;

                var _repo = new ProjectsRepo(db);

                var productsList = _repo.ProductsList();

                ViewBag.ProductsList = productsList;
                ViewBag.IsSetProductsList = productsList.Count > 0;
                ViewBag.Projects = _repo.GetContext().Projects.Where(q => q.IsChecked == false).ToList();

                return View(task);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult IsTaskExist(string query) {
            int taskID = 0;
            if (int.TryParse(query, out taskID)) {
                using (ACSystemContext dtx = new ACSystemContext()) {
                    Tasks task = dtx.Tasks.Find(taskID);

                    if (task != null)
                        return Json(taskID, JsonRequestBehavior.AllowGet);
                    else
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            } else {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        #region OPERATIONS

        public ActionResult Accept(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var task = db.Tasks.Find(ID);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            task.Accepted = true;
            db.Entry(task).State = System.Data.Entity.EntityState.Modified;

            List<Todo> todos = task.Todos.Where(q => q.Checked == false && q.Type == TodoType.Akceptacja).ToList();
            todos.ForEach(x => {
                x.Checked = true;
                db.Entry(x).State = System.Data.Entity.EntityState.Modified;
            });

            try
            {
                db.SaveChanges();

                return RedirectToAction("Details", new { ID = ID.Value });
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult ForceFinish(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var task = db.Tasks.Find(ID);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            task.Status = Core.TaskStatus.Finished;

            db.Entry(task).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();

                return RedirectToAction("Details", new { ID = ID.Value });
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult ForceInvoice(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var task = db.Tasks.Find(ID);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            task.ForceInvoice = true;

            db.Entry(task).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();

                return RedirectToAction("Details", new { ID = ID.Value });
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult DontForceInvoice(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var task = db.Tasks.Find(ID);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            task.ForceInvoice = false;

            db.Entry(task).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();

                return RedirectToAction("Details", new { ID = ID.Value });
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult BlockInvoice(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var task = db.Tasks.Find(ID);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            task.Blocked = true;

            db.Entry(task).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();

                return RedirectToAction("Details", new { ID = ID.Value });
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult UnblockInvoice(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var task = db.Tasks.Find(ID);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            task.Blocked = false;

            db.Entry(task).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();

                return RedirectToAction("Details", new { ID = ID.Value });
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Delete(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var task = db.Tasks.Find(ID);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var apartmentID = task.ApartmentId;

            db.Tasks.Remove(task);

            try
            {
                db.SaveChanges();

                return RedirectToAction("ApartmentDetails", "Projects", new { id = apartmentID });
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult FillTask(int? ID)
        {
            var context = new ACSystemContext();

            var task = context.Tasks.Find(ID);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var workDone = task.Task_Fitter.Where(q => q.IsActive).Sum(q => q.WorkDone);

            task.ProductCount = workDone;
            context.Entry(task).State = System.Data.Entity.EntityState.Modified;

            var left = $"0 z {task.ProductCount} {task.Product.Jm}";

            try
            {
                context.SaveChanges();

                return RedirectToAction("Details", new { ID = ID });
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "FillTask",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                context.ExceptionsLog.Add(log);

                context.SaveChanges();

                return RedirectToAction("Details", new { ID = ID });
            }
        }

        [HttpPost]
        public ActionResult CreateInvoiceAdd(int id, decimal amount)
        {
            using (var db = new ACSystemContext())
            {
                var item = new Invoice_Adds
                {
                    TaskID = id,
                    Amount = amount
                };

                db.Invoice_Adds.Add(item);

                try
                {
                    db.SaveChanges();

                    return Json(new { success = true, amount, id = item.ID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        [HttpPost]
        public ActionResult DeleteInvoiceAdd(int id)
        {
            using (var db = new ACSystemContext())
            {
                var add = db.Invoice_Adds.Find(id);

                if (add == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                db.Invoice_Adds.Remove(add);

                try
                {
                    db.SaveChanges();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        #endregion

        #region QUESTIONS

        [HttpPost]
        public ActionResult SaveAnswerInTask(int Task_QuestionID, int AnswerID, string AdditionalInfo, string Translate)
        {
            using (ACSystemContext db = new ACSystemContext())
            {
                Task_Question question = db.Task_Question.Find(Task_QuestionID);

                question.AnswerID = AnswerID;
                question.AdditionalInformation = AdditionalInfo;
                question.Translation = Translate;

                db.Entry(question).State = System.Data.Entity.EntityState.Modified;

                try
                {
                    db.SaveChanges();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        [HttpGet]
        public ActionResult DeleteQuestion(int ID)
        {
            using (ACSystemContext db = new ACSystemContext())
            {
                Task_Question question = db.Task_Question.Find(ID);

                if (question == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                db.Task_Question.Remove(question);

                try
                {
                    db.SaveChanges();
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        #endregion
    }
}