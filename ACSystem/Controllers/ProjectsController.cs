﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using ACSystem.Core.IRepo;
using ACSystem.ViewModels;
using System.Globalization;
using ACSystem.Core.Service;
using ACSystem.Core;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Drawing.Imaging;
using ACSystem.Core.Filters;
using System.IO;
using System.Text;
using ACSystem.Core.Extensions;
using Microsoft.AspNet.Identity;
using PdfSharp;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using ACSystem.Core.Models.ViewModels.Select;
using System.Xml.Linq;
using TaskStatus = ACSystem.Core.TaskStatus;
using System.Data.Entity.Core.Objects;
using ClosedXML.Excel;
using ACSystem.Core.ViewModels;
using Microsoft.AspNet.Identity.EntityFramework;
using System.IO.Compression;

//using Word = Microsoft.Office.Interop.Word;

namespace ACSystem.Controllers
{
    [AuthLog(Roles = "Admin")]
    public class ProjectsController : Controller
    {
        /// <summary>
        /// Zmienna przechowująca warstwę logiczną aplikacji
        /// </summary>
        private readonly IProjectsRepo _repo;
        private readonly ITasksRepo _taskRepo;

        public ProjectsController(IProjectsRepo repo, ITasksRepo taskRepo)
        {

            _repo = repo;
            _taskRepo = taskRepo;
        }

        #region PROJECTS
        /// <summary>
        /// Funkcja wyświetlająca główną listę klientów
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista projektów", Url = "/Projects", Icon = "fa fa-archive", Active = true });

            return View();
        }

        /// <summary>
        /// Pobiera i wyświetla szczegółowe dane o projekcie
        /// </summary>
        /// <param name="id">ID Klienta</param>
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista projektów", Url = "/Projects", Icon = "fa fa-archive", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Szczegóły projektu", Url = "/Projects/Details", Icon = "", Active = true });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Projects project = await _repo.GetProject((int)id);
            if (project == null)
            {
                return HttpNotFound();
            }

            var productsList = _repo.ProductsList();
            var product = productsList.OrderBy(q => q.Name).FirstOrDefault(q => q.Product_Project.Count(x => x.ProjectID == id) > 0 || q.ForAllProjects);
            var product_project = product.Product_Project.FirstOrDefault(q => q.ProjectID == id);
            var price = product.Price;
            if (product_project != null)
                price = product_project.Price;
            var planProductList = _repo.GetContext().Products.Where(q => q.Product_Project.Any(x => x.ProjectID == id.Value) || q.ForAllProjects).ToList();
            var checkTaskPriceNorm = _repo.GetContext().view_CheckTaskPriceNorm.Where(q => q.ProjectID == id).ToList();
            var checkPayrollRateNorm = _repo.GetContext().view_CheckPayrollRateNorm.Where(q => q.ProjectID == id).ToList();
            var settings = _repo.GetContext().Settings.FirstOrDefault();

            ViewBag.ProductsList = productsList;
            ViewBag.Questions = (product_project != null) ? product_project.Question_Product_Projects.Select(q => q.Question).ToList() : new List<Question>();
            ViewBag.Product = product;
            ViewBag.IsSetProductsList = productsList.Count > 0;
            ViewBag.PlanProductList = planProductList;
            ViewBag.Payroll = _repo.PayrollItemsForProject(id.Value);
            ViewBag.Invoice = _repo.InvoiceElementsForProject(id.Value);
            ViewBag.Fees = _repo.FeesForProject(id.Value);
            ViewBag.Percent = settings?.InvoicePercent ?? 0;
            ViewBag.InvoiceConverted = _repo.GetContext().ProjectInvoicesPercent_Elements.Where(q => q.ProjectID == project.Id && q.ProjectInvoicesPercent.IsActive).ToList().Sum(q => q.Amount);
            ViewBag.CheckTaskPriceNorm = checkTaskPriceNorm;
            ViewBag.CheckPayrollRateNorm = checkPayrollRateNorm;
            ViewBag.ProjectsList = _repo.GetContext().Projects.Where(q => q.IsChecked == false).ToList();

            var projectList = new List<Projects>();

            if (project.ChildProject)
            {
                projectList.Add(project.ParentProject);
                projectList.AddRange(project.ParentProject.ChildProjects.Where(q => q.Id != project.Id));
            }
            else
            {
                projectList.AddRange(project.ChildProjects);
            }

            ViewBag.Projects = projectList;

            var daysRemind = settings?.ProjectEndReminder ?? 0;

            ViewBag.RemindEnd = (DateTime.Now >= project.EndDate.AddDays(daysRemind * -1) && project.Status != ProjectStatus.Finished && project.Status != ProjectStatus.FinishedWithFaults);

            var rooms = _repo.GetContext().Rooms.ToList();
            ViewBag.Rooms = rooms;

            #region PREV NEXT PROJECT

            var allProjects = _repo.GetContext().Projects.OrderBy(q => q.ProjectNumber).ToList();

            var currentIndex = allProjects.FindIndex(q => q.Id == id.Value);

            int? prevProject = null, nextProject = null;

            if (currentIndex > 0)
                prevProject = allProjects[currentIndex - 1].Id;

            if (currentIndex < (allProjects.Count() - 1))
                nextProject = allProjects[currentIndex + 1].Id;

            ViewBag.PrevProject = prevProject;
            ViewBag.NextProject = nextProject;

            #endregion

            ViewBag.CanCheck = Core.Service.ProjectService.CanFinishAndChackProject(id.Value).All(q => q.Value == false);

            #region EVENTS

            var events = _repo.GetContext().Events;
            var retEvents = new List<EventsViewModel>();

            foreach (var e in events)
            {
                if (e.Type == EventType.Project && e.ReferenceTo == id.Value)
                {
                    if (e.ReferenceTo == 221)
                        Console.WriteLine("tu");
                    var eventProject = _repo.GetContext().Projects.Find(e.ReferenceTo);

                    var newElem = new EventsViewModel
                    {
                        ID = e.Id,
                        Date = e.Start,
                        Project = eventProject.ProjectNumber,
                        Title = e.Title,
                        Description = e.Description,
                        Type = "Project"
                    };

                    retEvents.Add(newElem);
                }
                else if (e.Type == EventType.Task)
                {
                    var task = _repo.GetContext().Tasks.Find(e.ReferenceTo);

                    if (task == null || task.Apartment.ProjectId != id.Value)
                        continue;

                    var newElem = new EventsViewModel
                    {
                        ID = e.Id,
                        Date = e.Start,
                        Project = task.Apartment.Project.ProjectNumber,
                        LGH = task.Apartment.Letter + task.Apartment.NumberOfApartment,
                        Task = task.GetTaskName(task.Apartment.ProjectId) + " " + task.TaskSufix,
                        Title = e.Title,
                        Description = e.Description,
                        Type = "Task"
                    };

                    retEvents.Add(newElem);
                }
            }

            ViewBag.Events = retEvents;

            #endregion

            return View(project);
        }

        public ActionResult GetPrevNextID(int current, string dest, string orderby, string search, string filter)
        {
            var db = new ACSystemContext();

            var showDisabled = filter.Contains("0");
            var filterList = filter.ToIntArray(',');

            var query = db.Projects.Where(p =>
                            p.ProjectNumber.Contains(search) ||
                            p.Street.Contains(search) ||
                            p.City.Contains(search) ||
                            p.Customer.Name.Contains(search) ||
                            p.InSystemID.Contains(search)
                            );

            if (filterList.Contains(4))
                query = query.Where(p => filterList.Contains((int)p.Status));
            else
                query = query.Where(p => filterList.Contains((int)p.Status) && p.IsChecked == false);

            switch (orderby)
            {
                case "projectNumber":
                    if (dest == "asc")
                        query = query.OrderBy(p => p.ProjectNumber);
                    else
                        query = query.OrderByDescending(p => p.ProjectNumber);
                    break;
                case "street":
                    if (dest == "asc")
                        query = query.OrderBy(p => p.Street);
                    else
                        query = query.OrderByDescending(p => p.Street);
                    break;
                case "city":
                    if (dest == "asc")
                        query = query.OrderBy(p => p.City);
                    else
                        query = query.OrderByDescending(p => p.City);
                    break;
                case "status":
                    if (dest == "asc")
                        query = query.OrderBy(p => p.Status).ThenBy(q => q.EndDate);
                    else
                        query = query.OrderByDescending(p => p.Status).ThenBy(q => q.EndDate);
                    break;
                case "customer":
                    if (dest == "asc")
                        query = query.OrderBy(p => p.Customer);
                    else
                        query = query.OrderByDescending(p => p.Customer);
                    break;
                case "inSystemID":
                    if (dest == "asc")
                        query = query.OrderBy(p => p.InSystemID);
                    else
                        query = query.OrderByDescending(p => p.InSystemID);
                    break;
                default:
                    if (dest == "asc")
                        query = query.OrderBy(p => p.ProjectNumber);
                    else
                        query = query.OrderByDescending(p => p.ProjectNumber);
                    break;
            }

            var list = query.ToList();

            var currentIndex = list.FindIndex(q => q.Id == current);

            int? prev = null, next = null;

            if (currentIndex > 0)
                prev = list[currentIndex - 1].Id;

            if (currentIndex < list.Count - 1)
                next = list[currentIndex + 1].Id;

            return Json(new { prev = prev, next = next }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Wyświetla formularz tworzenia nowego projektu
        /// </summary>
        public ActionResult Create(int? customer_id)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista projektów", Url = "/Projects", Icon = "fa fa-archive", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Dodaj projekt", Url = "/Projects/Create", Icon = "", Active = true });

            var customers = _repo.GetCustomers();

            ViewBag.Customers = new SelectList(customers, "Id", "Name");
            //ViewBag.ProjectManagerId = projectManagersId;

            var statusList = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem { Text = "Nie rozpoczęte", Value = ((int)ProjectStatus.NotStarted).ToString() },
                    new SelectListItem { Text = "W trakcie", Value = ((int)ProjectStatus.UnderWork).ToString() },
                    new SelectListItem { Text = "Zakończone", Value = ((int)ProjectStatus.Finished).ToString() },
                    new SelectListItem { Text = "Zakończone z błędami", Value = ((int)ProjectStatus.FinishedWithFaults).ToString() }
                },
                "Value", "Text"
            );


            var calendars = _repo.GetContext().Calendar.ToList();
            var fitters = _repo.GetContext().ACSystemUser.Where(q => q.LockoutEnabled == false).ToList();
            ViewBag.Users = fitters;

            ViewBag.StatusList = statusList;
            ViewBag.ProjectManagersList = _repo.GetProjectManagersByCustomerId((customers.Count() > 0) ? Convert.ToInt32(customers.FirstOrDefault().Id) : 0);
            ViewBag.Calendars = calendars;
            ViewBag.ParentProjects = _repo.GetContext().Projects.Where(q => q.ChildProject == false).ToList();

            if (customer_id != null)
            {
                var project = new Projects
                {
                    CustomerId = (int)customer_id
                };

                return View(project);
            }

            var newProject = new Projects
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now
            };

            return View(newProject);
        }

        /// <summary>
        /// Przetwarza wysłane metodą POST dane, w celu utworzenia projektu
        /// </summary>
        /// <param name="projects">Model z danymi projektu</param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "Id,CustomerId,ProjectManagerId,NumberOfApartment,ProjectNumber,CustomerProjectNumber,Street,City,Status,ProjectManagers,StartDate,EndDate,Description,CalendarID,ShowInCalendar,InSystemID,TakePercent,IsChecked,IsGroupedInvoice,ChildProject,ParentProjectID,InvoiceDescription,FirstDoService,SupervisorID,CompanyPercent,FitterPercent")] Projects projects)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista projektów", Url = "/Projects", Icon = "fa fa-archive", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Dodaj projekt", Url = "/Projects/Create", Icon = "", Active = true });

            ViewBag.Customers = new SelectList(_repo.GetCustomers(), "Id", "Name");
            ViewBag.ProjectManagerId = new SelectList(_repo.GetProjectManagers(), "Id", "Name");

            var statusList = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem { Text = "Nie rozpoczęte", Value = ((int)ProjectStatus.NotStarted).ToString() },
                    new SelectListItem { Text = "W trakcie", Value = ((int)ProjectStatus.UnderWork).ToString() },
                    new SelectListItem { Text = "Zakończone", Value = ((int)ProjectStatus.Finished).ToString() },
                    new SelectListItem { Text = "Zakończone z błędami", Value = ((int)ProjectStatus.FinishedWithFaults).ToString() }
                },
                "Value", "Text"
            );

            ViewBag.StatusList = statusList;

            var calendars = _repo.GetContext().Calendar.ToList();
            ViewBag.Calendars = calendars;
            var fitters = _repo.GetContext().ACSystemUser.Where(q => q.LockoutEnabled == false).ToList();
            ViewBag.Users = fitters;

            if (ModelState.IsValid)
            {
                int? projectId = null;
                string errorCode = "";

                try
                {
                    projectId = _repo.Create(projects);

                    if (projectId == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                    FortnoxService.ProjectManager manage = new FortnoxService.ProjectManager();
                    var added = manage.Create(new FortnoxAPILibrary.Project
                    {
                        ProjectNumber = projects.InSystemID,

                        Description = projects.ProjectNumber,
                        StartDate = projects.StartDate.ToString("yyyy-MM-dd"),
                        EndDate = projects.EndDate.ToString("yyyy-MM-dd"),
                        Status = FortnoxAPILibrary.Connectors.ProjectConnector.Status.ONGOING
                    }, out errorCode);

                    projects.InFortnox = true;
                    _repo.Edit(projects, User.Identity.GetUserId());
                    _repo.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var validationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            ModelState.AddModelError((validationError.PropertyName == null) ? "ProjectNumber" : validationError.PropertyName,
                                                     (validationError.PropertyName == null) ? $"Podany numer projektu '{projects.ProjectNumber}' już istnieje!" : validationError.ErrorMessage);
                        }
                    }

                    ViewBag.ProjectManagersList = _repo.GetProjectManagersByCustomerId(projects.CustomerId);
                    return View(projects);
                }
                catch (Exception ex)
                {
                    var db = _repo.GetContext();

                    var log = new ExceptionsLog
                    {
                        Message = ex.Message,
                        Type = ex.GetType().ToString(),
                        Source = ex.Source,
                        StackTrace = ex.StackTrace,
                        URL = "CreateProject",
                        LogDate = DateTime.Now,
                        User = User.Identity.Name
                    };

                    db.ExceptionsLog.Add(log);

                    db.SaveChanges();
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                return Redirect("/Projects/Details/" + projectId);
            }

            ViewBag.ParentProjects = _repo.GetContext().Projects.Where(q => q.ChildProject == false).ToList();
            ViewBag.ProjectManagersList = _repo.GetProjectManagersByCustomerId(projects.CustomerId);

            return View(projects);
        }

        /// <summary>
        /// Wyświetla formularz edycji projektu
        /// </summary>
        /// <param name="id">ID edytowanego projektu</param>
        /// <returns>Model z danymi projektu</returns>
        public async Task<ActionResult> Edit(int? id)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista projektów", Url = "/Projects", Icon = "fa fa-archive", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Edycja projektu", Url = "/Projects", Icon = "", Active = true });

            ViewBag.Customers = new SelectList(_repo.GetCustomers(), "Id", "Name");
            ViewBag.ProjectManagerId = new SelectList(_repo.GetProjectManagers(), "Id", "Name");

            var statusList = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem { Text = "Nie rozpoczęte", Value = ((int)ProjectStatus.NotStarted).ToString() },
                    new SelectListItem { Text = "W trakcie", Value = ((int)ProjectStatus.UnderWork).ToString() },
                    new SelectListItem { Text = "Zakończone", Value = ((int)ProjectStatus.Finished).ToString() },
                    new SelectListItem { Text = "Zakończone z błędami", Value = ((int)ProjectStatus.FinishedWithFaults).ToString() }
                },
                "Value", "Text"
            );

            ViewBag.StatusList = statusList;

            var calendars = _repo.GetContext().Calendar.ToList();
            ViewBag.Calendars = calendars;
            var fitters = _repo.GetContext().ACSystemUser.Where(q => q.LockoutEnabled == false).ToList();
            ViewBag.Users = fitters;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Projects project = await _repo.GetProject((int)id);

            if (project == null)
            {
                return HttpNotFound();
            }

            ViewBag.ProjectManagersList = _repo.GetProjectManagersByCustomerId(project.CustomerId);
            ViewBag.IsAnyError = await _repo.IsAnyError(project.Id);
            ViewBag.ParentProjects = _repo.GetContext().Projects.Where(q => q.ChildProject == false && q.Id != id.Value).ToList();
            ViewBag.CanFinishProjectDict = Core.Service.ProjectService.CanFinishAndChackProject(id.Value);

            return View(project);
        }

        /// <summary>
        /// Przetwarza wysłane dane metodą POST, w celu edycji danych projektu
        /// </summary>
        /// <param name="projects">Model z danymi projektu</param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id,CustomerId,ProjectManagerId,NumberOfApartment,ProjectNumber,Street,City,Status,CreateDate,ProjectManagers,StartDate,EndDate,Description,CalendarID,ShowInCalendar,InSystemID,TakePercent,IsChecked,IsGroupedInvoice,ChildProject,ParentProjectID,InvoiceDescription,FirstDoService,InFortnox,SupervisorID,CompanyPercent,FitterPercent,CustomerProjectNumber")] Projects projects)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista projektów", Url = "/Projects", Icon = "fa fa-archive", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Edycja projektu", Url = "/Projects", Icon = "", Active = true });

            ViewBag.Customers = new SelectList(_repo.GetCustomers(), "Id", "Name");
            ViewBag.ProjectManagerId = new SelectList(_repo.GetProjectManagers(), "Id", "Name");

            var statusList = new SelectList(new List<SelectListItem>
                {
                    new SelectListItem { Text = "Nie rozpoczęte", Value = ((int)ProjectStatus.NotStarted).ToString() },
                    new SelectListItem { Text = "W trakcie", Value = ((int)ProjectStatus.UnderWork).ToString() },
                    new SelectListItem { Text = "Zakończone", Value = ((int)ProjectStatus.Finished).ToString() },
                    new SelectListItem { Text = "Zakończone z błędami", Value = ((int)ProjectStatus.FinishedWithFaults).ToString() }
                },
                "Value", "Text"
            );

            ViewBag.StatusList = statusList;

            var calendars = _repo.GetContext().Calendar.ToList();
            ViewBag.Calendars = calendars;
            var fitters = _repo.GetContext().ACSystemUser.Where(q => q.LockoutEnabled == false).ToList();
            ViewBag.Users = fitters;

            if (ModelState.IsValid)
            {
                _repo.Edit(projects, User.Identity.GetUserId());

                try
                {
                    _repo.SaveChanges();

                    var project = _repo.GetProject(projects.Id).GetAwaiter().GetResult();
                    var manage = new FortnoxService.ProjectManager();

                    if (!project.InFortnox.HasValue)
                        project.InFortnox = false;

                    var status = FortnoxAPILibrary.Connectors.ProjectConnector.Status.ONGOING;

                    switch (project.Status)
                    {
                        case Core.ProjectStatus.NotStarted:
                            status = FortnoxAPILibrary.Connectors.ProjectConnector.Status.ONGOING;
                            break;
                        case Core.ProjectStatus.UnderWork:
                            status = FortnoxAPILibrary.Connectors.ProjectConnector.Status.ONGOING;
                            break;
                        case Core.ProjectStatus.Finished:
                            status = FortnoxAPILibrary.Connectors.ProjectConnector.Status.COMPLETED;
                            break;
                        case Core.ProjectStatus.FinishedWithFaults:
                            status = FortnoxAPILibrary.Connectors.ProjectConnector.Status.COMPLETED;
                            break;
                    }

                    if (project.InFortnox.HasValue && project.InFortnox.Value)
                    {
                        var fortnox_project = manage.Get(project.InSystemID);

                        if (fortnox_project != null)
                        {
                            fortnox_project.Description = project.ProjectNumber;
                            fortnox_project.StartDate = project.StartDate.ToString("yyyy-MM-dd");
                            fortnox_project.EndDate = project.EndDate.ToString("yyyy-MM-dd");
                            fortnox_project.ProjectLeader = (project.ProjectManagers.Any()) ? project.Project_ProjectManager.FirstOrDefault()?.ProjectManager?.Name : "";
                            fortnox_project.Status = status;

                            manage.Update(fortnox_project);
                        }
                    }
                    else
                    {
                        var pm = "";
                        if (project.ProjectManagers.Any())
                        {
                            var pmObj = _repo.GetContext().ProjectManagers.FirstOrDefault(q => q.Id == project.ProjectManagers.FirstOrDefault());

                            if (pmObj != null)
                                pm = pmObj.Name;
                        }

                        var data = new FortnoxAPILibrary.Project
                        {
                            ProjectNumber = project.InSystemID,
                            Description = project.ProjectNumber,
                            StartDate = project.StartDate.ToString("yyyy-MM-dd"),
                            EndDate = project.EndDate.ToString("yyyy-MM-dd"),
                            ProjectLeader = pm,
                            Status = status
                        };
                        string errorCode = "";
                        project.InFortnox = manage.Create(data, out errorCode) != null;

                        _repo.Edit(project, User.Identity.GetUserId());
                        _repo.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "", "");
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                return Redirect("/Projects/Details/" + projects.Id);
            }

            ViewBag.ProjectManagersList = _repo.GetProjectManagersByCustomerId(projects.CustomerId);
            ViewBag.ParentProjects = _repo.GetContext().Projects.Where(q => q.ChildProject == false && q.Id != projects.Id).ToList();
            ViewBag.CanFinishProjectDict = Core.Service.ProjectService.CanFinishAndChackProject(projects.Id);

            return View(projects);
        }

        /// <summary>
        /// Usuwa projekt
        /// </summary>
        /// <param name="id">ID Projektu</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = "";
            var project = _repo.GetContext().Projects.Find(id);

            for (int i = 0; i < 3; i++)
            {
                result = await _repo.Delete((int)id);

                if (result.Split(';')[0] == "true")
                {
                    if (!string.IsNullOrEmpty(project.InSystemID))
                    {
                        var manage = new FortnoxService.ProjectManager();
                        manage.Delete(project.InSystemID);
                    }

                    break;
                }
            }

            var rsSplit = result.Split(';');

            if (rsSplit.Length > 1)
                return Json(new { success = rsSplit[0], message = rsSplit[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Nieoczekiwany błąd." }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region APARTMENTS
        public ActionResult ApartmentDetails(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var apartment = _repo.ApartmentById((int)id);

            var productsList = _repo.ProductsList();

            ViewBag.ProductsList = productsList;

            var product = _repo.ProductsList().FirstOrDefault();

            ViewBag.Product = product;

            ViewBag.IsSetProductsList = productsList.Count > 0;
            ViewBag.Payroll = _repo.PayrollItemsForApartment(id.Value);
            ViewBag.Invoices = _repo.InvoiceElementsForApartment(id.Value);
            ViewBag.Fees = _repo.FeesForApartment(id.Value) ?? new List<Fees>();
            ViewBag.Projects = _repo.GetContext().Projects.Where(q => q.IsChecked == false).ToList();

            var db = _repo.GetContext();

            var allApartments = db.Apartments.Where(q => q.ProjectId == apartment.ProjectId).OrderBy(q => q.Cage).ThenBy(q => q.Floor).ThenBy(q => q.NumberOfApartment.Length).ThenBy(q => q.Letter).ThenBy(q => q.NumberOfApartment).ToList();

            var currentIndex = allApartments.FindIndex(q => q.Id == apartment.Id);

            int? prevApartment = null, nextApartment = null;

            if (currentIndex > 0)
                prevApartment = allApartments[currentIndex - 1].Id;

            if (currentIndex < (allApartments.Count() - 1))
                nextApartment = allApartments[currentIndex + 1].Id;

            ViewBag.PrevApartment = prevApartment;
            ViewBag.NextApartment = nextApartment;

            ViewBag.Percent = (decimal)(db.Settings.FirstOrDefault()?.InvoicePercent ?? 0);
            ViewBag.InvoiceConverted = _repo.GetContext().ProjectInvoicesPercent_Elements.Where(q => q.ProjectID == apartment.ProjectId && q.ProjectInvoicesPercent.IsActive).ToList().Sum(q => q.Amount);

            return View(apartment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApartmentCreate(int ProjectId, string NumberOfApartment, string Letter, int Floor, string Cage)
        {
            var apartment = new Apartments
            {
                ProjectId = ProjectId,
                NumberOfApartment = NumberOfApartment,
                Letter = Letter,
                Floor = Floor,
                Cage = Cage
            };

            _repo.ApartmentCreate(apartment);

            return Redirect("/Projects/Details/" + ProjectId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProjectCopy(int Id, string numbers, bool? copyTask)
        {
            var context = _repo.GetContext();
            var project = context.Projects.Find(Id);

            var numbersArray = numbers.Split(',');

            foreach (var number in numbersArray)
            {
                var n = number.Split('#');
                var newProject = new Projects
                {
                    CustomerId = project.CustomerId,
                    InSystemID = n[0],
                    ProjectNumber =(n.Length > 1) ? n[1] : "",
                    Street = project.Street,
                    City = project.City,
                    Status = ProjectStatus.NotStarted,
                    StartDate = project.StartDate,
                    EndDate = project.EndDate,
                    CalendarID = project.CalendarID,
                    CreateDate = DateTime.Now,
                    Apartments = new List<Apartments>(),
                    Description = project.Description
                };


                foreach (var a in project.Apartments)
                {
                    var newApartment = new Apartments()
                    {
                        NumberOfApartment = a.NumberOfApartment,
                        Letter = a.Letter,
                        //ProjectId = newProject.Id
                        Tasks = new List<Tasks>(),
                        Cage = a.Cage,
                        Floor = a.Floor
                    };

                    newProject.Apartments.Add(newApartment);
                    //context.Apartments.Add(newApartment);

                    if (!copyTask.HasValue || copyTask.Value != true) continue;

                    foreach (var t in a.Tasks)
                    {
                        if (t.Production != TaskProduction.Developer)
                            continue;

                        int[] questions = t.Task_Question.Select(q => q.QuestionID).ToArray();

                        var newTask = new Tasks
                        {
                            ProductId = t.ProductId,
                            ProductPrice = t.ProductPrice,
                            ProductCount = t.ProductCount,
                            Status = Core.TaskStatus.Free,
                            PercentType1 = t.PercentType1,
                            PercentType2 = t.PercentType2,
                            PercentType3 = t.PercentType3,
                            PercentType4 = t.PercentType4,
                            PercentType5 = t.PercentType5,
                            PercentType6 = t.PercentType6,
                            StartDate = t.StartDate,
                            EndDate = t.EndDate,
                            IsHourlyPay = t.IsHourlyPay,
                            HourlyPrice = t.HourlyPrice,
                            Accepted = t.Accepted,
                            Description = t.Description,
                            Task_Question = new List<Task_Question>()
                        };

                        if (newTask.Production == TaskProduction.Developer && questions != null && questions.Length > 0)
                        {
                            foreach (int q in questions)
                            {
                                newTask.Task_Question.Add(new Task_Question
                                {
                                    QuestionID = q
                                });
                            }
                        }

                        newApartment.Tasks.Add(newTask);
                    }
                }

                foreach (var p in project.Product_Project)
                {
                    newProject.Product_Project.Add(new Product_Project
                    {
                        ProductID = p.ProductID,
                        ProjectNumber = (n.Length > 1) ? n[1] : "",
                        ProductName = p.ProductName,
                        Price = p.Price,
                        PercentType1 = p.PercentType1,
                        PercentType2 = p.PercentType2,
                        OnInvoiceName = p.OnInvoiceName,
                        Question_Product_Projects = p.Question_Product_Projects.Select(q => new Question_Product_Project
                        {
                            QuestionID = q.QuestionID,
                            Project_ProductID = q.Project_ProductID
                        }).ToList()
                    });
                }

                context.Projects.Add(newProject);
            }

            try
            {
                context.SaveChanges();

                return Redirect("/Projects");
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "SavePayroll",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                context.ExceptionsLog.Add(log);

                context.SaveChanges();

                return Redirect("/Projects");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApartmentCopy(int ProjectId, int ApartmentIdToCopy, string numbers, bool? CopyTask)
        {
            Apartments apartment = _repo.ApartmentById(ApartmentIdToCopy);
            string[] numbersArray = numbers.Split(',');

            foreach (var number in numbersArray)
            {
                Apartments newApartment = new Apartments()
                {
                    NumberOfApartment = number,
                    ProjectId = apartment.ProjectId,
                    Description = apartment.Description,
                    Cage = "",
                    Floor = 0,
                    Letter = ""
                };

                int newId = _repo.ApartmentCreate(newApartment);

                if (newId > 0 && CopyTask.HasValue && CopyTask.Value == true)
                {
                    var tasks = _repo.TaskListByApartment(ApartmentIdToCopy);

                    foreach (var task in tasks)
                    {
                        int[] questions = task.Task_Question.Select(q => q.QuestionID).ToArray();

                        Tasks newTask = new Tasks
                        {
                            ApartmentId = newId,
                            ProductId = task.ProductId,
                            ProductPrice = task.ProductPrice,
                            ProductCount = task.ProductCount,
                            Status = Core.TaskStatus.Free,
                            PercentType1 = task.PercentType1,
                            PercentType2 = task.PercentType2,
                            PercentType3 = task.PercentType3,
                            PercentType4 = task.PercentType4,
                            PercentType5 = task.PercentType5,
                            PercentType6 = task.PercentType6,
                            StartDate = task.StartDate,
                            EndDate = task.EndDate,
                            TaskSufix = task.TaskSufix,
                            DistancePrice = task.DistancePrice,
                            IsHourlyPay = task.IsHourlyPay,
                            HourlyPrice = task.HourlyPrice,
                            Accepted = task.Accepted,
                            Description = task.Description,
                            ForceInvoice = task.ForceInvoice,
                            Production = task.Production
                        };

                        string taskId = _repo.TaskCreate(newTask, FitterID: User.Identity.GetUserId(), questions: questions);
                    }
                }
            }

            return Redirect("/Projects/Details/" + ProjectId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApartmentEdit(Apartments Apartment)
        {
            _repo.ApartmentEdit(Apartment);

            return Redirect("/Projects/Details/" + Apartment.ProjectId);
        }

        [HttpGet]
        public ActionResult ApartmentDelete(int ProjectId, int ApartmentId)
        {
            _repo.ApartmentDelete(ApartmentId);

            return Json(new { success = "true" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MoveApartments(int CurrentProjectID, int MoveProjectID)
        {
            var db = _repo.GetContext();

            var apartments = db.Apartments.Where(q => q.ProjectId == CurrentProjectID);
            var moveProjectApartments = db.Apartments.Where(q => q.ProjectId == MoveProjectID);

            foreach (var a in apartments)
            {
                a.ProjectId = MoveProjectID;

                if (moveProjectApartments.Any(q => q.NumberOfApartment == a.NumberOfApartment))
                    a.NumberOfApartment += " #v2";

                db.Entry(a).State = EntityState.Modified;
            }

            try
            {
                db.SaveChanges();

                return Redirect("/Projects/Details/" + MoveProjectID);
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "MoveApartments",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                return Redirect("/Projects/Details/" + CurrentProjectID);
            }
        }

        [HttpPost]
        public ActionResult MoveOneTask(string TasksArray, int MoveApartmentID)
        {
            string[] tasksArray = TasksArray.Split(',');
            List<int> tasksIDList = new List<int>();

            tasksArray.ForEach(x =>
            {
                int value = 0;
                if (int.TryParse(x, out value))
                    tasksIDList.Add(value);
            });

            if (tasksIDList.Count > 1 || tasksIDList.Count == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Tasks task = _repo.GetContext().Tasks.Find(tasksIDList.FirstOrDefault());

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            task.ApartmentId = MoveApartmentID;
            _repo.GetContext().Entry(task).State = EntityState.Modified;

            try
            {
                _repo.GetContext().SaveChanges();
                return Redirect(Request.UrlReferrer.AbsoluteUri);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "MoveOneTask", User.Identity.Name);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult MoveMultipleTasks(string TasksArray, int MoveApartmentID)
        {
            string[] tasksArray = TasksArray.Split(',');
            List<int> tasksIDList = new List<int>();

            tasksArray.ForEach(x =>
            {
                int value = 0;
                if (int.TryParse(x, out value))
                    tasksIDList.Add(value);
            });

            foreach (var id in tasksIDList)
            {
                Tasks task = _repo.GetContext().Tasks.Find(id);

                if (task == null)
                    continue;

                task.ApartmentId = MoveApartmentID;
                _repo.GetContext().Entry(task).State = EntityState.Modified;
            }

            try
            {
                _repo.GetContext().SaveChanges();
                return Redirect(Request.UrlReferrer.AbsoluteUri);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "MoveMultipleTasks", User.Identity.Name);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult ChangeFloor(string ApartmentsArray, int Floor, int ProjectID)
        {
            var apArr = ApartmentsArray.Split(',');

            var apIDList = new List<int>();

            foreach (var ap in apArr)
            {
                var value = 0;

                if (int.TryParse(ap, out value))
                    apIDList.Add(value);
            }

            var db = new ACSystemContext();

            var apartments = db.Apartments.Where(q => apIDList.Contains(q.Id)).ToList();

            foreach (var a in apartments)
            {
                a.Floor = Floor;

                db.Entry(a).State = EntityState.Modified;
            }

            try
            {
                db.SaveChanges();
                return Redirect("/Projects/Details/" + ProjectID);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "ChangeFloor", User.Identity.Name);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult ChangeCage(string ApartmentsArray, string Cage, int ProjectID)
        {
            var apIDList = ApartmentsArray.ToIntArray(',');

            var db = new ACSystemContext();

            var apartments = db.Apartments.Where(q => apIDList.Contains(q.Id)).ToList();

            foreach (var a in apartments)
            {
                a.Cage = Cage;

                db.Entry(a).State = EntityState.Modified;
            }

            try
            {
                db.SaveChanges();
                return Redirect("/Projects/Details/" + ProjectID);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "ChangeCage", User.Identity.Name);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region TASKS
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TaskCreate(Tasks task, int[] questions)
        {
            if (!string.IsNullOrEmpty(task.ApartmentsArray))
            {
                task.Distance = 0;
                task.DistancePrice = 0;
                foreach (var a in task.ApartmentsArray.Split(','))
                {
                    var id = 0;

                    if (int.TryParse(a, out id))
                    {
                        task.ApartmentId = id;

                        var res = _repo.TaskCreate(task, FitterID: User.Identity.GetUserId(), questions: questions);
                    }
                }

                return Redirect($"/Projects/Details/{task.ProjectID}");
            }
            else if (task.ApartmentId != 0)
            {
                task.Distance = 0;
                task.DistancePrice = 0;
                var result = _repo.TaskCreate(task, FitterID: User.Identity.GetUserId(), questions: questions);

                if (result.Split(':')[0] == "success")
                {
                    return Redirect($"/Projects/ApartmentDetails/{task.ApartmentId}");
                }
                else
                {
                    ViewBag.Error = result.Split(':')[1];
                    return View();
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        public async Task<ActionResult> TaskDelete(int id)
        {
            var result = await _repo.TaskDelete(id);

            return Json(new { success = result.Split(';')[0], message = result.Split(';')[1] }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteTaskGroup(int[] tasks)
        {
            var db = _repo.GetContext();

            var tasksList = db.Tasks.Where(q => tasks.Contains(q.Id)).ToList();

            if (!tasksList.Any()) return Json(new {success = false}, JsonRequestBehavior.AllowGet);

            db.Tasks.RemoveRange(tasksList);

            db.SaveChanges();

            return Json(new {success = true}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteApartmentGroup(int[] apartments)
        {
            var db = _repo.GetContext();

            var apartmentsList = db.Apartments.Where(q => apartments.Contains(q.Id)).ToList();

            if (!apartmentsList.Any()) return Json(new { success = false }, JsonRequestBehavior.AllowGet);

            db.Apartments.RemoveRange(apartmentsList);

            db.SaveChanges();

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TaskEdit(Tasks task, int[] questions, string[] answers)
        {
            _repo.TaskEdit(task: task, userID: User.Identity.GetUserId(), questions: questions, answers: answers);
            return Redirect("/Projects/ApartmentDetails/" + task.ApartmentId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TaskEditGroup(Tasks task)
        {
            var db = _repo.GetContext();

            var form = Request.Form;

            var changePrice = (form["change-price"] != null && form["change-price"] == "True");
            var changeQnty = (form["change-qnty"] != null && form["change-qnty"] == "True");
            var changeDateStart = (form["change-date-start"] != null && form["change-date-start"] == "True");
            var changeDateEnd = (form["change-date-end"] != null && form["change-date-end"] == "True");
            var changeDescription = (form["change-description"] != null && form["change-description"] == "True");
            var changePercents = (form["change-percents"] != null && form["change-percents"] == "True");

            if (!string.IsNullOrEmpty(task.TasksArray))
            {
                foreach (var t in task.TasksArray.Split(','))
                {
                    var id = 0;

                    if (!int.TryParse(t, out id)) continue;

                    var getTask = db.Tasks.Find(id);

                    if (getTask == null) continue;

                    if (task.Status != TaskStatus.DoNotChange) getTask.Status = task.Status;
                    if (changePrice) getTask.ProductPrice = task.ProductPrice;
                    if (changeQnty) getTask.ProductCount = task.ProductCount;
                    if (changeDateStart) getTask.StartDate = task.StartDate;
                    if (changeDateEnd) getTask.EndDate = task.EndDate;
                    if (changeDescription) getTask.Description = task.Description;
                    if (changePercents)
                    {
                        getTask.PercentType1 = task.PercentType1;
                        getTask.PercentType2 = task.PercentType2;
                        getTask.PercentType3 = task.PercentType3;
                        getTask.PercentType4 = task.PercentType4;
                        getTask.PercentType5 = task.PercentType5;
                        getTask.PercentType6 = task.PercentType6;
                    }

                    db.Entry(getTask).State = EntityState.Modified;
                }

                db.SaveChanges();

                return Redirect($"/Projects/Details/{task.ProjectID}");
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TaskSerwisCreate(Tasks task, string[] questionDescription, string[] questionSelect, string[] questionCode, string[] questionRoom, string BackTo = "apartment")
        {
            if (!string.IsNullOrEmpty(task.ApartmentsArray))
            {
                task.Distance = 0;
                var productType = task.Product.ProductType;

                task.Product = null;

                foreach (var a in task.ApartmentsArray.Split(','))
                {
                    var id = 0;

                    if (int.TryParse(a, out id))
                    {
                        task.ApartmentId = id;
                        var res = _repo.TaskCreate(task, FitterID: User.Identity.GetUserId());
                    }
                }

                return Redirect($"/Projects/Details/{task.ProjectID}");

            }
            else if (task.ApartmentId != 0)
            {
                task.Distance = 0;
                var productType = task.Product.ProductType;

                task.Product = null;

                var patches = new List<TaskPatches>();

                var descriptions = questionDescription;
                var questions = questionSelect;

                if (descriptions != null && questions != null && questionCode != null && questionRoom != null)
                {
                    var descArray = descriptions;
                    var questionsArray = questions;

                    for (var i = 0; i < questionsArray.Length; i++)
                    {
                        var patch = new TaskPatches
                        {
                            TaskID = task.Id,
                            ItemTitle = questionsArray[i],
                            ItemDescription = descArray[i],
                            Code = questionCode[i],
                            Room = questionRoom[i],
                            PhotoBlocked = false
                        };

                        patches.Add(patch);
                    }
                }

                _repo.TaskPatchesCreate(patches);

                task.ProductCount = (patches.Count() > 0) ? patches.Count() : 1;

                var result = _repo.TaskCreate(task, FitterID: User.Identity.GetUserId());

                if (BackTo == "apartment")
                    return Redirect($"/Projects/ApartmentDetails/{task.ApartmentId}");
                else
                    return Redirect($"/Task/Details/{task.Id}");
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TaskSerwisEdit(Tasks task, string[] questionDescription, string[] questionSelect, string[] questionCode, string[] questionRoom)
        {
            var patches = new List<TaskPatches>();

            var req = Request.Form;

            foreach (var key in req.Keys)
            {
                if (key.ToString().Contains("desc") && key.ToString().Contains("description") == false)
                {
                    var patchID = int.Parse(key.ToString().Split('-')[0]);

                    var patch = _repo.GetContext().TaskPatches.Find(patchID);

                    patch.ItemDescription = req[patchID + "-desc"];
                    patch.Code = req[patchID + "-code"];
                    patch.Room = req[patchID + "-room"];

                    if (req[patchID + "-check"] != null)
                        patch.Done = true;
                    else
                        patch.Done = false;

                    _repo.GetContext().Entry(patch).State = EntityState.Modified;

                    _repo.SaveChanges();
                }
            }

            var descriptions = questionDescription;
            var questions = questionSelect;

            if (descriptions != null && questions != null)
            {
                var descArray = descriptions;
                var questionsArray = questions;

                for (var i = 0; i < questionsArray.Length; i++)
                {
                    var patch = new TaskPatches
                    {
                        TaskID = task.Id,
                        ItemTitle = questionsArray[i],
                        ItemDescription = descArray[i],
                        Code = questionCode[i],
                        Room = questionRoom[i],
                        PhotoBlocked = false
                    };

                    patches.Add(patch);
                }
            }

            _repo.TaskPatchesCreate(patches);

            var patchesCount = _repo.GetContext().TaskPatches.Count(q => q.TaskID == task.Id);

            task.ProductCount = (patchesCount > 0) ? patchesCount : 1;

            var result = _repo.TaskEdit(task, User.Identity.GetUserId());

            return Redirect($"/Projects/ApartmentDetails/{task.ApartmentId}");
        }

        public ActionResult AcceptTask(int? id, bool ajax = false)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var task = _repo.GetTaskNoAsync(id.Value);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            task.Accepted = true;
            task.Todo = false;

            var todos = _repo.GetContext().Todo.Where(q => q.TaskID == task.Id && q.Type == TodoType.Akceptacja);

            foreach (var todo in todos)
            {
                todo.Checked = true;

                _repo.GetContext().Entry(todo).State = EntityState.Modified;
            }

            _repo.TaskEdit(task, "", false);

            if (ajax)
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            else
                return Redirect($"/Projects/ApartmentDetails/{task.ApartmentId}");
        }

        public ActionResult ForceFinishTask(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var task = _repo.GetTaskNoAsync(id.Value);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            task.Status = TaskStatus.Finished;

            _repo.TaskEdit(task);

            return Redirect($"/Projects/ApartmentDetails/{task.ApartmentId}");
        }

        [HttpGet]
        public ActionResult LockTask(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var task = _repo.GetTaskNoAsync(id.Value);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (task.Blocked)
                task.Blocked = false;
            else
                task.Blocked = true;

            var result = _repo.TaskEdit(task, canStatusChange: false);

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult QuestionPhoto(int? id, string question)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var photos = _repo.GetQuestionPhotos((int)id, question);

            return Json(new { photos }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SaveQuestionTranslate(int? ID, string translated)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var context = _repo.GetContext();

            var questionPhoto = context.TaskQuestionPhoto.Find(ID.Value);

            if (questionPhoto == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            questionPhoto.TranslatedDescription = translated;

            var task = questionPhoto.Task_Question.Task;
            var todos = task.Todos.Where(q => q.Type == TodoType.TlumaczeniaPytanKontrolnych);

            foreach (var t in todos)
            {
                t.Checked = true;
                context.Entry(t).State = EntityState.Modified;
            }

            context.Entry(questionPhoto).State = EntityState.Modified;

            try
            {
                context.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult SendRaport(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var url = $"http://{HttpContext.Request.Url.Host}:{HttpContext.Request.Url.Port}/Print/Raport/{ID.Value}";

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

            var fileName = Strings.RandomString(16);

            using (var sr = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
            {
                var fileContent = sr.ReadToEnd();

                PdfDocument pdf = PdfGenerator.GeneratePdf(fileContent, PageSize.A4);
                pdf.Save(Server.MapPath($"~/dist/RaportPDF/{fileName}.pdf"));
            }

            var context = _repo.GetContext();
            var currentTask = context.Tasks.Find(ID.Value);

            var photosList = (from tf in currentTask.Task_Fitter from p in tf.Photos where p.Blocked == false select Server.MapPath($"~/Monter/dist/Task_Images/{p.Photo}")).ToList();

            List<string> errorPhotos = (currentTask.Task_Questions != null) ? (from er in currentTask.Task_Questions.TaskQuestionPhoto where !string.IsNullOrEmpty(er.TranslatedDescription) select Server.MapPath($"~/Monter/dist/Question_Images/{er.Photo}")).ToList() : new List<string>();
            photosList.AddRange(errorPhotos);

            errorPhotos = currentTask.Task_Question != null && currentTask.Task_Question.Any() ? currentTask.Task_Question.Where(q => !string.IsNullOrEmpty(q.Photo)).Select(q => Server.MapPath($"~/Monter/dist/Question_Images/{q.Photo}")).ToList() : new List<string>();
            photosList.AddRange(errorPhotos);

            var exception = "";

            MailService.Raport(currentTask, Server.MapPath($"~/dist/RaportPDF/{fileName}.pdf"), photosList, out exception);

            if (!string.IsNullOrEmpty(exception))
                return Json(new {success = false, message = exception}, JsonRequestBehavior.AllowGet);

            currentTask.ReportSent = true;
            currentTask.ReportSentDate = DateTime.Now;
            currentTask.Todo = false;

            var todos = currentTask.Todos.Where(q => q.Type == TodoType.ZakoczenieZBledami);

            foreach (var t in todos)
            {
                t.Checked = true;

                context.Entry(t).State = EntityState.Modified;
            }

            context.Entry(currentTask).State = EntityState.Modified;

            context.SaveChanges();

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RaportyZip(string Apartamenty)
        {
            if (string.IsNullOrEmpty(Apartamenty))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var apartmentsIDs = Apartamenty.ToIntArray(',');
            var pdfList = new Dictionary<string, string>();
            var zipName = $"{Strings.RandomString(16)}.zip";

            using (var db = new ACSystemContext())
            {
                var tasks = db.Tasks.Where(q => (q.Status == TaskStatus.Finished || q.Status == TaskStatus.FinishedWithFaults) && q.Product.ProductType == TaskType.Kitchen && apartmentsIDs.Contains(q.ApartmentId));

                foreach (var t in tasks)
                {
                    var url = $"http://{HttpContext.Request.Url.Host}:{HttpContext.Request.Url.Port}/Print/Raport/{t.Id}";

                    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

                    var fileName = string.Format("LGH{0} {1} {2}", t.Apartment.NumberOfApartment, t.GetTaskNameOnInvoice(t.Apartment.ProjectId), Strings.RandomString(6));

                    using (var sr = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
                    {
                        var fileContent = sr.ReadToEnd();
                        var pdfPath = Server.MapPath($"~/dist/RaportPDF/{fileName}.pdf");

                        PdfDocument pdf = PdfGenerator.GeneratePdf(fileContent, PageSize.A4);
                        pdf.Save(pdfPath);

                        pdfList.Add(fileName + ".pdf", pdfPath);
                    }
                }

                var zipPath = Server.MapPath($"~/dist/RaportPDF/{zipName}");

                using (ZipArchive archive = ZipFile.Open(zipPath, ZipArchiveMode.Create))
                {
                    foreach (var file in pdfList)
                    {
                        archive.CreateEntryFromFile(file.Value, file.Key);
                    }
                }
            }

            return Json(new { success = true, file = zipName }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SendApartmentGroupRaport(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var url = $"http://{HttpContext.Request.Url.Host}:{HttpContext.Request.Url.Port}/Print/ApartmentRaportGrouped/{ID.Value}";

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

            var fileName = Strings.RandomString(16);

            using (var sr = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
            {
                var fileContent = sr.ReadToEnd();

                PdfDocument pdf = PdfGenerator.GeneratePdf(fileContent, PageSize.A4);
                pdf.Save(Server.MapPath($"~/dist/RaportPDF/{fileName}.pdf"));
            }

            var context = _repo.GetContext();
            var report = context.ReportNag.Find(ID.Value);

            var exception = "";

            MailService.ApartmentGroupRaport(report, Server.MapPath($"~/dist/RaportPDF/{fileName}.pdf"), out exception);

            if (!string.IsNullOrEmpty(exception))
                return Json(new { success = false, message = exception }, JsonRequestBehavior.AllowGet);

            report.SendDate = DateTime.Now;
            context.Entry(report).State = EntityState.Modified;

            context.SaveChanges();

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MassiveSendGroupReports(string reportsID)
        {
            if (string.IsNullOrEmpty(reportsID))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var idArray = reportsID.ToIntArray(',');

            var url = $"http://{HttpContext.Request.Url.Host}:{HttpContext.Request.Url.Port}/Print/MassivePrintGroupReports?Reports={reportsID}&Print=true";

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

            var fileName = Strings.RandomString(16);

            using (var sr = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
            {
                var fileContent = sr.ReadToEnd();

                PdfDocument pdf = PdfGenerator.GeneratePdf(fileContent, PageSize.A4);
                pdf.Save(Server.MapPath($"~/dist/RaportPDF/{fileName}.pdf"));
            }

            var context = _repo.GetContext();
            var reports = context.ReportNag.Where(q => idArray.Contains(q.ID));

            var exception = "";

            MailService.ApartmentGroupRaport(reports.FirstOrDefault(), Server.MapPath($"~/dist/RaportPDF/{fileName}.pdf"), out exception);

            if (!string.IsNullOrEmpty(exception))
                return Json(new { success = false, message = exception }, JsonRequestBehavior.AllowGet);

            foreach(var report in reports)
            {
                report.SendDate = DateTime.Now;
                context.Entry(report).State = EntityState.Modified;
            }

            context.SaveChanges();

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteAccess(string fitterId, int taskId)
        {
            var db = _repo.GetContext();

            var access = db.Task_AccessGrant.FirstOrDefault(q => q.FitterID == fitterId && q.TaskID == taskId);

            if (access == null)
                return Json(new {success = false}, JsonRequestBehavior.AllowGet);

            db.Task_AccessGrant.Remove(access);

            try
            {
                db.SaveChanges();

                return Json(new {success = true}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult DeletePlan(int eventID)
        {
            var db = _repo.GetContext();

            var plan = db.Events.Find(eventID);

            if (plan == null)
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);

            db.Events.Remove(plan);

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult PlanFitterToTask(int ApartmentID, int TaskID, string FitterID, DateTime PlannedDate,
            string Message)
        {
            var newEvent = new Events
            {
                Type = EventType.Plan,
                ReferenceTo = TaskID,
                ReferenceToUser = FitterID,
                Start = PlannedDate,
                End = PlannedDate,
                AllDay = true,
                Title = "Plan wykonania zadania",
                Description = Message
            };

            if (_repo.AddEvent(newEvent))
                if (ApartmentID > 0)
                    return Redirect("/Projects/ApartmentDetails/" + ApartmentID);
                else
                    return Redirect("/Task/Details/" + TaskID);
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #region PRINT
        public ActionResult PrintTask(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var task = _repo.GetTaskNoAsync(id.Value);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return PartialView("Print/Task", task);
        }
        #endregion
        #endregion

        #region EVENTS
        [HttpPost]
        public ActionResult AddEvent(Events events)
        {
            if (_repo.AddEvent(events))
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ACTIVITY
        [HttpGet]
        public ActionResult ActivityPhotos(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var photos = _repo.GetActivityPhotos((int)id);

            return Json(new { photos }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteActivityPhoto(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var context = _repo.GetContext();
            var result = false;

            try
            {
                var photo = context.TaskActivityPhotos.Find(id.Value);

                if (photo == null)
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);

                context.TaskActivityPhotos.Remove(photo);
                context.SaveChanges();

                result = true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                result = false;
            }

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActivityDelete(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var context = _repo.GetContext();

            try
            {
                var activity = context.Task_Fitter.Find(id.Value);
                var taskID = activity.TaskId;
                var task = context.Tasks.Find(taskID);

                activity.IsActive = false;

                foreach (var p in activity.Photos)
                {
                    p.Blocked = true;
                    context.Entry(p).State = EntityState.Modified;
                }

                context.Entry(activity).State = EntityState.Modified;

                context.SaveChanges();

                var ac = context.Task_Fitter.Where(q => q.TaskId == taskID && q.Status != Core.TaskStatus.UnderWork && q.IsActive == true);

                if (ac.Count() > 0)
                {
                    ac = ac.OrderByDescending(q => q.Timestamp);

                    task.Status = ac.FirstOrDefault().Status;
                }
                else
                {
                    task.Status = Core.TaskStatus.Free;
                }

                context.Entry(task).State = EntityState.Modified;
                context.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "ActivityDelete",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                context.ExceptionsLog.Add(log);

                context.SaveChanges();
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        [HttpPost]
        public ActionResult ActivityExcel(int? ApartmentID, int? ProjectID)
        {
            var db = new ACSystemContext();
            var apartment = db.Apartments.Find(ApartmentID);
            var project = db.Projects.Find(ProjectID);
            var taskDict = new Dictionary<int, int>();

            var taskLp = 1;
            foreach (var t in db.Tasks.Where(q => q.ApartmentId == ApartmentID).ToList())
            {
                taskDict.Add(t.Id, taskLp);
                taskLp++;
            }

            var activities = new List<Task_Fitter>();

            if (ApartmentID.HasValue)
                activities = db.Task_Fitter.Where(q =>
                                    q.Task.ApartmentId == ApartmentID &&
                                    (q.Status == TaskStatus.Finished ||
                                        q.Status == TaskStatus.FinishedWithFaults ||
                                        q.Status == TaskStatus.Stoped ||
                                        q.Status == TaskStatus.PlaceNotReady ||
                                        q.Status == TaskStatus.NieNasze ||
                                        q.Status == TaskStatus.DoWyjasnienia)
                                    ).OrderByDescending(q => q.Timestamp).ToList();
            else if (ProjectID.HasValue)
                activities = db.Task_Fitter.Where(q =>
                            q.Task.Apartment.ProjectId == ProjectID &&
                            (q.Status == TaskStatus.Finished ||
                             q.Status == TaskStatus.FinishedWithFaults ||
                             q.Status == TaskStatus.Stoped ||
                             q.Status == TaskStatus.PlaceNotReady ||
                             q.Status == TaskStatus.NieNasze ||
                             q.Status == TaskStatus.DoWyjasnienia)
                        ).OrderByDescending(q => q.Timestamp).ToList();


            var excel = new XLWorkbook();
            excel.AddWorksheet("Szczegółowo");
            excel.AddWorksheet("Zadania");

            var sheet = excel.Worksheet(1);
            var sheet2 = excel.Worksheet(2);

            IXLColumn taskLP_col = sheet.Column(1),
                      productName_col = sheet.Column(2),
                      polishDesc_col = sheet.Column(3),
                      sweDesc_col = sheet.Column(4),
                      fitterName_col = sheet.Column(5),
                      workDone_col = sheet.Column(6),
                      productJM_col = sheet.Column(7),
                      workTime_col = sheet.Column(8),
                      status_col = sheet.Column(9),
                      errorType_col = sheet.Column(10),
                      date_col = sheet.Column(11),
                      timestamp_col = sheet.Column(12),
                      taskLp_2_col = sheet2.Column(1),
                      productName_2_col = sheet2.Column(2),
                      polishDesc_2_col = sheet2.Column(3),
                      sweDesc_2_col = sheet2.Column(4),
                      workDone_2_col = sheet2.Column(5),
                      productJm_2_col = sheet2.Column(6),
                      status_2_col = sheet2.Column(7),
                      date_2_col = sheet2.Column(8),
                      timestamp_2_col = sheet2.Column(9);

            #region Nagłówek

            taskLP_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            taskLP_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            taskLP_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            taskLP_col.AdjustToContents();

            taskLp_2_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            taskLp_2_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            taskLp_2_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            taskLp_2_col.AdjustToContents();

            if (ApartmentID.HasValue)
            {
                taskLP_col.Cell(1).Value = "Lp.z.";
                taskLp_2_col.Cell(1).Value = "Lp.z.";
            }
            else
            {
                taskLP_col.Cell(1).Value = "LGH";
                taskLp_2_col.Cell(1).Value = "LGH";
            }

            productName_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            productName_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            productName_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            productName_col.AdjustToContents();
            productName_col.Cell(1).Value = "Nazwa produktu";

            productName_2_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            productName_2_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            productName_2_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            productName_2_col.AdjustToContents();
            productName_2_col.Cell(1).Value = "Nazwa produktu";

            polishDesc_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            polishDesc_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            polishDesc_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            polishDesc_col.AdjustToContents();
            polishDesc_col.Cell(1).Value = "Opis polski";

            polishDesc_2_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            polishDesc_2_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            polishDesc_2_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            polishDesc_2_col.AdjustToContents();
            polishDesc_2_col.Cell(1).Value = "Opis polski";

            sweDesc_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            sweDesc_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            sweDesc_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            sweDesc_col.AdjustToContents();
            sweDesc_col.Cell(1).Value = "Opis szwedzki";

            sweDesc_2_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            sweDesc_2_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            sweDesc_2_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            sweDesc_2_col.AdjustToContents();
            sweDesc_2_col.Cell(1).Value = "Opis szwedzki";

            fitterName_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            fitterName_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            fitterName_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            fitterName_col.AdjustToContents();
            fitterName_col.Cell(1).Value = "Monter";

            workDone_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            workDone_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            workDone_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            workDone_col.AdjustToContents();
            workDone_col.Cell(1).Value = "Wykonana ilość";

            workDone_2_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            workDone_2_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            workDone_2_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            workDone_2_col.AdjustToContents();
            workDone_2_col.Cell(1).Value = "Wykonana ilość";

            productJM_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            productJM_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            productJM_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            productJM_col.AdjustToContents();
            productJM_col.Cell(1).Value = "JM.";

            productJm_2_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            productJm_2_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            productJm_2_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            productJm_2_col.AdjustToContents();
            productJm_2_col.Cell(1).Value = "JM.";

            workTime_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            workTime_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            workTime_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            workTime_col.AdjustToContents();
            workTime_col.Cell(1).Value = "Czas pracy (h)";

            status_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            status_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            status_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            status_col.AdjustToContents();
            status_col.Cell(1).Value = "Status";

            status_2_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            status_2_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            status_2_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            status_2_col.AdjustToContents();
            status_2_col.Cell(1).Value = "Status";

            errorType_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            errorType_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            errorType_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            errorType_col.AdjustToContents();
            errorType_col.Cell(1).Value = "Błąd";

            date_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            date_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            date_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            date_col.AdjustToContents();
            date_col.Cell(1).Value = "Data";

            date_2_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            date_2_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            date_2_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            date_2_col.AdjustToContents();
            date_2_col.Cell(1).Value = "Data";

            timestamp_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            timestamp_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            timestamp_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            timestamp_col.AdjustToContents();
            timestamp_col.Cell(1).Value = "Timestamp";

            timestamp_2_col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
            timestamp_2_col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            timestamp_2_col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
            timestamp_2_col.AdjustToContents();
            timestamp_2_col.Cell(1).Value = "Timestamp";

            #endregion

            var row = 2;

            foreach (var a in activities)
            {
                if (ApartmentID.HasValue)
                    taskLP_col.Cell(row).Value = a.TaskId.ToString().PadLeft(6, '0');
                else
                    taskLP_col.Cell(row).Value = a.Task.Apartment.NumberOfApartment;

                productName_col.Cell(row).Value = a.Task.GetTaskName(a.Task.Apartment.ProjectId);
                polishDesc_col.Cell(row).Value = a.Task.Description;
                sweDesc_col.Cell(row).Value = a.Task.InvoiceDescription;
                fitterName_col.Cell(row).Value = a.Fitter?.User.FullName ?? "";
                workDone_col.Cell(row).Value = a.WorkDone;
                productJM_col.Cell(row).Value = a.Task.Product.Jm;
                workTime_col.Cell(row).Value = Math.Round((decimal)a.WorkTime / 60, 1);

                var stringStatus = "";
                switch (a.Status)
                {
                    case TaskStatus.Free:
                        stringStatus = "Dostępne";
                        break;
                    case TaskStatus.UnderWork:
                        stringStatus = "W trakcie";
                        break;
                    case TaskStatus.Stoped:
                        stringStatus = "Przerwane";
                        break;
                    case TaskStatus.Finished:
                        stringStatus = "Zakończone";
                        break;
                    case TaskStatus.FinishedWithFaults:
                        stringStatus = "Zakończone z błędami";
                        break;
                    case TaskStatus.PlaceNotReady:
                        stringStatus = "Miejsce nieprzygotowane";
                        break;
                    case TaskStatus.Planned:
                        stringStatus = "Planowane";
                        break;
                    case TaskStatus.NieNasze:
                        stringStatus = "Nie nasze";
                        break;
                    case TaskStatus.DoWyjasnienia:
                        stringStatus = "Do wyjaśnienia";
                        break;
                }

                status_col.Cell(row).Value = stringStatus;

                errorType_col.Cell(row).Value = (a.Task.ErrorType == TaskErrorType.Quantity) ? "Ilościowy" : "----";
                date_col.Cell(row).Value = (a.HandDateTime.HasValue) ? a.HandDateTime.Value.ToString("yyyy-MM-dd HH:mm") : a.Timestamp.ToString("yyyy-MM-dd HH:mm");
                timestamp_col.Cell(row).Value = a.Timestamp.ToString("yyyy-MM-dd HH:mm");

                if (a.IsActive == false)
                    sheet.Row(row).Style.Font.Strikethrough = true;

                row++;
            }

            row = 2;

            foreach (var a in activities.Where(q => q.IsActive).GroupBy(q => q.Task))
            {
                if (ApartmentID.HasValue)
                    taskLp_2_col.Cell(row).Value = a.Key.Id.ToString().PadLeft(6, '0');
                else
                    taskLp_2_col.Cell(row).Value = a.Key.Apartment.NumberOfApartment;

                productName_2_col.Cell(row).Value = a.Key.GetTaskName(a.Key.Apartment.ProjectId);
                polishDesc_2_col.Cell(row).Value = a.Key.Description;
                sweDesc_2_col.Cell(row).Value = a.Key.InvoiceDescription;
                workDone_2_col.Cell(row).Value = a.Sum(q => q.WorkDone);
                productJm_2_col.Cell(row).Value = a.Key.Product.Jm;

                var stringStatus = "";
                switch (a.Key.Status)
                {
                    case TaskStatus.Free:
                        stringStatus = "Dostępne";
                        break;
                    case TaskStatus.UnderWork:
                        stringStatus = "W trakcie";
                        break;
                    case TaskStatus.Stoped:
                        stringStatus = "Przerwane";
                        break;
                    case TaskStatus.Finished:
                        stringStatus = "Zakończone";
                        break;
                    case TaskStatus.FinishedWithFaults:
                        stringStatus = "Zakończone z błędami";
                        break;
                    case TaskStatus.PlaceNotReady:
                        stringStatus = "Miejsce nieprzygotowane";
                        break;
                    case TaskStatus.Planned:
                        stringStatus = "Planowane";
                        break;
                    case TaskStatus.NieNasze:
                        stringStatus = "Nie nasze";
                        break;
                    case TaskStatus.DoWyjasnienia:
                        stringStatus = "Do wyjaśnienia";
                        break;
                }

                status_2_col.Cell(row).Value = stringStatus;

                date_2_col.Cell(row).Value = (a.OrderByDescending(q => q.HandDateTime).FirstOrDefault().HandDateTime.HasValue) ? a.OrderByDescending(q => q.HandDateTime).FirstOrDefault().HandDateTime.Value.ToString("yyyy-MM-dd HH:mm") : a.OrderByDescending(q => q.Timestamp).FirstOrDefault().Timestamp.ToString("yyyy-MM-dd HH:mm");
                timestamp_col.Cell(row).Value = a.OrderByDescending(q => q.Timestamp).FirstOrDefault().Timestamp.ToString("yyyy-MM-dd HH:mm");

                row++;
            }

            taskLP_col.AdjustToContents();
            productName_col.AdjustToContents();
            fitterName_col.AdjustToContents();
            workDone_col.AdjustToContents();
            productJM_col.AdjustToContents();
            workTime_col.AdjustToContents();
            status_col.AdjustToContents();
            errorType_col.AdjustToContents();
            date_col.AdjustToContents();
            timestamp_col.AdjustToContents();
            taskLp_2_col.AdjustToContents();
            productName_2_col.AdjustToContents();
            polishDesc_2_col.AdjustToContents();
            sweDesc_2_col.AdjustToContents();
            workDone_2_col.AdjustToContents();
            productJm_2_col.AdjustToContents();
            status_2_col.AdjustToContents();
            date_2_col.AdjustToContents();
            timestamp_2_col.AdjustToContents();


            var fileName = (ApartmentID.HasValue)
                ? $"DziennikAktywnosci-LGH{apartment.NumberOfApartment}-{DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss")}.xlsx"
                : $"DziennikAktywnosci-Projekt_{project.ProjectNumber}-{DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss")}.xlsx";
            var filePath = Path.Combine(Server.MapPath("~/dist/AcitivityExcel"), fileName);

            try
            {
                excel.SaveAs(filePath);

                return Json(new { success = true, file = fileName }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                return Json(new { success = false, exception = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region MODALS

        [HttpGet]
        public ActionResult Modal_AddApartment(int? ProjectID)
        {
            if (!ProjectID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.ProjectID = ProjectID.Value;

            return PartialView();
        }

        [HttpGet]
        public ActionResult Modal_CopyApartment(int? ProjectID, int? ApartmentIDToCopy)
        {
            if (!ProjectID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!ApartmentIDToCopy.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.ProjectID = ProjectID.Value;
            ViewBag.ApartmentIDToCopy = ApartmentIDToCopy.Value;

            return PartialView();
        }

        [HttpGet]
        public ActionResult Modal_CopyProject(int? ProjectID)
        {
            if (!ProjectID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.ProjectID = ProjectID.Value;

            return PartialView();
        }

        [HttpGet]
        public ActionResult Modal_EditApartment(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var apartment = _repo.ApartmentById(id.Value);

            if (apartment == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return PartialView(apartment);
        }

        [HttpGet]
        public ActionResult Modal_AddTask(int? ApartmentID)
        {
            if (ApartmentID == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var productsList = _repo.ProductsList().OrderBy(q => q.Name).ToList();

            ViewBag.ProductsList = productsList;

            var apartment = _repo.GetContext().Apartments.Find(ApartmentID.Value);
            var product = productsList.FirstOrDefault(q => q.Product_Project.Count(x => x.ProjectID == apartment.ProjectId) > 0 || q.ForAllProjects);

            ViewBag.Product = product;

            ViewBag.IsSetProductsList = productsList.Count > 0;

            ViewBag.ApartmentID = ApartmentID.Value;
            ViewBag.ProjectID = _repo.GetContext().Apartments.Find(ApartmentID.Value)?.ProjectId ?? 0;

            decimal pType1 = 0, pType2 = 0, pType3 = 0, pType4 = 0, pType5 = 0, pType6 = 0;

            decimal.TryParse(product.PercentHeight.Type1.ToString().Split('.')[0], out pType1);
            decimal.TryParse(product.PercentHeight.Type2.ToString().Split('.')[0], out pType2);
            decimal.TryParse(product.PercentHeight.Type3.ToString().Split('.')[0], out pType3);
            decimal.TryParse(product.PercentHeight.Type4.ToString().Split('.')[0], out pType4);
            decimal.TryParse(product.PercentHeight.Type5.ToString().Split('.')[0], out pType5);
            decimal.TryParse(product.PercentHeight.Type6.ToString().Split('.')[0], out pType6);

            var productProject = product.Product_Project.FirstOrDefault(q => q.ProjectID == apartment.ProjectId);

            if (productProject != null && productProject.PercentType1.HasValue)
                pType1 = productProject.PercentType1.Value;

            if (productProject != null && productProject.PercentType2.HasValue)
                pType2 = productProject.PercentType2.Value;

            var price = product.Price;

            if (productProject != null && apartment != null)
                price = productProject.Price;

            var questions = (productProject != null) ? productProject.Question_Product_Projects.Select(q => q.Question).ToList() : new List<Question>();
            var questionIDs = questions.Select(x => x.ID).ToList();
            var answers = _repo.GetContext().Answer.Where(q => questionIDs.Contains(q.QuestionID)).ToList();

            ViewBag.Questions = questions;
            ViewBag.Answers = answers;

            var model = new Tasks
            {
                ProductPrice = price,
                PercentType1 = pType1,
                PercentType2 = pType2,
                PercentType3 = pType3,
                PercentType4 = pType4,
                PercentType5 = pType5,
                PercentType6 = pType6
            };

            return PartialView(model);
        }

        [HttpGet]
        public async Task<ActionResult> Modal_EditTask(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var task = await _repo.GetTask(id.Value);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var productsList = _repo.ProductsList();
            var product = task.Product;

            var productProject = product.Product_Project.FirstOrDefault(q => q.ProjectID == task.Apartment.ProjectId);
            var questions = (productProject != null) ? productProject.Question_Product_Projects.Select(q => q.Question).ToList() : new List<Question>();
            var questionIDs = questions.Select(x => x.ID).ToList();
            var answers = _repo.GetContext().Answer.Where(q => questionIDs.Contains(q.QuestionID)).ToList();
            var taskQuestions = task.Task_Question.ToList();

            ViewBag.Product = product;
            ViewBag.Questions = questions;
            ViewBag.Answers = answers;
            ViewBag.TaskQuestions = taskQuestions;
            ViewBag.ProductsList = productsList;
            ViewBag.IsSetProductsList = productsList.Count > 0;
            ViewBag.ProjectID = _repo.GetContext().Apartments.Find(task.ApartmentId)?.ProjectId ?? 0;

            return PartialView(task);
        }

        [HttpGet]
        public ActionResult Modal_DetailsTask(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = _repo.GetContext();
            var task = _repo.GetTaskNoAsync(id.Value);

            var plan = new List<PlannedTaskListForTask>();
            var events = db.Events.Where(q => q.Type == EventType.Plan && q.ReferenceTo == task.Id);

            foreach (var e in events)
            {
                var fitter = db.Fitters.Find(e.ReferenceToUser);

                var p = new PlannedTaskListForTask
                {
                    EventID = e.Id,
                    FitterID =  fitter.Id,
                    FitterName = fitter.User.FullName,
                    PlannedDate = e.Start
                };

                plan.Add(p);
            }


            ViewBag.Fitters = task.Fitters;
            ViewBag.Plan = plan;

            return PartialView(task);
        }

        [HttpGet]
        public ActionResult Modal_AddTaskEvent(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var model = new Events
            {
                ReferenceTo = id.Value
            };

            return PartialView(model);
        }

        [HttpGet]
        public async Task<ActionResult> Modal_AddTaskSerwis(int? id, string backTo = "apartment")
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var source = await _repo.GetTask(id.Value);
            var rooms = _repo.GetContext().Rooms.ToList();

            ViewBag.SourceTask = source;
            ViewBag.Rooms = rooms;
            ViewBag.BackTo = backTo;

            var serwis = new Tasks
            {
                ApartmentId = source.ApartmentId,
                Apartment = source.Apartment,
                TaskOriginalId = source.Id,
                ProductId = source.ProductId,
                Product = source.Product,
                ProductPrice = source.ProductPrice,
                ProductCount = source.ProductCount,
                Production = source.Production,
                StartDate = source.StartDate,
                EndDate = source.EndDate,
                PercentType1 = source.PercentType1,
                PercentType2 = source.PercentType2,
                PercentType3 = source.PercentType3,
                PercentType4 = source.PercentType4,
                PercentType5 = source.PercentType5,
                PercentType6 = source.PercentType6,
                Task_Questions = source.Task_Questions,
                TaskPatches = source.TaskPatches
            };

            return PartialView(serwis);
        }

        [HttpGet]
        public ActionResult Modal_EditTaskSerwis(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var rooms = _repo.GetContext().Rooms.ToList();

            ViewBag.Rooms = rooms;

            var task = _repo.GetTaskNoAsync(id.Value);

            if (task.TaskOriginalId.HasValue)
            {
                var oryginalTask = _repo.GetTaskNoAsync(task.TaskOriginalId.Value);
                ViewBag.SourceTask = oryginalTask;
            }

            return PartialView(task);
        }

        [HttpGet]
        public ActionResult Modal_AddKitchenServiceTask(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var productsList = _repo.ProductsList().Where(q => q.ProductType == TaskType.Kitchen).ToList();
            var apartment = _repo.GetContext().Apartments.Find(id.Value);
            var product = _repo.ProductsList().FirstOrDefault(q => q.Product_Project.Count(x => x.ProjectID == apartment.ProjectId) > 0 || q.ForAllProjects);
            var rooms = _repo.GetContext().Rooms.ToList();

            ViewBag.ProductsList = productsList;
            ViewBag.Product = product;
            ViewBag.IsSetProductsList = productsList.Any();
            ViewBag.ApartmentID = id.Value;
            ViewBag.ProjectID = _repo.GetContext().Apartments.Find(id.Value)?.ProjectId ?? 0;
            ViewBag.Rooms = rooms;

            decimal pType1 = 0, pType2 = 0, pType3 = 0, pType4 = 0, pType5 = 0, pType6 = 0;

            decimal.TryParse(product.PercentHeight.Type1.ToString().Split('.')[0], out pType1);
            decimal.TryParse(product.PercentHeight.Type2.ToString().Split('.')[0], out pType2);
            decimal.TryParse(product.PercentHeight.Type3.ToString().Split('.')[0], out pType3);
            decimal.TryParse(product.PercentHeight.Type4.ToString().Split('.')[0], out pType4);
            decimal.TryParse(product.PercentHeight.Type5.ToString().Split('.')[0], out pType5);
            decimal.TryParse(product.PercentHeight.Type6.ToString().Split('.')[0], out pType6);

            var model = new Tasks
            {
                ProductPrice = product.Price,
                PercentType1 = pType1,
                PercentType2 = pType2,
                PercentType3 = pType3,
                PercentType4 = pType4,
                PercentType5 = pType5,
                PercentType6 = pType6
            };

            return PartialView("Modals/Modal_AddKitchenServiceTask", model);
        }

        [HttpGet]
        public ActionResult Modal_EditTaskGroup()
        {
            return PartialView("Modals/Modal_EditTaskGroup", new Tasks());
        }

        [HttpGet]
        public ActionResult Modal_AddConstructionServiceTask(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var productsList = _repo.ProductsList().Where(q => q.ProductType == TaskType.Construction).ToList();

            ViewBag.ProductsList = productsList;

            var apartment = _repo.GetContext().Apartments.Find(id.Value);
            var product = _repo.ProductsList().FirstOrDefault(q => q.Product_Project.Count(x => x.ProjectID == apartment.ProjectId) > 0 || q.ForAllProjects);
            var rooms = _repo.GetContext().Rooms.ToList();

            ViewBag.Product = product;
            ViewBag.IsSetProductsList = productsList.Any();
            ViewBag.ApartmentID = id.Value;
            ViewBag.ProjectID = _repo.GetContext().Apartments.Find(id.Value)?.ProjectId ?? 0;
            ViewBag.Rooms = rooms;

            decimal pType1 = 0, pType2 = 0, pType3 = 0, pType4 = 0, pType5 = 0, pType6 = 0;

            decimal.TryParse(product.PercentHeight.Type1.ToString().Split('.')[0], out pType1);
            decimal.TryParse(product.PercentHeight.Type2.ToString().Split('.')[0], out pType2);
            decimal.TryParse(product.PercentHeight.Type3.ToString().Split('.')[0], out pType3);
            decimal.TryParse(product.PercentHeight.Type4.ToString().Split('.')[0], out pType4);
            decimal.TryParse(product.PercentHeight.Type5.ToString().Split('.')[0], out pType5);
            decimal.TryParse(product.PercentHeight.Type6.ToString().Split('.')[0], out pType6);

            var model = new Tasks
            {
                ProductPrice = product.Price,
                PercentType1 = pType1,
                PercentType2 = pType2,
                PercentType3 = pType3,
                PercentType4 = pType4,
                PercentType5 = pType5,
                PercentType6 = pType6
            };

            return PartialView("Modals/Modal_AddConstructionServiceTask", model);
        }

        [HttpGet]
        public ActionResult Modal_AddFitterToTask(int? ApartmentID, int? TaskID)
        {
            if (!ApartmentID.HasValue || !TaskID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.ApartmentID = ApartmentID.Value;
            ViewBag.TaskID = TaskID.Value;

            return PartialView(_repo.GetFreeFitters());
        }

        [HttpGet]
        public ActionResult Modal_PlanFitterToTask(int? ApartmentID, int? TaskID)
        {
            if (!ApartmentID.HasValue || !TaskID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.ApartmentID = ApartmentID.Value;
            ViewBag.TaskID = TaskID.Value;

            return PartialView(_repo.GetFitters().Where(q => q.IsLocked == false).ToList());
        }

        [HttpGet]
        public ActionResult Modal_GrantAccessToTask(int? ApartmentID, int? TaskID)
        {
            if (!ApartmentID.HasValue || !TaskID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.ApartmentID = ApartmentID.Value;
            ViewBag.TaskID = TaskID.Value;

            return PartialView(_repo.GetOuterFitters());
        }

        [HttpGet]
        public ActionResult Modal_ProjectAccess(int? ProjectID)
        {
            if (!ProjectID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.ProjectID = ProjectID.Value;

            var fitters = new ACSystemContext().ACSystemUser.Where(q => q.CanSeeAll == false).Select(q => new FittersSelect { ID = q.Id, Name = q.Name, SurName = q.Surname }).ToList();

            return PartialView("Modals/Modal_ProjectAccess", fitters);
        }

        [HttpGet]
        public ActionResult Modal_ApartmentAccess(int? ApartmentID)
        {
            if (!ApartmentID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.ApartmentID = ApartmentID.Value;

            var fitters = new ACSystemContext().ACSystemUser.Where(q => q.CanSeeAll == false).Select(q => new FittersSelect { ID = q.Id, Name = q.Name, SurName = q.Surname }).ToList();

            return PartialView("Modals/Modal_ApartmentAccess", fitters);
        }

        [HttpGet]
        public ActionResult Modal_TaskAccess(int? TaskID)
        {
            if (!TaskID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.TaskID = TaskID.Value;

            var fitters = new ACSystemContext().ACSystemUser.Where(q => q.CanSeeAll == false).Select(q => new FittersSelect { ID = q.Id, Name = q.Name, SurName = q.Surname }).ToList();

            return PartialView("Modals/Modal_TaskAccess", fitters);
        }

        [HttpGet]
        public ActionResult Modal_AddProjectManager(int? CustomerID)
        {
            if (!CustomerID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.CustomerID = CustomerID.Value;

            return PartialView("Modals/Modal_AddProjectManager");
        }


        [HttpGet]
        public ActionResult Modal_NewOrderInApartment(int? ProjectID, int? ApartmentID)
        {
            if (!ProjectID.HasValue || !ApartmentID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = _repo.GetContext();

            var apartment = db.Apartments.Find(ApartmentID);

            if (apartment == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.Tasks = apartment.Tasks.Select(q => new TasksSelect { ID = q.Id, ProductName = q.GetTaskName(q.Apartment.ProjectId), Sufix = q.TaskSufix }).ToList();
            ViewBag.ProjectID = ProjectID.Value;
            ViewBag.ApartmentID = ApartmentID.Value;

            return PartialView("Modals/Modal_NewOrderInApartment");
        }

        [HttpGet]
        public ActionResult Modal_NewOrderInProject(int? ProjectID)
        {
            if (!ProjectID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = _repo.GetContext();

            var project = db.Projects.Find(ProjectID);

            if (project == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var apartments = project.Apartments;

            ViewBag.Apartments = apartments.Select(q => new ApartmentsSelect { ID = q.Id, ApartmentNumber = q.NumberOfApartment }).ToList();

            var tasks = new List<TasksSelect>();

            if (apartments.FirstOrDefault() != null)
            {
                tasks = apartments.FirstOrDefault().Tasks.Select(q => new TasksSelect { ID = q.Id, ProductName = q.GetTaskName(q.Apartment.ProjectId), Sufix = q.TaskSufix }).ToList();
            }

            ViewBag.Tasks = tasks;
            ViewBag.ProjectID = ProjectID.Value;

            return PartialView("Modals/Modal_NewOrderInProject");
        }


        [HttpGet]
        public ActionResult Modal_TaskPhotosAll(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var task = db.Tasks.Find(ID);

            if (task == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            return PartialView("Modals/Modal_TaskPhotosAll", task);
        }

        [HttpGet]
        public ActionResult Modal_HandyFinishTask(int? ID, string backTo = "apartment")
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var fitters = _repo.GetFitters().Where(q => q.IsLocked == false).ToList();
            var task = _repo.GetTaskNoAsync(ID.Value);

            var context = _repo.GetContext();


            ViewBag.Fitters = fitters.Where(q => !q.IsLocked).ToList();
            ViewBag.BackTo = backTo;

            if (task == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            return PartialView("Modals/Modal_HandyFinishTask", task);
        }

        [HttpGet]
        public ActionResult Modal_ReportDetails(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var report = _repo.GetContext().ReportNag.Find(ID.Value);

            if (report == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            report.Elements = _repo.GetContext().ReportElem.Where(q => q.ReportNagID == report.ID).ToList();

            return PartialView("Modals/Modal_ReportDetails", report);
        }

        [HttpGet]
        public ActionResult Modal_AddEvent(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var apartments = _repo.GetContext().Apartments.Where(q => q.ProjectId == ID.Value).ToList();
            ViewBag.Apartments = apartments;
            ViewBag.ProjectID = ID.Value;

            return PartialView("Modals/Modal_AddEvent");
        }

        [HttpPost]
        public ActionResult GetPartial_Modal_AddTask_Questions(int ProductID, int ProjectID, int? TaskID)
        {
            try
            {
                using (var db = new ACSystemContext())
                {
                    List<Question> result = new List<Question>();
                    List<Task_Question> taskQuestions = new List<Task_Question>();
                    List<int> taskQuestionIDs = new List<int>();
                    List<Answer> answers = new List<Answer>();


                    var data = db.Product_Project.FirstOrDefault(q => q.ProductID == ProductID && q.ProjectID == ProjectID);

                    if (data != null)
                    {
                        List<int> questionsID = data.Question_Product_Projects.Select(q => q.QuestionID).ToList();

                        result = db.Question.Where(q => questionsID.Contains(q.ID)).ToList();
                        answers = db.Answer.Where(q => questionsID.Contains(q.QuestionID)).ToList();
                    }

                    if (TaskID.HasValue)
                        taskQuestions = db.Task_Question.Where(q => q.TaskID == TaskID).ToList();


                    return PartialView("Partials/Modal_AddTask_Questions", new QuestionsToTask { Questions = result, Task_Questions = taskQuestions, Answers = answers, IsCreating = !TaskID.HasValue });
                }
            }
            catch(Exception ex)
            {
                Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        [HttpGet]
        public ActionResult Modal_AddPlaceError(int? projectID, int? apartmentID, int? taskID, string place)
        {
            Projects project = null;
            Apartments apartment = null;
            Tasks task = null;

            using (var db = new ACSystemContext())
            {

                switch (place)
                {
                    case "project":
                        if (!projectID.HasValue)
                            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                        project = db.Projects.Find(projectID);
                        break;
                    case "apartment":
                        if (!projectID.HasValue || !apartmentID.HasValue)
                            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                        project = db.Projects.Find(projectID);
                        apartment = db.Apartments.Find(apartmentID);
                        break;
                    case "task":
                        if (!projectID.HasValue || !apartmentID.HasValue || !taskID.HasValue)
                            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                        project = db.Projects.Find(projectID);
                        apartment = db.Apartments.Find(apartmentID);
                        task = db.Tasks.Find(taskID);
                        break;
                }
            }

            var model = new PlaceError
            {
                ProjectID = projectID,
                ApartmentID = apartmentID,
                TaskID = taskID,
                FitterID = User.Identity.GetUserId(),
                Date = DateTime.Now,
                Project = project,
                Apartment = apartment,
                Task = task
            };

            #region ViewBags

            ViewBag.Place = place;

            #endregion

            return PartialView("Modals/Modal_AddPlaceError", model);
        }

        [HttpPost]
        public ActionResult AddPlaceError(string _place, List<string> _photos, PlaceError _model)
        {
            if (_photos == null) _photos = new List<string>();

            _model.PlaceError_Photos = new List<PlaceError_Photo>();

            foreach (var p in _photos)
            {
                _model.PlaceError_Photos.Add(new PlaceError_Photo { Photo = p });
            }

            using (var db = new ACSystemContext())
            {
                db.PlaceError.Add(_model);

                try
                {
                    db.SaveChanges();

                    switch (_place)
                    {
                        case "project":
                            return Redirect($"/Projects/Details/{_model.ProjectID}");
                        case "apartment":
                            return Redirect($"/Projects/ApartmentDetails/{_model.ApartmentID}");
                        case "task":
                            return Redirect($"/Task/Details/{_model.TaskID}");
                        default:
                            return Redirect($"/Projects/Details/{_model.ProjectID}");
                    }
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        #endregion

        #region AJAX
        /// <summary>
        /// Funkcja wykorzystywana przez AJAX do pobrania listy projektów
        /// </summary>
        /// <param name="page">Aktualna strona tabeli</param>
        /// <param name="orderBy">Kolumna, po której ma być sortowana tabela</param>
        /// <param name="dest">Kierunek sortowania tabeli</param>
        /// <param name="search">Fraza przy pomocy której tabela zostaje przeszukiwana</param>
        /// <returns>Zwraca listę projektów</returns>
        [HttpGet]
        public ActionResult ProjectsList(int? page, string orderBy = "projectNumber", string dest = "asc", string search = "", string filter = "0,1,2,3")
        {
            var pageSize = 20;

            var rowsCount = _repo.Count();

            var pagesCount = rowsCount / pageSize;

            if (rowsCount <= pageSize || page <= 0 || page == null) page = 1;

            var excludeRows = (page - 1) * pageSize;

            var rows = 0;


            var daysRemind = _repo.GetContext().Settings.FirstOrDefault()?.ProjectEndReminder ?? 0;

            var list = _repo.List(out rows, excludeRows, pageSize, orderBy, dest, search, filter, daysRemind);

            //foreach (var l in list)
            //{
            //    if (DateTime.Now >= l.EndDate.AddDays(daysRemind * -1) && l.Status != ProjectStatus.Finished && l.Status != ProjectStatus.FinishedwithFaults)
            //        l.RemindBeforeEnd = true;
            //    else
            //        l.RemindBeforeEnd = false;

            //    if (DateTime.Now > l.EndDate && l.Status != ProjectStatus.Finished && l.Status != ProjectStatus.FinishedwithFaults)
            //        l.ProjectEnd = true;
            //    else
            //        l.ProjectEnd = false;
            //}

            pagesCount = (rows % pageSize == 0) ? rows / pageSize : (rows / pageSize) + 1;

            if (pagesCount <= 0) pagesCount = 1;

            return Json(new { data = list, pages = pagesCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ProjectManagersList(int customerId)
        {
            return Json(new { data = _repo.GetProjectManagersByCustomerId(customerId) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ProjectManagerById(int id)
        {
            var pm = _repo.ProjectManagerById(id);

            return Json(new { name = pm.Name, phonenumber = pm.PhoneNumber, email = pm.Email }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditTaskQuestion(int TaskId, string Type, TaskQuestionStatus Value)
        {
            var result = _repo.EditQuestion($"UPDATE Task_Questions SET {Type} = '{(int)Value}' WHERE QuestionId = {TaskId}");

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditTaskPatch(int PatchID, bool PatchStatus)
        {
            var result = _repo.EditPatch($"UPDATE TaskPatches SET Done = '{PatchStatus}' WHERE ID = {PatchID}");

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddFitterToTask(int? ApartmentID, int? TaskID, string FitterID)
        {
            if (!ApartmentID.HasValue || !TaskID.HasValue || string.IsNullOrEmpty(FitterID))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = _repo.FitterTaskGet(TaskID.Value, FitterID);

            if (result)
                return Redirect($"/Projects/ApartmentDetails/{ApartmentID.Value}");
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public ActionResult GrantAccessToTask(int? ApartmentID, int? TaskID, string FitterID)
        {
            if (!ApartmentID.HasValue || !TaskID.HasValue || string.IsNullOrEmpty(FitterID))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = _repo.AccessGrantToTask(TaskID.Value, FitterID);

            if (result)
                if (ApartmentID > 0)
                    return Redirect($"/Projects/ApartmentDetails/{ApartmentID.Value}");
                else
                    return Redirect($"/Task/Details/{TaskID}");
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public ActionResult ProjectAccess(int ProjectID, string FitterID)
        {
            var db = new ACSystemContext();

            var access = new Access_User_Project
            {
                UserID = FitterID,
                ProjectID = ProjectID
            };

            db.Access_User_Project.Add(access);

            try
            {
                db.SaveChanges();

                return Redirect("/Projects/Details/" + ProjectID);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult ProjectAccess_Delete(int ID)
        {
            var db = new ACSystemContext();

            var access = db.Access_User_Project.Find(ID);

            db.Access_User_Project.Remove(access);

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApartmentAccess(int ApartmentID, string FitterID)
        {
            var db = new ACSystemContext();

            var access = new Access_User_Apartment
            {
                UserID = FitterID,
                ApartmentID = ApartmentID
            };

            db.Access_User_Apartment.Add(access);

            try
            {
                db.SaveChanges();

                return Redirect("/Projects/ApartmentDetails/" + ApartmentID);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult ApartmentAccess_Delete(int ID)
        {
            var db = new ACSystemContext();

            var access = db.Access_User_Apartment.Find(ID);

            db.Access_User_Apartment.Remove(access);

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult TaskAccess(int TaskID, string FitterID)
        {
            var db = new ACSystemContext();

            var access = new Access_User_Task
            {
                UserID = FitterID,
                TaskID = TaskID
            };

            db.Access_User_Task.Add(access);

            try
            {
                db.SaveChanges();

                return Redirect("/Task/Details/" + TaskID);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult TaskAccess_Delete(int ID)
        {
            var db = new ACSystemContext();

            var access = db.Access_User_Task.Find(ID);

            db.Access_User_Task.Remove(access);

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TaskTodo(TodoType? type, string filter = "", string apartmentOrder = "desc", string dest = "asc")
        {
            var db = new ACSystemContext();
            var model = new TaskTodoViewModel();
            ViewBag.ApartmentOrder = apartmentOrder;
            ViewBag.LastType = type;

            var finalTodoList = new List<Todo>();

            List<TodoListViewModel> todoList = db.Todo.Where(q => q.Checked == false && (q.Task != null || q.Type == TodoType.Wiadomosc || q.Type == TodoType.NowyBlad)).Select(q => new TodoListViewModel()
            {
                ProjectId = (q.Task == null) ? (q.PlaceError.ProjectID) : (q.Task.Apartment.ProjectId),
                ApartmentId = (q.Task == null) ? (q.PlaceError.ApartmentID) : (q.Task.ApartmentId),
                TaskId = q.Task.Id,
                Error = q.ErrorType,
                ErrorId = q.PlaceErrorID,
                Project = (q.Task == null) ? q.PlaceError.Project.ProjectNumber : q.Task.Apartment.Project.ProjectNumber,
                AssignedUser = (q.AssignedUser.Name) + " " + q.AssignedUser.Surname,
                Apartment = (q.Task == null) ? (q.PlaceError.Apartment == null) ? "" : (q.PlaceError.Apartment.Letter) + q.PlaceError.Apartment.NumberOfApartment : (q.Task.Apartment.Letter + q.Task.Apartment.NumberOfApartment),
                //Apartment = (q.Task == null) ? (q.PlaceError.Apartment.Letter) + q.PlaceError.Apartment.NumberOfApartment : (q.Task.Apartment.Letter + q.Task.Apartment.NumberOfApartment),
                //Task = (q.Task == null) ? (q.PlaceError.Task == null) ? "" : q.PlaceError.Task.GetTaskName(q.PlaceError.ProjectID.Value) + q.PlaceError.Task.TaskSufix : q.Task.GetTaskName(q.Task.Apartment.ProjectId) + q.Task.TaskSufix,
                //Task = (q.Task == null) ? q.PlaceError.Task.GetTaskName(q.Apartment.ProjectId) + q.PlaceError.Task.TaskSufix : (q.Task.GetTaskName(q.Apartment.ProjectId) + q.Task.TaskSufix),
                TaskModel = q.Task,
                PlaceError = q.PlaceError,
                Fitter = (q.Fitter.User.Name) + (q.Fitter.User.Surname),
                Type = q.Type,
                Date = q.Date,
                IsTaskAccepted = q.Task.Accepted,
                TodoId = q.ID

            }).ToList();

            todoList.ForEach(q =>
            {
                q.Task = (q.TaskModel == null) ? (q.PlaceError == null || q.PlaceError.Task == null) ? "" : q.PlaceError.Task.GetTaskName(q.PlaceError.ProjectID.Value) + q.PlaceError.Task.TaskSufix : q.TaskModel.GetTaskName(q.TaskModel.Apartment.ProjectId) + q.TaskModel.TaskSufix;
            });

            var sortedTodoList = new List<TodoListViewModel>();

            if (!filter.IsNullOrEmpty())
            {
                switch (filter)
                {
                    case "project":
                        if (dest != "asc")
                        {
                            sortedTodoList = todoList.OrderByDescending(q => q.Project).ToList();
                            ViewBag.Dest = "asc";
                        }
                        else
                        {
                            sortedTodoList = todoList.OrderBy(q => q.Project).ToList();
                            ViewBag.Dest = "desc";

                        }
                        ViewBag.LastFilter = filter;
                        return View(sortedTodoList);
                    case "username":
                        if (dest != "desc")
                        {
                            sortedTodoList = todoList.OrderByDescending(q => q.AssignedUser).ToList();
                            ViewBag.Dest = "desc";
                        }
                        else
                        {
                            sortedTodoList = todoList.OrderBy(q => q.AssignedUser).ToList();
                            ViewBag.Dest = "asc";
                        }
                        ViewBag.LastFilter = filter;

                        return View(sortedTodoList);
                    case "task":
                        if (dest != "desc")
                        {
                            sortedTodoList = todoList.OrderByDescending(q => q.Task).ToList();
                            ViewBag.Dest = "desc";
                        }
                        else
                        {
                            sortedTodoList = todoList.OrderBy(q => q.Task).ToList();
                            ViewBag.Dest = "asc";
                        }
                        ViewBag.LastFilter = filter;
                        return View(sortedTodoList);
                    case "fitter":
                        if (dest != "desc")
                        {
                            sortedTodoList = todoList.OrderByDescending(q => q.Fitter).ToList();
                            ViewBag.Dest = "desc";
                        }
                        else
                        {
                            sortedTodoList = todoList.OrderBy(q => q.Fitter).ToList();
                            ViewBag.Dest = "asc";
                        }
                        ViewBag.LastFilter = filter;
                        return View(sortedTodoList);
                    case "date":
                        if (dest != "desc")
                        {
                            sortedTodoList = todoList.OrderBy(q => q.Date).ToList();
                            ViewBag.Dest = "desc";

                        }
                        else
                        {
                            sortedTodoList = todoList.OrderByDescending(q => q.Date).ToList();
                            ViewBag.Dest = "asc";

                        }
                        ViewBag.LastFilter = filter;
                        return View(sortedTodoList);
                    case "apartment":
                        if (dest != "asc")
                        {
                            sortedTodoList = todoList.OrderByDescending(q => q.Apartment.Length).ThenByDescending(q => q.Apartment).ToList();
                            ViewBag.Dest = "asc";
                        }
                        else
                        {
                            sortedTodoList = todoList.OrderBy(q => q.Apartment.Length).ThenBy(q => q.Apartment).ToList();
                            ViewBag.Dest = "desc";
                        }
                        ViewBag.LastFilter = filter;
                        return View(sortedTodoList);
                    default:
                        break;
                }

            }

            if (type != null)
            {
                foreach (var t in todoList)
                {
                    if (t.Type == type)
                    {
                        sortedTodoList.Insert(0, t);
                    }
                }
            }
            else
            {
                sortedTodoList = todoList.ToList();
            }
            return View(sortedTodoList);
        }

        [HttpGet]
        public ActionResult TaskTodoJson(int? page, string orderBy = "name", string dest = "asc", string search = "", string status = "3,4")
        {
            var pageSize = 20;

            var rowsCount = _repo.TaskTodoCount();

            var pagesCount = rowsCount / pageSize;

            if (rowsCount <= pageSize || page <= 0 || page == null) page = 1;

            var excludeRows = (page - 1) * pageSize;

            var rows = 0;

            var list = _repo.TaskTodo(out rows, excludeRows, pageSize, orderBy, dest, search, status);

            pagesCount = rows / pageSize;

            if (pagesCount <= 0) pagesCount = 1;

            return Json(new { data = list, pages = pagesCount }, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult Modal_NewTodo()
        {
            var db = new ACSystemContext();
            var model = new NewToDoViewModel();
            model.Fitters = db.Fitters.OrderBy(q => q.User.Id).ToList();

            var users = db.ACSystemUser.Where(q => q.Id != null).ToList();

            List<ACSystemUser> admins = new List<ACSystemUser>();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var adminRole = roleManager.Roles.FirstOrDefault(q => q.Name == "Admin");
            foreach ( var u in users)
            {
                if(u.Roles.Any(q => q.RoleId == adminRole.Id))
                {
                    admins.Add(u);
                }
            }
            model.Users = admins;
            return PartialView("~/Views/Popup/Todo/Modal_NewTodo.cshtml", model);
        }

        public void CheckTodo(int todoid)
        {
            var db = new ACSystemContext();

            Todo todotochange = db.Todo.FirstOrDefault(q => q.ID == todoid);

            if (todotochange != null)
            {
                todotochange.Checked = true;
                }
            }

        [HttpGet]
        public JsonResult NewTodo(string assignedUserId, string message)
        {
            var newTodo = new Todo
            {
                TaskID = null,
                Message = message,
                FitterID = assignedUserId,
                Type = (TodoType)6,
                Date = DateTime.Now,
                Checked = false,
                AssignedUserId = assignedUserId
            };
            var db = new ACSystemContext();
            db.Todo.Add(newTodo);

                db.SaveChanges();
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);


        }

        public PartialViewResult _NewTodoForm()
        {
            var todoService = new TodoService();
            var model = todoService.CreateToDoFormViewModel();
            return PartialView("~/Views/Projects/Partials/_NewTodoForm.cshtml", model);
        }

        public PartialViewResult ShowTodoMessage(int ID)
        {
            var db = new ACSystemContext();
            var model = db.Todo.FirstOrDefault(x => x.ID == ID);
            return PartialView("~/Views/Popup/Todo/Modal_TodoMessage.cshtml", model);

        }

        public JsonResult SaveTodoMessage(int todoID, string MessageContent)
        {
            var db = new ACSystemContext();
            var todoToEdit = db.Todo.FirstOrDefault(q => q.ID == todoID);
            if (todoToEdit != null)
            {
                todoToEdit.Message = MessageContent;
                db.SaveChanges();
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Modal_AssignTodoAdmin(int ID)
        {
            var db = new ACSystemContext();
            var model = new AssignUserViewModel();
            var users = db.ACSystemUser.Where(q => q.Id != null).ToList();

            List<ACSystemUser> admins = new List<ACSystemUser>();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var adminRole = roleManager.Roles.FirstOrDefault(q => q.Name == "Admin");
            foreach (var u in users)
            {
                if (u.Roles.Any(q => q.RoleId == adminRole.Id))
                {
                    admins.Add(u);
                }
            }
            model.UsersList = admins;
            var todo = db.Todo.FirstOrDefault(q => q.ID == ID);
            model.Todo = todo;
            model.UsersList = admins;
            return PartialView("~/Views/Popup/Todo/Modal_AssignTodoAdmin.cshtml", model);
        }

        public void AssignTodoAdmin(int todoId, string adminId)
        {
            var db = new ACSystemContext();
            Todo todoToEdit = db.Todo.FirstOrDefault(q => q.ID == todoId);
            todoToEdit.AssignedUserId = adminId;

            db.SaveChanges();
        }

        [HttpPost]
        public ActionResult TodoCheck(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = _repo.TodoCheck(ID.Value);

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TaskAcceptFromTodo(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = false;

            var db = new ACSystemContext();

            var todo = db.Todo.Find(ID.Value);
            var task = todo.Task;

            todo.Checked = true;
            task.Accepted = true;

            db.Entry(todo).State = EntityState.Modified;
            db.Entry(task).State = EntityState.Modified;

            try
            {
                db.SaveChanges();

                result = true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                result = false;
            }

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MakeFillTaskFromTodo(int? ID) {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            bool result = false;

            using (ACSystemContext dtx = new ACSystemContext()) {
                Todo todo = dtx.Todo.Find(ID);

                if (todo == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                Tasks task = dtx.Tasks.Find(todo.TaskID);

                if (task == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                decimal taskCount = task.Task_Fitter.Where(q => q.IsActive && q.Status != TaskStatus.Free && q.Status != TaskStatus.UnderWork).Sum(q => q.WorkDone);
                decimal taskLeft = task.ProductCount - taskCount;

                if (taskLeft <= 0)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Ilość do uzupełnienia musi być większa od zera.");

                task.ProductCount = taskCount;
                task.Status = TaskStatus.Finished;
                dtx.Entry(task).State = EntityState.Modified;

                Tasks newTask = new Tasks {
                    ApartmentId = task.ApartmentId,
                    ProductId = task.ProductId,
                    ProductPrice = task.ProductPrice,
                    ProductCount = taskLeft,
                    Status = Core.TaskStatus.Free,
                    PercentType1 = task.PercentType1,
                    PercentType2 = task.PercentType2,
                    PercentType3 = task.PercentType3,
                    PercentType4 = task.PercentType4,
                    PercentType5 = task.PercentType5,
                    PercentType6 = task.PercentType6,
                    StartDate = task.StartDate,
                    EndDate = task.EndDate,
                    TaskSufix = task.TaskSufix,
                    DistancePrice = task.DistancePrice,
                    IsHourlyPay = task.IsHourlyPay,
                    HourlyPrice = task.HourlyPrice,
                    Accepted = task.Accepted,
                    Description = task.Description,
                    ForceInvoice = task.ForceInvoice,
                    Production = task.Production
                };
                dtx.Tasks.Add(newTask);

                todo.Checked = true;
                dtx.Entry(todo).State = EntityState.Modified;

                try {
                    dtx.SaveChanges();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                } catch (Exception ex) {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, HttpContext.User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Błąd wykonywania skryptu bazy danych.");
                }
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChangeProjectDescription(int projectID, string description)
        {
            var db = _repo.GetContext();

            var project = db.Projects.Find(projectID);

            if (project == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            project.Description = description;
            db.Entry(project).State = EntityState.Modified;;

            try
            {
                db.SaveChanges();

                return Json(new {success = true}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new {success = false}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChangeProjectInvoiceDescription(int projectID, string description)
        {
            var db = _repo.GetContext();

            var project = db.Projects.Find(projectID);

            if (project == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            project.InvoiceDescription = description;
            db.Entry(project).State = EntityState.Modified; ;

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChangeApartmentDescription(int apartmentID, string description)
        {
            var db = _repo.GetContext();

            var apartment = db.Apartments.Find(apartmentID);

            if (apartment == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            apartment.Description = description;
            db.Entry(apartment).State = EntityState.Modified; ;

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChangeApartmentInvoiceDescription(int apartmentID, string description)
        {
            var db = _repo.GetContext();

            var apartment = db.Apartments.Find(apartmentID);

            if (apartment == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            apartment.InvoiceDescription = description;
            db.Entry(apartment).State = EntityState.Modified; ;

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddProjectManager(int customerID, string name, string email, string phonenumber)
        {
            var db = _repo.GetContext();

            var pm = new ProjectManagers
            {
                CustomerId = customerID,
                Name = name,
                Email = email,
                PhoneNumber = phonenumber
            };

            db.ProjectManagers.Add(pm);

            try
            {
                db.SaveChanges();

                return Json(new {success = true, pmID = pm.Id, pmName = pm.Name}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new {success = false}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadTaskDocument(int TaskID)
        {
            if (Request.Files.Count > 0)
            {
                var db = _repo.GetContext();

                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var fi = new FileInfo(fileName);
                        var ext = fi.Extension.ToUpper();
                        newFileName += fi.Extension;

                        var filePath = Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName);
                        file.SaveAs(filePath);

                        if (ext == ".JPG" || ext == ".JPEG" || ext == ".PNG" || ext == ".BMP")
                        {
                            newFileName = "resized_" + newFileName;

                            using (var image = Image.FromFile(filePath))
                            using (var newImage = Images.ScaleImage(image, 600, 800))
                            {
                                image.Dispose();
                                switch (ext)
                                {
                                    case ".JPG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".JPEG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".PNG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Png);
                                        break;
                                    case ".BMP":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Bmp);
                                        break;
                                }
                            }

                            System.IO.File.Delete(filePath);
                        }

                        var doc = new TaskDocuments
                        {
                            TaskID = TaskID,
                            FileName = fileName,
                            FilePath = newFileName
                        };

                        db.TaskDocuments.Add(doc);
                    }

                    db.SaveChanges();

                    return Json(new { result = "true", fileName = fileName, newFileName = newFileName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = "false", message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadProjectDocument(int ProjectID)
        {
            if (Request.Files.Count > 0)
            {
                var db = _repo.GetContext();

                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var fi = new FileInfo(fileName);
                        var ext = fi.Extension.ToUpper();
                        newFileName += fi.Extension;

                        var filePath = Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName);
                        file.SaveAs(filePath);

                        if (ext == ".JPG" || ext == ".JPEG" || ext == ".PNG" || ext == ".BMP")
                        {
                            newFileName = "resized_" + newFileName;

                            using (var image = Image.FromFile(filePath))
                            using (var newImage = Images.ScaleImage(image, 600, 800))
                            {
                                image.Dispose();
                                switch (ext)
                                {
                                    case ".JPG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".JPEG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".PNG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Png);
                                        break;
                                    case ".BMP":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Bmp);
                                        break;
                                }
                            }

                            System.IO.File.Delete(filePath);
                        }

                        var doc = new ProjectDocuments
                        {
                            ProjectID = ProjectID,
                            FileName = fileName,
                            FilePath = newFileName
                        };

                        db.ProjectDocuments.Add(doc);
                    }

                    db.SaveChanges();

                    return Json(new { result = "true", fileName = fileName, newFileName = newFileName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = "false", message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult TaskCommentsSetViewed(int ID)
        {
            var db = _repo.GetContext();

            var comments = db.TaskComment.Where(q => q.ApartmentID == ID).ToList();

            foreach(var c in comments)
            {
                c.IsViewed = true;

                var todoList = c.Task.Todos.Where(q => q.Type == TodoType.NowyKomentarz).ToList();

                foreach (var todo in todoList)
                {
                    todo.Checked = true;

                    db.Entry(todo).State = EntityState.Modified;
                }

                db.Entry(c).State = EntityState.Modified;
            }

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SetCommentToDo(int ID)
        {
            var db = _repo.GetContext();

            var comment = db.TaskComment.Find(ID);

            var allComments = db.TaskComment.Where(q => q.ApartmentID == comment.ApartmentID).ToList();

            var haveTodo = false;

            foreach (var comm in allComments)
            {
                if (haveTodo)
                    continue;

                if (comm.Task.Todos.Any(q => q.Type == TodoType.NowyKomentarz && q.Checked == false))
                    haveTodo = true;
            }

            if (haveTodo)
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);

            var newTodo = new Todo
            {
                TaskID = comment.TaskID,
                FitterID = User.Identity.GetUserId(),
                Type = TodoType.NowyKomentarz,
                Date = DateTime.Now,
                Checked = false
            };

            db.Todo.Add(newTodo);

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult SetTaskToForceInvoice(int ID, bool set = false)
        {
            var db = _repo.GetContext();

            var task = db.Tasks.Find(ID);

            task.ForceInvoice = set;

            db.Entry(task).State = EntityState.Modified;

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CheckProjectID(string ID, int ProjectID)
        {
            var db = _repo.GetContext();

            var isIDExist = db.Projects.Any(q => q.InSystemID == ID && q.Id != ProjectID);

            return Json(new { exist = isIDExist }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangeProjectCheck(int ProjectID, bool IsChecked)
        {
            var db = _repo.GetContext();

            var project = db.Projects.Find(ProjectID);

            project.IsChecked = IsChecked;

            db.Entry(project).State = EntityState.Modified;

            var user = db.ACSystemUser.Find(User.Identity.GetUserId());
            var activityMessage = (IsChecked) ? $"Użytkownik {user.FullName} zmienił stan projektu na SPRAWDZONY." : $"Użytkownik {user.FullName} zmienił stan projektu na NIE SPRAWDZONY";

            var activity = new Project_Activity
            {
                ProjectID = ProjectID,
                UserID = User.Identity.GetUserId(),
                Message = activityMessage,
                Timestamp = DateTime.Now
            };

            db.Project_Activity.Add(activity);

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult BlockPhoto(bool isBlocked, string source, int id)
        {
            var context = _repo.GetContext();

            switch (source)
            {
                case "Question_Images":
                    {
                        var photo = context.TaskQuestionPhoto.Find(id);

                        photo.Blocked = !isBlocked;
                        context.Entry(photo).State = EntityState.Modified;
                    }
                    break;
                case "Patch_Images":
                    {
                        var photo = context.TaskPatches.Find(id);

                        photo.PhotoBlocked = !isBlocked;
                        context.Entry(photo).State = EntityState.Modified;
                    }
                    break;
                case "Task_Images":
                    {
                        var photo = context.TaskActivityPhotos.Find(id);

                        photo.Blocked = !isBlocked;
                        context.Entry(photo).State = EntityState.Modified;
                    }
                    break;
            }

            try
            {
                context.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeletePhoto(string source, int id)
        {
            var context = _repo.GetContext();

            switch (source)
            {
                case "Question_Images":
                    {
                        var photo = context.TaskQuestionPhoto.Find(id);
                        var photoPath = Server.MapPath($"~/Monter/dist/Question_Images/{photo.Photo}");

                        if (System.IO.File.Exists(photoPath))
                            System.IO.File.Delete(photoPath);

                        context.TaskQuestionPhoto.Remove(photo);
                    }
                    break;
                case "Patch_Images":
                    {
                        var photo = context.TaskPatches.Find(id);
                        var photoPath = Server.MapPath($"~/Monter/dist/Patch_Images/{photo.Photo}");

                        if (System.IO.File.Exists(photoPath))
                            System.IO.File.Delete(photoPath);

                        photo.Photo = "";
                        photo.PhotoBlocked = false;

                        context.Entry(photo).State = EntityState.Modified;
                    }
                    break;
                case "Task_Images":
                    {
                        var photo = context.TaskActivityPhotos.Find(id);
                        var photoPath = Server.MapPath($"~/Monter/dist/Task_Images/{photo.Photo}");

                        if (System.IO.File.Exists(photoPath))
                            System.IO.File.Delete(photoPath);

                        context.TaskActivityPhotos.Remove(photo);
                    }
                    break;
            }

            try
            {
                context.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveQuestionTranslation(int taskID, int descID, string question, string descPL, string descSE)
        {
            var context = _repo.GetContext();

            var model = new TaskQuestionPhoto()
            {
                Task_QuestionsID = taskID,
                Question = question,
                InService = false
            };

            if (descID > 0)
                model = context.TaskQuestionPhoto.Find(descID);

            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            model.Description = descPL;
            model.TranslatedDescription = descSE;

            if (descID > 0)
                context.Entry(model).State = EntityState.Modified;
            else
                context.TaskQuestionPhoto.Add(model);

            try
            {
                context.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult HandyFinishTask(int TaskID, TaskStatus Status, decimal CountToGo, string BackTo = "apartment")
        {
            var context = _repo.GetContext();

            var task = context.Tasks.Find(TaskID);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var _form = HttpContext.Request.Form;

            foreach (var f in _form.AllKeys)
            {
                var field = f.Split('_');

                if (!field[0].Equals("work-done"))
                    continue;

                if (_form[f].Equals("0"))
                    continue;

                decimal workDone = 0;
                decimal workTime = 0;

                if (decimal.TryParse(_form[f].Replace('.',','), out workDone) == false)
                    continue;

                CountToGo -= workDone;

                if (decimal.TryParse(_form["work-time_" + field[1]].Replace('.', ','), out workTime) == false)
                    continue;

                var task_fitter = new Task_Fitter
                {
                    TaskId = TaskID,
                    FitterId = field[1],
                    WorkDone = workDone,
                    WorkTime = (int)Math.Ceiling(workTime * 60),
                    Status = Status,
                    Timestamp = DateTime.Now,
                    InPayroll = false,
                    IsActive = true,
                    DayChecked = false
                };

                context.Task_Fitter.Add(task_fitter);
            }

            if (CountToGo > 0)
            {
                var task_fitter = new Task_Fitter
                {
                    TaskId = TaskID,
                    FitterId = User.Identity.GetUserId(),
                    WorkDone = CountToGo,
                    WorkTime = 0,
                    Status = Status,
                    Timestamp = DateTime.Now,
                    InPayroll = false,
                    IsActive = true,
                    DayChecked = false
                };

                context.Task_Fitter.Add(task_fitter);
            }

            task.Status = Status;
            task.ClosedDate = DateTime.Now;

            #region CHANGE PROJECT STATUS

            if (task.Apartment == null)
                task.Apartment = context.Apartments.Find(task.ApartmentId);

            var projectID = task.Apartment.ProjectId;

            var isAnyNotFinished = context.Tasks.Any(q => q.Apartment.ProjectId == projectID && q.Id != task.Id && (q.Status == TaskStatus.Free || q.Status == TaskStatus.Stoped || q.Status == TaskStatus.UnderWork || q.Status == TaskStatus.Planned));

            if (isAnyNotFinished == false && task.Status != TaskStatus.Stoped && task.Status != TaskStatus.Free && task.Status != TaskStatus.UnderWork)
            {
                var project = context.Projects.Find(projectID);

                if (context.Tasks.Any(q => q.Apartment.ProjectId == projectID && q.Id != task.Id && (q.Status == TaskStatus.FinishedWithFaults || q.Status == TaskStatus.PlaceNotReady || task.Status == TaskStatus.FinishedWithFaults || task.Status == TaskStatus.PlaceNotReady)))
                {
                    project.Status = ProjectStatus.FinishedWithFaults;
                }
                else
                {
                    project.Status = ProjectStatus.Finished;
                }

                context.Entry(project).State = EntityState.Modified;
            }
            else if (isAnyNotFinished == false && (task.Status == TaskStatus.Free || task.Status == TaskStatus.Stoped || task.Status == TaskStatus.UnderWork))
            {
                var project = context.Projects.Find(projectID);

                project.Status = ProjectStatus.UnderWork;

                context.Entry(project).State = EntityState.Modified;
            }

            #endregion

            context.Entry(task).State = EntityState.Modified;

            try
            {
                context.SaveChanges();

                if (BackTo == "apartment")
                    return Redirect("/Projects/ApartmentDetails/" + task.ApartmentId);
                else
                    return Redirect("/Task/Details/" + task.Id);
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "HandyFinishTask",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                context.ExceptionsLog.Add(log);

                context.SaveChanges();

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


        }

        [HttpPost]
        public ActionResult FillTask(int ID)
        {
            var context = _repo.GetContext();

            var task = context.Tasks.Find(ID);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var workDone = task.Task_Fitter.Where(q => q.IsActive).Sum(q => q.WorkDone);

            task.ProductCount = workDone;
            context.Entry(task).State = EntityState.Modified;

            var left = $"0 z {task.ProductCount} {task.Product.Jm}";

            try
            {
                context.SaveChanges();

                return Json(new { success = true, left = left }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "FillTask",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                context.ExceptionsLog.Add(log);

                context.SaveChanges();

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ConnectProjectWithProduct(int ProjectID, int ProductID, decimal Price, string productName, decimal? PercentType1, decimal? PercentType2, string invoice, bool NoExtra)
        {
            var db = _repo.GetContext();

            var projectNumber = db.Projects.Find(ProjectID)?.ProjectNumber ?? "";
            var productType = "";

            var product = db.Products.Find(ProductID);

            if (product != null)
            {
                if (string.IsNullOrEmpty(productName))
                {
                    productName = product.Name;
                }

                switch (product.ProductType)
                {
                    case TaskType.Construction:
                        productType = "Budowlanka";
                        break;
                    case TaskType.Kitchen:
                        productType = "Kuchnia";
                        break;
                }
            }

            List<Question_Product_Project> qpp = product.Product_Questions.Select(x => new Question_Product_Project {
                QuestionID = x.QuestionID
            }).ToList();

            var pp = new Product_Project
            {
                ProjectID = ProjectID,
                ProjectNumber = projectNumber,
                ProductID = ProductID,
                ProductName = productName,
                Price = Price,
                PercentType1 = (PercentType1.HasValue) ? PercentType1.Value : 0,
                PercentType2 = (PercentType2.HasValue) ? PercentType2.Value : 0,
                OnInvoiceName = invoice,
                Question_Product_Projects = qpp,
                NoExtra = NoExtra
            };

            db.Product_Project.Add(pp);

            try
            {
                db.SaveChanges();

                return Json(new { success = true, ConnectID = pp.ID, ProjectID, ProductID, ProductName = productName, Price, ProjectName = projectNumber, ProductType = productType, pp.PercentType1, pp.PercentType2, pp.OnInvoiceName, pp.NoExtra }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "ConnectProjectWithProduct",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteConnectWithProjectAndProduct(int ConnectID)
        {
            var db = _repo.GetContext();

            var connect = db.Product_Project.Find(ConnectID);

            db.Product_Project.Remove(connect);

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "DeleteConnectWithProjectAndProduct",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public string AJAX_EditableActivityWorkDone(string field, string value)
        {
            var sField = field.Split('-')[0];

            var ID = 0;

            int.TryParse(field.Split('-')[1], out ID);

            if (string.IsNullOrEmpty(sField)) return "false;Nazwa pola nie może być pusta";
            if (ID == 0) return "false;Nie podano ID aktywności";

            var db = new ACSystemContext();

            var activity = db.Task_Fitter.Find(ID);

            if (activity == null) return "false;Nie znaleziono podanej aktywności";

            switch (sField)
            {
                case "activityWorkDone":
                    decimal workDone = 0;
                    if (decimal.TryParse(value, out workDone))
                    {
                        activity.WorkDone = workDone;
                        db.Entry(activity).State = EntityState.Modified;
                    }
                    else
                        return "false;Podana wartość jest nieprawidłowa!";
                    break;
                case "activityWorkTime":
                    decimal workTime = 0;
                    if (decimal.TryParse(value, out workTime))
                    {
                        activity.WorkTime = (int)(workTime * 60);
                        db.Entry(activity).State = EntityState.Modified;
                    }
                    else
                        return "false;Podana wartość jest nieprawidłowa!";
                    break;
                default:
                    return "false;Błędna nazwa pola";
            }

            try
            {
                db.SaveChanges();
                return $"true;{value}";
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "Edit activity", User.Identity.Name);
                return "false;Wystąpił nieoczekiwany błąd.";
            }
        }

        [HttpPost]
        public string AJAX_EditableProductPrice(string field, string value)
        {
            var sField = field.Split('-')[0];

            var ID = 0;

            int.TryParse(field.Split('-')[1], out ID);

            if (string.IsNullOrEmpty(sField)) return "false;Nazwa pola nie może być pusta";
            if (ID == 0) return "false;Nie podano ID powiązania.";

            var db = new ACSystemContext();

            var connect = db.Product_Project.Find(ID);

            if (connect == null) return "false;Nie znaleziono podanego powiązania";

            decimal price = 0;
            if (decimal.TryParse(value, out price))
            {
                connect.Price = price;
                db.Entry(connect).State = EntityState.Modified;
            }
            else
                return "false;Podana wartość jest nieprawidłowa!";

            try
            {
                db.SaveChanges();
                return $"true;{value}";
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "Edit price", User.Identity.Name);
                return "false;Wystąpił nieoczekiwany błąd.";
            }
        }

        [HttpPost]
        [Obsolete]
        public ActionResult AJAX_GroupReportsList(string search, int lgh)
        {
            var db = _repo.GetContext();
            var apartment = db.Apartments.Find(lgh);

            DateTime? date = null;
            DateTime tryDate = DateTime.Now;

            if (DateTime.TryParse(search, out tryDate))
                date = tryDate;

            var lghNumber = $"LGH{apartment.Letter}{apartment.NumberOfApartment}/";

            var list = db.ReportNag.Where(q => q.ApartmentID == lgh && ((lghNumber + q.Number).Contains(search) || q.Project.InSystemID.Contains(search) || q.Project.ProjectNumber.Contains(search) || (date.HasValue && EntityFunctions.TruncateTime(q.Date) == EntityFunctions.TruncateTime(date)))).ToList();
            var returnList = list.Select(q => new
                {
                    ID = q.ID,
                    Type = q.Type,
                    LGH = lghNumber,
                    Number = q.Number,
                    Data = q.Date.ToString("yyyy-MM-dd"),
                    ProjectInSystemID = q.Project.InSystemID,
                    ProjectNumber = q.Project.ProjectNumber,
                    Status = q.Status,
                    Precursor = (q.PrecursorReport != null) ? q.PrecursorReport.Number : -1
                })
                .ToList();

            return Json(new { list = returnList }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Obsolete]
        public ActionResult AJAX_GroupProjectReportsList(string search, int id)
        {
            var db = _repo.GetContext();
            var project = db.Projects.Find(id);

            DateTime? date = null;
            DateTime tryDate = DateTime.Now;

            if (DateTime.TryParse(search, out tryDate))
                date = tryDate;

            var list = db.ReportNag.Where(q => q.ProjectID == id && (("LGH" + q.Apartment.Letter + q.Apartment.NumberOfApartment + "/" + q.Number).Contains(search) || q.Project.InSystemID.Contains(search) || q.Project.ProjectNumber.Contains(search) || (date.HasValue && EntityFunctions.TruncateTime(q.Date) == EntityFunctions.TruncateTime(date)))).ToList();
            var returnList = list.Select(q => new
            {
                ID = q.ID,
                Type = q.Type,
                LGH = $"LGH{q.Apartment.Letter}{q.Apartment.NumberOfApartment}/",
                LGHNumber = $"{q.Apartment.Letter}{q.Apartment.NumberOfApartment}",
                Number = q.Number,
                Data = q.Date.ToString("yyyy-MM-dd"),
                Status = q.Status,
                Precursor = (q.PrecursorReport != null) ? q.PrecursorReport.Number : -1
            })
                .ToList();

            return Json(new { list = returnList }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AJAX_DeleteFile(int ID, string URL, string From)
        {
            if (ID > 0)
            {
                if (From == "project")
                {
                    var dbFile = _repo.GetContext().ProjectDocuments.Find(ID);

                    _repo.GetContext().ProjectDocuments.Remove(dbFile);
                }
                else if (From == "task")
                {
                    var dbFile = _repo.GetContext().TaskDocuments.Find(ID);

                    _repo.GetContext().TaskDocuments.Remove(dbFile);
                }

                try
                {
                    _repo.SaveChanges();
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "AJAX_DeleteFile", User.Identity.Name);

                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }

            var filePath = Server.MapPath("~" + URL);

            if (System.IO.File.Exists(filePath))
            {
                try
                {
                    System.IO.File.Delete(filePath);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "AJAX_DeleteFile", User.Identity.Name);
                }
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AJAX_DeleteSelectedProjectProducts(int[] ProjectProductIDs)
        {
            var projectProducts = _repo.GetContext().Product_Project.Where(q => ProjectProductIDs.Contains(q.ID));
            _repo.GetContext().Product_Project.RemoveRange(projectProducts);

            try
            {
                _repo.GetContext().SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "AJAX_DeleteSelectedProjectProducts", User.Identity.Name);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult PlanProductProject(int ProjectID, int ProductID, decimal Count)
        {
            var db = new ACSystemContext();

            var plan = new Product_Plan
            {
                ProjectID = ProjectID,
                ProductID = ProductID,
                Count = Count
            };

            db.Product_Plan.Add(plan);

            try
            {
                db.SaveChanges();

                var workDoneList = db.Task_Fitter.Where(q => q.Task.Apartment.ProjectId == ProjectID && q.Task.ProductId == ProductID && q.IsActive);
                var workDone = (workDoneList.Count() > 0) ? workDoneList.Sum(q => q.WorkDone) : 0;
                var workLeft = Count - workDone;
                var product = db.Products.Find(ProductID);

                return Json(new { success = true, PlanID = plan.PlanID, ProductID = ProductID, ProductName = product.Name, Jm = product.Jm, PlanCount = Count, PlanDone = workDone, PlanLeft = workLeft }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeletePlanProjectProduct(int PlanID)
        {
            var db = _repo.GetContext();

            var plan = db.Product_Plan.Find(PlanID);

            db.Product_Plan.Remove(plan);

            try
            {
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EditConnectProductPrice(int ConnectID, decimal Price, string name, decimal? PercentType1, decimal? PercentType2, string OnInvoiceName, bool NoExtra)
        {
            using (var db = new ACSystemContext())
            {
                var connect = db.Product_Project.Find(ConnectID);

                if (connect == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                connect.Price = Price;
                connect.PercentType1 = (PercentType1.HasValue) ? PercentType1 : 0;
                connect.PercentType2 = (PercentType2.HasValue) ? PercentType2 : 0;
                connect.ProductName = name;
                connect.OnInvoiceName = OnInvoiceName;
                connect.NoExtra = NoExtra;

                db.Entry(connect).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        [HttpPost]
        public void EditPlanCount(int pk, decimal value)
        {
            var plan = _repo.GetContext().Product_Plan.Find(pk);

            plan.Count = value;

            _repo.GetContext().Entry(plan).State = EntityState.Modified;

            try
            {
                _repo.SaveChanges();
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, HttpContext.Request.Url.AbsoluteUri, User.Identity.Name);
            }
        }

        [HttpGet]
        public void ChangeQuestionDescriptionState(int ID)
        {
            var db = new ACSystemContext();

            var desc = db.TaskQuestionPhoto.Find(ID);

            desc.InService = !desc.InService;

            db.Entry(desc).State = EntityState.Modified;
            db.SaveChanges();
        }

        [HttpGet]
        public ActionResult SendPlaceErrorReport(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var uri = new Uri(Request.Url.AbsoluteUri).GetServerUrl();
            var pdfCreated = HttpRequests.RequestUrl_WithSuccess(uri + "/Reports/PlaceError_ClientReport?_placeErrorID=" + ID);
            var send = false;
            var _exception = "";

            using (var db = new ACSystemContext())
            {
                var error = db.PlaceError.Find(ID);

                if (error == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                if (pdfCreated)
                {
                    var emailTitle = "";
                    var emails = error.Project.Project_ProjectManager.Select(q => q.ProjectManager.Email).ToList();
                    var pdfPath = Path.Combine(Server.MapPath("~/dist/PlaceErrors"), $"{ID}.pdf");

                    if (error.Task != null)
                        emailTitle = $"OBS! Uppgift {error.Task.Name} Eventuellt Problem!";
                    else if (error.Apartment != null)
                        emailTitle = $"OBS! LGH {error.Apartment.Letter}{error.Apartment.NumberOfApartment} Eventuellt Problem!";
                    else
                        emailTitle = $"OBS! Projekt {error.Project.ProjectNumber} Eventuellt Problem!";

                    send = MailService.PlaceError_MailReport(emailTitle, emails, pdfPath, out _exception);

                    if (send)
                    {
                        error.SendDate = DateTime.Now;
                        db.Entry(error).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                else
                {
                    send = false;
                    _exception = "Błąd generowania dokumentu PDF";
                }
            }

            return Json(new { success = send, error = _exception }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeletePlaceError (int id)
        {
            using (var db = new ACSystemContext())
            {
                var error = db.PlaceError.Find(id);
                var todos = db.Todo.Where(q => q.PlaceErrorID == id);

                db.PlaceError.Remove(error);
                db.Todo.RemoveRange(todos);

                try
                {
                    db.SaveChanges();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region ORDERS

        public ActionResult ShowOrder(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = _repo.GetContext();

            var order = db.Order.Find(ID.Value);

            if (order == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return PartialView("Modals/Modal_ShowOrder", order);
        }

        public ActionResult TranslateOrder(int? ID, string Redirect)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = _repo.GetContext();

            var order = db.Order.Find(ID.Value);

            if (order == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.Redirect = Redirect;

            return PartialView("Modals/Modal_TranslateOrder", order);
        }

        [HttpPost]
        public ActionResult TranslateOrder(string Redirect, int[] ID, string[] Item, string[] Translated)
        {
            var context = _repo.GetContext();

            for (var i = 0; i < ID.Length; i++)
            {
                var item = context.OrderItem.Find(ID[i]);
                item.Item = Item[i];
                item.Translated = Translated[i];

                context.Entry(item).State = EntityState.Modified;
            }

            try
            {
                context.SaveChanges();

                return this.Redirect(Redirect);
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "TranslateOrder",
                    LogDate = DateTime.Now,
                    User = User.Identity.Name
                };

                context.ExceptionsLog.Add(log);

                context.SaveChanges();
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult DeleteOrder(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = _repo.GetContext();

            var order = db.Order.Find(ID.Value);

            if (order == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            db.OrderItem.RemoveRange(order.OrderItems);
            db.Order.Remove(order);

            try
            {
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FinishOrder(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = _repo.GetContext();

            var order = db.Order.Find(ID.Value);

            if (order == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            order.IsFinished = true;

            db.Entry(order).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PrintOrder(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = _repo.GetContext();

            var order = db.Order.Find(ID.Value);

            if (order == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return PartialView("Print/PrintOrder", order);
        }

        public ActionResult Modal_SendOrder(int? ProjectID, int? OrderID)
        {
            if (!ProjectID.HasValue || !OrderID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = _repo.GetContext();

            var project = db.Projects.Find(ProjectID.Value);

            if (project == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.PMs = project.Project_ProjectManager;
            ViewBag.OrderID = OrderID.Value;

            return PartialView("Modals/Modal_SendOrder");
        }

        [HttpPost]
        public ActionResult SendOrder(int OrderID, string Email)
        {
            var db = _repo.GetContext();

            var order = db.Order.Find(OrderID);

            if (order == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = MailService.Order(Email, order);

            return Json(result ? new {success = true} : new {success = false}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult NewOrder(int ProjectID, int? ApartmentID, int? TaskID, string[] orderItem, bool fromProject = false)
        {
            var db = new ACSystemContext();

            var order = new Order
            {
                ProjectID = ProjectID,
                ApartmentID = ApartmentID,
                TaskID = TaskID,
                FitterID = User.Identity.GetUserId(),
                IsFinished = false
            };

            foreach (var i in orderItem)
            {
                var item = new OrderItem
                {
                    Item = i
                };

                order.OrderItems.Add(item);
            }

            db.Order.Add(order);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!fromProject)
                return Redirect("/Projects/ApartmentDetails/" + ApartmentID.Value);
            else
                return Redirect("/Projects/Details/" + ProjectID);
        }

        [HttpGet]
        public ActionResult TasksList(int apartmentId)
        {
            var db = new ACSystemContext();

            List<object> json = new List<object>();

            var list = db.Tasks.Where(q => q.ApartmentId == apartmentId).Select(q => new { Id = q.Id, ProductName = q.GetTaskName(q.Apartment.ProjectId), Sufix = q.TaskSufix }).ToList();

            foreach (var l in list)
                json.Add(Json(new { Id = l.Id, ProductName = l.ProductName, Sufix = l.Sufix }).Data);

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region REPORTS

        [HttpPost]
        public ActionResult MakeGroupReport(int[] apartments, string type, string what)
        {
            var db = _repo.GetContext();
            var projectID = -1;
            var success = true;

            var reportType = TaskReportType.Dev_All;

            switch (what)
            {
                case "all":
                    reportType = (type == "dev") ? TaskReportType.Dev_All : TaskReportType.Service_All;
                    break;
                case "kitchen":
                    reportType = (type == "dev") ? TaskReportType.Dev_Kitchen : TaskReportType.Service_Kitchen;
                    break;
                case "construction":
                    reportType = (type == "dev") ? TaskReportType.Dev_Construction : TaskReportType.Service_Construction;
                    break;
            }

            var reports = new List<ReportNag>();

            foreach (var a in apartments)
            {
                var needGenerateNewRaport = false;

                var lastReport = db.ReportNag.OrderByDescending(q => q.Date).FirstOrDefault(q => q.ApartmentID == a && q.Type == reportType);
                var apartment = db.Apartments.Find(a);

                if (apartment == null)
                    continue;

                projectID = apartment.ProjectId;

                if (lastReport != null)
                {
                    var elements = db.ReportElem.Where(q => q.ReportNagID == lastReport.ID).ToList();

                    foreach (var e in elements)
                    {
                        Tasks apartmentTask = apartment.Tasks.FirstOrDefault(q => q.Id == e.TaskID);

                        if (apartmentTask == null)
                            continue;

                        if (apartmentTask.Task_Fitter.Where(q => q.IsActive).Sum(q => q.WorkDone) != e.WorkDone)
                            needGenerateNewRaport = true;

                        if (apartmentTask.ClosedDate != e.LastReportDate)
                            needGenerateNewRaport = true;
                    }
                }
                else
                {
                    needGenerateNewRaport = true;
                }


                if (needGenerateNewRaport == false)
                    continue;

                var reportsCount = db.ReportNag.Count(q => q.ApartmentID == a);

                var reportNag = new ReportNag
                {
                    Number = reportsCount + 1,
                    Type = reportType,
                    Date = DateTime.Now,
                    ProjectID = apartment.ProjectId,
                    ApartmentID = apartment.Id,
                    Status = apartment.Project.Status,
                    Precursor = lastReport?.ID
                };

                var tasks = new List<Tasks>();

                switch (reportType)
                {
                    case TaskReportType.Dev_All:
                        tasks = apartment.Tasks.Where(q => q.Production == TaskProduction.Developer).ToList();
                        break;
                    case TaskReportType.Dev_Kitchen:
                        tasks = apartment.Tasks.Where(q => q.Production == TaskProduction.Developer && q.Product.ProductType == TaskType.Kitchen).ToList();
                        break;
                    case TaskReportType.Dev_Construction:
                        tasks = apartment.Tasks.Where(q => q.Production == TaskProduction.Developer && q.Product.ProductType == TaskType.Construction).ToList();
                        break;
                    case TaskReportType.Service_All:
                        tasks = apartment.Tasks.Where(q => q.Production == TaskProduction.Complaint || q.Production == TaskProduction.Warranty).ToList();
                        break;
                    case TaskReportType.Service_Kitchen:
                        tasks = apartment.Tasks.Where(q => (q.Production == TaskProduction.Complaint || q.Production == TaskProduction.Warranty) && q.Product.ProductType == TaskType.Kitchen).ToList();
                        break;
                    case TaskReportType.Service_Construction:
                        tasks = apartment.Tasks.Where(q => (q.Production == TaskProduction.Complaint || q.Production == TaskProduction.Warranty) && q.Product.ProductType == TaskType.Construction).ToList();
                        break;

                }

                foreach (var t in tasks)
                {
                    var percent = 0;
                    var percentDone = (int)Math.Ceiling(((t.Task_Fitter.Where(q => q.IsActive)?.Sum(q => q.WorkDone) ?? 0) / t.ProductCount) * 100);

                    if (t.Status == TaskStatus.Free)
                        percent = 0;
                    else if (t.Status == TaskStatus.FinishedWithFaults)
                        percent = 90;
                    else if (t.Status == TaskStatus.Finished || t.Status == TaskStatus.FinishedWithFaultsCorrected)
                        percent = 100;
                    else
                        percent = (percentDone == 100) ? 99 : percentDone;

                    var elem = new ReportElem
                    {
                        TaskID = t.Id,
                        WorkDone = t.Task_Fitter.Where(q => q.IsActive)?.Sum(q => q.WorkDone) ?? 0,
                        WorkToDo = t.ProductCount,
                        LastReportDate = t.Task_Fitter.OrderByDescending(q => q.Timestamp).FirstOrDefault()?.Timestamp,
                        FitterID = t.Task_Fitter.OrderByDescending(q => q.Timestamp).FirstOrDefault(q => q.IsActive)?.FitterId ?? null,
                        PercentDone = percent,
                        Questions = new List<Report_Questions>()
                    };

                    var tq = db.Task_Questions.FirstOrDefault(q => q.QuestionId == t.Id);

                    if (tq != null)
                        elem.Questions.Add(Report_Questions.CopyFromTask(tq));


                    reportNag.Elements.Add(elem);
                }

                reports.Add(reportNag);
                db.ReportNag.Add(reportNag);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "MakeGroupReport", User.Identity.Name);
                    success = false;
                }
            }

            return Json(new { success = success }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetReport_ServiceTasksWithCode(int? ProjectID)
        {
            if (!ProjectID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = _repo.GetContext();

            var url = $"http://{HttpContext.Request.Url.Host}:{HttpContext.Request.Url.Port}/Print/ServiceTasksWithCode/{ProjectID.Value}";

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

            var fileName = Strings.RandomString(16);

            using (var sr = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
            {
                var fileContent = sr.ReadToEnd();

                PdfDocument pdf = PdfGenerator.GeneratePdf(fileContent, PageSize.A4);
                try
                {
                    pdf.Save(Server.MapPath($"~/dist/RaportPDF/{fileName}.pdf"));
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "GetReport_ServiceTaskWithCode", User.Identity.Name);
                    return null;
                }
            }

            //currentTask.ReportSent = true;
            //currentTask.ReportSentDate = DateTime.Now;
            //currentTask.Todo = false;
            //context.Entry(currentTask).State = EntityState.Modified;

            //context.SaveChanges();

            return Json(new { success = true, file = fileName }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {

            }
            base.Dispose(disposing);
        }
    }
}

