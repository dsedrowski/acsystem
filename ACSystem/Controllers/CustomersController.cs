﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ACSystem.Core.Models;
using ACSystem.Core.IRepo;
using ACSystem.ViewModels;
using ACSystem.Core.Filters;
using ACSystem.FortnoxService;
using FN = FortnoxAPILibrary;

namespace ACSystem.Controllers
{
    [AuthLog(Roles = "Admin")]
    public class CustomersController : Controller
    {
        /// <summary>
        /// Zmienna przechowująca warstwę logiczną aplikacji
        /// </summary>
        private readonly ICustomersRepo _repo;

        public CustomersController(ICustomersRepo repo)
        {
            _repo = repo;
        }

        /// <summary>
        /// Funkcja wyświetlająca główną listę klientów
        /// </summary>
        public ActionResult Index()
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista klientów", Url = "/Customers", Icon = "fa fa-users", Active = true });

            return View();
        }

        /// <summary>
        /// Funkcja wykorzystywana przez AJAX do pobrania listy klientów
        /// </summary>
        /// <param name="page">Aktualna strona tabeli</param>
        /// <param name="orderBy">Kolumna, po której ma być sortowana tabela</param>
        /// <param name="dest">Kierunek sortowania tabeli</param>
        /// <param name="search">Fraza przy pomocy której tabela zostaje przeszukiwana</param>
        /// <returns>Zwraca listę klientów</returns>
        [HttpGet]
        public ActionResult CustomersList(int? page, string orderBy = "name", string dest = "asc", string search = "", int filter = -1)
        {
            var pageSize = 20;

            var rowsCount = _repo.Count();

            var pagesCount = rowsCount / pageSize;

            if (rowsCount <= pageSize || page <= 0 || page == null) page = 1;

            var excludeRows = (page - 1) * pageSize;

            var rows = 0;

            var list = _repo.List(out rows, excludeRows, pageSize, orderBy, dest, search, filter);

            pagesCount = rows / pageSize;

            if (pagesCount <= 0) pagesCount = 1;

            return Json(new { data = list, pages = pagesCount }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Pobiera i wyświetla szczegółowe dane o kliencie
        /// </summary>
        /// <param name="id">ID Klienta</param>
        public ActionResult Details(int? id)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista klientów", Url = "/Customers", Icon = "fa fa-users", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Szczegóły klienta", Url = "/Customers/Details", Icon = "", Active = true });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customers customers = _repo.GetCustomerById((int)id);
            if (customers == null)
            {
                return HttpNotFound();
            }

            var db = new ACSystemContext();

            var allCustomers = db.Customers.OrderBy(q => q.Name).ToList();
            var currentIndex = allCustomers.FindIndex(q => q.Id == id.Value);

            int? prev = null, next = null;

            if (currentIndex > 0)
                prev = allCustomers[currentIndex - 1].Id;

            if (currentIndex < allCustomers.Count - 1)
                next = allCustomers[currentIndex + 1].Id;

            ViewBag.Prev = prev;
            ViewBag.Next = next;

            return View(customers);
        }

        public ActionResult GetPrevNextID(int current, string dest, string orderby, string search, string filter)
        {
            var db = new ACSystemContext();

            var showDisabled = filter.Contains("0");

            var customers = db.Customers.Where(c => c.Name.Contains(search) || c.Street.Contains(search) || c.Email.Contains(search));

            if (filter.Contains("0"))
                customers = customers.Where(q => q.IsLocked || !q.IsLocked);
            else
                customers = customers.Where(q => !q.IsLocked);

            switch (orderby)
            {
                case "name":
                    if (dest == "asc")
                        customers = customers.OrderBy(c => c.Name);
                    else
                        customers = customers.OrderByDescending(c => c.Name);
                    break;
                case "address":
                    if (dest == "asc")
                        customers = customers.OrderBy(c => c.Street);
                    else
                        customers = customers.OrderByDescending(c => c.Street);
                    break;
                case "email":
                    if (dest == "asc")
                        customers = customers.OrderBy(c => c.Email);
                    else
                        customers = customers.OrderByDescending(c => c.Email);
                    break;
                default:
                    if (dest == "asc")
                        customers = customers.OrderBy(c => c.Name);
                    else
                        customers = customers.OrderByDescending(c => c.Name);
                    break;
            }

            var list = customers.ToList();

            var currentIndex = list.FindIndex(q => q.Id == current);

            int? prev = null, next = null;

            if (currentIndex > 0)
                prev = list[currentIndex - 1].Id;

            if (currentIndex < list.Count - 1)
                next = list[currentIndex + 1].Id;

            return Json(new { prev = prev, next = next }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Wyświetla formularz tworzenia nowego klienta
        /// </summary>
        public ActionResult Create()
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista klientów", Url = "/Customers", Icon = "fa fa-users", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Dodaj klienta", Url = "/Customers/Create", Icon = "", Active = true });

            var model = new Customers();

            return View(model);
        }

        /// <summary>
        /// Przetwarza wysłane metodą POST dane, w celu utworzenia klienta
        /// </summary>
        /// <param name="customers">Model z danymi klienta</param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Street,HouseNo,PostalCode,City,VATNumber,Email,PaymentTerm,DocumentsLeanguage,AutoSendEmail,VAT,ReverseCharge,IsLocked")] Customers customers)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista klientów", Url = "/Customers", Icon = "fa fa-users", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Dodaj klienta", Url = "/Customers/Create", Icon = "", Active = true });

            if (ModelState.IsValid)
            {
                if(!_repo.Create(customers))
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                try
                {
                    _repo.SaveChanges();

                    CustomerManage manage = new CustomerManage();
                    customers.InSystemID = manage.Create(new FN.Customer
                    {
                        Name = customers.Name,
                        Address1 = customers.Address + " " + customers.HouseNo,
                        ZipCode = customers.PostalCode,
                        City = customers.City,
                        VATNumber = customers.VATNumber,
                        Email = customers.Email,
                        VATType = (customers.ReverseCharge) ? FN.Connectors.CustomerConnector.VATType.SEREVERSEDVAT : FN.Connectors.CustomerConnector.VATType.SEVAT,
                        EmailInvoice = customers.Email,
                        EmailInvoiceCC = "info@acbygg.com"
                    })?.CustomerNumber;

                    _repo.Edit(customers);
                    _repo.SaveChanges();
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "", "");
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                return RedirectToAction("Index");
            }

            return View(customers);
        }

        /// <summary>
        /// Wyświetla formularz edycji klienta
        /// </summary>
        /// <param name="id">ID edytowanego klienta</param>
        /// <returns>Model z danymi klienta</returns>
        public ActionResult Edit(int? id)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista klientów", Url = "/Customers", Icon = "fa fa-users", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Szczegóły klienta", Url = "/Customers/Details", Icon = "", Active = true });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customers customers = _repo.GetCustomerById((int)id);
            if (customers == null)
            {
                return HttpNotFound();
            }
            return View(customers);
        }

        /// <summary>
        /// Przetwarza wysłane dane metodą POST, w celu edycji danych klienta
        /// </summary>
        /// <param name="customers">Model z danymi klienta</param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Street,HouseNo,PostalCode,City,VATNumber,Email,CreateDate,PaymentTerm,DocumentsLeanguage,AutoSendEmail,VAT,ReverseCharge,IsLocked,InSystemID")] Customers customers)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista klientów", Url = "/Customers", Icon = "fa fa-users", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Szczegóły klienta", Url = "/Customers/Details", Icon = "", Active = true });

            if (ModelState.IsValid)
            {
                _repo.Edit(customers);

                try
                {
                    _repo.SaveChanges();

                    var customer = _repo.GetCustomerById(customers.Id);

                    var manage = new CustomerManage();

                    if (!string.IsNullOrEmpty(customer.InSystemID))
                    {
                        var fortnox_customer = manage.Get(customer.InSystemID);

                        fortnox_customer.Name = customers.Name;
                        fortnox_customer.Address1 = customers.Address + " " + customers.HouseNo;
                        fortnox_customer.ZipCode = customers.PostalCode;
                        fortnox_customer.City = customers.City;
                        fortnox_customer.VATNumber = customers.VATNumber;
                        fortnox_customer.Email = customers.Email;
                        fortnox_customer.VATType = (customers.ReverseCharge) ? FN.Connectors.CustomerConnector.VATType.SEREVERSEDVAT : FN.Connectors.CustomerConnector.VATType.SEVAT;
                        fortnox_customer.EmailInvoice = customers.Email;
                        fortnox_customer.EmailInvoiceCC = "info@acbygg.com";

                        manage.Update(fortnox_customer);
                    }
                    else
                    {
                        customer.InSystemID = manage.Create(new FN.Customer
                        {
                            Name = customers.Name,
                            Address1 = customers.Address + " " + customers.HouseNo,
                            ZipCode = customers.PostalCode,
                            City = customers.City,
                            VATNumber = customers.VATNumber,
                            Email = customers.Email,
                            VATType = (customers.ReverseCharge) ? FN.Connectors.CustomerConnector.VATType.SEREVERSEDVAT : FN.Connectors.CustomerConnector.VATType.SEVAT,
                            EmailInvoice = customers.Email,
                            EmailInvoiceCC = "info@acbygg.com"
                        })?.CustomerNumber;

                        _repo.Edit(customer);
                        _repo.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "", "");
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                return RedirectToAction("Index");
            }
            return View(customers);
        }

        /// <summary>
        /// Usuwa klienta
        /// </summary>
        /// <param name="id">ID Klienta</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = "";

            var customer = _repo.GetCustomerById(id.Value);

            for (int i = 0; i < 3; i++)
            {
                result = _repo.Delete((int)id);

                if (result.Split(';')[0] == "true")
                {
                    if (!string.IsNullOrEmpty(customer.InSystemID))
                    {
                        var manage = new CustomerManage();
                        manage.Delete(customer.InSystemID);
                    }

                    break;
                }
            }

            var rsSplit = result.Split(';');

            if (rsSplit.Length > 1)
                return Json(new { success = rsSplit[0], message = rsSplit[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Nieoczekiwany błąd." }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Tworzy Menedżera Projektu dla danego klienta
        /// </summary>
        /// <param name="Id">ID Klienta</param>
        /// <param name="Name">Imię i nazwisko menedżera</param>
        /// <param name="Email">Email menedżera</param>
        /// <param name="PhoneNumber">Telefon menedżera</param>
        [HttpPost]
        public ActionResult ProjectManagerCreate(int Id, string Name, string Email, string PhoneNumber)
        {
            var projectManager = new ProjectManagers()
            {
                Name = Name,
                Email = Email,
                PhoneNumber = PhoneNumber,
                CustomerId = Id
            };

            var projectManagerId = _repo.ProjectManagerCreate(projectManager);

            return Redirect("/Customers/Details/" + Id);
        }

        /// <summary>
        /// Edytuje Menedżera Projektu dla danego klienta
        /// </summary>
        /// <param name="CustomerId">ID Klienta</param>
        /// <param name="Id">ID Menedżera</param>
        /// <param name="Name">Imię i nazwisko menedżera</param>
        /// <param name="Email">Email menedżera</param>
        /// <param name="PhoneNumber">Telefon menedżera</param>
        [HttpPost]
        public ActionResult ProjectManagerEdit(int CustomerId, int Id, string Name, string Email, string PhoneNumber)
        {
            var projectManager = new ProjectManagers()
            {
                Id = Id,
                Name = Name,
                Email = Email,
                PhoneNumber = PhoneNumber
            };

            var projectManagerId = _repo.ProjectManagerEdit(projectManager);

            return Redirect("/Customers/Details/" + CustomerId);
        }

        /// <summary>
        /// Usuwa Menedżera Projektu
        /// </summary>
        /// <param name="id">ID Menedżera Projektu</param>
        [HttpGet]
        public ActionResult ProjectManagerDelete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = "";

            for (int i = 0; i < 3; i++)
            {
                result = _repo.ProjectManagerDelete((int)id);

                if (result.Split(';')[0] == "true")
                    break;
            }

            var rsSplit = result.Split(';');

            if (rsSplit.Length > 1)
                return Json(new { success = rsSplit[0], message = rsSplit[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Nieoczekiwany błąd." }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Zwraca dane o Menedżerze Projektu
        /// </summary>
        /// <param name="id">ID Menedżera</param>
        [HttpGet]
        public ActionResult ProjectManagerById(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return Json(_repo.ProjectManagerById((int)id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LockManager(int ID, bool Locked)
        {
            var pm = _repo.ProjectManagerById(ID);

            if (pm == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var projectManager = new ProjectManagers
            {
                Id = ID,
                Name = pm.Name,
                Email = pm.Email,
                PhoneNumber = pm.PhoneNumber,
                IsLocked = Locked
            };

            var result = _repo.ProjectManagerEdit(projectManager);

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
