﻿using AC = ACSystem.Core.Models;
using ACSystem.FortnoxService;
using FT = FortnoxAPILibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using ACSystem.Core.Models;
using static FortnoxAPILibrary.Connectors.CustomerConnector;
using System.Threading;

namespace ACSystem.Controllers
{
    public class FortnoxController : Controller
    {
        public ActionResult Customer_Synchronize()
        {
            var reload = false;

            using (var db = new AC.ACSystemContext())
            {
                var manage = new CustomerManage();
                var customers = db.Customers.ToList();

                if (customers.Any(q => string.IsNullOrEmpty(q.InSystemID)))
                {
                    customers.Where(q => string.IsNullOrEmpty(q.InSystemID)).ToList().ForEach(x =>
                    {
                        try


                        {
                            var inSystemID = manage.Create(new FT.Customer
                            {
                                Name = x.Name,
                                Address1 = x.Address + " " + x.HouseNo,
                                ZipCode = x.PostalCode,
                                City = x.City,
                                VATNumber = x.VATNumber,
                                Email = x.Email,
                                VATType = (x.ReverseCharge) ? VATType.SEREVERSEDVAT : VATType.SEVAT,
                                EmailInvoice = x.Email,
                                EmailInvoiceCC = "info@acbygg.com"
                            });

                            x.InSystemID = inSystemID.CustomerNumber;
                            db.Entry(x).State = System.Data.Entity.EntityState.Modified;

                            reload = true;
                        }
                        catch (Exception ex)
                        {
                            Core.Service.Helpers.Log(ex, "Customer_Synchronize", User.Identity.Name);
                        }
                    });

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Core.Service.Helpers.Log(ex, "Customer_Synchronize", User.Identity.Name);

                        return Json(new
                        {
                            success = false,
                            message = ex.Message,
                            reload = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                manage.List().ForEach(x =>
                {
                    if (!customers.Any(q => !string.IsNullOrEmpty(q.InSystemID) && q.InSystemID.Equals(x.CustomerNumber)))
                    {
                        db.Customers.Add(new AC.Customers
                        {
                            InSystemID = x.CustomerNumber,
                            Name = x.Name,
                            Street = x.Address1,
                            PostalCode = x.ZipCode,
                            City = x.City,
                            VATNumber = (!string.IsNullOrEmpty(x.VATNumber)) ? x.VATNumber : "brak",
                            Email = (!string.IsNullOrEmpty(x.Email)) ? x.Email : "brak",
                            CreateDate = DateTime.Now
                        });

                        reload = true;
                    }
                });

                try
                {
                    db.SaveChanges();

                    return Json(new
                    {
                        success = true, message = "Pomyślnie zsynchronizowano dane.", reload
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "Customer_Synchronize", User.Identity.Name);

                    return Json(new
                    {
                        success = false,
                        message = ex.Message,
                        reload = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult Product_Synchronize()
        {
            var reload = false;
            var manager = new ArticleManager();

            using (var db = new AC.ACSystemContext())
            {
                var products = db.Products.ToList();

                if (products.Any(q => string.IsNullOrEmpty(q.InSystemID)))
                {
                    products.Where(q => string.IsNullOrEmpty(q.InSystemID)).ToList().ForEach(x =>
                    {
                        if (!string.IsNullOrEmpty(x.DescriptionSE))
                        {
                            try
                            {
                                var inSystemID = manager.Create(new FT.Article
                                {
                                    Description = x.DescriptionSE,
                                    SalesPrice = x.Price.ToString(),
                                    Unit = x.JmSwe.Trim(),
                                    Active = x.IsActive.ToString().ToLower(),
                                    Type = FortnoxAPILibrary.Connectors.ArticleConnector.ArticleType.SERVICE,
                                    PurchasePrice = (x.Price * (x.PercentHeight.Type1 / 100)).ToString().Replace(',', '.')
                                }, products);

                                x.InSystemID = inSystemID.ArticleNumber;
                                db.Entry(x).State = System.Data.Entity.EntityState.Modified;

                                reload = true;
                            }
                            catch (Exception ex)
                            {
                                Core.Service.Helpers.Log(ex, "Customer_Synchronize", User.Identity.Name);
                            }
                        }
                    });

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Core.Service.Helpers.Log(ex, "Customer_Synchronize", User.Identity.Name);

                        return Json(new
                        {
                            success = false,
                            message = ex.Message,
                            reload = false
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                //manager.List().ForEach(x =>
                //{
                //    if (x.Description == "TestProduktu")
                //        Console.WriteLine("tu");

                //    if (!products.Any(q => !string.IsNullOrEmpty(q.InSystemID) && q.InSystemID.Equals(x.ArticleNumber)))
                //    {
                //        db.Products.Add(new AC.Products
                //        {
                //            Name = x.Description,
                //            DescriptionSE = x.Description,
                //            Jm = x.Unit,
                //            JmSwe = x.Unit,
                //            InSystemID = x.ArticleNumber,
                //            IsActive = true,
                //            PercentHeight = new AC.PercentHeight
                //            {
                //                Type1 = 0,
                //                Type2 = 0,
                //                Type3 = 0,
                //                Type4 = 0,
                //                Type5 = 0,
                //                Type6 = 0
                //            }
                //        });

                //        reload = true;
                //    }
                //});

                try
                {
                    db.SaveChanges();

                    return Json(new
                    {
                        success = true,
                        message = "Pomyślnie zsynchronizowano dane.",
                        reload
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "Products_Synchronize", User.Identity.Name);

                    return Json(new
                    {
                        success = false,
                        message = ex.Message,
                        reload = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult Project_Synchronize()
        {
            var reload = false;
            List<string> errors = new List<string>();

            using (var db = new AC.ACSystemContext())
            {
                var manage = new ProjectManager();
                var projects = db.Projects.ToList();

                if (projects.Any(q => q.InFortnox != true))
                {
                    var notInFortnox = projects.Where(q => q.InFortnox != true).ToList();
                    notInFortnox.ForEach(x =>
                    {
                        string errorCode = "";

                        try
                        {
                            var status = FT.Connectors.ProjectConnector.Status.NOTSTARTED;

                            switch (x.Status)
                            {
                                case Core.ProjectStatus.NotStarted:
                                    status = FT.Connectors.ProjectConnector.Status.NOTSTARTED;
                                    break;
                                case Core.ProjectStatus.UnderWork:
                                    status = FT.Connectors.ProjectConnector.Status.ONGOING;
                                    break;
                                case Core.ProjectStatus.Finished:
                                    status = FT.Connectors.ProjectConnector.Status.COMPLETED;
                                    break;
                                case Core.ProjectStatus.FinishedWithFaults:
                                    status = FT.Connectors.ProjectConnector.Status.COMPLETED;
                                    break;
                            }

                            Thread.Sleep(350);

                            var fortnox = manage.Create(new FT.Project
                            {
                                ProjectNumber = x.InSystemID,
                                Description = x.ProjectNumber,
                                StartDate = x.StartDate.ToString("yyyy-MM-dd"),
                                EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                                ProjectLeader = (x.ProjectManagers.Any()) ? x.Project_ProjectManager.FirstOrDefault().ProjectManager.Name : "",
                                Status = status                                
                            }, out errorCode);

                            x.InFortnox = true;
                            db.Entry(x).State = System.Data.Entity.EntityState.Modified;

                            reload = true;
                        }
                        catch (Exception ex)
                        {
                            if (!errorCode.Equals("2001182"))
                                errors.Add(ex.InnerException?.Message);

                            Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                        }

                        if (errorCode.Equals("2001182"))
                        {
                            x.InFortnox = true;
                            db.Entry(x).State = System.Data.Entity.EntityState.Modified;
                        }

                    });
                }

                //manage.List().ForEach(x =>
                //{
                //    if (!projects.Any(q => (!string.IsNullOrEmpty(q.InSystemID) && q.InSystemID.Equals(x.ProjectNumber)) || q.ProjectNumber.Equals(x.Description)))
                //    {
                //        var status = Core.ProjectStatus.NotStarted;

                //        switch (x.Status)
                //        {
                //            case FT.Connectors.ProjectConnector.Status.NOTSTARTED:
                //                status = Core.ProjectStatus.NotStarted;
                //                break;
                //            case FT.Connectors.ProjectConnector.Status.ONGOING:
                //                status = Core.ProjectStatus.UnderWork;
                //                break;
                //            case FT.Connectors.ProjectConnector.Status.COMPLETED:
                //                status = Core.ProjectStatus.Finished;
                //                break;
                //        }

                //        var startDate = (!string.IsNullOrEmpty(x.StartDate)) ? DateTime.Parse(x.StartDate) : DateTime.Now;
                //        var endDate = (!string.IsNullOrEmpty(x.EndDate)) ? DateTime.Parse(x.EndDate) : DateTime.Now;

                //        if (!db.Projects.Any(q => q.ProjectNumber.Equals(x.Description)))
                //        {
                //            var customer = db.Customers.FirstOrDefault(q => q.Name.Contains("AC SERVICE"));

                //            if (customer != null)
                //            {
                //                db.Projects.Add(new AC.Projects
                //                {
                //                    InSystemID = x.ProjectNumber,
                //                    ProjectNumber = x.Description,
                //                    StartDate = startDate,
                //                    EndDate = endDate,
                //                    Status = status,
                //                    City = "Dodać miasto",
                //                    CreateDate = DateTime.Now,
                //                    CustomerId = customer.Id
                //                });
                //            }
                //        }
                //        reload = true;
                //    }
                //    else if (projects.Any(q => (!string.IsNullOrEmpty(q.InSystemID) && q.InSystemID.Equals(x.ProjectNumber)) || q.ProjectNumber.Equals(x.Description)))
                //    {
                //        var p = projects.FirstOrDefault(q => (!string.IsNullOrEmpty(q.InSystemID) && q.InSystemID.Equals(x.ProjectNumber)) || q.ProjectNumber.Equals(x.Description));
                //        p.InFortnox = true;
                //        db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                //    }
                //});

                try
                {
                    db.SaveChanges();

                    if (errors.Count > 0)
                        return Json(new
                        {
                            success = false,
                            message = string.Join(Environment.NewLine, errors.ToArray()),
                            reload
                        }, JsonRequestBehavior.AllowGet);
                    

                    return Json(new
                    {
                        success = true,
                        message = "Pomyślnie zsynchronizowano dane.",
                        reload
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "Project_Synchronize", User.Identity.Name);

                    return Json(new
                    {
                        success = false,
                        message = ex.Message,
                        reload = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult User_Synchronize()
        {
            var reload = false;

            using (var db = new AC.ACSystemContext())
            {
                var manage = new UserManager();
                var users = db.ACSystemUser.ToList();

                if (users.Any(q => !string.IsNullOrEmpty(q.InSystemUserID)))
                {
                    users.Where(q => !string.IsNullOrEmpty(q.InSystemUserID)).ToList().ForEach(x =>
                    {
                        try
                        {
                            var inSystemID = manage.Create(new FT.CostCenter
                            {
                                Code = x.InSystemUserID,
                                Description = x.FullName,
                                Note = $"{x.PhoneNumber} {x.Email}"
                            });

                            reload = true;
                        }
                        catch (Exception ex)
                        {
                            Core.Service.Helpers.Log(ex, "User_Synchronize", User.Identity.Name);
                        }
                    });
                }

                try
                {
                    db.SaveChanges();

                    return Json(new
                    {
                        success = true,
                        message = "Pomyślnie zsynchronizowano dane.",
                        reload
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "Customer_Synchronize", User.Identity.Name);

                    return Json(new
                    {
                        success = false,
                        message = ex.Message,
                        reload = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult Send_Invoice(int? ID)
        {
            var reload = false;

            if (!ID.HasValue)
                return Json(new
                {
                    success = false,
                    message = "Nie podano ID faktury.",
                    reload
                }, JsonRequestBehavior.AllowGet);

            using (var db = new AC.ACSystemContext())
            {
                var invoice = db.Invoice.Find(ID);

                if (invoice == null)
                    return Json(new
                    {
                        success = false,
                        message = "Nie znaleziono faktury o podanym ID.",
                        reload
                    }, JsonRequestBehavior.AllowGet);

                int[] paymentTerms = { 0, 10, 15, 20, 30, 45 };

                var fortnox_invoice = new FT.Invoice
                {
                    Address1 = $"{invoice.Customer.Street} {invoice.Customer.HouseNo}",
                    City = invoice.Customer.City,
                    CustomerName = invoice.Customer.Name,
                    CustomerNumber = invoice.Customer.InSystemID,
                    DeliveryName = invoice.Project.ProjectNumber,
                    DeliveryAddress1 = invoice.Project.Street,
                    DeliveryCity = invoice.Project.City,
                    DeliveryDate = invoice.DeliveryDate.ToString("yyyy-MM-dd"),
                    //DocumentNumber = invoice.InvoiceNo,
                    DueDate = invoice.InvoiceDate.AddDays(invoice.Customer.PaymentTerm).ToString("yyyy-MM-dd"),
                    InvoiceDate = invoice.InvoiceDate.ToString("yyyy-MM-dd"),
                    InvoicePeriodStart = invoice.InvoiceDateFrom.ToString("yyyy-MM-dd"),
                    InvoicePeriodEnd = invoice.InvoiceDateTo.ToString("yyyy-MM-dd"),
                    Project = invoice.Project.InSystemID,
                    TotalToPay = ((float)invoice.Amount).ToString(),
                    Total = ((float)(invoice.Amount / 1 + ((decimal)invoice.Customer.VAT / 100))).ToString(),
                    TotalVAT = ((float)(invoice.Amount - (invoice.Amount / 1 + ((decimal)invoice.Customer.VAT / 100)))).ToString(),
                    Remarks = invoice.Project.InvoiceDescription,
                    TermsOfPayment = (paymentTerms.Contains(invoice.Customer.PaymentTerm)) ? invoice.Customer.PaymentTerm.ToString() : "",
                    YourOrderNumber = invoice.Project.InSystemID,
                    YourReference = (invoice.Project.Project_ProjectManager.Any()) ? invoice.Project.Project_ProjectManager.FirstOrDefault().ProjectManager.Name : "",
                    EDIInformation = (!string.IsNullOrEmpty(invoice.Project.CustomerProjectNumber)) ? new FT.InvoiceEDIInformation { EDIYourElectronicReference = invoice.Project.CustomerProjectNumber } : null
                };

                List<FT.InvoiceRow> rows = new List<FT.InvoiceRow>();


                if (invoice.Project.IsGroupedInvoice)
                {
                    var groupedRows = new List<InvoiceElements>();

                    foreach (var row in invoice.Elements)
                    {
                        Expression<Func<InvoiceElements, bool>> findRowExpression = (q) => q.ProductName == row.ProductName && 
                                                                                           q.UnitPrice == row.UnitPrice && 
                                                                                           q.Task != null && q.Task.InvoiceDescription == row.Task.InvoiceDescription;

                        if (groupedRows.AsQueryable().Any(findRowExpression))
                            groupedRows.AsQueryable().FirstOrDefault(findRowExpression).Quantity += row.Quantity;
                        else
                            groupedRows.Add(row);
                    }

                    var firstApartment = invoice.Elements.OrderBy(q => q.ApartmentNumber.Length).ThenBy(q => q.ApartmentNumber).FirstOrDefault();
                    var lastApartment = invoice.Elements.OrderByDescending(q => q.ApartmentNumber.Length).ThenByDescending(q => q.ApartmentNumber).FirstOrDefault();
                    var firstFinishDate = invoice.Elements.OrderBy(q => q.Task.ClosedDate).FirstOrDefault();
                    var lastFinishDate = invoice.Elements.OrderByDescending(q => q.Task.ClosedDate).FirstOrDefault();

                    rows.Add(new FT.InvoiceRow
                    {
                        Description = $"LGH {firstApartment.ApartmentNumber} - {lastApartment.ApartmentNumber}",
                        AccountNumber = "",
                        VAT = ""
                    });

                    rows.Add(new FT.InvoiceRow
                    {
                        Description = $"{firstFinishDate.Task.EndDate.ToString("yyyy-MM-dd")} - {lastFinishDate.Task.EndDate.ToString("yyyy-MM-dd")}",
                        AccountNumber = "",
                        VAT = ""
                    });

                    foreach (var g in groupedRows)
                    {
                        var taskFitter = g.Task.Task_Fitter.FirstOrDefault(q => q.Status == ACSystem.Core.TaskStatus.Finished ||
                                                                       q.Status == ACSystem.Core.TaskStatus
                                                                           .FinishedWithFaults);

                        rows.Add(new FT.InvoiceRow
                        {
                            ArticleNumber = g.Task.Product.InSystemID,
                            DeliveredQuantity = ((float)g.Quantity).ToString().Replace(',', '.'),
                            Description = $"{g.Task.GetTaskNameOnInvoice(g.ProjectID)}",
                            Price = ((float)g.UnitPrice).ToString().Replace(',', '.'),
                            Total = ((float)g.Sum).ToString().Replace(',', '.'),
                            Unit = g.Task.Product.JmSwe,
                            VAT = (invoice.Customer.ReverseCharge) ? "0" : invoice.Customer.VAT.ToString(),
                            AccountNumber = (invoice.Customer.ReverseCharge || invoice.Customer.VAT.Equals("0")) ? "3231" : "3001",
                            CostCenter = taskFitter?.Fitter?.User?.InSystemUserID?.ToUpper()
                        });

                        if (!string.IsNullOrEmpty(g.Task.InvoiceDescription))
                        {
                            var wrapedText = Core.Service.Strings.WordWrap(g.Task.InvoiceDescription, 50);

                            foreach (var w in wrapedText)
                            {
                                rows.Add(new FT.InvoiceRow
                                {
                                    Description = w,
                                    AccountNumber = "",
                                    VAT = ""
                                });
                            }
                        }
                    }
                }
                else
                {
                    var apartments = invoice.Elements.GroupBy(q => q.ApartmentNumber);

                    foreach (var a in apartments)
                    {
                        rows.Add(new FT.InvoiceRow
                        {
                            Description = "LGH " + a.Key,
                            AccountNumber = "",
                            VAT = ""
                        });

                        if (!string.IsNullOrEmpty(a.FirstOrDefault().Task.Apartment.InvoiceDescription))
                        {
                            var wrapedText = Core.Service.Strings.WordWrap(a.FirstOrDefault().Task.Apartment.InvoiceDescription, 50);

                            foreach (var w in wrapedText)
                            {
                                rows.Add(new FT.InvoiceRow
                                {
                                    Description = w,
                                    AccountNumber = "",
                                    VAT = ""
                                });
                            }
                        }

                        foreach (var row in a)
                        {
                            var taskFitter = row.Task.Task_Fitter.FirstOrDefault(q => q.Status == ACSystem.Core.TaskStatus.Finished ||
                                                                          q.Status == ACSystem.Core.TaskStatus
                                                                              .FinishedWithFaults);

                            rows.Add(new FT.InvoiceRow
                            {
                                ArticleNumber = row.Task.Product.InSystemID,
                                DeliveredQuantity = ((float)row.Quantity).ToString().Replace(',', '.'),
                                Description = $"{row.Task.GetTaskNameOnInvoice(row.ProjectID)}",
                                Price = ((float)row.UnitPrice).ToString().Replace(',', '.'),
                                Total = (row.Invoice_AddsID.HasValue) ? ((float)row.Total).ToString().Replace(',', '.') : ((float)row.Sum).ToString().Replace(',', '.'),
                                Unit = row.Task.Product.JmSwe,
                                VAT = (invoice.Customer.ReverseCharge) ? "0" : invoice.Customer.VAT.ToString(),
                                AccountNumber = (invoice.Customer.ReverseCharge || invoice.Customer.VAT.Equals("0")) ? "3231" : "3001",
                                CostCenter = taskFitter?.Fitter?.User?.InSystemUserID?.ToUpper()
                            });

                            if (!string.IsNullOrEmpty(row.Task.InvoiceDescription))
                            {
                                var wrapedText = Core.Service.Strings.WordWrap(row.Task.InvoiceDescription, 50);

                                foreach (var w in wrapedText)
                                {
                                    rows.Add(new FT.InvoiceRow
                                    {
                                        Description = w,
                                        AccountNumber = "",
                                        VAT = ""
                                    });
                                }
                            }

                            if (!string.IsNullOrEmpty(row.Description))
                            {
                                var wrapedText = Core.Service.Strings.WordWrap(row.Description, 50);

                                foreach (var w in wrapedText)
                                {
                                    rows.Add(new FT.InvoiceRow
                                    {
                                        Description = w,
                                        AccountNumber = "",
                                        VAT = ""
                                    });
                                }
                            }
                        }
                    }
                }

                fortnox_invoice.InvoiceRows = rows;

                try
                {
                    InvoiceManager manager = new InvoiceManager();

                    var result = manager.Create(fortnox_invoice, invoice.Customer.Email);

                    invoice.Status = Core.InvoiceStatus.Fortnox;
                    invoice.FortnoxUrl = result.DocumentNumber;
                    db.Entry(invoice).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    reload = true;

                    return Json(new
                    {
                        success = true,
                        message = "Pomyślnie wysłano fakturę do Fortnox.",
                        reload
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        success = false,
                        message = ex.Message + Environment.NewLine + ex.InnerException?.Message,
                        reload = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
