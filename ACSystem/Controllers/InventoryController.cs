﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ACSystem.Core.Models;
using ACSystem.Core.IRepo;
using ACSystem.ViewModels;
using ACSystem.Core.Filters;
using ACSystem.Core.Service;
using System.IO;
using ACSystem.Core;
using ACSystem.Core.Extensions;
using ACSystem.Core.Models.ViewModels.Select;
using System.Drawing;
using System.Drawing.Imaging;

namespace ACSystem.Controllers
{
    [AuthLog(Roles = "Admin")]
    public class InventoryController : Controller
    {
        private readonly IInventoryRepo _repo;

        public InventoryController(IInventoryRepo repo)
        {
            _repo = repo;
        }

        public ActionResult Index()
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista narzędzi", Url = "/Inventory", Icon = "fa fa-wrench", Active = true });

            return View();
        }

        [HttpGet]
        public ActionResult InventoryList(int? page, string orderBy = "numberOfMachine", string dest = "asc", string search = "", string filter = "0,1",
                                        string numer_ew_s = "", string numer_ew_t = "", string typ = "", string marka = "", DateTime? data = null,
                                        string magazyn = "", string posiada = "", string akcesoria = "")
        {
            var pageSize = 20;

            var rowsCount = _repo.Count();

            var pagesCount = rowsCount / pageSize;

            if (rowsCount <= pageSize || page <= 0 || page == null) page = 1;

            var excludeRows = (page - 1) * pageSize;

            var rows = 0;

            var list = _repo.List(out rows, excludeRows, pageSize, orderBy, dest, search, filter, numer_ew_s, numer_ew_t, typ, marka, data, magazyn, posiada, akcesoria);

            pagesCount = rows / pageSize;

            if (pagesCount <= 0) pagesCount = 1;

            return Json(new { data = list, pages = pagesCount }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista narzędzi", Url = "/Inventory", Icon = "fa fa-wrench", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Szczegóły narzędzia", Url = "/Inventory/Details", Icon = "", Active = true });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inventory inventory = await _repo.GetInventory((int)id);
            if (inventory == null)
            {
                return HttpNotFound();
            }

            var db = new ACSystemContext();

            var all = db.Inventory.OrderBy(q => q.EvidenceNumber).ToList();
            var currentIndex = all.FindIndex(q => q.ID == id.Value);

            int? prev = null, next = null;

            if (currentIndex > 0)
                prev = all[currentIndex - 1].ID;

            if (currentIndex < all.Count - 1)
                next = all[currentIndex + 1].ID;

            ViewBag.Prev = prev;
            ViewBag.Next = next;

            return View(inventory);
        }

        public ActionResult GetPrevNextID(int current, string dest, string orderby, string search, string filter)
        {
            var db = new ACSystemContext();

            var showDisabled = filter.Contains("0");
            var filterList = filter.ToIntArray(',');

            var inventory = db.Inventory.Where(p => p.EvidenceNumber.Contains(search) || p.Type.Name.Contains(search) || p.Mark.Contains(search));
            inventory = inventory.Where(q => filterList.Contains((int)q.Status));

            switch (orderby)
            {
                case "numberOfMachine":
                    if (dest == "asc")
                        inventory = inventory.OrderBy(p => p.EvidenceNumber);
                    else
                        inventory = inventory.OrderByDescending(p => p.EvidenceNumber);
                    break;
                case "type":
                    if (dest == "asc")
                        inventory = inventory.OrderBy(p => p.Type.Name);
                    else
                        inventory = inventory.OrderByDescending(p => p.Type.Name);
                    break;
                case "mark":
                    if (dest == "asc")
                        inventory = inventory.OrderBy(p => p.Mark);
                    else
                        inventory = inventory.OrderByDescending(p => p.Mark);
                    break;
                case "status":
                    if (dest == "asc")
                        inventory = inventory.OrderBy(p => p.Status);
                    else
                        inventory = inventory.OrderByDescending(p => p.Status);
                    break;
                case "buydate":
                    if (dest == "asc")
                        inventory = inventory.OrderBy(p => p.BuyDate);
                    else
                        inventory = inventory.OrderByDescending(p => p.BuyDate);
                    break;
                default:
                    if (dest == "asc")
                        inventory = inventory.OrderBy(p => p.EvidenceNumber);
                    else
                        inventory = inventory.OrderByDescending(p => p.EvidenceNumber);
                    break;
            }

            var list = inventory.ToList();

            var currentIndex = list.FindIndex(q => q.ID == current);

            int? prev = null, next = null;

            if (currentIndex > 0)
                prev = list[currentIndex - 1].ID;

            if (currentIndex < list.Count - 1)
                next = list[currentIndex + 1].ID;

            return Json(new { prev = prev, next = next }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista narzędzi", Url = "/Inventory", Icon = "fa fa-wrench", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Dodaj narzędzie", Url = "/Inventory/Create", Icon = "", Active = true });

            var db = new ACSystemContext();

            var fitters = db.ACSystemUser.ToList();
            ViewBag.Fitters = fitters;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Inventory inventory)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista narzędzi", Url = "/Inventory", Icon = "fa fa-wrench", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Dodaj narzędzie", Url = "/Inventory/Create", Icon = "", Active = true });

            var accessories = Request.Form["AccessoriesID[]"]?.ToIntArray(',') ?? new List<int>().ToArray();
            var files = Request.Form["Files[]"]?.Split(',') ?? new List<string>().ToArray();

            foreach (var accessory in accessories)
            {
                var connect = new Inventory_Accessories_Connect
                {
                    AccessoryID = accessory,
                    Accessory = _repo.GetContext().Inventory_Accessories.Find(accessory)
                };

                inventory.Inventory_Accessories_Connect.Add(connect);
            }

            foreach (var file in files)
            {
                var inventoryFile = new Inventory_Photo
                {
                    Name = file.Split(';')[0],
                    URL = file.Split(';')[1]
                };

                inventory.Inventory_Photo.Add(inventoryFile);
            }

            inventory.BuyPriceNetto = Decimal.Divide(inventory.BuyPriceBrutto, (decimal)1.23);

            if (inventory.EvidenceNumber.IsNullOrEmpty() && inventory.EvidenceNumber2.IsNullOrEmpty())
            {
                ModelState.AddModelError("EvidenceNumber", "Przynajmniej jeden numer ewidencyjny musi zostać uzupełniony");
                ModelState.AddModelError("EvidenceNumber2", "Przynajmniej jeden numer ewidencyjny musi zostać uzupełniony");
            }

            var isSNumerExist = _repo.GetContext().Inventory.Any(q => q.EvidenceNumber.Equals(inventory.EvidenceNumber));

            if (isSNumerExist)
            {
                ModelState.AddModelError("EvidenceNumber", "Podany numer już istnieje.");
            }

            if (ModelState.IsValid)
            {
                if (_repo.Create(inventory) > 0)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                try
                {
                    _repo.SaveChanges();

                    if (inventory.OwnerType == InventoryOwnerType.Private)
                        _repo.GiveInventory(inventory.ID, inventory.OwnerID, null);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "InventoryCreate", User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                return RedirectToAction("Index");
            }

            var fitters = _repo.GetContext().ACSystemUser.ToList();
            ViewBag.Fitters = fitters;

            return View(inventory);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista narzędzi", Url = "/Inventory", Icon = "fa fa-wrench", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Edytuj narzędzie", Url = "/Inventory/Edit", Icon = "", Active = true });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inventory inventory = await _repo.GetInventory((int)id);
            if (inventory == null)
            {
                return HttpNotFound();
            }

            var fitters = _repo.GetContext().ACSystemUser.ToList();
            ViewBag.Fitters = fitters;

            return View(inventory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Inventory inventory)
        {
            ViewBag.Breadcrump = new List<BreadcrumpModel>();
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Lista narzędzi", Url = "/Inventory", Icon = "fa fa-wrench", Active = false });
            ViewBag.Breadcrump.Add(new BreadcrumpModel { Page = "Edytuj narzędzie", Url = "/Inventory/Edit", Icon = "", Active = true });

            var accessories = Request.Form["AccessoriesID[]"]?.ToIntArray(',') ?? new List<int>().ToArray();
            var files = Request.Form["Files[]"]?.Split(',') ?? new List<string>().ToArray();

            foreach (var accessory in accessories)
            {
                var connect = new Inventory_Accessories_Connect
                {
                    AccessoryID = accessory,
                    Accessory = _repo.GetContext().Inventory_Accessories.Find(accessory)
                };

                inventory.Inventory_Accessories_Connect.Add(connect);
            }

            foreach (var file in files)
            {
                var inventoryFile = new Inventory_Photo
                {
                    Name = file.Split(';')[0],
                    URL = file.Split(';')[1]
                };

                inventory.Inventory_Photo.Add(inventoryFile);
            }

            inventory.BuyPriceNetto = Decimal.Divide(inventory.BuyPriceBrutto, (decimal)1.23);
            
            if (inventory.EvidenceNumber.IsNullOrEmpty() && inventory.EvidenceNumber2.IsNullOrEmpty())
            {
                ModelState.AddModelError("EvidenceNumber", "Przynajmniej jeden numer ewidencyjny musi zostać uzupełniony");
                ModelState.AddModelError("EvidenceNumber2", "Przynajmniej jeden numer ewidencyjny musi zostać uzupełniony");
            }

            var isSNumerExist = _repo.GetContext().Inventory.Any(q => q.EvidenceNumber.Equals(inventory.EvidenceNumber) && q.ID != inventory.ID);

            if (isSNumerExist)
            {
                ModelState.AddModelError("EvidenceNumber", "Podany numer już istnieje.");
            }

            if (ModelState.IsValid)
            {
                var prevOwnerType = _repo.GetContext().Inventory.Where(q => q.ID == inventory.ID).Select(q => q.OwnerType)?.FirstOrDefault() ?? InventoryOwnerType.General;
                var prevOwner = _repo.GetContext().Inventory.Where(q => q.ID == inventory.ID).Select(q => q.OwnerID)?.FirstOrDefault() ?? "";

                _repo.Edit(inventory);

                try
                {
                    _repo.SaveChanges();

                    if ((prevOwnerType == InventoryOwnerType.General && inventory.OwnerType == Core.InventoryOwnerType.Private) || (prevOwner != inventory.OwnerID && inventory.OwnerType == Core.InventoryOwnerType.Private))
                    {
                        _repo.FreeInventory(inventory.ID, null);
                        _repo.GiveInventory(inventory.ID, inventory.OwnerID, null);
                    }
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                return RedirectToAction("Index");
            }

            var fitters = _repo.GetContext().ACSystemUser.ToList();
            ViewBag.Fitters = fitters;

            return View(inventory);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = "";

            for (int i = 0; i < 3; i++)
            {
                result = await _repo.Delete((int)id);

                if (result.Split(';')[0] == "true")
                    break;
            }

            var rsSplit = result.Split(';');

            if (rsSplit.Length > 1)
                return Json(new { success = rsSplit[0], message = rsSplit[1] }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Nieoczekiwany błąd." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthLog(Roles = "Inwentarz,Admin")]
        public ActionResult UploadImage()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var extension = new FileInfo(fileName);

                        newFileName += extension.Extension;

                        var filePath = Path.Combine(Server.MapPath("~/dist/Inventory_Images/"), newFileName);
                        file.SaveAs(filePath);
                    }

                    return Json(new { result = "true", message = newFileName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = "false", message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GiveInventory(int? InventoryID, string FitterID, int ProjectID)
        {
            if (!InventoryID.HasValue || string.IsNullOrEmpty(FitterID))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            _repo.GiveInventory(InventoryID.Value, FitterID, ProjectID);

            return Redirect("/Inventory/");
        }

        [HttpPost]
        public ActionResult FreeInventory(int? InventoryID, int WarehouseID)
        {
            if (!InventoryID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            _repo.FreeInventory(InventoryID.Value, WarehouseID);

            if (!Request.UrlReferrer.AbsolutePath.Contains("Inventory"))
                return Redirect(Request.UrlReferrer.AbsoluteUri);

            return Redirect("/Inventory/");
        }

        [HttpPost]
        public ActionResult SellInventory(int? InventoryID, decimal SellPrice)
        {
            if (!InventoryID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            _repo.SellInventory(InventoryID.Value, SellPrice);

            return Redirect("/Inventory/");
        }

        [HttpGet]
        public ActionResult Modal_GiveInventory(int? InventoryID)
        {
            if (!InventoryID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.InventoryID = InventoryID.Value;

            var projects = _repo.GetContext().Projects.ToList();
            ViewBag.Projects = projects;

            return PartialView(_repo.Fitters());
        }

        [HttpGet]
        public ActionResult Modal_FreeInventory(int? InventoryID)
        {
            if (!InventoryID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.InventoryID = InventoryID.Value;

            var warehouses = _repo.GetContext().InventoryWarehouse.ToList();

            return PartialView(warehouses);
        }

        [HttpGet]
        public ActionResult Modal_SellInventory(int? InventoryID)
        {
            if (!InventoryID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.InventoryID = InventoryID.Value;

            return PartialView();
        }

        #region AJAX

        [HttpPost]
        public ActionResult AJAX_DisactiveAccessory(int ID)
        {
            var accessory = _repo.GetContext().Inventory_Accessories_Connect.Find(ID);
            var removed = false;

            if (accessory.Active == false)
            {
                _repo.GetContext().Inventory_Accessories_Connect.Remove(accessory);
                removed = true;
            }
            else
            {
                accessory.Active = false;
                _repo.GetContext().Entry(accessory).State = EntityState.Modified;
            }

            try
            {
                _repo.SaveChanges();

                return Json(new { success = true, remove = removed }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "AJAX_DisactiveAccessory", User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AJAX_DeleteFile(int ID, string URL)
        {
            if (ID > 0)
            {
                var dbFile = _repo.GetContext().Inventory_Photo.Find(ID);

                _repo.GetContext().Inventory_Photo.Remove(dbFile);

                try
                {
                    _repo.SaveChanges();
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "AJAX_DeleteFile", User.Identity.Name);

                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }

            var filePath = Server.MapPath("~" + URL);

            if (System.IO.File.Exists(filePath))
            {
                try
                {
                    System.IO.File.Delete(filePath);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "AJAX_DeleteFile", User.Identity.Name);
                }
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AJAX_EditFileName(int ID, string Name)
        {
            var file = _repo.GetContext().Inventory_Photo.Find(ID);

            file.Name = Name;

            _repo.GetContext().Entry(file).State = EntityState.Modified;

            try
            {
                _repo.SaveChanges();

                return Json(new { ID, Name }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ACSystem.Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult AJAX_AddInventoryFile()
        {
            if (Request.Files.Count > 0)
            {
                var db = new ACSystemContext();

                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    Inventory_Photo inventoryPhoto = null;

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var fi = new FileInfo(fileName);
                        var ext = fi.Extension.ToUpper();
                        newFileName += fi.Extension;


                        var filePath = Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName);
                        file.SaveAs(filePath);

                        if (ext == ".JPG" || ext == ".JPEG" || ext == ".PNG" || ext == ".BMP")
                        {
                            newFileName = "resized_" + newFileName;

                            using (var image = Image.FromFile(filePath))
                            using (var newImage = Images.ScaleImage(image, 600, 800))
                            {
                                image.Dispose();
                                switch (ext)
                                {
                                    case ".JPG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".JPEG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".PNG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Png);
                                        break;
                                    case ".BMP":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/Monter/dist/Documents/"), newFileName), ImageFormat.Bmp);
                                        break;
                                }
                            }

                            System.IO.File.Delete(filePath);
                        }
                    }

                    return Json(new { result = "true", FileName = fileName, URL = "/dist/Documents/" + newFileName, ID = (inventoryPhoto != null) ? inventoryPhoto.ID : -1 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = "false", message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {

            }
            base.Dispose(disposing);
        }
    }
}
