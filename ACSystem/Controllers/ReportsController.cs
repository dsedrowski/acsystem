﻿using ACSystem.Core;
using ACSystem.Core.Extensions;
using ACSystem.Core.Models;
using ACSystem.Core.Service;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNet.Identity;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace ACSystem.Controllers
{
    public class ReportsController : Controller
    {
        #region HOURLY WORK REPORT

        public ActionResult HourlyWorkReport(int? ProjectID, int? ApartmentID)
        {
            if (!ProjectID.HasValue && !ApartmentID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var tasksList = new List<Tasks>();
            var apartmentsList = new List<Apartments>();
            Projects project = null;
            Apartments apartment = null;

            if (ProjectID.HasValue)
            {
                project = db.Projects.Find(ProjectID.Value);
                tasksList = db.Tasks.Where(q => q.Apartment.ProjectId == ProjectID.Value).ToList();
            }
            else if (ApartmentID.HasValue)
            {
                apartment = db.Apartments.Find(ApartmentID.Value);
                tasksList = db.Tasks.Where(q => q.ApartmentId == ApartmentID.Value).ToList();

                ProjectID = apartment.ProjectId;
            }

            apartmentsList = db.Apartments.Where(q => q.ProjectId == ProjectID).ToList();

            var workUncheckedList = new List<Task_Fitter>();
            var workAllList = new List<Task_Fitter>();

            var daysUncheckedList = new List<DateTime>();
            var daysAllList = new List<DateTime>();
            var doubleList = new Dictionary<int, List<Task_Fitter>>();

            foreach (var t in tasksList)
            {
                var workUnchecked = t.Task_Fitter.Where(q => q.DayChecked == false && q.IsActive);
                var workAll = t.Task_Fitter.Where(q => q.IsActive);

                workUncheckedList.AddRange(workUnchecked);
                workAllList.AddRange(workAll);

                foreach (var w in workAll)
                {
                    var d = workAll.Where(q => q.Timestamp.Date == w.Timestamp.Date && q.FitterId == w.FitterId && q.TaskId == w.TaskId && q.WorkDone == w.WorkDone && q.WorkTime == w.WorkTime && q.Task.IsHourlyPay && q.Status != TaskStatus.Free && q.Status != TaskStatus.UnderWork).ToList();

                    var alreadyInList = false;

                    foreach (var d2 in d)
                    {
                        if (doubleList.ContainsKey(d2.Id))
                            alreadyInList = true;

                        foreach (var c in doubleList.Keys)
                            if (doubleList[c].Any(q => q.Timestamp.Date == w.Timestamp.Date && q.FitterId == w.FitterId && q.TaskId == w.TaskId && q.WorkDone == w.WorkDone && q.WorkTime == w.WorkTime && q.Task.IsHourlyPay && q.Status != TaskStatus.Free && q.Status != TaskStatus.UnderWork))
                                alreadyInList = true;
                    }

                    if (d.Count() > 1 && !alreadyInList)
                        doubleList.Add(w.Id, d);
                }

            }

            daysUncheckedList = workUncheckedList.GroupBy(q => q.Timestamp.Date).Select(q => q.Key).ToList();
            daysAllList = workAllList.GroupBy(q => q.Timestamp.Date).Select(q => q.Key).ToList();

            ViewBag.Project = project;
            ViewBag.Apartment = apartment;
            ViewBag.Apartments = apartmentsList;
            ViewBag.DaysUncheckedList = daysUncheckedList;
            ViewBag.DaysAllList = daysAllList;
            ViewBag.DoubleList = doubleList;

            return View();
        }

        public ActionResult HourlyWorkReportDetails(DateTime? Day, int ProjectID = -1, int ApartmentID = -1)
        {
            if (!Day.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.CurrentDay = Day;

            var db = new ACSystemContext();

            var tasksList = new List<Tasks>();
            var workList = new List<Task_Fitter>();
            Projects project = null; Apartments apartment = null;

            if (ProjectID > 0)
            {
                project = db.Projects.Find(ProjectID);
                tasksList = db.Tasks.Where(q => q.Apartment.ProjectId == ProjectID).ToList();
            }
            else if (ApartmentID > 0)
            {
                apartment = db.Apartments.Find(ApartmentID);
                tasksList = db.Tasks.Where(q => q.ApartmentId == ApartmentID).ToList();
            }

            if (tasksList.Count() == 0)
                return View(workList);

            foreach (var t in tasksList)
                workList.AddRange(t.Task_Fitter.Where(q => q.IsActive && q.Timestamp.Date == Day.Value));

            ViewBag.Project = project;
            ViewBag.Apartment = apartment;

            return View(workList);
        }

        public ActionResult HourlyWorkReport_CheckDay(DateTime? Day, int ProjectID = -1, int ApartmentID = -1)
        {
            if (!Day.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var tasksList = new List<Tasks>();
            var workList = new List<Task_Fitter>();
            Projects project = null; Apartments apartment = null;

            var redirectURL = "/Reports/HourlyWorkReport?";

            if (ProjectID > 0)
            {
                project = db.Projects.Find(ProjectID);
                tasksList = db.Tasks.Where(q => q.Apartment.ProjectId == ProjectID).ToList();

                redirectURL += $"ProjectID={ProjectID}";
            }
            else if (ApartmentID > 0)
            {
                apartment = db.Apartments.Find(ApartmentID);
                tasksList = db.Tasks.Where(q => q.ApartmentId == ApartmentID).ToList();

                redirectURL += $"ApartmentID={ApartmentID}";
            }


            if (tasksList.Count() == 0)
                return Redirect(redirectURL);

            foreach (var t in tasksList)
                workList.AddRange(t.Task_Fitter.Where(q => q.IsActive && q.Timestamp.Date == Day.Value && !q.DayChecked));

            foreach (var w in workList)
            {
                w.DayChecked = true;

                db.Entry(w).State = EntityState.Modified;
            }

            db.SaveChanges();

            return Redirect(redirectURL);
        }

        public ActionResult HorulyWorkReport_DisactiveWork(int? ActivityID, string IDType, int ID = -1)
        {
            if (!ActivityID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var activity = db.Task_Fitter.Find(ActivityID.Value);

            activity.IsActive = false;

            db.Entry(activity).State = EntityState.Modified;

            db.SaveChanges();

            return Redirect($"/Reports/HourlyWorkReport?{IDType}={ID}");
        }

        [HttpPost]
        [Obsolete]
        public ActionResult HourlyWorkReport_GenerateExcel(int ProjectID, int[] Products, string[] Fitters, DateTime DateFrom, DateTime DateTo)
        {
            var db = new ACSystemContext();

            var project = db.Projects.Find(ProjectID);
            var activities = db.Task_Fitter.Where(
                                        q =>
                                        q.IsActive &&
                                        q.Task.Apartment.ProjectId == ProjectID &&
                                        ((q.HandDateTime.HasValue && EntityFunctions.TruncateTime(q.HandDateTime.Value) >= DateFrom) || (q.HandDateTime.HasValue == false && EntityFunctions.TruncateTime(q.Timestamp) >= DateFrom)) &&
                                        ((q.HandDateTime.HasValue && EntityFunctions.TruncateTime(q.HandDateTime.Value) <= DateTo) || (q.HandDateTime.HasValue == false && EntityFunctions.TruncateTime(q.Timestamp) <= DateTo)) &&
                                        Products.Contains(q.Task.ProductId) &&
                                        Fitters.Contains(q.FitterId) &&
                                        q.Status != TaskStatus.Free &&
                                        q.Status != TaskStatus.UnderWork &&
                                        (q.WorkDone > 0 || q.WorkTime > 0)
                                    );

            var excel = new XLWorkbook();

            foreach (var p in Products)
            {
                var product = db.Products.Find(p);

                if (product == null)
                    continue;

                var worksheetName = (product.Name.Length > 31) ? product.Name.Substring(0, 31) : product.Name;

                excel.AddWorksheet(product.Name);

                var data = activities.Where(q => q.Task.ProductId == p);

                var sheet = excel.Worksheet(product.Name);

                var daysCount = (DateTo - DateFrom).Days;

                var fittersCol = sheet.Column(1);
                var fittersDict = new Dictionary<string, decimal>();
                var fittersMoneyDict = new Dictionary<string, decimal>();
                var fittersCountDict = new Dictionary<string, decimal>();
                decimal allSum = 0, allCountSum = 0, allMoneySum = 0;

                int fi = 2, nextTable = 2, nextTable2 = 2, nextTableSumCol = 2, nextTableSumCol2 = 2;
                foreach (var f in Fitters)
                {
                    var fitter = db.Fitters.Find(f);

                    fittersDict.Add(f, 0);

                    fittersCol.Width = 20;
                    fittersCol.Cell(fi).Style.Fill.BackgroundColor = XLColor.LightGray;
                    fittersCol.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    fittersCol.Cell(fi).Style.Border.OutsideBorderColor = XLColor.Black;
                    fittersCol.Cell(fi).Value = fitter.User.FullName;

                    fi++;
                }

                fittersCol.Cell(fi).Style.Fill.BackgroundColor = XLColor.Gray;
                fittersCol.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                fittersCol.Cell(fi).Style.Border.OutsideBorderColor = XLColor.LightGray;
                fittersCol.Cell(fi).Value = "SUMA";

                fittersCol.Cell(1).Style.Fill.BackgroundColor = XLColor.Gray;
                fittersCol.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                fittersCol.Cell(1).Style.Border.OutsideBorderColor = XLColor.LightGray;
                fittersCol.Cell(1).Value = "Godziny";

                var currentDay = DateFrom.AddDays(-1);
                fi = 2;
                for (var i = 0; i <= daysCount; i++)
                {
                    var col = sheet.Column(i + 2);
                    currentDay = currentDay.AddDays(1);

                    col.Width = 4;
                    col.Cell(1).Style.Alignment.TopToBottom = true;
                    col.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
                    col.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    col.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
                    col.Cell(1).Value = currentDay.ToString("yyyy-MM-dd");

                    var dayData = data.Where(q => (q.HandDateTime.HasValue && EntityFunctions.TruncateTime(q.HandDateTime) == EntityFunctions.TruncateTime(currentDay)) || (q.HandDateTime.HasValue == false && EntityFunctions.TruncateTime(q.Timestamp) == EntityFunctions.TruncateTime(currentDay)));

                    decimal daySum = 0;
                    decimal dayMoneySum = 0;
                    decimal dayCountSum = 0;

                    foreach (var f in Fitters)
                    {
                        decimal? hours = 0;
                        var comment = "";

                        if (dayData != null && dayData.Count() > 0)
                        {
                            try
                            {
                                var dataForFitter = dayData?.Where(q => q.FitterId == f);

                                if (dataForFitter != null && dataForFitter.Count() > 0)
                                {
                                    List<string> commentArray = new List<string>();

                                    foreach (var d in dataForFitter)
                                    {
                                        var apartment = d.Task.Apartment;

                                        var taskLp = 1;
                                        var taskDict = new Dictionary<int, int>();

                                        foreach (var t in apartment.Tasks)
                                        {
                                            taskDict.Add(t.Id, taskLp);
                                            taskLp++;
                                        }

                                        var commentString = $"LGH {d.Task.Apartment.Letter}{d.Task.Apartment.NumberOfApartment} Zadanie {taskDict[d.TaskId]}";

                                        if (!commentArray.Contains(commentString))
                                            commentArray.Add(commentString);
                                    }

                                    comment = string.Join(",", commentArray);

                                    hours = dataForFitter.Sum(q => q.WorkTime);
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                        }

                        col.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        col.Cell(fi).Style.Border.OutsideBorderColor = XLColor.Black;
                        col.Cell(fi).Value = (hours != null) ? hours / 60 : 0;

                        if (!string.IsNullOrEmpty(comment))
                            col.Cell(fi).Comment.AddText(comment);

                        daySum += (hours != null) ? hours.Value / 60 : 0;
                        fittersDict[f] += (hours != null) ? hours.Value / 60 : 0;
                        allSum += (hours != null) ? hours.Value / 60 : 0;

                        fi++;
                    }

                    col.Cell(fi).Style.Fill.BackgroundColor = XLColor.LightGray;
                    col.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    col.Cell(fi).Style.Border.OutsideBorderColor = XLColor.Black;
                    col.Cell(fi).Value = daySum;

                    nextTable = fi + 2;
                    nextTableSumCol = fi + 2;

                    #region ILOŚC PRODUKTÓW

                    col.Width = 4;
                    col.Cell(nextTable).Style.Alignment.TopToBottom = true;
                    col.Cell(nextTable).Style.Fill.BackgroundColor = XLColor.LightGray;
                    col.Cell(nextTable).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    col.Cell(nextTable).Style.Border.OutsideBorderColor = XLColor.Black;
                    col.Cell(nextTable).Value = currentDay.ToString("yyyy-MM-dd");

                    fittersCol.Cell(nextTable).Style.Fill.BackgroundColor = XLColor.Gray;
                    fittersCol.Cell(nextTable).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    fittersCol.Cell(nextTable).Style.Border.OutsideBorderColor = XLColor.LightGray;
                    fittersCol.Cell(nextTable).Value = "Ilości";

                    fi = nextTable + 1;

                    foreach (var f in Fitters)
                    {
                        var fitter = db.Fitters.Find(f);

                        if (!fittersCountDict.Keys.Contains(f))
                            fittersCountDict.Add(f, 0);

                        fittersCol.Width = 20;
                        fittersCol.Cell(fi).Style.Fill.BackgroundColor = XLColor.LightGray;
                        fittersCol.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        fittersCol.Cell(fi).Style.Border.OutsideBorderColor = XLColor.Black;
                        fittersCol.Cell(fi).Value = fitter.User.FullName;

                        decimal count = 0;
                        var comment = "";

                        if (dayData != null && dayData.Count() > 0)
                        {
                            try
                            {
                                var dataForFitter = dayData?.Where(q => q.FitterId == f);

                                if (dataForFitter != null && dataForFitter.Count() > 0)
                                {
                                    List<string> commentArray = new List<string>();

                                    foreach (var d in dataForFitter)
                                    {
                                        var apartment = d.Task.Apartment;

                                        var taskLp = 1;
                                        var taskDict = new Dictionary<int, int>();

                                        foreach (var t in apartment.Tasks)
                                        {
                                            taskDict.Add(t.Id, taskLp);
                                            taskLp++;
                                        }

                                        var commentString = $"LGH {d.Task.Apartment.Letter}{d.Task.Apartment.NumberOfApartment} Zadanie {taskDict[d.TaskId]}";

                                        if (!commentArray.Contains(commentString))
                                            commentArray.Add(commentString);
                                    }

                                    comment = string.Join(",", commentArray);

                                    count = dataForFitter.Sum(q => q.WorkDone);
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                        }

                        col.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        col.Cell(fi).Style.Border.OutsideBorderColor = XLColor.Black;
                        col.Cell(fi).Value = count;

                        if (!string.IsNullOrEmpty(comment))
                            col.Cell(fi).Comment.AddText(comment);

                        dayCountSum += count;
                        fittersCountDict[f] += count;
                        allCountSum += count;

                        fi++;
                    }

                    fittersCol.Cell(fi).Style.Fill.BackgroundColor = XLColor.Gray;
                    fittersCol.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    fittersCol.Cell(fi).Style.Border.OutsideBorderColor = XLColor.LightGray;
                    fittersCol.Cell(fi).Value = "SUMA";

                    col.Cell(fi).Style.Fill.BackgroundColor = XLColor.LightGray;
                    col.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    col.Cell(fi).Style.Border.OutsideBorderColor = XLColor.Black;
                    col.Cell(fi).Value = dayCountSum;

                    #endregion

                    nextTable2 = fi + 2;
                    nextTableSumCol2 = fi + 2;

                    col.Width = 4;
                    col.Cell(nextTable2).Style.Alignment.TopToBottom = true;
                    col.Cell(nextTable2).Style.Fill.BackgroundColor = XLColor.LightGray;
                    col.Cell(nextTable2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    col.Cell(nextTable2).Style.Border.OutsideBorderColor = XLColor.Black;
                    col.Cell(nextTable2).Value = currentDay.ToString("yyyy-MM-dd");

                    fittersCol.Cell(nextTable2).Style.Fill.BackgroundColor = XLColor.Gray;
                    fittersCol.Cell(nextTable2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    fittersCol.Cell(nextTable2).Style.Border.OutsideBorderColor = XLColor.LightGray;
                    fittersCol.Cell(nextTable2).Value = "Kwoty";

                    fi = nextTable2 + 1;

                    foreach (var f in Fitters)
                    {
                        var fitter = db.Fitters.Find(f);

                        if (!fittersMoneyDict.Keys.Contains(f))
                            fittersMoneyDict.Add(f, 0);

                        fittersCol.Width = 20;
                        fittersCol.Cell(fi).Style.Fill.BackgroundColor = XLColor.LightGray;
                        fittersCol.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        fittersCol.Cell(fi).Style.Border.OutsideBorderColor = XLColor.Black;
                        fittersCol.Cell(fi).Value = fitter.User.FullName;

                        decimal money = 0;
                        var comment = "";

                        if (dayData != null && dayData.Count() > 0)
                        {
                            try
                            {
                                var dataForFitter = dayData?.Where(q => q.FitterId == f);

                                if (dataForFitter != null && dataForFitter.Count() > 0)
                                {
                                    List<string> commentArray = new List<string>();

                                    foreach (var d in dataForFitter)
                                    {
                                        var apartment = d.Task.Apartment;

                                        var taskLp = 1;
                                        var taskDict = new Dictionary<int, int>();

                                        foreach (var t in apartment.Tasks)
                                        {
                                            taskDict.Add(t.Id, taskLp);
                                            taskLp++;
                                        }

                                        var commentString = $"LGH {d.Task.Apartment.Letter}{d.Task.Apartment.NumberOfApartment} Zadanie {taskDict[d.TaskId]}";

                                        if (!commentArray.Contains(commentString))
                                            commentArray.Add(commentString);
                                    }

                                    comment = string.Join(",", commentArray);

                                    money = dataForFitter.Sum(q => q.WorkDone) * dataForFitter.FirstOrDefault().Task.ProductPrice;
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                        }

                        col.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        col.Cell(fi).Style.Border.OutsideBorderColor = XLColor.Black;
                        col.Cell(fi).Value = money;

                        if (!string.IsNullOrEmpty(comment))
                            col.Cell(fi).Comment.AddText(comment);

                        dayMoneySum += money;
                        fittersMoneyDict[f] += money;
                        allMoneySum += money;

                        fi++;
                    }

                    fittersCol.Cell(fi).Style.Fill.BackgroundColor = XLColor.Gray;
                    fittersCol.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    fittersCol.Cell(fi).Style.Border.OutsideBorderColor = XLColor.LightGray;
                    fittersCol.Cell(fi).Value = "SUMA";

                    col.Cell(fi).Style.Fill.BackgroundColor = XLColor.LightGray;
                    col.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    col.Cell(fi).Style.Border.OutsideBorderColor = XLColor.Black;
                    col.Cell(fi).Value = dayMoneySum;

                    fi = 2;
                }

                var colLast = sheet.Column(daysCount + 3);
                colLast.Width = 4;
                nextTable++;
                nextTable2++;

                foreach (var f in Fitters)
                {
                    colLast.Cell(1).Style.Alignment.TopToBottom = true;
                    colLast.Cell(1).Style.Fill.BackgroundColor = XLColor.LightGray;
                    colLast.Cell(1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    colLast.Cell(1).Style.Border.OutsideBorderColor = XLColor.Black;
                    colLast.Cell(1).Value = "SUMA";

                    colLast.Cell(fi).Style.Fill.BackgroundColor = XLColor.LightGray;
                    colLast.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    colLast.Cell(fi).Style.Border.OutsideBorderColor = XLColor.Black;
                    colLast.Cell(fi).Value = fittersDict[f];

                    colLast.Cell(nextTableSumCol).Style.Alignment.TopToBottom = true;
                    colLast.Cell(nextTableSumCol).Style.Fill.BackgroundColor = XLColor.LightGray;
                    colLast.Cell(nextTableSumCol).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    colLast.Cell(nextTableSumCol).Style.Border.OutsideBorderColor = XLColor.Black;
                    colLast.Cell(nextTableSumCol).Value = "SUMA";

                    colLast.Cell(nextTable).Style.Fill.BackgroundColor = XLColor.LightGray;
                    colLast.Cell(nextTable).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    colLast.Cell(nextTable).Style.Border.OutsideBorderColor = XLColor.Black;
                    colLast.Cell(nextTable).Value = fittersCountDict[f];

                    colLast.Cell(nextTableSumCol2).Style.Alignment.TopToBottom = true;
                    colLast.Cell(nextTableSumCol2).Style.Fill.BackgroundColor = XLColor.LightGray;
                    colLast.Cell(nextTableSumCol2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    colLast.Cell(nextTableSumCol2).Style.Border.OutsideBorderColor = XLColor.Black;
                    colLast.Cell(nextTableSumCol2).Value = "SUMA";

                    colLast.Cell(nextTable2).Style.Fill.BackgroundColor = XLColor.LightGray;
                    colLast.Cell(nextTable2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    colLast.Cell(nextTable2).Style.Border.OutsideBorderColor = XLColor.Black;
                    colLast.Cell(nextTable2).Value = fittersMoneyDict[f];

                    fi++;
                    nextTable++;
                    nextTable2++;
                }

                colLast.Cell(fi).Style.Fill.BackgroundColor = XLColor.LightGray;
                colLast.Cell(fi).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                colLast.Cell(fi).Style.Border.OutsideBorderColor = XLColor.Black;
                colLast.Cell(fi).Value = allSum;

                colLast.Cell(nextTable).Style.Fill.BackgroundColor = XLColor.LightGray;
                colLast.Cell(nextTable).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                colLast.Cell(nextTable).Style.Border.OutsideBorderColor = XLColor.Black;
                colLast.Cell(nextTable).Value = allCountSum;

                colLast.Cell(nextTable2).Style.Fill.BackgroundColor = XLColor.LightGray;
                colLast.Cell(nextTable2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                colLast.Cell(nextTable2).Style.Border.OutsideBorderColor = XLColor.Black;
                colLast.Cell(nextTable2).Value = allMoneySum;
            }

            var fileName = $"RaportGodzin-{project.InSystemID}-{DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss")}.xlsx";

            var filePath = Path.Combine(Server.MapPath("~/dist/HourRaport"), fileName);

            try
            {
                excel.SaveAs(filePath);

                return Json(new { success = true, file = fileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }


            return null;
        }

        #endregion

        #region PLACE ERROR

        public ActionResult PlaceError_ClientReport(int? _placeErrorID)
        {
            if (!_placeErrorID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            using (ACSystemContext db = new ACSystemContext())
            {
                var errorPhotos = db.PlaceError_Photo.Where(q => q.PlaceErrorID == _placeErrorID).ToList();
                var placeError = db.PlaceError.Find(_placeErrorID);
                var user = placeError.Fitter;

                var userSign = new Uri(Server.MapPath("~/Monter/dist/Podpisy/" + user.Podpis)).AbsoluteUri;

                System.Data.DataTable table = new System.Data.DataTable();
                table.Columns.Add(new System.Data.DataColumn("filepath1"));
                table.Columns.Add(new System.Data.DataColumn("filepath2"));

                var i = 0;
                var j = 0;
                System.Data.DataRow row = null;

                foreach (var e in errorPhotos)
                {
                    j++;

                    if (i == 0)
                        row = table.NewRow();

                    var filePath = new Uri(Server.MapPath("/Monter" + e.Photo)).AbsoluteUri;
                    row["filepath" + (i + 1)] = filePath;

                    if (i == 0 && j < errorPhotos.Count())
                        i = 1;
                    else
                    {
                        if (j == 0 && j == errorPhotos.Count())
                            row["filepath2"] = null;

                        i = 0;
                        table.Rows.Add(row);
                        row = null;
                    }
                }

                Assembly assembly = Assembly.LoadFrom(Server.MapPath("~/bin/ACSystem.Reports.dll"));

                Warning[] warnings;
                string error = "";

                var pdfPath = Path.Combine(Server.MapPath("~/dist/PlaceErrors"), $"{_placeErrorID}.pdf");

                var success = ReportingService.PlaceError.ClientReport(_placeErrorID.Value, userSign, table, assembly, pdfPath, out warnings, out error);

                return Json(new { success, error, warnings }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region GROUPED REPORT

        public ActionResult Project_GroupedReport(string Reports, bool Send = false)
        {
            if (string.IsNullOrEmpty(Reports))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Assembly assembly = Assembly.LoadFrom(Server.MapPath("~/bin/ACSystem.Reports.dll"));

            Warning[] warnings;
            string error = "";
            var pdfName = $"RaportGrupowy-{DateTime.Now.ToString("yyyy-MM-dd-HH-mm")}.pdf";
            var pdfPath = Path.Combine(Server.MapPath("~/dist/GroupReports"), pdfName);

            var success = ReportingService.GroupedReport.Report(Reports, assembly, pdfPath, out warnings, out error);

            if (success && Send)
            {
                var IDs = Reports.ToIntArray(',');
                using (var db = new ACSystemContext())
                {
                    string exception;
                    var report = db.ReportNag.Find(IDs[0]);
                    success = MailService.ApartmentGroupRaport(report, pdfPath, out exception);
                }                
            }

            return Json(new { success, error, warnings, pdfName, date = DateTime.Now.ToString("yyyy-MM-dd") }, JsonRequestBehavior.AllowGet);
        }   

        public ActionResult Project_CheckReportsAsPrinted(string Reports)
        {
            if (string.IsNullOrEmpty(Reports))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var IDs = Reports.ToIntArray(',');

            using (var db = new ACSystemContext())
            {
                var reports = db.ReportNag.Where(q => q.Printed == null && IDs.Contains(q.ID));
                reports.ForEach(x =>
                {
                    x.Printed = true;

                    db.Entry(x).State = EntityState.Modified;
                });

                db.SaveChanges();
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult Project_CheckReportsAsSended(string Reports)
        {
            if (string.IsNullOrEmpty(Reports))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var IDs = Reports.ToIntArray(',');

            using (var db = new ACSystemContext())
            {
                var reports = db.ReportNag.Where(q => q.Printed == null && IDs.Contains(q.ID));
                reports.ForEach(x =>
                {
                    x.SendDate = DateTime.Now;

                    db.Entry(x).State = EntityState.Modified;
                });

                db.SaveChanges();
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}