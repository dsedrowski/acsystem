﻿using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ACSystem.Controllers
{
    public class QuestionController : Controller
    {
        public ActionResult Index()
        {
            var questionsType = new List<QuestionType>();
            var questions = new List<Question>();
            var answersDict = new Dictionary<int, List<Answer>>();

            GetDataForMainPage(out questionsType, out questions, out answersDict);

            ViewBag.Types = questionsType;
            ViewBag.AnswersForFirstQuestion = answersDict;

            return View(questions);
        }

        [HttpPost]
        public ActionResult Index(QuestionType _data)
        {
            if (_data == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            using (var db = new ACSystemContext())
            {
                db.QuestionType.Add(_data);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            return Redirect(Request.RawUrl + "#afterSubmit");
        }

        private void GetDataForMainPage(out List<QuestionType> questionsType, out List<Question> questions, out Dictionary<int, List<Answer>> answersDict)
        {
            var _questionsType = new List<QuestionType>();
            var _questions = new List<Question>();
            var _answersDict = new Dictionary<int, List<Answer>>();

            using (var db = new ACSystemContext())
            {
                _questionsType = db.QuestionType.ToList();
                _questions = db.Question.ToList();

                var firstQuestionID = _questions.FirstOrDefault()?.ID ?? -1;

                foreach (var type in _questionsType)
                {
                    var firstQuestionForType = _questions.FirstOrDefault(q => q.QuestionTypeID == type.ID);

                    if (firstQuestionForType != null)
                    {
                        var answers = db.Answer.Where(q => q.QuestionID == firstQuestionForType.ID).ToList();
                        _answersDict.Add(type.ID, answers);
                    }
                    else
                        _answersDict.Add(type.ID, new List<Answer>());
                }
            }

            questionsType = _questionsType;
            questions = _questions;
            answersDict = _answersDict;
        }

        public ActionResult AnswersForQuestion(int ID)
        {
            using (var db = new ACSystemContext())
            {
                var list = db.Answer.Where(q => q.QuestionID == ID).Select(q => 
                            new
                            {
                                q.AnswerPol,
                                q.AnswerTranslate,
                                q.QuestionID,
                                q.ID,
                                ExtraField_Get = (q.ExtraFieldNotTT) ? "Bez tłumaczenia" : (q.ExtraFieldTT) ? "Do tłumaczenia" : "Brak info",
                                IsError = (q.IsError == true) ? "Tak" : "Nie",
                                IsNotApplicable = q.IsNotApplicable == true ? "Tak" : "Nie"
                            }).ToList();

                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddQuestion(Question _data)
        {
            using (var db = new ACSystemContext())
            {
                db.Question.Add(_data);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            return Json(_data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddAnswer(Answer _data)
        {
            switch (_data.ExtraFields)
            {
                case "tt":
                    _data.ExtraFieldTT = true;
                    break;
                case "ntt":
                    _data.ExtraFieldNotTT = true;
                    break;
            }

            using (var db = new ACSystemContext())
            {
                db.Answer.Add(_data);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            return Json(_data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int ID)
        {
            using (var db = new ACSystemContext())
            {
                var question = db.Question.Find(ID);

                db.Question.Remove(question);

                try
                {
                    db.SaveChanges();

                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        public ActionResult DeleteAnswer(int ID)
        {
            using (var db = new ACSystemContext())
            {
                var answer = db.Answer.Find(ID);

                db.Answer.Remove(answer);

                try
                {
                    db.SaveChanges();

                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        #region QuestionsForProject

        public ActionResult QuestionsForProduct(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            using (ACSystemContext db = new ACSystemContext())
            {
                Products product = db.Products.Find(ID);

                if (product == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                List<Product_Question> list = db.Product_Question.Where(q => q.ProductID == ID).ToList();

                var questionsType = new List<QuestionType>();
                var questions = new List<Question>();
                var answersDict = new Dictionary<int, List<Answer>>();

                GetDataForMainPage(out questionsType, out questions, out answersDict);

                ViewBag.Types = questionsType;
                ViewBag.AnswersForFirstQuestion = answersDict;
                ViewBag.Product = product;
                ViewBag.Product_Questions = list;

                return View(questions);
            }
        }

        public ActionResult ConnectQuestionToProduct(int QuestionID, int ProductID)
        {
            using (var db = new ACSystemContext())
            {
                db.Database.Log = Console.WriteLine;
                var data = db.Product_Question.FirstOrDefault(q => q.QuestionID == QuestionID && q.ProductID == ProductID);

                if (data == null)
                {
                    data = new Product_Question
                    {
                        QuestionID = QuestionID,
                        ProductID = ProductID
                    };

                    db.Product_Question.Add(data);
                }
                else
                {
                    db.Product_Question.Remove(data);
                }

                try
                {
                    db.SaveChanges();

                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        #endregion

        #region QuestionsForProductProject

        public ActionResult QuestionsForProductProject(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            using (var db = new ACSystemContext())
            {
                var product_project = db.Product_Project.Find(ID);

                if (product_project == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                var question_product_project = db.Question_Product_Project.Where(q => q.Project_ProductID == ID).ToList();
                List<int> product_questions_list = product_project.Product.Product_Questions.Where(q => q.Question != null).Select(q => q.QuestionID).ToList();
                List<int> question_types = product_project.Product.Product_Questions.Where(q => q.Question != null).Select(q => q.Question.QuestionTypeID).ToList();

                var questionsType = new List<QuestionType>();
                var questions = new List<Question>();
                var answersDict = new Dictionary<int, List<Answer>>();

                GetDataForMainPage(out questionsType, out questions, out answersDict);

                ViewBag.Types = questionsType.Where(q => question_types.Contains(q.ID)).ToList();
                ViewBag.AnswersForFirstQuestion = answersDict;
                ViewBag.Product_Project = product_project;
                ViewBag.Question_Product_Project = question_product_project;
                ViewBag.Product_Questions_List = product_questions_list;


                return View(questions);
            }
                
        }

        public ActionResult ConnectQuestionToProductProject(int QuestionID, int Product_ProjectID)
        {
            using (var db = new ACSystemContext())
            {
                var data = db.Question_Product_Project.FirstOrDefault(q => q.QuestionID == QuestionID && q.Project_ProductID == Product_ProjectID);

                if (data == null)
                {
                    data = new Question_Product_Project
                    {
                        QuestionID = QuestionID,
                        Project_ProductID = Product_ProjectID
                    };

                    db.Question_Product_Project.Add(data);
                }
                else
                {
                    db.Question_Product_Project.Remove(data);
                }

                try
                {
                    db.SaveChanges();

                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        #endregion
    }
}