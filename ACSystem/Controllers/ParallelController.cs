﻿using ACSystem.Core.Filters;
using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using ACSystem.Core.Models.ViewModels.Select;
using ACSystem.Core.Service;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Linq.Expressions;
using ACSystem.Core;
using ClosedXML.Excel;
using ACSystem.Core.Extensions;
using System.IO;
using ThreadTask = System.Threading.Tasks;

namespace ACSystem.Controllers
{
    [AuthLog(Roles = "Admin")]
    public class ParallelController : Controller
    {
        private readonly ACSystemContext _db = new ACSystemContext();

        #region GODZINY

        public ActionResult Hours(int? month, int? year)
        {
            if (!month.HasValue)
                month = DateTime.Now.Month;

            if (!year.HasValue)
                year = DateTime.Now.Year;

            ViewBag.Month = month.Value;
            ViewBag.Year = year.Value;

            var fitters = _db.Fitters.Where(u => u.User.LockoutEnabled != true).OrderBy(u => u.User.Surname).ToList();

            return View(fitters);
        }

        [HttpPost]
        public ActionResult Hours(int month, int year, string search, int? projectID)
        {
            ViewBag.Month = month;
            ViewBag.Year = year;

            var fitters = _db.Fitters.Where(u => (u.User.Name.Contains(search) || u.User.Surname.Contains(search)) && u.User.LockoutEnabled != true)
                                     .OrderBy(u => u.User.Surname).ToList();

            return View(fitters);
        }

        [HttpGet()]
        public async ThreadTask.Task<ActionResult> GenerateHours(int year, int month) {
            var settings = _db.Settings.FirstOrDefault();
            var fitters = _db.Fitters.Where(u => u.User.LockoutEnabled != true);

            await fitters.ForEachAsync(fitter => {
                var user = fitter.User;

                for (int i = 1; i <= DateTime.DaysInMonth(year, month); i++) {
                    DateTime date = new DateTime(year, month, i);
                    bool isWeekend = date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;

                    if (!user.Fitter_Hours.Any(e => e.Date.Date == date.Date)) {
                        user.Fitter_Hours.Add(new Fitter_Hours {
                            Date = date,
                            Hours = (!isWeekend) ? settings.HoursPerDay : 0,
                            FreeDay = false
                        });

                        _db.Entry(user).State = EntityState.Modified;
                    }
                }
            });

            try {
                _db.SaveChanges();

                return RedirectToAction($"Hours", new { year, month });
            } catch (Exception) {
                throw;
            }
        }

        public class SaveDayData {
            public int id { get; set; }
            public decimal hours { get; set; }
            public bool freeDay { get; set; }
        }

        [HttpPost]
        public async ThreadTask.Task<ActionResult> SaveDay(SaveDayData data) {
            var entity = await _db.Fitter_Hours.FindAsync(data.id);

            if (entity == null)
                return Json(new { success = false });

            try {
                entity.Hours = (!data.freeDay) ? data.hours : 0;
                entity.FreeDay = data.freeDay;

                _db.Entry(entity).State = EntityState.Modified;
                await _db.SaveChangesAsync();

                return Json(new { success = true });
            } catch (Exception) {
                return Json(new { success = false });
            }
        }


        #endregion

        public ActionResult Content()
        {
            var projects = _db.Projects.Select(q => new ProjectsSelect { ID = q.Id, ProjectNumber = q.ProjectNumber}).ToList();

            ViewBag.ProjectsList = projects;

            return View();
        }

        public ActionResult FittersLocation()
        {
            var fitters = _db.Fitters.Where(q => q.TaskAlreadyWork != null).ToList();

            return View(fitters);
        }

        [HttpPost]
        public ActionResult Partial_ContentElements(int projectId)
        {
            var project = _db.Projects.Find(projectId);
            var settings = _db.Settings.FirstOrDefault();

            ViewBag.Percent = settings.InvoicePercent;
            ViewBag.InvoiceConverted = _db.ProjectInvoicesPercent_Elements.Where(q => q.ProjectID == project.Id).ToList().Sum(q => q.Amount);

            return PartialView("Partials/Partial_ContentElements", project);
        }

        #region PROJECT INVOICE PERCENT
        public ActionResult InvoicePercentCreate()
        {
            var list = new List<ProjectInvoicesPercent_Elements>();
            var newPercent = new ProjectInvoicesPercent();

            using (var db = new ACSystemContext())
            {
                var percent = (decimal)(db.Settings.FirstOrDefault()?.InvoicePercent ?? 0) / 100;
                var invoices = db.Invoice.Where(q => (q.IsPercentGenerated == null || q.IsPercentGenerated == false) && q.Complete == true && q.Project.TakePercent == true);

                if (invoices == null || invoices.Any() == false) return Redirect("/Parallel/InvoicePercentList");

                foreach (var inv in invoices)
                {
                    if (inv.Amount <= 0) continue;

                    if (list.Any(q => q.ProjectID == inv.ProjectID))
                    {
                        var elem = list.FirstOrDefault(q => q.ProjectID == inv.ProjectID);

                        elem.Amount += Math.Ceiling(inv.Amount * percent);
                    }
                    else
                    {
                        var newElem = new ProjectInvoicesPercent_Elements
                        {
                            ProjectID = inv.ProjectID,
                            ProjectNumber = inv.Project.InSystemID ?? inv.ProjectID.ToString(),
                            ProjectName = inv.Project.ProjectNumber,
                            Amount = Math.Ceiling(inv.Amount * percent)
                        };

                        list.Add(newElem);
                    }
                }

                newPercent = new ProjectInvoicesPercent
                {
                    GenerateDate = DateTime.Now,
                    AmountSum = list.Sum(q => q.Amount),
                    CalcPercent = (int)(percent * 100),
                    IsActive = false,
                    Element = list
                };

                db.ProjectInvoicesPercent.Add(newPercent);

                try
                {
                    db.SaveChanges();
                }
                catch(Exception ex)
                {
                    var log = new ExceptionsLog
                    {
                        Message = ex.Message,
                        Type = ex.GetType().ToString(),
                        Source = ex.Source,
                        StackTrace = ex.StackTrace,
                        URL = "InvoicePercentCreate",
                        LogDate = DateTime.Now,
                        User = User.Identity.Name
                    };

                    db.ExceptionsLog.Add(log);

                    db.SaveChanges();
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            return View(newPercent);
        }

        [HttpPost]
        public ActionResult InvoicePercentCreate(int ID)
        {
            using (var db = new ACSystemContext())
            {
                var model = db.ProjectInvoicesPercent.Find(ID);

                if (model == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                foreach (var m in model.Element)
                {
                    var invoices = db.Invoice.Where(q => q.ProjectID == m.ProjectID && (q.IsPercentGenerated == null || q.IsPercentGenerated == false) && q.Complete == true).ToList();

                    invoices.ForEach(q => q.IsPercentGenerated = true);
                }

                model.IsActive = true;

                db.Entry(model).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    var log = new ExceptionsLog
                    {
                        Message = ex.Message,
                        Type = ex.GetType().ToString(),
                        Source = ex.Source,
                        StackTrace = ex.StackTrace,
                        URL = "InvoicePercentCreate",
                        LogDate = DateTime.Now,
                        User = User.Identity.Name
                    };

                    db.ExceptionsLog.Add(log);

                    db.SaveChanges();
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                return Redirect("/Parallel/InvoicePercentList");
            }
        }

        [HttpGet]
        public ActionResult InvoicePercentDetails(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var model = new ProjectInvoicesPercent();

            var db = new ACSystemContext();

            model = db.ProjectInvoicesPercent.Find(ID);

            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            model.Element = db.ProjectInvoicesPercent_Elements.Where(q => q.ProjectInvoicesPercentID == model.ID).OrderBy(q => q.ProjectNumber).ToList();

            ViewBag.ShowSave = false;

            return View("InvoicePercentCreate", model);
        }

        public ActionResult InvoicePercentList()
        {
            return View();
        }

        #region AJAX

        public ActionResult AJAX_InvoicePercentList(int? page, string orderBy = "date", string dest = "desc")
        {
            using (var db = new ACSystemContext())
            {
                int pageSize = 20,
                    rowsCount = db.ProjectInvoicesPercent.Count(q => q.IsActive),
                    pagesCount = rowsCount / pageSize;

                if (rowsCount <= pageSize || page <= 0 || !page.HasValue)
                    page = 1;

                int excludeRows = (page.Value - 1) * pageSize,
                    rows = 0;

                var list = InvoicePercent_GetList(db, out rows, excludeRows, pageSize, orderBy, dest);

                pagesCount = (rows % pageSize == 0) ? rows / pageSize : (rows / pageSize) + 1;

                if (pagesCount <= 0) pagesCount = 1;

                return Json(new { data = list, pages = pagesCount }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region SERVICES

        private List<ProjectInvoicesPercentViewModel> InvoicePercent_GetList(ACSystemContext db, out int rows, int skip, int take, string orderBy, string dest)
        {
            rows = 0;

            List<ProjectInvoicesPercentViewModel> query = db.ProjectInvoicesPercent.Where(q => q.IsActive).Select(
                        q => new ProjectInvoicesPercentViewModel
                        {
                            ID = q.ID,
                            GenerateDate = q.GenerateDate,
                            AmountSum = q.AmountSum,
                            CalcPercent = q.CalcPercent
                        }).ToList();

            switch (orderBy)
            {
                case "date":
                    query = (dest == "asc") ? query.OrderBy(q => q.GenerateDate).ToList() : query.OrderByDescending(q => q.GenerateDate).ToList();
                    break;
                case "amount":
                    query = (dest == "asc") ? query.OrderBy(q => q.AmountSum).ToList() : query.OrderByDescending(q => q.AmountSum).ToList();
                    break;
                case "percent":
                    query = (dest == "asc") ? query.OrderBy(q => q.CalcPercent).ToList() : query.OrderByDescending(q => q.CalcPercent).ToList();
                    break;
            }

            rows = query.Count();

            query = query.Skip(skip).Take(take).ToList();

            return query;
        }

        #endregion
        #endregion

        #region TASKS MINUS

        public ActionResult TasksMinus()
        {
            var data = _db.Database.SqlQuery<TasksMinus>(
                    @"SELECT * FROM (
                        SELECT Id AS TaskID, ProductCount,
                        (SELECT SUM(WorkDone) FROM Task_Fitter WHERE TaskId = t.Id AND IsActive = 1) AS WorkDone,
                        (ProductCount - (SELECT SUM(WorkDone) FROM Task_Fitter WHERE TaskId = t.Id AND IsActive = 1)) AS WorkDoneMinus
                        FROM Tasks AS t
                    ) AS Wynik
                    WHERE WorkDoneMinus < 0"
                ).ToList();

            var tasks = new List<Tasks>();

            foreach (var d in data)
            {
                var t = _db.Tasks.Find(d.TaskID);

                if (t == null)
                    continue;

                tasks.Add(t);
            }

            return View(tasks);
        }

        #endregion

        #region INVENTORY

        public ActionResult Inventory()
        {
            return View();
        }

        public ActionResult FreeFitter(string ID)
        {
            if (string.IsNullOrEmpty(ID))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var fitter = _db.Fitters.Find(ID);

            if (fitter == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var task = _db.Tasks.Find(fitter.TaskAlreadyWork);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var newActivity = new Task_Fitter
            {
                TaskId = task.Id,
                FitterId = fitter.Id,
                WorkDone = 0,
                WorkTime = 0,
                Status = Core.TaskStatus.Stoped,
                Timestamp = DateTime.Now,
                IsActive = true,
                HandDateTime = DateTime.Now
            };

            task.Task_Fitter.Add(newActivity);
            task.Status = Core.TaskStatus.Stoped;

            fitter.TaskAlreadyWork = null;

            _db.Entry(task).State = EntityState.Modified;
            _db.Entry(fitter).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region NORMY

        public ActionResult CheckTaskPriceNorm()
        {
            var normy = _db.view_CheckTaskPriceNorm.ToList();

            return View(normy);
        }

        public ActionResult CheckPayrollRateNorm()
        {
            var normy = _db.view_CheckPayrollRateNorm.ToList();

            return View(normy);
        }

        [HttpPost]
        public ActionResult DontShowInCheckTaskPriceNorm(int? ID)
        {
            if (!ID.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var task = _db.Tasks.Find(ID);

            if (task == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            task.DontShowInNormCheck = true;

            _db.Entry(task).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.NotModified);
            }

        }

        [HttpPost]
        public ActionResult DontShowInCheckPayrollRateNorm(int? ID)
        {
            if (!ID.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var item = _db.PayrollItems.Find(ID);

            if (item == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            item.DontShowInNormCheck = true;

            _db.Entry(item).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.NotModified);
            }

        }

        [HttpPost]
        public ActionResult GenerateFeeFromCheckPayrollRateNorm(int? ID, decimal? Diff)
        {
            if (!ID.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (!Diff.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var item = _db.PayrollItems.Find(ID);

            if (item == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            item.DontShowInNormCheck = true;

            var lastFeeNumber = _db.Fees.OrderByDescending(q => q.FeeNumber).FirstOrDefault();

            var fee = new Fees
            {
                FeeNumber = (lastFeeNumber != null) ? lastFeeNumber.FeeNumber + 1 : 1,
                FitterID = item.FitterID,
                ProjectID = item.ProjectID,
                ApartmentID = item.ApartmentID,
                TaskID = item.TaskID,
                FeeDate = DateTime.Now,
                Description = $"Korekta listy płac nr. {item.Payroll.Number.ToString().PadLeft(5, '0')}",
                FeeAmount = Diff.Value,
                Status = Core.FeeStatus.DoPotracenia
            };

            _db.Fees.Add(fee);
            _db.Entry(item).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.NotModified);
            }

        }

        #endregion

        #region UNACCEPTED_TASKS

        public ActionResult UnacceptedTasks(int? ID)
        {
            using (var db = new ACSystemContext())
            {
                Expression<Func<Tasks, bool>> expression = q => !q.Accepted;

                var project = (ID.HasValue) ? db.Projects.Find(ID) : null;

                if (project != null)
                    expression = q => q.Apartment.ProjectId == ID.Value && !q.Accepted;

                var tasks = db.Tasks.Where(expression)
                                    .Select(q => new TaskFullViewModel
                                    {
                                        TaskID = q.Id,
                                        ProductName = q.TaskSufix,
                                        ProjectId = q.Apartment.ProjectId,
                                        ProjectNumber = q.Apartment.Project.ProjectNumber,
                                        ApartmentId = q.ApartmentId,
                                        ApartmentNumber = "LGH " + q.Apartment.NumberOfApartment,
                                        ProductId = q.ProductId,
                                        ProductPrice = q.ProductPrice,
                                        StartDate = q.StartDate,
                                        EndDate = q.EndDate,
                                        ProductCount = q.ProductCount,
                                        JM = q.Product.Jm,
                                        Type = q.Product.ProductType,
                                        Status = q.Status,
                                        Product = q.Product,
                                        Product_Project = q.Product.Product_Project.ToList()
                                    })
                                    .ToList();

                tasks.ForEach(q =>
                {
                    q.ProductName = q.GetTaskName(q.ProjectId) + " " + q.ProductName;
                });

                if (project != null)
                    ViewBag.UnacceptedTasks_Header = $"Projekt {project.ProjectNumber} - niezaakceptowane zadania";
                else
                    ViewBag.UnacceptedTasks_Header = "Wszystkie niezaakceptowane zadania";

                ViewBag.IsForProject = (project != null);

                return View(tasks);
            }
        }

        #endregion

        #region TASKS IN PROJECT

        [HttpGet]
        public ActionResult TasksInProject(int projectID)
        {
            using (ACSystemContext dtx = new ACSystemContext())
            {
                Projects project = dtx.Projects.Find(projectID);
                List<Apartments> apartments = dtx.Apartments.Where(q => q.ProjectId == projectID).ToList();
                List<int> tasks = dtx.Tasks.Where(q => q.Apartment.ProjectId == projectID).Select(q => q.Id).ToList();
                List<ACSystemUser> fitters = dtx.Task_Fitter.Where(q => tasks.Contains(q.TaskId)).GroupBy(q => q.Fitter).Select(q => q.Key.User).ToList();

                Filter_ProjectExcel filter = new Filter_ProjectExcel()
                {
                    Project = project,
                    Apartments = apartments,
                    Fitters = fitters,
                    Products = project.Product_Project.ToList()
                };

                return View(filter);
            }
        }

        [HttpPost]
        public ActionResult TasksInProject_GenerateExcel(int ProjectID, int[] Apartments, string[] Fitters, int[] Products, TaskStatus[] States, TaskProduction[] TaskTypes)
        {
            if (States == null || States.Length == 0)
                States = new TaskStatus[]
                {
                    TaskStatus.Free,
                    TaskStatus.UnderWork,
                    TaskStatus.Stoped,
                    TaskStatus.Planned,
                    TaskStatus.PlaceNotReady,
                    TaskStatus.DoWyjasnienia,
                    TaskStatus.NieNasze,
                    TaskStatus.FinishedWithFaults,
                    TaskStatus.FinishedWithFaultsCorrected,
                    TaskStatus.Finished
                };

            if (TaskTypes == null || TaskTypes.Length == 0)
                TaskTypes = new TaskProduction[]
                {
                    TaskProduction.Developer,
                    TaskProduction.Complaint,
                    TaskProduction.Warranty
                };

            using (ACSystemContext dtx = new ACSystemContext())
            {
                Projects project = dtx.Projects.Find(ProjectID);
                List<Tasks> tasks = dtx.Tasks.Where(q => q.Apartment.ProjectId == ProjectID)
                                             .Where(q => Apartments.Contains(q.ApartmentId))
                                             .Where(q => Products.Contains(q.ProductId))
                                             .Where(q => q.Task_Fitter.Any(x => Fitters.Contains(x.FitterId)) || q.Task_Fitter.Any() == false)
                                             .Where(q => States.Contains(q.Status))
                                             .Where(q => TaskTypes.Contains(q.Production))
                                             .ToList();

                XLWorkbook xl = new XLWorkbook();
                xl.AddWorksheet("Zestawienie");

                IXLWorksheet st = xl.Worksheet(1);
                IXLRow header = st.Row(1);

                header.Cell("A").Value = "ID";
                header.Cell("B").Value = "LGH";
                header.Cell("C").Value = "Produkt";
                header.Cell("D").Value = "Ilość wyk.";
                header.Cell("E").Value = "Ilość zad.";
                header.Cell("F").Value = "Pomieszczenie";
                header.Cell("G").Value = "Opis";
                header.Cell("H").Value = "Opis rek. (SWE)";
                header.Cell("I").Value = "Opis rek. (POL)";
                header.Cell("J").Value = "Status";
                header.Cell("K").Value = "Data";
                header.Cell("L").Value = "Opis FV (POL)";
                header.Cell("M").Value = "Opis FV (SWE)";
                header.Cell("N").Value = "Cena";
                header.Cell("O").Value = "Wypłacono";
                header.Cell("P").Value = "FV";
                header.Cell("R").Value = "Lista płac";
                header.Cell("S").Value = "Monteży";

                header.Height = 40;
                header.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                header.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                header.Cells("A", "S").Style.Fill.BackgroundColor = XLColor.Gray;
                header.Cells("A", "S").Style.Font.FontColor = XLColor.White;
                header.Cells("A", "S").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                header.Cells("A", "S").Style.Border.OutsideBorderColor = XLColor.Black;

                IXLRow currentRow = header.RowBelow();
                foreach (Tasks t in tasks)
                {
                    bool isService = t.TaskPatches.Any();
                    int loopTimes = (isService) ? t.TaskPatches.Count() : 1;

                    for (int i = 0; i < loopTimes; i++)
                    {
                        TaskPatches patch = (isService) ? t.TaskPatches.ElementAt(i) : null;
                        List<string> invoices = t.InvoiceElements.Select(q => q.Invoice.InvoiceNo).ToList();
                        List<PayrollItems> payrolls = t.PayrollItems.Where(q => q.Active == true).ToList();
                        List<string> payrollsNumbers = payrolls.Select(q => q.Payroll.Number.ToString().PadLeft(5, '0')).ToList();
                        List<string> fitters = t.Task_Fitter.Where(q => q.IsActive).GroupBy(q => q.Fitter.User).Select(q => q.Key.FullName).ToList();

                        currentRow.Cell("A").Value = t.Id.ToString().PadLeft(6, '0');
                        currentRow.Cell("B").Value = string.Format("LGH {0}{1}", t.Apartment.Letter, t.Apartment.NumberOfApartment);
                        currentRow.Cell("C").Value = string.Format("{0} {1}", t.GetTaskName(ProjectID), t.TaskSufix);
                        currentRow.Cell("D").Value = (isService) ? (patch.Done) ? 1 : 0
                                                                 : t.Task_Fitter.Where(q => q.IsActive).Sum(q => q.WorkDone);
                        currentRow.Cell("E").Value = (isService) ? 1
                                                                 : t.ProductCount;
                        currentRow.Cell("F").Value = (isService) ? patch.Room : "";
                        currentRow.Cell("G").Value = t.Description;
                        currentRow.Cell("H").Value = (isService) ? patch.ItemDescription : "";
                        currentRow.Cell("I").Value = (isService) ? patch.ItemDescription : "";
                        currentRow.Cell("J").Value = t.Status.GetDescription();
                        currentRow.Cell("K").Value = (isService) ? patch.Timestamp?.ToString("yyyy-MM-dd")
                                                                 : t.ClosedDate?.ToString("yyyy-MM-dd") ?? t.Task_Fitter.OrderByDescending(q => q.HandDateTime).FirstOrDefault()?.HandDateTime?.ToString("yyyy-MM-dd");
                        currentRow.Cell("L").Value = t.InvoiceDescriptionPL;
                        currentRow.Cell("M").Value = t.InvoiceDescription;
                        currentRow.Cell("N").Value = t.ProductPrice;
                        currentRow.Cell("O").Value = payrolls.Sum(q => q.ToPayPLN);
                        currentRow.Cell("P").Value = string.Join(", ", invoices);
                        currentRow.Cell("R").Value = string.Join(", ", payrollsNumbers);
                        currentRow.Cell("S").Value = string.Join(", ", fitters);

                        currentRow.Height = 20;
                        currentRow.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        currentRow.Cell("K").SetDataType(XLCellValues.DateTime).Style.NumberFormat.Format = "yyyy-MM-dd";
                        currentRow.Cell("N").SetDataType(XLCellValues.Number);
                        currentRow.Cells("A", "S").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        currentRow.Cells("A", "S").Style.Border.OutsideBorderColor = XLColor.Black;

                        if (t.Status == TaskStatus.Finished || t.Status == TaskStatus.FinishedWithFaultsCorrected)
                        {
                            currentRow.Cell("J").Style.Fill.BackgroundColor = XLColor.Green;
                            currentRow.Cell("J").Style.Font.FontColor = XLColor.White;
                        }
                        else if (t.Status == TaskStatus.FinishedWithFaults)
                        {
                            currentRow.Cell("J").Style.Fill.BackgroundColor = XLColor.Red;
                            currentRow.Cell("J").Style.Font.FontColor = XLColor.White;
                        }
                    }

                    currentRow = currentRow.RowBelow();
                }

                st.Column("A").AdjustToContents();
                st.Column("B").AdjustToContents();
                st.Column("C").AdjustToContents();
                st.Column("D").AdjustToContents();
                st.Column("E").AdjustToContents();
                st.Column("F").AdjustToContents();
                st.Column("G").AdjustToContents();
                st.Column("H").AdjustToContents();
                st.Column("I").AdjustToContents();
                st.Column("J").AdjustToContents();
                st.Column("K").AdjustToContents();
                st.Column("L").AdjustToContents();
                st.Column("M").AdjustToContents();
                st.Column("N").AdjustToContents();
                st.Column("O").AdjustToContents();
                st.Column("P").AdjustToContents();
                st.Column("R").AdjustToContents();
                st.Column("S").AdjustToContents();

                int lastRow = tasks.Count() * 2;
                st.Range("A2:A" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);
                st.Range("B2:B" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);
                st.Range("C2:C" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);
                st.Range("D2:D" + lastRow).CellsUsed().SetDataType(XLCellValues.Number);
                st.Range("E2:E" + lastRow).CellsUsed().SetDataType(XLCellValues.Number);
                st.Range("F2:F" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);
                st.Range("G2:G" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);
                st.Range("H2:H" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);
                st.Range("I2:I" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);
                st.Range("J2:J" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);
                st.Range("K2:K" + lastRow).CellsUsed().SetDataType(XLCellValues.DateTime);
                st.Range("L2:L" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);
                st.Range("M2:M" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);
                st.Range("N2:N" + lastRow).CellsUsed().SetDataType(XLCellValues.Number);
                st.Range("O2:O" + lastRow).CellsUsed().SetDataType(XLCellValues.Number);
                st.Range("P2:P" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);
                st.Range("R2:R" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);
                st.Range("S2:S" + lastRow).CellsUsed().SetDataType(XLCellValues.Text);

                string fileName = string.Format("Zadania_w_projekcie_{0}_{1}.xlsx", project.InSystemID, DateTime.Now.ToString("yyyy-MM-dd"));
                string filePath = Path.Combine(Server.MapPath("~/dist/Reports"), fileName);

                try
                {
                    xl.SaveAs(filePath);
                    return Json(new { success = true, file = fileName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                    throw ex;
                }
            }
        }

        #endregion

        #region TASKS PRODUCT PROJECT

        public ActionResult TasksForProductProject(int? ProjectID, int? ProductID) {
            if (ProjectID.HasValue == false || ProductID.HasValue == false)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            using (ACSystemContext dtx = new ACSystemContext()) {
                Product_Project pp = dtx.Product_Project.FirstOrDefault(q => q.ProjectID == ProjectID && q.ProductID == ProductID);
                List<view_TaskForProductProject> tasksForProductProject = dtx.Database.SqlQuery<view_TaskForProductProject>($"SELECT * FROM view_TaskForProductProject WHERE ProjectID = {ProjectID} AND ProductID = {ProductID}").ToList();
                List <view_FitterTasksWorkDone> fittersTasksWorkDone = dtx.Database.SqlQuery<view_FitterTasksWorkDone>($"SELECT * FROM view_FitterTasksWorkDone WHERE ProjectID = {ProjectID} AND ProductID = {ProductID}").ToList();
                ViewBag.TasksForProductProject = tasksForProductProject;
                ViewBag.FittersTasksWorkDone = fittersTasksWorkDone;

                return View(pp);
            }
        }

        #endregion
    }
}