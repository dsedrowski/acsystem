﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ACSystem.Core;
using ACSystem.Core.Models;
using Newtonsoft.Json;
using TaskStatus = ACSystem.Core.TaskStatus;

namespace ACSystem.Controllers
{
    [Authorize]
    public class GanttController : Controller
    {
        public ActionResult Index(int id)
        {
            return Redirect("~/Gantt/index.html");
        }

        [HttpGet]
        public async Task<ActionResult> Project(int? id)
        {
            using (var ctx = new ACSystemContext())
            {
                var ganttData = new GanttJsonData
                {
                    tasksList = new List<GanttJsonData.Task>()
                };

                var element = new GanttJsonData.Task();
                var rnd = new Random();

                var data = await ctx.Database
                    .SqlQuery<view_GanttData>($"SELECT * FROM view_GanttData WHERE ProjectId = {id}").ToListAsync();


                foreach (var project in data.GroupBy(e => new
                {
                    e.ProjectId,
                    e.ProjectFortnox,
                    e.ProjectStatus,
                    e.ProjectStatusString,
                    e.ProjectNumber,
                    e.ProjectDescription,
                    e.ProjectStartDate,
                    e.ProjectEndDate
                }))
                {
                    var pclass = project.Key.ProjectStatus == ProjectStatus.FinishedWithFaults || project.Key.ProjectStatus == ProjectStatus.Finished ? "gtaskblue" : "gtaskbluecomplete";

                    element = new GanttJsonData.Task()
                    {
                        pID = project.Key.ProjectId + 1000000,
                        pName = $"<a href=\"/Projects/Details/{project.Key.ProjectId}\" target=_blank>{project.Key.ProjectNumber}</a>",
                        pBarText = project.Key.ProjectNumber,
                        pNotes = project.Key.ProjectDescription,
                        pCaption = project.Key.ProjectNumber,
                        pParent = 0,
                        pGroup = 1,
                        pComp = 0,
                        pClass = pclass,
                        pOpen = 1,
                        pLink = "/Projects/Details/" + project.Key.ProjectId
                    };

                    ganttData.tasksList.Add(element);

                    foreach (var cage in project.OrderBy(e => e.Cage).GroupBy(e => e.Cage))
                    {
                        var level = 2;
                        var cageId = rnd.Next(1, 100000);

                        if (!string.IsNullOrEmpty(cage.Key))
                        {
                            pclass = (cage.All(e =>
                                e.TaskStatus == TaskStatus.Finished || e.TaskStatus == TaskStatus.FinishedWithFaults ||
                                e.TaskStatus == TaskStatus.FinishedWithFaultsCorrected))
                            ? "gtaskpurplecomplete" : "gtaskpurple";

                            element = new GanttJsonData.Task() {
                                pID = cageId,
                                pName = "Klatka " + cage.Key,
                                pBarText = "Klatka " + cage.Key,
                                pCaption = "Klatka " + cage.Key,
                                pParent = project.Key.ProjectId + 1000000,
                                pGroup = 1,
                                pComp = 0,
                                pClass = pclass
                            };
                            ganttData.tasksList.Add(element);

                            level = 3;
                        }

                        foreach (var floor in cage.OrderBy(e => e.Floor).GroupBy(e => e.Floor))
                        {
                            var floorId = rnd.Next(1, 100000);

                            pclass = (floor.All(e =>
                                e.TaskStatus == TaskStatus.Finished || e.TaskStatus == TaskStatus.FinishedWithFaults ||
                                e.TaskStatus == TaskStatus.FinishedWithFaultsCorrected))
                                ? "gtaskpinkcomplete" : "gtaskpink";

                            element = new GanttJsonData.Task() {
                                pID = floorId,
                                pName = "Piętro " + floor.Key.ToString(),
                                pBarText = "Piętro " + floor.Key,
                                pCaption = "Piętro " + floor.Key,
                                pParent = (level == 3) ? cageId : project.Key.ProjectId + 1000000,
                                pGroup = 1,
                                pComp = 0,
                                pClass = pclass
                            };
                            ganttData.tasksList.Add(element);

                            foreach (var lgh in floor.OrderBy(e => e.LGH).GroupBy(e => new
                            {
                                e.ApartmentId,
                                e.LGH,
                                e.ApartmentDescription
                            }))
                            {
                                pclass = (lgh.All(e =>
                                    e.TaskStatus == TaskStatus.Finished || e.TaskStatus == TaskStatus.FinishedWithFaults ||
                                    e.TaskStatus == TaskStatus.FinishedWithFaultsCorrected))
                                    ? "gtaskyellowcomplete" : "gtaskyellow";

                                element = new GanttJsonData.Task() {
                                    pID = lgh.Key.ApartmentId + 2000000,
                                    pName = $"<a href=\"/Projects/ApartmentDetails/{lgh.Key.ApartmentId}\" target=_blank>LGH {lgh.Key.LGH}</a>",
                                    pBarText = lgh.Key.LGH,
                                    pCaption = "LGH" + lgh.Key.LGH,
                                    pParent = floorId,
                                    pGroup = 1,
                                    pComp = 0,
                                    pClass = pclass,
                                    pLink = "/Projects/ApartmentDetails/" + lgh.Key.ApartmentId
                                };
                                ganttData.tasksList.Add(element);

                                foreach (var task in lgh.OrderBy(e => e.TaskStartDate))
                                {
                                    pclass = (task.TaskStatus == TaskStatus.Finished || task.TaskStatus == TaskStatus.FinishedWithFaults ||
                                              task.TaskStatus == TaskStatus.FinishedWithFaultsCorrected)
                                            ? "gtaskgreencomplete" : "gtaskgreen";

                                    element = new GanttJsonData.Task() {
                                        pID = task.TaskId + 3000000,
                                        pName = $"<a href=\"/Task/Details/{task.TaskId}\" target=_blank>{task.TaskName}</a>",
                                        pBarText = task.TaskName,
                                        pCaption = task.TaskName,
                                        pParent = lgh.Key.ApartmentId + 2000000,
                                        pGroup = 0,
                                        pComp = 0,
                                        pClass = pclass,
                                        pLink = "/Task/Details/" + task.TaskId,
                                        pStart = task.TaskStartDate?.ToString("yyyy-MM-dd") ?? "",
                                        pEnd = task.TaskEndDate?.ToString("yyyy-MM-dd") ?? "",
                                        status = task.TaskStatusString
                                    };
                                    ganttData.tasksList.Add(element);
                                }
                            }
                        }
                    }
                }

                ganttData.tasks = ganttData.tasksList.ToArray();

                var jsonResult = Json(ganttData.tasks, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
        }

        protected class GanttJsonData
        {
            public List<Task> tasksList { get; set; }
            public Task[] tasks { get; set; }

            public class Task
            {
                public int pID { get; set; }
                public string pName { get; set; }
                public string pStart { get; set; }
                public string pEnd { get; set; }
                public string pPlanStart { get; set; }
                public string pPlanEnd { get; set; }
                public string pClass { get; set; }
                public string pLink { get; set; }
                public int pMile { get; set; }
                public string pRes { get; set; }
                public int pComp { get; set; }
                public int pGroup { get; set; }
                public int pParent { get; set; }
                public int pOpen { get; set; }
                public string pDepend { get; set; }
                public string pCaption { get; set; }
                public string pNotes { get; set; }
                public decimal pCost { get; set; }
                public string pBarText { get; set; }
                public string status { get; set; }


                //public int progress { get; set; }
                //public bool progressByWorklog { get; set; }
                //public int relevance { get; set; }
                //public string type { get; set; }
                //public string typeId { get; set; }
                //public string description { get; set; }
                //public string code { get; set; }
                //public int level { get; set; }
                //public string status { get; set; }
                //public string depends { get; set; }
                //public bool canWrite { get; set; }
                //publi
                //public int duration { get; set; }
                //public bool startIsMilestone { get; set; }
                //public bool endIsMilestone { get; set; }
                //public bool collapsed { get; set; }
                //public List<string> assigs { get; set; }
                //public bool hasChild { get; set; }
                //public string before { get; set; }
                //public string startString { get; set; }
                //public string endString { get; set; }
                //public string owner { get; set; }
            }
        }
    }
}