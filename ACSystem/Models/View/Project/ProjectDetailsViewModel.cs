﻿using ACSystem.Core;
using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Models.View
{
    public class ProjectDetailsViewModel
    {
        public int Id { get; set; }
        public string InSystemId { get; set; }
        public string ProjectName { get; set; }
        public ProjectStatus Status { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Address { get; set; }
        public string Supervisor { get; set; }
        public string InvoiceDescription { get; set; }

        public int? ParentProjectID { get; set; }
        public string ParentProjectName { get; set; }

        public decimal WorkTimeDone { get; set; }
        public decimal WorkTimeRequired { get; set; }

        public decimal InvoicesQuantity { get; set; }
        public decimal PlaceErrorsQuantity { get; set; }
        public decimal FeesQuantity { get; set; }
        public decimal InventoryQuantity { get; set; }

        public bool FirstDoService { get; set; }
        public bool Remind { get; set; }
        public bool IsChild { get; set; }
        public bool IsChecked { get; set; }
        public bool HasChilds { get; set; }

        public Customers Customer { get; set; }
        public Calendar Calendar { get; set; }
        public Settings Settings { get; set; }

        public Dictionary<string, bool> BeforeCheckValidationMessages { get; set; }
    }
}