namespace ACSystem.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class defaultaccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BankAccounts", "DefaultAccount", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BankAccounts", "DefaultAccount");
        }
    }
}
