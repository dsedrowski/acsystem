﻿using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.ViewModels
{
    public class TaskTodoViewModel
    {
        IEnumerable<Todo> TodoList { get; set; }
        public List<ACSystemUser> AdminList { get; set; }
    }
}