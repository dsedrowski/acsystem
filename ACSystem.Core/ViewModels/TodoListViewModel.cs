﻿using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.ViewModels
{
    public class TodoListViewModel
    {
        public int? TodoId { get; set; }
        public int? ProjectId { get; set; }
        public int? ApartmentId { get; set; }
        public int? TaskId { get; set; }
        public string Project { get; set; }
        public string AssignedUser { get; set; }
        public string Apartment { get; set; }
        public string Fitter { get; set; }
        public string Task { get; set; }
        public TodoType Type { get; set; }
        public DateTime Date { get; set; }
        public string Error { get; set; }
        public bool? IsTaskAccepted { get; set; }
        public int? ErrorId { get; set; }
        public Tasks TaskModel { get; set; }
        public PlaceError PlaceError { get; set; }
    }
}