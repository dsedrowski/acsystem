﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Inventory_Fitter
    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "Monter")]
        public string UserID { get; set; }

        [Display(Name = "Maszyna")]
        public int InventoryID { get; set; }

        [Display(Name = "Data pobrania")]
        public DateTime TakeDate { get; set; }

        [Display(Name = "Data zdania")]
        public DateTime? PutDate { get; set; }

        [Display(Name = "Projekt")]
        public int? ProjectID { get; set; }

        [Display(Name = "Magazyn")]
        public int? WarehouseID { get; set; }

        public virtual Inventory Inventory { get; set; }

        [Display(Name = "Projekt")]
        public virtual Projects Project { get; set; }

        [Display(Name = "Magazyn")]
        public virtual InventoryWarehouse Warehouse { get; set; }

        public virtual ACSystemUser User { get; set; }
    }
}