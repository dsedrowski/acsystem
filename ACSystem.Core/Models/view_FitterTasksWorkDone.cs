﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models {
    public class view_FitterTasksWorkDone {
        [Key]
        public int ProjectID { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string FullName { get; set; }
        public decimal WorkDone { get; set; }
    }
}