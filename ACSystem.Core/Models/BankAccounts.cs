﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class BankAccounts
    {
        public BankAccounts()
        {
            this.InvoiceList = new HashSet<Invoice>();
        }

        [Key]
        public int ID { get; set; }

        public string Currency { get; set; }

        public string AccountNumber { get; set; }

        public bool DefaultAccount { get; set; }

        public virtual ICollection<Invoice> InvoiceList { get; set; }
    }
}