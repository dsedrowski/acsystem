﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Task_Question
    {
        [Key]
        public int ID { get; set; }

        public int QuestionID { get; set; }

        public int TaskID { get; set; }

        public int? AnswerID { get; set; }

        public string AdditionalInformation { get; set; }

        public string Translation { get; set; }

        public string Photo { get; set; }

        public virtual Question Question { get; set; }

        public virtual Tasks Task { get; set; }

        public virtual Answer Answer { get; set; }

        public static bool IsAllOk(IEnumerable<Task_Question> questions) => questions.All(q => q.Answer == null || q.Answer.IsError != true);
    }
}