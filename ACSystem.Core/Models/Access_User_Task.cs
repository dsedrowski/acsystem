﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Access_User_Task
    {
        [Key]
        public int ID { get; set; }

        public string UserID { get; set; }

        public int TaskID { get; set; }

        public virtual ACSystemUser User { get; set; }

        public virtual Tasks Task { get; set; }
    }
}