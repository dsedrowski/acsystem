﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class InventoryType
    {
        public InventoryType()
        {
            this.InventoryList = new HashSet<Inventory>();
        }

        [Key]
        public int ID { get; set; }

        [Display(Name = "Nazwa typu")]
        public string Name { get; set; }

        public virtual ICollection<Inventory> InventoryList { get; set; }
    }
}