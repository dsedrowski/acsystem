﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class InventoryWarehouse
    {
        public InventoryWarehouse()
        {
            this.Inventory = new HashSet<Inventory>();
            this.Inventory_Fitter = new HashSet<Inventory_Fitter>();
        }

        [Key]
        public int ID { get; set; }

        [Display(Name = "Adres")]
        public string Street { get; set; }

        [Display(Name = "Miasto")]
        public string City { get; set; }

        [Display(Name = "Kraj")]
        public string Country { get; set; }

        public virtual ICollection<Inventory> Inventory { get; set; }

        public virtual ICollection<Inventory_Fitter> Inventory_Fitter { get; set; }
    }
}