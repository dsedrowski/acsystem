﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Task_AccessGrant
    {
        [Key]
        public int ID { get; set; }

        public int TaskID { get; set; }

        public string FitterID { get; set; }

        public virtual Tasks Task { get; set; }

        public virtual ACSystemUser User { get; set; }
    }
}