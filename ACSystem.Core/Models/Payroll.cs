﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Payroll
    {
        public Payroll()
        {
            this.Items = new HashSet<PayrollItems>();
            this.Fees = new HashSet<Fees>();
            this.Installments = new HashSet<Installment>();
            this.Fitter_Hours_Project = new HashSet<Fitter_Hours_Project>();
            this.Payroll_Errors = new HashSet<Payroll_Errors>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public int Number { get; set; }

        public DateTime CreateDate { get; set; }

        public decimal Sum { get; set; }

        public decimal SumPLN { get; set; }

        [DataType(DataType.Currency)]
        public decimal CurrencyRate { get; set; }

        public double CurrencyRate2 { get; set; }

        public PayStatus Status { get; set; }

        public bool Active { get; set; }

        public virtual ICollection<PayrollItems> Items { get; set; }

        public virtual ICollection<Fees> Fees { get; set; }

        public virtual ICollection<Installment> Installments { get; set; }

        public virtual ICollection<Fitter_Hours> Fitter_Hours { get; set; }

        public virtual ICollection<Fitter_Hours_Project> Fitter_Hours_Project { get; set; }

        public virtual ICollection<Payroll_Errors> Payroll_Errors { get; set; }

        public string GetNumber() => Number.ToString().PadLeft(5, '0');
    }

    public class PayrollItems
    {
        [Key]
        //[Column(Order = 1)]
        public int Id { get; set; }

        public int PayrollID { get; set; }

        public string FitterID { get; set; }

        public string FitterName { get; set; }

        public int? ProjectID { get; set; }

        public string ProjectNumber { get; set; }

        public int? ApartmentID { get; set; }

        public string ApartmentNumber { get; set; }

        public int? ProductID { get; set; }

        public string ProductName { get; set; }

        public int? TaskID { get; set; }

        public DateTime TaskEnd { get; set; }

        public decimal WorkDone { get; set; }

        public string WorkDoneUnit { get; set; }

        public decimal WorkTime { get; set; }

        public bool IsHourlyPay { get; set; }

        public decimal ToPay { get; set; }

        public decimal ToPayPLN { get; set; }

        public decimal Fees { get; set; }

        public decimal FeesPLN { get; set; }

        public PayItemStatus Status { get; set; }

        public bool? Active { get; set; }

        public string FeeDescription { get; set; }

        public int? FeeID { get; set; }

        public int? RentAndLoanID { get; set; }

        public string InvoiceNumber { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public string InvoiceOwner { get; set; }

        public DateTime? InvoiceTimestamp { get; set; }

        public bool? DontShowInNormCheck { get; set; }

        public int HourlyRate { get; set; }

        public virtual Payroll Payroll { get; set; }

        public virtual Projects Project { get; set; }

        public virtual Apartments Apartment { get; set; }

        public virtual Tasks Task { get; set; }
    }

    public class CreatePayrollViewModel
    {
        public int Number { get; set; }
        public DateTime CreateDate { get; set; }
        public string FitterID { get; set; }
        public bool ForFitter { get; set; }
    }
}