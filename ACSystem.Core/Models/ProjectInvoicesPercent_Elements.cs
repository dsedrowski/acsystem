﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class ProjectInvoicesPercent_Elements
    {
        [Key]
        public int ID { get; set; }

        public int ProjectInvoicesPercentID { get; set; }

        public int ProjectID { get; set; }

        public string ProjectNumber { get; set; }

        public string ProjectName { get; set; }

        public decimal Amount { get; set; }

        public virtual ProjectInvoicesPercent ProjectInvoicesPercent { get; set; }
    }
}