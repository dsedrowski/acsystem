﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ACSystem.Core.Models
{
    public class Products
    {
        public Products()
        {
            this.Tasks = new HashSet<Tasks>();
            this.Product_Project = new HashSet<Product_Project>();
            this.Product_Plan = new HashSet<Product_Plan>();
            this.ProductGroups_Main = new HashSet<Product_Group>();
            this.ProductGroups_Sub = new HashSet<Product_Group>();
            this.Product_Questions = new HashSet<Product_Question>();
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string DescriptionPL { get; set; }

        public string DescriptionEN { get; set; }

        public string DescriptionSE { get; set; }

        public decimal Price { get; set; }

        public string Jm { get; set; }

        public string JmEng { get; set; }

        public string JmSwe { get; set; }

        public TaskType ProductType { get; set; }

        public bool IsActive { get; set; }

        public bool ForAllProjects { get; set; }

        public string InSystemID { get; set; }

        public bool NoExtra { get; set; }

        public decimal ExpectedTime { get; set; }

        public int TimePercent { get; set; }

        public virtual PercentHeight PercentHeight { get; set; }

        public virtual ICollection<Tasks> Tasks { get; set; }

        public virtual ICollection<Product_Project> Product_Project { get; set; }

        public virtual ICollection<Product_Plan> Product_Plan { get; set; }

        public virtual ICollection<Product_Group> ProductGroups_Main { get; set; }

        public virtual ICollection<Product_Group> ProductGroups_Sub { get; set; }

        public virtual ICollection<Product_Question> Product_Questions { get; set; }

        public string GetTaskName(int projectID)
        {
            var connect = this.Product_Project.FirstOrDefault(q => q.ProjectID == projectID);

            return (connect != null && !string.IsNullOrEmpty(connect.ProductName)) ? connect.ProductName : this.Name;
        }

        public string GetTaskNameOnInvoice(int projectID)
        {
            var connect = this.Product_Project.FirstOrDefault(q => q.ProjectID == projectID);

            return (connect != null && !string.IsNullOrEmpty(connect.OnInvoiceName)) ? connect.OnInvoiceName : this.DescriptionSE;
        }

        public Product_Project GetConnectForProject(int projectID)
        {
            return this.Product_Project.FirstOrDefault(q => q.ProjectID == projectID);
        }
    }
}