﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class FeeFiles
    {
        public int ID { get; set; }
        public int FeeID { get; set; }
        public string FileName { get; set; }

        public virtual Fees Fee { get; set; }
    }
}