﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class TaskDocuments
    {
        [Key]
        public int ID { get; set; }

        public int TaskID { get; set; }

        public string FileName { get; set; }

        public string FilePath { get; set; }

        public virtual Tasks Task { get; set; }
    }
}