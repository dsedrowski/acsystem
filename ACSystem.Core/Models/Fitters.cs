﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Fitters
    {
        public Fitters()
        {
            this.Fees = new HashSet<Fees>();
            this.Task_Fitter = new HashSet<Task_Fitter>();
            this.TaskComments = new HashSet<TaskComment>();
            this.Orders = new HashSet<Order>();
            this.RentAndLoan = new HashSet<RentAndLoan>();
        }

        
        [Key, ForeignKey("User")]
        public string Id { get; set; }

        public int? TaskAlreadyWork { get; set; }

        public int PercentType { get; set; }

        public int HourlyRate { get; set; }

        public virtual ACSystemUser User { get; set; } 
        
        public virtual Tasks Task { get; set; }

      //  public virtual Todo Todo { get; set; }

        public virtual ICollection<Fees> Fees { get; set; }   
        
        public virtual ICollection<Task_Fitter> Task_Fitter { get; set; }

        public virtual ICollection<TaskComment> TaskComments { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public virtual ICollection<RentAndLoan> RentAndLoan { get; set; }

    }
}