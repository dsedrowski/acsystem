﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ACSystem.Core.Models {
    public class Document {
        public int ID { get; set; }
        public int ObjectID { get; set; }
        public Types Type { get; set; }
        public string Location { get; set; }
        public string OriginalFileName { get; set; }

        [ForeignKey("ObjectID")]
        public virtual Apartments Apartment { get; set; }
        [ForeignKey("ObjectID")]
        public virtual Tasks Task { get; set; }
        [ForeignKey("ObjectID")]
        public virtual Task_Fitter TaskActivity { get; set; }

        public enum Types {
            Apartment,
            Task,
            TaskActivity
        }
    }
}