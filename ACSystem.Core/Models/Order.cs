﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Order
    {
        public Order()
        {
            this.OrderItems = new HashSet<OrderItem>();
        }

        [Key]
        public int ID { get; set; }

        public int ProjectID { get; set; }

        public int? ApartmentID { get; set; }

        public int? TaskID { get; set; }

        public string FitterID { get; set; }

        public bool IsFinished { get; set; }

        public virtual Projects Project { get; set; }

        public virtual Apartments Apartment { get; set; }

        public virtual Tasks Task { get; set; }

        public virtual Fitters Fitter { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }
    }
}