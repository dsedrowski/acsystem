﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class view_CheckPayrollRateNorm
    {
        public int PayrollID { get; set; }

        public string PayrollNumber { get; set; }

        [Key]
        public int PayrollItemID { get; set; }

        public int ProjectID { get; set; }

        public string ProjectNumber { get; set; }

        public int ApartmentID { get; set; }

        public string ApartmentNumber { get; set; }

        public int TaskID { get; set; }

        public string ProductName { get; set; }

        public string FitterName { get; set; }

        public decimal WorkDone { get; set; }

        public decimal PricePlan { get; set; }

        public decimal PercentTypePlan { get; set; }

        public decimal ToPayPlan { get; set; }

        public decimal PriceReal { get; set; }

        public decimal PercentTypeReal { get; set; }

        public decimal ToPayReal { get; set; }
    }
}