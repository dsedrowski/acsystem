﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Task_Fitter
    {
        public Task_Fitter()
        {
            this.Photos = new HashSet<TaskActivityPhotos>();
            this.Documents = new HashSet<Document>();
        }

        [Key]
        public int Id { get; set; }

        public int TaskId { get; set; }

        public string FitterId { get; set; }

        public decimal WorkDone { get; set; }

        public int WorkTime { get; set; }

        public TaskStatus Status { get; set; }

        public DateTime Timestamp { get; set; }

        public bool InPayroll { get; set; }

        public bool IsActive { get; set; }

        public bool DayChecked { get; set; }

        public DateTime? HandDateTime { get; set; }

        public string ReportUser { get; set; }

        public decimal RealWorkTime { get; set; }

        public virtual Tasks Task { get; set; }

        public virtual Fitters Fitter { get; set; }

        [ForeignKey("ReportUser")]
        public virtual ACSystemUser ReportUserEntity { get; set; }

        public virtual ICollection<TaskActivityPhotos> Photos { get; set; }
        public virtual ICollection<Document> Documents { get; set; }

        [NotMapped]
        public decimal WorkTimeDecimal { get; set; }
    }
}