﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ACSystem.Core.Models
{

    // You can add profile data for the user by adding more properties to your ACSystemUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ACSystemUser : IdentityUser
    {
        public ACSystemUser()
        {
            this.Task_AccessGrant = new HashSet<Task_AccessGrant>();
            this.ReportElems = new HashSet<ReportElem>();
            this.InventoryList = new HashSet<Inventory>();
            this.Inventory_Fitter = new HashSet<Inventory_Fitter>();
            this.Access_User_Project = new HashSet<Access_User_Project>();
            this.Access_User_Apartment = new HashSet<Access_User_Apartment>();
            this.Access_User_Task = new HashSet<Access_User_Task>();
            this.Todos = new HashSet<Todo>();
            this.Activities = new HashSet<Project_Activity>();
            this.FavouriteProjects = new HashSet<FavouriteProject>();
            this.PlaceErrors = new HashSet<PlaceError>();
            this.FavouriteCity = new HashSet<FavouriteCity>();
            this.Fitter_Hours = new HashSet<Fitter_Hours>();
            this.UserChangeLog = new HashSet<UserChangeLog>();
            this.UserChangeLog_Changer = new HashSet<UserChangeLog>();
            this.ReportUsers = new HashSet<Task_Fitter>();
            this.UserDocuments = new HashSet<UserDocuments>();

            this.FirstDoService = false;
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        [NotMapped]
        public string FullName { get { return Surname + " " + Name; } }
        public string DateOfBirth { get; set; }
        public Position Position { get; set; }
        public string Image { get; set; }
        public string GoogleId { get; set; }
        public bool SyncWithGoogle { get; set; }
        public UserType UserType { get; set; }
        public string Podpis { get; set; }
        public int? CalendarID { get; set; }
        public string InSystemUserID { get; set; }
        public DateTime? LockTimestamp { get; set; }
        public bool CanSeeAll { get; set; }
        public bool? FirstDoService { get; set; }
        public decimal? CompanyPercent { get; set; }
        public decimal? FitterPercent { get; set; }
        public DateTime? HireDate { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public virtual Calendar Calendar { get; set; }
        public virtual Fitters Fitter { get; set; }
        public virtual ICollection<Task_AccessGrant> Task_AccessGrant { get; set; }
        public virtual ICollection<ReportElem> ReportElems { get; set; }
        public virtual ICollection<Inventory> InventoryList { get; set; }
        public virtual ICollection<Inventory_Fitter> Inventory_Fitter { get; set; }
        public virtual ICollection<Inventory_Check> Inventory_Check { get; set; }
        public virtual ICollection<Access_User_Project> Access_User_Project { get; set; }
        public virtual ICollection<Access_User_Apartment> Access_User_Apartment { get; set; }
        public virtual ICollection<Access_User_Task> Access_User_Task { get; set; }
        public virtual ICollection<Todo> Todos { get; set; }
        public virtual ICollection<Project_Activity> Activities { get; set; }
        public virtual ICollection<FavouriteProject> FavouriteProjects { get; set; }
        public virtual ICollection<FavouriteCity> FavouriteCity { get; set; }
        public virtual ICollection<PlaceError> PlaceErrors { get; set; }
        public virtual ICollection<Fitter_Crew> Crew_Supervisor { get; set; }
        public virtual ICollection<Fitter_Crew> Crew_Members { get; set; }
        public virtual ICollection<Projects> ProjectsSupervisor { get; set; }
        public virtual ICollection<Fitter_Hours> Fitter_Hours { get; set; }
        public virtual ICollection<UserChangeLog> UserChangeLog { get; set; }
        public virtual ICollection<UserChangeLog> UserChangeLog_Changer { get; set; }
        public virtual ICollection<Task_Fitter> ReportUsers { get; set; }
        public virtual ICollection<UserDocuments> UserDocuments { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ACSystemUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("UserFullName", this.FullName.ToString()));
            // Add custom user claims here
            return userIdentity;
        }
    }

}