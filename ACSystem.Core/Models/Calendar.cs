﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Calendar
    {

        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public string ColorHex { get; set; }
        
        public virtual ICollection<Projects> Project { get; set; }

        public virtual ICollection<Events> Event { get; set; }

        public virtual ICollection<ACSystemUser> User { get; set; }
    }
}