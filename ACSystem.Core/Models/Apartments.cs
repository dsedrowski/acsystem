﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Apartments
    {
        public Apartments()
        {
            this.Tasks = new HashSet<Tasks>();
            this.PayrollItems = new HashSet<PayrollItems>();
            this.TaskComments = new HashSet<TaskComment>();
            this.Orders = new HashSet<Order>();
            this.Fees = new HashSet<Fees>();
            this.ReportNags = new HashSet<ReportNag>();
            this.Access_User_Apartment = new HashSet<Access_User_Apartment>();
            this.PlaceErrors = new HashSet<PlaceError>();
            this.Documents = new HashSet<Document>();
        }

        [Key]
        public int Id { get; set; }

        public string NumberOfApartment { get; set; }

        public int ProjectId { get; set; }

        public string Description { get; set; }

        public string InvoiceDescription { get; set; }

        public string Letter { get; set; }

        public int Floor { get; set; }

        public string Cage { get; set; }

        public virtual Projects Project { get; set; }

        public virtual ICollection<Tasks> Tasks { get; set; }

        public virtual ICollection<PayrollItems> PayrollItems { get; set; }

        public virtual ICollection<TaskComment> TaskComments { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public virtual ICollection<Fees> Fees { get; set; }

        public virtual ICollection<ReportNag> ReportNags { get; set; }

        public virtual ICollection<Access_User_Apartment> Access_User_Apartment { get; set; }

        public virtual ICollection<PlaceError> PlaceErrors { get; set; }

        public virtual ICollection<Document> Documents { get; set; }
    }
}