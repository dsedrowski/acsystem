﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class RentAndLoan
    {
        public RentAndLoan()
        {
            this.Installments = new HashSet<Installment>();
        }

        [Key]
        public int ID { get; set; }

        public string FitterID { get; set; }

        public InstallmentFrequency Frequency { get; set; }

        public string Description { get; set; }

        public decimal FullAmount { get; set; }

        public decimal InstallmentAmount { get; set; }

        public decimal LeftAmount { get; set; }

        public Currency Currency { get; set; }

        public DateTime StartDate { get; set; }

        public decimal InterestRate { get; set; }

        public virtual Fitters Fitter { get; set; }

        public virtual ICollection<Installment> Installments { get; set; }

        [NotMapped]
        public decimal AddAmount { get; set; }

        #region CONVERTERS

        [NotMapped]
        public string FullAmountToInput
        {
            get
            {
                return FullAmount.ToString().Replace(',', '.');
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    decimal result = 0;
                    if (decimal.TryParse(value.Replace('.', ','), out result))
                        FullAmount = result;
                }
            }
        }

        [NotMapped]
        public string LeftAmountToInput
        {
            get
            {
                return LeftAmount.ToString().Replace(',', '.');
            }
        }

        [NotMapped]
        public string InstallmentAmountToInput
        {
            get
            {
                return InstallmentAmount.ToString().Replace(',', '.');
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    decimal result = 0;
                    if (decimal.TryParse(value.Replace('.', ','), out result))
                        InstallmentAmount = result;
                }
            }
        }

        #endregion
    }

    public class RentAndLoanViewModel
    {
        public int ID { get; set; }

        public string FitterID { get; set; }

        public string FitterName { get; set; }

        public string FitterSurname { get; set; }

        public InstallmentFrequency Frequency { get; set; }

        public string Description { get; set; }

        public decimal FullAmount { get; set; }

        public decimal InstallmentAmount { get; set; }

        public decimal LeftAmount { get; set; }

        public Currency Currency { get; set; }

        #region Converters

        public string FitterFullName
        {
            get
            {
                return $"{FitterSurname} {FitterName}";
            }
        }

        public string FrequencyString
        {
            get
            {
                return (Frequency == InstallmentFrequency.PerPayroll) ? "Co wypłatę" : "Co miesiąc";
            }
        }

        public string CurrencyString
        {
            get
            {
                return (Currency == Currency.SEK) ? "SEK" : "PLN";
            }
        }

        public int Status
        {
            get
            {
                if (FullAmount == LeftAmount)
                    return 0;
                else if (LeftAmount <= 0)
                    return 2;
                else
                    return 1;
            }
        }

        #endregion
    }
}