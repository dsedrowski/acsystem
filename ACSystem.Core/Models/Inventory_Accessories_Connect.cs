﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Inventory_Accessories_Connect
    {
        [Key]
        public int ID { get; set; }

        public int InventoryID { get; set; }

        public int AccessoryID { get; set; }

        public bool Active { get; set; }

        public virtual Inventory Inventory { get; set; }

        public virtual Inventory_Accessories Accessory { get; set; }
    }
}