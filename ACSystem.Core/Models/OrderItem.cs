﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class OrderItem
    {
        [Key]
        public int ID { get; set; }

        public int OrderID { get; set; }

        public string Item { get; set; }

        public string Translated { get; set; }

        public virtual Order Order { get; set; }
    }
}