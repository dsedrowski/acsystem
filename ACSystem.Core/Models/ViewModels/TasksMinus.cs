﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class TasksMinus
    {
        public int TaskID { get; set; }

        public decimal ProductCount { get; set; }

        public decimal WorkDone { get; set; }

        public decimal WorkDoneMinus { get; set; }
    }
}