﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class QuestionsToTask
    {
        public List<Question> Questions { get; set; }
        public List<Task_Question> Task_Questions { get; set; }
        public List<Answer> Answers { get; set; }

        public bool IsCreating { get; set; }
    }
}