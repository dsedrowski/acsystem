﻿using System;

namespace ACSystem.Core.Models.ViewModels
{
    public class ProjectsListViewModel
    {
        public int Id { get; set; }
        public string ProjectNumber { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public int ApartmentsCount { get; set; }
        public ProjectStatus Status { get; set; }
        public string Customer { get; set; }
        public int CustomerId { get; set; }
        public bool CanDelete { get; set; }
        public string ColorHex { get; set; }
        public string InSystemID { get; set; }
        public bool IsChecked { get; set; }
        public bool HasMatchedChildrens { get; set; }
        public bool RemindBeforeEnd { get; set; }
        public bool ProjectEnd { get; set; }
        public DateTime EndDate { get; set; }
    }
}