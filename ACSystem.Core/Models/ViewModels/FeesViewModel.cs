﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class FeesViewModel
    {
        public int ID { get; set; }

        public int FeeNumber { get; set; }

        public string FeeNo { get { return FeeNumber.ToString().PadLeft(5, '0'); } }

        public string FitterID { get; set; }

        public string FitterFirstName { get; set; }

        public string FitterLastName { get; set; }

        public string FitterName { get { return $"{FitterFirstName} {FitterLastName}"; } }

        public int? ProjectID { get; set; }

        public int? ApartmentID { get; set; }

        public int? TaskID { get; set; }

        public string ProjectNumber { get; set; }

        public string ApartmentNumber { get; set; }

        public string ProductName { get; set; }

        public int? PayrollID { get; set; }

        public int? PayrollNumber { get; set; }
        
        public string PayrolNo { get { if (PayrollNumber.HasValue) return PayrollNumber.Value.ToString().PadLeft(5, '0'); else return ""; } }

        public DateTime FeeDate { get; set; }

        public string FeeDateConverted { get { return FeeDate.ToString("yyyy-MM-dd"); } }

        public DateTime? TakeDate { get; set; }

        public string TakeDateConverted { get { if (TakeDate.HasValue) return TakeDate.Value.ToString("yyyy-MM-dd"); else return String.Empty; } }

        public string Description { get; set; }

        public decimal FeeAmount { get; set; }

        public FeeStatus Status { get; set; }

    }
}