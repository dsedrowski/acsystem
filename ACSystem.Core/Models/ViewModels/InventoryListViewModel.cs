﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class InventoryListViewModel
    {
        public int Id { get; set; }
        public string Mark { get; set; }
        public string Type { get; set; }
        public string NumberOfMachine { get; set; }
        public string NumberOfMachine2 { get; set; }
        public DateTime? BuyDate { get; set; }
        public InventoryStatus Status { get; set; }
        public string Warehouse { get; set; }
        public string Owner { get; set; }
        public bool AccessoriesMatch { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsVitalityEnd { get; set; }
        public bool IsVitalityComeToEnd { get; set; }
        public bool ReviewIsComing { get; set; }

        public string BuyDateString { get { return BuyDate?.ToString("yyyy-MM-dd"); } }
    }
}