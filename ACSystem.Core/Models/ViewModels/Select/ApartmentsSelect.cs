﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels.Select
{
    public class ApartmentsSelect
    {
        public int ID { get; set; }

        public string ApartmentNumber { get; set; }
    }
}