﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels.Select
{
    public class TasksSelect
    {
        public int ID { get; set; }

        public string ProductName { get; set; }

        public string Sufix { get; set; }

        public string Apartment { get; set; }

        public int ApartmentID { get; set; }

        public string NameWithSufix { get { return $"{ProductName} {Sufix}"; } }
    }
}