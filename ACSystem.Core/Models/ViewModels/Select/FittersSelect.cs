﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels.Select
{
    public class FittersSelect
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public string SurName { get; set; }

        public string FullName { get { return $"{Name} {SurName}"; } }

        public bool IsLocked { get; set; }
    }
}