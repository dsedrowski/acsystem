﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class TasksPhotos
    {

        public int ID { get; set; }

        public string LP { get; set; }

        public string Zadanie { get; set; }

        public string Pytanie { get; set; }

        public string Rodzaj { get; set; }

        public string Opis { get; set; }

        public string Link { get; set; }

        public DateTime DataZamkniecia { get; set; }

        public bool Blocked { get; set; }

        public string Source { get; set; }

    }
}