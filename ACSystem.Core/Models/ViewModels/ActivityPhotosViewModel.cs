﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class ActivityPhotosViewModel
    {
        public int ID { get; set; }
        public string Photo { get; set; }
        public string Desc { get; set; }
    }

    public class QuestionPhotosViewModel
    {
        public int ID { get; set; }
        public string Photo { get; set; }
        public string Desc { get; set; }
        public string Translated { get; set; }
    }
}