﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class FitterWorkDoneViewModel
    {
        public string FitterID { get; set; }
        public decimal WorkDone { get; set; }
        public int WorkTime { get; set; }
        public decimal RealWorkTime { get; set; }
    }
}