﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class EventsViewModel
    {
        public int ID { get; set; }

        public DateTime Date { get; set; }

        public string Project { get; set; }

        public string LGH { get; set; }

        public string Task { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }
    }
}