﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class InvoiceElementViewModel
    {
        public int ID { get; set; }

        public int? TaskID { get; set; }

        public string ProductName { get; set; }
        
        public decimal Quantity { get; set; }

        public string Unit { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal Sum { get; set; }
    }
}