﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class ProductsViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        [Display(Name = "Opis (polski)")]
        public string DescriptionPL { get; set; }

        [Display(Name = "Opis (angielski)")]
        public string DescriptionEN { get; set; }

        [Display(Name = "Opis (szwedzki)")]
        public string DescriptionSE { get; set; }

        [Display(Name = "Cena")]
        public decimal Price { get; set; }

        [Display(Name = "Jednostka miary")]
        public string Jm { get; set; }

        [Display(Name = "Jednostka miary (EN)")]
        public string JmEng { get; set; }

        [Display(Name = "Jednostka miary (SE)")]
        public string JmSwe { get; set; }

        [Display(Name = "Stawka 1")]
        public decimal? Type1 { get; set; }

        [Display(Name = "Stawka 2")]
        public decimal? Type2 { get; set; }

        [Display(Name = "Stawka 3")]
        public decimal? Type3 { get; set; }

        [Display(Name = "Stawka 4")]
        public decimal? Type4 { get; set; }

        [Display(Name = "Stawka 5")]
        public decimal? Type5 { get; set; }

        [Display(Name = "Stawka 6")]
        public decimal? Type6 { get; set; }

        [Display(Name = "Rodzaj produktu")]
        public TaskType ProductType { get; set; }

        public bool IsActive { get; set; }

        public string InSystemID { get; set; }

        public bool ForAllProjects { get; set; }

        public bool NoExtra { get; set; }

        [Display(Name = "Planowany czas wykonania [h]")]
        public decimal ExpectedTime { get; set; }

        [Display(Name = "Procent czasu")]
        public int TimePercent { get; set; }
        
        public string TypeString
        {
            get
            {
                switch (ProductType)
                {
                    case TaskType.Construction:
                        return "Budowlanka";
                    case TaskType.Kitchen:
                        return "Kuchnia";
                    default:
                        return "";
                }
            }
        }

        public List<int> ConnectedProjects { get; set; }

        public List<Product_Project> Product_Prodject { get; set; }

        public List<Product_Group> Product_Group { get; set; }

        public List<Product_Question> Questions { get; set; }
    }

    public class ProductsListViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        [Display(Name = "Opis (polski)")]
        public string DescriptionPL { get; set; }

        [Display(Name = "Opis (angielski)")]
        public string DescriptionEN { get; set; }

        [Display(Name = "Opis (szwedzki)")]
        public string DescriptionSE { get; set; }

        [Display(Name = "Cena")]
        public decimal Price { get; set; }

        [Display(Name = "Jednostka miary")]
        public string Jm { get; set; }

        [Display(Name = "Jednostka miary (EN)")]
        public string JmEng { get; set; }

        [Display(Name = "Jednostka miary (SE)")]
        public string JmSwe { get; set; }

        [Display(Name = "Stawka 1")]
        public decimal? Type1 { get; set; }

        [Display(Name = "Stawka 2")]
        public decimal? Type2 { get; set; }

        [Display(Name = "Stawka 3")]
        public decimal? Type3 { get; set; }

        [Display(Name = "Stawka 4")]
        public decimal? Type4 { get; set; }

        [Display(Name = "Stawka 5")]
        public decimal? Type5 { get; set; }

        [Display(Name = "Stawka 6")]
        public decimal? Type6 { get; set; }

        [Display(Name = "Rodzaj produktu")]
        public TaskType ProductType { get; set; }

        public bool IsActive { get; set; }

        public bool ForAllProjects { get; set; }

        public string TypeString
        {
            get
            {
                switch (ProductType)
                {
                    case TaskType.Construction:
                        return "Budowlanka";
                    case TaskType.Kitchen:
                        return "Kuchnia";
                    default:
                        return "";
                }
            }
        }
    }
}