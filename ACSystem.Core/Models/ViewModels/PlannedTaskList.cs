﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class PlannedTaskList
    {
        public int TaskID { get; set; }

        public string TaskName { get; set; }

        public DateTime PlannedDate { get; set; }
    }
}