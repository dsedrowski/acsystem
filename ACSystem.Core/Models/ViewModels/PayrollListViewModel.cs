﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class PayrollListViewModel
    {
        public int Id { get; set; }
        
        public int Number { get; set; }

        public string NumberString { get; set; }

        public string No { get { return Number.ToString().PadLeft(5, '0'); } }

        public DateTime CreateDate { get; set; }

        public string Sum { get; set; }

        public string SumMinusHours { get; set; }

        public decimal? SumPlnMinusHours { get; set; }

        public string PayLeft { get; set; }

        public PayStatus Status { get; set; }

        public string Date
        {
            get
            {
                return CreateDate.ToString("yyyy-MM-dd HH:mm");
            }
        }
    }
}