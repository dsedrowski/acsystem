﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class PlannedTaskListForTask
    {
        public int EventID { get; set; }
        public string FitterID { get; set; }
        public string FitterName { get; set; }
        public DateTime PlannedDate { get; set; }
        public string Description { get; set; }
    }
}