﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class ParallelProjectContentViewModel
    {
        public string ProductName { get; set; }

        public decimal Count { get; set; }

        public string Unit { get; set; }

        public decimal Done { get; set; }

        public decimal DonePercent { get; set; }

        public decimal Faults { get; set; }

        public decimal Value { get; set; }

        public decimal WorkHours { get; set; }

        public decimal PayrollSum { get; set; }

        public decimal PayrollSumPLN { get; set; }

        public decimal Fees { get; set; }

        public decimal FeesPLN { get; set; }
    }
}