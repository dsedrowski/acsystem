﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class InvoiceViewModel
    {
        public int ID { get; set; }

        public int InvoiceNumber { get; set; }

        public string ProjectNumber { get; set; }

        public string CustomerName { get; set; }

        public decimal Total { get; set; }

        public DateTime InvoiceDate { get; set; }

        public DateTime PaymentTerm { get; set; }

        public InvoiceStatus Status { get; set; }

        public string InvoiceNo { get { return InvoiceNumber.ToString().PadLeft(6, '0'); } }

        public string InvoiceDateString { get { return InvoiceDate.ToString("yyyy-MM-dd"); } }

        public string PaymentTermString { get { return PaymentTerm.ToString("yyyy-MM-dd"); } }

        public string URL { get; set; }
    }
}