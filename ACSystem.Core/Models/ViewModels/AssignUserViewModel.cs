﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class AssignUserViewModel
    {
        public List<ACSystemUser> UsersList { get; set; }
        public Todo Todo { get; set; }
    }
}