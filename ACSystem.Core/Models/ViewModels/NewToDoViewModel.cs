﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class NewToDoViewModel
    {
        public List<Tasks> TaskList { get; set; }
        public List<Fitters> Fitters { get; set; }
        //public List<Type> Types { get; set; }
        public List<ACSystemUser> Users { get; set; }
    }
}