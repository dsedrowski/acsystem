﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class CustomersListViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string HouseNo { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Email { get; set; }

        public bool IsLocked { get; set; }

        public string Address
        {
            get
            {
                var address = Street;
                address += (!string.IsNullOrEmpty(HouseNo)) ? "/" + HouseNo : " ";
                address += " " + PostalCode;
                address += " " + City;

                return address;
            }
        }
    }
}