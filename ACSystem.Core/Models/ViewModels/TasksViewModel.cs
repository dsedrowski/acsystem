﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class TasksViewModel
    {
        public int Id { get; set; }

        public string ProductName { get; set; }

        public decimal ProductCount { get; set; }

        public string Jm { get; set; }

        public decimal ProductPrice { get; set; }

        public int Week { get; set; }

        public TaskType Type { get; set; }

        public TaskStatus Status { get; set; }
    }

    public class TaskTodo
    {
        public int Id { get; set; }

        public string ProductName { get; set; }

        public int ProjectId { get; set; }
        public string ProjectNumber { get; set; }
        
        public int ApartmentId { get; set; }
        public string ApartmentNumber { get; set; }

        public DateTime ClosedDate { get; set; }

        public TaskType Type { get; set; }

        public TaskStatus Status { get; set; }

        public string ClosedDateString
        {
            get
            {
                return ClosedDate.ToString("yyyy-MM-dd");
            }
        }

        public string FitterFinished { get; set; }
    }

    public class TaskFullViewModel
    {
        public int TaskID { get; set; }
        public string ProductName { get; set; }

        public int ProjectId { get; set; }
        public string ProjectNumber { get; set; }

        public int ApartmentId { get; set; }
        public string ApartmentNumber { get; set; }
        
        public string CurrentFitterId { get; set; }
        public string CurrentFitterName { get; set; }

        public int ProductId { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public decimal ProductPrice { get; set; }

        public decimal? PercentType1 { get; set; }

        public decimal? PercentType2 { get; set; }

        public decimal? PercentType3 { get; set; }

        public decimal? PercentType4 { get; set; }

        public decimal? PercentType5 { get; set; }

        public decimal? PercentType6 { get; set; }

        public decimal? ProductCount { get; set; }

        public string JM { get; set; }

        public bool Accepted { get; set; }

        public TaskType Type { get; set; }

        public TaskStatus Status { get; set; }

        public QuestionsViewModel Questions { get; set; }

        public Products Product { get; set; }

        public List<Product_Project> Product_Project { get; set; }

        public string GetTaskName(int projectID)
        {
            var connect = this.Product_Project.FirstOrDefault(q => q.ProjectID == projectID);

            return (connect != null && !string.IsNullOrEmpty(connect.ProductName)) ? connect.ProductName : this.Product.Name;
        }
    }

    public class ProjectFullViewModel
    {
        public int ProjectId { get; set; }
        public string ProjectNumber { get; set; }
    }

    public class QuestionsViewModel
    {
        public TaskQuestionStatus BaseUnits { get; set; }
        public TaskQuestionStatus WallUnits { get; set; }
        public TaskQuestionStatus TallUnits { get; set; }
        public TaskQuestionStatus Fillers { get; set; }
        public TaskQuestionStatus CeillingScribe { get; set; }
        public TaskQuestionStatus SidePiece { get; set; }
        public TaskQuestionStatus Worktop { get; set; }
        public TaskQuestionStatus Sink { get; set; }
        public TaskQuestionStatus Handles { get; set; }
        public TaskQuestionStatus Plinth { get; set; }
        public TaskQuestionStatus Grids { get; set; }
        public TaskQuestionStatus Lighting { get; set; }
        public TaskQuestionStatus Silicon { get; set; }
        public TaskQuestionStatus Manuals { get; set; }
        public TaskQuestionStatus Machines { get; set; }
        public TaskQuestionStatus Oven { get; set; }
        public TaskQuestionStatus FridgeFreezer1 { get; set; }
        public TaskQuestionStatus FridgeFreezer2 { get; set; }
        public TaskQuestionStatus Hob { get; set; }
        public TaskQuestionStatus Dishwasher { get; set; }
        public TaskQuestionStatus Mikrowave { get; set; }
        public TaskQuestionStatus Fan { get; set; }
    }
}