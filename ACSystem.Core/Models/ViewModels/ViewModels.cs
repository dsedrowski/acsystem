﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class PrevNext
    {
        public int Prev { get; set; }
        public int Next { get; set; }
    }
}