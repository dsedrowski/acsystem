﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models.ViewModels
{
    public class Filter_ProjectExcel
    {
        public Projects Project { get; set; }
        public List<Apartments> Apartments { get; set; }
        public List<ACSystemUser> Fitters { get; set; }
        public List<Product_Project> Products { get; set; }
    }
}