﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ACSystem.Core.Models {
    public class view_GanttData {
        public int ProjectId { get; set; }
        public string ProjectFortnox { get; set; }
        public ProjectStatus ProjectStatus { get; set; }
        public string ProjectStatusString { get; set; }
        public string ProjectNumber { get; set; }
        public string ProjectDescription { get; set; }
        public DateTime? ProjectStartDate { get; set; }
        public DateTime? ProjectEndDate { get; set; }
        public int ApartmentId { get; set; }
        public string Cage { get; set; }
        public int Floor { get; set; }
        public string LGH { get; set; }
        public string ApartmentDescription { get; set; }

        [Key]
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public TaskStatus TaskStatus { get; set; }
        public string TaskStatusString { get; set; }
        public decimal ExpectedTime { get; set; }
        public int ExpectedDays { get; set; }
        public DateTime? TaskStartDate { get; set; }
        public DateTime? TaskEndDate { get; set; }
    }
}