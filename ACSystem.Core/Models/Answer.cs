﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Answer
    {
        [Key]
        public int ID { get; set; }

        public int QuestionID { get; set; }

        public string AnswerPol { get; set; }

        public string AnswerTranslate { get; set; }

        public bool? IsError { get; set; }

        public bool? IsNotApplicable { get; set; }

        public bool ExtraFieldNotTT { get; set; }

        public bool ExtraFieldTT { get; set; }

        public virtual Question Question { get; set; }

        public virtual ICollection<Task_Question> Task_Questions { get; set; }
        
        #region NotMapped

        [NotMapped]
        public string ExtraFields { get; set; }

        [NotMapped]
        public string ExtraField_Get
        {
            get
            {
                if (ExtraFieldNotTT)
                    return "Bez tłumaczenia";
                else if (ExtraFieldTT)
                    return "Do tłumaczenia";
                else
                    return "Brak info";
            }
        }

        [NotMapped]
        public string IsError_Get => (IsError == true) ? "Tak" : "Nie";

        [NotMapped]
        public string IsNotApplicable_Get => (IsNotApplicable == true) ? "Tak" : "Nie"; 

        #endregion
    }
}