﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Policy;
using System.Web;
// ReSharper disable All

namespace ACSystem.Core.Models
{
    public class Tasks
    {
        public Tasks()
        {
            this.Fitters = new HashSet<Fitters>();
            this.Task_Fitter = new HashSet<Task_Fitter>();
            this.TaskPatches = new HashSet<TaskPatches>();
            this.Fees = new HashSet<Fees>();
            this.InvoiceElements = new HashSet<InvoiceElements>();
            this.Task_AccessGrant = new HashSet<Task_AccessGrant>();
            this.PayrollItems = new HashSet<PayrollItems>();
            this.TaskComments = new HashSet<TaskComment>();
            this.Orders = new HashSet<Order>();
            this.TaskDocuments = new HashSet<TaskDocuments>();
            this.ReportElems = new HashSet<ReportElem>();
            this.Access_User_Task = new HashSet<Access_User_Task>();
            this.Todos = new HashSet<Todo>();
            this.TaskGroup = new HashSet<Tasks>();
            this.Task_Question = new HashSet<Task_Question>();
            this.PlaceErrors = new HashSet<PlaceError>();
            this.Documents = new HashSet<Document>();
        }

        [Key]
        public int Id { get; set; }

        public int ApartmentId { get; set; }

        public int ProductId { get; set; }

        public string TaskSufix { get; set; }

        public int? TaskOriginalId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public decimal ProductPrice { get; set; }

        public decimal Distance { get; set; }

        public decimal DistancePrice { get; set; }

        public bool IsHourlyPay { get; set; }

        public decimal? HourlyPrice { get; set; }

        public decimal PercentType1 { get; set; }

        public decimal PercentType2 { get; set; }

        public decimal PercentType3 { get; set; }

        public decimal PercentType4 { get; set; }

        public decimal PercentType5 { get; set; }

        public decimal PercentType6 { get; set; }

        public decimal ProductCount { get; set; }

        public TaskStatus Status { get; set; }

        public TaskProduction Production { get; set; }

        public TaskErrorType ErrorType { get; set; }

        public string Leader { get; set; }

        public bool Payroll { get; set; }

        public bool Invoice { get; set; }

        public bool Accepted { get; set; }

        public string Description { get; set; }

        public string ColorHex { get; set; }

        public DateTime? ClosedDate { get; set; }

        public bool Blocked { get; set; }

        public bool Todo { get; set; }

        public bool? ReportSent { get; set; }

        public string ChangeReason { get; set; }

        public bool ForceInvoice { get; set; }

        public bool IsInFees { get; set; }

        public DateTime? ReportSentDate { get; set; }

        public string InvoiceDescription { get; set; }

        public string InvoiceDescriptionPL { get; set; }

        public string ErrorCode { get; set; }

        public int? RoomID { get; set; }

        public int? GroupTaskID { get; set; }

        public bool? DontShowInNormCheck { get; set; }
        public string Name { get; set; }

        public virtual Apartments Apartment { get; set; }

        public virtual Products Product { get; set; }

        public virtual Task_Questions Task_Questions { get; set; }

        public virtual Rooms Room { get; set; }

        public virtual Tasks TaskGrouped { get; set; }

        public virtual ICollection<Tasks> TaskGroup { get; set; }

        public virtual ICollection<Fitters> Fitters { get; set; }

        public virtual ICollection<Task_Fitter> Task_Fitter { get; set; }

        public virtual ICollection<TaskPatches> TaskPatches { get; set; }

        public virtual ICollection<Fees> Fees { get; set; }

        public virtual ICollection<InvoiceElements> InvoiceElements { get; set; }

        public virtual ICollection<Task_AccessGrant> Task_AccessGrant { get; set; }

        public virtual ICollection<PayrollItems> PayrollItems { get; set; }

        public virtual ICollection<TaskComment> TaskComments { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public virtual ICollection<TaskDocuments> TaskDocuments { get; set; }

        public virtual ICollection<ReportElem> ReportElems { get; set; }

        public virtual ICollection<Access_User_Task> Access_User_Task { get; set; }

        public virtual ICollection<Todo> Todos { get; set; }

        public virtual ICollection<Task_Question> Task_Question { get; set; }

        public virtual ICollection<PlaceError> PlaceErrors { get; set; }

        public virtual ICollection<Invoice_Adds> Invoice_Adds { get; set; }
        public virtual ICollection<Document> Documents { get; set; }

        [NotMapped]
        public decimal ProductPriceSum
        {
            get { return ProductCount * ProductPrice; }
        }

        [NotMapped]
        public string ApartmentsArray { get; set; }
        [NotMapped]
        public string TasksArray { get; set; }

        [NotMapped]
        public string ProjectID { get; set; }

        public string GetTaskName(int projectID)
        {
            var connect = this.Product.Product_Project.FirstOrDefault(q => q.ProjectID == projectID);

            return (connect != null && !string.IsNullOrEmpty(connect.ProductName)) ? connect.ProductName : this.Product.Name;
        }

        public string GetTaskNameOnInvoice(int projectID)
        {
            var connect = this.Product.Product_Project.FirstOrDefault(q => q.ProjectID == projectID);

            return (connect != null && !string.IsNullOrEmpty(connect.OnInvoiceName)) ? connect.OnInvoiceName : this.Product.DescriptionSE;
        }

        [NotMapped]
        public static List<TaskStatus> StatusesToCalculateTimes { get { return new List<TaskStatus> {
                TaskStatus.Finished,
                TaskStatus.FinishedWithFaults,
                TaskStatus.FinishedWithFaultsCorrected,
                TaskStatus.Stoped
            };
            }
        }
    }
}