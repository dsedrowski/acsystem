﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class FavouriteCity
    {
        [Key, Required, Column(Order = 1) ]
        public int ID { get; set; }

        public string UserId { get; set; }

        public string City { get; set; }

        public virtual ACSystemUser User { get; set; }
    }
}