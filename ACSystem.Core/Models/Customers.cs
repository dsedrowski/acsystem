﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Customers
    {
        public Customers()
        {
            this.ProjectManagers = new HashSet<ProjectManagers>();
            this.Projects = new HashSet<Projects>();
            this.InvoiceList = new HashSet<Invoice>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Nazwa klienta")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Ulica")]
        public string Street { get; set; }

        [Display(Name = "Numer Domu")]
        public string HouseNo { get; set; }

        [Required]
        [Display(Name = "Kod pocztowy")]
        public string PostalCode { get; set; }

        [Required]
        [Display(Name = "Miasto")]
        public string City { get; set; }

        [Required]
        [Display(Name = "NIP")]
        public string VATNumber { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Termin płatności (dni)")]
        public int PaymentTerm { get; set; }

        [Required]
        [Display(Name = "Język dokumentów")]
        public Leanguage DocumentsLeanguage { get; set; }

        [Required]
        [Display(Name = "Stawka VAT")]
        public int VAT { get; set; }

        [Required]
        [Display(Name = "Automatyczna wysyłka")]
        public bool AutoSendEmail { get; set; }

        [Required]
        [Display(Name = "Data utworzenia")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Odwrotne obciążenie")]
        public bool ReverseCharge { get; set; }

        [Display(Name = "Klient zablokowany")]
        public bool IsLocked { get; set; }

        [Display(Name = "ID w systemie")]
        public string InSystemID { get; set; }

        [NotMapped]
        public string Address
        {
            get {
                    var address = Street;
                        address += (!string.IsNullOrEmpty(HouseNo)) ? "/" + HouseNo : " ";
                        address += " " + PostalCode;
                        address += " " + City;

                    return address;
                }
        }

        public virtual ICollection<ProjectManagers> ProjectManagers { get; set; }

        public virtual ICollection<Projects> Projects { get; set; }

        public virtual ICollection<Invoice> InvoiceList { get; set; }
    }
}