﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class view_CheckTaskPriceNorm
    {
        public int ProjectID { get; set; }

        public string ProjectNumber { get; set; }

        public int ApartmentID { get; set; }

        public string LGH { get; set; }

        [Key]
        public int TaskID { get; set; }

        public string ProductName { get; set; }

        public decimal ProductPriceReal { get; set; }

        public decimal ProductPricePlanned { get; set; }
    }
}