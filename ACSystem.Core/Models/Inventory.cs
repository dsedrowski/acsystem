﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Inventory
    {
        public Inventory()
        {
            this.Inventory_Accessories_Connect = new HashSet<Inventory_Accessories_Connect>();
            this.Inventory_Fitter = new HashSet<Inventory_Fitter>();
            this.Inventory_Photo = new HashSet<Inventory_Photo>();
            this.Inventory_Check = new HashSet<Inventory_Check>();
        }

        [Key]
        public int ID { get; set; }

        public int? ProjectID { get; set; }

        [Display(Name = "Magazyn")]
        public int? WarehouseID { get; set; }

        [Required]
        [Display(Name = "Typ maszyny")]
        public int TypeID { get; set; }

        [Display(Name = "Właściciel")]
        public string OwnerID { get; set; }

        [Required]
        [Display(Name = "Własność")]
        public InventoryOwnerType OwnerType { get; set; }

        [Display(Name = "Numer ewidencyjny S")]
        public string EvidenceNumber { get; set; }

        [Display(Name = "Numer ewidencyjny T")]
        public string EvidenceNumber2 { get; set; }

        [Required]
        [Display(Name = "Marka")]
        public string Mark { get; set; }

        [Required]
        [Display(Name = "Numer modelu")]
        public string ModelNumber { get; set; }

        [Required]
        [Display(Name = "Status")]
        public InventoryStatus Status { get; set; }

        [Required]
        [Display(Name = "Data zakupu")]
        public DateTime BuyDate { get; set; }

        [Display(Name = "Kwota zakupu netto")]
        public decimal BuyPriceNetto { get; set; }

        [Required]
        [Display(Name = "Kwota zakupu brutto")]
        public decimal BuyPriceBrutto { get; set; }

        [Display(Name = "Miejsce zakupu")]
        public string BuyPlace { get; set; }

        [Display(Name = "Cena sprzedaży")]
        public decimal? SellPrice { get; set; }
        [Required]
        [Display(Name = "Stan maszyny")]
        public Inventory_ConditionStatus ConditionStatus { get; set; }

        [Required]
        [Display(Name = "Żywotność maszyny (lata)")]
        public int LifeTime { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Display(Name = "Projekt")]
        public virtual Projects Project { get; set; }

        [Display(Name = "Magazyn")]
        public virtual InventoryWarehouse Warehouse { get; set; }

        [Display(Name = "Typ maszyny")]
        public virtual InventoryType Type { get; set; }

        [Display(Name = "Właściciel")]
        public virtual ACSystemUser Owner { get; set; }

        public virtual ICollection<Inventory_Accessories_Connect> Inventory_Accessories_Connect { get; set; }

        public virtual ICollection<Inventory_Fitter> Inventory_Fitter { get; set; }

        public virtual ICollection<Inventory_Photo> Inventory_Photo { get; set; }

        public virtual ICollection<Inventory_Check> Inventory_Check { get; set; }
    }
}