﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Inventory_Accessories
    {
        public Inventory_Accessories()
        {
            this.Photos = new HashSet<Inventory_Accessories_Photo>();
            this.Inventory_Accessories_Connect = new HashSet<Inventory_Accessories_Connect>();
        }

        [Key]
        public int ID { get; set; }
        
        [Display(Name = "Nazwa akcesorium")]
        public string Name { get; set; }
        
        [Display(Name = "Numer seryjny")]
        public string EvidenceNumber { get; set; }

        public virtual ICollection<Inventory_Accessories_Connect> Inventory_Accessories_Connect { get; set; }

        public virtual ICollection<Inventory_Accessories_Photo> Photos { get; set; }
    }
}