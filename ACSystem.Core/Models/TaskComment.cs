﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class TaskComment
    {
        public int ID { get; set; }

        public string FitterID { get; set; }

        public int ApartmentID { get; set; }

        public int TaskID { get; set; }

        public string Comment { get; set; }

        public DateTime Timestamp { get; set; }

        public bool IsViewed { get; set; }

        public virtual Fitters Fitter { get; set; }

        public virtual Apartments Apartment { get; set; }

        public virtual Tasks Task { get; set; }
    }
}