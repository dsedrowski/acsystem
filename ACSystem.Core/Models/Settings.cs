﻿using System.ComponentModel.DataAnnotations;

namespace ACSystem.Core.Models {
    public class Settings {
        [Key]
        public int ID { get; set; }

        public decimal ServiceTaskTime { get; set; }

        public int InvoicePercent { get; set; }

        public int ProjectEndReminder { get; set; }

        public int HoursPerDay { get; set; }
    }
}