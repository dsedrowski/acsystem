﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Fitter_Hours
    {
        public Fitter_Hours()
        {
            this.Fitter_Hours_Project = new HashSet<Fitter_Hours_Project>();
        }

        [Key]
        public int ID { get; set; }

        public string FitterID { get; set; }

        public DateTime Date { get; set; }

        public decimal Hours { get; set; }

        public bool FreeDay { get; set; }

        public int? PayrollID { get; set; }

        public virtual ACSystemUser Fitter { get; set; }

        [ForeignKey("PayrollID")]
        public virtual Payroll Payroll { get; set; }

        public virtual ICollection<Fitter_Hours_Project> Fitter_Hours_Project { get; set; }
    }
}