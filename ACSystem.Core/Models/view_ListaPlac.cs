﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class view_ListaPlac
    {
        [Key]
        public string UzytkownikID { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public int ProjektID { get; set; }
        public string NumerProjektu { get; set; }
        public int ApartamentID { get; set; }
        public string NumerApartamentu { get; set; }
        public int ProduktID { get; set; }
        public string NazwaProduktu { get; set; }
        public int CzynnoscID { get; set; }
        public TaskStatus CzynnoscStatus { get; set; }
        public int ZadanieID { get; set; }
        public TaskStatus ZadanieStatus { get; set; }
        public decimal PracaWykonana { get; set; }
        public decimal CenaProduktu { get; set; }
        public decimal? Stawka { get; set; }
        public decimal? DoZaplaty { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool Zaakceptowane { get; set; }

        public static IQueryable<view_ListaPlac> All()
        {
            var db = new ACSystemContext();

            return db.Database.SqlQuery<view_ListaPlac>("SELECT * FROM view_ListaPlac").AsQueryable();
        }

        public static IQueryable<view_ListaPlac> AllBetweenDates(DateTime start, DateTime end)
        {
            var db = new ACSystemContext();

            return db.Database.SqlQuery<view_ListaPlac>(
                $"SELECT * FROM view_ListaPlac WHERE Timestamp BETWEEN '{start}' AND '{end}'").AsQueryable();
        }

        public static IQueryable<view_ListaPlac> AllForTask(int taskID)
        {
            var db = new ACSystemContext();

            return db.Database.SqlQuery<view_ListaPlac>($"SELECT * FROM view_ListaPlac WHERE ZadanieID = {taskID}").AsQueryable();
        }

        public static IQueryable<view_ListaPlac> AllForTaskBetweenDates(int taskID, DateTime start, DateTime end)
        {
            var db = new ACSystemContext();

            return db.Database.SqlQuery<view_ListaPlac>(
                $"SELECT * FROM view_ListaPlac WHERE ZadanieID = {taskID} AND Timestamp BETWEEN '{start}' AND '{end}'").AsQueryable();
        }

        public static IQueryable<view_ListaPlac> AllForFitter(string fitterID)
        {
            var db = new ACSystemContext();

            return db.Database.SqlQuery<view_ListaPlac>(
                "SELECT * FROM view_ListaPlac WHERE UzytkownikID = @FitterID",
                new SqlParameter("FitterID", fitterID)).AsQueryable();
        }

        public static IQueryable<view_ListaPlac> AllForFitterBetweenDates(string fitterID, DateTime start, DateTime end)
        {
            var db = new ACSystemContext();

            return db.Database.SqlQuery<view_ListaPlac>(
                "SELECT * FROM view_ListaPlac WHERE UzytkownikID = @FitterID AND Timestamp BETWEEN '@StartDate' AND '@EndDate'",
                new SqlParameter("FitterID", fitterID),
                new SqlParameter("StartDate", start),
                new SqlParameter("EndDate", end)).AsQueryable();
        }

        public static IQueryable<view_ListaPlac> AllFinishedToDate(DateTime toDate)
        {
            var db = new ACSystemContext();

            var query = $@"SELECT UzytkownikID, Imie, Nazwisko, ProjektID, NumerProjektu, ApartamentID, NumerApartamentu, ProduktID, NazwaProduktu, ZadanieID, ZadanieStatus, SUM(PracaWykonana) AS PracaWykonana, SUM(DoZaplaty) AS DoZaplaty
                FROM view_ListaPlac 
                        WHERE (ZadanieStatus <> {(int)TaskStatus.Free} AND ZadanieStatus <> {(int)TaskStatus.UnderWork}) AND CAST(Timestamp AS DATE) <= '{toDate.ToString("yyyy-MM-dd")}' AND InPayroll = 0
                        GROUP BY UzytkownikID, Imie, Nazwisko, ProjektID, NumerProjektu, ApartamentID, NumerApartamentu, ProduktID, NazwaProduktu, ZadanieID, ZadanieStatus";

            return db.Database.SqlQuery<view_ListaPlac>(query).AsQueryable();
        }
    }
}