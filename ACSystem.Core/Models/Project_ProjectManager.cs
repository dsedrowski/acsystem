﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Project_ProjectManager
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int ProjectManagerId { get; set; }

        public virtual Projects Project { get; set; }
        public virtual ProjectManagers ProjectManager { get; set; }
    }
}