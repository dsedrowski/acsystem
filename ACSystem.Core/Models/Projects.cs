﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACSystem.Core.Models
{
    public class Projects
    {
        public Projects()
        {
            this.Project_ProjectManager = new HashSet<Project_ProjectManager>();
            this.Apartments = new HashSet<Apartments>();
            this.InvoiceList = new HashSet<Invoice>();
            this.PayrollItems = new HashSet<PayrollItems>();
            this.Orders = new HashSet<Order>();
            this.Fees = new HashSet<Fees>();
            this.Documents = new HashSet<ProjectDocuments>();
            this.Product_Project = new HashSet<Product_Project>();
            this.ChildProjects = new HashSet<Projects>();
            this.ReportNags = new HashSet<ReportNag>();
            this.Access_User_Project = new HashSet<Access_User_Project>();
            this.Activities = new HashSet<Project_Activity>();
            this.Product_Plan = new HashSet<Product_Plan>();
            this.FavouriteProjects = new HashSet<FavouriteProject>();
            this.PlaceErrors = new HashSet<PlaceError>();
            this.Fitter_Hours_Project = new HashSet<Fitter_Hours_Project>();

            this.FirstDoService = false;
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Nazwa klienta")]
        public int CustomerId { get; set; }

        [Required]
        //[Index(IsUnique = true)]
        [StringLength(450)]
        [Display(Name = "Numer projektu")]
        public string ProjectNumber { get; set; }

        [Display(Name = "Numer projektu klienta")]
        public string CustomerProjectNumber { get; set; }

        [Display(Name = "Ulica")]
        public string Street { get; set; }

        [Required]
        [Display(Name = "Miasto")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Status")]
        public ProjectStatus Status { get; set; }

        [Required]
        [Display(Name = "Data rozpoczęcia")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Data zakończenia")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Data utworzenia")]
        public DateTime CreateDate { get; set; }

        public string Description { get; set; }

        public int? CalendarID { get; set; }

        public bool ShowInCalendar { get; set; }

        public string InSystemID { get; set; }

        public bool TakePercent { get; set; }

        public bool IsChecked { get; set; }

        public string InvoiceDescription { get; set; }

        public bool IsGroupedInvoice { get; set; }

        public bool ChildProject { get; set; }

        public int? ParentProjectID { get; set; }

        public bool? FirstDoService { get; set; }

        public bool? InFortnox { get; set; }

        public string SupervisorID { get; set; }

        public decimal? CompanyPercent { get; set; }

        public decimal? FitterPercent { get; set; }

        public virtual Customers Customer { get; set; }

        public virtual ACSystemUser Supervisor { get; set; }

        public virtual ICollection<Project_ProjectManager> Project_ProjectManager { get; set; }

        public virtual ICollection<Apartments> Apartments { get; set; }

        public virtual ICollection<Invoice> InvoiceList { get; set; }

        public virtual ICollection<InvoiceElements> InvoiceElements { get; set; }

        public virtual ICollection<PayrollItems> PayrollItems { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public virtual ICollection<Fees> Fees { get; set; }

        public virtual ICollection<ProjectDocuments> Documents { get; set; }

        public virtual ICollection<Product_Project> Product_Project { get; set; }

        public virtual ICollection<ReportNag> ReportNags { get; set; }

        public virtual Calendar Calendar { get; set; }

        public virtual Projects ParentProject { get; set; }

        public virtual ICollection<Projects> ChildProjects { get; set; }

        public virtual ICollection<Inventory> InventoryList { get; set; }

        public virtual ICollection<Inventory_Fitter> Inventory_FitterList { get; set; }

        public virtual ICollection<Access_User_Project> Access_User_Project { get; set; }

        public virtual ICollection<Product_Plan> Product_Plan { get; set; }

        public virtual ICollection<Project_Activity> Activities { get; set; }
        
        public virtual ICollection<FavouriteProject> FavouriteProjects { get; set; }

        public virtual ICollection<PlaceError> PlaceErrors { get; set; }

        public virtual ICollection<Fitter_Hours_Project> Fitter_Hours_Project { get; set; }

        private List<int> _projectManagers;
        [NotMapped]
        [Required]
        [Display(Name = "Menedżerowie projektu")]
        public List<int> ProjectManagers {
            get
            {
                if (_projectManagers != null)
                    return _projectManagers;

                _projectManagers = new List<int>();

                foreach(var id in Project_ProjectManager)
                {
                    _projectManagers.Add(id.ProjectManagerId);
                }

                return _projectManagers;
            }

            set
            {
                this._projectManagers = value;
            }
        }
    }
}