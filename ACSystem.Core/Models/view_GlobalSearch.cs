﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class view_GlobalSearch
    {
        public string InSystemID { get; set; }
        public string ProjectNumber { get; set; }
        public string Adres { get; set; }
        public string LGH { get; set; }
        public string Task { get; set; }
        public string Fitter { get; set; }
        public string TimeStamp { get; set; }

        public int? ProjectID { get; set; }
        public int? ApartmentID { get; set; }
        public int? TaskID { get; set; }
        public string FitterID { get; set; }

        public int? InvoiceID { get; set; }
        public string InvoiceNumber { get; set; }
        public string FortnoxUrl { get; set; }
        public string P_id { get; set; }
        public string P_no { get; set; }
    }
}