﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class TaskActivityPhotos
    {
        [Key]
        public int Id { get; set; }
        public int Task_FitterId { get; set; }
        public string Photo { get; set; }
        public string Desc { get; set; }
        public bool Blocked { get; set; }

        public virtual Task_Fitter Task_Fitter { get; set; } 
    }
}