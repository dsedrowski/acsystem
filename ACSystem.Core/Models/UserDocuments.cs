﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models {
    public class UserDocuments {

        [Key]
        public int ID { get; set; }

        public string UserID { get; set; }

        public string URL { get; set; }

        public string DocumentDescription { get; set; }

        public bool ShowToUser { get; set; }

        [ForeignKey("UserID")]
        public virtual ACSystemUser User { get; set; }
    }
}