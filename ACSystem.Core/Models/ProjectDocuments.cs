﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class ProjectDocuments
    {
        [Key]
        public int ID { get; set; }

        public int ProjectID { get; set; }

        public string FileName { get; set; }

        public string FilePath { get; set; }

        public virtual Projects Project { get; set; }
    }
}