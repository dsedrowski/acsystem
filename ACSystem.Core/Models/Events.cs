﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ACSystem.Core.Models
{
    public class Events
    {
        [Key]
        public int Id { get; set; }
        public EventType Type { get; set; }
        public int? ReferenceTo { get; set; }
        public string ReferenceToUser { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public bool AllDay { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? CalendarID { get; set; }

        public virtual Calendar Calendar { get; set; }
    }
}