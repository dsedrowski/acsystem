﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Todo
    {
        [Key]
        public int ID { get; set; }

        public int? TaskID { get; set; }

       // [ForeignKey("Fitter")]
        public string FitterID { get; set; }

        public TodoType Type { get; set; }

        public DateTime Date { get; set; }

        public string ErrorType { get; set; }

        public bool Checked { get; set; }

        public string AssignedUserId { get; set; }

        public string Message { get; set; }

        public int? PlaceErrorID { get; set; }

        public virtual Tasks Task { get; set; }

        public virtual ACSystemUser User { get; set; }

        public virtual ACSystemUser AssignedUser { get; set; }

        public virtual PlaceError PlaceError { get; set; }
        public virtual Fitters Fitter { get; set; }
    }
}