﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Product_Group
    {
        [Key] public int Product_GroupID { get; set; }

        public int ProductMainID { get; set; }

        public int ProductSubID { get; set; }

        public decimal Quantity { get; set; }

        public bool CreateTask { get; set; }

        public virtual Products ProductMain { get; set; }

        public virtual Products ProductSub { get; set; }


    }
}