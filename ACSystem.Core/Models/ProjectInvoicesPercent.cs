﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class ProjectInvoicesPercent
    {
        public ProjectInvoicesPercent()
        {
            this.Element = new HashSet<ProjectInvoicesPercent_Elements>();
        }

        [Key]
        public int ID { get; set; }

        public DateTime GenerateDate { get; set; }

        public decimal AmountSum { get; set; }

        public int CalcPercent { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<ProjectInvoicesPercent_Elements> Element { get; set; }
    }

    public class ProjectInvoicesPercentViewModel
    {
        public int ID { get; set; }

        public DateTime GenerateDate { get; set; }

        public decimal AmountSum { get; set; }

        public int CalcPercent { get; set; }

        public string GenerateDate_String
        {
            get
            {
                return GenerateDate.ToString("yyyy-MM-dd HH:mm");
            }
        }

        public string AmountSum_String
        {
            get
            {
                return Math.Ceiling(AmountSum).ToString();
            }
        }
    }
}