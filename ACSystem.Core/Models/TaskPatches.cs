﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class TaskPatches
    {
        [Key]
        public int ID { get; set; }

        public int TaskID { get; set; }

        public string ItemTitle { get; set; }

        public string ItemDescription { get; set; }

        public bool Done { get; set; }

        public DateTime? Timestamp { get; set; }

        public string FitterName { get; set; }

        public string Photo { get; set; }

        public bool PhotoBlocked { get; set; }

        public string Room { get; set; }

        public string Code { get; set; }

        public virtual Tasks Task { get; set; }
    }
}