﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Fitter_Hours_Project
    {
        [Key]
        public int ID { get; set; }

        public int Fitter_HoursID { get; set; }

        public int ProjectID { get; set; }

        public decimal Hours { get; set; }

        public bool FreeDay { get; set; }

        public int? PayrollID { get; set; }

        public virtual Fitter_Hours Fitter_Hours { get; set; }

        public virtual Projects Project { get; set; }

        public virtual Payroll Payroll { get; set; }
    }
}