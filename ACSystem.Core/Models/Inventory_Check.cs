﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Inventory_Check
    {
        public Inventory_Check()
        {
            this.Photos = new HashSet<Inventory_Check_Photo>();
        }

        [Key]
        public int ID { get; set; }

        public int InventoryID { get; set; }

        public string UserID { get; set; }

        public DateTime CheckDate { get; set; }

        [Required]
        [Display(Name = "Stan maszyny")]
        public Inventory_ConditionStatus ConditionStatus { get; set; }

        [Required]
        [Display(Name = "Komentarz")]
        public string Comment { get; set; }

        public virtual Inventory Inventory { get; set; }

        public virtual ACSystemUser User { get; set; }

        public virtual ICollection<Inventory_Check_Photo> Photos { get; set; }
    }
}