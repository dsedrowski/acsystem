﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class ReportNag
    {
        public ReportNag()
        {
            this.Followers = new HashSet<ReportNag>();
            this.Elements = new HashSet<ReportElem>();
        }

        [Key]
        public int ID { get; set; }

        public TaskReportType Type { get; set; }

        public int Number { get; set; }

        public DateTime Date { get; set; }

        public int ProjectID { get; set; }

        public int ApartmentID { get; set; }

        public ProjectStatus Status { get; set; }

        public int? Precursor { get; set; }

        public DateTime? SendDate { get; set; }

        public bool? Printed { get; set; }

        #region VIRTUALS

        public virtual Projects Project { get; set; }

        public virtual Apartments Apartment { get; set; }

        public virtual ReportNag PrecursorReport { get; set; }

        public ICollection<ReportNag> Followers { get; set; }

        public ICollection<ReportElem> Elements { get; set; }

        #endregion
    }
}