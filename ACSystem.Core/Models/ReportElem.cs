﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class ReportElem
    {
        [Key]
        public int ID { get; set; }

        public int ReportNagID { get; set; }

        public int TaskID { get; set; }

        public decimal WorkDone { get; set; }

        public decimal WorkToDo { get; set; }

        public DateTime? LastReportDate { get; set; }

        public string FitterID { get; set; }

        public int PercentDone { get; set; }

        #region VIRTUALS

        public virtual ReportNag ReportNag { get; set; }

        public virtual Tasks Task { get; set; }

        public virtual ACSystemUser Fitter { get; set; }

        public virtual ICollection<Report_Questions> Questions { get; set; }

        #endregion
    }
}