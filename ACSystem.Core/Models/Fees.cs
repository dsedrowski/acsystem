﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Fees
    {
        public Fees()
        {
            this.Files = new HashSet<FeeFiles>();
        }

        [Key]
        public int ID { get; set; }

        [Required]
        [Index(IsUnique = true)]
        public int FeeNumber { get; set; }

        public string FitterID { get; set; }

        public int? ProjectID { get; set; }

        public int? ApartmentID { get; set; }

        public int? TaskID { get; set; }

        public int? PayrollID { get; set; }

        public DateTime FeeDate { get; set; }

        public DateTime? TakeDate { get; set; }

        public string Description { get; set; }

        public decimal FeeAmount { get; set; }

        public FeeStatus Status { get; set; }

        public string Image { get; set; }

        public virtual Fitters Fitter { get; set; }

        public virtual Projects Project { get; set; }

        public virtual Apartments Apartment { get; set; }

        public virtual Tasks Task { get; set; }

        public virtual Payroll Payroll { get; set; }

        public virtual ICollection<FeeFiles> Files { get; set; }
    }
}