﻿using ACSystem.Core.IRepo;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel.DataAnnotations.Schema;
using ACSystem.Core.Extensions;
using System.Text;
// ReSharper disable All

namespace ACSystem.Core.Models
{
    public class ACSystemContext : IdentityDbContext, IACSystemContext
    {
        public ACSystemContext()
            : base("DefaultConnection")
        {
            this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
            Database.SetInitializer<ACSystemContext>(null);
        }

        public static ACSystemContext Create()
        {
            return new ACSystemContext();
        }

        public ACSystemContext CreateContext()
        {
            return new ACSystemContext();
        }

        public DbSet<ACSystemUser> ACSystemUser { get; set; }
        public DbSet<Customers> Customers { get; set; }
        public DbSet<ProjectManagers> ProjectManagers { get; set; }
        public DbSet<Projects> Projects { get; set; }
        public DbSet<Tasks> Tasks { get; set; }
        public DbSet<Task_Fitter> Task_Fitter {get;set;}
        public DbSet<Task_Questions> Task_Questions { get; set; }
        public DbSet<TaskQuestionPhoto> TaskQuestionPhoto { get; set; }
        public DbSet<TaskActivityPhotos> TaskActivityPhotos { get; set; }
        public DbSet<TaskPatches> TaskPatches { get; set; }
        public DbSet<Apartments> Apartments { get; set; }
        public DbSet<Project_ProjectManager> Project_ProjectManager { get; set; }
        public DbSet<Inventory> Inventory { get; set; }
        public DbSet<Inventory_Accessories> Inventory_Accessories { get; set; }
        public DbSet<Inventory_Accessories_Photo> Inventory_Accessories_Photo { get; set; }
        public DbSet<Inventory_Accessories_Connect> Inventory_Accessories_Connect { get; set; }
        public DbSet<Inventory_Check> Inventory_Check { get; set; }
        public DbSet<Inventory_Check_Photo> Inventory_Check_Photo { get; set; }
        public DbSet<Inventory_Fitter> Inventory_Fitter { get; set; }
        public DbSet<Inventory_Photo> Inventory_Photo { get; set; }
        public DbSet<InventoryType> InventoryType { get; set; }
        public DbSet<InventoryWarehouse> InventoryWarehouse { get; set; }
        public DbSet<Fitters> Fitters { get; set; }
        public DbSet<Products> Products { get; set; }
        public DbSet<PercentHeight> PercentHeight { get; set; }
        public DbSet<Events> Events { get; set; }
        public DbSet<Payroll> Payroll { get; set; }
        public DbSet<PayrollItems> PayrollItems { get; set; }
        public DbSet<Fees> Fees{ get; set; }
        public DbSet<FeeFiles> FeeFiles { get; set; }
        public DbSet<Invoice> Invoice { get; set; }
        public DbSet<InvoiceElements> InvoiceElements { get; set; }
        public DbSet<BankAccounts> BankAccounts { get; set; }
        public DbSet<ExceptionsLog> ExceptionsLog { get; set; }
        public DbSet<Task_AccessGrant> Task_AccessGrant { get; set; }
        public DbSet<TaskComment> TaskComment { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderItem> OrderItem { get; set; }
        public DbSet<TaskDocuments> TaskDocuments { get; set; }
        public DbSet<Calendar> Calendar { get; set; }
        public DbSet<RentAndLoan> RentAndLoan { get; set; }
        public DbSet<Installment> Installment { get; set; }
        public DbSet<ProjectInvoicesPercent> ProjectInvoicesPercent { get; set; }
        public DbSet<ProjectInvoicesPercent_Elements> ProjectInvoicesPercent_Elements { get; set; }
        public DbSet<ProjectDocuments> ProjectDocuments { get; set; }
        public DbSet<Product_Project> Product_Project { get; set; }
        public DbSet<ReportNag> ReportNag { get; set; }
        public DbSet<ReportElem> ReportElem { get; set; }
        public DbSet<Rooms> Rooms { get; set; }
        public DbSet<Access_User_Project> Access_User_Project { get; set; }
        public DbSet<Access_User_Apartment> Access_User_Apartment { get; set; }
        public DbSet<Access_User_Task> Access_User_Task { get; set; }
        public DbSet<Todo> Todo { get; set; }
        public DbSet<Project_Activity> Project_Activity { get; set; }
        public DbSet<Product_Plan> Product_Plan { get; set; }
        public DbSet<Product_Group> Product_Group { get; set; }
        public DbSet<QuestionType> QuestionType { get; set; }
        public DbSet<Question> Question { get; set; }
        public DbSet<Product_Question> Product_Question { get; set; }
        public DbSet<Task_Question> Task_Question { get; set; }
        public DbSet<Answer> Answer { get; set; }
        public DbSet<Question_Product_Project> Question_Product_Project { get; set; }
        public DbSet<view_CheckTaskPriceNorm> view_CheckTaskPriceNorm { get; set; }
        public DbSet<view_CheckPayrollRateNorm> view_CheckPayrollRateNorm { get; set; }
        public DbSet<FavouriteProject> FavouriteProjects { get; set; }
        public DbSet<PlaceError> PlaceError { get; set; }
        public DbSet<PlaceError_Photo> PlaceError_Photo { get; set; }
        public DbSet<FavouriteCity> FavouriteCity { get; set; }
        public DbSet<Fitter_Crew> Fitter_Crew { get; set; }
        public DbSet<Invoice_Adds> Invoice_Adds { get; set; }
        public DbSet<Fitter_Hours> Fitter_Hours { get; set; }
        public DbSet<Fitter_Hours_Project> Fitter_Hours_Project { get; set; }
        public DbSet<UserChangeLog> UserChangeLog { get; set; }
        public DbSet<UserDocuments> UserDocuments { get; set; }
        public DbSet<Payroll_Errors> Payroll_Errors { get; set; }
        public DbSet<view_TaskForProductProject> view_TaskForProductProject { get; set; }
        public DbSet<view_FitterTasksWorkDone> view_FitterTasksWorkDone { get; set; }
        public DbSet<view_GanttData> view_GantData { get; set; }
        public DbSet<Settings> Settings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Payroll>().HasKey(x => x.Id);

            #region ACSystemUser

            modelBuilder.Entity<ACSystemUser>().HasOptional(x => x.Fitter).WithRequired(x => x.User).WillCascadeOnDelete(true);
            modelBuilder.Entity<ACSystemUser>().HasOptional(x => x.Calendar).WithMany(x => x.User).HasForeignKey(x => x.CalendarID);
            modelBuilder.Entity<FavouriteCity>().HasOptional(x => x.User).WithMany(x => x.FavouriteCity).HasForeignKey(x => x.UserId);


            #endregion

            #region Access_User

            modelBuilder.Entity<Access_User_Project>().HasRequired(x => x.Project).WithMany(x => x.Access_User_Project).HasForeignKey(x => x.ProjectID);
            modelBuilder.Entity<Access_User_Project>().HasRequired(x => x.User).WithMany(x => x.Access_User_Project).HasForeignKey(x => x.UserID);

            modelBuilder.Entity<Access_User_Apartment>().HasRequired(x => x.Apartment).WithMany(x => x.Access_User_Apartment).HasForeignKey(x => x.ApartmentID);
            modelBuilder.Entity<Access_User_Apartment>().HasRequired(x => x.User).WithMany(x => x.Access_User_Apartment).HasForeignKey(x => x.UserID);

            modelBuilder.Entity<Access_User_Task>().HasRequired(x => x.Task).WithMany(x => x.Access_User_Task).HasForeignKey(x => x.TaskID);
            modelBuilder.Entity<Access_User_Task>().HasRequired(x => x.User).WithMany(x => x.Access_User_Task).HasForeignKey(x => x.UserID);

            #endregion

            #region Answer

            modelBuilder.Entity<Answer>().HasRequired(x => x.Question).WithMany(x => x.Answers).HasForeignKey(x => x.QuestionID).WillCascadeOnDelete(true);

            #endregion

            #region Apartments

            modelBuilder.Entity<Apartments>().HasRequired(x => x.Project).WithMany(x => x.Apartments).HasForeignKey(x => x.ProjectId).WillCascadeOnDelete(true);

            #endregion

            #region Events

            //modelBuilder.Entity<Events>().HasOptional(x => x.Task).WithMany(x => x.Events).HasForeignKey(x => x.ReferenceTo).WillCascadeOnDelete(true);
            modelBuilder.Entity<Events>().HasOptional(x => x.Calendar).WithMany(x => x.Event).HasForeignKey(x => x.CalendarID);

            #endregion

            #region FavouriteProject

            modelBuilder.Entity<FavouriteProject>().HasRequired(q => q.Project).WithMany(q => q.FavouriteProjects).HasForeignKey(q => q.ProjectID);
            modelBuilder.Entity<FavouriteProject>().HasRequired(q => q.User).WithMany(q => q.FavouriteProjects).HasForeignKey(q => q.UserID);

            #endregion

            #region FavouriteCities

            //modelBuilder.Entity<FavouriteCity>().HasRequired(q => q.Fitter).WithMany(q => q.FavouriteCities).HasForeignKey(q => q.FitterId);
            modelBuilder.Entity<FavouriteCity>().HasKey(q => q.ID);



            #endregion

            #region Fees

            modelBuilder.Entity<Fees>().HasRequired(x => x.Fitter).WithMany(x => x.Fees).HasForeignKey(x => x.FitterID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Fees>().HasOptional(x => x.Project).WithMany(x => x.Fees).HasForeignKey(x => x.ProjectID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Fees>().HasOptional(x => x.Apartment).WithMany(x => x.Fees).HasForeignKey(x => x.ApartmentID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Fees>().HasOptional(x => x.Task).WithMany(x => x.Fees).HasForeignKey(x => x.TaskID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Fees>().HasRequired(x => x.Payroll).WithMany(x => x.Fees).HasForeignKey(x => x.PayrollID).WillCascadeOnDelete(true);

            #endregion

            #region FeesFiles

            modelBuilder.Entity<FeeFiles>().HasRequired(x => x.Fee).WithMany(x => x.Files).HasForeignKey(x => x.FeeID).WillCascadeOnDelete(true);

            #endregion

            #region Fitters

            modelBuilder.Entity<Fitters>().HasOptional(x => x.Task).WithMany(x => x.Fitters).HasForeignKey(x => x.TaskAlreadyWork).WillCascadeOnDelete(false);
            modelBuilder.Entity<Fitter_Crew>().HasRequired(x => x.Supervisor).WithMany(x => x.Crew_Supervisor).HasForeignKey(x => x.SupervisorID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Fitter_Crew>().HasRequired(x => x.Member).WithMany(x => x.Crew_Members).HasForeignKey(x => x.MemberID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Fitter_Hours>().HasRequired(x => x.Fitter).WithMany(x => x.Fitter_Hours).HasForeignKey(x => x.FitterID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Fitter_Hours_Project>().HasOptional(x => x.Payroll).WithMany(x => x.Fitter_Hours_Project).HasForeignKey(x => x.PayrollID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Fitter_Hours_Project>().HasRequired(x => x.Fitter_Hours).WithMany(x => x.Fitter_Hours_Project).HasForeignKey(x => x.Fitter_HoursID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Fitter_Hours_Project>().HasRequired(x => x.Project).WithMany(x => x.Fitter_Hours_Project).HasForeignKey(x => x.ProjectID).WillCascadeOnDelete(true);

            #endregion

            #region Invoice

            modelBuilder.Entity<Invoice>().HasRequired(x => x.BankAccount).WithMany(x => x.InvoiceList).HasForeignKey(x => x.BankAccountID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Invoice>().HasRequired(x => x.Customer).WithMany(x => x.InvoiceList).HasForeignKey(x => x.CustomerID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Invoice>().HasRequired(x => x.Project).WithMany(x => x.InvoiceList).HasForeignKey(x => x.ProjectID).WillCascadeOnDelete(false);

            #endregion

            #region Invoice_Adds

            modelBuilder.Entity<Invoice_Adds>().HasRequired(x => x.Task).WithMany(x => x.Invoice_Adds).HasForeignKey(x => x.TaskID);
            modelBuilder.Entity<Invoice_Adds>().HasOptional(x => x.Invoice).WithMany(x => x.Invoice_Adds).HasForeignKey(x => x.InvoiceID);

            #endregion

            #region InvoiceElements

            modelBuilder.Entity<InvoiceElements>().HasRequired(x => x.Invoice).WithMany(x => x.Elements).HasForeignKey(x => x.InvoiceID).WillCascadeOnDelete(true);
            modelBuilder.Entity<InvoiceElements>().HasOptional(x => x.Task).WithMany(x => x.InvoiceElements).HasForeignKey(x => x.TaskID).WillCascadeOnDelete(true);
            modelBuilder.Entity<InvoiceElements>().HasRequired(x => x.Project).WithMany(x => x.InvoiceElements).HasForeignKey(x => x.ProjectID).WillCascadeOnDelete(true);

            #endregion

            #region Inventory

            modelBuilder.Entity<Inventory>().HasOptional(x => x.Project).WithMany(x => x.InventoryList).HasForeignKey(x => x.ProjectID);
            modelBuilder.Entity<Inventory>().HasOptional(x => x.Warehouse).WithMany(x => x.Inventory).HasForeignKey(x => x.WarehouseID);
            modelBuilder.Entity<Inventory>().HasRequired(x => x.Type).WithMany(x => x.InventoryList).HasForeignKey(x => x.TypeID);
            modelBuilder.Entity<Inventory>().HasOptional(x => x.Owner).WithMany(x => x.InventoryList).HasForeignKey(x => x.OwnerID);

            #region Inventory_Fitter

            modelBuilder.Entity<Inventory_Fitter>().HasRequired(x => x.User).WithMany(x => x.Inventory_Fitter).HasForeignKey(x => x.UserID);
            modelBuilder.Entity<Inventory_Fitter>().HasRequired(x => x.Inventory).WithMany(x => x.Inventory_Fitter).HasForeignKey(x => x.InventoryID);
            modelBuilder.Entity<Inventory_Fitter>().HasOptional(x => x.Project).WithMany(x => x.Inventory_FitterList).HasForeignKey(x => x.ProjectID);
            modelBuilder.Entity<Inventory_Fitter>().HasOptional(x => x.Warehouse).WithMany(x => x.Inventory_Fitter).HasForeignKey(x => x.WarehouseID);

            #endregion

            #region Inventory_Photo

            modelBuilder.Entity<Inventory_Photo>().HasRequired(x => x.Inventory).WithMany(x => x.Inventory_Photo).HasForeignKey(x => x.InventoryID);

            #endregion

            #region Inventory_Accessories_Photo

            modelBuilder.Entity<Inventory_Accessories_Photo>().HasRequired(x => x.Inventory_Accessories).WithMany(x => x.Photos).HasForeignKey(x => x.Inventory_AccessoriesID);

            #endregion

            #region Inventory_Accessories_Connect

            modelBuilder.Entity<Inventory_Accessories_Connect>().HasRequired(x => x.Inventory).WithMany(x => x.Inventory_Accessories_Connect).HasForeignKey(x => x.InventoryID);
            modelBuilder.Entity<Inventory_Accessories_Connect>().HasRequired(x => x.Accessory).WithMany(x => x.Inventory_Accessories_Connect).HasForeignKey(x => x.AccessoryID);

            #endregion

            #region Inventory_Check

            modelBuilder.Entity<Inventory_Check>().HasRequired(x => x.User).WithMany(x => x.Inventory_Check).HasForeignKey(x => x.UserID);
            modelBuilder.Entity<Inventory_Check>().HasRequired(x => x.Inventory).WithMany(x => x.Inventory_Check).HasForeignKey(x => x.InventoryID);

            #endregion

            #region Inventory_Check_Photo

            modelBuilder.Entity<Inventory_Check_Photo>().HasRequired(x => x.Inventory_Check).WithMany(x => x.Photos).HasForeignKey(x => x.Inventory_CheckID);

            #endregion

            #endregion

            #region Installment

            modelBuilder.Entity<Installment>().HasRequired(x => x.RentAndLoan).WithMany(x => x.Installments).HasForeignKey(x => x.RentAndLoanID);
            modelBuilder.Entity<Installment>().HasOptional(x => x.Payroll).WithMany(x => x.Installments).HasForeignKey(x => x.PayrollID);

            #endregion

            #region Order

            modelBuilder.Entity<Order>().HasRequired(x => x.Project).WithMany(x => x.Orders).HasForeignKey(x => x.ProjectID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Order>().HasOptional(x => x.Apartment).WithMany(x => x.Orders).HasForeignKey(x => x.ApartmentID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Order>().HasOptional(x => x.Task).WithMany(x => x.Orders).HasForeignKey(x => x.TaskID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Order>().HasRequired(x => x.Fitter).WithMany(x => x.Orders).HasForeignKey(x => x.FitterID).WillCascadeOnDelete(true);

            #endregion

            #region OrderItem

            modelBuilder.Entity<OrderItem>().HasRequired(x => x.Order).WithMany(x => x.OrderItems).HasForeignKey(x => x.OrderID).WillCascadeOnDelete(true);

            #endregion

            #region PayrollItems

            modelBuilder.Entity<PayrollItems>().HasRequired(x => x.Payroll).WithMany(x => x.Items).HasForeignKey(x => x.PayrollID).WillCascadeOnDelete(true);
            modelBuilder.Entity<PayrollItems>().HasRequired(x => x.Project).WithMany(x => x.PayrollItems).HasForeignKey(x => x.ProjectID).WillCascadeOnDelete(true);
            modelBuilder.Entity<PayrollItems>().HasOptional(x => x.Task).WithMany(x => x.PayrollItems).HasForeignKey(x => x.TaskID).WillCascadeOnDelete(true);
            modelBuilder.Entity<PayrollItems>().HasOptional(x => x.Apartment).WithMany(x => x.PayrollItems).HasForeignKey(x => x.ApartmentID).WillCascadeOnDelete(true);

            #endregion

            #region Products

            modelBuilder.Entity<Products>().HasOptional(x => x.PercentHeight).WithRequired(x => x.Product).WillCascadeOnDelete(true);

            #endregion

            #region Product_Group

            modelBuilder.Entity<Product_Group>().HasRequired(x => x.ProductMain).WithMany(x => x.ProductGroups_Main)
                .HasForeignKey(x => x.ProductMainID);

            modelBuilder.Entity<Product_Group>().HasRequired(x => x.ProductSub).WithMany(x => x.ProductGroups_Sub)
                .HasForeignKey(x => x.ProductSubID);

            #endregion

            #region Product_Project

            modelBuilder.Entity<Product_Project>().HasRequired(x => x.Product).WithMany(x => x.Product_Project).HasForeignKey(x => x.ProductID);
            modelBuilder.Entity<Product_Project>().HasRequired(x => x.Project).WithMany(x => x.Product_Project).HasForeignKey(x => x.ProjectID);

            #endregion

            #region Product_Plan

            modelBuilder.Entity<Product_Plan>().HasRequired(x => x.Product).WithMany(x => x.Product_Plan).HasForeignKey(x => x.ProductID);
            modelBuilder.Entity<Product_Plan>().HasRequired(x => x.Project).WithMany(x => x.Product_Plan).HasForeignKey(x => x.ProjectID);

            #endregion

            #region Projects

            modelBuilder.Entity<Projects>().HasRequired(x => x.Customer).WithMany(x => x.Projects).HasForeignKey(x => x.CustomerId).WillCascadeOnDelete(true);
            modelBuilder.Entity<Projects>().HasOptional(x => x.Calendar).WithMany(x => x.Project).HasForeignKey(x => x.CalendarID);
            modelBuilder.Entity<Product_Question>().HasRequired(x => x.Product).WithMany(x => x.Product_Questions).HasForeignKey(x => x.ProductID);
            modelBuilder.Entity<Product_Question>().HasRequired(x => x.Question).WithMany(x => x.Product_Questions).HasForeignKey(x => x.QuestionID);
            modelBuilder.Entity<Projects>().HasOptional(x => x.Supervisor).WithMany(x => x.ProjectsSupervisor).HasForeignKey(x => x.SupervisorID);

            #endregion

            #region Project_Activity

            modelBuilder.Entity<Project_Activity>().HasRequired(x => x.Project).WithMany(x => x.Activities).HasForeignKey(x => x.ProjectID);
            modelBuilder.Entity<Project_Activity>().HasRequired(x => x.User).WithMany(x => x.Activities).HasForeignKey(x => x.UserID);

            #endregion

            #region ProjectManagers

            modelBuilder.Entity<ProjectManagers>().HasRequired(x => x.Customer).WithMany(x => x.ProjectManagers).HasForeignKey(x => x.CustomerId).WillCascadeOnDelete(true);

            #endregion

            #region ProjectInvociesPercent_Elements

            modelBuilder.Entity<ProjectInvoicesPercent_Elements>().HasRequired(x => x.ProjectInvoicesPercent).WithMany(x => x.Element).HasForeignKey(x => x.ProjectInvoicesPercentID);

            #endregion

            #region ProjectDocuments

            modelBuilder.Entity<ProjectDocuments>().HasRequired(x => x.Project).WithMany(x => x.Documents).HasForeignKey(x => x.ProjectID);

            #endregion

            #region RentAndLoan

            modelBuilder.Entity<RentAndLoan>().HasRequired(x => x.Fitter).WithMany(x => x.RentAndLoan).HasForeignKey(x => x.FitterID);

            #endregion

            #region ReportNag

            modelBuilder.Entity<ReportNag>().HasRequired(x => x.Project).WithMany(x => x.ReportNags).HasForeignKey(x => x.ProjectID);
            modelBuilder.Entity<ReportNag>().HasRequired(x => x.Apartment).WithMany(x => x.ReportNags).HasForeignKey(x => x.ApartmentID);
            modelBuilder.Entity<ReportNag>().HasOptional(x => x.PrecursorReport).WithMany(x => x.Followers).HasForeignKey(x => x.Precursor);

            #endregion

            #region ReportElem

            modelBuilder.Entity<ReportElem>().HasRequired(x => x.ReportNag).WithMany(x => x.Elements).HasForeignKey(x => x.ReportNagID);
            modelBuilder.Entity<ReportElem>().HasRequired(x => x.Task).WithMany(x => x.ReportElems).HasForeignKey(x => x.TaskID);
            modelBuilder.Entity<ReportElem>().HasOptional(x => x.Fitter).WithMany(x => x.ReportElems).HasForeignKey(x => x.FitterID);

            #endregion

            #region Report_Questions

            modelBuilder.Entity<Report_Questions>().HasRequired(x => x.ReportElem).WithMany(x => x.Questions).HasForeignKey(x => x.ReportElemID);

            #endregion

            #region Tasks

            modelBuilder.Entity<Tasks>().HasRequired(X => X.Apartment).WithMany(X => X.Tasks).HasForeignKey(X => X.ApartmentId).WillCascadeOnDelete(true);
            modelBuilder.Entity<Tasks>().HasRequired(x => x.Product).WithMany(x => x.Tasks).HasForeignKey(x => x.ProductId).WillCascadeOnDelete(true);
            modelBuilder.Entity<Tasks>().HasOptional(x => x.Room).WithMany(x => x.Tasks).HasForeignKey(x => x.RoomID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Tasks>().HasOptional(x => x.TaskGrouped).WithMany(x => x.TaskGroup).HasForeignKey(x => x.GroupTaskID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Task_Question>().HasRequired(x => x.Task).WithMany(x => x.Task_Question).HasForeignKey(x => x.TaskID);
            modelBuilder.Entity<Task_Question>().HasRequired(x => x.Question).WithMany(x => x.Task_Question).HasForeignKey(x => x.QuestionID);

            #endregion

            #region Task_Fitter

            modelBuilder.Entity<Task_Fitter>().HasRequired(x => x.Task).WithMany(x => x.Task_Fitter).HasForeignKey(x => x.TaskId).WillCascadeOnDelete(true);
            modelBuilder.Entity<Task_Fitter>().HasRequired(x => x.Fitter).WithMany(x => x.Task_Fitter).HasForeignKey(x => x.FitterId).WillCascadeOnDelete(true);

            #endregion

            #region TaskActivityPhotos

            modelBuilder.Entity<TaskActivityPhotos>().HasRequired(x => x.Task_Fitter).WithMany(x => x.Photos).HasForeignKey(x => x.Task_FitterId).WillCascadeOnDelete(true);

            #endregion

            #region TaskQuestionPhoto

            modelBuilder.Entity<TaskQuestionPhoto>().HasRequired(x => x.Task_Question).WithMany(x => x.TaskQuestionPhoto).HasForeignKey(x => x.Task_QuestionsID).WillCascadeOnDelete(true);

            #endregion

            #region Task_Question

            modelBuilder.Entity<Task_Question>().HasRequired(q => q.Question).WithMany(q => q.Task_Question).HasForeignKey(q => q.QuestionID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Task_Question>().HasRequired(x => x.Task).WithMany(q => q.Task_Question).HasForeignKey(q => q.TaskID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Task_Question>().HasOptional(x => x.Answer).WithMany(q => q.Task_Questions).HasForeignKey(q => q.AnswerID).WillCascadeOnDelete(false);

            #endregion

            #region TaskPatches

            modelBuilder.Entity<TaskPatches>().HasRequired(x => x.Task).WithMany(x => x.TaskPatches).HasForeignKey(x => x.TaskID).WillCascadeOnDelete(true);

            #endregion

            #region Task_AccessGrant

            modelBuilder.Entity<Task_AccessGrant>().HasRequired(x => x.Task).WithMany(x => x.Task_AccessGrant).HasForeignKey(x => x.TaskID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Task_AccessGrant>().HasRequired(x => x.User).WithMany(x => x.Task_AccessGrant).HasForeignKey(x => x.FitterID).WillCascadeOnDelete(true);

            #endregion

            #region TaskComment

            modelBuilder.Entity<TaskComment>().HasRequired(x => x.Fitter).WithMany(x => x.TaskComments).HasForeignKey(x => x.FitterID).WillCascadeOnDelete(true);
            modelBuilder.Entity<TaskComment>().HasRequired(x => x.Apartment).WithMany(x => x.TaskComments).HasForeignKey(x => x.ApartmentID).WillCascadeOnDelete(true);
            modelBuilder.Entity<TaskComment>().HasRequired(x => x.Task).WithMany(x => x.TaskComments).HasForeignKey(x => x.TaskID).WillCascadeOnDelete(true);

            #endregion

            #region TaskDocuments

            modelBuilder.Entity<TaskDocuments>().HasRequired(x => x.Task).WithMany(x => x.TaskDocuments).HasForeignKey(x => x.TaskID).WillCascadeOnDelete(true);

            #endregion

            #region Todo

            modelBuilder.Entity<Todo>().HasOptional(x => x.Task).WithMany(x => x.Todos).HasForeignKey(x => x.TaskID);
            modelBuilder.Entity<Todo>().HasRequired(x => x.User).WithMany(x => x.Todos).HasForeignKey(x => x.FitterID);
            modelBuilder.Entity<Todo>().HasOptional(x => x.PlaceError).WithMany(x => x.Todos).HasForeignKey(x => x.PlaceErrorID);
            //  modelBuilder.Entity<Todo>().HasRequired(x => x.Fitter).WithMany().HasForeignKey(x => x.FitterID);
            //   modelBuilder.Entity<Todo>().HasOptional(x => x.Fitter).WithOptionalPrincipal(x => x.Todo);

            #endregion

            #region UserChangeLog

            modelBuilder.Entity<UserChangeLog>().HasRequired(x => x.User).WithMany(x => x.UserChangeLog).HasForeignKey(x => x.UserId);
            modelBuilder.Entity<UserChangeLog>().HasRequired(x => x.ChangeUser).WithMany(x => x.UserChangeLog_Changer).HasForeignKey(x => x.ChangeUserId);

            #endregion


            #region Question

            modelBuilder.Entity<Question>().HasRequired(x => x.Type).WithMany(x => x.Questions).HasForeignKey(x => x.QuestionTypeID);

            #endregion

            #region Question_Product_Project

            modelBuilder.Entity<Question_Product_Project>().HasRequired(x => x.Question).WithMany(x => x.Question_Product_Projects).HasForeignKey(x => x.QuestionID).WillCascadeOnDelete(true);
            modelBuilder.Entity<Question_Product_Project>().HasRequired(x => x.Product_Project).WithMany(x => x.Question_Product_Projects).HasForeignKey(x => x.Project_ProductID).WillCascadeOnDelete(true);

            #endregion
        }

        protected override DbEntityValidationResult ValidateEntity(DbEntityEntry entityEntry, IDictionary<object, object> items)
        {
            var result = new DbEntityValidationResult(entityEntry, new List<DbValidationError>());

            if (entityEntry.State == EntityState.Added)
                CheckUnique(entityEntry, result);

            if (!result.IsValid)
                return result;

            return base.ValidateEntity(entityEntry, items);
        }

        private void CheckUnique(DbEntityEntry entityEntry, DbEntityValidationResult result)
        {
            var entity = entityEntry.Entity;

            var propertiesDictionary = (from p in entity.GetType().GetProperties()
                                        let attrs = p.GetCustomAttributes(typeof(IndexAttribute),
                                        false).Cast<IndexAttribute>()
                                        where attrs.Any(a => a.IsUnique)
                                        select new
                                        {
                                            Property = p,
                                            Attributes = attrs.Where(a => a.IsUnique)
                                        }).ToList();

            var indexNames = propertiesDictionary.SelectMany(x => x.Attributes).Select(x => x.Name).Distinct();

            foreach (var indexName in indexNames)
            {
                Dictionary<string, PropertyInfo> involvedProperties = propertiesDictionary.Where(p => p.Attributes.Any(a => a.Name == indexName)).ToDictionary(p => p.Property.Name, p => p.Property);

                DbSet set = Set(entity.GetType());

                var whereClause = "";
                var whereParams = new List<object>();
                var i = 0;
                foreach (var involvedProperty in involvedProperties)
                {
                    if (whereClause.Length > 0)
                        whereClause += " AND ";

                    if (Nullable.GetUnderlyingType(involvedProperty.Value.PropertyType) != null)
                        whereClause += $"it.{involvedProperty.Key}.Value.Equals(@{i})";
                    else
                        whereClause += $"it.{involvedProperty.Key}.Equals(@{i})";

                    whereParams.Add(involvedProperty.Value.GetValue(entity));
                    i++;
                }

                if (entityEntry.State == EntityState.Modified)
                {
                    whereClause += " AND (";

                    var key = this.GetEntityKey(entity);
                    if (key != null)
                    {
                        for (var j = i; j < key.EntityKeyValues.Count() + 1; j++)
                        {
                            if (j != i)
                                whereClause += " OR ";
                            whereClause += $"it.{key.EntityKeyValues[j - i].Key} <> @{j}";

                            whereParams.Add(key.EntityKeyValues[j - i].Value);
                        }
                    }
                    whereClause += " )";
                }

                if (set.Where(whereClause, whereParams.ToArray()).Any())
                {
                    var errorMessageBuilder = new StringBuilder()
                        .Append(involvedProperties.Count > 1 ? "Podane " : "Podana ")
                        .Append(involvedProperties.Count > 1 ? "wartości " : "wartość ")
                        .Append("'")
                        .Append(string.Join(", ", involvedProperties.Keys))
                        .Append("' ")
                        .Append(involvedProperties.Count > 1 ? "już istnieją! " : "już istnieje! ");

                    result.ValidationErrors.Add(new DbValidationError(indexName, errorMessageBuilder.ToString()));
                }
            }
        }
    }
}