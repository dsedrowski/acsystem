﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Fitter_Crew
    {
        [Key]
        public int ID { get; set; }

        public string SupervisorID { get; set; }

        public string MemberID { get; set; }

        public virtual ACSystemUser Supervisor { get; set; }
        public virtual ACSystemUser Member { get; set; }
    }
}