﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class PercentHeight
    {
        [Key, ForeignKey("Product")]
        public int Id { get; set; }

        public decimal? Type1 { get; set; }

        public decimal? Type2 { get; set; }

        public decimal? Type3 { get; set; }

        public decimal? Type4 { get; set; }

        public decimal? Type5 { get; set; }

        public decimal? Type6 { get; set; }

        public virtual Products Product { get; set; }
    }
}