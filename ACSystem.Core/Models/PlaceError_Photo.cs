﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class PlaceError_Photo
    {
        [Key]
        public int ID { get; set; }

        public int PlaceErrorID { get; set; }

        public string Photo { get; set; }

        [ForeignKey("PlaceErrorID")]
        public virtual PlaceError PlaceError { get; set; }
    }
}