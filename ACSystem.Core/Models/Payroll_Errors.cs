﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace ACSystem.Core.Models
{
    [Table("acbyggco_acsystem.Payroll_Errors")]
    public class Payroll_Errors
    {
        [Key]
        public int ID { get; set; }

        public int PayrollID { get; set; }

        public string Error { get; set; }

        [ForeignKey("PayrollID")]
        public virtual Payroll Payroll { get; set; }
    }
}