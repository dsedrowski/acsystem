﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class ExceptionsLog
    {
        [Key]
        public long LogID { get; set; }

        public string Message { get; set; }

        public string Type { get; set; }

        public string Source { get; set; }

        public string StackTrace { get; set; }

        public string URL { get; set; }

        public DateTime LogDate { get; set; }

        public string User { get; set; }
    }
}