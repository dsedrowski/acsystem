﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Product_Plan
    {
        [Key]
        public int PlanID { get; set; }

        public int ProjectID { get; set; }

        public int ProductID { get; set; }

        public decimal Count { get; set; }

        public virtual Projects Project { get; set; }

        public virtual Products Product { get; set; }
    }
}