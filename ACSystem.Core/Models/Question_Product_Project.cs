﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Question_Product_Project
    {
        [Key]
        public int ID { get; set; }

        public int QuestionID { get; set; }

        public int Project_ProductID { get; set; }

        public virtual Question Question { get; set; }

        public virtual Product_Project Product_Project { get; set; }

    }
}