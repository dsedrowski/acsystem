﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Rooms
    {
        public Rooms()
        {
            this.Tasks = new HashSet<Tasks>();
        }

        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public string Translate { get; set; }

        public virtual ICollection<Tasks> Tasks { get; set; }
    }
}