﻿using ACSystem.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Task_Questions
    {
        public Task_Questions()
        {
            this.TaskQuestionPhoto = new HashSet<TaskQuestionPhoto>();
        }

        [Key, ForeignKey("Task")]
        public int QuestionId { get; set; }
        [Display(Name = "Szafki dolne")]
        public TaskQuestionStatus BaseUnits { get; set; }
        [Display(Name = "Szafki ścienne")]
        public TaskQuestionStatus WallUnits { get; set; }
        [Display(Name = "Szafy wysokie")]
        public TaskQuestionStatus TallUnits { get; set; }
        [Display(Name = "Passbit")]
        public TaskQuestionStatus Fillers { get; set; }
        [Display(Name = "Takanslutning")]
        public TaskQuestionStatus CeillingScribe { get; set; }
        [Display(Name = "Tacksida")]
        public TaskQuestionStatus SidePiece { get; set; }
        [Display(Name = "Blat")]
        public TaskQuestionStatus Worktop { get; set; }
        [Display(Name = "Zlew")]
        public TaskQuestionStatus Sink { get; set; }
        [Display(Name = "Raczki")]
        public TaskQuestionStatus Handles { get; set; }
        [Display(Name = "Cokoły")]
        public TaskQuestionStatus Plinth { get; set; }
        [Display(Name = "Kratka wentylacyjna")]
        public TaskQuestionStatus Grids { get; set; }
        [Display(Name = "Oświetlenie")]
        public TaskQuestionStatus Lighting { get; set; }
        [Display(Name = "Fugi silikonowe")]
        public TaskQuestionStatus Silicon { get; set; }
        [Display(Name = "Instrukcje montazu i uzytkowania")]
        public TaskQuestionStatus Manuals { get; set; }
        [Display(Name = "Urządzenia")]
        public TaskQuestionStatus Machines { get; set; }
        [Display(Name = "Piekarnik")]
        public TaskQuestionStatus Oven { get; set; }
        [Display(Name = "Zamrażarka 1")]
        public TaskQuestionStatus FridgeFreezer1 { get; set; }
        [Display(Name = "Zamrażarka 2")]
        public TaskQuestionStatus FridgeFreezer2 { get; set; }
        [Display(Name = "Plyta grzejna")]
        public TaskQuestionStatus Hob { get; set; }
        [Display(Name = "Zmywarka")]
        public TaskQuestionStatus Dishwasher { get; set; }
        [Display(Name = "Mikrofalówka")]
        public TaskQuestionStatus Mikrowave { get; set; }
        [Display(Name = "Okap kuchenny")]
        public TaskQuestionStatus Fan { get; set; }
        [Display(Name = "Wycięcie na rury")]
        public TaskQuestionStatus CutIntoPipes { get; set; }
        [Display(Name = "Wycięcie na wentylację")]
        public TaskQuestionStatus CutOutForVentilation { get; set; }
        [Display(Name = "Śmietnik")]
        public TaskQuestionStatus Trash { get; set; }
        [Display(Name = "Barnspärr")]
        public TaskQuestionStatus ChildSafety { get; set; }


        public virtual Tasks Task { get; set; }

        public virtual ICollection<TaskQuestionPhoto> TaskQuestionPhoto { get; set; }

        [NotMapped]
        public List<string> QuestionsList = new List<string>
        {
            "BaseUnits",
            "WallUnits",
            "TallUnits",
            "Fillers",
            "CeillingScribe",
            "SidePiece",
            "Worktop",
            "Sink",
            "Handles",
            "Plinth",
            "Grids",
            "Lighting",
            "Silicon",
            "Manuals",
            "CutIntoPipes",
            "CutOutForVentilation",
            "Trash",
            "ChildSafety",
            "Machines"
        };

        [NotMapped] public List<string> MachinesList = new List<string>
        {
            "Oven",
            "FridgeFreezer1",
            "FridgeFreezer2",
            "Hob",
            "Dishwasher",
            "Mikrowave",
            "Fan"
        };

        [NotMapped] public List<string> QuestionsNames = new List<string>
        {
            "Szafki dolne",
            "Szafki ścienne",
            "Szafy wysokie",
            "Passbit",
            "Takanslutning",
            "Tacksida",
            "Blat",
            "Zlew",
            "Raczki",
            "Cokoły",
            "Kratka wentylacyjna",
            "Oświetlenie",
            "Fugi silikonowe",
            "Instrukcje montazu i uzytkowania",
            "Piekarnik",
            "Zamrażarka 1",
            "Zamrażarka 2",
            "Plyta grzejna",
            "Zmywarka",
            "Mikrofalówka",
            "Okap kuchenny",
            "Wycięcie na rury",
            "Wycięcie na wentylację",
            "Śmietnik",
            "Barnspärr"
        };
        
        public static bool IsAllOk(Task_Questions questions)
        {
            foreach(var q in questions.QuestionsList)
            {
                var prop = (TaskQuestionStatus)questions.GetPropValue(q);

                if (prop == TaskQuestionStatus.Nie)
                    return false;
            }

            return true;
        }
    }
}