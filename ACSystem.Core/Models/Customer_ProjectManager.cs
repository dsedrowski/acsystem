﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Customer_ProjectManager
    {
        public Customer_ProjectManager()
        {

        }

        public int Id { get; set; }

        public int CustomerId { get; set; }

        public int ProjectManagerId { get; set; }

        public virtual Customers Customer { get; set; }

        public virtual ProjectManagers ProjectManager { get; set; }
    }
}