﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class InvoiceElements
    {
        [Key]
        public int ID { get; set; }

        public int InvoiceID { get; set; }

        public int ProjectID { get; set; }

        public string ApartmentNumber { get; set; }

        public int?  TaskID { get; set; }

        public string ProductName { get; set; }

        public decimal Quantity { get; set; }

        public string Unit { get; set; }

        public decimal UnitPrice { get; set; }

        public bool Complete { get; set; }

        public decimal? Total { get; set; }

        public string Description { get; set; }

        public int? Invoice_AddsID { get; set; }

        public virtual Invoice Invoice { get; set; }

        public virtual Tasks Task { get; set; }

        public virtual Projects Project { get; set; }

        public decimal Sum => Quantity * UnitPrice;
    }
}