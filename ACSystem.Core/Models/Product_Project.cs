﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Product_Project
    {
        public Product_Project()
        {
            this.Question_Product_Projects = new HashSet<Question_Product_Project>();
        }

        [Key]
        public int ID { get; set; }

        public int ProjectID { get; set; }

        public string ProjectNumber { get; set; }

        public int ProductID { get; set; }

        public string ProductName { get; set; }

        public decimal Price { get; set; }

        public decimal? PercentType1 { get; set; }

        public decimal? PercentType2 { get; set; }

        public string OnInvoiceName { get; set; }

        public bool NoExtra { get; set; }

        public virtual Projects Project { get; set; }

        public virtual Products Product { get; set; }

        public virtual ICollection<Question_Product_Project> Question_Product_Projects { get; set; }
    }
}