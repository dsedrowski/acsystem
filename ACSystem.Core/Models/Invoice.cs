﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Invoice
    {
        public Invoice()
        {
            this.Elements = new HashSet<InvoiceElements>();
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int InvoiceNumber { get; set; }

        public string InvoiceNo { get { return InvoiceNumber.ToString().PadLeft(6, '0'); } }

        public int CustomerID { get; set; }

        public int ProjectID { get; set; }

        public int BankAccountID { get; set; }

        public decimal Amount { get; set; }

        public decimal PenaltyInterest { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime InvoiceDate { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime DeliveryDate { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PaymentTerm { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime InvoiceDateFrom { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime InvoiceDateTo { get; set; }

        public InvoiceStatus Status { get; set; }

        public string Comments { get; set; }

        public DateTime CreateDate { get; set; }

        public bool Complete { get; set; }

        public bool? IsPercentGenerated { get; set; }

        public string FortnoxUrl { get; set; }

        public virtual Customers Customer { get; set; }

        public virtual Projects Project { get; set; }

        public virtual BankAccounts BankAccount { get; set; }

        public virtual ICollection<InvoiceElements> Elements { get; set; }

        public virtual ICollection<Invoice_Adds> Invoice_Adds { get; set; }
    }
}