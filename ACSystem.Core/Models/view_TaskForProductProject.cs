﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models {
    public class view_TaskForProductProject {
        [Key]
        public int ProjectID { get; set; }
        public string InSystemID { get; set; }
        public string ProjectNumber { get; set; }
        public int ApartmentID { get; set; }
        public string ApartmentNumber { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public DateTime? ClosedDate { get; set; }
        public string Fitters { get; set; }
        public string Payrolls { get; set; }
        public string Invoices { get; set; }
        public decimal WorkDone { get; set; }
        public decimal ProductCount { get; set; }
        public TaskStatus Status { get; set; }
    }
}