﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Product_Question
    {
        [Key]
        public int ID { get; set; }

        public int QuestionID { get; set; }

        public int ProductID { get; set; }

        public virtual Question Question { get; set; }

        public virtual Products Product { get; set; }
    }
}