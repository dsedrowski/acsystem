﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Project_Activity
    {
        [Key]
        public int ID { get; set; }

        public int ProjectID { get; set; }

        public string UserID { get; set; }

        public string Message { get; set; }

        public DateTime Timestamp { get; set; }

        public Projects Project { get; set; }

        public ACSystemUser User { get; set; }
    }
}