﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class ProjectManagers
    {
        public ProjectManagers()
        {
            //this.Customer_ProjectManager = new HashSet<Customer_ProjectManager>();
            this.Project_ProjectManager = new HashSet<Project_ProjectManager>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Imię i nazwisko")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Numer telefonu")]
        public string PhoneNumber { get; set; }

        public bool IsLocked { get; set; }

        public int CustomerId { get; set; }

        public virtual Customers Customer { get; set; }

        //public virtual ICollection<Customer_ProjectManager> Customer_ProjectManager { get; set; }

        public virtual ICollection<Project_ProjectManager> Project_ProjectManager { get; set; }
    }
}