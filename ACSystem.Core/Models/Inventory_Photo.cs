﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Inventory_Photo
    {
        [Key]
        public int ID { get; set; }

        public int InventoryID { get; set; }

        public string Name { get; set; }

        public string URL { get; set; }

        public virtual Inventory Inventory { get; set; }
    }
}