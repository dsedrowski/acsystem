﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class QuestionType
    {
        public QuestionType()
        {
            this.Questions = new HashSet<Question>();
        }

        [Key]
        public int ID { get; set; }

        public string Type { get; set; }

        public virtual ICollection<Question> Questions { get; set; }
    }
}