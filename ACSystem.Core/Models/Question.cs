﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Question
    {
        public Question()
        {
            this.Product_Questions = new HashSet<Product_Question>();
            this.Task_Question = new HashSet<Task_Question>();
            this.Question_Product_Projects = new HashSet<Question_Product_Project>();
        }

        [Key]
        public int ID { get; set; }

        public int QuestionTypeID { get; set; }

        public string QuestionPol { get; set; }

        public string QuestionTranslate { get; set; }

        public virtual QuestionType Type { get; set; }

        public virtual ICollection<Product_Question> Product_Questions { get; set; }

        public virtual ICollection<Task_Question> Task_Question { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }

        public virtual ICollection<Question_Product_Project> Question_Product_Projects { get; set; }
    }
}