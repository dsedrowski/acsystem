﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class TaskQuestionPhoto
    {
        [Key]
        public int Id { get; set; }
        public int Task_QuestionsID { get; set; }
        public string Question { get; set; }
        public string Photo { get; set; }
        public string Description { get; set; }
        public string TranslatedDescription { get; set; }
        public bool Blocked { get; set; }
        public bool InService { get; set; }

        public virtual Task_Questions Task_Question { get; set; } 
    }
}