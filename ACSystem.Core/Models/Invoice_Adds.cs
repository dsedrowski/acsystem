﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Invoice_Adds
    {
        [Key]
        public int ID { get; set; }

        public int TaskID { get; set; }

        public decimal Amount { get; set; }

        public int? InvoiceID { get; set; }

        public virtual Tasks Task { get; set; }

        public virtual Invoice Invoice { get; set; }
    }
}