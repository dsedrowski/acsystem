﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class PlaceError
    {
        public PlaceError()
        {
            this.PlaceError_Photos = new HashSet<PlaceError_Photo>();
        }

        [Key]
        public int ID { get; set; }

        public int? ProjectID { get; set; }

        public int? ApartmentID { get; set; }

        public int? TaskID { get; set; }

        public string FitterID { get; set; }

        public DateTime? Date { get; set; }

        public string Description { get; set; }

        public string Translated { get; set; }

        public DateTime? SendDate { get; set; }

        [ForeignKey("ProjectID")]
        public virtual Projects Project { get; set; }

        [ForeignKey("ApartmentID")]
        public virtual Apartments Apartment { get; set; }

        [ForeignKey("TaskID")]
        public virtual Tasks Task { get; set; }

        [ForeignKey("FitterID")]
        public virtual ACSystemUser Fitter { get; set; }

        public virtual ICollection<PlaceError_Photo> PlaceError_Photos { get; set; }

        public virtual ICollection<Todo> Todos { get; set; }

    }
}