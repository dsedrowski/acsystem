﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class FavouriteProject
    {
        [Key]
        public int ID { get; set; }

        public int ProjectID { get; set; }

        public string UserID { get; set; }


        public virtual Projects Project { get; set; }

        public virtual ACSystemUser User { get; set; }
    }
}