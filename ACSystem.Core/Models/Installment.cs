﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Models
{
    public class Installment
    {
        [Key]
        public int ID { get; set; }

        public int RentAndLoanID { get; set; }

        public int? PayrollID { get; set; }

        public decimal ChargeAmount { get; set; }

        public DateTime Timestamp { get; set; }

        public virtual RentAndLoan RentAndLoan { get; set; }

        public virtual Payroll Payroll { get; set; }
    }
}