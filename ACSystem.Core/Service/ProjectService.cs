﻿using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Service
{
    public class ProjectService
    {
        public static Dictionary<string, bool> CanFinishAndChackProject(int ID)
        {
            var db = new ACSystemContext();

            var project = db.Projects.Find(ID);

            if (project == null)
                return null;

            var tasksError = db.Tasks.Where(q => q.Apartment.ProjectId == ID).Any(q => q.Status == TaskStatus.FinishedWithFaults || q.Status == TaskStatus.PlaceNotReady || q.Status == TaskStatus.DoWyjasnienia);
            var taskNotFinished = db.Tasks.Where(q => q.Apartment.ProjectId == ID).Any(q => q.Status == TaskStatus.Free || q.Status == TaskStatus.Stoped || q.Status == TaskStatus.UnderWork || q.Status == TaskStatus.Planned);
            var taskNotInvoiced = db.Tasks.Where(q => q.Apartment.ProjectId == ID).Any(q => q.Accepted && q.Invoice == false);
            //var taskNotPayroll = db.Tasks.Where(q => q.Apartment.ProjectId == ID).Any(q => q.Task_Fitter.Count(x => (x.Status == TaskStatus.Finished || q.Status == TaskStatus.FinishedWithFaults) && x.InPayroll == false && x.IsActive) > 0);
            var taskNotPayroll = db.Database.SqlQuery<object>($"SELECT Counts FROM view_IsAnyToPayroll WHERE Id = {ID}").Any();
            var taskNotFees = db.Tasks.Where(q => q.Apartment.ProjectId == ID).Any(q => q.Fees.Any(x => x.Status == FeeStatus.DoPotracenia));
            var apartmentNotFees = db.Apartments.Where(q => q.ProjectId == ID).Any(q => q.Fees.Any(x => x.Status == FeeStatus.DoPotracenia));
            var projectNotFees = db.Fees.Where(q => q.ProjectID == ID).Any(q => q.Status == FeeStatus.DoPotracenia);
            var anyInventory = db.Inventory.Any(q => q.ProjectID == ID);

            var NotFees = (taskNotFees || apartmentNotFees || projectNotFees);

            var dict = new Dictionary<string, bool>
            {
                { "Projekt zawiera zadania z błędami", tasksError },
                { "Projekt zawiera niezakończone zadania", taskNotFinished },
                { "Projekt zawiera niezafakturowane zadania", taskNotFinished },
                { "Projekt zawiera zadania nie zaciągniete do list płac", taskNotPayroll },
                { "Projekt zawiera niepobrane szkody", NotFees },
                { "Do projektu przypisane są narzędzia", anyInventory }
            };
            return dict;
        }
    }
}