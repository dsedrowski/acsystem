﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace ACSystem.Core.Service
{
    public class HttpRequests
    {
        public static bool RequestUrl_WithSuccess(string url)
        {
            WebRequest req = (HttpWebRequest)WebRequest.Create(url);
            WebResponse response = req.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());

            ResponseObject obj = JsonConvert.DeserializeObject<ResponseObject>(reader.ReadToEnd());

            return obj.success;
        }

        private class ResponseObject
        {
            public bool success { get; set; }
            //public string error { get; set; }
        }
    }
}