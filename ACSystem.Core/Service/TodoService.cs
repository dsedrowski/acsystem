﻿using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Service
{
    public class TodoService
    {
        public NewToDoViewModel CreateToDoFormViewModel()
        {
            NewToDoViewModel newToDoViewModel = new NewToDoViewModel();
            var db = new ACSystemContext();

            //List<string> projectsList = db.Projects.Where(x => x.ProjectNumber != null).Select(y => y.ProjectNumber).ToList();
            //newToDoViewModel.ProjectList = projectsList;

            newToDoViewModel.TaskList = db.Tasks.ToList();
            newToDoViewModel.Users = db.ACSystemUser.Where(q => q.Position == 0).ToList();
            newToDoViewModel.Fitters = db.Fitters.ToList();
            //newToDoViewModel.Users = db.
            //newToDoViewModel.FitterNames = db.Fitters.Select;
            return newToDoViewModel;
        }
    }
}