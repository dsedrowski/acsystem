﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.XPath;

namespace ACSystem.Core.Service
{
    public class Settings
    {
        public static string ReadSetting(string _value, string _path)
        {
            try
            {
                XPathDocument doc = new XPathDocument(_path);
                XPathNavigator nav = doc.CreateNavigator();

                XPathExpression exp;

                exp = nav.Compile($@"/settings/{_value}");

                XPathNodeIterator iterator = nav.Select(exp);

                while (iterator.MoveNext())
                {
                    return iterator.Current.Value;
                }

                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static bool WriteSetting(string _element, string _value, string _path)
        {
            try
            {
                XmlTextReader reader = new XmlTextReader(_path);
                XmlDocument doc = new XmlDocument();
                doc.Load(reader);

                reader.Close();

                XmlNode oldNode;
                XmlElement root = doc.DocumentElement;
                oldNode = root.SelectSingleNode($@"/settings/{_element}");
                oldNode.InnerText = _value;

                doc.Save(_path);

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}