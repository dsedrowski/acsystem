﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services.Description;

namespace ACSystem.Core.Service
{
    public class Objects 
    {
        public static string GetDisplayName<T>(string parametr)
        {
            MemberInfo property = typeof(T).GetProperty(parametr);
            var dd = property.GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute;

            if (dd != null)
            {
                return dd.Name;
            }

            return String.Empty;
        }

    }
}