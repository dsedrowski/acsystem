﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;

namespace ACSystem.Core.Service
{
    public class MailService
    {
#if !DEBUG
        private static readonly string SMTP = "nl1-wts2.my-hosting-panel.com";
        private static readonly int PORT = 25;
        private static readonly string USER = "rapport@acbygg.com";
        private static readonly string PASSWORD = "ah66rX_2";
        private static readonly string MESSAGE_FROM = "rapport@acbygg.com";
#endif

#if DEBUG
        private static readonly string SMTP = "smtp.gmail.com";
        private static readonly int PORT = 465;
        private static readonly string USER = "damian.sedrowski@gmail.com";
        private static readonly string PASSWORD = "leokadia123";
        private static readonly string MESSAGE_FROM = "damian.sedrowski@gmail.com";
#endif
        private static readonly string MESSAGE_FROM_DISPLAY = "Rapport Från ACBYGG";
        private static readonly string MESSAGE_REPLY_TO = "info@acbygg.com";

        public static bool Invoice(Invoice invoice, string pdfPath, out string _exception)
        {
            var client = Client();

            List<Attachment> attachments = new List<Attachment>();

            attachments.Add(AttachmentPDF(pdfPath));

            var message = Message(
                                    invoice.Customer.Email,
                                    $"Invoice No. {invoice.InvoiceNo}, Order number: {invoice.Project.ProjectNumber}",
                                    "",
                                    attachments);

            client.Credentials = new NetworkCredential(USER, PASSWORD);

            try
            {
                SmtpClient newClient = new SmtpClient("smtp.1and1.com", PORT);
                newClient.EnableSsl = true;
                newClient.UseDefaultCredentials = true;
                newClient.Credentials = new NetworkCredential(USER, PASSWORD);
                newClient.Send(message);
            }
            catch (Exception ex)
            {
                var db = new ACSystemContext();

                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "mail",
                    LogDate = DateTime.Now,
                    User = "admin"
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                _exception = ex.Message;

                return false;
            }

            _exception = "";

            return true;
        }

        public static bool Raport(Tasks task, string pdfPath, List<string> imagesPaths, out string _exception)
        {
            var client = Client();

            var attachments = new List<Attachment>
            {
                AttachmentPDF(pdfPath)
            };

            attachments.AddRange(imagesPaths.Select(Attachment));

            if (task.Apartment.Project.Project_ProjectManager.Any() == false)
            {
                _exception = "Brak menedżerów projektu.";
                return false;
            }

            var emails = task.Apartment.Project.Project_ProjectManager.Select(pm => pm.ProjectManager.Email).ToList();
            emails.Add("rapport@acbygg.com");

            var emailTitle = "";

            bool isAllOk = (task.Task_Questions != null) ? Task_Questions.IsAllOk(task.Task_Questions) : Task_Question.IsAllOk(task.Task_Question);

            if (task.Status == TaskStatus.Finished && isAllOk)
                emailTitle = $"{task.Apartment.Project.ProjectNumber} LGH {task.Apartment.NumberOfApartment} OK";
            else
                emailTitle = $"OBS! {task.Apartment.Project.ProjectNumber} LGH {task.Apartment.Letter}{task.Apartment.NumberOfApartment} !";


            var message = Message(
                                    emails,
                                    emailTitle,
                                    "",
                                    attachments);

            if (task.Status != TaskStatus.Finished)
                message.Priority = MailPriority.High;

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                var db = new ACSystemContext();

                var log = new ExceptionsLog
                {
                    Message = ex.Message + " Emails: " + string.Join(",", emails),
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "mail",
                    LogDate = DateTime.Now,
                    User = "admin"
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                _exception = ex.Message;

                return false;
            }

            _exception = "";

            return true;
        }

        public static bool ApartmentGroupRaport(ReportNag report, string pdfPath, out string _exception, string title = "")
        {
            var client = Client();

            var attachments = new List<Attachment>
            {
                AttachmentPDF(pdfPath)
            };

            if (report.Project.Project_ProjectManager.Any() == false)
            {
                _exception = "Brak menedżerów projektu.";
                return false;
            }

            var emails = report.Project.Project_ProjectManager.Select(pm => pm.ProjectManager.Email).ToList();

            var emailTitle = (string.IsNullOrEmpty(title)) ? $"{report.Project.InSystemID} {report.Project.ProjectNumber} LGH {report.Apartment.NumberOfApartment}" : title;


            var message = Message(
                                    emails,
                                    emailTitle,
                                    "",
                                    attachments);

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                var db = new ACSystemContext();

                var log = new ExceptionsLog
                {
                    Message = ex.Message + " Emails: " + string.Join(",", emails),
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "mail",
                    LogDate = DateTime.Now,
                    User = "admin"
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                _exception = ex.Message;

                return false;
            }

            _exception = "";

            return true;
        }

        public static bool PlaceNotReady(Tasks task, List<string> imagesPaths, Leanguage leanguage, out string _exception)
        {
            var client = Client();

            var attachments = imagesPaths.Select(Attachment).ToList();

            var emails = task.Apartment.Project.Project_ProjectManager.Select(pm => pm.ProjectManager.Email).ToList();

            var msg = "";
            var subject = "";

            switch (leanguage)
            {
                case Leanguage.English:
                    msg = "Building site not prepared for our fitting.";
                    subject = $"Project Number: {task.Apartment.Project.ProjectNumber}, LGH: {task.Apartment.Letter}{task.Apartment.NumberOfApartment}";
                    break;
                case Leanguage.Swedish:
                    msg = "Arbetsplatsen ej förbered för vårt arbete.";
                    subject = $"{task.Apartment.Project.ProjectNumber} LGH: {task.Apartment.Letter}{task.Apartment.NumberOfApartment} Plats ej klar för montage {task.EndDate.ToString("yyyy-MM-dd HH:mm")}";
                    break;
                case Leanguage.Polish:
                    msg = $"Miejsce budowy dla zadania {task.GetTaskName(task.Apartment.ProjectId)} nie jest przygotowane. Prace zostały wstrzymane.";
                    subject = $"Numer projektu: {task.Apartment.Project.ProjectNumber}, LGH: {task.Apartment.Letter}{task.Apartment.NumberOfApartment}";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(leanguage), leanguage, null);
            }

            var message = Message(
                                    emails,
                                    subject,
                                    msg,
                                    attachments);

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                var db = new ACSystemContext();

                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "mail",
                    LogDate = DateTime.Now,
                    User = "admin"
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                _exception = ex.Message;

                return false;
            }

            _exception = "";

            return true;
        }

        public static bool NewTodo(Tasks task, IACSystemContext context)
        {
            var client = Client();

            var attachments = new List<Attachment>();

            if (task == null) return false;
            if (context == null) return false;

            task.Product = context.Products.Find(task.ProductId);
            task.Apartment = context.Apartments.Find(task.ApartmentId);

            var msg = $"Dla zadania {task.Product?.Name} w apartamencie {task.Apartment?.Letter}{task.Apartment?.NumberOfApartment} projektu {task.Apartment?.Project.ProjectNumber} jest czynność do wykonania.";

            var message = Message(
                                    "richard@acbygg.com",
                                    $"Numer projektu: {task.Apartment?.Project.ProjectNumber}, LGH: {task.Apartment?.NumberOfApartment} Zadanie: {task.Product?.Name}",
                                    msg,
                                    attachments);

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                var db = new ACSystemContext();

                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "mail",
                    LogDate = DateTime.Now,
                    User = "admin"
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                //_exception = ex.Message;

                return false;
            }

            //_exception = "";

            return true;
        }

        public static bool TaskActivity(string fitterMail, string projectNumber, string apartmentNumber, string task,
            decimal workDone, decimal worktime, DateTime timestamp, string status)
        {
            var client = Client();

            var msg = $"Projekt {projectNumber}, Apartament {apartmentNumber}, Zadanie {task}. Zmieniono status na {status}. Wykonana praca: {workDone}. Czas pracy: {worktime}. Data zakończenia: {timestamp:yyyy-MM-dd HH:mm}";

            var message = Message(
                fitterMail,
                $"Zmiana statusu zadania {task}.",
                msg, new List<Attachment>());

            try
            {
                client.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                var db = new ACSystemContext();

                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "mail",
                    LogDate = DateTime.Now,
                    User = "admin"
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                //_exception = ex.Message;

                return false;
            }
        }

        public static bool Order(string mail, Order order)
        {
            var client = Client();

            //var msg = $"<h3>Ordernummer: {order.ID.ToString().PadLeft(4, '0')}</h3>";
            var msg = $"<h4>{order.Project.ProjectNumber}";

            if (order.Apartment != null)
                msg += $" LGH: {order.Apartment.NumberOfApartment}";

            msg += "</h4>";

            msg += "<p>Hej,</p>";
            msg +=
                "<p>Ni behöver beställa lite material till denna lägenhet så att vi kan slutföra vårt arbete. Meddela oss gärna när ni vet när sakerna kommer så att vi kan boka in arbetet.</p>";
            msg += "<p>Med vänliga Hälsningar</p>";

            msg += "<ol>";

            msg = order.OrderItems.Aggregate(msg, (current, item) => current + $"<li>{item.Item}</li>");

            msg += "</ol>";

            msg += "<br />";

            msg += $"<p>{order.Fitter.User.FullName}</p>";

            msg += "<p>AC Bygg</p>";

            var lgh = "";

            if (order.Apartment != null)
                lgh = $" LGH {order.Apartment.NumberOfApartment}";

            var message = Message(
                mail,
                $"{order.Project.ProjectNumber}{lgh} Saker att beställa",
                msg, new List<Attachment>());

            message.IsBodyHtml = true;

            try
            {
                client.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                var db = new ACSystemContext();

                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "mail",
                    LogDate = DateTime.Now,
                    User = "admin"
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();

                //_exception = ex.Message;

                return false;
            }
        }

        public static bool Payroll(Payroll payroll, string fitterName, string fitterEmail, string pdfPath)
        {
            var client = Client();

            var emails = new List<string>
            {
                fitterEmail,
                "richard@acbygg.com",
                "lucas@acbygg.com"
            };

            var attachments = new List<Attachment>
            {
                AttachmentPDF(pdfPath)
            };

            var msg = Message(emails, $"Lista płac nr. {payroll.Number}, {fitterName}", "", attachments);

            try
            {
                client.Send(msg);
            }
            catch (Exception ex)
            {
                Helpers.Log(ex, "Generowanie pdf listy płac", "");
                return false;
            }

            return true;

        }

        public static bool SendPassword(string email, string name, string password, out string _exception)
        {
            var client = Client();

            var body = $@"
                            Dzień dobry ! <br />
                            Użytkownikowi o loginie {email}, system ACSystem przydzielił hasło {password}. <br />
                            <br /><br />
                            Email został wygenerowany automatycznie. Prosimy na niego nie odpowiadać !
                        ";

            var title = "ACSystem - dane logowania";

            var message = Message(email, title, body, new List<Attachment>());

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                _exception = ex.Message;

                return false;
            }

            _exception = "";

            return true;

        }

        public static bool SendResetPasswordCode(string email, string name, string code, out string _exception)
        {
            var client = Client();

            var body = $@"
                            Dzień dobry ! <br />
                            Dla użytkownika {email}, została zgłoszona chęć zmiany hasła. W tym celu prosimy o przejście <a href={code}>TUTAJ</a> <br />
                            <br />
                            <br /><br />
                            Email został wygenerowany automatycznie. Prosimy na niego nie odpowiadać !
                        ";

            var title = "ACSystem - zmiana hasła";

            var message = Message(email, title, body, new List<Attachment>());

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                Service.Helpers.Log(ex, nameof(SendResetPasswordCode), "");
                _exception = ex.Message;

                return false;
            }

            _exception = "";

            return true;
        }

        #region PlaceError

        public static bool PlaceError_MailReport(string _title, List<string> _emails, string _pdfPath, out string _exception)
        {
            var client = Client();

            var attachments = new List<Attachment>
            {
                AttachmentPDF(_pdfPath)
            };

            if (!_emails.Any())
            {
                _exception = "Brak menedżerów projektu.";
                return false;
            }

            var message = Message(_emails, _title, "", attachments);
            message.Priority = MailPriority.High;

            try
            {
                client.Send(message);

                _exception = "";
                return true;
            }
            catch (Exception ex)
            {
                Helpers.Log(ex, "PlaceError_MailReport", "");
                _exception = ex.Message;
                return false;
            }
        }

        #endregion

        private static SmtpClient Client()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;
            SmtpClient client = new SmtpClient(SMTP)
            {
                EnableSsl = false,
                Credentials = new NetworkCredential(USER, PASSWORD)
            };
            //client.DeliveryMethod = SmtpDeliveryMethod.Network;

            return client;
        }

        private static MailMessage Message(string to, string subject, string body, List<Attachment> attachments)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(MESSAGE_FROM, MESSAGE_FROM_DISPLAY, Encoding.UTF8);
            message.ReplyToList.Add(MESSAGE_REPLY_TO);
            message.To.Add(new MailAddress(to));
            message.Subject = subject;
            message.SubjectEncoding = Encoding.UTF8;
            message.Body = body;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            message.Priority = MailPriority.Normal;

            foreach (var attachment in attachments)
            {
                message.Attachments.Add(attachment);
            }

            return message;
        }

        private static MailMessage Message(List<string> to, string subject, string body, List<Attachment> attachments)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(MESSAGE_FROM, MESSAGE_FROM_DISPLAY, Encoding.UTF8);
            message.ReplyToList.Add(MESSAGE_REPLY_TO);
            foreach (var t in to)
            {
                message.To.Add(new MailAddress(t));
            }
            message.Subject = subject;
            message.SubjectEncoding = Encoding.UTF8;
            message.Body = body;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            message.Priority = MailPriority.Normal;

            foreach (var attachment in attachments)
            {
                message.Attachments.Add(attachment);
            }

            return message;
        }

        private static Attachment AttachmentPDF(string path)
        {
            Attachment attachment = new Attachment(path, MediaTypeNames.Application.Pdf);
            ContentDisposition disposition = attachment.ContentDisposition;
            disposition.CreationDate = File.GetCreationTime(path);
            disposition.ModificationDate = File.GetLastWriteTime(path);
            disposition.ReadDate = File.GetLastAccessTime(path);
            disposition.FileName = Path.GetFileName(path);
            disposition.Size = new FileInfo(path).Length;
            disposition.DispositionType = DispositionTypeNames.Attachment;

            return attachment;
        }

        private static Attachment Attachment(string path)
        {
            Attachment attachment = new Attachment(path);
            ContentDisposition disposition = attachment.ContentDisposition;
            disposition.CreationDate = File.GetCreationTime(path);
            disposition.ModificationDate = File.GetLastWriteTime(path);
            disposition.ReadDate = File.GetLastAccessTime(path);
            disposition.FileName = Path.GetFileName(path);
            disposition.Size = new FileInfo(path).Length;
            disposition.DispositionType = DispositionTypeNames.Attachment;

            return attachment;
        }
    }
}