﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace ACSystem.Core.Service
{
    public class NBPApi
    {
        public static NBPApiModel GetRate(string currency)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"http://api.nbp.pl/api/exchangerates/rates/A/{currency.ToUpper()}?format=json");
            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;
            request.Method = "GET";
            request.ContentType = "application/json; charset=UTF-8";
            request.Accept = "application/json";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            string result;

            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                result = rdr.ReadToEnd();
            }

            var toRet = JsonConvert.DeserializeObject<NBPApiModel>(result);

            return toRet;
        }
    }

    public class NBPApiModel
    {
        public string table { get; set; }
        public string currency { get; set; }
        public string code { get; set; }
        public List<NBPApiRatesModel> rates { get; set; }
    }

    public class NBPApiRatesModel
    {
        public string no { get; set; }
        public DateTime effectiveDate { get; set; }
        public double mid { get; set; }
    }
}