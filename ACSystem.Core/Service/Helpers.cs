﻿using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace ACSystem.Core.Service
{
    public class Helpers
    {
        public static void Log(_Exception ex, string url, string user)
        {
            using (var db = new ACSystemContext())
            {
                var log = new ExceptionsLog
                {
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    URL = url,
                    Message = $"MESSAGE: {ex.Message}, INNER MESSAGE: {ex.InnerException?.InnerException?.Message}",
                    StackTrace = ex.StackTrace,
                    LogDate = DateTime.Now,
                };

                db.ExceptionsLog.Add(log);

                db.SaveChanges();
            }

            return;
        }
    }
}