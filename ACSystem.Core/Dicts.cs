﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core
{
    public class Dicts
    {
        public static Dictionary<string, string> Swe_Questions = new Dictionary<string, string>
        {
            { "Szafki dolne", "Underskåp" },
            { "Szafki ścienne", "Väggskåp" },
            { "Szafy wysokie", "Högskåp" },
            { "Passbit", "Passbit" },
            { "Takanslutning", "Takanslutning" },
            { "Tacksida", "Tacksida" },
            { "Blat", "Bänkskiva" },
            { "Zlew", "Diskbänk" },
            { "Raczki", "Handtag" },
            { "Cokoły", "Sockel" },
            { "Kratka wentylacyjna", "Galler" },
            { "Oświetlenie", "Belysning" },
            { "Fugi silikonowe", "Fogning" },
            { "Instrukcje montazu i uzytkowania", "Manualer" },
            { "Urządzenia", "Maskiner" },
            { "Piekarnik", "Ugn" },
            { "Zamrażarka 1", "Kyl/Frys 1" },
            { "Zamrażarka 2", "Kyl/Frys 2" },
            { "Plyta grzejna", "Häll" },
            { "Zmywarka", "Diskmaskin" },
            { "Mikrofalówka", "Mikro" },
            { "Okap kuchenny", "Fläkt" },
            { "Wycięcie na rury", "Ursågat för rör" },
            { "Wycięcie na wentylację", "Ursågat för ventilation" },
            { "Śmietnik", "Sopsortering" },
            { "Barnspärr", "Barnspärr" }
        };

        public static Dictionary<string, string> Eng_Questions = new Dictionary<string, string>
        {
            { "Szafki dolne", "Base unit" },
            { "Szafki ścienne", "Wall unit" },
            { "Szafy wysokie", "Tall unit" },
            { "Passbit", "Filler" },
            { "Takanslutning", "Ceiling scribe" },
            { "Tacksida", "Side panel" },
            { "Blat", "Worktop" },
            { "Zlew", "Sink" },
            { "Raczki", "Handle" },
            { "Cokoły", "Plinth" },
            { "Kratka wentylacyjna", "Grate" },
            { "Oświetlenie", "Lightning" },
            { "Fugi silikonowe", "Silicone" },
            { "Instrukcje montazu i uzytkowania", "Manuals" },
            { "Urządzenia", "Appliances" },
            { "Piekarnik", "Oven" },
            { "Zamrażarka 1", "Fridge/Freezer 1" },
            { "Zamrażarka 2", "Fridge/Freezer 2" },
            { "Plyta grzejna", "Hob" },
            { "Zmywarka", "Dishwasher" },
            { "Mikrofalówka", "Micro" },
            { "Okap kuchenny", "Hood" },
            { "Wycięcie na rury", "Ursågat för rör" },
            { "Wycięcie na wentylację", "Ursågat för ventilation" },
            { "Śmietnik", "Sopsortering" },
            { "Barnspärr", "Barnspärr" }
        };

        public static Dictionary<string, string> PL_Questions = new Dictionary<string, string>
        {
            { "BaseUnits", "Szafki dolne" },
            { "WallUnits", "Szafki ścienne" },
            { "TallUnits", "Szafy wysokie" },
            { "Fillers", "Passbit" },
            { "CeillingScribe", "Takanslutning" },
            { "SidePiece", "Tacksida" },
            { "Worktop", "Blat" },
            { "Sink", "Zlew" },
            { "Handles", "Raczki" },
            { "Plinth", "Cokoły" },
            { "Grids", "Kratka wentylacyjna" },
            { "Lighting", "Oświetlenie" },
            { "Silicon", "Fugi silikonowe" },
            { "Manuals", "Instrukcje montazu i uzytkowania" },
            { "CutIntoPipes", "Wycięcie na rury" },
            { "CutOutForVentilation", "Wycięcie na wentylację" },
            { "Trash", "Śmietnik" },
            { "ChildSafety", "Barnspärr" },
            { "Machines", "Urządzenia" },
            { "Oven", "Piekarnik" },
            { "FridgeFreezer1", "Zamrażarka 1" },
            { "FridgeFreezer2", "Zamrażarka 2" },
            { "Hob", "Plyta grzejna" },
            { "Dishwasher", "Zmywarka" },
            { "Mikrowave", "Mikrofalówka" },
            { "Fan", "Okap kuchenny" }
        };

        public static Dictionary<ProjectStatus, string> StatusyProjektu = new Dictionary<ProjectStatus, string>
        {
            { ProjectStatus.UnderWork, "W trakcie" },
            { ProjectStatus.NotStarted, "Nie rozpoczęte" },
            { ProjectStatus.FinishedWithFaults, "Zakończone z błędami" },
            { ProjectStatus.Finished, "Zakończone" }
        };
    }
}