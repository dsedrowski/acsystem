﻿using ACSystem.Core.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
// ReSharper disable All

namespace ACSystem.Core.IRepo
{
    public interface IACSystemContext : IDisposable, IObjectContextAdapter
    {
        DbSet<ACSystemUser> ACSystemUser { get; set; }
        DbSet<Customers> Customers { get; set; }
        DbSet<ProjectManagers> ProjectManagers { get; set; }
        DbSet<Projects> Projects { get; set; }
        DbSet<Tasks> Tasks { get; set; }
        DbSet<Task_Fitter> Task_Fitter { get; set; }
        DbSet<Task_Questions> Task_Questions { get; set; }
        DbSet<TaskQuestionPhoto> TaskQuestionPhoto { get; set; }
        DbSet<TaskActivityPhotos> TaskActivityPhotos { get; set; }
        DbSet<TaskPatches> TaskPatches { get; set; }
        DbSet<Apartments> Apartments { get; set; }
        DbSet<Project_ProjectManager> Project_ProjectManager { get; set; }
        DbSet<Inventory> Inventory { get; set; }
        DbSet<Inventory_Accessories> Inventory_Accessories { get; set; }
        DbSet<Inventory_Accessories_Photo> Inventory_Accessories_Photo { get; set; }
        DbSet<Inventory_Accessories_Connect> Inventory_Accessories_Connect { get; set; }
        DbSet<Inventory_Check> Inventory_Check { get; set; }
        DbSet<Inventory_Check_Photo> Inventory_Check_Photo { get; set; }
        DbSet<Inventory_Fitter> Inventory_Fitter { get; set; }
        DbSet<Inventory_Photo> Inventory_Photo { get; set; }
        DbSet<InventoryType> InventoryType { get; set; }
        DbSet<InventoryWarehouse> InventoryWarehouse { get; set; }
        DbSet<Fitters> Fitters { get; set; }
        DbSet<Products> Products { get; set; }
        DbSet<PercentHeight> PercentHeight { get; set; }
        DbSet<Events> Events { get; set; }
        DbSet<Payroll> Payroll { get; set; }
        DbSet<PayrollItems> PayrollItems { get; set; }
        DbSet<Fees> Fees { get; set; }
        DbSet<FeeFiles> FeeFiles { get; set; }
        DbSet<Invoice> Invoice { get; set; }
        DbSet<InvoiceElements> InvoiceElements { get; set; }
        DbSet<BankAccounts> BankAccounts { get; set; }
        DbSet<ExceptionsLog> ExceptionsLog { get; set; }
        DbSet<Task_AccessGrant> Task_AccessGrant { get; set; }
        DbSet<TaskComment> TaskComment { get; set; }
        DbSet<Order> Order { get; set; }
        DbSet<OrderItem> OrderItem { get; set; }
        DbSet<TaskDocuments> TaskDocuments { get; set; }
        DbSet<Calendar> Calendar { get; set; }
        DbSet<RentAndLoan> RentAndLoan { get; set; }
        DbSet<Installment> Installment { get; set; }
        DbSet<ProjectInvoicesPercent> ProjectInvoicesPercent { get; set; }
        DbSet<ProjectInvoicesPercent_Elements> ProjectInvoicesPercent_Elements { get; set; }
        DbSet<ProjectDocuments> ProjectDocuments { get; set; }
        DbSet<Product_Project> Product_Project { get; set; }
        DbSet<ReportNag> ReportNag { get; set; }
        DbSet<ReportElem> ReportElem { get; set; }
        DbSet<Rooms> Rooms { get; set; }
        DbSet<Access_User_Project> Access_User_Project { get; set; }
        DbSet<Access_User_Apartment> Access_User_Apartment { get; set; }
        DbSet<Access_User_Task> Access_User_Task { get; set; }
        DbSet<Todo> Todo { get; set; }
        DbSet<Project_Activity> Project_Activity { get; set; }
        DbSet<Product_Plan> Product_Plan { get; set; }
        DbSet<Product_Group> Product_Group { get; set; }
        DbSet<QuestionType> QuestionType { get; set; }
        DbSet<Question> Question { get; set; }
        DbSet<Product_Question> Product_Question { get; set; }
        DbSet<Task_Question> Task_Question { get; set; }
        DbSet<Answer> Answer { get; set; }
        DbSet<Question_Product_Project> Question_Product_Project { get; set; }
        DbSet<view_CheckTaskPriceNorm> view_CheckTaskPriceNorm { get; set; }
        DbSet<view_CheckPayrollRateNorm> view_CheckPayrollRateNorm { get; set; }
        DbSet<FavouriteProject> FavouriteProjects { get; set; }
        DbSet<PlaceError> PlaceError { get; set; }
        DbSet<PlaceError_Photo> PlaceError_Photo { get; set; }
        DbSet<FavouriteCity> FavouriteCity { get; set; }
        DbSet<Fitter_Crew> Fitter_Crew { get; set; }
        DbSet<Invoice_Adds> Invoice_Adds { get; set; }
        DbSet<Fitter_Hours> Fitter_Hours { get; set; }
        DbSet<Fitter_Hours_Project> Fitter_Hours_Project { get; set; }
        DbSet<Settings> Settings { get; set; }

        DbSet<UserChangeLog> UserChangeLog { get; set; }
        DbSet<Payroll_Errors> Payroll_Errors { get; set; }

        int SaveChanges();
        Task<int> SaveChangesAsync();

        ACSystemContext CreateContext();

        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        Database Database { get; }

        DbEntityEntry Entry(object entity);

    }
}