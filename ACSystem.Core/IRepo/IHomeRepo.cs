﻿using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACSystem.Core.IRepo
{
    public interface IHomeRepo
    {
        IQueryable<Tasks> TasksList();

        IQueryable<Tasks> TasksList(int apartmentId);

        Task<Tasks> GetTask(int id);

        IQueryable<Events> EventsList();

        Task<Events> GetEvent(int id);

        bool TaskEdit(Tasks task);

        bool EventEdit(Events events);

        IQueryable<Projects> ProjectsList();

        IQueryable<Apartments> ApartmentsList(int projectId);

        IACSystemContext GetContext();
        
        void SaveChanges();
    }
}
