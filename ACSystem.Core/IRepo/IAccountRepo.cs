﻿using ACSystem.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ACSystem.Core.IRepo
{
    public interface IAccountRepo
    {
        /// <summary>
        /// Zwraca listę użytkowników
        /// </summary>
        List<ACSystemUser> UsersList();

        /// <summary>
        /// Zwraca dane użytkownika
        /// </summary>
        /// <param name="id">ID Uzytkownika</param>
        /// <returns>Dane użytkonika</returns>
        EditUserViewModel UserById(string id);

        /// <summary>
        /// Zwraca pełne dane użytkownika
        /// </summary>
        /// <param name="id">ID Użytkownika</param>
        /// <returns>Pełne dane użytkownika</returns>
        ACSystemUser FullUserById(string id);

        /// <summary>
        /// Edytuje użytkownika
        /// </summary>
        /// <param name="model">Model z danymi uzytkownika</param>
        bool UpdateUser(EditUserViewModel model);

        void AddFitter(Fitters fitter);

        void EditFitter(Fitters fitter, string userId);

        void DeleteFitter(Fitters fitter);

        bool DeleteFitterInventory(string userId);

        void SaveChanges();
    }
}