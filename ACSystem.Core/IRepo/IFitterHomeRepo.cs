﻿using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACSystem.Core.IRepo
{
    public interface IFitterHomeRepo
    {
        int TaskAvailableCount(string userId, bool onlyFavouriteProjects);

        List<IGrouping<Payroll, PayrollItems>> PayrollListForFitter(string id);

        List<PayrollItems> PayrollItemsForFitter(string id);

        IQueryable<Tasks> TasksList();

        IQueryable<Tasks> TasksList(int apartmentId);

        Task<Tasks> GetTask(int id);

        IQueryable<Events> EventsList();

        Task<Events> GetEvent(int id);
        Fitters GetFitter(string id);
        IACSystemContext GetContext();

        bool IsFitterBusy(string userId);
    }
}
