﻿using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using ACSystem.Core.Models.ViewModels.Select;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace ACSystem.Core.IRepo
{
    public interface IProjectsRepo : IDisposable
    {
        IACSystemContext GetContext();

        /// <summary>
        /// Liczba wierszy w zapytaniu
        /// </summary>
        int Count();

        /// <summary>
        /// Lista projektów
        /// </summary>
        /// <param name="rows">Liczba wierszy w zapytaniu</param>
        /// <param name="skip">O ile wierszy zapytanie ma przeskoczyć</param>
        /// <param name="take">Ilość wierszy do pobrania</param>
        /// <param name="orderBy">Kolumna, po której sortowana będzie tabela</param>
        /// <param name="dest">Kierunek sortowania</param>
        /// <param name="search">Słowa potrzebne do wyszukiwania</param>
        List<ProjectsListViewModel> List(out int rows, int? skip = null, int? take = null, string orderBy = "name", string dest = "asc", string search = "", string filter = "0,1,2,3", int daysRemind = 0);

        List<Tasks> ListWithAvailableTask(string userId, bool showHistory);

        /// <summary>
        /// Pobiera dane projektu
        /// </summary>
        /// <param name="id">ID projektu</param>
        Task<Projects> GetProject(int id);

        Task<bool> IsAnyError(int id);

        /// <summary>
        /// Tworzy projekt
        /// </summary>
        /// <param name="project">Model z danymi projektu</param>
        int? Create(Projects project);

        /// <summary>
        /// Edytuje projekt
        /// </summary>
        /// <param name="project">Model z danymi projektu</param>
        /// <returns></returns>
        bool Edit(Projects project, string userID);

        /// <summary>
        /// Usuwa projekt
        /// </summary>
        /// <param name="id">ID Projektu</param>
        Task<string> Delete(int id);

        IEnumerable<Customers> GetCustomers();

        IEnumerable<ProjectManagers> GetProjectManagers();

        List<PayrollItems> PayrollItemsForProject(int projectID);

        List<Invoice> InvoiceElementsForProject(int projectID);

        List<Fees> FeesForProject(int projectID);

        IEnumerable<ProjectManagerViewModel> GetProjectManagersByCustomerId(int id);

        ProjectManagers ProjectManagerById(int id);

        Apartments ApartmentById(int id);

        List<Apartments> ApartmentsList(int projectId);

        int ApartmentCreate(Apartments apartment);

        void ApartmentEdit(Apartments apartment);

        void ApartmentDelete(int apartmentId);

        List<PayrollItems> PayrollItemsForApartment(int apartmentID);

        List<InvoiceElements> InvoiceElementsForApartment(int apartmentID);

        List<Fees> FeesForApartment(int apartmentID);

        List<Products> ProductsList();

        List<TasksViewModel> TaskList(out int rows, int id, int? skip = null, int? take = null, string orderBy = "name", string dest = "asc", string search = "", string status = "0,1,2,3,4", string type = "0,1,2");

        List<Tasks> TaskListByApartment(int ApartmentId);

        List<TasksViewModel> TaskListByWeek(int week);

        Task<Tasks> GetTask(int id);

        Tasks GetTaskNoAsync(int id);

        Task<Tasks> GetCurrentTask(string fitterId);

        string TaskCreate(Tasks task, bool Accepted = true, bool GetTask = false, string FitterID = "", int[] questions = null, bool saveChanges = true);

        bool TaskPatchesCreate(List<TaskPatches> taskPatches);

        bool TaskEdit(Tasks task, string userID = "", bool canStatusChange = true, int[] questions = null, string[] answers = null);

        Task<string> TaskDelete(int id);

        //List<Fitters> FittersListByTask

        bool FitterTaskGet(int taskId, string userId);

        bool AccessGrantToTask(int taskId, string userId);

        List<FittersSelect> GetFreeFitters();

        List<FittersSelect> GetFitters();

        List<FittersSelect> GetOuterFitters();

        List<FittersSelect> GetTaskFittersList(int id);

        Fitters GetFitter(string id);

        bool IsFitterBusy(string userId);

        void ChangeStatus(List<FitterWorkDoneViewModel> workDone, decimal Distance, TaskErrorType ErrorType, TaskStatus status, string[] photos, string[] photosDesc, string[] photosQuestion, string[] photosQuestionDesc, string[] question, Task_Questions questions, DateTime? handDateTime, string[] answers, string[] additionalInformations, string reportUserId);

        void LeaveTask(string userId, decimal done);

        List<ActivityPhotosViewModel> GetActivityPhotos(int id);

        List<QuestionPhotosViewModel> GetQuestionPhotos(int id, string question);

        bool AddEvent(Events events);

        bool CreateQuestions(int taskId);

        bool EditQuestion(string query);

        bool EditPatch(string query);

        List<ProjectsSelect> GetProjectsList();

        List<ApartmentsSelect> GetApartmentsList(int id);

        Products GetProduct(int id);

        int TaskTodoCount();
        List<TaskTodo> TaskTodo(out int rows, int? skip = default(int?), int? take = default(int?), string orderBy = "name", string dest = "asc", string search = "", string statuss = "0,1,2,3,4");

        bool TodoCheck(int ID);

        bool CanUserReportHours(string userId, DateTime date, decimal hours);

        /// <summary>
        /// Zapisuje dane
        /// </summary>
        void SaveChanges();
    }
}