﻿using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ACSystem.Core.IRepo
{
    public interface ITasksRepo
    {
        int Count();

        int AvailableCount();

        List<TasksViewModel> List(out int rows, int id, int? skip = null, int? take = null, string orderBy = "name", string dest = "asc", string search = "", string status = "0,1,2,3,4", string type = "0,1,2");

        Task<Tasks> GetTask(int id);        

        bool Create(Tasks task);

        bool Edit(Tasks task);

        Task<string> Delete(int id);

        void SaveChanges();
    }
}
