﻿using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACSystem.Core.IRepo
{
    public interface IProductsRepo
    {
        int Count();

        List<ProductsListViewModel> List(out int rows, int? skip = null, int? take = null, string orderBy = "name", string dest = "asc", string search = "", string filter = "");

        Task<Products> GetProduct(int id);

        ProductsViewModel GetProductViewModel(int id);

        Products Create(ProductsViewModel products);

        bool Create(PercentHeight ph);

        bool Edit(Products product, List<int> projects);

        bool Edit(PercentHeight ph);

        Task<string> Delete(int id);

        #region Percentage
        Task<PercentHeight> GetPercentHeight(int id);
        #endregion

        void AddDefaultProductToProjects(Products pr, PercentHeight ph);

        void SaveChanges();
    }
}
