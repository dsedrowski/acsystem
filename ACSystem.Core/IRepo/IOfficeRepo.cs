﻿using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using ACSystem.Core.Models.ViewModels.Select;
using ACSystem.Core.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACSystem.Core.IRepo
{
    public interface IOfficeRepo
    {

        ACSystemContext GetContext();

        #region PAYROLL
        int Count();

        List<PayrollListViewModel> List(out int rows, int? skip = default(int?), int? take = default(int?), string orderBy = "date", string dest = "desc", string search = "", string filters = "0,1,2");

        Task<Payroll> GetPayroll(int id);

        int LastPayrollNumber();

        //Task<int?> CreatePayroll(int number, DateTime toDate);

        int? CreatePayroll(out Dictionary<string, string> hoursNotReported, int number, DateTime toDate, string fitterID = "", int? ProjectID = null);

        Task<string> DeletePayroll(int id);

        void SupervisorAddsService(List<SupervisorAdds> list, List<Projects> projects, PayrollItems data, Fitters fitter, Tasks task);
        #endregion

        #region INVOICE
        int InvoicesCount();

        int LastInvoiceNumber();

        List<InvoiceViewModel> InvoicesList(out int rows, int? skip = default(int?), int? take = default(int?), string orderBy = "invoicedate", string dest = "desc", string search = "", string filters = "0,1,2");

        Task<Invoice> GetInvoice(int id);

        Task<int?> CreateInvoice(Invoice invoice);

        Task<int?> EditInvoice(Invoice invoice);

        Task<string> DeleteInvoice(int id);

        Task<string> DeleteInvoiceElement(int id);

        Task<string> DeleteInvoiceElementByApartment(string apartmentNumber, int invoiceID);

        Task<bool> SaveInvoice(string ids);

        List<InvoiceElements> GetInvoiceElements(int id);

        List<InvoiceElements> GetInvoiceElements(List<int> id);

        int CustomerPaymentTerm(int id);

        DateTime LastInvoice(int customerID, int projectID);

        List<TasksSelect> GetNotInvoicedTasks(int ProjectID);

        Task<List<int>> GenerateInvoiceElements(DateTime To, Invoice invoice);

        List<BankAccountsSelect> AddBankAccount(string Currency, string Account);

        InvoiceElementViewModel CreateSingleInvoiceElement(int invoiceID, int taskID);

        List<TasksSelect> GetApartmentTasksList(string apartmentNumber, int invoiceID); 

        #endregion

        #region FEES
        int FeesCount();

        int LastFeeNumber();

        List<FeesViewModel> FeesList(out int rows, int? skip = default(int?), int? take = default(int?), string orderBy = "feedate", string dest = "desc", string search = "", string filters = "0,1");

        Task<Fees> GetFee(int id);        

        int? CreateFee(Fees fee, string userID);

        Task<int?> EditFee(Fees fee);

        Task<string> DeleteFee(int id);
        #endregion

        #region GENERALS
        List<FittersSelect> GetFittersList();

        List<ProjectsSelect> GetProjectsList();

        List<ProjectsSelect> GetProjectsList(int id);

        List<ApartmentsSelect> GetApartmentsList(int id);

        List<TasksSelect> GetTasksList(int id);

        List<CustomerSelect> GetCustomersList();

        List<BankAccounts> GetBankAccountsList();        
        #endregion
    }
}
