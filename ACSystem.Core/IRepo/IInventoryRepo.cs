﻿using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using ACSystem.Core.Models.ViewModels.Select;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ACSystem.Core.IRepo
{
    public interface IInventoryRepo
    {
        /// <summary>
        /// Liczba wierszy w zapytaniu
        /// </summary>
        int Count();

        /// <summary>
        /// Lista narzędzi
        /// </summary>
        /// <param name="rows">Liczba wierszy w zapytaniu</param>
        /// <param name="skip">O ile wierszy zapytanie ma przeskoczyć</param>
        /// <param name="take">Ilość wierszy do pobrania</param>
        /// <param name="orderBy">Kolumna, po której sortowana będzie tabela</param>
        /// <param name="dest">Kierunek sortowania</param>
        /// <param name="search">Słowa potrzebne do wyszukiwania</param>
        List<InventoryListViewModel> List(out int rows, int? skip = null, int? take = null, string orderBy = "name", string dest = "asc", string search = "", string filter = "0,1",
                                        string numer_ew_s = "", string numer_ew_t = "", string typ = "", string marka = "", DateTime? data = null,
                                        string magazyn = "", string posiada = "", string akcesoria = "");

        /// <summary>
        /// Pobiera dane narzędzia
        /// </summary>
        /// <param name="id">ID narzędzia</param>
        Task<Inventory> GetInventory(int id);

        /// <summary>
        /// Tworzy narzędzie
        /// </summary>
        /// <param name="inventory">Model z danymi narzędzia</param>
        int Create(Inventory inventory);

        /// <summary>
        /// Edytuje narzędzie
        /// </summary>
        /// <param name="inventory">Model z danymi narzędzia</param>
        /// <returns></returns>
        bool Edit(Inventory inventory);

        /// <summary>
        /// Usuwa narzędzie
        /// </summary>
        /// <param name="id">ID narzędzia</param>
        Task<string> Delete(int id);

        bool GiveInventory(int InventoryID, string FitterID, int? ProjectID);

        bool FreeInventory(int InventoryID, int? WarehouseID);

        bool SellInventory(int InventoryID, decimal SellPrice);

        List<FittersSelect> Fitters();

        /// <summary>
        /// Zapisuje dane
        /// </summary>
        void SaveChanges();

        IACSystemContext GetContext();
    }
}