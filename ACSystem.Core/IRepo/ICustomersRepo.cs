﻿using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACSystem.Core.IRepo
{
    public interface ICustomersRepo
    {
        /// <summary>
        /// Liczba wierszy w zapytaniu
        /// </summary>
        int Count();

        /// <summary>
        /// Lista klientów
        /// </summary>
        /// <param name="rows">Liczba wierszy w zapytaniu</param>
        /// <param name="skip">O ile wierszy zapytanie ma przeskoczyć</param>
        /// <param name="take">Ilość wierszy do pobrania</param>
        /// <param name="orderBy">Kolumna, po której sortowana będzie tabela</param>
        /// <param name="dest">Kierunek sortowania</param>
        /// <param name="search">Słowa potrzebne do wyszukiwania</param>
        List<CustomersListViewModel> List(out int rows, int? skip = null, int? take = null, string orderBy = "name", string dest = "asc", string search = "", int filter = -1);

        /// <summary>
        /// Pobiera dane klienta
        /// </summary>
        /// <param name="id">ID klienta</param>
        /// <returns></returns>
        Customers GetCustomerById(int id);

        /// <summary>
        /// Tworzy klienta
        /// </summary>
        /// <param name="customer">Model z danymi klienta</param>
        bool Create(Customers customer);

        /// <summary>
        /// Edytuje klienta
        /// </summary>
        /// <param name="customer">Model z danymi klienta</param>
        /// <returns></returns>
        bool Edit(Customers customer);

        /// <summary>
        /// Usuwa klienta
        /// </summary>
        /// <param name="id">ID Klienta</param>
        string Delete(int id);

        /// <summary>
        /// Tworzy Menedżera Projektu
        /// </summary>
        /// <param name="projectManager">Model z danymi Menedżera Projektu</param>
        /// <returns></returns>
        int ProjectManagerCreate(ProjectManagers projectManager);

        /// <summary>
        /// Edytuje Menedżera Projektu
        /// </summary>
        /// <param name="projectManager">Model z danymi Menedżera Projektu</param>
        bool ProjectManagerEdit(ProjectManagers projectManager);

        /// <summary>
        /// Usuwa Menedżera Projektu
        /// </summary>
        /// <param name="id">ID Menedżera Projektu</param>
        string ProjectManagerDelete(int id);
                
        /// <summary>
        /// Zwraca pełne dane Menedżera projektu
        /// </summary>
        /// <param name="id">ID Menedżera Projektu</param>
        ProjectManagers ProjectManagerByIdFull(int id);

        /// <summary>
        /// Zwraca dane Menedżera Projektu
        /// </summary>
        /// <param name="id">ID Menedżera Projektu</param>
        ProjectManagerViewModel ProjectManagerById(int id);

        /// <summary>
        /// Zapisuje dane
        /// </summary>
        void SaveChanges();
    }
}
