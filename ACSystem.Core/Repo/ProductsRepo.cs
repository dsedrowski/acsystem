﻿using ACSystem.Core.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACSystem.Core.Models;
using System.Threading.Tasks;
using System.Data.Entity;
using ACSystem.Core.Models.ViewModels;

namespace ACSystem.Core.Repo
{
    public class ProductsRepo : IProductsRepo
    {
        private readonly IACSystemContext _db;

        public ProductsRepo(IACSystemContext db)
        {
            _db = db;
        }

        public int Count()
        {
            return _db.Products.Count();
        }

        public Products Create(ProductsViewModel productVM)
        {
            var ppList = new List<Product_Project>();

            if (productVM.ConnectedProjects != null)
                foreach (var p in productVM.ConnectedProjects)
                {
                    var projectName = _db.Projects.Where(q => q.Id == p).Select(q => q.ProjectNumber).FirstOrDefault();

                    var pp = new Product_Project
                    {
                        ProjectID = p,
                        ProjectNumber = projectName,
                        ProductName = productVM.Name,
                        Price = productVM.Price,
                        PercentType1 = productVM.Type1,
                        PercentType2 = productVM.Type2,
                        OnInvoiceName = productVM.DescriptionSE,
                        NoExtra = productVM.NoExtra
                    };

                    ppList.Add(pp);
                }

            var product = new Products
            {
                Name = productVM.Name,
                DescriptionPL = productVM.DescriptionPL,
                DescriptionEN = productVM.DescriptionEN,
                DescriptionSE = productVM.DescriptionSE,
                Price = productVM.Price,
                Jm = productVM.Jm,
                JmEng = productVM.JmEng,
                JmSwe = productVM.JmSwe,
                ProductType = productVM.ProductType,
                IsActive = productVM.IsActive,
                ForAllProjects = productVM.ForAllProjects,
                NoExtra = productVM.NoExtra,
                ExpectedTime = productVM.ExpectedTime,
                TimePercent = productVM.TimePercent,
                Product_Project = ppList
            };

            var ph = new PercentHeight
            {
                Type1 = productVM.Type1,
                Type2 = productVM.Type2,
                Type3 = productVM.Type3,
                Type4 = productVM.Type4,
                Type5 = productVM.Type5,
                Type6 = productVM.Type6
            };

            try
            {
                _db.Products.Add(product);

                SaveChanges();

                ph.Id = product.Id;
                Create(ph);

                AddDefaultProductToProjects(product, ph);

                return product ;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }
        }

        public bool Create(PercentHeight ph)
        {
            try
            {
                _db.PercentHeight.Add(ph);

                SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
        }

        public async Task<string> Delete(int id)
        {
            var product = await GetProduct(id);

            try
            {
                _db.Products.Remove(product);
                SaveChanges();

                return "true;" + product.Name;
            }
            catch(Exception ex)
            {
                return "false;" + ex.Message;
            }
        }

        public bool Edit(PercentHeight ph)
        {
            try
            {
                _db.Entry(ph).State = EntityState.Modified;
                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
        }

        public bool Edit(Products product, List<int> projects)
        {
            try
            {
                var ppList = new List<Product_Project>();
                var ppList_DB = _db.Product_Project.Where(q => q.ProductID == product.Id).ToList();

                if (projects != null)
                {
                    foreach (var p in projects)
                    {
                        var projectName = _db.Projects.Where(q => q.Id == p).Select(q => q.ProjectNumber).FirstOrDefault();
                        
                        if (!ppList_DB.Any(q => q.ProjectID == p)) {

                            var pp = new Product_Project
                            {
                                ProjectID = p,
                                ProjectNumber = projectName,
                                ProductName = product.Name,
                                Price = product.Price,
                                PercentType1 = product.PercentHeight.Type1,
                                PercentType2 = product.PercentHeight.Type2,
                                OnInvoiceName = product.DescriptionSE,
                                NoExtra = product.NoExtra
                            };

                            ppList.Add(pp);
                        }    
                    }
                }

                foreach (Product_Project pp in ppList_DB) {
                    pp.NoExtra = product.NoExtra;
                    _db.Entry(pp).State = EntityState.Modified;
                }

                _db.Product_Project.AddRange(ppList);
                _db.Entry(product).State = EntityState.Modified;
                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
        }

        public async Task<PercentHeight> GetPercentHeight(int id)
        {
            return await _db.PercentHeight.FindAsync(id);
        }

        public async Task<Products> GetProduct(int id)
        {
            return await _db.Products.FindAsync(id);
        }

        public ProductsViewModel GetProductViewModel(int id)
        {
            return _db.Products.Where(q => q.Id == id).Include(q => q.PercentHeight).Select(q => new ProductsViewModel()
                                                                                                                    {
                                                                                                                        Id = q.Id,
                                                                                                                        Name = q.Name,
                                                                                                                        DescriptionPL = q.DescriptionPL,
                                                                                                                        DescriptionEN = q.DescriptionEN,
                                                                                                                        DescriptionSE = q.DescriptionSE,
                                                                                                                        Price = q.Price,
                                                                                                                        Jm = q.Jm,
                                                                                                                        JmEng = q.JmEng,
                                                                                                                        JmSwe = q.JmSwe,
                                                                                                                        Type1 = q.PercentHeight.Type1,
                                                                                                                        Type2 = q.PercentHeight.Type2,
                                                                                                                        Type3 = q.PercentHeight.Type3,
                                                                                                                        Type4 = q.PercentHeight.Type4,
                                                                                                                        Type5 = q.PercentHeight.Type5,
                                                                                                                        Type6 = q.PercentHeight.Type6,
                                                                                                                        ProductType = q.ProductType,
                                                                                                                        IsActive = q.IsActive,
                                                                                                                        InSystemID = q.InSystemID,
                                                                                                                        ForAllProjects = q.ForAllProjects,
                                                                                                                        ConnectedProjects = q.Product_Project.Select(x => x.ProjectID).ToList(),
                                                                                                                        Product_Prodject = q.Product_Project.ToList(),
                                                                                                                        Product_Group = q.ProductGroups_Main.ToList(),
                                                                                                                        Questions = q.Product_Questions.ToList(),
                                                                                                                        NoExtra = q.NoExtra,
                                                                                                                        ExpectedTime = q.ExpectedTime,
                                                                                                                        TimePercent = q.TimePercent
                                                                                                                    }).FirstOrDefault();
        }

        public List<ProductsListViewModel> List(out int rows, int? skip = default(int?), int? take = default(int?), string orderBy = "name", string dest = "asc", string search = "", string filter = "")
        {
            if (skip == null)
                skip = 0;

            if (take == null)
                take = 20;

            rows = 0;

            IQueryable<ProductsListViewModel> query = _db.Products.Include(q => q.PercentHeight).Select(q => new ProductsListViewModel()
                                                                                                                                {
                                                                                                                                    Id = q.Id,
                                                                                                                                    Name = q.Name,
                                                                                                                                    DescriptionPL = q.DescriptionPL,
                                                                                                                                    DescriptionEN = q.DescriptionEN,
                                                                                                                                    DescriptionSE = q.DescriptionSE,
                                                                                                                                    Price = q.Price,
                                                                                                                                    Jm = q.Jm,
                                                                                                                                    Type1 = q.PercentHeight.Type1,
                                                                                                                                    Type2 = q.PercentHeight.Type2,
                                                                                                                                    Type3 = q.PercentHeight.Type3,
                                                                                                                                    Type4 = q.PercentHeight.Type4,
                                                                                                                                    Type5 = q.PercentHeight.Type5,
                                                                                                                                    Type6 = q.PercentHeight.Type6,
                                                                                                                                    ProductType = q.ProductType,
                                                                                                                                    IsActive = q.IsActive
                                                                                                                                });
            IQueryable<ProductsListViewModel> queryOrderBy = null;
            IQueryable<ProductsListViewModel> queryWhere = null;
            IQueryable<ProductsListViewModel> querySkipTake = null;

            switch (orderBy)
            {
                case "name":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Name);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Name);
                    break;
                case "price":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Price);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Price);
                    break;
                default:
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Name);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Name);
                    break;
            }

            queryWhere = queryOrderBy.Where(p => p.Name.Contains(search));

            if (filter.Contains("0") == false)
                queryWhere = queryWhere.Where(q => q.IsActive);

            rows = queryWhere.Count();

            querySkipTake = queryWhere.Skip((int)skip).Take((int)take);

            return querySkipTake.ToList();
        }

        public void AddDefaultProductToProjects(Products pr, PercentHeight ph)
        {
            if (!pr.ForAllProjects)
                return;

            var projects = _db.Projects.ToList();

            var projectProducts = new List<Product_Project>();

            projects.ForEach(x =>
            {
                if (!x.Product_Project.Any(q => q.ProductID == pr.Id))
                {
                    projectProducts.Add(new Product_Project
                    {
                        ProjectID = x.Id,
                        ProductID = pr.Id,
                        ProjectNumber = x.ProjectNumber,
                        ProductName = pr.Name,
                        Price = pr.Price,
                        PercentType1 = ph.Type1,
                        PercentType2 = ph.Type2,
                        OnInvoiceName = pr.DescriptionSE
                    });
                }
            });

            _db.Product_Project.AddRange(projectProducts);

            try
            {
                _db.SaveChanges();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }
    }
}