﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using ACSystem.Core.Models.ViewModels.Select;
using ACSystem.Core.Service;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ACSystem.Core.Repo
{
    public class OfficeRepo : IOfficeRepo
    {
        private readonly IACSystemContext _db;

        public OfficeRepo(IACSystemContext db)
        {
            _db = db;
        }

        public ACSystemContext GetContext()
        {
            return _db as ACSystemContext;
        }

        #region PAYROLL
        public int Count()
        {
            return _db.Payroll.Count();
        }

        public List<PayrollListViewModel> List(out int rows, int? skip = default(int?), int? take = default(int?), string orderBy = "date", string dest = "desc", string search = "", string filters = "0,1,2")
        {
            if (skip == null)
                skip = 0;

            if (take == null)
                take = 20;

            rows = 0;

            if (string.IsNullOrEmpty(filters))
                return null;

            List<int> filter = new List<int>();

            foreach (var fl in filters.Split(','))
            {
                filter.Add(Convert.ToInt32(fl));
            }


            IQueryable<PayrollListViewModel> query = _db.Payroll.Where(q => q.Active == true)
                                                         .Select(c => new PayrollListViewModel
                                                         {
                                                             Id = c.Id,
                                                             Number = c.Number,
                                                             NumberString = SqlFunctions.StringConvert((decimal)c.Number),
                                                             CreateDate = c.CreateDate,
                                                             Sum = SqlFunctions.StringConvert((decimal)c.Sum),
                                                             Status = c.Status
                                                         });

            IQueryable<PayrollListViewModel> queryOrderBy = null;
            List<PayrollListViewModel> queryWhere = null;
            IQueryable<PayrollListViewModel> queryFilter = null;
            IQueryable<PayrollListViewModel> querySkipTake = null;

            switch (orderBy)
            {
                case "date":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.CreateDate);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.CreateDate);
                    break;
                case "number":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Number);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Number);
                    break;
                case "sum":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Sum);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Sum);
                    break;
                case "status":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Status);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Status);
                    break;
                default:
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.CreateDate);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.CreateDate);
                    break;
            }

            queryWhere = queryOrderBy.Where(p => p.NumberString.Contains(search) || p.Sum.Contains(search)).ToList();

            var fittersList = _db.Fitters.ToList();

            foreach (var l in queryWhere)
            {
                decimal sumMinusHours = 0, toPayLeft = 0;
                l.SumPlnMinusHours = 0;

                var payroll = _db.Payroll.Find(l.Id);

                var fitters = payroll.Items.OrderBy(q => q.FitterName).GroupBy(q => q.FitterName).Select(q => q.Key);

                foreach (var fitter in fitters)
                {
                    var elems = payroll.Items.Where(q => q.FitterName == fitter && q.Active.HasValue && q.Active.Value);

                    var fitterID = "0";

                    if (elems != null && elems.Count() > 0)
                        fitterID = elems.FirstOrDefault().FitterID;

                    var hourlyRate = elems.FirstOrDefault()?.HourlyRate ?? 0;

                    var fitt = fittersList.FirstOrDefault(q => q.User.FullName == fitter);
                    var hours = elems.Sum(q => q.WorkTime);
                    var hoursLeft = elems.Where(q => q.Status != PayItemStatus.Wyplacone).Sum(q => q.WorkTime);
                    var hoursLeftCost = hoursLeft * hourlyRate;
                    var hourCost = hours * hourlyRate;
                    var hourCostPLN = (double)hourCost * payroll.CurrencyRate2;
                    var hourLeftCostPLN = (double)hoursLeftCost * payroll.CurrencyRate2;

                    sumMinusHours += (elems.Sum(q => q.ToPay) - hourCost) - elems.Sum(q => q.Fees);
                    l.SumPlnMinusHours += (elems.Sum(q => q.ToPayPLN) - (decimal)hourCostPLN) - elems.Sum(q => q.FeesPLN);
                    toPayLeft += (elems.Where(q => q.Status != PayItemStatus.Wyplacone).Sum(q => (q.ToPayPLN))) - (decimal)hourLeftCostPLN;
                    toPayLeft -= (elems.Where(q => q.Status != PayItemStatus.Wyplacone).Sum(q => q.FeesPLN));
                }

                l.SumMinusHours = Math.Ceiling(sumMinusHours).ToString(CultureInfo.InvariantCulture) + " SEK";

                var items = payroll.Items;

                var sum = items.Where(q => q.Status != PayItemStatus.Wyplacone);

                l.PayLeft = Math.Round(toPayLeft, 2) + " PLN";

                if (items.Any(q => q.Status == PayItemStatus.Wyplacone) &&
                    items.Any(q => q.Status != PayItemStatus.Wyplacone))
                    l.Status = PayStatus.CzesciowoWyplacone;
                else if (items.Any(q => q.Status == PayItemStatus.DoWyplaty) == false &&
                         items.Any(q => q.Status == PayItemStatus.Wstrzymane) == false)
                    l.Status = PayStatus.Zamkniete;
                else
                    l.Status = PayStatus.Gotowe;
            }

            queryFilter = queryWhere.Where(p => filter.Contains((int)p.Status)).AsQueryable();

            rows = queryFilter.Count();

            querySkipTake = queryFilter.Skip((int)skip).Take((int)take);

            return querySkipTake.ToList();
        }

        public async Task<Payroll> GetPayroll(int id)
        {
            var payroll = await _db.Payroll.FindAsync(id);

            return payroll;
        }

        public int LastPayrollNumber()
        {
            var payroll = _db.Payroll.Where(q => q.Active == true).OrderByDescending(q => q.Number).FirstOrDefault();

            return (payroll != null) ? payroll.Number : 1;
        }

        [Obsolete]
        public int? CreatePayroll(out Dictionary<string, string> hoursNotReported, int number, DateTime toDate, string fitterID = "", int? ProjectID = null)
        {
            var currencyRate = NBPApi.GetRate("SEK").rates.OrderByDescending(q => q.effectiveDate).FirstOrDefault();

            var viewToCheck = new view_ListaPlac();
            var taskToCheck = new Tasks();
            var feesToCheck = new List<Fees>();
            var taskFitterToCheck = new Task_Fitter();
            decimal? workTimetoCheck = null;
            decimal? toPayCheck = null;
            hoursNotReported = new Dictionary<string, string>();
            var localHoursNotReported = new Dictionary<string, string>();

            try
            {
                IQueryable<view_ListaPlac> allList;

                if (!string.IsNullOrEmpty(fitterID))
                    allList = view_ListaPlac.AllFinishedToDate(toDate).Where(q => q.UzytkownikID == fitterID);
                else if (ProjectID.HasValue)
                    allList = view_ListaPlac.AllFinishedToDate(toDate).Where(q => q.ProjektID == ProjectID);
                else
                    allList = view_ListaPlac.AllFinishedToDate(toDate);

                if (allList == null || !allList.Any()) return null;

                var allUsers = allList.GroupBy(e => e.UzytkownikID).Select(q => q.Key).ToList();
                //var allUsersList = _db.ACSystemUser.Where(e => allUsers.Contains(e.Id)).ToList();


                //allUsersList.ForEach(x =>
                //{
                //    var fiveDaysPast = DateTime.Now.AddDays(-5);
                //    var startDate = DateTime.Parse("2021-02-01");
                //    var fitterHours = _db.Fitter_Hours.Where(q => q.FitterID == x.Id && q.Date >= startDate && q.Date <= fiveDaysPast && !q.Fitter.LockoutEnabled).ToList().Where(q => q.Hours > 0 || q.FreeDay || q.Date.DayOfWeek == DayOfWeek.Saturday || q.Date.DayOfWeek == DayOfWeek.Sunday);

                //    if (!fitterHours.Any() && !localHoursNotReported.ContainsKey(x.Id))
                //    {
                //        localHoursNotReported.Add(x.Id, x.FullName);
                //    }
                //});

                //hoursNotReported = localHoursNotReported;

                List<Fees> fees = _db.Fees.Where(q => q.Status == FeeStatus.DoPotracenia && allUsers.Contains(q.FitterID) && EntityFunctions.TruncateTime(q.FeeDate) <= toDate).ToList();

                List<RentAndLoan> rentAndLoan = new List<RentAndLoan>();

                if (string.IsNullOrEmpty(fitterID))
                    rentAndLoan = _db.RentAndLoan.Where(q => q.LeftAmount > 0 /*&& allUsers.Contains(q.FitterID)*/ && q.StartDate <= EntityFunctions.TruncateTime(DateTime.Now)).ToList();
                else
                    rentAndLoan = _db.RentAndLoan.Where(q => q.LeftAmount > 0 && q.FitterID == fitterID && q.StartDate <= EntityFunctions.TruncateTime(DateTime.Now)).ToList();

                var sum = (allList.Count() > 0) ? allList.Sum(q => q.DoZaplaty).Value : 0;

                var payroll = new Payroll
                {
                    Number = number,
                    CreateDate = DateTime.Now,
                    Sum = sum,
                    SumPLN = sum * (decimal)(currencyRate.mid - (double)0.0100),
                    CurrencyRate = (decimal)Math.Round((currencyRate.mid - (double)0.0100), 4),
                    CurrencyRate2 = (currencyRate.mid - (double)0.0100),
                    Status = PayStatus.Gotowe
                };

                _db.Payroll.Add(payroll);
                _db.SaveChanges();

                var payrollItems = new List<PayrollItems>();
                var feesList = new List<Fees>();

                if (fees.Any())
                {
                    payrollItems.AddRange(fees.Select(fee => new PayrollItems
                    {
                        PayrollID = payroll.Id,
                        FitterID = fee.FitterID,
                        FitterName = $"{fee.Fitter?.User?.FullName}",
                        ProjectID = fee.Task?.Apartment?.ProjectId ?? 0,
                        ProjectNumber = fee.Task?.Apartment?.Project?.ProjectNumber,
                        ApartmentID = fee.Task?.ApartmentId ?? 0,
                        ApartmentNumber = fee.Task?.Apartment.NumberOfApartment,
                        ProductID = fee.Task?.ProductId ?? 0,
                        ProductName = fee.Task?.Product?.Name + fee.Task?.TaskSufix,
                        TaskID = fee.TaskID,
                        TaskEnd = fee.Task?.EndDate ?? DateTime.Now,
                        WorkDone = 0,
                        WorkTime = 0,
                        ToPay = 0,
                        ToPayPLN = 0,
                        Fees = fee.FeeAmount,
                        FeesPLN = fee.FeeAmount * (decimal)(currencyRate.mid - (double)0.0100),
                        Status = PayItemStatus.DoWyplaty,
                        Active = false,
                        FeeDescription = fee.Description,
                        FeeID = fee.ID
                    }));

                    feesList.AddRange(fees);
                }

                if (rentAndLoan.Any())
                {
                    var rents = new List<PayrollItems>();

                    foreach (var rent in rentAndLoan)
                    {
                        if (allUsers.Contains(rent.FitterID) && (rent.Frequency == InstallmentFrequency.PerMonth && (rent.Installments.FirstOrDefault() == null || rent.Installments.OrderByDescending(q => q.Timestamp).FirstOrDefault().Timestamp.ToString("yyyyMM") != DateTime.Now.ToString("yyyyMM"))) || rent.Frequency == InstallmentFrequency.PerPayroll)
                        {
                            var daysFromLast = (rent.Installments != null && rent.Installments.Any()) ? (DateTime.Now - rent.Installments.OrderByDescending(q => q.Timestamp).FirstOrDefault().Timestamp).TotalDays : 1;

                            decimal rentSek = 0;
                            decimal rentPln = 0;

                            if (rent.Currency == Currency.SEK)
                            {
                                rentSek = rent.InstallmentAmount + ((decimal)rent.InterestRate * (decimal)(rent.LeftAmount / 365) * (decimal)((daysFromLast > 0) ? daysFromLast : 1));
                                rentPln = (rent.InstallmentAmount + ((decimal)rent.InterestRate * (decimal)(rent.LeftAmount / 365) * (decimal)((daysFromLast > 0) ? daysFromLast : 1))) * (decimal)(currencyRate.mid - (double)0.0100);
                            }
                            else
                            {
                                rentSek = (rent.InstallmentAmount + ((decimal)rent.InterestRate * (decimal)(rent.LeftAmount / 365) * (decimal)((daysFromLast > 0) ? daysFromLast : 1))) / (decimal)(currencyRate.mid - (double)0.0100);
                                rentPln = (rent.InstallmentAmount + ((decimal)rent.InterestRate * (decimal)(rent.LeftAmount / 365) * (decimal)((daysFromLast > 0) ? daysFromLast : 1)));
                            }

                            rents.Add(new PayrollItems
                            {
                                PayrollID = payroll.Id,
                                FitterID = rent.FitterID,
                                FitterName = $"{rent.Fitter.User.FullName}",
                                ProjectID = 0,
                                ProjectNumber = "",
                                ApartmentID = 0,
                                ApartmentNumber = "",
                                ProductID = 0,
                                ProductName = "",
                                TaskID = 0,
                                TaskEnd = DateTime.Now,
                                WorkDone = 0,
                                WorkTime = 0,
                                ToPay = 0,
                                ToPayPLN = 0,
                                Fees = rentSek,
                                FeesPLN = rentPln,
                                Status = PayItemStatus.DoWyplaty,
                                Active = false,
                                FeeDescription = rent.Description,
                                RentAndLoanID = rent.ID
                            });
                        }
                    }

                    payrollItems.AddRange(rents);
                }

                foreach (var a in allList)
                {
                    viewToCheck = a;

                    var taskFitter = _db.Task_Fitter.Where(q => q.TaskId == a.ZadanieID && q.IsActive && (q.Status == TaskStatus.Finished || q.Status == TaskStatus.FinishedWithFaults || q.Status == TaskStatus.Stoped)).OrderByDescending(q => q.Timestamp).FirstOrDefault();
                    taskFitterToCheck = taskFitter;

                    if (taskFitter == null) continue;

                    var fitter = _db.Fitters.Find(a.UzytkownikID);

                    //this.SupervisorAddsService(supervisorAdds, projects, a, fitter, taskFitter.Task);

                    var taskEndDate = taskFitter.Timestamp;

                    feesToCheck = fees;

                    var task = _db.Tasks.Find(a.ZadanieID);
                    taskToCheck = task;

                    if (task == null) continue;

                    if (task.IsInFees) continue;

                    if (task.Blocked) continue;

                    var workTime = (decimal)task.Task_Fitter.Where(q => q.FitterId == a.UzytkownikID && q.IsActive).Sum(q => q.WorkTime) / (decimal)60;//task.Task_Fitter.Sum(q => q.WorkTime) / 60;
                    workTimetoCheck = workTime;

                    if (a.DoZaplaty == null) continue;
                    if (fitter == null) continue;

                    var toPay = (task.IsHourlyPay) ? (workTime * fitter.HourlyRate) : a.DoZaplaty.Value;
                    toPayCheck = toPay;

                    var item = new PayrollItems
                    {
                        PayrollID = payroll.Id,
                        FitterID = a.UzytkownikID,
                        FitterName = $"{a.Nazwisko} {a.Imie}",
                        ProjectID = a.ProjektID,
                        ProjectNumber = a.NumerProjektu,
                        ApartmentID = a.ApartamentID,
                        ApartmentNumber = a.NumerApartamentu,
                        ProductID = a.ProduktID,
                        ProductName = $"{a.NazwaProduktu} {task.TaskSufix}",
                        TaskID = a.ZadanieID,
                        TaskEnd = taskEndDate,
                        ToPay = toPay,
                        ToPayPLN = toPay * (decimal)(currencyRate.mid - (double)0.01),
                        WorkDone = task.Task_Fitter.Where(q => q.FitterId == a.UzytkownikID && q.IsActive && q.InPayroll == false).Sum(q => q.WorkDone),
                        WorkDoneUnit = task.Product.Jm,
                        WorkTime = 0,//workTime,
                        IsHourlyPay = task.IsHourlyPay,
                        Fees = 0,
                        FeesPLN = 0,
                        Status = PayItemStatus.DoWyplaty,
                        Active = false
                    };

                    payrollItems.Add(item);
                }
                List<Fitter_Hours> hours = new List<Fitter_Hours>();

                if (string.IsNullOrEmpty(fitterID))
                    hours = _db.Fitter_Hours.Where(q => q.PayrollID.HasValue == false && q.FreeDay == false && q.Date <= toDate).ToList();
                else
                    hours = _db.Fitter_Hours.Where(q => q.PayrollID.HasValue == false && q.FreeDay == false && q.Date <= toDate && q.FitterID == fitterID).ToList();

                foreach (var h in hours.GroupBy(q => q.FitterID))
                {
                    if (h.Sum(q => q.Hours) == 0)
                        continue;

                    if (!payrollItems.Any(e => e.FitterID.Equals(h.Key)))
                        continue;

                    var hoursItem = new PayrollItems
                    {
                        PayrollID = payroll.Id,
                        FitterID = h.Key,
                        FitterName = $"{h.FirstOrDefault().Fitter.FullName}",
                        ProjectID = null,
                        ProjectNumber = "",
                        ApartmentID = null,
                        ApartmentNumber = "",
                        ProductID = null,
                        ProductName = "",
                        TaskID = null,
                        TaskEnd = h.OrderByDescending(q => q.Date).FirstOrDefault().Date,
                        ToPay = 0,
                        ToPayPLN = 0,
                        WorkDone = 0,
                        WorkDoneUnit = "",
                        WorkTime = h.Sum(q => q.Hours),
                        IsHourlyPay = false,
                        Fees = 0,
                        FeesPLN = 0,
                        Status = PayItemStatus.DoWyplaty,
                        Active = false
                    };

                    payrollItems.Add(hoursItem);
                }

                _db.PayrollItems.AddRange(payrollItems);
                var itemsSuccess = _db.SaveChanges();

                if (itemsSuccess > 0)
                {
                    //foreach (var fee in feesList)
                    //{
                    //    fee.Status = FeeStatus.Potracone;
                    //    fee.PayrollID = payroll.Id;

                    //    await EditFee(fee.ID, FeeStatus.Potracone, payroll.Id);
                    //}

                    //foreach (var task in tasksList)
                    //{
                    //    //task.Payroll = true;
                    //    _db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                    //}

                    sum = payroll.Sum - feesList.Sum(q => q.FeeAmount);

                    payroll.Sum = sum;
                    payroll.SumPLN = sum * (decimal)(currencyRate.mid - (double)0.01);
                    payroll.Active = false;

                    _db.Entry(payroll).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();

                }

                return payroll.Id;
            }
            catch (DbEntityValidationException ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }
        }

        public void SupervisorAddsService(List<SupervisorAdds> list, List<Projects> projects, PayrollItems data, Fitters fitter, Tasks task)
        {
            if (task.Production != TaskProduction.Developer || !data.ProductID.HasValue)
                return;

            if (data.Project != null)
            {
                Product_Project productInProject = data.Project.Product_Project.FirstOrDefault(q => q.ID == task.ProductId);

                if (productInProject != null && productInProject.NoExtra)
                    return;
            }

            Projects project = null;
            if (projects.Any(q => q.Id == data.ProjectID) == false)
            {
                project = _db.Projects.Find(data.ProjectID);
                projects.Add(project);
            }
            else
                project = projects.FirstOrDefault(q => q.Id == data.ProjectID);

            var supervisorID = !string.IsNullOrEmpty(project.SupervisorID) ? project.SupervisorID : fitter.User.Crew_Members.Any() ? fitter.User.Crew_Members.FirstOrDefault().SupervisorID : null;

            if (supervisorID == null)
                return;

            SupervisorAdds supervisor = null;
            var supervisorExist = list.Any(q => q.SupervisorID == supervisorID && q.ProjectID == data.ProjectID);
            if (supervisorExist)
                supervisor = list.FirstOrDefault(q => q.SupervisorID == supervisorID && q.ProjectID == data.ProjectID);
            else
                supervisor = new SupervisorAdds { SupervisorID = supervisorID, ProjectID = data.ProjectID.Value, Supervisor = _db.ACSystemUser.Find(supervisorID) };

            var taskValue = task.ProductPrice * data.WorkDone;
            var companyValue = taskValue - data.ToPay;

            var companyPercent = !string.IsNullOrEmpty((project.SupervisorID)) ? (project.CompanyPercent.HasValue ? project.CompanyPercent.Value / 100 : 0) : (supervisor.Supervisor.CompanyPercent.HasValue ? supervisor.Supervisor.CompanyPercent.Value / 100 : 0);
            var fitterPercent = !string.IsNullOrEmpty(project.SupervisorID) ? (project.FitterPercent.HasValue ? project.FitterPercent.Value / 100 : 0) : (supervisor.Supervisor.FitterPercent.HasValue ? supervisor.Supervisor.FitterPercent.Value / 100 : 0);

            var takeFromCompany = companyValue * companyPercent;
            var takeFromFitter = (data.ToPay) * fitterPercent;

            supervisor.Amount = (supervisorExist) ? supervisor.Amount + takeFromCompany + takeFromFitter : takeFromCompany + takeFromFitter;

            data.ToPay = data.ToPay - takeFromFitter;

            if (supervisorExist == false)
                list.Add(supervisor);
        }

        //public async Task<int?> CreatePayroll(int number, DateTime toDate, string fitterID)
        //{
        //    var currencyRate = NBPApi.GetRate("SEK").rates.OrderByDescending(q => q.effectiveDate).FirstOrDefault();

        //    try
        //    {
        //        var allList = view_ListaPlac.AllFinishedToDate(toDate).Where(q => q.UzytkownikID == fitterID);

        //        if (allList == null || allList.Count() <= 0)
        //            return null;

        //        var sum = (allList.Count() > 0) ? allList.Sum(q => q.DoZaplaty).Value : 0;

        //        var payroll = new Payroll
        //        {
        //            Number = number,
        //            CreateDate = DateTime.Now,
        //            Sum = sum,
        //            SumPLN = sum * (decimal)(currencyRate.mid - (double)0.0100),
        //            CurrencyRate2 = (currencyRate.mid - (double)0.0100),
        //            Status = PayStatus.Gotowe
        //        };

        //        _db.Payroll.Add(payroll);
        //        await _db.SaveChangesAsync();

        //        var payrollItems = new List<PayrollItems>();
        //        var feesList = new List<Fees>();
        //        var tasksList = new List<Tasks>();

        //        foreach (var a in allList)
        //        {
        //            var taskEndDate = _db.Task_Fitter.Where(q => q.TaskId == a.ZadanieID && (q.Status == TaskStatus.Finished || q.Status == TaskStatus.FinishedWithFaults || q.Status == TaskStatus.Stoped)).OrderByDescending(q => q.Timestamp).FirstOrDefault();

        //            var fees = _db.Fees.Where(q => q.TaskID == a.ZadanieID && q.FitterID == a.UzytkownikID && q.Status == FeeStatus.DoPotracenia).ToList();

        //            var task = _db.Tasks.Find(a.ZadanieID);

        //            tasksList.Add(task);

        //            if (fees != null && fees.Count() > 0)
        //            {
        //                foreach (var fee in fees)
        //                {
        //                    var feeItem = new PayrollItems
        //                    {
        //                        PayrollID = payroll.Id,
        //                        FitterID = a.UzytkownikID,
        //                        FitterName = $"{a.Nazwisko} {a.Imie}",
        //                        ProjectID = a.ProjektID,
        //                        ProjectNumber = a.NumerProjektu,
        //                        ApartmentID = a.ApartamentID,
        //                        ApartmentNumber = a.NumerApartamentu,
        //                        ProductID = a.ProduktID,
        //                        ProductName = a.NazwaProduktu + task.TaskSufix,
        //                        TaskEnd = taskEndDate.Timestamp,
        //                        WorkDone = 0,
        //                        WorkTime = 0,
        //                        ToPay = 0,
        //                        ToPayPLN = 0,
        //                        Fees = fee.FeeAmount,
        //                        FeesPLN = fee.FeeAmount * (decimal)(currencyRate.mid - (double)0.0100),
        //                        Status = PayItemStatus.DoWyplaty
        //                    };

        //                    payrollItems.Add(feeItem);
        //                }

        //                feesList.AddRange(fees);
        //            }

        //            var fitter = _db.Fitters.Find(a.UzytkownikID);
        //            var workTime = task.Task_Fitter.Where(q => q.FitterId == a.UzytkownikID).Sum(q => q.WorkTime) / 60;//task.Task_Fitter.Sum(q => q.WorkTime) / 60;
        //            var toPay = (task.IsHourlyPay) ? (workTime * fitter.HourlyRate) : a.DoZaplaty.Value;

        //            var item = new PayrollItems
        //            {
        //                PayrollID = payroll.Id,
        //                FitterID = a.UzytkownikID,
        //                FitterName = $"{a.Nazwisko} {a.Imie}",
        //                ProjectID = a.ProjektID,
        //                ProjectNumber = a.NumerProjektu,
        //                ApartmentID = a.ApartamentID,
        //                ApartmentNumber = a.NumerApartamentu,
        //                ProductID = a.ProduktID,
        //                ProductName = $"{a.NazwaProduktu} {task.TaskSufix}",
        //                TaskEnd = taskEndDate.Timestamp,
        //                ToPay = toPay,
        //                ToPayPLN = toPay * (decimal)(currencyRate.mid - (double)0.01),
        //                WorkDone = task.Task_Fitter.Sum(q => q.WorkDone),
        //                WorkDoneUnit = task.Product.Jm,
        //                WorkTime = workTime,
        //                IsHourlyPay = task.IsHourlyPay,
        //                Fees = 0,
        //                FeesPLN = 0,
        //                Status = PayItemStatus.DoWyplaty
        //            };

        //            payrollItems.Add(item);
        //        }

        //        _db.PayrollItems.AddRange(payrollItems);
        //        var itemsSuccess = await _db.SaveChangesAsync();

        //        if (itemsSuccess > 0)
        //        {
        //            foreach (var fee in feesList)
        //            {
        //                fee.Status = FeeStatus.Potracone;
        //                fee.PayrollID = payroll.Id;

        //                await EditFee(fee.ID, FeeStatus.Potracone, payroll.Id);
        //            }

        //            foreach (var task in tasksList)
        //            {
        //                task.Payroll = true;
        //                _db.Entry(task).State = System.Data.Entity.EntityState.Modified;
        //            }

        //            sum = payroll.Sum - feesList.Sum(q => q.FeeAmount);

        //            payroll.Sum = sum;
        //            payroll.SumPLN = sum * (decimal)(currencyRate.mid - (double)0.01);

        //            _db.Entry(payroll).State = System.Data.Entity.EntityState.Modified;
        //            _db.SaveChanges();

        //        }

        //        return payroll.Id;
        //    }
        //    catch (DbEntityValidationException ex)
        //    {
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        public async Task<string> DeletePayroll(int id)
        {
            var payroll = await GetPayroll(id);

            if (payroll == null)
                return "false;Pusto";

            var fees = _db.Fees.Where(q => q.PayrollID == payroll.Id).ToList();
            var rents = _db.Installment.Where(q => q.PayrollID == payroll.Id).ToList();
            var hours = _db.Fitter_Hours_Project.Where(q => q.PayrollID == payroll.Id).ToList();

            foreach (var p in payroll.Items)
            {
                var allActivities = _db.Task_Fitter.Where(q => q.TaskId == p.TaskID && q.FitterId == p.FitterID).ToList();

                foreach (var aa in allActivities)
                {
                    aa.InPayroll = false;
                    _db.Entry(aa).State = System.Data.Entity.EntityState.Modified;
                }
            }

            foreach (var fee in fees)
            {
                fee.Status = FeeStatus.DoPotracenia;
                fee.PayrollID = null;
                fee.TakeDate = null;

                _db.Entry(fee).State = System.Data.Entity.EntityState.Modified;
            }

            foreach (var hour in hours)
            {
                hour.PayrollID = null;
                _db.Entry(hour).State = System.Data.Entity.EntityState.Modified;
            }

            foreach (var rent in rents)
            {
                var rentAndLoan = rent.RentAndLoan;

                rentAndLoan.LeftAmount = rentAndLoan.LeftAmount + rent.ChargeAmount;

                _db.Installment.Remove(rent);
            }

            try
            {
                var result = _db.Payroll.Remove(payroll);

                await _db.SaveChangesAsync();

                return "true;" + payroll.Number;
            }
            catch (Exception ex)
            {
                return $"false;{ex.Message}";
            }
        }
        #endregion

        #region INVOICE

        public int InvoicesCount()
        {
            return _db.Invoice.Count();
        }

        public int LastInvoiceNumber()
        {
            var invoice = _db.Invoice.Where(q => q.Complete == true).OrderByDescending(q => q.InvoiceNumber).FirstOrDefault();

            return (invoice != null) ? invoice.InvoiceNumber : 1;
        }

        public List<InvoiceViewModel> InvoicesList(out int rows, int? skip = default(int?), int? take = default(int?), string orderBy = "invoicedate", string dest = "desc", string search = "", string filters = "0,1,2")
        {
            if (skip == null)
                skip = 0;

            if (take == null)
                take = 20;

            rows = 0;

            if (string.IsNullOrEmpty(filters))
                return null;

            List<int> filter = new List<int>();

            foreach (var fl in filters.Split(','))
            {
                filter.Add(Convert.ToInt32(fl));
            }


            IQueryable<InvoiceViewModel> query = _db.Invoice
                                                         .Where(q => q.Complete == true)
                                                         .Select(c => new InvoiceViewModel
                                                         {
                                                             ID = c.ID,
                                                             InvoiceNumber = c.InvoiceNumber,
                                                             ProjectNumber = c.Project.ProjectNumber,
                                                             CustomerName = c.Customer.Name,
                                                             Total = c.Amount,
                                                             InvoiceDate = c.InvoiceDate,
                                                             PaymentTerm = c.PaymentTerm,
                                                             Status = c.Status,
                                                             URL = c.FortnoxUrl
                                                         });

            IQueryable<InvoiceViewModel> queryOrderBy = null;
            IQueryable<InvoiceViewModel> queryWhere = null;
            IQueryable<InvoiceViewModel> queryFilter = null;
            IQueryable<InvoiceViewModel> querySkipTake = null;

            switch (orderBy)
            {
                case "invoicedate":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.InvoiceDate);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.InvoiceDate);
                    break;
                case "paymentterm":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.PaymentTerm);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.PaymentTerm);
                    break;
                case "number":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.InvoiceNumber);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.InvoiceNumber);
                    break;
                case "fortnox":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.URL);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.URL);
                    break;
                case "customer":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.CustomerName);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.CustomerName);
                    break;
                case "project":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProjectNumber);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProjectNumber);
                    break;
                case "total":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Total);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Total);
                    break;
                case "status":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Status);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Status);
                    break;
                default:
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.InvoiceDate);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.InvoiceDate);
                    break;
            }

            queryWhere = queryOrderBy.Where(p => p.ProjectNumber.Contains(search) || p.CustomerName.Contains(search));

            queryFilter = queryWhere.Where(p => filter.Contains((int)p.Status));

            rows = queryFilter.Count();

            querySkipTake = queryFilter.Skip((int)skip).Take((int)take);

            return querySkipTake.ToList();
        }

        public async Task<Invoice> GetInvoice(int id)
        {
            return await _db.Invoice.FindAsync(id);
        }

        public async Task<int?> CreateInvoice(Invoice invoice)
        {
            try
            {
                _db.Invoice.Add(invoice);

                await _db.SaveChangesAsync();
            }
            catch (DbEntityValidationException ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }

            return invoice.ID;
        }

        public async Task<int?> EditInvoice(Invoice invoice)
        {
            try
            {
                _db.Entry(invoice).State = System.Data.Entity.EntityState.Modified;

                await _db.SaveChangesAsync();
            }
            catch (DbEntityValidationException ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }

            return invoice.ID;
        }

        public async Task<string> DeleteInvoice(int id)
        {
            var invoice = await GetInvoice(id);

            if (invoice == null)
                return "false;Pusto";

            foreach (var elm in invoice.Elements)
            {
                if (elm.Invoice_AddsID.HasValue)
                {
                    var adds = _db.Invoice_Adds.Find(elm.Invoice_AddsID);

                    if (adds == null)
                        continue;

                    adds.InvoiceID = null;
                    _db.Entry(adds).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    var task = elm.Task;

                    if (task == null)
                        continue;

                    task.Invoice = false;

                    _db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                }
            }

            try
            {
                var result = _db.Invoice.Remove(invoice);

                await _db.SaveChangesAsync();

                return "true;" + invoice.InvoiceNumber;
            }
            catch (Exception ex)
            {
                return $"false;{ex.Message}";
            }
        }

        public async Task<string> DeleteInvoiceElement(int id)
        {
            var element = _db.InvoiceElements.Find(id);

            if (element == null)
                return "false;Pusto";

            var task = element.Task;
            task.Invoice = false;

            _db.Entry(task).State = System.Data.Entity.EntityState.Modified;

            try
            {
                var result = _db.InvoiceElements.Remove(element);

                await _db.SaveChangesAsync();

                return "true;" + id;
            }
            catch (Exception ex)
            {
                return $"false;{ex.Message}";
            }
        }

        public async Task<string> DeleteInvoiceElementByApartment(string apartmentNumber, int invoiceID)
        {
            var elements = _db.InvoiceElements.Where(q => q.ApartmentNumber == apartmentNumber && q.InvoiceID == invoiceID);

            if (elements == null || elements.Count() == 0)
                return "true;Pusto";

            foreach (var element in elements)
            {
                var task = element.Task;
                task.Invoice = false;

                _db.Entry(task).State = System.Data.Entity.EntityState.Modified;
            }

            try
            {
                var result = _db.InvoiceElements.RemoveRange(elements);

                await _db.SaveChangesAsync();

                return "true;" + invoiceID;
            }
            catch (Exception ex)
            {
                return $"false;{ex.Message}";
            }
        }

        public async Task<bool> SaveInvoice(string ids)
        {
            var idSplit = ids.Split(';');

            var lastId = LastInvoiceNumber() + 1;

            foreach (var idd in idSplit)
            {
                var id = int.Parse(idd);

                var invoice = _db.Invoice.Find(id);

                if (invoice == null)
                    continue;

                invoice.Complete = true;
                invoice.InvoiceNumber = lastId;
                invoice.Amount = _db.InvoiceElements.Where(q => q.InvoiceID == id).ToList().Sum(q => q.Sum);

                foreach (var element in invoice.Elements)
                {
                    var task = element.Task;
                    task.Invoice = true;

                    _db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                }

                _db.Database.ExecuteSqlCommand($"UPDATE InvoiceElements SET Complete = 1 WHERE InvoiceID = '{id}'");

                lastId++;
            }

            try
            {
                await _db.SaveChangesAsync();

                return true;
            }
            catch (DbEntityValidationException ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }

        }

        public List<InvoiceElements> GetInvoiceElements(int id)
        {
            return _db.InvoiceElements.Where(q => q.InvoiceID == id).ToList();
        }

        public List<InvoiceElements> GetInvoiceElements(List<int> id)
        {
            var elems = new List<InvoiceElements>();

            foreach (var i in id)
            {
                var e = _db.InvoiceElements.Where(q => q.InvoiceID == i).ToList();
                elems.AddRange(e);
            }

            return elems;
        }

        public int CustomerPaymentTerm(int id)
        {
            return _db.Customers.Find(id).PaymentTerm;
        }

        public DateTime LastInvoice(int customerID, int projectID)
        {
            var lastDate = _db.Invoice.Where(q => q.CustomerID == customerID && q.ProjectID == projectID && q.Complete == true).OrderByDescending(q => q.InvoiceDateTo).FirstOrDefault()?.InvoiceDateTo;

            return (lastDate.HasValue) ? lastDate.Value : DateTime.Now;
        }

        public List<TasksSelect> GetNotInvoicedTasks(int ProjectID)
        {
            var tasks = _db.Tasks.Where(q => q.Apartment.ProjectId == ProjectID && (q.Status == TaskStatus.Finished || q.Status == TaskStatus.FinishedWithFaults) && q.Invoice == false && q.Blocked == false)
                .Select(q => new TasksSelect
                {
                    ID = q.Id,
                    ProductName = ((string.IsNullOrEmpty(q.Apartment.Project.Product_Project.FirstOrDefault(x => x.ProductID == q.ProductId).ProductName)) ?
                                q.Product.Name : q.Apartment.Project.Product_Project.FirstOrDefault(x => x.ProductID == q.ProductId).ProductName),
                    Sufix = q.TaskSufix,
                    Apartment = q.Apartment.NumberOfApartment,
                    ApartmentID = q.ApartmentId
                }).ToList();

            return tasks;
        }

        public async Task<List<int>> GenerateInvoiceElements(DateTime To, Invoice invoice)
        {
            //From = DateTime.Parse(From.ToString("yyyy-MM-dd") + " 00:00:00");
            To = DateTime.Parse(To.ToString("yyyy-MM-dd") + " 23:59:59");

            var tasks = _db.Tasks.Where(q => (q.Status == TaskStatus.Finished || q.Status == TaskStatus.FinishedWithFaults || q.Status == TaskStatus.FinishedWithFaultsCorrected || q.ForceInvoice == true) && q.Invoice == false && q.Blocked == false && q.Accepted == true && q.GroupTaskID.HasValue == false).ToList();

            var projects = tasks.Where(q => q.Apartment.Project.ChildProject == false).GroupBy(q => q.Apartment.ProjectId).Select(q => q.Key).ToList();

            var childs = tasks.Where(q => q.Apartment.Project.ChildProject == true).GroupBy(q => q.Apartment.ProjectId);

            foreach (var c in childs)
            {
                var pID = c.FirstOrDefault()?.Apartment.Project.ParentProjectID ?? 0;
                if (projects.All(q => q != pID))
                    projects.Add(pID);
            }

            var elements = new List<InvoiceElements>();
            var invoiceList = new List<int>();

            var invNumber = LastInvoiceNumber() + 1;

            if (invoice.ProjectID > 0)
                projects = projects.Where(q => q == invoice.ProjectID).ToList();

            foreach (var p in projects)
            {

                if (string.IsNullOrEmpty(tasks.FirstOrDefault(q => q.Apartment.ProjectId == p || q.Apartment.Project.ParentProjectID == p)?.Apartment.Project.InSystemID))
                    continue;

                var tp = tasks.Where(q => q.Apartment.ProjectId == p || q.Apartment.Project.ParentProjectID == p).OrderBy(q => q.ClosedDate);

                var test = tp.Where(q => q.ApartmentId == 3360);
                if (p == 216)
                    Console.WriteLine("y");

                var invDateFrom = DateTime.Now;
                var invDateTo = DateTime.Now;

                var tasksWithClosedDate = tp.Where(q => q.ClosedDate != null).ToList();

                if (tasksWithClosedDate.FirstOrDefault() != null)
                {
                    invDateFrom = tasksWithClosedDate.FirstOrDefault().ClosedDate.Value;
                }

                if (tasksWithClosedDate.LastOrDefault() != null)
                {
                    invDateTo = tasksWithClosedDate.LastOrDefault().ClosedDate.Value;
                }

                var inv = new Invoice
                {
                    InvoiceNumber = invNumber,
                    CustomerID = tp.FirstOrDefault().Apartment.Project.CustomerId,
                    ProjectID = p,
                    BankAccountID = invoice.BankAccountID,
                    Comments = invoice.Comments,
                    Complete = false,
                    InvoiceDate = invoice.InvoiceDate,
                    DeliveryDate = invoice.DeliveryDate,
                    PaymentTerm = invoice.PaymentTerm,
                    InvoiceDateFrom = invDateFrom,
                    InvoiceDateTo = invDateTo,
                    PenaltyInterest = invoice.PenaltyInterest,
                    CreateDate = DateTime.Now,
                };

                var invoiceID = await CreateInvoice(inv);

                if (invoiceID == null)
                    continue;

                invoiceList.Add(invoiceID.Value);

                foreach (var t in tp)
                {
                    var firstTF = t.Task_Fitter.FirstOrDefault(q => q.IsActive);

                    if (!t.ClosedDate.HasValue && firstTF == null)
                        t.ClosedDate = DateTime.Now;

                    if (!t.ClosedDate.HasValue && firstTF.Timestamp > To)
                        continue;

                    decimal quantity = 0;
                    var unit = t.Product.Jm;
                    var unitPrice = t.ProductPrice;

                    if (t.ForceInvoice && !t.IsHourlyPay)
                        quantity = t.ProductCount;
                    else if (t.IsHourlyPay)
                    {
                        quantity = t.Task_Fitter.Where(q => q.IsActive).Sum(x => x.WorkTime) / 60;
                        unit = "h";
                        unitPrice = t.HourlyPrice ?? 0;
                    }
                    else
                        quantity = t.Task_Fitter.Where(q => q.IsActive).Sum(x => x.WorkDone);

                    var element = new InvoiceElements
                    {
                        InvoiceID = invoiceID.Value,
                        ProjectID = p,
                        ApartmentNumber = (t.Apartment.Project.ChildProject) ? t.Apartment.Letter + t.Apartment.NumberOfApartment + " " + t.Apartment.Project.ProjectNumber : t.Apartment.Letter + t.Apartment.NumberOfApartment,
                        TaskID = t.Id,
                        ProductName = t.GetTaskName(t.Apartment.ProjectId) + " " + t.TaskSufix,
                        Quantity = quantity,//(t.ForceInvoice) ? t.ProductCount : t.Task_Fitter.Sum(x => x.WorkDone),
                        Unit = unit,
                        UnitPrice = unitPrice,//t.ProductPrice,
                        Complete = false
                    };

                    if (t.TaskGroup.Any())
                    {
                        elements.AddRange(t.TaskGroup.Select(tg => new InvoiceElements
                        {
                            InvoiceID = invoiceID.Value,
                            ProjectID = p,
                            ApartmentNumber = (t.Apartment.Project.ChildProject) ? t.Apartment.Letter + t.Apartment.NumberOfApartment + " " + t.Apartment.Project.ProjectNumber : t.Apartment.Letter + t.Apartment.NumberOfApartment,
                            TaskID = tg.Id,
                            ProductName = tg.GetTaskName(t.Apartment.ProjectId),
                            Quantity = tg.ProductCount,
                            Unit = tg.Product.Jm,
                            UnitPrice = tg.ProductPrice,
                            Complete = false
                        }));
                    }

                    if (t.Product.ProductGroups_Main.Any())
                    {
                        foreach (var tg in t.Product.ProductGroups_Main)
                        {
                            if (t.TaskGroup.Any(q => q.ProductId == tg.ProductSubID) || tg.CreateTask == true || t.Production != TaskProduction.Developer)
                                continue;

                            var price = tg.ProductSub.Price;

                            if (tg.ProductSub.Product_Project.Any(q => q.ProjectID == p))
                                price = tg.ProductSub.Product_Project.FirstOrDefault(q => q.ProjectID == p)?.Price ?? 0;

                            var notTaskElement = new InvoiceElements
                            {
                                InvoiceID = invoiceID.Value,
                                ProjectID = p,
                                ApartmentNumber = (t.Apartment.Project.ChildProject) ? t.Apartment.Letter + t.Apartment.NumberOfApartment + " " + t.Apartment.Project.ProjectNumber : t.Apartment.Letter + t.Apartment.NumberOfApartment,
                                TaskID = null,
                                ProductName = tg.ProductSub.Name,
                                Quantity = tg.Quantity,
                                Unit = tg.ProductSub.Jm,
                                UnitPrice = price,
                                Complete = false
                            };

                            elements.Add(notTaskElement);
                        }
                    }

                    var distancePayment = t.Distance * t.DistancePrice;

                    if (distancePayment > 0)
                    {
                        var distancePaymentElement = new InvoiceElements
                        {
                            InvoiceID = invoiceID.Value,
                            ProjectID = p,
                            ApartmentNumber = (t.Apartment.Project.ChildProject) ? t.Apartment.NumberOfApartment + " " + t.Apartment.Project.ProjectNumber : t.Apartment.NumberOfApartment,
                            TaskID = t.Id,
                            ProductName = t.GetTaskName(t.Apartment.ProjectId) + " " + t.TaskSufix + " Dojazd",
                            Quantity = t.Distance,
                            Unit = "km",
                            UnitPrice = t.DistancePrice,
                            Complete = false
                        };

                        elements.Add(distancePaymentElement);
                    }

                    if (element.Sum != 0)
                        elements.Add(element);
                }

                invNumber++;
            }

            var adds = _db.Invoice_Adds.Where(q => q.InvoiceID.HasValue == false).ToList();

            if (adds.Count > 0)
            {
                foreach (var x in adds)
                {
                    if (x.Task == null)
                        continue;

                    int? inv = -1;

                    if (_db.Invoice.Any(q => invoiceList.Contains(q.ID) && q.ProjectID == x.Task.Apartment.ProjectId))
                    {
                        inv = _db.Invoice.FirstOrDefault(q => invoiceList.Contains(q.ID) && q.ProjectID == x.Task.Apartment.ProjectId).ID;
                    }
                    else
                    {
                        var invNew = new Invoice
                        {
                            InvoiceNumber = invNumber,
                            CustomerID = x.Task.Apartment.Project.CustomerId,
                            ProjectID = x.Task.Apartment.ProjectId,
                            BankAccountID = invoice.BankAccountID,
                            Comments = invoice.Comments,
                            Complete = false,
                            InvoiceDate = invoice.InvoiceDate,
                            DeliveryDate = invoice.DeliveryDate,
                            PaymentTerm = invoice.PaymentTerm,
                            InvoiceDateFrom = DateTime.Now,
                            InvoiceDateTo = DateTime.Now,
                            PenaltyInterest = invoice.PenaltyInterest,
                            CreateDate = DateTime.Now,
                        };

                        invNumber++;

                        inv = await CreateInvoice(invNew);
                        invoiceList.Add(inv.Value);
                    }

                    elements.Add(new InvoiceElements
                    {
                        InvoiceID = inv.Value,
                        ProjectID = x.Task.Apartment.ProjectId,
                        ApartmentNumber = (x.Task.Apartment.Project.ChildProject) ? x.Task.Apartment.Letter + x.Task.Apartment.NumberOfApartment + " " + x.Task.Apartment.Project.ProjectNumber : x.Task.Apartment.Letter + x.Task.Apartment.NumberOfApartment,
                        TaskID = x.TaskID,
                        ProductName = x.Task.GetTaskName(x.Task.Apartment.ProjectId) + " " + x.Task.TaskSufix,
                        Quantity = 0,
                        Unit = x.Task.Product.Jm,
                        UnitPrice = 0,
                        Total = x.Amount,
                        Description = "Tillägsfaktura för fel belopp på första fakturan",
                        Invoice_AddsID = x.ID
                    });
                }
            }

            try
            {
                var result = _db.InvoiceElements.AddRange(elements);
                await _db.SaveChangesAsync();

                return invoiceList;
            }
            catch (DbEntityValidationException ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }

        }

        public List<BankAccountsSelect> AddBankAccount(string Currency, string Account)
        {
            var account = new BankAccounts
            {
                Currency = Currency,
                AccountNumber = Account
            };

            _db.BankAccounts.Add(account);

            try
            {
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                Console.WriteLine(1);
            }

            return _db.BankAccounts.Select(q => new BankAccountsSelect { ID = q.ID, AccountNumber = q.AccountNumber }).ToList();
        }

        public InvoiceElementViewModel CreateSingleInvoiceElement(int invoiceID, int taskID)
        {
            var task = _db.Tasks.Find(taskID);

            if (task == null)
                return null;

            var element = new InvoiceElements
            {
                InvoiceID = invoiceID,
                ApartmentNumber = task.Apartment.NumberOfApartment,
                TaskID = task.Id,
                ProductName = task.GetTaskName(task.Apartment.ProjectId) + " " + task.TaskSufix,
                Quantity = task.Task_Fitter.Sum(x => x.WorkDone),
                Unit = task.Product.Jm,
                UnitPrice = task.ProductPrice,
                Complete = false
            };

            _db.InvoiceElements.Add(element);

            try
            {
                _db.SaveChanges();

                var elementVM = new InvoiceElementViewModel
                {
                    ID = element.ID,
                    TaskID = element.TaskID,
                    ProductName = element.ProductName,
                    Quantity = element.Quantity,
                    Unit = element.Unit,
                    UnitPrice = element.UnitPrice,
                    Sum = Math.Round((element.UnitPrice * element.Quantity), 2)
                };

                return elementVM;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }
        }

        public List<TasksSelect> GetApartmentTasksList(string apartmentNumber, int invoiceID)
        {
            var invoice = _db.Invoice.Find(invoiceID);

            if (invoice == null) return null;

            var apartments = _db.Tasks.Where(q => q.Apartment.NumberOfApartment == apartmentNumber && q.Apartment.ProjectId == invoice.ProjectID && (q.Status == TaskStatus.Finished || q.Status == TaskStatus.FinishedWithFaults) && q.Invoice == false && q.Blocked == false).Select(q => new TasksSelect { ID = q.Id, ProductName = q.GetTaskName(q.Apartment.ProjectId), Sufix = q.TaskSufix, Apartment = q.Apartment.NumberOfApartment, ApartmentID = q.ApartmentId }).ToList();

            return apartments;
        }
        #endregion

        #region FEES

        public int LastFeeNumber()
        {
            var fee = _db.Fees.OrderByDescending(q => q.FeeNumber).FirstOrDefault();

            return (fee != null) ? fee.FeeNumber : 1;
        }

        public int FeesCount()
        {
            return _db.Fees.Count();
        }

        public List<FeesViewModel> FeesList(out int rows, int? skip = default(int?), int? take = default(int?), string orderBy = "feedate", string dest = "desc", string search = "", string filters = "0,1")
        {
            if (skip == null)
                skip = 0;

            if (take == null)
                take = 20;

            rows = 0;

            if (string.IsNullOrEmpty(filters))
                return null;

            List<int> filter = new List<int>();

            foreach (var fl in filters.Split(','))
            {
                filter.Add(Convert.ToInt32(fl));
            }


            IQueryable<FeesViewModel> query = _db.Fees
                                                         .Select(q =>
                                                            new FeesViewModel
                                                            {
                                                                ID = q.ID,
                                                                FeeNumber = q.FeeNumber,
                                                                FitterID = q.FitterID,
                                                                FitterFirstName = q.Fitter.User.Name,
                                                                FitterLastName = q.Fitter.User.Surname,
                                                                ProjectID = q.ProjectID,
                                                                ApartmentID = q.ApartmentID,
                                                                TaskID = q.TaskID,
                                                                ProjectNumber = q.Project.ProjectNumber,
                                                                ApartmentNumber = q.Apartment.NumberOfApartment,
                                                                ProductName = (q.TaskID.HasValue && q.ProjectID.HasValue) ?
                                                                (string.IsNullOrEmpty(q.Project.Product_Project.FirstOrDefault(x => x.ProductID == q.Task.ProductId).ProductName)) ?
                                                                q.Task.Product.Name : q.Project.Product_Project.FirstOrDefault(x => x.ProductID == q.Task.ProductId).ProductName : "",
                                                                PayrollID = q.PayrollID,
                                                                PayrollNumber = q.Payroll.Number,
                                                                FeeDate = q.FeeDate,
                                                                TakeDate = q.TakeDate,
                                                                Description = q.Description,
                                                                FeeAmount = q.FeeAmount,
                                                                Status = q.Status
                                                            }
                                                        );

            IQueryable<FeesViewModel> queryOrderBy = null;
            IQueryable<FeesViewModel> queryWhere = null;
            IQueryable<FeesViewModel> queryFilter = null;
            IQueryable<FeesViewModel> querySkipTake = null;

            switch (orderBy)
            {
                case "number":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.FeeNumber);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.FeeNumber);
                    break;
                case "fitter":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.FitterFirstName);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.FitterFirstName);
                    break;
                case "project":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProjectNumber);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProjectNumber);
                    break;
                case "apartment":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ApartmentNumber);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ApartmentNumber);
                    break;
                case "product":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProductName);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProductName);
                    break;
                case "feedate":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.FeeDate);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.FeeDate);
                    break;
                case "offdate":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.TakeDate);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.TakeDate);
                    break;
                case "amount":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.FeeAmount);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.FeeAmount);
                    break;
                case "payroll":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.PayrollNumber);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.PayrollNumber);
                    break;
                case "status":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Status);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Status);
                    break;
                default:
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.FeeDate);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.FeeDate);
                    break;
            }

            queryWhere = queryOrderBy.Where(p => p.ProjectNumber.Contains(search) || p.ApartmentNumber.Contains(search) || p.FitterFirstName.Contains(search) || p.FitterLastName.Contains(search) || p.ProductName.Contains(search));

            queryFilter = queryWhere.Where(p => filter.Contains((int)p.Status));

            rows = queryFilter.Count();

            querySkipTake = queryFilter.Skip((int)skip).Take((int)take);

            return querySkipTake.ToList();
        }

        public async Task<Fees> GetFee(int id)
        {
            var fee = await _db.Fees.FindAsync(id);

            return fee;
        }

        public int? CreateFee(Fees fee, string userID)
        {
            Projects project = null;

            if (fee.ProjectID.HasValue)
                project = _db.Projects.Find(fee.ProjectID.Value);
            else if (fee.ApartmentID.HasValue)
                project = _db.Projects.FirstOrDefault(q => q.Apartments.Any(x => x.Id == fee.ApartmentID));
            else if (fee.TaskID.HasValue)
            {
                var task = _db.Tasks.Find(fee.TaskID.Value);
                project = _db.Projects.Find(task.Apartment.ProjectId);
            }

            if (project != null && project.IsChecked)
            {
                project.IsChecked = false;
                _db.Entry(project).State = System.Data.Entity.EntityState.Modified;

                var user = _db.ACSystemUser.Find(userID);

                var activity = new Project_Activity
                {
                    ProjectID = project.Id,
                    UserID = userID,
                    Message = $"Projekt zmieniony na NIE SPRAWDZONY, użytkownik {user.FullName} dodał do projektu szkodę.",
                    Timestamp = DateTime.Now
                };

                _db.Project_Activity.Add(activity);
            }


            try
            {
                _db.Fees.Add(fee);

                _db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }


            return fee.ID;
        }

        public async Task<int?> EditFee(Fees fee)
        {
            try
            {
                _db.Entry(fee).State = System.Data.Entity.EntityState.Modified;

                await _db.SaveChangesAsync();
            }
            catch (DbEntityValidationException ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return null;
            }


            return fee.ID;
        }

        public async Task<bool> EditFee(int FeeID, FeeStatus Status, int PayrollID)
        {
            var fee = await GetFee(FeeID);
            fee.Status = Status;
            fee.PayrollID = PayrollID;
            fee.TakeDate = DateTime.Now;

            try
            {
                _db.Entry(fee).State = System.Data.Entity.EntityState.Modified;

                await _db.SaveChangesAsync();
            }
            catch (DbEntityValidationException ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }


            return true;
        }

        public async Task<string> DeleteFee(int id)
        {
            var fee = await GetFee(id);

            if (fee == null)
                return "false;Pusto";

            try
            {
                var result = _db.Fees.Remove(fee);

                await _db.SaveChangesAsync();

                return "true;" + fee.FeeNumber;
            }
            catch (Exception ex)
            {
                return $"false;{ex.Message}";
            }
        }

        #endregion

        #region GENERALS

        public List<FittersSelect> GetFittersList()
        {
            return _db.Fitters.Select(q => new FittersSelect { ID = q.Id, Name = q.User.Name, SurName = q.User.Surname, IsLocked = q.User.LockoutEnabled }).ToList();
        }

        public List<ProjectsSelect> GetProjectsList()
        {
            return _db.Projects.Select(q => new ProjectsSelect { ID = q.Id, ProjectNumber = "ID: " + q.InSystemID + "  |  No.: " + q.ProjectNumber + "  |  Miasto: " + q.City }).ToList();
        }

        public List<ProjectsSelect> GetProjectsList(int id)
        {
            return _db.Projects.Where(q => q.CustomerId == id).Select(q => new ProjectsSelect { ID = q.Id, ProjectNumber = q.ProjectNumber }).ToList();
        }

        public List<ApartmentsSelect> GetApartmentsList(int id)
        {
            return _db.Apartments.Where(q => q.ProjectId == id).Select(q => new ApartmentsSelect { ID = q.Id, ApartmentNumber = q.Letter + " " + q.NumberOfApartment }).ToList();
        }

        public List<TasksSelect> GetTasksList(int id)
        {
            return _db.Tasks.Where(q => q.ApartmentId == id).Select(q => new TasksSelect
            {
                ID = q.Id,
                ProductName = ((string.IsNullOrEmpty(q.Apartment.Project.Product_Project.FirstOrDefault(x => x.ProductID == q.ProductId).ProductName)) ?
                q.Product.Name : q.Apartment.Project.Product_Project.FirstOrDefault(x => x.ProductID == q.ProductId).ProductName) + " " + q.TaskSufix
            }).ToList();
        }

        public List<CustomerSelect> GetCustomersList()
        {
            return _db.Customers.Select(q => new CustomerSelect { ID = q.Id, Name = q.Name }).ToList();
        }

        public List<BankAccounts> GetBankAccountsList()
        {
            return _db.BankAccounts.ToList();
        }
        #endregion

    }

    #region CLASSES

    public class SupervisorAdds
    {
        public string SupervisorID { get; set; }
        public int ProjectID { get; set; }
        public decimal Amount { get; set; }

        public ACSystemUser Supervisor { get; set; }
    }

    #endregion
}