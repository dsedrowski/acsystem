﻿using ACSystem.Core.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ACSystem.Core.Repo
{
    public class TasksRepo : ITasksRepo
    {
        private readonly IACSystemContext _db;

        public TasksRepo(IACSystemContext db)
        {
            _db = db;
        }

        public int Count()
        {
            return _db.Tasks.Count();
        }

        public int AvailableCount()
        {
            return _db.Tasks.Where(q => q.Status == TaskStatus.Free || q.Status == TaskStatus.Stoped).Count();
        }

        public bool Create(Tasks task)
        {
            try
            {
                _db.Tasks.Add(task);

                SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "TaskCreate", "");
                return false;
            }
        }

        public async Task<string> Delete(int id)
        {
            var task = await GetTask(id);

            try
            {
                _db.Tasks.Remove(task);
                SaveChanges();

                return "true;" + task.Id;
            }
            catch (Exception ex)
            {
                return "false;" + ex.Message;
            }
        }

        public bool Edit(Tasks task)
        {
            try
            {
                _db.Entry(task).State = EntityState.Modified;
                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "TaskEdit", "");
                return false;
            }
        }

        public async Task<Tasks> GetTask(int id)
        {
            return await _db.Tasks.FindAsync(id);
        }

        public List<TasksViewModel> List(out int rows, int id, int? skip = default(int?), int? take = default(int?), string orderBy = "name", string dest = "asc", string search = "", string statuss = "0,1,2,3,4", string types = "0,1,2")
        {
            if (skip == null)
                skip = 0;

            if (take == null)
                take = 20;

            rows = 0;

            if (string.IsNullOrEmpty(statuss) && string.IsNullOrEmpty(types))
                return null;

            List<int> status = new List<int>();
            List<int> type = new List<int>();

            foreach (var fl in statuss.Split(','))
            {
                status.Add(Convert.ToInt32(fl));
            }

            foreach (var fl in types.Split(','))
            {
                type.Add(Convert.ToInt32(fl));
            }

            IQueryable<TasksViewModel> query = _db.Tasks.Where(q => q.Id == id).Include(q => q.Product).Select(q => new TasksViewModel()
            {
                Id = q.Id,
                ProductName = q.GetTaskName(q.Apartment.ProjectId),
                ProductCount = q.ProductCount,
                ProductPrice = q.ProductPrice,
                Jm = q.Product.Jm,
                Type = q.Product.ProductType,
                Status = q.Status
            });
            IQueryable<TasksViewModel> queryOrderBy = null;
            IQueryable<TasksViewModel> queryFilter = null;
            IQueryable<TasksViewModel> queryWhere = null;
            IQueryable<TasksViewModel> querySkipTake = null;

            switch (orderBy)
            {
                case "name":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProductName);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProductName);
                    break;
                case "count":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProductCount);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProductCount);
                    break;
                case "price":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProductPrice);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProductPrice);
                    break;
                case "jm":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Jm);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Jm);
                    break;
                case "type":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Type);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Type);
                    break;
                case "status":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Status);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Status);
                    break;
                default:
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProductName);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProductName);
                    break;
            }

            queryWhere = queryOrderBy.Where(p => p.ProductName.Contains(search));

            queryFilter = queryWhere.Where(p => status.Contains((int)p.Status) && type.Contains((int)p.Type));

            rows = queryFilter.Count();

            querySkipTake = queryFilter.Skip((int)skip).Take((int)take);

            return querySkipTake.ToList();
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }
    }
}