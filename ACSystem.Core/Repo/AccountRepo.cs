﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ACSystem.Core.Repo
{
    public class AccountRepo : IAccountRepo
    {
        private readonly IACSystemContext _db;

        public AccountRepo()
        {
            _db = new ACSystemContext();
        }

        public AccountRepo(IACSystemContext db)
        {
            _db = db;
        }

        public List<ACSystemUser> UsersList()
        {
            return _db.ACSystemUser.ToList();
        }

        public EditUserViewModel UserById(string id)
        {
            var user = _db.ACSystemUser.Where(u => u.Id == id).FirstOrDefault();

            var rolesList = new List<string>();

            foreach(var role in user.Roles)
            {
                rolesList.Add(role.RoleId);
            }

            if (user.Fitter == null)
            {
                var newFitter = new Fitters
                {
                    Id = user.Id,
                    PercentType = 1,
                    HourlyRate = 0
                };

                AddFitter(newFitter);

                user.Fitter = newFitter;
            }

            var registerViewModel = new EditUserViewModel()
            {
                Id = user.Id,
                Role = rolesList,
                Email = user.Email,
                Name = user.Name,
                Surname = user.Surname,
                Position = user.Position,
                DateOfBirth = user.DateOfBirth,
                PhoneNumber = user.PhoneNumber,
                Image = user.Image,
                PercentType = user.Fitter.PercentType,
                CalendarID = user.CalendarID,
                HourlyRate = user.Fitter.HourlyRate,
                Podpis = user.Podpis,
                InSystemUserID = user.InSystemUserID,
                LockoutEnabled = user.LockoutEnabled,
                LockTimestamp = user.LockTimestamp,
                CanSeeAll = user.CanSeeAll,
                FirstDoService = user.FirstDoService ?? false,
                HireDate = user.HireDate,
                ReleaseDate = user.ReleaseDate
            };

            return registerViewModel;
        }

        public ACSystemUser FullUserById(string id)
        {
            return _db.ACSystemUser.Where(u => u.Id == id).FirstOrDefault();
        }

        public bool UpdateUser(EditUserViewModel model)
        {
            try
            {
                var original = _db.ACSystemUser.Find(model.Id);

                if (original != null)
                {
                    original.UserName = model.Email;
                    original.Name = model.Name;
                    original.Surname = model.Surname;
                    original.DateOfBirth = model.DateOfBirth;
                    original.Position = model.Position;
                    original.Email = model.Email;
                    original.PhoneNumber = model.PhoneNumber;
                    original.CalendarID = model.CalendarID;
                    original.Podpis = model.Podpis;
                    original.HireDate = model.HireDate;
                    original.ReleaseDate = model.ReleaseDate;
                }

                _db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
        }

        public void AddFitter(Fitters fitter)
        {
            try
            {
                _db.Fitters.Add(fitter);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return;
            }
        }

        public void EditFitter(Fitters fitter, string userId)
        {
            var current = _db.Fitters.Find(fitter.Id);

            if (current.PercentType != fitter.PercentType)
            {
                _db.UserChangeLog.Add(new UserChangeLog
                {
                    UserId = fitter.Id,
                    Message = string.Format("Zmieniono stawkę procentową ze stawki nr {0} na stawkę nr {1}", current.PercentType, fitter.PercentType),
                    Timestamp = DateTime.Now,
                    ChangeUserId = userId
                });                
            }

            if (current.HourlyRate != fitter.HourlyRate)
            {
                _db.UserChangeLog.Add(new UserChangeLog
                {
                    UserId = fitter.Id,
                    Message = string.Format("Zmieniono stawkę godzinową z {0}kr na {1}kr", current.HourlyRate, fitter.HourlyRate),
                    Timestamp = DateTime.Now,
                    ChangeUserId = userId
                });
            }

            current.PercentType = fitter.PercentType;
            current.HourlyRate = fitter.HourlyRate;

            _db.Entry(current).State = EntityState.Modified;

            SaveChanges();
        }

        public void DeleteFitter(Fitters fitter)
        {
            var inventory_fitter = _db.Inventory_Fitter.Where(f => f.UserID == fitter.Id);

            try
            {
                _db.Fitters.Remove(fitter);

                foreach (var fi in inventory_fitter)
                {
                    if (fi.Inventory.Status == InventoryStatus.Busy)
                    {
                        var inventory = _db.Inventory.Where(i => i.ID == fi.InventoryID).FirstOrDefault();
                        inventory.Status = InventoryStatus.Free;

                        _db.Entry(inventory).State = EntityState.Modified;
                    }
                }

                _db.Inventory_Fitter.RemoveRange(inventory_fitter);

                _db.SaveChanges();
            }
            catch
            {
                return;
            }
        }

        public bool DeleteFitterInventory(string userId)
        {
            var fi = _db.Inventory_Fitter.Where(q => q.UserID == userId);

            foreach (var f in fi)
            {
                _db.Inventory_Fitter.Remove(f);
            }

            return true;
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }
    }
}