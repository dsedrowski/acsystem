﻿using ACSystem.Core.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACSystem.Core.Models;
using System.Threading.Tasks;
using System.Data.Entity;
using ACSystem.Core.Models.ViewModels;

namespace ACSystem.Core.Repo
{
    public class CustomersRepo : ICustomersRepo
    {
        private readonly IACSystemContext _db;

        public CustomersRepo(IACSystemContext db)
        {
            _db = db;
        }

        public bool Create(Customers customer)
        {
            try
            {
                customer.CreateDate = DateTime.Now;
                _db.Customers.Add(customer);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public string Delete(int id)
        {
            var customer = GetCustomerById(id);

            try
            {
                _db.Customers.Remove(customer);
                
                SaveChanges();
                return "true;" + customer.Name;
            }
            catch (Exception ex)
            {
                return "false;" + ex.Message;
            }
        }

        public bool Edit(Customers customer)
        {
            try
            {
                _db.Entry(customer).State = EntityState.Modified;
            }
            catch
            {
                return false;
            }

            return true;
        }

        public Customers GetCustomerById(int id)
        {
            return _db.Customers.Find(id);
        }

        public int Count()
        {
            return _db.Customers.Count();
        }

        public List<CustomersListViewModel> List(out int rows, int? skip = null, int? take = null, string orderBy = "name", string dest = "asc", string search = "", int filter = -1)
        {
            if (skip == null)
                skip = 0;

            if (take == null)
                take = 20;

            rows = 0;

            IQueryable<CustomersListViewModel> query = null;            
                        
            switch (orderBy)
            {
                case "name":
                    if (dest == "asc")
                        query = _db.Customers.Select(c => new CustomersListViewModel { Id = c.Id, Name = c.Name, Street = c.Street, HouseNo = c.HouseNo, PostalCode = c.PostalCode, City = c.City, Email = c.Email, IsLocked = c.IsLocked })
                                        .OrderBy(c => c.Name)
                                        .Where(c => c.Name.Contains(search) || c.Street.Contains(search) || c.Email.Contains(search));
                    else
                        query = _db.Customers.Select(c => new CustomersListViewModel { Id = c.Id, Name = c.Name, Street = c.Street, HouseNo = c.HouseNo, PostalCode = c.PostalCode, City = c.City, Email = c.Email, IsLocked = c.IsLocked })
                                        .OrderByDescending(c => c.Name)
                                        .Where(c => c.Name.Contains(search) || c.Street.Contains(search) || c.Email.Contains(search));
                    break;
                case "address":
                    if (dest == "asc")
                        query = _db.Customers.Select(c => new CustomersListViewModel { Id = c.Id, Name = c.Name, Street = c.Street, HouseNo = c.HouseNo, PostalCode = c.PostalCode, City = c.City, Email = c.Email, IsLocked = c.IsLocked })
                                        .OrderBy(c => c.Street)
                                        .Where(c => c.Name.Contains(search) || c.Street.Contains(search) || c.Email.Contains(search));
                    else
                        query = _db.Customers.Select(c => new CustomersListViewModel { Id = c.Id, Name = c.Name, Street = c.Street, HouseNo = c.HouseNo, PostalCode = c.PostalCode, City = c.City, Email = c.Email, IsLocked = c.IsLocked })
                                        .OrderByDescending(c => c.Street)
                                        .Where(c => c.Name.Contains(search) || c.Street.Contains(search) || c.Email.Contains(search));
                    break;
                case "email":
                    if (dest == "asc")
                        query = _db.Customers.Select(c => new CustomersListViewModel { Id = c.Id, Name = c.Name, Street = c.Street, HouseNo = c.HouseNo, PostalCode = c.PostalCode, City = c.City, Email = c.Email, IsLocked = c.IsLocked })
                                        .OrderBy(c => c.Email)
                                        .Where(c => c.Name.Contains(search) || c.Street.Contains(search) || c.Email.Contains(search));
                    else
                        query = _db.Customers.Select(c => new CustomersListViewModel { Id = c.Id, Name = c.Name, Street = c.Street, HouseNo = c.HouseNo, PostalCode = c.PostalCode, City = c.City, Email = c.Email, IsLocked = c.IsLocked })
                                        .OrderByDescending(c => c.Email)
                                        .Where(c => c.Name.Contains(search) || c.Street.Contains(search) || c.Email.Contains(search));
                    break;
               default:
                    if (dest == "asc")
                        query = _db.Customers.Select(c => new CustomersListViewModel { Id = c.Id, Name = c.Name, Street = c.Street, HouseNo = c.HouseNo, PostalCode = c.PostalCode, City = c.City, Email = c.Email, IsLocked = c.IsLocked })
                                        .OrderBy(c => c.Name)
                                        .Where(c => c.Name.Contains(search) || c.Street.Contains(search) || c.Email.Contains(search));
                    else
                        query = _db.Customers.Select(c => new CustomersListViewModel { Id = c.Id, Name = c.Name, Street = c.Street, HouseNo = c.HouseNo, PostalCode = c.PostalCode, City = c.City, Email = c.Email, IsLocked = c.IsLocked })
                                        .OrderByDescending(c => c.Name)
                                        .Where(c => c.Name.Contains(search) || c.Street.Contains(search) || c.Email.Contains(search));
                    break;
            }

            if (filter != -1)
                query = query.Where(q => q.IsLocked || !q.IsLocked);
            else
                query = query.Where(q => !q.IsLocked);

            rows = query.Count();

            var result = query.Skip((int)skip).Take((int)take);

            return result.ToList();
        }

        public int ProjectManagerCreate(ProjectManagers projectManager)
        {
            _db.ProjectManagers.Add(projectManager);
            SaveChanges();

            return projectManager.Id;
        }

        public bool ProjectManagerEdit(ProjectManagers projectManager)
        {
            try
            {
                var pm = _db.ProjectManagers.Find(projectManager.Id);

                pm.Name = projectManager.Name;
                pm.PhoneNumber = projectManager.PhoneNumber;
                pm.Email = projectManager.Email;
                pm.IsLocked = projectManager.IsLocked;

                _db.Entry(pm).State = EntityState.Modified;
                SaveChanges();
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }

            return true;
        }

        public string ProjectManagerDelete(int id)
        {
            var projectManager = ProjectManagerByIdFull(id);

            try
            {
                _db.ProjectManagers.Remove(projectManager);
                SaveChanges();
                return "true;" + projectManager.Name;
            }
            catch (Exception ex)
            {
                return "false;" + ex.Message;
            }
        }             
        
        public ProjectManagers ProjectManagerByIdFull(int id)
        {
            return _db.ProjectManagers.Find(id);
        }

        public ProjectManagerViewModel ProjectManagerById(int id)
        {
            return _db.ProjectManagers.Where(p => p.Id == id).Select(p => new ProjectManagerViewModel { Id = p.Id, Name = p.Name, Email = p.Email, PhoneNumber = p.PhoneNumber, IsLocked = p.IsLocked }).FirstOrDefault();
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }
    }
}