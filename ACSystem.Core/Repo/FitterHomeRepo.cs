﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ACSystem.Core.Repo{

    public class FitterHomeRepo : IFitterHomeRepo
    {
        private readonly IACSystemContext _db;

        public FitterHomeRepo(IACSystemContext db)
        {
            _db = db;
        }

        public int TaskAvailableCount(string fitterId, bool onlyFavouriteProjects)
        {
            var user = _db.ACSystemUser.Find(fitterId);

            if (user == null) return 0;

            var favouriteIDs = user.FavouriteProjects.Select(q => q.ProjectID).ToList();

            if (user.UserType == UserType.Wewnetrzny)
                return _db.Tasks.Where(
                    q =>
                    (q.Status == TaskStatus.Free || q.Status == TaskStatus.Stoped || q.Status == TaskStatus.DoWyjasnienia) &&
                    (
                        user.CanSeeAll ||
                        (
                            q.Access_User_Task.Count(t => t.UserID.Equals(fitterId)) > 0 ||
                            q.Apartment.Access_User_Apartment.Count(a => a.UserID.Equals(fitterId)) > 0 ||
                            q.Apartment.Project.Access_User_Project.Count(p => p.UserID.Equals(fitterId)) > 0
                        )
                    )
                    && (!onlyFavouriteProjects || (onlyFavouriteProjects && favouriteIDs.Contains(q.Apartment.ProjectId)))
                    ).Count();
            else
            {
                var tasks = 0;

                foreach (var tg in user.Task_AccessGrant)
                {
                    if ((tg.Task.Status == TaskStatus.Free || tg.Task.Status == TaskStatus.Stoped || tg.Task.Status == TaskStatus.DoWyjasnienia) &&
                        tg.Task.ProductCount > 0)
                        tasks++;
                }

                return tasks;
            }
        }

        public List<IGrouping<Payroll, PayrollItems>> PayrollListForFitter(string id)
        {
            var list = _db.PayrollItems.Where(q => q.FitterID == id && q.Active == true && q.Payroll.Active == true).GroupBy(q => q.Payroll).OrderByDescending(q => q.Key.CreateDate).ToList();

            return list;
        }

        public List<PayrollItems> PayrollItemsForFitter(string id)
        {
            var list = _db.PayrollItems.Where(q => q.FitterID == id).ToList();

            return list;
        }


        public IQueryable<Tasks> TasksList()
        {
            return _db.Tasks;
        }

        public IQueryable<Tasks> TasksList(int apartmentId)
        {
            return _db.Tasks.Where(q => q.ApartmentId == apartmentId).AsQueryable();
        }

        public async Task<Tasks> GetTask(int id)
        {
            return await _db.Tasks.FindAsync(id);
        }

        public IQueryable<Events> EventsList()
        {
            return _db.Events;
        }

        public async Task<Events> GetEvent(int id)
        {
            return await _db.Events.FindAsync(id);
        }


        public Fitters GetFitter(string id)
        {
            return _db.Fitters.Find(id);
        }

        public IACSystemContext GetContext()
        {
            return _db;
        }

        public bool IsFitterBusy(string userId)
        {
            var user = _db.Fitters.Find(userId);

            if (user.TaskAlreadyWork == null)
                return false;
            else
                return true;
        }
    }
}