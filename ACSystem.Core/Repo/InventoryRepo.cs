﻿using ACSystem.Core.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using System.Threading.Tasks;
using System.Data.Entity;
using ACSystem.Core.Models.ViewModels.Select;
using ACSystem.Core.Extensions;
using System.Data.Entity.SqlServer;

namespace ACSystem.Core.Repo
{
    public class InventoryRepo : IInventoryRepo
    {
        private readonly IACSystemContext _db;

        public InventoryRepo(IACSystemContext db)
        {
            _db = db;
        }

        public int Count()
        {
            return _db.Inventory.Count();
        }

        public async Task<string> Delete(int id)
        {
            var inventory = await GetInventory(id);

            var fitter_inventory = _db.Inventory_Fitter.Where(q => q.InventoryID == id).ToList();
            var inventory_accessories_connect = _db.Inventory_Accessories_Connect.Where(q => q.InventoryID == id).ToList();
            var inventory_photo = _db.Inventory_Photo.Where(q => q.InventoryID == id).ToList();
            var inventory_check = _db.Inventory_Check.Where(q => q.InventoryID == id).ToList();

            try
            {
                _db.Inventory_Fitter.RemoveRange(fitter_inventory);
                _db.Inventory_Accessories_Connect.RemoveRange(inventory_accessories_connect);
                _db.Inventory_Photo.RemoveRange(inventory_photo);
                _db.Inventory_Check.RemoveRange(inventory_check);
                _db.Inventory.Remove(inventory);

                SaveChanges();

                return "true;" + inventory.EvidenceNumber;
            }
            catch (Exception ex)
            {
                return "false;" + ex.Message;
            }
        }

        public bool Edit(Inventory inventory)
        {
            try
            {
                _db.Entry(inventory).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }

            return true;
        }

        public async Task<Inventory> GetInventory(int id)
        {
            return await _db.Inventory.FindAsync(id);
        }

        public int Create(Inventory inventory)
        {
            try
            {
                _db.Inventory.Add(inventory);
            }
            catch (Exception ex)
            {
                Service.Helpers.Log(ex, "CreateInventory", "");

                return -1;
            }

            return inventory.ID;
        }

        public List<InventoryListViewModel> List(out int rows, int? skip = default(int?), int? take = default(int?), string orderBy = "name", string dest = "asc", string search = "", string filters = "0,1",
                                        string numer_ew_s = "", string numer_ew_t = "", string typ = "", string marka = "", DateTime? data = null,
                                        string magazyn = "", string posiada = "", string akcesoria = "")
        {
            if (skip == null)
                skip = 0;

            if (take == null)
                take = 20;

            rows = 0;

            if (string.IsNullOrEmpty(filters))
                return null;

            List<int> filter = new List<int>();

            foreach(var fl in filters.Split(','))
            {
                filter.Add(Convert.ToInt32(fl));
            }

            IQueryable<InventoryListViewModel> query = _db.Inventory.Select(
                                        c => new InventoryListViewModel
                                        {
                                            Id = c.ID,
                                            Type = c.Type.Name,
                                            Mark = c.Mark,
                                            NumberOfMachine = (!(c.EvidenceNumber == null || c.EvidenceNumber.Equals(string.Empty))) ? "S-" + c.EvidenceNumber : "",
                                            NumberOfMachine2 = (!(c.EvidenceNumber2 == null || c.EvidenceNumber2.Equals(string.Empty))) ? "T-" + c.EvidenceNumber2 : "",
                                            BuyDate = c.BuyDate,
                                            Status = c.Status,
                                            Warehouse = (c.WarehouseID.HasValue) ? c.Warehouse.Street + " " + c.Warehouse.City : "---",
                                            Owner = (c.OwnerType == InventoryOwnerType.Private) ? c.Owner.Name + " " + c.Owner.Surname : (!c.WarehouseID.HasValue) ? c.Inventory_Fitter.OrderByDescending(q => q.TakeDate).FirstOrDefault().User.Name + " " + c.Inventory_Fitter.OrderByDescending(q => q.TakeDate).FirstOrDefault().User.Surname : "---",
                                            AccessoriesMatch = (string.IsNullOrEmpty(akcesoria)) ? true : c.Inventory_Accessories_Connect.Any(q => !string.IsNullOrEmpty(q.Accessory.EvidenceNumber) && q.Accessory.EvidenceNumber.Contains(akcesoria)),
                                            IsPrivate = (c.OwnerType == InventoryOwnerType.Private),
                                            IsVitalityEnd = DbFunctions.DiffDays(DateTime.Now, DbFunctions.AddMonths(c.BuyDate, c.LifeTime * 12)) < 0,
                                            IsVitalityComeToEnd = DbFunctions.DiffMonths(DateTime.Now, DbFunctions.AddMonths(c.BuyDate, c.LifeTime * 12)) < 3,
                                            ReviewIsComing = (c.Inventory_Check.OrderByDescending(q => q.CheckDate).FirstOrDefault() != null) ? DbFunctions.DiffMonths(DateTime.Now, c.Inventory_Check.OrderByDescending(q => q.CheckDate).FirstOrDefault().CheckDate) <= -11 : false
                                        }
                                        );
            IQueryable<InventoryListViewModel> queryOrderBy = null;
            IQueryable<InventoryListViewModel> queryWhere = null;
            IQueryable<InventoryListViewModel> queryFilter = null;
            IQueryable<InventoryListViewModel> querySkipTake = null;

            switch (orderBy)
            {
                case "numberOfMachine":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.NumberOfMachine);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.NumberOfMachine);
                    break;
                case "numberOfMachine2":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.NumberOfMachine2);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.NumberOfMachine2);
                    break;
                case "type":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Type);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Type);
                    break;
                case "mark":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Mark);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Mark);
                    break;
                case "status":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Status);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Status);
                    break;
                case "buydate":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.BuyDate);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.BuyDate);
                    break;
                case "magazyn":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Warehouse);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Warehouse);
                    break;
                case "owner":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Owner);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Owner);
                    break;
                default:
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.NumberOfMachine);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.NumberOfMachine);
                    break;
            }

            queryWhere = queryOrderBy.Where(p => p.NumberOfMachine.Contains(numer_ew_s) && p.NumberOfMachine2.Contains(numer_ew_t) && p.Type.Contains(typ) && p.Mark.Contains(marka)
                                            && ((data.HasValue == false || p.BuyDate.HasValue == false) || (data.HasValue && p.BuyDate.HasValue && p.BuyDate.Value == data.Value))
                                            && p.Warehouse.Contains(magazyn) && p.Owner.Contains(posiada) && p.AccessoriesMatch);

            queryFilter = queryWhere.Where(p => filter.Contains((int)p.Status));

            rows = queryFilter.Count();

            querySkipTake = queryFilter.Skip((int)skip).Take((int)take);

            return querySkipTake.ToList();
        }

        public bool GiveInventory(int InventoryID, string FitterID, int? ProjectID)
        {
            var inventory = _db.Inventory.Find(InventoryID);

            var fitter_inventory = new Inventory_Fitter
            {
                UserID = FitterID,
                InventoryID = InventoryID,
                ProjectID = ProjectID,
                TakeDate = DateTime.Now
            };

            _db.Inventory_Fitter.Add(fitter_inventory);

            inventory.Status = InventoryStatus.Busy;
            inventory.WarehouseID = null;
            inventory.ProjectID = ProjectID;
            _db.Entry(inventory).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "GiveInventory", "ADMIN");

                return false;
            }
        }


        public bool FreeInventory(int InventoryID, int? WarehouseID)
        {
            var inventory = _db.Inventory.Find(InventoryID);
            var fitter_inventory = _db.Inventory_Fitter.Where(q => q.InventoryID == InventoryID && q.PutDate == null).FirstOrDefault();

            if (fitter_inventory == null)
                return false;

            inventory.Status = InventoryStatus.Free;
            inventory.WarehouseID = WarehouseID;
            inventory.ProjectID = null;

            _db.Entry(inventory).State = EntityState.Modified;

            fitter_inventory.PutDate = DateTime.Now;
            fitter_inventory.WarehouseID = WarehouseID;

            _db.Entry(fitter_inventory).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "FreeInventory", "ADMIN");

                return false;
            }
        }

        public bool SellInventory(int InventoryID, decimal SellPrice)
        {
            var inventory = _db.Inventory.Find(InventoryID);

            inventory.Status = InventoryStatus.Sold;
            inventory.SellPrice = SellPrice;

            _db.Entry(inventory).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "SellInventory", "ADMIN");

                return false;
            }
        }

        public List<FittersSelect> Fitters()
        {
            return _db.Fitters.Where(q => q.User.LockoutEnabled == false).Select(q => new FittersSelect { ID = q.Id, Name = q.User.Name, SurName = q.User.Surname }).ToList();
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }

        public IACSystemContext GetContext()
        {
            return _db;
        }
    }
}