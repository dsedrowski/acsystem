﻿using ACSystem.Core.Extensions;
using ACSystem.Core.IRepo;
using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using ACSystem.Core.Models.ViewModels.Select;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ACSystem.Core.Repo
{
    public class ProjectsRepo : IProjectsRepo
    {
        private readonly IACSystemContext _db;

        public ProjectsRepo(IACSystemContext db)
        {
            _db = db;
        }

        public IACSystemContext GetContext()
        {
            return _db;
        }

        #region PROJECTS

        #region ADMIN

        public int Count()
        {
            return _db.Projects.Count();
        }

        public List<ProjectsListViewModel> List(out int rows, int? skip = default(int?), int? take = default(int?), string orderBy = "projectNumber", string dest = "asc", string search = "", string filters = "0,1,2,3", int daysRemind = 0)
        {
            if (skip == null)
                skip = 0;

            if (take == null)
                take = 20;

            rows = 0;

            if (string.IsNullOrEmpty(filters))
                return null;

            List<int> filter = new List<int>();

            foreach (var fl in filters.Split(','))
            {
                filter.Add(Convert.ToInt32(fl));
            }

            IQueryable<ProjectsListViewModel> query = _db.Projects.Include(c => c.Customer)
                                                         .Select(c => new ProjectsListViewModel
                                                         {
                                                             Id = c.Id,
                                                             ProjectNumber = c.ProjectNumber,
                                                             Street = c.Street,
                                                             City = c.City,
                                                             ApartmentsCount = c.Apartments.Count,
                                                             Status = c.Status,
                                                             Customer = c.Customer.Name,
                                                             CustomerId = c.CustomerId,
                                                             ColorHex = (c.CalendarID.HasValue) ? c.Calendar.ColorHex : "#FFF",
                                                             InSystemID = c.InSystemID,
                                                             IsChecked = c.IsChecked,
                                                             HasMatchedChildrens = c.ChildProjects.Any(p =>
                                                                                                             p.ProjectNumber.Contains(search) ||
                                                                                                             p.Street.Contains(search) ||
                                                                                                             p.City.Contains(search) ||
                                                                                                             p.Customer.Name.Contains(search) ||
                                                                                                             p.InSystemID.Contains(search)),
                                                             EndDate = c.EndDate,
                                                             RemindBeforeEnd = (DateTime.Now >= DbFunctions.AddDays(c.EndDate, daysRemind * -1) && c.Status != ProjectStatus.Finished && c.Status != ProjectStatus.FinishedWithFaults),
                                                             ProjectEnd = (DateTime.Now > c.EndDate && c.Status != ProjectStatus.Finished && c.Status != ProjectStatus.FinishedWithFaults)
                                                         });

            IQueryable<ProjectsListViewModel> queryOrderBy = null;
            IQueryable<ProjectsListViewModel> queryWhere = null;
            IQueryable<ProjectsListViewModel> queryFilter = null;
            IQueryable<ProjectsListViewModel> querySkipTake = null;
            List<ProjectsListViewModel> toReturn = new List<ProjectsListViewModel>();

            switch (orderBy)
            {
                case "projectNumber":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProjectNumber);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProjectNumber);
                    break;

                case "street":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Street);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Street);
                    break;

                case "city":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.City);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.City);
                    break;

                case "status":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Status).ThenBy(q => q.EndDate);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Status).ThenBy(q => q.EndDate);
                    break;

                case "customer":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Customer);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Customer);
                    break;

                case "inSystemID":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.InSystemID);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.InSystemID);
                    break;

                default:
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProjectNumber);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProjectNumber);
                    break;
            }

            queryWhere = queryOrderBy.Where(p =>
                            p.ProjectNumber.Contains(search) ||
                            p.Street.Contains(search) ||
                            p.City.Contains(search) ||
                            p.Customer.Contains(search) ||
                            p.InSystemID.Contains(search)
                            //||
                            //p.HasMatchedChildrens
                            );

            if (filter.Contains(4))
                queryFilter = queryWhere.Where(p => filter.Contains((int)p.Status));
            else
                queryFilter = queryWhere.Where(p => filter.Contains((int)p.Status) && p.IsChecked == false);

            rows = queryFilter.Count();

            var reminder = new List<ProjectsListViewModel>();

            var projectEnds = queryFilter.Where(q => q.ProjectEnd).OrderBy(q => q.EndDate);
            var reminds = queryFilter.Where(q => !q.ProjectEnd && q.RemindBeforeEnd).OrderBy(q => q.EndDate);
            var others = queryFilter.Where(q => !q.ProjectEnd && !q.RemindBeforeEnd);

            reminder.AddRange(projectEnds);
            reminder.AddRange(reminds);
            reminder.AddRange(others);

            querySkipTake = reminder.AsQueryable().Skip((int)skip).Take((int)take);

            foreach (var q in querySkipTake)
            {
                var project = _db.Projects.Find(q.Id);
                var canDelete = true;

                foreach (var a in project.Apartments)
                {
                    if (a.Tasks.Where(x => x.Status != TaskStatus.Free).Count() != 0)
                        canDelete = false;
                }

                q.CanDelete = canDelete;

                toReturn.Add(q);
            }

            return toReturn.ToList();
        }

        public int? Create(Projects project)
        {
            try
            {
                project.CreateDate = DateTime.Now;
                var projectId = _db.Projects.Add(project);

                SaveChanges();

                var defaultProducts = _db.Products.Where(q => q.ForAllProjects).ToList();
                var projectProducts = new List<Product_Project>();

                defaultProducts.ForEach(x =>
                {
                    projectProducts.Add(new Product_Project
                    {
                        ProjectID = projectId.Id,
                        ProductID = x.Id,
                        ProjectNumber = project.ProjectNumber,
                        ProductName = x.Name,
                        Price = x.Price,
                        PercentType1 = x.PercentHeight.Type1,
                        PercentType2 = x.PercentHeight.Type2,
                        OnInvoiceName = x.DescriptionSE
                    });
                });

                _db.Product_Project.AddRange(projectProducts);

                AddProject_ProjectManager(projectId.Id, project.ProjectManagers);

                SaveChanges();

                return project.Id;
            }
            catch (DbEntityValidationException ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "CreateProject",
                    LogDate = DateTime.Now,
                    User = "a"
                };

                _db.ExceptionsLog.Add(log);

                _db.SaveChanges();
                throw ex;
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "CreateProject",
                    LogDate = DateTime.Now,
                    User = "a"
                };

                _db.ExceptionsLog.Add(log);

                _db.SaveChanges();
                return null;
            }
        }

        public async Task<string> Delete(int id)
        {
            var project = await GetProject(id);

            try
            {
                _db.Product_Project.RemoveRange(project.Product_Project);
                _db.Project_ProjectManager.RemoveRange(project.Project_ProjectManager);
                _db.Projects.Remove(project);
                SaveChanges();

                return "true;" + project.ProjectNumber;
            }
            catch (Exception ex)
            {
                return "false;" + ex.ToString();
            }
        }

        public bool Edit(Projects project, string userID)
        {
            var projectNowStatus = _db.Projects.Where(q => q.Id == project.Id).Select(q => q.Status).FirstOrDefault();

            if (projectNowStatus != project.Status)
            {
                var user = _db.ACSystemUser.Find(userID);

                var activity = new Project_Activity
                {
                    ProjectID = project.Id,
                    UserID = userID,
                    Message = $"Użytkownik {user.FullName} zmienił status projektu z {Dicts.StatusyProjektu[projectNowStatus]} na {Dicts.StatusyProjektu[project.Status]}.",
                    Timestamp = DateTime.Now
                };

                _db.Project_Activity.Add(activity);
            }

            try
            {
                DeleteProject_ProjectManager(project.Id);
                _db.Entry(project).State = EntityState.Modified;
                AddProject_ProjectManager(project.Id, project.ProjectManagers);
            }
            catch (Exception ex)
            {
                Service.Helpers.Log(ex, "Repo_Edit_Project", "ADMIN");
                return false;
            }

            return true;
        }

        public async Task<bool> IsAnyError(int id)
        {
            var error = false;

            var apartments = await _db.Apartments.Where(q => q.ProjectId == id).ToListAsync();

            foreach (var apartment in apartments)
            {
                var task = await _db.Tasks.Where(q => q.ApartmentId == apartment.Id && q.Status == TaskStatus.FinishedWithFaults).ToListAsync();

                if (task == null || task.Count() > 0) error = true;
            }

            return error;
        }

        public IEnumerable<Customers> GetCustomers()
        {
            return _db.Customers.Where(q => !q.IsLocked);
        }

        public IEnumerable<ProjectManagers> GetProjectManagers()
        {
            return _db.ProjectManagers;
        }

        public IEnumerable<ProjectManagerViewModel> GetProjectManagersByCustomerId(int id)
        {
            if (id == 0)
                return null;

            return _db.Customers.Where(q => q.Id == id).FirstOrDefault().ProjectManagers.Where(q => !q.IsLocked)
                                         .Select(q => new ProjectManagerViewModel { Id = q.Id, Name = q.Name, Email = q.Email, PhoneNumber = q.PhoneNumber });
        }

        public ProjectManagers ProjectManagerById(int id)
        {
            return _db.ProjectManagers.Find(id);
        }

        private void AddProject_ProjectManager(int projectId, List<int> projectManagersIds)
        {
            foreach (var projectManagerId in projectManagersIds)
            {
                var project_projectManager = new Project_ProjectManager
                {
                    ProjectId = projectId,
                    ProjectManagerId = projectManagerId
                };

                var result = _db.Project_ProjectManager.Add(project_projectManager);

                Console.WriteLine(result);
            }
        }

        private void DeleteProject_ProjectManager(int projectId)
        {
            var projectList = _db.Project_ProjectManager.Where(p => p.ProjectId == projectId).ToList();

            _db.Project_ProjectManager.RemoveRange(projectList);
        }

        public List<PayrollItems> PayrollItemsForProject(int projectID)
        {
            var payrollList = new List<PayrollItems>();

            var apartments = _db.Apartments.Where(q => q.ProjectId == projectID);

            foreach (var a in apartments)
            {
                var payrollItems = PayrollItemsForApartment(a.Id);
                payrollList.AddRange(payrollItems);
            }

            return payrollList;
        }

        public List<Invoice> InvoiceElementsForProject(int projectID)
        {
            var invoices = _db.Invoice.Where(q => q.ProjectID == projectID && q.Complete == true).ToList();

            return invoices;
        }

        public List<Fees> FeesForProject(int projectID)
        {
            var feesList = new List<Fees>();

            //var apartments = _db.Apartments.Where(q => q.ProjectId == projectID);

            //foreach (var a in apartments)
            //{
            //    var fees = FeesForApartment(a.Id);
            //    feesList.AddRange(fees);
            //}

            feesList = _db.Fees.Where(q => q.ProjectID == projectID).ToList();

            return feesList;
        }

        #endregion ADMIN

        #region FITTER

        [Obsolete]
        public List<Tasks> ListWithAvailableTask(string userId, bool showHistory)
        {
            var user = _db.ACSystemUser.Find(userId);

            if (user == null) return new List<Tasks>();

            var dateNow = DateTime.Parse(DateTime.Now.ToShortDateString());

            if (user.UserType == UserType.Wewnetrzny)
                return _db.Tasks.Where(
                    q =>
                    (
                        showHistory ||
                        (q.Status == TaskStatus.Free || q.Status == TaskStatus.Stoped || q.Status == TaskStatus.FinishedWithFaults || (q.Status == TaskStatus.PlaceNotReady || q.Status == TaskStatus.DoWyjasnienia && EntityFunctions.TruncateTime(q.Task_Fitter.FirstOrDefault(x => x.Status == TaskStatus.PlaceNotReady).Timestamp) != dateNow))) &&
                        q.ProductCount > 0 &&
                        (
                            user.CanSeeAll ||
                            (
                                q.Access_User_Task.Count(t => t.UserID.Equals(userId)) > 0 ||
                                q.Apartment.Access_User_Apartment.Count(a => a.UserID.Equals(userId)) > 0 ||
                                q.Apartment.Project.Access_User_Project.Count(p => p.UserID.Equals(userId)) > 0
                            )
                        )
                    ).ToList();
            else
            {
                var tasksList = new List<Tasks>();

                foreach (var tg in user.Task_AccessGrant)
                {
                    if ((showHistory || (tg.Task.Status == TaskStatus.Free || tg.Task.Status == TaskStatus.Stoped || tg.Task.Status == TaskStatus.FinishedWithFaults || tg.Task.Status == TaskStatus.PlaceNotReady || tg.Task.Status == TaskStatus.DoWyjasnienia)) && tg.Task.ProductCount > 0)
                        tasksList.Add(tg.Task);
                }

                return tasksList;
            }
        }

        #endregion FITTER

        #region BOTH

        public async Task<Projects> GetProject(int id)
        {
            return await _db.Projects.FindAsync(id);
        }

        public List<ProjectsSelect> GetProjectsList()
        {
            try
            {
                return _db.Projects.Where(q => q.IsChecked == false).Select(q => new ProjectsSelect { ID = q.Id, ProjectNumber = q.ProjectNumber }).ToList();
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return new List<ProjectsSelect>();
            }
        }

        #endregion BOTH

        #endregion PROJECTS

        #region APARTMENTS

        #region ADMIN

        public int ApartmentCreate(Apartments apartment)
        {
            if (_db.Apartments.Any(q => q.ProjectId == apartment.ProjectId && q.NumberOfApartment == apartment.NumberOfApartment && q.Letter == apartment.Letter) == false)
            {
                _db.Apartments.Add(apartment);
                SaveChanges();
            }

            return apartment.Id;
        }

        public void ApartmentEdit(Apartments apartment)
        {
            _db.Entry(apartment).State = EntityState.Modified;
            SaveChanges();
        }

        public void ApartmentDelete(int apartmentId)
        {
            var context = new ACSystemContext();

            //var tasks = context.Tasks.Where(q => q.ApartmentId == apartment.Id);
            //foreach (var task in tasks)
            //{
            //    var questions = context.Task_Questions.Find(task.Id);
            //    var users = context.Fitters.Where(q => q.TaskAlreadyWork == task.Id);

            //    try
            //    {
            //        if (questions != null)
            //            context.Task_Questions.Remove(questions);

            //        foreach (var user in users)
            //        {
            //            user.TaskAlreadyWork = null;
            //            context.Entry(user).State = EntityState.Modified;
            //        }

            //        context.Tasks.Remove(task);
            //        SaveChanges();
            //    }
            //    catch (Exception ex)
            //    {
            //        var log = new ExceptionsLog
            //        {
            //            Message = ex.Message,
            //            Type = ex.GetType().ToString(),
            //            Source = ex.Source,
            //            StackTrace = ex.StackTrace,
            //            URL = "ApartmentDelete",
            //            LogDate = DateTime.Now
            //        };

            //        context.ExceptionsLog.Add(log);

            //        context.SaveChanges();
            //    }
            //}
            var a = context.Apartments.Find(apartmentId);
            context.ReportNag.RemoveRange(a.ReportNags);
            context.Apartments.Remove(a);
            context.SaveChanges();
        }

        public Apartments ApartmentById(int id)
        {
            return _db.Apartments.Where(q => q.Id == id).FirstOrDefault();
        }

        public List<Apartments> ApartmentsList(int projectId)
        {
            return _db.Apartments.Include(q => q.Tasks).Where(q => q.ProjectId == projectId).ToList();
        }

        public List<PayrollItems> PayrollItemsForApartment(int apartmentID)
        {
            return _db.PayrollItems.Where(q => q.ApartmentID == apartmentID && q.Active == true).ToList();
        }

        public List<InvoiceElements> InvoiceElementsForApartment(int apartmentID)
        {
            var elems = new List<InvoiceElements>();

            var tasks = _db.Tasks.Where(q => q.ApartmentId == apartmentID);

            foreach (var task in tasks)
            {
                elems.AddRange(_db.InvoiceElements.Where(q => q.TaskID == task.Id && q.Complete == true));
            }

            return elems;
        }

        public List<Fees> FeesForApartment(int apartmentID)
        {
            //var apartment = _db.Apartments.Find(apartmentID);

            //if (apartment == null) return null;

            var feesList = new List<Fees>();

            //var tasks = apartment.Tasks;

            //foreach (var t in tasks)
            //{
            //    feesList.AddRange(t.Fees);
            //}

            feesList = _db.Fees.Where(q => q.ApartmentID == apartmentID).ToList();

            return feesList;
        }

        #endregion ADMIN

        #region BOTH

        public List<ApartmentsSelect> GetApartmentsList(int id)
        {
            return _db.Apartments.Where(q => q.ProjectId == id).Select(q => new ApartmentsSelect { ID = q.Id, ApartmentNumber = q.Letter + " " + q.NumberOfApartment }).ToList();
        }

        #endregion BOTH

        #endregion APARTMENTS

        #region TASKS

        #region ADMIN

        public List<TasksViewModel> TaskList(out int rows, int id, int? skip = default(int?), int? take = default(int?), string orderBy = "name", string dest = "asc", string search = "", string statuss = "3,4", string types = "0,1,2")
        {
            if (skip == null)
                skip = 0;

            if (take == null)
                take = 20;

            rows = 0;

            if (string.IsNullOrEmpty(statuss) && string.IsNullOrEmpty(types))
                return null;

            List<int> status = new List<int>();
            List<int> type = new List<int>();

            foreach (var fl in statuss.Split(','))
            {
                status.Add(Convert.ToInt32(fl));
            }

            foreach (var fl in types.Split(','))
            {
                type.Add(Convert.ToInt32(fl));
            }

            IQueryable<TasksViewModel> query = _db.Tasks.Where(q => q.Id == id).Include(q => q.Product).Select(q => new TasksViewModel()
            {
                Id = q.Id,
                ProductName = q.GetTaskName(q.Apartment.ProjectId),
                ProductCount = q.ProductCount,
                ProductPrice = q.ProductPrice,
                Jm = q.Product.Jm,
                Type = q.Product.ProductType,
                Status = q.Status
            });
            IQueryable<TasksViewModel> queryOrderBy = null;
            IQueryable<TasksViewModel> queryFilter = null;
            IQueryable<TasksViewModel> queryWhere = null;
            IQueryable<TasksViewModel> querySkipTake = null;

            switch (orderBy)
            {
                case "name":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProductName);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProductName);
                    break;

                case "count":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProductCount);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProductCount);
                    break;

                case "price":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProductPrice);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProductPrice);
                    break;

                case "jm":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Jm);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Jm);
                    break;

                case "type":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Type);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Type);
                    break;

                case "status":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Status);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Status);
                    break;

                default:
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProductName);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProductName);
                    break;
            }

            queryWhere = queryOrderBy.Where(p => p.ProductName.Contains(search));

            queryFilter = queryWhere.Where(p => status.Contains((int)p.Status) && type.Contains((int)p.Type));

            rows = queryFilter.Count();

            querySkipTake = queryFilter.Skip((int)skip).Take((int)take);

            return querySkipTake.ToList();
        }

        public int TaskTodoCount()
        {
            return _db.Tasks.Where(q => q.Todo == true).Count();
        }

        public List<TaskTodo> TaskTodo(out int rows, int? skip = default(int?), int? take = default(int?), string orderBy = "name", string dest = "asc", string search = "", string statuss = "0,1,2,3,4")
        {
            if (skip == null)
                skip = 0;

            if (take == null)
                take = 20;

            rows = 0;

            if (string.IsNullOrEmpty(statuss))
                return null;

            List<int> status = new List<int>();
            List<int> type = new List<int>();

            foreach (var fl in statuss.Split(','))
            {
                status.Add(Convert.ToInt32(fl));
            }

            IQueryable<TaskTodo> query = _db.Tasks.Where(q => q.Todo == true).Select(q => new TaskTodo()
            {
                Id = q.Id,
                ProductName = q.GetTaskName(q.Apartment.ProjectId),
                ProjectId = q.Apartment.ProjectId,
                ProjectNumber = q.Apartment.Project.ProjectNumber,
                ApartmentId = q.ApartmentId,
                ApartmentNumber = q.Apartment.NumberOfApartment,
                ClosedDate = q.ClosedDate.Value,
                Type = q.Product.ProductType,
                Status = q.Status
            });
            IQueryable<TaskTodo> queryOrderBy = null;
            IQueryable<TaskTodo> queryFilter = null;
            IQueryable<TaskTodo> queryWhere = null;
            IQueryable<TaskTodo> querySkipTake = null;
            List<TaskTodo> queryWithFitters = new List<TaskTodo>();

            switch (orderBy)
            {
                case "name":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProductName);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProductName);
                    break;

                case "project":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProjectNumber);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProjectNumber);
                    break;

                case "apartment":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ApartmentNumber);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ApartmentNumber);
                    break;

                case "status":
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.Status);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.Status);
                    break;

                default:
                    if (dest == "asc")
                        queryOrderBy = query.OrderBy(p => p.ProductName);
                    else
                        queryOrderBy = query.OrderByDescending(p => p.ProductName);
                    break;
            }

            queryWhere = queryOrderBy.Where(p => p.ProductName.Contains(search) || p.ProjectNumber.Contains(search) || p.ApartmentNumber.Contains(search));

            queryFilter = queryWhere.Where(p => status.Contains((int)p.Status));

            rows = queryFilter.Count();

            querySkipTake = queryFilter.Skip((int)skip).Take((int)take);

            foreach (var qr in querySkipTake)
            {
                var task_fitter = _db.Task_Fitter.OrderByDescending(q => q.Timestamp).FirstOrDefault(q => q.TaskId == qr.Id && q.IsActive);

                qr.FitterFinished = task_fitter?.Fitter?.User?.FullName;

                queryWithFitters.Add(qr);
            }

            return queryWithFitters;
        }

        public List<Products> ProductsList()
        {
            return _db.Products.ToList();
        }

        public string TaskCreate(Tasks task, bool Accepted = true, bool GetTask = false, string FitterID = "", int[] questions = null, bool saveChanges = true)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pl-PL");

            task.Product = _db.Products.Find(task.ProductId);

            switch (task.Production)
            {
                case TaskProduction.Complaint:
                    {
                        var count = _db.Tasks.Where(q => q.Production == TaskProduction.Complaint && q.ApartmentId == task.ApartmentId && q.Product.Name == task.Product.Name);
                        task.TaskSufix = "R" + (count.Count() + 1);
                        task.Blocked = true;
                        task.ProductPrice = 0;
                    }
                    break;

                case TaskProduction.Warranty:
                    {
                        var count = _db.Tasks.Where(q => q.Production == TaskProduction.Warranty && q.ApartmentId == task.ApartmentId && q.Product.Name == task.Product.Name);
                        task.TaskSufix = "G" + (count.Count() + 1);
                        task.ProductPrice = 0;
                    }
                    break;
            }

            task.Accepted = Accepted;

            if (Accepted == false)
            {
                var newTodo = new Todo
                {
                    FitterID = FitterID,
                    Type = TodoType.Akceptacja,
                    Date = DateTime.Now,
                    Checked = false
                };

                task.Todos.Add(newTodo);

                //Service.MailService.NewTodo(task, _db);
            }

            if (task.Production != TaskProduction.Developer && task.TaskOriginalId.HasValue == false)
            {
                Apartments apartment = _db.Apartments.Find(task.ApartmentId);
                Tasks originalTask = apartment.Tasks.FirstOrDefault(q => q.ProductId == task.ProductId);

                if (originalTask != null)
                    task.TaskOriginalId = originalTask.Id;
            }

            if (task.Production == TaskProduction.Developer && questions != null && questions.Length > 0)
            {
                foreach (int q in questions)
                {
                    task.Task_Question.Add(new Task_Question
                    {
                        QuestionID = q
                    });
                }
            }

            try
            {
                _db.Tasks.Add(task);

                if (saveChanges)
                    SaveChanges();

                task.Product = _db.Products.Find(task.ProductId);

                var taskApartment = _db.Apartments.Find(task.ApartmentId);

                if (taskApartment != null)
                {
                    var project = _db.Projects.Find(taskApartment.ProjectId);

                    if (project.IsChecked)
                    {
                        project.IsChecked = false;
                        _db.Entry(project).State = EntityState.Modified;

                        var user = _db.ACSystemUser.Find(FitterID);

                        var activity = new Project_Activity
                        {
                            ProjectID = project.Id,
                            UserID = FitterID,
                            Message = $"Projekt zmieniony na NIE SPRAWDZONY, użytkownik {user.FullName} dodał do projektu zadanie.",
                            Timestamp = DateTime.Now
                        };

                        _db.Project_Activity.Add(activity);
                    }
                }

                if (GetTask)
                {
                    var fitter = _db.Fitters.Find(FitterID);
                    fitter.TaskAlreadyWork = task.Id;
                    task.Status = TaskStatus.UnderWork;
                    task.Leader = FitterID;

                    _db.Entry(fitter).State = EntityState.Modified;
                    _db.Entry(task).State = EntityState.Modified;
                }

                if (saveChanges)
                    SaveChanges();

                if (task.Product.ProductGroups_Main.Any() && task.Production == TaskProduction.Developer)
                    MakeTaskGroup(task);

                return $"success:{task.Id}";
            }
            catch (DbEntityValidationException ex)
            {
                var Error = "";

                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Error += "Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage + Environment.NewLine;
                    }
                }

                return $"error:DbEntityValidationException#{Error}";
            }
            catch (EntityException ex)
            {
                return $"error:EntityException#{ex.ToString()}";
            }
            catch (DbUnexpectedValidationException ex)
            {
                return $"error:#DbUnexpectedValidationException{ex.InnerException}";
            }
            catch (UpdateException ex)
            {
                return $"error:UpdateException#{ex.Message}#{ex.InnerException}#{ex.ToString()}";
            }
            catch (DbUpdateException ex)
            {
                return $"error:DbUpdateException#{ex.Message}#{ex.InnerException}#{ex.ToString()}";
            }
            catch (Exception ex)
            {
                return $"error:Exception#{ex.Message}";
            }
        }

        private void MakeTaskGroup(Tasks task)
        {
            var list = new List<Tasks>();

            foreach (var g in task.Product.ProductGroups_Main)
            {
                var price = g.ProductSub.Price;

                if (g.ProductSub.Product_Project.Any(q => q.ProjectID == task.Apartment.ProjectId))
                    price = g.ProductSub.Product_Project.FirstOrDefault(q => q.ProjectID == task.Apartment.ProjectId).Price;

                var t = new Tasks
                {
                    ApartmentId = task.ApartmentId,
                    ProductId = g.ProductSubID,
                    StartDate = task.StartDate,
                    EndDate = task.EndDate,
                    ProductPrice = price,
                    Distance = task.Distance,
                    DistancePrice = task.DistancePrice,
                    IsHourlyPay = task.IsHourlyPay,
                    HourlyPrice = task.HourlyPrice,
                    PercentType1 = task.PercentType1,
                    PercentType2 = task.PercentType2,
                    PercentType3 = task.PercentType3,
                    PercentType4 = task.PercentType4,
                    PercentType5 = task.PercentType5,
                    PercentType6 = task.PercentType6,
                    ProductCount = g.Quantity,
                    Description = task.Description,
                    Blocked = true,
                    InvoiceDescription = task.InvoiceDescription,
                    InvoiceDescriptionPL = task.InvoiceDescriptionPL,
                    GroupTaskID = task.Id,
                    Accepted = true
                };

                list.Add(t);
            }

            _db.Tasks.AddRange(list);

            try
            {
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                var log = new ExceptionsLog
                {
                    Message = ex.Message,
                    Type = ex.GetType().ToString(),
                    Source = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = "DeleteGroupProduct",
                    LogDate = DateTime.Now
                };

                _db.ExceptionsLog.Add(log);

                _db.SaveChanges();
            }
        }

        public bool TaskPatchesCreate(List<TaskPatches> taskPatches)
        {
            try
            {
                _db.TaskPatches.AddRange(taskPatches);

                _db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
        }

        public async Task<string> TaskDelete(int id)
        {
            var task = await GetTask(id);
            var questions = _db.Task_Questions.Find(id);
            var users = _db.Fitters.Where(q => q.TaskAlreadyWork == id);

            try
            {
                if (questions != null)
                    _db.Task_Questions.Remove(questions);

                foreach (var user in users)
                {
                    user.TaskAlreadyWork = null;
                    _db.Entry(user).State = EntityState.Modified;
                }

                _db.Tasks.Remove(task);
                SaveChanges();

                return "true;" + task.Id;
            }
            catch (Exception ex)
            {
                return "false;" + ex.Message + " INNER EXCEPTION: " + ex.InnerException?.InnerException?.Message;
            }
        }

        public bool TaskEdit(Tasks task, string userID = "", bool canStatusChange = true, int[] questions = null, string[] answers = null)
        {
            try
            {
                if (task.Status == TaskStatus.FinishedWithFaults || task.Status == TaskStatus.PlaceNotReady || task.Status == TaskStatus.DoWyjasnienia)
                {
                    if (!string.IsNullOrEmpty(userID))
                    {
                        var newTodo = new Todo
                        {
                            TaskID = task.Id,
                            FitterID = userID,
                            Type = TodoType.ZmianaStatusu,
                            Date = DateTime.Now,
                            Checked = false
                        };

                        _db.Todo.Add(newTodo);
                    }
                    //if (task.Status == TaskStatus.FinishedWithFaults)
                    //    Service.MailService.NewTodo(task, _db);

                    task.ClosedDate = DateTime.Now;
                }

                if (!string.IsNullOrEmpty(task.ChangeReason) && canStatusChange && !string.IsNullOrEmpty(userID))
                {
                    var actuallyWorkDone = _db.Task_Fitter.Where(q => q.TaskId == task.Id).ToList().Sum(q => q.WorkDone);

                    var task_fitter = new Task_Fitter
                    {
                        FitterId = userID,
                        Status = task.Status,
                        TaskId = task.Id,
                        WorkDone = task.ProductCount - actuallyWorkDone,
                        WorkTime = 0,
                        Timestamp = DateTime.Now,
                        IsActive = true,
                        DayChecked = false
                    };

                    _db.Task_Fitter.Add(task_fitter);
                }

                #region QUESTIONS

                List<Task_Question> task_Questions = _db.Task_Question.Where(q => q.TaskID == task.Id).ToList();
                Dictionary<int, int> answersDict = answers?.ToDoubleIntDictionary('-') ?? new Dictionary<int, int>();

                if (questions != null && questions.Length > 0)
                {
                    List<Task_Question> questionsToAdd = new List<Task_Question>();
                    List<Task_Question> questionsToRemove = new List<Task_Question>();

                    foreach (int q in questions)
                    {
                        int? answerID = (answersDict.ContainsKey(q)) ? (int?)answersDict[q] : null;

                        if (!task_Questions.Any(x => x.QuestionID == q))
                            questionsToAdd.Add(new Task_Question { TaskID = task.Id, QuestionID = q, AnswerID = answerID });
                        else
                        {
                            Task_Question selectedTaskQuestion = task_Questions.FirstOrDefault(x => x.QuestionID == q);
                            selectedTaskQuestion.AnswerID = answerID;
                            _db.Entry(selectedTaskQuestion).State = EntityState.Modified;
                        }
                    }

                    foreach (Task_Question q in task_Questions)
                    {
                        if (!questions.Contains(q.QuestionID))
                            questionsToRemove.Add(q);
                    }

                    if (questionsToRemove.Count > 0)
                        _db.Task_Question.RemoveRange(questionsToRemove);

                    _db.Task_Question.AddRange(questionsToAdd);
                }
                else if (task_Questions.Count > 0)
                {
                    _db.Task_Question.RemoveRange(task_Questions);
                }

                #endregion QUESTIONS

                #region CHANGE PROJECT STATUS

                if (task.Apartment == null)
                    task.Apartment = _db.Apartments.Find(task.ApartmentId);

                var projectID = task.Apartment.ProjectId;

                var isAnyNotFinished = _db.Tasks.Any(q => q.Apartment.ProjectId == projectID && q.Id != task.Id && (q.Status == TaskStatus.Free || q.Status == TaskStatus.Stoped || q.Status == TaskStatus.UnderWork || q.Status == TaskStatus.Planned));

                if (isAnyNotFinished == false && task.Status != TaskStatus.Stoped && task.Status != TaskStatus.Free && task.Status != TaskStatus.UnderWork)
                {
                    var project = _db.Projects.Find(projectID);

                    if (_db.Tasks.Any(q => q.Apartment.ProjectId == projectID && q.Id != task.Id && (q.Status == TaskStatus.FinishedWithFaults || q.Status == TaskStatus.PlaceNotReady || task.Status == TaskStatus.FinishedWithFaults || task.Status == TaskStatus.PlaceNotReady)))
                    {
                        project.Status = ProjectStatus.FinishedWithFaults;
                    }
                    else
                    {
                        project.Status = ProjectStatus.Finished;
                    }

                    _db.Entry(project).State = EntityState.Modified;
                }
                else if (isAnyNotFinished == false && (task.Status == TaskStatus.Free || task.Status == TaskStatus.Stoped || task.Status == TaskStatus.UnderWork))
                {
                    var project = _db.Projects.Find(projectID);

                    project.Status = ProjectStatus.UnderWork;

                    _db.Entry(project).State = EntityState.Modified;
                }

                #endregion CHANGE PROJECT STATUS

                _db.Entry(task).State = EntityState.Modified;

                SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Service.Helpers.Log(ex, "TaskEdit", "Admin");
                return false;
            }
        }

        public Tasks GetTaskNoAsync(int id)
        {
            var task = _db.Tasks.Find(id);

            return _db.Tasks.Find(id);
        }

        public List<Tasks> TaskListByApartment(int ApartmentId)
        {
            return _db.Tasks.Where(q => q.ApartmentId == ApartmentId).ToList();
        }

        public List<TasksViewModel> TaskListByWeek(int week)
        {
            return _db.Tasks
                //.Where(q => SqlFunctions.DatePart("week", q.StartDate) == week || SqlFunctions.DatePart("week", q.EndDate) == week)
                .Select(q => new TasksViewModel()
                {
                    Id = q.Id,
                    ProductName = q.GetTaskName(q.Apartment.ProjectId),
                    ProductCount = q.ProductCount,
                    ProductPrice = q.ProductPrice,
                    Jm = q.Product.Jm,
                    Week = SqlFunctions.DatePart("iso_week", q.StartDate) ?? 0,
                    Type = q.Product.ProductType,
                    Status = q.Status
                })
                .ToList();
        }

        public bool CreateQuestions(int taskId)
        {
            var questions = new Task_Questions
            {
                QuestionId = taskId,
                BaseUnits = 0,
                WallUnits = 0,
                TallUnits = 0,
                Fillers = 0,
                SidePiece = 0,
                Worktop = 0,
                Sink = 0,
                Handles = 0,
                Plinth = 0,
                Grids = 0,
                Lighting = 0,
                Silicon = 0,
                Manuals = 0,
                Machines = 0,
                Oven = TaskQuestionStatus.Nie_Dotyczy,
                FridgeFreezer1 = TaskQuestionStatus.Nie_Dotyczy,
                FridgeFreezer2 = TaskQuestionStatus.Nie_Dotyczy,
                Hob = TaskQuestionStatus.Nie_Dotyczy,
                Dishwasher = TaskQuestionStatus.Nie_Dotyczy,
                Mikrowave = TaskQuestionStatus.Nie_Dotyczy,
                Fan = TaskQuestionStatus.Nie_Dotyczy,
                CeillingScribe = TaskQuestionStatus.Nie_Dotyczy
            };

            _db.Task_Questions.Add(questions);
            _db.SaveChanges();

            return true;
        }

        public bool EditQuestion(string query)
        {
            try
            {
                var result = _db.Database.ExecuteSqlCommand(query);

                if (result > 0) return true; else return false;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
        }

        public bool EditPatch(string query)
        {
            try
            {
                var result = _db.Database.ExecuteSqlCommand(query);

                if (result > 0) return true; else return false;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
        }

        public bool TodoCheck(int ID)
        {
            var todo = _db.Todo.Find(ID);

            if (todo == null)
                return false;

            todo.Checked = true;

            _db.Entry(todo).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
        }

        public bool AnyTaskToDo()
        {
            //bool anyTodo = this._db.Todo.Any(q => q.Checked == false && (q.Task != null || q.Type == TodoType.Wiadomosc || q.Type == TodoType.NowyBlad));
            var todos = this._db.Todo.Where(q => q.Checked == false && (q.Task != null || q.Type == TodoType.Wiadomosc || q.Type == TodoType.NowyBlad)).ToList();
            return todos.Any(q => q.Checked == false && (q.Task != null || q.Type == TodoType.Wiadomosc || q.Type == TodoType.NowyBlad));
        }

        #endregion ADMIN

        #region FITTER

        public async Task<Tasks> GetTask(int id)
        {
            var task = _db.Tasks.Find(id);

            return await _db.Tasks.FindAsync(id);
        }

        public async Task<Tasks> GetCurrentTask(string fitterId)
        {
            var taskId = _db.Fitters.Find(fitterId).TaskAlreadyWork;

            if (taskId != null)
            {
                var task = _db.Tasks.Find(taskId);

                return await _db.Tasks.FindAsync(taskId);
            }
            else
                return null;
        }

        public bool IsFitterBusy(string userId)
        {
            var user = _db.Fitters.Find(userId);

            return user.TaskAlreadyWork != null;
        }

        public void ChangeStatus(List<FitterWorkDoneViewModel> workDone, decimal Distance, TaskErrorType errorType, TaskStatus status, string[] photos, string[] photosDesc, string[] photosQuestion, string[] photosQuestionDesc, string[] question, Task_Questions questions, DateTime? handDateTime, string[] answers, string[] additionalInformations, string reportUserId)
        {
            var firstFitter = _db.Fitters.Find(workDone.FirstOrDefault().FitterID);
            var currentTask = _db.Tasks.Find(firstFitter.TaskAlreadyWork);

            currentTask.ErrorType = errorType;
            currentTask.Distance = Distance;

            if (photosQuestion != null && photosQuestionDesc != null)
            {
                var photosQuestionCount = photosQuestion.Count();

                var newContext = _db.CreateContext();

                for (var i = 0; i < photosQuestionCount; i++)
                {
                    var taskQuestionPhotos = new TaskQuestionPhoto
                    {
                        Task_QuestionsID = currentTask.Id,
                        Photo = photosQuestion[i],
                        Description = photosQuestionDesc[i],
                        Question = question[i],
                        Blocked = false,
                        InService = false
                    };

                    newContext.TaskQuestionPhoto.Add(taskQuestionPhotos);

                    newContext.SaveChanges();
                }

                if (photosQuestionCount > 0)
                {
                    var newTodo = new Todo
                    {
                        TaskID = currentTask.Id,
                        FitterID = reportUserId,
                        Type = TodoType.TlumaczeniaPytanKontrolnych,
                        Date = DateTime.Now,
                        Checked = false
                    };

                    _db.Todo.Add(newTodo);
                }
            }

            if (status == TaskStatus.FinishedWithFaults || status == TaskStatus.PlaceNotReady)
            {
                currentTask.ClosedDate = (handDateTime.HasValue) ? handDateTime.Value : DateTime.Now;

                var newTodo = new Todo
                {
                    TaskID = currentTask.Id,
                    FitterID = reportUserId,
                    Type = TodoType.ZakoczenieZBledami,
                    ErrorType = (status == TaskStatus.PlaceNotReady) ? "Miejsce nieprzygotowane" : (errorType == TaskErrorType.Quantity) ? "Błąd ilościowy" : "----",
                    Date = DateTime.Now,
                    Checked = false
                };

                _db.Todo.Add(newTodo);
            }
            else if (status == TaskStatus.Finished)
            {
                currentTask.ClosedDate = (handDateTime.HasValue) ? handDateTime.Value : DateTime.Now;

                var needToFindStatus = currentTask.Task_Fitter.OrderByDescending(q => q.Timestamp).FirstOrDefault(q => q.Status != TaskStatus.UnderWork);

                if (needToFindStatus != null && needToFindStatus.Status == TaskStatus.FinishedWithFaults)
                {
                    _db.Todo.Add(new Todo
                    {
                        TaskID = currentTask.Id,
                        FitterID = reportUserId,
                        Type = TodoType.PoprawionyBlad,
                        Date = DateTime.Now,
                        Checked = false
                    });
                }
            }
            else if (status == TaskStatus.Stoped) {
                var newTodo = new Todo {
                    TaskID = currentTask.Id,
                    FitterID = reportUserId,
                    Type = TodoType.ZatrzymaneZadanie,
                    Date = DateTime.Now,
                    Checked = false
                };

                _db.Todo.Add(newTodo);
            }

            var task_fitterList = new List<Task_Fitter>();

            foreach (var done in workDone)
            {
                var task_fitter = new Task_Fitter
                {
                    TaskId = currentTask.Id,
                    FitterId = done.FitterID,
                    WorkDone = done.WorkDone,
                    WorkTime = done.WorkTime,
                    RealWorkTime = done.RealWorkTime,
                    Status = status,
                    Timestamp = DateTime.Now,
                    IsActive = true,
                    DayChecked = false,
                    HandDateTime = (handDateTime.HasValue) ? handDateTime.Value : DateTime.Now,
                    ReportUser = reportUserId
                };

                var fitter = _db.Fitters.Find(done.FitterID);
                Service.MailService.TaskActivity(fitter.User.Email, currentTask.Apartment.Project.ProjectNumber,
                    currentTask.Apartment.NumberOfApartment, currentTask.GetTaskName(currentTask.Apartment.ProjectId) + " " + currentTask.TaskSufix,
                    done.WorkDone, done.WorkTime / 60, DateTime.Now, status.ToString());

                if (status != TaskStatus.Free && status != TaskStatus.UnderWork)
                    task_fitterList.Add(task_fitter);
            }

            _db.Task_Fitter.AddRange(task_fitterList);

            currentTask.Status = status;

            if ((currentTask.Production == TaskProduction.Complaint) && currentTask.TaskOriginalId != null)
            {
                var originalTask = _db.Tasks.Find(currentTask.TaskOriginalId);

                var sourceFitter = originalTask.Task_Fitter.OrderBy(q => q.Timestamp).FirstOrDefault();

                if (sourceFitter?.FitterId != firstFitter.Id)
                {
                    var lastFee = _db.Fees.OrderByDescending(q => q.FeeNumber).FirstOrDefault()?.FeeNumber ?? 0;

                    var plusFee = new Fees
                    {
                        FeeNumber = lastFee + 1,
                        ProjectID = currentTask.Apartment.ProjectId,
                        ApartmentID = currentTask.ApartmentId,
                        TaskID = currentTask.Id,
                        FitterID = firstFitter.Id,
                        FeeDate = DateTime.Now,
                        Description = $"Kwota dodana dla {firstFitter.User.FullName}, odjęta dla {sourceFitter?.Fitter?.User?.FullName}",
                        FeeAmount = ((workDone.FirstOrDefault().WorkTime / 60) * 150) * -1,
                        Status = FeeStatus.DoPotracenia
                    };
                    _db.Fees.Add(plusFee);

                    if (sourceFitter != null)
                    {
                        var minusFee = new Fees
                        {
                            FeeNumber = lastFee + 2,
                            ProjectID = currentTask.Apartment.ProjectId,
                            ApartmentID = currentTask.ApartmentId,
                            TaskID = currentTask.Id,
                            FitterID = sourceFitter.FitterId,
                            FeeDate = DateTime.Now,
                            Description = $"Kwota odjęta dla {sourceFitter.Fitter.User.FullName}, dodana dla {firstFitter.User.FullName}",
                            FeeAmount = (workDone.FirstOrDefault().WorkTime / 60) * 150,
                            Status = FeeStatus.DoPotracenia
                        };
                        _db.Fees.Add(minusFee);
                    }
                }

                currentTask.IsInFees = true;

                if (status == TaskStatus.Finished)
                {
                    var serviceTasks = _db.Tasks.Where(q => q.TaskOriginalId == currentTask.TaskOriginalId && q.Id != currentTask.Id && q.Status != TaskStatus.Finished);

                    if (!serviceTasks.Any())
                    {
                        originalTask.Status = TaskStatus.FinishedWithFaultsCorrected;

                        _db.Entry(originalTask).State = EntityState.Modified;
                    }
                }
            }

            #region CHANGE PROJECT STATUS

            if (currentTask.Apartment == null)
                currentTask.Apartment = _db.Apartments.Find(currentTask.ApartmentId);

            var projectID = currentTask.Apartment.ProjectId;

            var isAnyNotFinished = _db.Tasks.Any(q => q.Apartment.ProjectId == projectID && q.Id != currentTask.Id && (q.Status == TaskStatus.Free || q.Status == TaskStatus.Stoped || q.Status == TaskStatus.UnderWork));

            if (isAnyNotFinished == false && currentTask.Status != TaskStatus.Stoped)
            {
                var project = _db.Projects.Find(projectID);

                if (_db.Tasks.Any(q => q.Apartment.ProjectId == projectID && q.Id != currentTask.Id && (q.Status == TaskStatus.FinishedWithFaults || q.Status == TaskStatus.PlaceNotReady || currentTask.Status == TaskStatus.FinishedWithFaults || currentTask.Status == TaskStatus.PlaceNotReady)))
                {
                    project.Status = ProjectStatus.FinishedWithFaults;
                }
                else
                {
                    project.Status = ProjectStatus.Finished;
                }

                _db.Entry(project).State = EntityState.Modified;
            }

            #endregion CHANGE PROJECT STATUS

            #region QUESTIONS

            List<Task_Question> task_Questions = _db.Task_Question.Where(q => q.TaskID == currentTask.Id).ToList();
            Dictionary<int, int> answersDict = answers?.ToDoubleIntDictionary('-', new string[2] { "ER-", "OK-" }) ?? new Dictionary<int, int>();
            List<int> questionsIDs = answersDict.Select(q => q.Key).ToList();

            if (answersDict.Any())
            {
                foreach (Task_Question q in task_Questions)
                {
                    int answer = answersDict[q.ID];
                    string additionalInformation = additionalInformations[questionsIDs.IndexOf(q.ID)];
                    int? idArrayIndex = question?.ToList().IndexOf(q.ID.ToString());

                    q.AnswerID = (answer > 0) ? (int?)answer : null;
                    q.AdditionalInformation = additionalInformation;

                    if (idArrayIndex.HasValue && idArrayIndex.Value >= 0)
                        q.Photo = photosQuestion[idArrayIndex.Value];


                    _db.Entry(q).State = EntityState.Modified;
                }
            }

            #endregion QUESTIONS

            SaveChanges();

            if (errorType == TaskErrorType.MakeFillTask)
            {
                var fillTask = new Tasks
                {
                    ApartmentId = currentTask.ApartmentId,
                    ProductId = currentTask.ProductId,
                    TaskOriginalId = currentTask.TaskOriginalId,
                    StartDate = DateTime.Parse(currentTask.StartDate.ToString("yyyy-MM-dd")),
                    EndDate = DateTime.Parse(currentTask.EndDate.AddDays(7).ToString("yyyy-MM-dd")),
                    ProductPrice = currentTask.ProductPrice,
                    PercentType1 = currentTask.PercentType1,
                    PercentType2 = currentTask.PercentType2,
                    PercentType3 = currentTask.PercentType3,
                    PercentType4 = currentTask.PercentType4,
                    PercentType5 = currentTask.PercentType5,
                    PercentType6 = currentTask.PercentType6,
                    ProductCount = currentTask.ProductCount - (currentTask.Task_Fitter.Sum(q => q.WorkDone)),
                    Status = TaskStatus.Free,
                    Production = currentTask.Production,
                    ErrorType = TaskErrorType.None,
                    Leader = "",
                    Payroll = false,
                    Invoice = false,
                    ClosedDate = null
                };

                _db.Tasks.Add(fillTask);
            }
            else if (errorType == TaskErrorType.Quantity)
            {
                currentTask.Status = TaskStatus.Finished;
                currentTask.ProductCount = currentTask.Task_Fitter.Where(q => q.IsActive).Sum(q => q.WorkDone);
                currentTask.Accepted = false;
                currentTask.Todo = true;
            }

            if (photos != null)
            {
                var photosCount = photos.Count();

                for (var i = 0; i < photosCount; i++)
                {
                    var taskActivityPhoto = new TaskActivityPhotos
                    {
                        Task_FitterId = task_fitterList.Where(q => q.FitterId == currentTask.Leader).FirstOrDefault().Id,
                        Photo = photos[i],
                        Desc = photosDesc[i],
                        Blocked = false
                    };

                    _db.TaskActivityPhotos.Add(taskActivityPhoto);
                    SaveChanges();
                }
            }

            currentTask.Leader = string.Empty;

            _db.Entry(currentTask).State = EntityState.Modified;

            var taskFitters = _db.Fitters.Where(q => q.TaskAlreadyWork == currentTask.Id);

            foreach (var fitter in taskFitters)
            {
                fitter.TaskAlreadyWork = null;
                _db.Entry(fitter).State = EntityState.Modified;
            }

            if (currentTask.Product.ProductType == TaskType.Kitchen)
            {
                questions.QuestionId = currentTask.Id;
                var task_questions = _db.Task_Questions.Find(currentTask.Id);

                if (task_questions != null)
                {
                    _db.Task_Questions.Remove(task_questions);
                    _db.Task_Questions.Add(questions);
                }
            }

            SaveChanges();
        }

        public List<FittersSelect> GetTaskFittersList(int id)
        {
            return _db.Fitters.Where(q => q.TaskAlreadyWork == id).Select(q => new FittersSelect { ID = q.Id, Name = q.User.Name, SurName = q.User.Surname }).ToList();
        }

        public Fitters GetFitter(string id)
        {
            return _db.Fitters.Find(id);
        }

        public void LeaveTask(string userId, decimal done)
        {
            var fitter = _db.Fitters.Find(userId);

            var task_fitter = new Task_Fitter
            {
                TaskId = fitter.Task.Id,
                FitterId = userId,
                WorkDone = done,
                Status = fitter.Task.Status,
                Timestamp = DateTime.Now,
                IsActive = true,
                DayChecked = false
            };

            _db.Task_Fitter.Add(task_fitter);
            fitter.TaskAlreadyWork = null;

            _db.Entry(fitter).State = EntityState.Modified;

            _db.SaveChanges();
        }

        #endregion FITTER

        #region BOTH

        public List<FittersSelect> GetFreeFitters()
        {
            return _db.Fitters.Where(q => q.TaskAlreadyWork == null && q.User.LockoutEnabled == false)
                .Select(q => new FittersSelect { ID = q.Id, Name = q.User.Name, SurName = q.User.Surname, IsLocked = q.User.LockoutEnabled }).ToList();
        }

        public List<FittersSelect> GetFitters()
        {
            return _db.Fitters
                .Select(q => new FittersSelect { ID = q.Id, Name = q.User.Name, SurName = q.User.Surname, IsLocked = q.User.LockoutEnabled }).ToList();
        }

        public List<FittersSelect> GetOuterFitters()
        {
            return _db.ACSystemUser.Where(q => q.UserType == UserType.Zewnetrzny).Where(q => q.LockoutEnabled == false)
                .Select(q => new FittersSelect() { ID = q.Id, Name = q.Name, SurName = q.Surname, IsLocked = q.LockoutEnabled }).ToList();
        }

        public bool FitterTaskGet(int taskId, string userId)
        {
            var currentFitter = _db.Fitters.Find(userId);
            var task = _db.Tasks.Find(taskId);

            var project = task.Apartment.Project;

            if (project.Status == ProjectStatus.NotStarted)
            {
                project.Status = ProjectStatus.UnderWork;
                _db.Entry(project).State = EntityState.Modified;
            }

            if (string.IsNullOrEmpty(task.Leader))
                task.Leader = userId;

            var task_fitter = new Task_Fitter
            {
                TaskId = taskId,
                FitterId = userId,
                Status = TaskStatus.UnderWork,
                Timestamp = DateTime.Now,
                WorkDone = 0,
                IsActive = true,
                DayChecked = false
            };

            try
            {
                _db.Task_Fitter.Add(task_fitter);
            }
            catch (Exception ex)
            {
                Service.Helpers.Log(ex, "GET_TASK", "");
                return false;
            }

            task.Status = TaskStatus.UnderWork;

            try
            {
                _db.Entry(task).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                Service.Helpers.Log(ex, "GET_TASK", "");
                return false;
            }

            currentFitter.TaskAlreadyWork = taskId;

            try
            {
                _db.Entry(currentFitter).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                Service.Helpers.Log(ex, "GET_TASK", "");
                return false;
            }

            try
            {
                SaveChanges();
            }
            catch (Exception ex)
            {
                Service.Helpers.Log(ex, "GET_TASK", "");
                return false;
            }

            return true;
        }

        public bool AccessGrantToTask(int taskId, string userId)
        {
            var accessGrant = new Task_AccessGrant()
            {
                TaskID = taskId,
                FitterID = userId
            };

            _db.Task_AccessGrant.Add(accessGrant);

            try
            {
                _db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
        }

        #endregion BOTH

        #endregion TASKS

        #region OTHER

        public List<ActivityPhotosViewModel> GetActivityPhotos(int id)
        {
            var dir = "/Monter/dist/Task_Images/";

            //#if DEBUG
            //    dir = "http://localhost:50993/dist/";
            //#endif

            return _db.TaskActivityPhotos.Where(q => q.Task_FitterId == id).Select(q => new ActivityPhotosViewModel { ID = q.Id, Photo = dir + q.Photo, Desc = q.Desc }).ToList();
        }

        public List<QuestionPhotosViewModel> GetQuestionPhotos(int id, string question)
        {
            var dir = "/Monter/dist/Question_Images/";

            //#if DEBUG
            //    dir = "http://localhost:50993/dist/";
            //#endif

            return _db.TaskQuestionPhoto.Where(q => q.Task_QuestionsID == id && q.Question == question).Select(q => new QuestionPhotosViewModel { ID = q.Id, Photo = dir + q.Photo, Desc = q.Description, Translated = q.TranslatedDescription }).ToList();
        }

        public bool AddEvent(Events events)
        {
            try
            {
                _db.Events.Add(events);
                SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
        }

        public Products GetProduct(int id)
        {
            return _db.Products.Find(id);
        }

        public bool CanUserReportHours(string userId, DateTime date, decimal hours) {
            var records = _db.Task_Fitter.Where(q => q.FitterId.Equals(userId) && EntityFunctions.TruncateTime(q.HandDateTime) == EntityFunctions.TruncateTime(date));

            var doneHours = (records.Any()) ? records.Sum(q => q.WorkTime) : 0;

            return doneHours + hours <= 16 * 60;
        }

        #endregion OTHER

        public void SaveChanges() => _db.SaveChanges();

        public void Dispose() => _db.Dispose();
    }
}