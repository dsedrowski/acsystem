﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ACSystem.Core.Repo
{
    public class HomeRepo : IHomeRepo
    {
        private readonly IACSystemContext _db;

        public HomeRepo(IACSystemContext db)
        {
            _db = db;
        }

        public IQueryable<Tasks> TasksList()
        {
            return _db.Tasks;
        }

        public IQueryable<Tasks> TasksList(int apartmentId)
        {
            return _db.Tasks.Where(q => q.ApartmentId == apartmentId).AsQueryable();
        }

        public async Task<Tasks> GetTask(int id)
        {
            return await _db.Tasks.FindAsync(id);
        }

        public IQueryable<Events> EventsList()
        {
            return _db.Events;
        }

        public async Task<Events> GetEvent(int id)
        {
            return await _db.Events.FindAsync(id);
        }

        public bool TaskEdit(Tasks task)
        {
            try
            {
                _db.Entry(task).State = EntityState.Modified;
                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
        }

        public bool EventEdit(Events events)
        {
            try
            {
                _db.Entry(events).State = EntityState.Modified;
                return true;
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "", "");
                return false;
            }
        }

        public IQueryable<Projects> ProjectsList()
        {
            return _db.Projects.AsQueryable();
        }

        public IQueryable<Apartments> ApartmentsList(int projectId)
        {
            return _db.Apartments.Where(q => q.ProjectId == projectId).AsQueryable();
        }

        public IACSystemContext GetContext()
        {
            return _db;
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }
    }
}