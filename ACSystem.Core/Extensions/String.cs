﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Extensions
{
    public static class StringsExtension
    {
        public static int[] ToIntArray(this string text, char separator)
        {
            var array = new List<int>();

            if (string.IsNullOrEmpty(text))
                return array.ToArray();

            var split = text.Split(separator);

            foreach (var s in split)
            {
                int elem = 0;

                if (int.TryParse(s, out elem))
                    array.Add(elem);
            }

            return array.ToArray();
        }

        public static Dictionary<int, int> ToDoubleIntDictionary(this string[] array, char separator, string[] removeString = null)
        {
            Dictionary<int, int> dict = new Dictionary<int, int>();

            foreach (string e in array)
            {
                string toSplitString = e;

                if (removeString != null)
                    foreach (string r in removeString)
                        toSplitString = toSplitString.Replace(r, "");

                string[] split = toSplitString.Split(separator);
                int key, value = -1;
                int.TryParse(split[1], out value);

                if (int.TryParse(split[0], out key))
                    dict.Add(key, value);
            }

            return dict;
        }

        public static bool IsNullOrEmpty(this string text)
        {
            return text == null || text.Equals(string.Empty);
        }

        public static string GetServerUrl(this Uri uri)
        {
            return uri.Scheme + Uri.SchemeDelimiter + uri.Host + ":" + uri.Port;
        }
    }
}