﻿using ACSystem.Core.Models;
using ACSystem.Core.Service;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace ACSystem.Core.Extensions
{
    public static class Helpers
    {
        public static MvcHtmlString CreateSerwisElementRow(this HtmlHelper helper, string element, string elementName, string desc, string code, string room, bool isChecked = false, string elementStatus = "none")
        {
            var check = (isChecked) ? "checked" : "";

            var row = $@"<tr class='{elementStatus}'>
                            <td><input type='text' class='form-control' name='{element}-code' value='{code}' /></td>
                            <td><input type='text' class='form-control' name='{element}-room' value='{room}' /></td>
                            <th>{elementName}</th>                            
                            <td><input type='text' class='form-control' name='{element}-desc' value='{desc}'/><input type='hidden' name='{element}-hidden' value='{elementName}'/></td>                            
                            <td><input type='checkbox' name='{element}-check' value='true' {check}/></td>
                        </tr>";

            return MvcHtmlString.Create(row);
        }

        public static MvcHtmlString RaportQuestions(this HtmlHelper helper, Task_Questions model, Leanguage lang)
        {
            if (model == null)
                return MvcHtmlString.Create("");

            var i = 1;
            var row = String.Empty;

            foreach (var prop in model.QuestionsList)
            {
                var propValue = model.GetPropValue(prop);

                if ((TaskQuestionStatus)propValue != TaskQuestionStatus.Nie_Dotyczy)
                {
                    var question = Objects.GetDisplayName<Task_Questions>(prop);
                    string[] statusy = { "NIE", "TAK" };

                    var descs = model.TaskQuestionPhoto.Where(q => q.Question == prop).ToList();

                    if (lang == Leanguage.English)
                    {
                        question = Dicts.Eng_Questions[question];
                        statusy[0] = "NOT OK";
                        statusy[1] = "OK";
                    }
                    else if (lang == Leanguage.Swedish)
                    {
                        question = Dicts.Swe_Questions[question];
                        statusy[0] = "EJ OK";
                        statusy[1] = "OK";
                    }

                    row += RaportQuestionRow(i, question, (TaskQuestionStatus)propValue, descs, statusy);
                    i++;
                }
            }

            if (model.Machines == TaskQuestionStatus.Tak)
            {
                foreach (var prop in model.MachinesList)
                {
                    var propValue = model.GetPropValue(prop);

                    if ((TaskQuestionStatus)propValue != TaskQuestionStatus.Nie_Dotyczy)
                    {
                        var question = Objects.GetDisplayName<Task_Questions>(prop);
                        string[] statusy = { "NIE", "TAK" };

                        var descs = model.TaskQuestionPhoto.Where(q => q.Question == prop).ToList();

                        if (lang == Leanguage.English)
                        {
                            question = Dicts.Eng_Questions[question];
                            statusy[0] = "NOT OK";
                            statusy[1] = "OK";
                        }
                        else if (lang == Leanguage.Swedish)
                        {
                            question = Dicts.Swe_Questions[question];
                            statusy[0] = "EJ OK";
                            statusy[1] = "OK";
                        }

                        row += RaportQuestionRow(i, question, (TaskQuestionStatus)propValue, descs, statusy);
                        i++;
                    }
                }
            }

            return MvcHtmlString.Create(row);
        }

        public static MvcHtmlString RaportQuestions(this HtmlHelper helper, List<TaskPatches> model, Leanguage lang)
        {
            var i = 1;
            var row = String.Empty;

            foreach (var prop in model)
             {
                var question = prop.ItemTitle;
                string[] statusy = { "NIE", "TAK" };

                if (lang == Leanguage.English)
                {
                    question = Dicts.Eng_Questions[question];
                    statusy[0] = "NOT OK";
                    statusy[1] = "OK";
                }
                else if (lang == Leanguage.Swedish)
                {
                    question = Dicts.Swe_Questions[question];
                    statusy[0] = "EJ OK";
                    statusy[1] = "OK";
                }

                row += RaportQuestionRow(i, question, (prop.Done) ? 1 : 0, statusy);
                i++;
            }

            return MvcHtmlString.Create(row);
        }

        public static MvcHtmlString RaportQuestionsAllStates(this HtmlHelper helper, Task_Questions model)
        {
            var i = 1;
            var row = String.Empty;

            foreach (var prop in model.QuestionsList)
            {
                var propValue = model.GetPropValue(prop);

                var question = Objects.GetDisplayName<Task_Questions>(prop);
                string[] statusy = { "NIE", "TAK", "NIE DOTYCZY" };

                var descs = model.TaskQuestionPhoto.Where(q => q.Question == prop).ToList();


                row += RaportQuestionRow(i, question, (TaskQuestionStatus)propValue, descs, statusy);
                i++;
            }

            if (model.Machines == TaskQuestionStatus.Tak)
            {
                foreach (var prop in model.MachinesList)
                {
                    var propValue = model.GetPropValue(prop);

                    var question = Objects.GetDisplayName<Task_Questions>(prop);
                    string[] statusy = { "NIE", "TAK", "NIE DOTYCZY" };

                    var descs = model.TaskQuestionPhoto.Where(q => q.Question == prop).ToList();


                    row += RaportQuestionRow(i, question, (TaskQuestionStatus)propValue, descs, statusy);
                    i++;
                }
            }

            return MvcHtmlString.Create(row);
        }

        public static MvcHtmlString RaportQuestionRadioButtons(this HtmlHelper helper, Task_Questions model)
        {
            var row = String.Empty;

            foreach(var prop in model.QuestionsList)
            {
                var propValue = model.GetPropValue(prop);

                row += RaportQuestionRadioButtonsRow(Objects.GetDisplayName<Task_Questions>(prop), prop, (TaskQuestionStatus)propValue);
            }

            return MvcHtmlString.Create(row);
        }

        public static MvcHtmlString TaskQuestionRow(this HtmlHelper helper, Task_Questions model, int taskID)
        {
            var row = "";

            foreach (var prop in model.QuestionsList)
            {
                row += TaskQuestionRow_Once(model, prop, taskID);
            }

            foreach (var machine in model.MachinesList)
            {
                row += TaskQuestionRow_Once(model, machine, taskID);
            }

            return MvcHtmlString.Create(row);
        }

        private static string TaskQuestionRow_Once(Task_Questions model, string prop, int taskID)
        {
            var row = "";

            var propValue = (TaskQuestionStatus)model.GetPropValue(prop);

            var question = Objects.GetDisplayName<Task_Questions>(prop);

            var descs = model.TaskQuestionPhoto.Where(q => q.Question == prop).ToList();

            var photoIcon = (model.TaskQuestionPhoto.Any(q => q.Question.Contains(prop) && !string.IsNullOrEmpty(q.Photo)) && propValue == TaskQuestionStatus.Nie)
                ? $"<span data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Zdjęcia\" data-task-id=\"{taskID}\" data-question-type=\"{prop}\" class=\"question-photo\"><i class=\"glyphicon glyphicon-camera\"></i></span>"
                : "";

            var commentIcon = $"<span data-toggle=\"tooltip\" class=\"show-translate-btn\" data-to-collapse=\".translate-{prop}\" data-placement=\"bottom\" title=\"Pokaż tłumaczenia\"><i class=\"fa fa-comment\"></i></a>";

            row += $"<tr><th>{question} {photoIcon} {commentIcon}</th><td class=\"{propValue} td-{prop}\">";

            row += $"<select class=\"form-control question-select {propValue}\" data-question-type=\"{prop}\">";

            row += (propValue == TaskQuestionStatus.Tak) ? "<option class=\"Tak\" value=\"Tak\" selected>Tak</option>" : "<option class=\"Tak\" value=\"Tak\">Tak</option>";
            row += (propValue == TaskQuestionStatus.Nie) ? "<option class=\"Nie\" value=\"Nie\" selected>Nie</option>" : "<option class=\"Nie\" value=\"Nie\">Nie</option>";
            row += (propValue == TaskQuestionStatus.Nie_Dotyczy) ? "<option class=\"Nie_Dotyczy\" value=\"Nie_Dotyczy\" selected>Nie_Dotyczy</option>" : "<option class=\"Nie_Dotyczy\" value=\"Nie_Dotyczy\">Nie_Dotyczy</option>";

            row += "</select>";

            row += $@"      </td>

                            </tr>
                        ";

            row += $@"<td colspan=""2""><table class=""table collapse translate-{prop}"">";

            foreach (var desc in descs)
            {

                row += $@"
                    <tr>
                        <td>
                            <input type=""text"" class=""form-control"" name=""{prop}_DescriptionPL_{desc?.Id}"" value=""{desc?.Description}"" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type=""text"" class=""form-control"" name=""{prop}_DescriptionSE_{desc?.Id}"" value=""{desc?.TranslatedDescription}"" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button type=""button"" class=""btn btn-primary save-translate"" data-question=""{prop}"" data-id=""{desc?.Id ?? -1}"" data-task-id=""{taskID}"">ZAPISZ</button>
                        </td>
                    </tr>
                    ";
            }

            row += $@"<tr>
                        <td>
                            <input type=""text"" class=""form-control"" name=""{prop}_DescriptionPL_-1"" value="""" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type=""text"" class=""form-control"" name=""{prop}_DescriptionSE_-1"" value="""" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button type=""button"" class=""btn btn-primary save-translate"" data-question=""{prop}"" data-id=""-1"" data-task-id=""{taskID}"">ZAPISZ</button>
                        </td>
                    </tr>";

            row += "</table></td>";

            return row;
        }

        private static string RaportQuestionRow(int lp, string question, TaskQuestionStatus status, List<TaskQuestionPhoto> descs, string[] statusy)
        {
            var bold = (status == TaskQuestionStatus.Nie) ? " class='bold'" : String.Empty;

            var row = $@"<tr>
                            <td>{lp.ToString()}.</td>
                            <td{bold}>{question}</td>
                            <td{bold}>{statusy[(int)status]}</td>
                            <td>
                                ";

            foreach(var d in descs)
            {
                if (d.Blocked)
                    continue;

                row += "<p>";
                row += (!string.IsNullOrEmpty(d.TranslatedDescription)) ? d.TranslatedDescription : d.Description;
                row += "</p>";
            }

            row +=   $@"
                            
                        </td>
                        </tr>";

            return row;
        }

        private static string RaportQuestionRow(int lp, string question, int status, string[] statusy)
        {
            var bold = (status == 0) ? " class='bold'" : String.Empty;

            var row = $@"<tr><td>{lp.ToString()}.</td><td{bold}>{question}</td><td{bold}>{statusy[(int)status]}</td></tr>";

            return row;
        }

        private static string RaportQuestionRadioButtonsRow(string question, string propName, TaskQuestionStatus status)
        {
            var nieChecked = (status == TaskQuestionStatus.Nie) ? "checked" : "";
            var takChecked = (status == TaskQuestionStatus.Tak) ? "checked" : "";
            var nieDotyczyChecked = (status == TaskQuestionStatus.Nie_Dotyczy) ? "checked" : "";

            var row = String.Empty;

            if (propName == "Machines")
            {
                if (string.IsNullOrEmpty(takChecked) && string.IsNullOrEmpty(nieDotyczyChecked))
                    nieDotyczyChecked = "checked";

                row = $@"
                            <div class='form-group'>
                                <label for='Task_Questions_{propName}' class='control-label'>{question}</label>
                                <br/>
                                <input type='radio' name='Task_Questions.{propName}' id='Task_Questions_{propName}' class='question question-machines' value='1' {takChecked} /> Tak
                                <input type='radio' name='Task_Questions.{propName}' id='Task_Questions_{propName}' class='question question-machines' value='2' {nieDotyczyChecked} /> Nie Dotyczy
                            </div>
                        ";
            }
            else
            {
                row = $@"
                            <div class='form-group'>
                                <label for='Task_Questions_{propName}' class='control-label'>{question}</label>
                                <br/>
                                <input type='radio' name='Task_Questions.{propName}' id='Task_Questions_{propName}' class='question' value='0' {nieChecked} /> Nie
                                <input type='radio' name='Task_Questions.{propName}' id='Task_Questions_{propName}' class='question' value='1' {takChecked} /> Tak
                                <input type='radio' name='Task_Questions.{propName}' id='Task_Questions_{propName}' class='question' value='2' {nieDotyczyChecked} /> Nie dotyczy
                                <br/>
                                <div id='Photo_Task_Questions_{propName}'>
                                    <div class='fileUpload btn btn-sm btn-like-bg'>
                                        <span>DODAJ ZDJĘCIE</span>
                                        <input type='file' class='upload' id='question-img' name='question-img' data-question='{propName}' accept='image/*' />
                                    </div>
                                </div>
                            </div>
                        ";
            }

            return row;
        }


    }
}