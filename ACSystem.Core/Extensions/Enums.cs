﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ACSystem.Core.Extensions
{
    public static class Enums
    {
        public static string GetDescription(this Enum _enum)
        {
            Type type = _enum.GetType();
            MemberInfo[] member = type.GetMember(_enum.ToString());
            Attribute attribute = member[0].GetCustomAttribute(typeof(DescriptionAttribute), false);
                        
            return ((DescriptionAttribute)attribute).Description;
        }
    }
}