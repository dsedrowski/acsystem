﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Extensions {
    public static class DecimalExtensions {
        public static string TwoOrNone(this decimal number) {
            var s = string.Format("{0:0.00}", number);

            if (s.EndsWith("00")) 
                return ((int)number).ToString();
            else
                return s.Replace(',','.');
        }
    }
}