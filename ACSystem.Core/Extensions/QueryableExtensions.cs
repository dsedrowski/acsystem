﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ACSystem.Core.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> ApplySorting<T>(this IQueryable<T> query, string sortBy, SortDirection sortDirection) where T : new()
        {
            var param = Expression.Parameter(typeof(T), "item");
            var sortExpression = Expression.Lambda<Func<T, object>>(Expression.Convert(Expression.Property(param, sortBy), typeof(object)), param);

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    query = query.OrderBy(sortExpression);
                    break;
                case SortDirection.Descending:
                    query = query.OrderByDescending(sortExpression);
                    break;
            }

            return query;
        }
    }
}