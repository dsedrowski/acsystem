﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace ACSystem.Core.Extensions
{
    public static class ObjectsExtensions
    {
        public static object GetPropValue(this object obj, string propName)
        {
            var objValue = obj.GetType().GetProperty(propName).GetValue(obj, null);

            return objValue;
        }

        public static int? GetId(this object value)
        {
            int? id;
            try
            {
                id = value.ToDynamic().Id;
            }
            catch
            {
                id = null;
            }

            return id;
        }

        public static dynamic ToDynamic(this object value)
        {
            IDictionary<string, object> expando = new ExpandoObject();

            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(value.GetType()))
                expando.Add(property.Name, property.GetValue(value));

            return (ExpandoObject)expando;
        }

        public static string ClearChars(this string str)
        {
            var charList = new List<char>
            {
                ' ', '/', ':', ',', '.', '(', ')', '#', '&', '=', '+', '-'
            };

            foreach (var c in charList)
            {
                str = str.Replace(c, '_');
            }

            return str;
        }
    }
}