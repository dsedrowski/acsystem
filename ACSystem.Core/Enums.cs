﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ACSystem.Core
{
    #region Statusy
    public enum ProjectStatus
    {
        UnderWork,
        NotStarted,
        FinishedWithFaults,
        Finished
    }

    public enum TaskStatus
    {
        [Description("Dostępne")]
        Free,

        [Description("W trakcie")]
        UnderWork,

        [Description("Zatrzymane")]
        Stoped,

        [Description("Miejsce nieprzygotowane")]
        PlaceNotReady,

        [Description("Zaplanowane")]
        Planned,

        [Description("Do wyjaśnienia")]
        DoWyjasnienia,

        [Description("Nie nasze")]
        NieNasze,

        [Description("Zakończone z błędami")]
        FinishedWithFaults,

        [Description("Zakończone z błędami (poprawione)")]
        FinishedWithFaultsCorrected,

        [Description("Zakończone")]
        Finished,

        [Description("Nie zmieniać (pole dla grupowych edycji)")]
        DoNotChange = 99
    }

    public enum TaskQuestionStatus
    {
        Nie,
        Tak,
        Nie_Dotyczy
    }

    public enum TaskProduction
    {
        Developer,
        Warranty,
        Complaint
    }

    public enum InventoryStatus
    {
        Free,
        Busy,
        Deleted,
        Sold
    }

    public enum PayItemStatus
    {
        DoWyplaty,
        Wstrzymane,
        Wyplacone
    }

    public enum PayStatus
    {
        Gotowe,
        CzesciowoWyplacone,
        Zamkniete
    }

    public enum FeeStatus
    {
        DoPotracenia,
        Potracone
    }

    public enum InvoiceStatus
    {
        Gotowa,
        Wyslana,
        Zaplacona,
        Xml,
        Fortnox
    }

    public enum Inventory_ConditionStatus
    {
        New,
        Good,
        Ok,
        ToChange,
        Trash
    }

    #endregion

    #region Typy
    public enum Position
    {
        Worker,
        Fitter
    }

    public enum TaskType
    {
        Kitchen,
        Construction,
        Service
    }

    public enum EventType
    {
        Task,
        User,
        Plan,
        Project
    }

    public enum Leanguage
    {
        Polish,
        English,
        Swedish
    }

    public enum TaskErrorType
    {
        None,
        Quantity,
        MakeFillTask
    }

    public enum UserType
    {
        Wewnetrzny,
        Zewnetrzny
    }

    public enum InstallmentFrequency
    {
        PerPayroll,
        PerMonth
    }

    public enum Currency
    {
        SEK,
        PLN
    }

    public enum TaskReportType
    {
        Dev_All,
        Dev_Kitchen,
        Dev_Construction,
        Service_All,
        Service_Kitchen,
        Service_Construction
    }

    public enum InventoryOwnerType
    {
        General,
        Private
    }

    public enum TodoType
    {
        TlumaczeniaPytanKontrolnych,
        Akceptacja,
        ZmianaStatusu,
        ZakoczenieZBledami,
        NowyKomentarz,
        PoprawionyBlad,
        Wiadomosc,
        NowyBlad,
        ZatrzymaneZadanie
    }

    #endregion

    #region Repositories

    public enum SortDirection
    {
        Ascending,
        Descending
    }

    #endregion
}