﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACSystem.Core
{
    public class Logger
    {
        private Logger() { }

        public static Logger Once { get; private set; }

        static Logger() { Once = new Logger(); }

        public void Log(string message, Exception ex)
        {
            string n = Environment.NewLine;
            string str = n + n + DateTime.Now.ToString() + ":" + n;
            str += "MachineName: " + Environment.MachineName + n;
            str += "UserName: " + Environment.UserName + n;
            str += "OSVersion: " + Environment.OSVersion + n;
            str += "Location: " + Environment.CurrentDirectory + n;
            str += "*********************************************" + n;
            StreamWriter fs = null;

            str += message + n;

            if (ex != null)
            {
                str += "Ex msg: ";
                str += ex.Message + n;
                str += "Stack trace: " + ex.StackTrace + n;

                if (ex.InnerException != null)
                {
                    str += "Inner ex msg: ";
                    str += ex.InnerException.Message + n;
                    str += "Inner Stack trace: " + ex.InnerException.StackTrace + n;
                }
            }

            try
            {
                Directory.CreateDirectory(DIR_NAME);
                string logFileName = String.Format("{0}.txt", DateTime.Now.ToShortDateString());

                fs = new StreamWriter(Path.Combine(DIR_NAME, logFileName), true);
                fs.Write(str);
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }

        public void Log(string title, string message)
        {
            string n = Environment.NewLine;
            string str = n + n + DateTime.Now.ToString() + ":" + n;
            str += "MachineName: " + Environment.MachineName + n;
            str += "UserName: " + Environment.UserName + n;
            str += "OSVersion: " + Environment.OSVersion + n;
            str += "Location: " + Environment.CurrentDirectory + n;
            str += "*********************************************" + n;
            StreamWriter fs = null;

            str += title + n;

            if (!string.IsNullOrEmpty(message))
            {
                str += "msg: ";
                str += message + n;
            }

            try
            {
                Directory.CreateDirectory(DIR_NAME);
                string logFileName = String.Format("{0}.txt", DateTime.Now.ToShortDateString());

                fs = new StreamWriter(Path.Combine(DIR_NAME, logFileName), true);
                fs.Write(str);
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }

        public void SaveReportToFile(string reportText)
        {
            string n = Environment.NewLine;
            string str = DateTime.Now.ToString() + ":" + n;
            str += "MachineName: " + Environment.MachineName + n;
            str += "UserName: " + Environment.UserName + n;
            str += "OSVersion: " + Environment.OSVersion + n;
            str += "Location: " + Environment.CurrentDirectory + n;
            str += "*********************************************" + n;

            str += reportText + n;

            StreamWriter fs = null;

            try
            {
                Directory.CreateDirectory(DIR_IMPORT_NAME);
                string logFileName = String.Format("Import {0}.txt", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture).Replace(':', '_').Replace('.', '_'));

                fs = new StreamWriter(Path.Combine(DIR_IMPORT_NAME, logFileName), true);
                fs.Write(str);
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }

        private static string DIR_NAME = "Logs";
        private static string DIR_IMPORT_NAME = "Import";
    }
}
