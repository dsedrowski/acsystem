﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ACSystem.Fitter.Startup))]
namespace ACSystem.Fitter
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Temp.SetVariables();
        }
    }
}
