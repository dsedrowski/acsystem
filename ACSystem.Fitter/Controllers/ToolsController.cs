﻿using ACSystem.Core.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ACSystem.Fitter.Controllers
{
    public class ToolsController : Controller
    {
        public ActionResult AddPlaceError(int? ProjectID, int? ApartmentID, int? TaskID, string place)
        {
            Projects project = null;
            Apartments apartment = null;
            Tasks task = null;

            using (var db = new ACSystemContext())
            {

                switch (place)
                {
                    case "project":
                        if (!ProjectID.HasValue)
                            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                        project = db.Projects.Find(ProjectID);
                        break;
                    case "apartment":
                        if (!ProjectID.HasValue || !ApartmentID.HasValue)
                            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                        project = db.Projects.Find(ProjectID);
                        apartment = db.Apartments.Find(ApartmentID);
                        break;
                    case "task":
                        if (!ProjectID.HasValue || !ApartmentID.HasValue || !TaskID.HasValue)
                            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                        project = db.Projects.Find(ProjectID);
                        apartment = db.Apartments.Find(ApartmentID);
                        task = db.Tasks.Find(TaskID);
                        break;
                }
            }

            var model = new PlaceError
            {
                ProjectID = ProjectID,
                ApartmentID = ApartmentID,
                TaskID = TaskID,
                FitterID = User.Identity.GetUserId(),
                Date = DateTime.Now,
                Project = project,
                Apartment = apartment,
                Task = task
            };

            #region ViewBags

            ViewBag.Place = place;

            #endregion

            return View(model);
        }

        [HttpPost]
        public ActionResult AddPlaceError(string _place, List<string> _photos, PlaceError _model)
        {
            if (_photos == null) _photos = new List<string>();

            _model.PlaceError_Photos = new List<PlaceError_Photo>();

            foreach (var p in _photos)
            {
                _model.PlaceError_Photos.Add(new PlaceError_Photo { Photo = p });
            }

            using (var db = new ACSystemContext())
            {
                db.PlaceError.Add(_model);

                try
                {
                    db.SaveChanges();

                    db.Todo.Add(new Todo
                    {
                        TaskID = null,
                        PlaceErrorID = _model.ID,
                        FitterID = User.Identity.GetUserId(),
                        Type = Core.TodoType.NowyBlad,
                        Date = DateTime.Now,
                        ErrorType = "",
                        Checked = false
                    });

                    db.SaveChanges();

                    switch (_place)
                    {
                        case "project":
                            return Redirect($"/Projects/Project/{_model.ProjectID}");
                        case "apartment":
                            return Redirect($"/Apartment/Details/{_model.ApartmentID}");
                        case "task":
                            return Redirect($"/Projects/Task/{_model.TaskID}");
                        default:
                            return Redirect($"/Projects/Project/{_model.ProjectID}");
                    }
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }
    }
}