﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ACSystem.Core.Models;
using ACSystem.Core.IRepo;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Data.Entity.Validation;
using System.Data.Entity.Core;
using System.Web;
using System.IO;
using ACSystem.Core.Service;
using System.Drawing;
using System.Drawing.Imaging;
using ACSystem.Core;
using ACSystem.Core.Models.ViewModels;
using System.Text;
using PdfSharp;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using TaskStatus = ACSystem.Core.TaskStatus;
using ACSystem.Core.Models.ViewModels.Select;

namespace ACSystem.Fitter.Controllers
{
    [Authorize]
    public class ProjectsController : Controller
    {
        private readonly IProjectsRepo _repo;

        public ProjectsController(IProjectsRepo repo)
        {
            _repo = repo;
        }

        public ActionResult AvailableTasks(string city = "", string number = "", bool showHistory = false)
        {
            var db = _repo.GetContext();
            var user = db.ACSystemUser.Find(User.Identity.GetUserId());
            ViewBag.FavouriteCity = db.FavouriteCity.Where(q => q.UserId == user.Id).ToList();
            ViewBag.Page = "AvailableTasks";
            ViewBag.City = city;
            ViewBag.Number = number;
            ViewBag.ShowHistory = showHistory;
            ViewBag.InnerUser = (user != null && user.UserType == UserType.Wewnetrzny);
            ViewBag.CanSeeAll = (user != null && user.CanSeeAll);
            ViewBag.User = user;
            ViewBag.FavouriteIDs = db.FavouriteProjects.Where(q => q.UserID == user.Id).Select(q => q.ProjectID).ToList();
            var projectsDict = new Dictionary<string, List<Projects>>
            {
                { "all", new List<Projects>() },
                { "developer", new List<Projects>() },
                { "service", new List<Projects>() },
            };

            var tasksList = _repo.ListWithAvailableTask(User.Identity.GetUserId(), showHistory);

            foreach (var task in tasksList)
            {
                if (!projectsDict["all"].Any(q => q.Id == task.Apartment.ProjectId))
                    projectsDict["all"].Add(task.Apartment.Project);

                if (task.Production == TaskProduction.Developer && task.Status != TaskStatus.FinishedWithFaults && !projectsDict["developer"].Any(q => q.Id == task.Apartment.ProjectId) && !(task.Apartment.Project.FirstDoService ?? false))
                    projectsDict["developer"].Add(task.Apartment.Project);

                if ((task.Production != TaskProduction.Developer || task.Status == TaskStatus.FinishedWithFaults) && !projectsDict["service"].Any(q => q.Id == task.Apartment.ProjectId))
                    projectsDict["service"].Add(task.Apartment.Project);
            }

            var projectsToRemove = new List<Projects>();

            foreach (var p in projectsDict["all"])
            {
                var allFinished = p.Apartments.Select(q => q.Tasks.Any(x => x.Production != TaskProduction.Developer && x.Status != TaskStatus.Finished)).All(q => q == false);

                if (p.FirstDoService ?? false && !allFinished)
                    projectsToRemove.Add(p);
            }

            projectsDict["all"].RemoveAll(q => projectsToRemove.Contains(q));
            projectsDict["developer"].RemoveAll(q => projectsToRemove.Contains(q));

            projectsDict["all"] = projectsDict["all"].Where(q => q.City.ToUpper().Contains(city.ToUpper()) && q.ProjectNumber.ToUpper().Contains(number.ToUpper())).OrderBy(q => q.ProjectNumber).ToList();
            projectsDict["developer"] = projectsDict["developer"].Where(q => q.City.ToUpper().Contains(city.ToUpper()) && q.ProjectNumber.ToUpper().Contains(number.ToUpper())).OrderBy(q => q.ProjectNumber).ToList();
            projectsDict["service"] = projectsDict["service"].Where(q => q.City.ToUpper().Contains(city.ToUpper()) && q.ProjectNumber.ToUpper().Contains(number.ToUpper())).OrderBy(q => q.ProjectNumber).ToList();

            return View(projectsDict);
        }

        public ActionResult FavouriteProjects(string city = "", string number = "", bool showHistory = false)
        {
            var db = _repo.GetContext();
            var user = db.ACSystemUser.Find(User.Identity.GetUserId());

            ViewBag.Page = "FavouriteProjects";
            ViewBag.City = city;
            ViewBag.Number = number;
            ViewBag.ShowHistory = showHistory;
            ViewBag.InnerUser = (user != null && user.UserType == UserType.Wewnetrzny);
            ViewBag.CanSeeAll = (user != null && user.CanSeeAll);
            ViewBag.User = user;
            var favouriteIDs = ViewBag.FavouriteIDs = db.FavouriteProjects.Where(q => q.UserID == user.Id).Select(q => q.ProjectID).ToList();

            var projectsDict = new Dictionary<string, List<Projects>>
            {
                { "all", new List<Projects>() },
                { "developer", new List<Projects>() },
                { "service", new List<Projects>() },
            };

            var tasksList = _repo.ListWithAvailableTask(User.Identity.GetUserId(), showHistory);

            foreach (var task in tasksList)
            {
                if (!projectsDict["all"].Any(q => q.Id == task.Apartment.ProjectId))
                    projectsDict["all"].Add(task.Apartment.Project);

                if (task.Production == TaskProduction.Developer && !projectsDict["developer"].Any(q => q.Id == task.Apartment.ProjectId) && !(task.Apartment.Project.FirstDoService ?? false))
                    projectsDict["developer"].Add(task.Apartment.Project);

                if (task.Production != TaskProduction.Developer && !projectsDict["service"].Any(q => q.Id == task.Apartment.ProjectId))
                    projectsDict["service"].Add(task.Apartment.Project);
            }

            var projectsToRemove = new List<Projects>();

            foreach (var p in projectsDict["all"])
            {
                var allFinished = p.Apartments.Select(q => q.Tasks.Any(x => x.Production != TaskProduction.Developer && x.Status != TaskStatus.Finished)).All(q => q == false);

                if (p.FirstDoService ?? false && !allFinished)
                    projectsToRemove.Add(p);
            }

            projectsDict["all"].RemoveAll(q => projectsToRemove.Contains(q));
            projectsDict["developer"].RemoveAll(q => projectsToRemove.Contains(q));

            projectsDict["all"] = projectsDict["all"].Where(q => favouriteIDs.Contains(q.Id) && q.City.ToUpper().Contains(city.ToUpper()) && q.ProjectNumber.ToUpper().Contains(number.ToUpper())).OrderBy(q => q.ProjectNumber).ToList();
            projectsDict["developer"] = projectsDict["developer"].Where(q => favouriteIDs.Contains(q.Id) && q.City.ToUpper().Contains(city.ToUpper()) && q.ProjectNumber.ToUpper().Contains(number.ToUpper())).OrderBy(q => q.ProjectNumber).ToList();
            projectsDict["service"] = projectsDict["service"].Where(q => favouriteIDs.Contains(q.Id) && q.City.ToUpper().Contains(city.ToUpper()) && q.ProjectNumber.ToUpper().Contains(number.ToUpper())).OrderBy(q => q.ProjectNumber).ToList();

            return View("AvailableTasks", projectsDict);
        }

        [HttpGet]
        public async Task<ActionResult> Project(int? id, string number = "", bool showHistory = false)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var userId = HttpContext.User.Identity.GetUserId();
            ViewBag.Fitter = _repo.GetFitter(userId);

            ViewBag.Number = number;
            ViewBag.ShowHistory = showHistory;

            return View(await _repo.GetProject((int)id));
        }

        [HttpGet]
        public async Task<ActionResult> Task(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var userId = HttpContext.User.Identity.GetUserId();

            ViewBag.Fitter = _repo.GetFitter(userId);
            ViewBag.IsFitterBusy = _repo.IsFitterBusy(userId);

            return View(await _repo.GetTask((int)id));
        }

        [HttpGet]
        public ActionResult GetTask(int id)
        {
            var userId = HttpContext.User.Identity.GetUserId();

            var isGet = _repo.FitterTaskGet(id, userId);

            if (isGet)
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> CurrentTask()
        {
            var userId = HttpContext.User.Identity.GetUserId();

            var fitterTask = await _repo.GetCurrentTask(userId);

            ViewBag.Fitter = _repo.GetFitter(userId);
            ViewBag.UserID = userId;
            ViewBag.FreeFitters = _repo.GetFreeFitters();

            return View(fitterTask);
        }

        [HttpPost]
        public async Task<ActionResult> CurrentTask(decimal? Distance, TaskErrorType ErrorType, int Status, string[] photos, string[] photosDesc, string[] photosQuestion, string[] photosQuestionDesc, string[] question, string InvoiceDescriptionPL, DateTime? activityDateTime, string[] answers, string[] additionalInformations)
        {
            ViewBag.Error = "";
            if (!Distance.HasValue) Distance = 0;

            var userId = HttpContext.User.Identity.GetUserId();
            var fitter = _repo.GetFitter(userId);

            var status = (Core.TaskStatus)Status;

            var questions = new Task_Questions();

            try
            {
                questions = new Task_Questions
                {
                    BaseUnits = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.BaseUnits"]),
                    WallUnits = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.WallUnits"]),
                    TallUnits = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.TallUnits"]),
                    Fillers = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Fillers"]),
                    CeillingScribe = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.CeillingScribe"]),
                    SidePiece = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.SidePiece"]),
                    Worktop = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Worktop"]),
                    Sink = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Sink"]),
                    Handles = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Handles"]),
                    Plinth = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Plinth"]),
                    Grids = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Grids"]),
                    Lighting = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Lighting"]),
                    Silicon = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Silicon"]),
                    Manuals = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Manuals"]),
                    CutIntoPipes = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.CutIntoPipes"]),
                    CutOutForVentilation = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.CutOutForVentilation"]),
                    Trash = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Trash"]),
                    ChildSafety = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.ChildSafety"]),
                    Machines = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Machines"]),
                    Oven = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Oven"]),
                    FridgeFreezer1 = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.FridgeFreezer1"]),
                    FridgeFreezer2 = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.FridgeFreezer2"]),
                    Hob = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Hob"]),
                    Dishwasher = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Dishwasher"]),
                    Mikrowave = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Mikrowave"]),
                    Fan = (TaskQuestionStatus)Convert.ToInt32(Request.Form["Task_Questions.Fan"])
                };
            }
            catch { }

            var taskID = fitter.TaskAlreadyWork.Value;
            var currentTask = await _repo.GetTask(taskID);
            var taskFitters = _repo.GetTaskFittersList(fitter.TaskAlreadyWork.Value);
            var workDone = new List<FitterWorkDoneViewModel>();

            var timeOverflow = false;
            foreach (var taskFitter in taskFitters)
            {
                var done = new FitterWorkDoneViewModel {FitterID = taskFitter.ID};


                var doneString = Request.Form["done-" + taskFitter.ID];
                var timeString = Request.Form["time-" + taskFitter.ID];

                if (doneString.Contains("."))
                    doneString = doneString.Replace('.', ',');


                if (timeString.Contains("."))
                    timeString = timeString.Replace('.', ',');

                done.WorkDone = Convert.ToDecimal(doneString);
                done.WorkTime = (int)Math.Ceiling(((done.WorkDone * ((currentTask.Production == TaskProduction.Developer) ? currentTask.Product.ExpectedTime : Temp.Settings.ServiceTaskTime)) * ((decimal)currentTask.Product.TimePercent / 100)) * 60);
                done.RealWorkTime = Convert.ToDecimal(timeString);

                if (!_repo.CanUserReportHours(taskFitter.ID, activityDateTime ?? DateTime.Now, done.WorkTime)) {
                    timeOverflow = true;
                    ViewBag.Error += $"Użytkownik {taskFitter.FullName} przekroczy limit 16h. Proszę podać mniejszą ilość.";
                }

                workDone.Add(done);
            }

            if (timeOverflow) {
                var fitterTask = await _repo.GetCurrentTask(userId);
                ViewBag.Fitter = _repo.GetFitter(userId);
                ViewBag.FreeFitters = _repo.GetFreeFitters();
                ViewBag.UserId = userId;

                if (fitterTask != null)
                    return View(fitterTask);
                else
                    return Redirect("/");
            }

            try
            {
                _repo.ChangeStatus(workDone, Distance.Value, ErrorType, status, photos, photosDesc, photosQuestion, photosQuestionDesc, question, questions, activityDateTime, answers, additionalInformations, userId);

                var context = _repo.GetContext();
                currentTask.InvoiceDescriptionPL = InvoiceDescriptionPL;

                var taskComment = Request.Form["TaskComment"];
                TaskComment comment = null;

                if (!string.IsNullOrEmpty(taskComment))
                {
                    comment = new TaskComment()
                    {
                        FitterID = userId,
                        ApartmentID = currentTask.ApartmentId,
                        TaskID = currentTask.Id,
                        Comment = taskComment,
                        Timestamp = DateTime.Now
                    };

                    var newTodo = new Todo
                    {
                        TaskID = currentTask.Id,
                        FitterID = userId,
                        Type = TodoType.NowyKomentarz,
                        Date = DateTime.Now,
                        Checked = false
                    };

                    context.Todo.Add(newTodo);

                    context.Entry(currentTask).State = System.Data.Entity.EntityState.Modified;
                }
                switch (status)
                {
                    case Core.TaskStatus.Finished:
                    {
                        if (currentTask.Task_Question.Any())
                        {
                            var leanguage = currentTask.Apartment.Project.Customer.DocumentsLeanguage;

                            var result = CreatePDFReport(currentTask.Id,
                                Server.MapPath($"~/dist/Reports/{currentTask.Id}.pdf"), leanguage);

                            if (string.IsNullOrEmpty(result))
                            {
                                var photosList = (from tf in currentTask.Task_Fitter
                                    from p in tf.Photos
                                    where p.Blocked == false
                                    select Server.MapPath($"~/dist/Task_Images/{p.Photo}")).ToList();

                                List<string> errorPhotos = (currentTask.Task_Questions != null) ? (from er in currentTask.Task_Questions.TaskQuestionPhoto where !string.IsNullOrEmpty(er.TranslatedDescription) select Server.MapPath($"~/dist/Question_Images/{er.Photo}")).ToList() : new List<string>();
                                photosList.AddRange(errorPhotos);

                                errorPhotos = currentTask.Task_Question != null && currentTask.Task_Question.Any() ? currentTask.Task_Question.Where(q => !string.IsNullOrEmpty(q.Photo)).Select(q => Server.MapPath($"~/dist/Question_Images/{q.Photo}")).ToList() : new List<string>();
                                photosList.AddRange(errorPhotos);

                                var _exception = "";

                                if (MailService.Raport(currentTask, Server.MapPath($"~/dist/Reports/{currentTask.Id}.pdf"), photosList, out _exception))
                                {
                                    currentTask.ReportSent = true;
                                    currentTask.ReportSentDate = DateTime.Now;
                                    context.Entry(currentTask).State = System.Data.Entity.EntityState.Modified;
                                }
                            }
                            else
                            {
                                Console.WriteLine(result);
                            }
                        }

                        if (comment != null)
                        {
                                context.TaskComment.Add(comment);
                        }

                        break;
                    }
                    case Core.TaskStatus.PlaceNotReady:
                    {
                        var leanguage = currentTask.Apartment.Project.Customer.DocumentsLeanguage;

                        var photosList = (from tf in currentTask.Task_Fitter from p in tf.Photos select Server.MapPath($"~/dist/Task_Images/{p.Photo}")).ToList();

                        var _exception = "";

                        if (MailService.PlaceNotReady(currentTask, photosList, leanguage, out _exception))
                        {
                            currentTask.ReportSent = true;
                            currentTask.ReportSentDate = DateTime.Now;
                            context.Entry(currentTask).State = System.Data.Entity.EntityState.Modified;
                        }

                        if (comment != null)
                        {
                            context.TaskComment.Add(comment);
                        }
                        break;
                    }
                    case TaskStatus.Free:
                        break;
                    case TaskStatus.UnderWork:
                        break;
                    case TaskStatus.Stoped:
                        if(comment != null)
                            context.TaskComment.Add(comment);
                        break;
                    case TaskStatus.FinishedWithFaults:
                        if (comment != null)
                            context.TaskComment.Add(comment);
                        break;
                    case TaskStatus.FinishedWithFaultsCorrected:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                context.SaveChanges();

                return Redirect("/Apartment/Details/" + currentTask.ApartmentId);
            }
            catch (DbEntityValidationException ex)
            {
                ViewBag.Error = "";

                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ViewBag.Error += "Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage + Environment.NewLine;
                    }
                }

                var fitterTask = await _repo.GetCurrentTask(userId);
                ViewBag.Fitter = _repo.GetFitter(userId);
                ViewBag.FreeFitters = _repo.GetFreeFitters();
                ViewBag.UserId = userId;

                if (fitterTask != null)
                    return View(fitterTask);
                else
                    return Redirect("/");
            }
            catch (EntityException ex)
            {
                ViewBag.Error = ex.ToString();
                ViewBag.FreeFitters = _repo.GetFreeFitters();
                ViewBag.Fitter = _repo.GetFitter(userId);
                ViewBag.UserId = userId;
                var fitterTask = await _repo.GetCurrentTask(userId);

                if (fitterTask != null)
                    return View(fitterTask);
                else
                    return Redirect("/");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                ViewBag.FreeFitters = _repo.GetFreeFitters();
                ViewBag.Fitter = _repo.GetFitter(userId);
                ViewBag.UserId = userId;
                var fitterTask = await _repo.GetCurrentTask(userId);

                if (fitterTask != null)
                    return View(fitterTask);
                else
                    return Redirect("/");
            }
        }

        [HttpPost]
        public async Task<ActionResult> LeaveTask(string WorkDone, string[] photos, string[] photosDesc, string[] photosQuestion, string[] photosQuestionDesc, string[] question)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var fitter = _repo.GetFitter(userId);

            if (string.IsNullOrEmpty(WorkDone))
                WorkDone = "0";

#if DEBUG

            if (WorkDone.Contains("."))
                WorkDone = WorkDone.Replace('.', ',');

#endif

            var done = Convert.ToDecimal(WorkDone);

            try
            {
                _repo.LeaveTask(userId, done);
                return Redirect("/Home");
            }
            catch (DbEntityValidationException ex)
            {
                ViewBag.Error = "";

                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ViewBag.Error += "Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage + Environment.NewLine;
                    }
                }

                var fitterTask = await _repo.GetCurrentTask(userId);
                return View(fitterTask);
            }
            catch (EntityException ex)
            {
                ViewBag.Error = ex.ToString();
                var fitterTask = await _repo.GetCurrentTask(userId);
                return View(fitterTask);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.ToString();
                var fitterTask = await _repo.GetCurrentTask(userId);
                return View(fitterTask);
            }
        }

        public ActionResult ChooseToAddTask()
        {
            return View();
        }

        public ActionResult AddTask()
        {
            var projectsList = _repo.GetProjectsList();

            var apartmentsList = _repo.GetApartmentsList((projectsList.FirstOrDefault() != null) ? projectsList.FirstOrDefault().ID : 0);

            ViewBag.ProjectsList = projectsList;
            ViewBag.ApartmentsList = apartmentsList;

            var productsList = _repo.ProductsList();

            ViewBag.ProductsList = productsList;

            var product = _repo.ProductsList().FirstOrDefault(q => q.Product_Project.Count(x => x.ProjectID == projectsList.FirstOrDefault().ID) > 0 || q.ForAllProjects);

            ViewBag.Product = product;

            ViewBag.IsSetProductsList = productsList.Count > 0;

            var price = product.Price;
            decimal pType1 = 0, pType2 = 0, pType3 = 0, pType4 = 0, pType5 = 0, pType6 = 0;

            if (product.Product_Project.Any() && projectsList.Any())
            {
                var connect = product.Product_Project.FirstOrDefault(q => q.ProjectID == projectsList.FirstOrDefault().ID);
                price = connect.Price;
                decimal.TryParse(connect.PercentType1.ToString().Split('.')[0], out pType1);
                decimal.TryParse(connect.PercentType2.ToString().Split('.')[0], out pType2);
            }
            else
            {
                decimal.TryParse(product.PercentHeight.Type1.ToString().Split('.')[0], out pType1);
                decimal.TryParse(product.PercentHeight.Type2.ToString().Split('.')[0], out pType2);
                decimal.TryParse(product.PercentHeight.Type3.ToString().Split('.')[0], out pType3);
                decimal.TryParse(product.PercentHeight.Type4.ToString().Split('.')[0], out pType4);
                decimal.TryParse(product.PercentHeight.Type5.ToString().Split('.')[0], out pType5);
                decimal.TryParse(product.PercentHeight.Type6.ToString().Split('.')[0], out pType6);
            }


            var model = new Tasks
            {
                ProductPrice = product.Price,
                PercentType1 = pType1,
                PercentType2 = pType2,
                PercentType3 = pType3,
                PercentType4 = pType4,
                PercentType5 = pType5,
                PercentType6 = pType6
            };


            var userId = HttpContext.User.Identity.GetUserId();

            ViewBag.IsFitterBusy = _repo.IsFitterBusy(userId);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddTask(Tasks task)
        {
            var userId = HttpContext.User.Identity.GetUserId();

            var getTask = Request.Form["GetTask"] == "True";

            task.Product = _repo.GetContext().Products.Find(task.ProductId);
            //task.ProductPrice = task.Product.Price;

            var db = _repo.GetContext();

            var apartment = db.Apartments.Find(task.ApartmentId);
            var project = db.Projects.Find(apartment.ProjectId);

            var connect = project.Product_Project.FirstOrDefault(q => q.ProductID == task.ProductId);

            task.ProductPrice = connect?.Price ?? task.Product.Price;
            task.PercentType1 = connect?.PercentType1 ?? task.Product.PercentHeight.Type1 ?? 0;
            task.PercentType2 = connect?.PercentType2 ?? task.Product.PercentHeight.Type2 ?? 0;

            var result = _repo.TaskCreate(task, false, getTask, userId);

            if (result.Split(':')[0] == "success")
            {
                return Redirect(getTask ? "/Projects/CurrentTask" : $"/");
            }
            else
            {
                var projectsList = _repo.GetProjectsList();
                var apartmentsList = _repo.GetApartmentsList((projectsList.FirstOrDefault() != null) ? projectsList.FirstOrDefault().ID : 0);
                var productsList = _repo.ProductsList();
                var product = _repo.ProductsList().FirstOrDefault();

                ViewBag.IsSetProductsList = productsList.Count > 0;
                ViewBag.Product = product;
                ViewBag.ProductsList = productsList;
                ViewBag.ProjectsList = projectsList;
                ViewBag.ApartmentsList = apartmentsList;
                ViewBag.IsFitterBusy = _repo.IsFitterBusy(userId);
                ViewBag.Error = result.Split(':')[1];

                return View(task);
            }
        }

        public ActionResult AddServiceKitchenTask()
        {
            var projectsList = _repo.GetProjectsList();

            var apartmentsList = _repo.GetApartmentsList((projectsList.FirstOrDefault() != null) ? projectsList.FirstOrDefault().ID : 0);

            ViewBag.ProjectsList = projectsList;
            ViewBag.ApartmentsList = apartmentsList;

            var productsList = _repo.ProductsList().Where(q => q.ProductType == TaskType.Kitchen).ToList();

            ViewBag.ProductsList = productsList;

            var product = _repo.ProductsList().FirstOrDefault();

            ViewBag.Product = product;

            ViewBag.IsSetProductsList = productsList.Count > 0;

            decimal pType1 = 0, pType2 = 0, pType3 = 0, pType4 = 0, pType5 = 0, pType6 = 0;

            decimal.TryParse(product.PercentHeight.Type1.ToString().Split('.')[0], out pType1);
            decimal.TryParse(product.PercentHeight.Type2.ToString().Split('.')[0], out pType2);
            decimal.TryParse(product.PercentHeight.Type3.ToString().Split('.')[0], out pType3);
            decimal.TryParse(product.PercentHeight.Type4.ToString().Split('.')[0], out pType4);
            decimal.TryParse(product.PercentHeight.Type5.ToString().Split('.')[0], out pType5);
            decimal.TryParse(product.PercentHeight.Type6.ToString().Split('.')[0], out pType6);

            var model = new Tasks
            {
                ProductPrice = product.Price,
                PercentType1 = pType1,
                PercentType2 = pType2,
                PercentType3 = pType3,
                PercentType4 = pType4,
                PercentType5 = pType5,
                PercentType6 = pType6
            };


            var userId = HttpContext.User.Identity.GetUserId();

            ViewBag.IsFitterBusy = _repo.IsFitterBusy(userId);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddServiceTask(Tasks task, string[] questionDescription, string[] questionSelect, string TaskPhoto)
        {
            var projectsList = _repo.GetProjectsList();

            var apartmentsList = _repo.GetApartmentsList((projectsList.FirstOrDefault() != null) ? projectsList.FirstOrDefault().ID : 0);

            ViewBag.ProjectsList = projectsList;
            ViewBag.ApartmentsList = apartmentsList;

            var productsList = _repo.ProductsList().Where(q => q.ProductType == TaskType.Kitchen).ToList();

            ViewBag.ProductsList = productsList;

            var product = _repo.ProductsList().FirstOrDefault();

            ViewBag.Product = product;

            ViewBag.IsSetProductsList = productsList.Count > 0;

            var userId = HttpContext.User.Identity.GetUserId();

            ViewBag.IsFitterBusy = _repo.IsFitterBusy(userId);

            var getTask = Request.Form["GetTask"] == "True";

            #region PATCHES

            var patches = new List<TaskPatches>();

            var descriptions = questionDescription;
            var questions = questionSelect;

            if (descriptions != null && questions != null)
            {
                var descArray = descriptions;
                var questionsArray = questions;

                for (var i = 0; i < questionsArray.Length; i++)
                {
                    var patch = new TaskPatches
                    {
                        TaskID = task.Id,
                        ItemTitle = questionsArray[i],
                        ItemDescription = descArray[i],
                        PhotoBlocked = false
                    };

                    patches.Add(patch);
                }
            }

            _repo.TaskPatchesCreate(patches);

            task.ProductCount = (patches.Count() > 0) ? patches.Count() : 1;

            #endregion

            #region TASK PHOTO

            if (!string.IsNullOrEmpty(TaskPhoto))
            {
                var activityPhoto = new TaskActivityPhotos
                {
                    Photo = TaskPhoto,
                    Desc = "Zdjęcie przed",
                    Blocked = false
                };

                var activity = new Task_Fitter
                {
                    FitterId = User.Identity.GetUserId(),
                    WorkDone = 0,
                    WorkTime = 0,
                    Status = TaskStatus.Free,
                    Timestamp = DateTime.Now,
                    InPayroll = false,
                    IsActive = true,
                    DayChecked = false
                };

                activity.Photos.Add(activityPhoto);

                task.Task_Fitter.Add(activity);
            }

            #endregion

            var result = _repo.TaskCreate(task, false, getTask, userId);

            if (result.Split(':')[0] == "success")
            {
                return Redirect(getTask ? "/Projects/CurrentTask" : $"/");
            }
            else
            {
                ViewBag.Error = result.Split(':')[1];
                return View(task);
            }
        }

        public ActionResult AddServiceConstructionTask()
        {
            var projectsList = _repo.GetProjectsList();

            var apartmentsList = _repo.GetApartmentsList((projectsList.FirstOrDefault() != null) ? projectsList.FirstOrDefault().ID : 0);

            ViewBag.ProjectsList = projectsList;
            ViewBag.ApartmentsList = apartmentsList;

            var productsList = _repo.ProductsList().Where(q => q.ProductType == TaskType.Construction).ToList();

            ViewBag.ProductsList = productsList;

            var product = _repo.ProductsList().FirstOrDefault();

            ViewBag.Product = product;

            ViewBag.IsSetProductsList = productsList.Count > 0;

            decimal pType1 = 0, pType2 = 0, pType3 = 0, pType4 = 0, pType5 = 0, pType6 = 0;

            decimal.TryParse(product.PercentHeight.Type1.ToString().Split('.')[0], out pType1);
            decimal.TryParse(product.PercentHeight.Type2.ToString().Split('.')[0], out pType2);
            decimal.TryParse(product.PercentHeight.Type3.ToString().Split('.')[0], out pType3);
            decimal.TryParse(product.PercentHeight.Type4.ToString().Split('.')[0], out pType4);
            decimal.TryParse(product.PercentHeight.Type5.ToString().Split('.')[0], out pType5);
            decimal.TryParse(product.PercentHeight.Type6.ToString().Split('.')[0], out pType6);

            var model = new Tasks
            {
                ProductPrice = product.Price,
                PercentType1 = pType1,
                PercentType2 = pType2,
                PercentType3 = pType3,
                PercentType4 = pType4,
                PercentType5 = pType5,
                PercentType6 = pType6
            };


            var userId = HttpContext.User.Identity.GetUserId();

            ViewBag.IsFitterBusy = _repo.IsFitterBusy(userId);

            return View(model);
        }

        [HttpGet]
        public ActionResult AddFitter(int? TaskID, string FitterID)
        {
            if(!TaskID.HasValue || string.IsNullOrEmpty(FitterID))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var result = _repo.FitterTaskGet(TaskID.Value, FitterID);

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetProduct(int? id, int? projectID)
        {
            if (id == null || projectID == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var product = _repo.GetProduct((int)id);
            var connect = product.GetConnectForProject(projectID.Value);

            return Json(new
            {
                Price = connect.Price,
                Jm = product.Jm,
                Type1 = connect.PercentType1,
                Type2 = connect.PercentType2,
                Type3 = product.PercentHeight.Type3,
                Type4 = product.PercentHeight.Type4,
                Type5 = product.PercentHeight.Type5,
                Type6 = product.PercentHeight.Type6
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetApartments(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var apartment = _repo.GetApartmentsList(id.Value);

            return Json(apartment, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetProductsForProject(int? id, int filtr)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = _repo.GetContext();

            var project = db.Projects.Find(id);

            if (project == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var connectedProducts = new List<ProductsSelect>();

            foreach (var p in project.Product_Project)
            {
                if (filtr == 1 && p.Product.ProductType == TaskType.Construction)
                    continue;

                if (filtr == 2 && p.Product.ProductType == TaskType.Kitchen)
                    continue;

                connectedProducts.Add(new ProductsSelect { ID = p.ProductID, Name = p.Product.GetTaskName(id.Value) });
            }

            return Json(connectedProducts, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditTaskPatch(int PatchID, bool PatchStatus)
        {
            var fitter = _repo.GetContext().ACSystemUser.Find(User.Identity.GetUserId())?.FullName;

            var query = (PatchStatus) ?
                        $"UPDATE TaskPatches SET Done = '{PatchStatus}', FitterName = '{fitter}', Timestamp = '{DateTime.Now.ToString("yyyy-MM-dd HH:mm")}' WHERE ID = {PatchID}"
                        :
                        $"UPDATE TaskPatches SET Done = '{PatchStatus}', FitterName = '', Timestamp = 'NULL' WHERE ID = {PatchID}";

            var result = _repo.EditPatch($"UPDATE TaskPatches SET Done = '{PatchStatus}', FitterName = '{fitter}', Timestamp = '{DateTime.Now.ToString("yyyy-MM-dd HH:mm")}' WHERE ID = {PatchID}");

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadImage(string destination = "Task_Images", string watermarkText = "", string prefix = "")
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var extension = new FileInfo(fileName);

                        newFileName = prefix + "_" + newFileName + extension.Extension;

                        var filePath = Path.Combine(Server.MapPath($"~/dist/{destination}/"), newFileName);
                        file.SaveAs(filePath);


                        using (var image = Image.FromFile(filePath))
                        using (var newImage = Images.ScaleImage(image, 1200, 1600))
                        {
                            image.Dispose();

                            using (Graphics g = Graphics.FromImage(newImage))
                            {
                                SolidBrush brush = new SolidBrush(Color.Red);
                                Font font = new Font("Arial", 16);
                                g.DrawString(watermarkText, font, brush, new PointF(0, 0));
                            }

                            switch (extension.Extension.ToUpper())
                            {
                                case ".JPG":
                                    newImage.Save(Path.Combine(Server.MapPath($"~/dist/{destination}/"), "resized_" + newFileName), ImageFormat.Jpeg);
                                    break;
                                case ".JPEG":
                                    newImage.Save(Path.Combine(Server.MapPath($"~/dist/{destination}/"), "resized_" + newFileName), ImageFormat.Jpeg);
                                    break;
                                case ".PNG":
                                    newImage.Save(Path.Combine(Server.MapPath($"~/dist/{destination}/"), "resized_" + newFileName), ImageFormat.Png);
                                    break;
                                case ".BMP":
                                    newImage.Save(Path.Combine(Server.MapPath($"~/dist/{destination}/"), "resized_" + newFileName), ImageFormat.Bmp);
                                    break;
                            }
                        }

                        System.IO.File.Delete(filePath);
                    }

                    if (destination == "Patch_Images")
                    {
                        var patchID = 0;

                        if (int.TryParse(Request.Form["patchID"], out patchID))
                        {
                            var db = _repo.GetContext();

                            var patch = db.TaskPatches.Find(patchID);

                            if (patch != null)
                            {
                                patch.Photo = "resized_" + newFileName;

                                db.Entry(patch).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }

                    return Json(new { result = "true", fileName = fileName, newFileName = "resized_" + newFileName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = "false", message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadTaskDocument(int TaskID)
        {
            if (Request.Files.Count > 0)
            {
                var db = _repo.GetContext();

                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var fi = new FileInfo(fileName);
                        var ext = fi.Extension.ToUpper();
                        newFileName += fi.Extension;

                        var filePath = Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName);
                        file.SaveAs(filePath);

                        if (ext == ".JPG" || ext == ".JPEG" || ext == ".PNG" || ext == ".BMP")
                        {
                            newFileName = "resized_" + newFileName;

                            using (var image = Image.FromFile(filePath))
                            using (var newImage = Images.ScaleImage(image, 600, 800))
                            {
                                image.Dispose();
                                switch (ext)
                                {
                                    case ".JPG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".JPEG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".PNG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName), ImageFormat.Png);
                                        break;
                                    case ".BMP":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName), ImageFormat.Bmp);
                                        break;
                                }
                            }

                            System.IO.File.Delete(filePath);
                        }

                        var doc = new TaskDocuments
                        {
                            TaskID = TaskID,
                            FileName = fileName,
                            FilePath = newFileName
                        };

                        db.TaskDocuments.Add(doc);
                    }

                    db.SaveChanges();

                    return Json(new { result = "true", fileName = fileName, newFileName = newFileName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = "false", message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult SetProjectAsFavourite(int ID, bool Add)
        {
            var db = _repo.GetContext();
            var userID = User.Identity.GetUserId();

            if (Add)
            {
                db.FavouriteProjects.Add(new FavouriteProject
                {
                    ProjectID = ID,
                    UserID = userID
                });
            }
            else
            {
                var data = db.FavouriteProjects.FirstOrDefault(q => q.UserID == userID && q.ProjectID == ID);

                if (data != null)
                    db.FavouriteProjects.Remove(data);
            }

            try
            {
                _repo.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        [HttpGet]
        public ActionResult SetCityAsFavourite(string city, bool Add)
        {
            var db = _repo.GetContext();
            var userID = User.Identity.GetUserId();

            if (Add)
            {
                db.FavouriteCity.Add(new FavouriteCity
                {
                    City = city,
                    UserId = userID
                });
            }
            else
            {
                var data = db.FavouriteCity.FirstOrDefault(q => q.UserId == userID && q.City == city);

                if (data != null)
                    db.FavouriteCity.Remove(data);
            }

            try
            {
                _repo.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        private string CreatePDFReport(int ID, string path, Leanguage leanguage)
        {
            var url = $"http://{HttpContext.Request.Url.Host}:{HttpContext.Request.Url.Port}/Print/Raport/{ID}";

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

            var fileName = Strings.RandomString(16);

            using (var sr = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
            {
                var fileContent = sr.ReadToEnd();

                PdfDocument pdf = PdfGenerator.GeneratePdf(fileContent, PageSize.A4);
                pdf.Save(path);

                //using (var sw = new StreamWriter(Server.MapPath($"~/dist/RaportHTML/{fileName}.html")))
                //{
                //    sw.Write(fileContent);
                //    sw.Flush();
                //    sw.Close();
                //}
            }

            return "";

            //Warning[] warnings;
            //string[] streamIds;
            //string mimeType = string.Empty;
            //string encoding = string.Empty;
            //string extension = string.Empty;

            //LocalReport report = new LocalReport();

            //ReportDataSource reportDataSource = new ReportDataSource();
            //ACSystemDataSet dataSet1 = new ACSystemDataSet();
            //dataSet1.EnforceConstraints = false;
            //dataSet1.BeginInit();

            //reportDataSource.Name = "DataSet1";

            //reportDataSource.Value = dataSet1.Raport;


            //report.DataSources.Clear();
            //report.DataSources.Add(reportDataSource);

            //var readRDLC = System.IO.File.Open(Server.MapPath("~/Reports/Raport_English.rdlc"), FileMode.Open);

            //if (leanguage == Leanguage.Polish)
            //    readRDLC = System.IO.File.Open(Server.MapPath("~/Reports/Raport_Polish.rdlc"), FileMode.Open);

            //if (leanguage == Leanguage.Swedish)
            //    readRDLC = System.IO.File.Open(Server.MapPath("~/Reports/Raport_Swedish.rdlc"), FileMode.Open);

            //report.LoadReportDefinition(readRDLC);

            //dataSet1.EndInit();

            //Reports.ACSystemDataSetTableAdapters.RaportTableAdapter tableAdapter = new Reports.ACSystemDataSetTableAdapters.RaportTableAdapter();
            //tableAdapter.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            //tableAdapter.ClearBeforeFill = true;

            //tableAdapter.Fill(dataSet1.Raport, ID);


            //byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            //System.IO.File.WriteAllBytes(path, bytes);

            //readRDLC.Close();

            //return warnings.Any() ? warnings.Aggregate("", (current, warning) => current + $"{warning.Code} | {warning.Message} | {warning.ObjectName} | {warning.ObjectType} | {warning.Severity}{Environment.NewLine}") : string.Empty;
        }
        }
}
