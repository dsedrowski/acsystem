﻿using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels.Select;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace ACSystem.Fitter.Controllers
{
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult Index()
        {
            var db = new ACSystemContext();

            var userID = User.Identity.GetUserId();
            var orders = db.Order.Where(q => q.FitterID == userID);

            return View(orders);
        }

        public ActionResult SendOrder()
        {
            var db = new ACSystemContext();

            var projectsList = db.Projects.Where(q => q.Status != Core.ProjectStatus.Finished && q.Status != Core.ProjectStatus.FinishedWithFaults).Select(q => new ProjectsSelect { ID = q.Id, ProjectNumber = q.ProjectNumber }).ToList();

            var firstProjectID = (projectsList.Any()) ? projectsList.FirstOrDefault().ID : 0;
            var apartmentsList = db.Apartments.Where(q => q.ProjectId == firstProjectID).Select(q => new ApartmentsSelect { ID = q.Id, ApartmentNumber = q.Letter + " " + q.NumberOfApartment }).ToList();

            //var firstApartmentID = (apartmentsList.Any()) ? apartmentsList.FirstOrDefault().ID : 0;
            var firstApartmentID = 0;
            var tasksList = db.Tasks.Where(q => q.ApartmentId == firstApartmentID && (q.Status == Core.TaskStatus.Free || q.Status == Core.TaskStatus.Stoped || q.Status == Core.TaskStatus.UnderWork)).Select(q => new TasksSelect { ID = q.Id, ProductName = q.GetTaskName(q.Apartment.ProjectId), Sufix = q.TaskSufix }).ToList();

            ViewBag.Projects = projectsList;
            ViewBag.Apartments = apartmentsList;
            ViewBag.Tasks = tasksList;

            return View();
        }

        [HttpPost]
        public ActionResult SendOrder(int ProjectID, int? ApartmentID, int? TaskID, string[] orderItem)
        {
            var db = new ACSystemContext();

            var order = new Order
            {
                ProjectID = ProjectID,
                ApartmentID = ApartmentID,
                TaskID = TaskID,
                FitterID = User.Identity.GetUserId(),
                IsFinished = false
            };

            foreach (var i in orderItem)
            {
                var item = new OrderItem
                {
                    Item = i
                };

                order.OrderItems.Add(item);
            }

            db.Order.Add(order);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return Redirect("/Order");
        }

        public ActionResult ShowOrder(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var order = db.Order.Find(ID);

            if (order == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(order);
        }

        #region AJAX

        [HttpGet]
        public ActionResult ApartmentsList(int projectId)
        {
            var db = new ACSystemContext();

            List<object> json = new List<object>();

            var list = db.Apartments.Where(q => q.ProjectId == projectId).Select(q => new { Id = q.Id, NumberOfApartment = q.Letter + " " + q.NumberOfApartment }).ToList();

            foreach (var l in list)
                json.Add(Json(new { Id = l.Id, NumberOfApartment = l.NumberOfApartment }).Data);

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult TasksList(int apartmentId)
        {
            var db = new ACSystemContext();

            List<object> json = new List<object>();

            var list = db.Tasks.Where(q => q.ApartmentId == apartmentId).Select(q => new { Id = q.Id, ProductName = q.GetTaskName(q.Apartment.ProjectId), Sufix = q.TaskSufix }).ToList();

            foreach (var l in list)
                json.Add(Json(new { Id = l.Id, ProductName = l.ProductName, Sufix = l.Sufix }).Data);

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}