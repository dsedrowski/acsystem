﻿using ACSystem.Core.Extensions;
using ACSystem.Core.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ACSystem.Fitter.Controllers
{
    public class ApartmentController : Controller
    {

        public ActionResult Details(int? ID)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var db = new ACSystemContext();

            var apartment = db.Apartments.Find(ID);

            if (apartment == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var userID = User.Identity.GetUserId();
            ViewBag.Fitter = db.Fitters.Find(userID);

            return View(apartment);
        }

        public ActionResult RaportServices(string[] IDs)
        {
            var patchesIDs = string.Join(",", IDs).ToIntArray(',');

            using (var db = new ACSystemContext())
            {
                var patches = db.TaskPatches.Where(q => patchesIDs.Contains(q.ID));
                var fitter = db.ACSystemUser.Find(User.Identity.GetUserId())?.FullName;

                foreach (var t in patches.GroupBy(q => q.Task))
                {
                    t.Key.TaskPatches.ToList().ForEach(x =>
                    {
                        if (patchesIDs.Contains(x.ID))
                        {
                            x.Done = true;
                            x.FitterName = fitter;
                            x.Timestamp = DateTime.Now;
                        }
                    });

                    if (t.Key.TaskPatches.All(x => x.Done))
                    {
                        t.Key.Status = Core.TaskStatus.Finished;
                        t.Key.Leader = null;
                        t.Key.ClosedDate = DateTime.Now;
                    }
                    else
                        t.Key.Status = Core.TaskStatus.Stoped;

                    t.Key.TaskPatches.ToList().ForEach(x =>
                    {
                        if (patchesIDs.Contains(x.ID))
                        {
                            t.Key.Task_Fitter.Add(new Task_Fitter
                            {
                                FitterId = User.Identity.GetUserId(),
                                WorkDone = 1,
                                WorkTime = 0,
                                Status = t.Key.Status,
                                Timestamp = DateTime.Now,
                                HandDateTime = DateTime.Now,
                                IsActive = true
                            });
                        }
                    });

                    db.Entry(t.Key);
                }

                try
                {
                    db.SaveChanges();

                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }
    }
}