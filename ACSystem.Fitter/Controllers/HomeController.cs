﻿using ACSystem.Core;
using ACSystem.Core.IRepo;
using ACSystem.Core.Models;
using ACSystem.Core.Models.ViewModels;
using ACSystem.Core.Repo;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace ACSystem.Fitter.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IFitterHomeRepo _repo;

        public HomeController(IFitterHomeRepo repo)
        {
            _repo = repo;
        }

        public ActionResult Index()
        {
            string userId = HttpContext.User.Identity.GetUserId();
            Fitters fitter = _repo.GetFitter(userId);
            ACSystemUser user = _repo.GetContext().ACSystemUser.Find(userId);

            ViewBag.Busy = (fitter != null) && (fitter.TaskAlreadyWork.HasValue);
            ViewBag.TaskCount = _repo.TaskAvailableCount(userId, false);
            ViewBag.FavouriteProjectsTaskCount = _repo.TaskAvailableCount(userId, true);

            var toDate = DateTime.Now.AddDays(-3);
            var plan = _repo.GetContext().Events.Where(q => q.Type == EventType.Plan && q.Start >= toDate && q.ReferenceToUser == userId).Select(q => new PlannedTaskList { TaskID = q.ReferenceTo.Value, PlannedDate = q.Start }).ToList();

            foreach (var p in plan)
            {
                var task = _repo.GetContext().Tasks.Find(p.TaskID);

                if (task == null) continue;

                p.TaskName = task.GetTaskName(task.Apartment.ProjectId) + " " + task.TaskSufix;
            }

            var unraportedHours = _repo.GetContext().Fitter_Hours.Where(q => q.FitterID == userId && q.Date <= DateTime.Now && q.Fitter_Hours_Project.Any() == false).Any();
            ViewBag.UnraportedHours = unraportedHours;

            ViewBag.Plan = plan;

            var calendars = _repo.GetContext().Calendar.OrderBy(q => q.Name).ToList();

            ViewBag.Calendars = calendars;

            return View(user ?? new ACSystemUser());
        }

        [HttpGet]
        public async Task<ActionResult> Calendar(DateTime start, DateTime end, string types = "0,1,2", string projectNumber = "", string apartmentNumber = "", string street = "", string city = "", string calendars = "")
        {
            var calListString = calendars.Split(',');

            var calListInt = (from c in calListString where !string.IsNullOrEmpty(c) select int.Parse(c)).Select(num => (int?)num).ToList();

            var user = _repo.GetFitter(User.Identity.GetUserId());

            var projects = _repo.GetContext().Projects.Where(q => (
                                                        (q.StartDate >= start && q.EndDate <= end) ||
                                                        (q.StartDate <= start && q.EndDate >= end) ||
                                                        (q.StartDate <= start && q.EndDate <= end && q.EndDate >= start) ||
                                                        (q.StartDate >= start && q.EndDate >= end && q.StartDate <= end)
                                                        )
                                                        &&
                                                        (q.ProjectNumber.Contains(projectNumber)) &&
                                                        (q.Street.Contains(street)) &&
                                                        (q.City.Contains(city))
                                                        &&
                                                        (calListInt.Contains(q.CalendarID) || q.CalendarID == null)
                                                        &&
                                                        (q.ShowInCalendar)
                                                        );

            var tasks = _repo.TasksList().Where(q =>
                                                    (
                                                        (q.StartDate >= start && q.EndDate <= end) ||
                                                        (q.StartDate <= start && q.EndDate >= end) ||
                                                        (q.StartDate <= start && q.EndDate <= end && q.EndDate >= start) ||
                                                        (q.StartDate >= start && q.EndDate >= end && q.StartDate <= end)
                                                        &&
                                                        (calListInt.Contains(q.Apartment.Project.CalendarID) || q.Apartment.Project.CalendarID == null)
                                                    )
                                                    &&
                                                        (q.Apartment.NumberOfApartment.Contains(apartmentNumber)) &&
                                                        (q.Apartment.Project.ProjectNumber.Contains(projectNumber)) &&
                                                        (q.Apartment.Project.Street.Contains(street)) &&
                                                        (q.Apartment.Project.City.Contains(city))
                                                );
            var events = _repo.EventsList().Where(q =>
                                                      (
                                                        (q.Start >= start && q.End <= end) ||
                                                        (q.Start <= start && q.End >= end) ||
                                                        (q.Start <= start && q.End <= end && q.End >= start) ||
                                                        (q.Start >= start && q.End >= end && q.Start <= end)
                                                      )
                                                );

            List<CalendarJson> json = new List<CalendarJson>();

            if (projects != null && types.Contains("4"))
            {
                foreach (var t in projects)
                {
                    var allDay = ((t.StartDate - t.EndDate).Days < 0);

                    if (calListInt.Contains(t.CalendarID) == false && t.CalendarID != null)
                        continue;

                    if ((t.StartDate.Day == t.EndDate.Day) && (t.StartDate.Month == t.EndDate.Month) && (t.StartDate.Year == t.EndDate.Year))
                    {
                        allDay = false;
                    }

                    var color = "#333333";

                    if (t.Calendar != null)
                        color = t.Calendar.ColorHex;

                    json.Add(new CalendarJson
                    {
                        id = "project-" + t.Id,
                        title = $"Projekt {t.ProjectNumber}",
                        allDay = allDay,
                        start = t.StartDate.ToString("yyyy-MM-dd HH:mm"),
                        end = (allDay) ? t.EndDate.AddDays(1).ToString("yyyy-MM-dd HH:mm") : t.EndDate.ToString("yyyy-MM-dd HH:mm"),
                        editable = true,
                        color = color
                    });
                }
            }

            if (tasks != null && types.Contains("0"))
            {
                foreach (var t in tasks)
                {
                    if (user.User.UserType != UserType.Wewnetrzny &&
                        t.Task_AccessGrant.Count(q => q.FitterID == user.Id) == 0)
                        continue;

                    if (calListInt.Contains(t.Apartment.Project.CalendarID) == false && t.Apartment.Project.CalendarID != null)
                        continue;

                    var allDay = ((t.StartDate - t.EndDate).Days < 0);

                    if ((t.StartDate.Day == t.EndDate.Day) && (t.StartDate.Month == t.EndDate.Month) && (t.StartDate.Year == t.EndDate.Year))
                    {
                        allDay = false;
                        t.StartDate = DateTime.Parse(t.StartDate.ToString("yyyy-MM-dd") + " 07:00");
                        t.EndDate = DateTime.Parse(t.EndDate.ToString("yyyy-MM-dd") + " 10:00");
                    }

                    var color = "#333333";

                    if (t.Apartment.Project.Calendar != null)
                        color = t.Apartment.Project.Calendar.ColorHex;

                    //if (t.Production == TaskProduction.Complaint) color = "#FFDA00";
                    //else if (t.Production == TaskProduction.Warranty) color = "#FF4900";

                    var id = $"group-{t.Apartment.ProjectId}";

                    if (json.Any(q => q.id == id)) continue;

                    json.Add(new CalendarJson
                    {
                        id = id,
                        title = $"Projekt {t.Apartment.Project.ProjectNumber}, {t.Apartment.Project.City} {t.Apartment.Project.Street}",
                        allDay = allDay,
                        start = t.StartDate.ToString("yyyy-MM-dd HH:mm"),
                        end = (allDay) ? t.EndDate.AddDays(1).ToString("yyyy-MM-dd HH:mm") : t.EndDate.ToString("yyyy-MM-dd HH:mm"),
                        editable = true,
                        color = color
                        //id = "task-" + t.Id,
                        //title = $"Projekt {t.Apartment.Project.ProjectNumber}, LGH {t.Apartment.NumberOfApartment}, Produkt {t.GetTaskName(q.Apartment.ProjectId)} {t.TaskSufix}",
                        //allDay = allDay,
                        //start = t.StartDate.ToString("yyyy-MM-dd HH:mm"),
                        //end = (allDay) ? t.EndDate.AddDays(1).ToString("yyyy-MM-dd HH:mm") : t.EndDate.ToString("yyyy-MM-dd HH:mm"),
                        //editable = true,
                        //color = color
                    });
                }
            }

            if (events != null)
            {
                foreach (var e in events)
                {
                    var title = String.Empty;
                    var id = String.Empty;

                    var skip = false;

                    var color = e.Calendar?.ColorHex ?? "#333333";

                    if (e.ReferenceTo.HasValue || !string.IsNullOrEmpty(e.ReferenceToUser))
                    {
                        switch (e.Type)
                        {
                            case EventType.Project:
                                if (types.Contains("3") == false) continue;

                                var project = _repo.GetContext().Projects.Find(e.ReferenceTo.Value);

                                if (project == null)
                                    skip = true;

                                if (calListInt.Contains(project.CalendarID) == false && project.CalendarID != null)
                                    skip = true;

                                title = $"{project.ProjectNumber} {e.Title}";
                                id = $"event4project-{e.Id}";

                                if (project.Calendar != null)
                                    color = project.Calendar.ColorHex;

                                break;

                            case EventType.Task:
                                if (types.Contains("1") == false)
                                    continue;
                                var task = await _repo.GetTask(e.ReferenceTo.Value);
                                if (task == null)
                                    skip = true;

                                if (user.User.UserType != UserType.Wewnetrzny &&
                                    task.Task_AccessGrant.Count(q => q.FitterID == user.Id) == 0)
                                    skip = true;

                                if (calListInt.Contains(task.Apartment.Project.CalendarID) == false && task.Apartment.Project.CalendarID != null)
                                    skip = true;

                                title = $"{task.Apartment.Project.ProjectNumber}.{task.Apartment.NumberOfApartment} {task.GetTaskName(task.Apartment.ProjectId)} {task.TaskSufix} {e.Title}";
                                id = $"event4task-{e.Id}";

                                if (task.Apartment.Project.Calendar != null)
                                    color = task.Apartment.Project.Calendar.ColorHex;

                                break;
                            case EventType.User:
                                if (types.Contains("2") == false)
                                    continue;
                                var fitter = _repo.GetContext().Fitters.FirstOrDefault(q => q.Id == e.ReferenceToUser);
                                if (fitter == null)
                                    skip = true;

                                if (calListInt.Contains(fitter.User.CalendarID) == false && fitter.User.CalendarID != null)
                                    skip = true;

                                title = $"{fitter.User.FullName} {e.Title}";
                                id = $"event4fitter-{e.Id}";

                                if (fitter.User.Calendar != null)
                                    color = fitter.User.Calendar.ColorHex;

                                break;
                            case EventType.Plan:
                                if (types.Contains("3") == false)
                                    continue;

                                if (e.ReferenceToUser != User.Identity.GetUserId())
                                    continue;

                                var task2 = await _repo.GetTask(e.ReferenceTo.Value);
                                if (task2 == null)
                                    break;

                                if (calListInt.Contains(task2.Apartment.Project.CalendarID) == false && task2.Apartment.Project.CalendarID != null)
                                    continue;

                                title =
                                    $"{task2.Apartment.Project.ProjectNumber}.{task2.Apartment.NumberOfApartment} {task2.GetTaskName(task2.Apartment.ProjectId)} {task2.TaskSufix} {e.Title}";
                                id = $"event4plan-{e.Id}";

                                break;
                        }
                    }

                    if (skip == false)
                    {
                        json.Add(new CalendarJson
                        {
                            id = id,
                            title = title,
                            allDay = e.AllDay,
                            start = e.Start.ToString("yyyy-MM-dd HH:mm"),
                            end = (e.AllDay) ? e.End.AddDays(1).ToString("yyyy-MM-dd HH:mm") : e.End.ToString("yyyy-MM-dd HH:mm"),
                            editable = true,
                            color = color
                        });
                    }
                }
            }

            return Json(json, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public async Task<ActionResult> TaskEvent(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var userId = HttpContext.User.Identity.GetUserId();
            ViewBag.Fitter = _repo.GetFitter(userId);
            ViewBag.IsFitterBusy = _repo.IsFitterBusy(userId);

            var t = await _repo.GetTask((int)id);

            return PartialView(t);
        }

        [HttpGet]
        public async Task<ActionResult> EventForTask(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var userId = HttpContext.User.Identity.GetUserId();
            ViewBag.Fitter = _repo.GetFitter(userId);

            var event4Task = await _repo.GetEvent(id.Value);

            if (event4Task.ReferenceTo.HasValue)
            {
                var t = await _repo.GetTask(event4Task.ReferenceTo.Value);
                var task = new TaskFullViewModel
                {
                    ProjectId = t.Apartment.ProjectId,
                    ProjectNumber = t.Apartment.Project.ProjectNumber,
                    ApartmentId = t.ApartmentId,
                    ApartmentNumber = t.Apartment.NumberOfApartment,
                    CurrentFitterId = t.Fitters.FirstOrDefault()?.Id ?? null,
                    CurrentFitterName = t.Fitters.FirstOrDefault()?.User.FullName ?? null,
                    ProductName = t.GetTaskName(t.Apartment.ProjectId)
                };
                ViewBag.TaskData = task;
            }


            return PartialView(event4Task);
        }

        [HttpGet]
        public async Task<ActionResult> EventForProject(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var event4Project = await _repo.GetEvent(id.Value);

            if (event4Project.ReferenceTo.HasValue)
            {
                var t = _repo.GetContext().Projects.Find(event4Project.ReferenceTo.Value);
                var project = new ProjectFullViewModel()
                {
                    ProjectId = t.Id,
                    ProjectNumber = t.ProjectNumber
                };

                ViewBag.ProjectData = project;
            }


            return PartialView(event4Project);
        }

        [HttpGet]
        public async Task<ActionResult> EventForPlan(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var userId = HttpContext.User.Identity.GetUserId();
            ViewBag.Fitter = _repo.GetFitter(userId);

            var event4Plan = await _repo.GetEvent(id.Value);

            if (event4Plan.ReferenceTo.HasValue)
            {
                var t = await _repo.GetTask(event4Plan.ReferenceTo.Value);
                var task = new TaskFullViewModel
                {
                    ProjectId = t.Apartment.ProjectId,
                    ProjectNumber = t.Apartment.Project.ProjectNumber,
                    ApartmentId = t.ApartmentId,
                    ApartmentNumber = t.Apartment.NumberOfApartment,
                    CurrentFitterId = t.Fitters.FirstOrDefault()?.Id ?? null,
                    CurrentFitterName = t.Fitters.FirstOrDefault()?.User.FullName ?? null,
                    ProductName = t.GetTaskName(t.Apartment.ProjectId)
                };
                ViewBag.TaskData = task;
            }


            return PartialView(event4Plan);
        }

        [HttpGet]
        public async Task<ActionResult> EventForFitter(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var userId = HttpContext.User.Identity.GetUserId();
            ViewBag.Fitter = _repo.GetFitter(userId);

            var event4Fitter = await _repo.GetEvent(id.Value);

            return PartialView(event4Fitter);
        }

        public ActionResult PayrollList()
        {
            var fitterID = HttpContext.User.Identity.GetUserId();

            var list = _repo.PayrollListForFitter(fitterID);

            ViewBag.PayrollItems = _repo.PayrollItemsForFitter(fitterID);

            return View(list);
        }

        public ActionResult PayrollItems(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var fitterID = HttpContext.User.Identity.GetUserId();

            var list = _repo.PayrollItemsForFitter(fitterID);

            var payroll = list.Where(q => q.PayrollID == id.Value).ToList();

            return View(payroll);
        }

        public ActionResult NotInPayroll()
        {
            var _db = new ACSystemContext();
            var fitterID = HttpContext.User.Identity.GetUserId();
            var list = _db.Task_Fitter.Where(q => q.FitterId == fitterID && q.InPayroll == false && q.Status == ACSystem.Core.TaskStatus.Finished).GroupBy(x => x.Task).ToList();
            return View(list);
        }

        public ActionResult Documents() {
            using (ACSystemContext dtx = new ACSystemContext()) {
                string userID = HttpContext.User.Identity.GetUserId();
                return View(dtx.UserDocuments.Where(q => q.UserID == userID && q.ShowToUser).ToList());
            }
        }

        private class CalendarJson
        {
            public string id { get; set; }
            public string title { get; set; }
            public bool allDay { get; set; }
            public string start { get; set; }
            public string end { get; set; }
            public bool editable { get; set; }
            public string color { get; set; }
        }
    }
}