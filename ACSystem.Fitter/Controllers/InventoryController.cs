﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ACSystem.Core.Models;
using Microsoft.AspNet.Identity;
using ACSystem.Core.Service;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using ACSystem.Core.Extensions;
using ACSystem.Core.Repo;

namespace ACSystem.Fitter.Controllers
{
    public class InventoryController : Controller
    {
        private ACSystemContext db = new ACSystemContext();

        public ActionResult Create()
        {
            var warehouses = db.InventoryWarehouse.ToList();
            var types = db.InventoryType.ToList();
            var accessories = db.Inventory_Accessories.ToList();

            ViewBag.Warehouses = warehouses;
            ViewBag.Types = types;
            ViewBag.Accessories = accessories;

            return View();
        }

        [HttpPost]
        public ActionResult Create(Inventory inventory)
        {
            var _repo = new InventoryRepo(db);

            var warehouses = db.InventoryWarehouse.ToList();
            var types = db.InventoryType.ToList();
            var accessoriesList = db.Inventory_Accessories.ToList();

            ViewBag.Warehouses = warehouses;
            ViewBag.Types = types;
            ViewBag.Accessories = accessoriesList;

            var accessories = Request.Form["AccessoriesID[]"]?.ToIntArray(',') ?? new List<int>().ToArray();
            var files = Request.Form["Files[]"]?.Split(',') ?? new List<string>().ToArray();

            foreach (var accessory in accessories)
            {
                var connect = new Inventory_Accessories_Connect
                {
                    AccessoryID = accessory,
                    Accessory = _repo.GetContext().Inventory_Accessories.Find(accessory)
                };

                inventory.Inventory_Accessories_Connect.Add(connect);
            }

            foreach (var file in files)
            {
                var inventoryFile = new Inventory_Photo
                {
                    Name = file.Split(';')[0],
                    URL = file.Split(';')[1]
                };

                inventory.Inventory_Photo.Add(inventoryFile);
            }

            inventory.BuyPriceNetto = Decimal.Divide(inventory.BuyPriceBrutto, (decimal)1.23);

            if (inventory.EvidenceNumber.IsNullOrEmpty() && inventory.EvidenceNumber2.IsNullOrEmpty())
            {
                ModelState.AddModelError("EvidenceNumber", "Przynajmniej jeden numer ewidencyjny musi zostać uzupełniony");
                ModelState.AddModelError("EvidenceNumber2", "Przynajmniej jeden numer ewidencyjny musi zostać uzupełniony");
            }

            if (ModelState.IsValid)
            {
                inventory.OwnerType = Core.InventoryOwnerType.Private;
                inventory.OwnerID = User.Identity.GetUserId();

                if (_repo.Create(inventory) > 0)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                try
                {
                    _repo.SaveChanges();

                    if (inventory.OwnerType == Core.InventoryOwnerType.Private)
                        _repo.GiveInventory(inventory.ID, inventory.OwnerID, null);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "InventoryCreate", User.Identity.Name);
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                return RedirectToAction("FreeList");
            }

            return View(inventory);
        }

        public ActionResult FreeList(string id)
        {
            if (string.IsNullOrEmpty(id))
                id = HttpContext.User.Identity.GetUserId();

            var list = db.Inventory.Where(q => q.Status == Core.InventoryStatus.Free && q.OwnerType == Core.InventoryOwnerType.General).ToList();
            var fitter_inventoryList = db.Inventory_Fitter.Where(q => q.PutDate == null && q.UserID == id).Select(q => q.InventoryID);
            var forFitterList = db.Inventory.Where(q => fitter_inventoryList.Contains(q.ID) && q.OwnerType == Core.InventoryOwnerType.General).ToList();
            var privateList = db.Inventory.Where(q => q.OwnerID == id && q.OwnerType == Core.InventoryOwnerType.Private).ToList();

            ViewBag.Free = list;
            ViewBag.ForFitter = forFitterList;
            ViewBag.Private = privateList;

            return View();
        }

        public ActionResult ForFitter(string id)
        {
            if(string.IsNullOrEmpty(id))
                id = HttpContext.User.Identity.GetUserId();

            var fitter_inventoryList = db.Inventory_Fitter.Where(q => q.PutDate == null && q.UserID == id).Select(q => q.InventoryID);

            var list = db.Inventory.Where(q => fitter_inventoryList.Contains(q.ID)).ToList();

            return View(list);
        }

        public ActionResult GetInventory(int ID)
        {
            var inventory = db.Inventory.Find(ID);

            var projects = db.Projects.Where(q => q.Status != Core.ProjectStatus.Finished && q.Status != Core.ProjectStatus.FinishedWithFaults).ToList();

            ViewBag.Projects = projects;

            return View(inventory);
        }

        [HttpPost]
        public ActionResult GetInventory(int ID, int ProjectID)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var inventory = db.Inventory.Find(ID);

            inventory.WarehouseID = null;
            inventory.ProjectID = ProjectID;
            inventory.Status = Core.InventoryStatus.Busy;
            db.Entry(inventory).State = EntityState.Modified;

            var fitter_inventory = new Inventory_Fitter
            {
                UserID = userId,
                InventoryID = inventory.ID,
                ProjectID = ProjectID,
                TakeDate = DateTime.Now
            };

            db.Inventory_Fitter.Add(fitter_inventory);

            db.SaveChanges();

            return Redirect("/Inventory/FreeList/" + userId);
        }

        public ActionResult PutInventory(int ID)
        {
            var inventory = db.Inventory.Find(ID);

            var warehouses = db.InventoryWarehouse.ToList();
            ViewBag.Warehouses = warehouses;

            var projects = db.Projects.Where(q => q.Status != Core.ProjectStatus.Finished).ToList();
            ViewBag.Projects = projects;

            return View(inventory);
        }

        [HttpPost]
        public ActionResult PutInventory(int ID, int WarehouseID, int ProjectID, bool MoveInventory = false)
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var inventory = db.Inventory.Find(ID);

            if (MoveInventory)
            {
                inventory.ProjectID = ProjectID;
                db.Entry(inventory).State = EntityState.Modified;

                var toGetWarehouseActivity = inventory.Inventory_Fitter.Where(q => q.PutDate.HasValue).OrderByDescending(q => q.PutDate).FirstOrDefault();
                var toCloseActivity = inventory.Inventory_Fitter.Where(q => !q.PutDate.HasValue).OrderByDescending(q => q.TakeDate).FirstOrDefault();

                toCloseActivity.WarehouseID = (toGetWarehouseActivity != null) ? toGetWarehouseActivity.WarehouseID : db.InventoryWarehouse.FirstOrDefault().ID;
                toCloseActivity.PutDate = DateTime.Now;

                db.Entry(toCloseActivity).State = EntityState.Modified;

                var fitter_inventory = new Inventory_Fitter
                {
                    UserID = userId,
                    InventoryID = inventory.ID,
                    ProjectID = ProjectID,
                    TakeDate = DateTime.Now
                };

                db.Inventory_Fitter.Add(fitter_inventory);
            }
            else
            {
                inventory.WarehouseID = WarehouseID;
                inventory.ProjectID = null;
                inventory.Status = Core.InventoryStatus.Free;
                db.Entry(inventory).State = EntityState.Modified;

                var fitter_inventory = db.Inventory_Fitter.Find(inventory.Inventory_Fitter.OrderByDescending(q => q.TakeDate).FirstOrDefault().ID);

                fitter_inventory.WarehouseID = WarehouseID;
                fitter_inventory.PutDate = DateTime.Now;

                db.Entry(fitter_inventory).State = EntityState.Modified;
            }

            db.SaveChanges();

            return Redirect("/Inventory/FreeList/" + userId);
        }

        [HttpPost]
        public ActionResult AJAX_CreateAccessory(string EvidenceNumber, string Name, string URL)
        {
            var db = new ACSystemContext();

            var connect = new Inventory_Accessories_Connect();


            var accessory = new Inventory_Accessories
            {
                EvidenceNumber = EvidenceNumber,
                Name = Name
            };

            var photo = new Inventory_Accessories_Photo
            {
                URL = URL
            };

            accessory.Photos.Add(photo);

            db.Inventory_Accessories.Add(accessory);

            try
            {
                db.SaveChanges();

                return Json(new { success = true, ID = accessory.ID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Core.Service.Helpers.Log(ex, "AJAX_CreateAccessory", User.Identity.Name);

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AJAX_UploadCheckConditionImage()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var fi = new FileInfo(fileName);
                        var ext = fi.Extension.ToUpper();
                        newFileName += fi.Extension;

                        var filePath = Path.Combine(Server.MapPath("~/dist/Documents/"), newFileName);
                        file.SaveAs(filePath);

                        if (ext == ".JPG" || ext == ".JPEG" || ext == ".PNG" || ext == ".BMP")
                        {
                            newFileName = "resized_" + newFileName;

                            using (var image = Image.FromFile(filePath))
                            using (var newImage = Images.ScaleImage(image, 600, 800))
                            {
                                image.Dispose();
                                switch (ext)
                                {
                                    case ".JPG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".JPEG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".PNG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName), ImageFormat.Png);
                                        break;
                                    case ".BMP":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName), ImageFormat.Bmp);
                                        break;
                                }
                            }

                            System.IO.File.Delete(filePath);
                        }
                    }

                    return Json(new { success = true, URL = "/dist/Documents/" + newFileName }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AJAX_AddInventoryFile()
        {
            if (Request.Files.Count > 0)
            {
                var db = new ACSystemContext();

                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    string fileName = "";
                    var newFileName = Strings.RandomString(24);

                    Inventory_Photo inventoryPhoto = null;

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testFiles = file.FileName.Split(new char[] { '\\' });
                            fileName = testFiles[testFiles.Length - 1];
                        }
                        else
                        {
                            fileName = file.FileName;
                        }

                        var fi = new FileInfo(fileName);
                        var ext = fi.Extension.ToUpper();
                        newFileName += fi.Extension;


                        var filePath = Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName);
                        file.SaveAs(filePath);

                        if (ext == ".JPG" || ext == ".JPEG" || ext == ".PNG" || ext == ".BMP")
                        {
                            newFileName = "resized_" + newFileName;

                            using (var image = Image.FromFile(filePath))
                            using (var newImage = Images.ScaleImage(image, 600, 800))
                            {
                                image.Dispose();
                                switch (ext)
                                {
                                    case ".JPG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".JPEG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName), ImageFormat.Jpeg);
                                        break;
                                    case ".PNG":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName), ImageFormat.Png);
                                        break;
                                    case ".BMP":
                                        newImage.Save(Path.Combine(Server.MapPath($"~/dist/Documents/"), newFileName), ImageFormat.Bmp);
                                        break;
                                }
                            }

                            System.IO.File.Delete(filePath);
                        }
                    }

                    return Json(new { result = "true", FileName = fileName, URL = "/dist/Documents/" + newFileName, ID = (inventoryPhoto != null) ? inventoryPhoto.ID : -1 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = "false", message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { result = "false", message = "Nie wybrano zdjęcia" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AJAX_DeleteFile(int ID, string URL)
        {
            if (ID > 0)
            {
                var dbFile = db.Inventory_Photo.Find(ID);

                db.Inventory_Photo.Remove(dbFile);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "AJAX_DeleteFile", User.Identity.Name);

                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }

            var filePath = Server.MapPath("~" + URL);

            if (System.IO.File.Exists(filePath))
            {
                try
                {
                    System.IO.File.Delete(filePath);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, "AJAX_DeleteFile", User.Identity.Name);
                }
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
    }
}
