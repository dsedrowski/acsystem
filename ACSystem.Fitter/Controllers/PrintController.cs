﻿using ACSystem.Core.Filters;
using ACSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ACSystem.Reports;
using Microsoft.Reporting.WebForms;

namespace ACSystem.Fitter.Controllers
{
    public class PrintController : Controller
    {
        private ACSystemContext _db;

        public PrintController()
        {
            _db = new ACSystemContext();
        }

        // GET: Print
        public ActionResult Invoice(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var invoice = _db.Invoice.Find(id.Value);

            if (invoice == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var pdfPath = Server.MapPath($"~/dist/Invoice/{id.Value}.pdf");

            if (!System.IO.File.Exists(pdfPath))
            {
                CreatePDF(id.Value, pdfPath);
            }

            return PartialView(invoice);
        }

        public ActionResult InvoiceExist(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var invoice = _db.Invoice.Find(id.Value);

            if (invoice == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var pdfPath = Server.MapPath($"~/dist/Invoice/{id.Value}.pdf");

            if (!System.IO.File.Exists(pdfPath))
            {
                CreatePDF(id.Value, pdfPath);
            }

            return Json(new {success = true}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Raport(int? ID, bool Print = false)
        {
            if (!ID.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var task = _db.Tasks.Find(ID.Value);

            if (task == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.Print = Print;

            return PartialView(task);
        }

        private string CreatePDF(int ID, string path)
        {
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            LocalReport report = new LocalReport();

            ReportDataSource reportDataSource = new ReportDataSource();
            ACSystemDataSet dataSet1 = new ACSystemDataSet();
            dataSet1.EnforceConstraints = false;
            dataSet1.BeginInit();

            reportDataSource.Name = "DataSet1";

            reportDataSource.Value = dataSet1.Invoice1;


            report.DataSources.Clear();
            report.DataSources.Add(reportDataSource);

            var readRDLC = System.IO.File.Open(Server.MapPath("~/Reports/Invoice.rdlc"), FileMode.Open);

            report.LoadReportDefinition(readRDLC);

            readRDLC.Dispose();
            readRDLC.Close();

            dataSet1.EndInit();

            Reports.ACSystemDataSetTableAdapters.Invoice1TableAdapter tableAdapter = new Reports.ACSystemDataSetTableAdapters.Invoice1TableAdapter();
            tableAdapter.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            tableAdapter.ClearBeforeFill = true;

            tableAdapter.Fill(dataSet1.Invoice1, ID);
            
            byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            System.IO.File.WriteAllBytes(path, bytes);
            
            if (warnings.Count() > 0)
            {
                var msg = "";

                foreach (var warning in warnings)
                {
                    msg += $"{warning.Code} | {warning.Message} | {warning.ObjectName} | {warning.ObjectType} | {warning.Severity}{Environment.NewLine}";
                }

                return msg;
            }

            return String.Empty;
        }
    }
}