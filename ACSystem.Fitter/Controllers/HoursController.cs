﻿using ACSystem.Core;
using ACSystem.Core.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ACSystem.Fitter.Controllers
{
    public class HoursController : Controller
    {
        // GET: Hours
        public ActionResult Index(int? month, int? year)
        {
            if (!month.HasValue && !year.HasValue)
            {
                month = DateTime.Now.Month;
                year = DateTime.Now.Year;
            }
            else if (!month.HasValue || !year.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            using (var db = new ACSystemContext())
            {
                var fitterID = HttpContext.User.Identity.GetUserId();
                var monthDays = DateTime.DaysInMonth(year.Value, month.Value);
                var startDate = new DateTime(year.Value, month.Value, 1);
                var endDate = new DateTime(year.Value, month.Value, monthDays);

                var hours = db.Fitter_Hours.Include("Fitter_Hours_Project").Where(q => q.FitterID == fitterID && q.Date >= startDate && q.Date <= endDate).ToList();

                if (hours.Any() == false)
                {
                    for (var i = 1; i <= monthDays; i++)
                    {
                        var newData = new Fitter_Hours
                        {
                            FitterID = fitterID,
                            Date = new DateTime(year.Value, month.Value, i)
                        };

                        hours.Add(newData);
                    }

                    db.Fitter_Hours.AddRange(hours);

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, "Tworzenie listy godzin dla miesiąca");
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                }

                ViewBag.Projects = db.Projects.Where(q => q.IsChecked == false).ToList();

                return View(hours);                    
            }
        }

        public ActionResult GetDetails(int id)
        {
            using (var db = new ACSystemContext())
            {
                var List = new List<object>();
                var record = db.Fitter_Hours.Find(id);

                if (record == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                foreach(var d in db.Fitter_Hours_Project.Where(q => q.Fitter_HoursID == id).ToList())
                {
                    List.Add(new { d.ID, Project = d.Project.ProjectNumber, d.Hours });
                }

                return Json(new { Date = record.Date.ToString("yyyy-MM-dd"), List }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddHours(int id, int projectID, decimal hours, bool freeDay)
        {
            using (var db = new ACSystemContext())
            {
                var data = new Fitter_Hours_Project
                {
                    Fitter_HoursID = id,
                    ProjectID = projectID,
                    Hours = hours,
                    FreeDay = freeDay
                };

                db.Fitter_Hours_Project.Add(data);

                try
                {
                    db.SaveChanges();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, "Dodawanie godzin");
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        public ActionResult DeleteHours(int id)
        {
            using (var db = new ACSystemContext())
            {
                var data = db.Fitter_Hours_Project.Find(id);

                if (data == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                db.Fitter_Hours_Project.Remove(data);

                try
                {
                    db.SaveChanges();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, "Dodawanie godzin");
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }

        public ActionResult UpdateHours(int hoursID, decimal hours)
        {
            using (var db = new ACSystemContext())
            {
                var data = db.Fitter_Hours.Find(hoursID);

                if (data == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);


                db.Entry(data).State = System.Data.Entity.EntityState.Modified;

                try
                {
                    db.SaveChanges();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Core.Service.Helpers.Log(ex, Request.Url.AbsoluteUri, "Edycja godzin");
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
        }
    }
}