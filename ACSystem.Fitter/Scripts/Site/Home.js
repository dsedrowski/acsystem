﻿$(document).ready(function () {
    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_square-blue',
    });

    $('input[type="checkbox"].minimal').iCheck('check');
    
    $('.filter').on('ifChanged', function () {
        SaveCurrentData();

        $('#calendar').fullCalendar('destroy');
        CalendarInit();
    });

    

    CalendarInit();
});

$(document).on('click', '#get-task', function () {
    if ($(this).hasClass('disabled')) {
        $.alert('Nie można pobrać więcej niż jedno zadanie. Zakończ lub zatrzymaj aktualnie wykonywane zadanie by wybrać inne!');
    }
    else {
        var id = $(this).data('task-id');

        $.ajax({
            url: '/Projects/GetTask',
            dataType: 'json',
            type: 'GET',
            data: { id: id },
            success: function (data) {
                if (data.success === true)
                    location.assign('/Projects/CurrentTask');
                else
                    $.alert('Błąd. Proszę spróbować jeszcze raz.');
            },
            error: function (XMLHttpRequest) {
                $.alert('Błąd ' + XMLHttpRequest.status + '. Proszę spróbować jeszcze raz.');
            }
        });
    }
});

$(document).on('click', '#start-search', function () {
    SaveCurrentData();

    $('#calendar').fullCalendar('destroy');
    CalendarInit();
})

$(document).on('click', '.close-modal', function () {
    Custombox.modal.close();
})

$(document).on("click", ".calendar-check", function () {
    if ($(this).hasClass("active"))
        $(this).removeClass("active");
    else
        $(this).addClass("active");

    SaveCurrentData();

    $('#calendar').fullCalendar('destroy');
    CalendarInit();
})

function SaveCurrentData() {

    var viewObject = $('#calendar').fullCalendar('getView');

    if (viewObject && viewObject.type)
        currentView = viewObject.type;

    if (currentView === 'listMonth')
        $('#print-calendar').show();
    else
        $('#print-calendar').hide();

    var moment = $('#calendar').fullCalendar('getDate');

    if (moment)
        myCurrentDate = moment.format();

}

function CalendarInit() {
    
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            right: 'month, listMonth, agendaFourDay, listWeek, agendaDay'
        },
        buttonText: {
            today: 'Dzisiaj',
            day: 'Dzień',
            listMonth: 'Miesiąc (lista)',
            listWeek: 'Tydzień (lista)'
        },
        events: {
            url: '/Home/Calendar',
            type: 'GET',
            data: {
                types: GetFilter(),
                projectNumber: $('#project-number-search').val(),
                apartmentNumber: $('#apartment-number-search').val(),
                street: $('#street-search').val(),
                city: $('#city-search').val(),
                calendars: GetCalendars(),
            }
        },
        views: {
            listMonth: {
                listDayFormat: '[Tydzień] WW, DD MMMM'
            },
            agendaFourDay: {
                type: 'agenda',
                duration: { days: 4 },
                buttonText: '4 Dni'
            }
        },
        viewRender: function (view, element) {
            var currentView = view.name;

            if (currentView === 'listMonth')
                $('#print-calendar').show();
            else
                $('#print-calendar').hide();

            var startDate = new Date(view.intervalStart);
            var endDate = new Date(view.intervalEnd);

            var pad = "00";
            var year = startDate.getFullYear().toString();
            var month = (startDate.getMonth() + 1).toString();
            var day = startDate.getDate().toString();

            var endYear = endDate.getFullYear().toString();
            var endMonth = (endDate.getMonth() + 1).toString();
            var endDay = (endDate.getDate() - 1).toString();

            var endDateString = endYear + "-" + pad.substring(0, pad.length - endMonth.length) + endMonth + "-" + pad.substring(0, pad.length - endDay.length) + endDay;

            var dateString = year + "-" + pad.substring(0, pad.length - month.length) + month + "-" + pad.substring(0, pad.length - day.length) + day;

            if (dateString != endDateString)
                dateString += "  -  " + endDateString;


            $(".calendar-date").html(dateString);
        },
        noEventMessage: "Brak wydarzeń w podanym okresie czasu",
        editable: true,
        dropable: true,
        weekNumbers: true,
        weekNumberCalculation: "local",
        locale: "pl",
        defaultView: currentView,
        defaultDate: myCurrentDate,
        monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwieceń', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
        monthNamesShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
        dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Niedziela'],
        dayNamesShort: ['Nd', 'Pon', 'Wt', 'Śr', 'Czw', 'Pt', 'Sb'],
        weekNumberTitle: 'T',
        eventLimit: true,
        eventLimitText: 'więcej...',
        allDayText: '',
        timeFormat: 'HH:mm',
        axisFormat: 'HH:mm',
        columnFormat: 'ddd D/M',
        eventClick: function (calEvent, jsEvent, view) {
            OpenEventDetails(calEvent.id);
        }
    })
}

function GetFilter() {
    var isFirst = true;
    var filter = "";

    var checkbox1 = $('#fltr-chbx-1').prop('checked');
    var checkbox2 = $('#fltr-chbx-2').prop('checked');
    var checkbox3 = $('#fltr-chbx-3').prop('checked');
    var checkbox4 = $('#fltr-chbx-4').prop('checked');
    var checkbox5 = $('#fltr-chbx-5').prop('checked');

    if (checkbox1 === true) {
        filter += "0";
        isFirst = false;
    }

    if (checkbox2 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 1;
        isFirst = false;
    }

    if (checkbox3 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 2;
        isFirst = false;
    }

    if (checkbox4 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 3;
        isFirst = false;
    }

    if (checkbox5 === true) {
        if (isFirst != true)
            filter += ",";

        filter += 4;
        isFirst = false;
    }

    return filter;

}

function OpenEventDetails(id) {
    var type = id.split('-')[0];
    var eventId = id.split('-')[1];

    var url = "";

    switch (type) {
        case 'task':
            url = "/Home/TaskEvent/" + eventId;
            LoadModalContent("#modal", url);
            break;
        case 'event4task':
            url = "/Home/EventForTask/" + eventId;
            LoadModalContent("#modal", url);
            break;
        case 'event4fitter':
            url = "/Home/EventForFitter/" + eventId;
            LoadModalContent("#modal", url);
            break;
        case 'event4plan':
            url = "/Home/EventForPlan/" + eventId;
            LoadModalContent("#modal", url);
        case 'event4project':
            url = "/Home/EventForProject/" + eventId;
            LoadModalContent("#modal", url);
            break;
        case 'project':
            location.assign("/Projects/Project/" + eventId);
            break;
        case 'group':
            location.assign("/Projects/Project/" + eventId);
            break;
        default:
            $.alert('Brak podanego typu wydarzenia');
    }

    //OpenModal('#modal', '.modal-dialog', url);
}

function LoadModalContent(element, url) {
    GetAjaxLoadModal(url, element);
}

function GetAjaxLoadModal(url, element) {
    $.ajax({
        url: url,
        async: true,
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            $(element).html(data);
            NewModal(element);
        },
        error: function (xhr, status) {
            toastr.error("Wystąpił błąd podczas ładowania zawartości!");
        }
    });
}

function NewModal(element) {
    new Custombox.modal({
        content: {
            effect: 'fadein',
            target: element,
            animateFrom: 'top',
            animateTo: 'bottom',
            positionX: 'center',
            positionY: 'center',
            fullscreen: false
        },
        overlay: {
            active: 0.5
        }
    }).open();
}

function GetCalendars() {
    var isFirst = true;
    var calendars = "";

    $(".calendar-check.active input").each(function () {
        if (isFirst === false)
            calendars += ",";

        calendars += $(this).val();

        isFirst = false;
    });

    return calendars;
}