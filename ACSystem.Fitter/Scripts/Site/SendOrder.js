﻿var nextLp = 0;

$(document).ready(function () {
    $('#Project').select2();
    $('#Apartments').select2();
    $('#Tasks').select2();
});

$(document).on('change', '#Project', function () {
    var projectId = $(this).val();

    $('#Apartments').html('');
    $('#Tasks').html('');

    $.getJSON('/Order/ApartmentsList?projectId=' + projectId, function (data) {
        var first = true;
        var apartmentId = null;

        $('#Apartments').append("<option></option>");

        $.each(data, function (index, value) {
            if (first === true) {
                apartmentId = value.Id;
                first = false;
            }

            var option = '<option value="' + value.Id + '">' + value.NumberOfApartment + '</option>';

            $('#Apartments').append(option);
        });

        if (apartmentId != null) {
            $.getJSON('/Order/TasksList?apartmentId=' + apartmentId, function (data) {

                if (data.length == 0) $('#TaskEventCreate').prop('disabled', true);
                else $('#TaskEventCreate').prop('disabled', false);
                
                $('#Tasks').append("<option></option>");

                $.each(data, function (index, value) {
                    var option = '<option value="' + value.Id + '">' + value.ProductName;

                    if (value.Sufix != null)
                        option += ' ' + value.Sufix;

                    option += '</option>';
                    $('#Tasks').append(option);
                });
            });
        }
        else {
            $('#TaskEventCreate').prop('disabled', true);
        }
    });
});

$(document).on('change', '#Apartments', function () {
    var apartmentId = $(this).val();

    $('#Tasks').html('');

    $.getJSON('/Order/TasksList?apartmentId=' + apartmentId, function (data) {

        if (data.length == 0) $('#TaskEventCreate').prop('disabled', true);
        else $('#TaskEventCreate').prop('disabled', false);

        $('#Tasks').append("<option></option>");

        $.each(data, function (index, value) {
            var option = '<option value="' + value.Id + '">' + value.ProductName;

            if (value.Sufix != null)
                option += ' ' + value.Sufix;

            option += '</option>';

            $('#Tasks').append(option);
        });
    });
});

$(document).on("click", "#add-order-item", function () {
    var item = $("#order-item").val();

    $("#order-item").val("");

    var row = '<tr class="row-' + (nextLp + 1) + '">';
    row += "<td>" + item + "</td>";
    row += '<td style="text-align: center;"><button type="button" data-row-number="' + (nextLp + 1) + '" class="btn btn-danger delete-order-item">USUŃ</button>';
    row += "</tr>";

    $("#order-list tbody").append(row);

    var hidden = '<input type="hidden" name="orderItem[]" value="' + item + '" class="row-' + (nextLp + 1) + '" />';

    $("#hidden-order-items").append(hidden);

    nextLp++;
});

$(document).on("click", ".delete-order-item", function() {
    var lp = $(this).data("row-number");

    $(".row-" + lp).remove();
});