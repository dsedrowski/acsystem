﻿$(document).ready(function () {
    $('.date-only').datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'pl',
        showTodayButton: true,
        useCurrent: true,
        calendarWeeks: true,
        showClear: true,
        showClose: true,
        tooltips: {
            today: 'Dzisiaj',
            clear: 'Wyczyść',
            close: 'Zamknij',
            selectMonth: 'Wybierz miesiąc',
            prevMonth: 'Poprzedni miesiąc',
            nextMonth: 'Następny miesiąc',
            selectYear: 'Wybierz rok',
            prevYear: 'Poprzedni rok',
            nextYear: 'Następny rok',
            selectDecade: 'Wybierz dekadę',
            prevDecade: 'Poprzednia dekada',
            nextDecade: 'Następna dekada',
            prevCentury: 'Poprzedni wiek',
            nextCentury: 'Następny wiek'
        }
    });

    $('.date-time').datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        locale: 'pl',
        showTodayButton: true,
        useCurrent: true,
        calendarWeeks: true,
        showClear: true,
        showClose: true,
        sideBySide: true,
        stepping: 1,
        tooltips: {
            today: 'Dzisiaj',
            clear: 'Wyczyść',
            close: 'Zamknij',
            selectMonth: 'Wybierz miesiąc',
            prevMonth: 'Poprzedni miesiąc',
            nextMonth: 'Następny miesiąc',
            selectYear: 'Wybierz rok',
            prevYear: 'Poprzedni rok',
            nextYear: 'Następny rok',
            selectDecade: 'Wybierz dekadę',
            prevDecade: 'Poprzednia dekada',
            nextDecade: 'Następna dekada',
            prevCentury: 'Poprzedni wiek',
            nextCentury: 'Następny wiek'
        }
    });
})


$(document).on('click', '.close-modal', function () {
    Custombox.modal.close();
});

document.addEventListener('custombox:content:complete', function () {
    //$('.custombox-content .select2').select2();
});

function GetAjaxWithFunctions(url, type, data, successFunc, errorFunc) {
    var result = null;

    $.ajax({
        url: url,
        data: data,
        type: type,
        dataType: 'json',
        success: function (data) { result = data; successFunc(data); },
        error: function (xhr, status) { errorFunc(xhr, status) },
    });

    return result;
}

$(document).on('click', '[data-index]', function () {
    var index = $(this).data('index');

    new Custombox.modal({
        content: {
            effect: 'fadein',
            target: '#modal-photos-' + index,
            animateFrom: 'top',
            animateTo: 'bottom',
            positionX: 'center',
            positionY: 'center',
        },
        overlay: {
            active: 0.5
        }
    }).open();
})

$('.favourite-star').click(function () {
    var id = $(this).data("id");

    if ($('.favourite-' + id + ' span').hasClass("fa-star")) {
        $('.favourite-' + id).removeClass('active')
        setTimeout(function () {
            $('.favourite-' + id).removeClass('active-2')
        }, 30)
        $('.favourite-' + id).removeClass('active-3')
        setTimeout(function () {
            $('.favourite-' + id + ' span').removeClass('fa-star')
            $('.favourite-' + id + ' span').addClass('fa-star-o')
        }, 15)

        $.get("/Projects/SetProjectAsFavourite", { ID: id, Add: false }, function () { toastr.success("Pomyślnie usunięto projekt z ulubionych") });
    } else {
        $('.favourite-' + id).addClass('active')
        $('.favourite-' + id).addClass('active-2')
        setTimeout(function () {
            $('.favourite-' + id + ' span').addClass('fa-star')
            $('.favourite-' + id + ' span').removeClass('fa-star-o')
        }, 150)
        setTimeout(function () {
            $('.favourite-' + id).addClass('active-3')
        }, 150)
        $('.favourite-' + id+ ' .info').addClass('info-tog')
        setTimeout(function () {
            $('.favourite-' + id + ' .info').removeClass('info-tog')
        }, 1000)

        $.get("/Projects/SetProjectAsFavourite", { ID: id, Add: true }, function () { toastr.success("Pomyślnie dodano projekt do ulubionych") });
    }
})

$('.favourite-city').click(function () {
    console.log($(this));
    var city = $(this).data("city");

    if ($('.favourite-' + city + ' span').hasClass("fa-star")) {
        $('.favourite-' + city).removeClass('active')
        setTimeout(function () {
            $('.favourite-' + city).removeClass('active-2')
        }, 30)
        $('.favourite-' + city).removeClass('active-3')
        setTimeout(function () {
            $('.favourite-' + city + ' span').removeClass('fa-star')
            $('.favourite-' + city + ' span').addClass('fa-star-o')
        }, 15)

        $.get("/Projects/SetCityAsFavourite", { city: city, Add: false }, function () { toastr.success("Pomyślnie usunięto miasto z ulubionych") });
    } else {
        $('.favourite-' + city).addClass('active')
        $('.favourite-' + city).addClass('active-2')
        setTimeout(function () {
            $('.favourite-' + city + ' span').addClass('fa-star')
            $('.favourite-' + city + ' span').removeClass('fa-star-o')
        }, 150)
        setTimeout(function () {
            $('.favourite-' + city).addClass('active-3')
        }, 150)
        $('.favourite-' + city + ' .info').addClass('info-tog')
        setTimeout(function () {
            $('.favourite-' + city + ' .info').removeClass('info-tog')
        }, 1000)

        $.get("/Projects/SetCityAsFavourite", { city: city, Add: true }, function () { toastr.success("Pomyślnie dodano miasto do ulubionych") });
    }
})