﻿$(document).on('change', '#ProductId', function () {
    GetProduct($(this).val(), false);
});

$(document).on('dp.change', '#StartDate', function () {
    var date = $(this).val();

    var newMoment = moment(date, 'YYYY-MM-DD HH:mm').add(4, 'hours').format('YYYY-MM-DD HH:mm');

    $('#EndDate').val(newMoment);
});

$(document).on('change', '#ProjectId', function () {
    var projectId = $(this).val();
    var filtr = $(this).data('filtr');

    $('#ApartmentId').html('');

    $.getJSON('/Projects/GetApartments/' + projectId, function (data) {
        var first = true;
        var apartmentId = null;

        if (data.length === 0)
            $('#TaskSave').prop('disabled', true);
        else
            $('#TaskSave').prop('disabled', false);

        $.each(data, function (index, value) {
            if (first === true) {
                apartmentId = value.Id;
                first = false;
            }

            var option = '<option value="' + value.ID + '">' + value.ApartmentNumber + '</option>'

            $('#ApartmentId').append(option);
        });        
    });

    GetProductsList(projectId, filtr);
});

$(document).on("click", ".add-service-element", function () {
    var desc = $(".description-question").last().val();
    var question = $(".select-question-name").last().val();

    var container = '<div class="question-container">';
    container += '<div class="col-xs-9">';
    container += '<input type="hidden" name="questionDescription[]" value="' + desc + '" />';
    container += '<input type="hidden" name="questionSelect[]" value="' + question + '" />';
    container += "<strong>" + question + "</strong>";
    container += "</div>";
    container += '<div class="col-xs-3">';
    container += '<button type="button" class="btn btn-danger delete-added-question"><i class="fa fa-remove"></i></button>';
    container += "</div>";
    container += '<div class="col-xs-12">';
    container += "<span>" + desc + "</span>";
    container += "</div>";
    container += "</div>";

    $(".added-questions").append(container);

    $(".description-question").last().val("");
    $(".select-question-name").last().val("");
});

$(document).on("change", "#task-img", function () {
    var formData = new FormData();
    var totalFiles = this.files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = this.files[i];

        formData.append("task-img", file);
    }
    $.ajax({
        type: "POST",
        url: "/Projects/UploadImage?destination=Task_Images&watermarkText=Zdjęcie przed",
        data: formData,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.result === "true") {
                toastr.success("Zdjęcie zostało dodane!");
                $(".task-photo").html(
                    '<div class="form-group task-question-photos">' +
                    '<label class="label-control">' + response.fileName + "</label>" +
                    '<input type="hidden" name="TaskPhoto" value="' + response.newFileName + '"/>'+
                    "</div>");
            }
            else {
                toastr.error(response.message);
            }
        },
        error: function (error) {
            toastr.error(error);
        }
    });
});

function GetProduct(id) {
    ClearProductValues();
    var projectID = $("#ProjectId").val();

    var successFn = function (data) {
        $('#ProductPrice').val(data.Price);
        $('#PercentType1').val(data.Type1);
        $('#PercentType2').val(data.Type2);
        $('#PercentType3').val(data.Type3);
        $('#PercentType4').val(data.Type4);
        $('#PercentType5').val(data.Type5);
        $('#PercentType6').val(data.Type6);
        $('#product-jm').append(data.Jm);
    };

    var errorFn = function () {
        toastr.error("Wystąpił błąd podczas aktualizacji danych o produkcie!!");
    };

    GetAjaxWithFunctions('/Projects/GetProduct?id=' + id + '&projectID=' + projectID, 'GET', null, successFn, errorFn, '.product-load');
}

function ClearProductValues() {
    $('#ProductPrice').val('');
    $('#PercentType1').val('');
    $('#PercentType2').val('');
    $('#PercentType3').val('');
    $('#PercentType4').val('');
    $('#PercentType5').val('');
    $('#PercentType6').val('');
    $('#product-jm').html('');
}

function GetProductsList(id, filtr) {
    $("#ProductId").html("");

    $.getJSON('/Projects/GetProductsForProject?id=' + id + '&filtr=' + filtr, function (data) {
        var first = "selected";

        if (data.length === 0)
            $('#TaskSave').prop('disabled', true);
        else
            $('#TaskSave').prop('disabled', false);

        $.each(data, function (index, value) {
            if (first === true) {
                first = "";
            }

            var option = '<option value="' + value.ID + '" ' + first + '>' + value.Name + '</option>';

            $('#ProductId').append(option);
        });
    });
}