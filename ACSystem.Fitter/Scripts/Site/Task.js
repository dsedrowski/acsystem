﻿$(document).ready(function () {
    $("#get-task").on("click", function () {
        if ($(this).hasClass("disabled")) {
            $.alert("Nie można pobrać więcej niż jedno zadanie. Zakończ lub zatrzymaj aktualnie wykonywane zadanie by wybrać inne!");
        }
        else {
            var id = $(this).data("task-id");

            $.ajax({
                url: "/Projects/GetTask",
                dataType: "json",
                type: "GET",
                data: { id: id },
                success: function (data) {
                    if (data.success === true)
                        location.assign("/Projects/CurrentTask");
                    else
                        $.alert("Błąd. Proszę spróbować jeszcze raz.");
                },
                error: function (XMLHttpRequest) {
                    $.alert("Błąd " + XMLHttpRequest.status + ". Proszę spróbować jeszcze raz.");
                }
            });
        }
    });

    //$(".custombox-content #task-img").change(function () {
    //    var formData = new FormData();
    //    var totalFiles = this.files.length;
    //    for (var i = 0; i < totalFiles; i++) {
    //        var file = this.files[i];

    //        formData.append("task-img", file);
    //    }
    //    $.ajax({
    //        type: "POST",
    //        url: '/Projects/UploadImage',
    //        data: formData,
    //        dataType: 'json',
    //        contentType: false,
    //        processData: false,
    //        success: function (response) {
    //            if (response.result === 'true') {
    //                toastr.success("Zdjęcie zostało dodane!");
    //                $('.custombox-modal #photos-names').append(
    //                                            '<div class="form-group">' +
    //                                                '<label class="label-control">' + response.fileName + '</label>' +
    //                                                '<input type="text" class="form-control" placeholder="Opis" name="photosDesc[]" />' +
    //                                                '<input type="hidden" name="photos[]" value="' + response.newFileName + '" />' +
    //                                            '</div>');
    //            }
    //            else {
    //                toastr.error(response.message);
    //            }
    //        },
    //        error: function (error) {
    //            toastr.error(error);
    //        }
    //    });
    //});

    if ($('.question-machines[value="1"]').is(":checked")) {
        //$('.machine').val('0');
        $(".machine").removeClass("hid").addClass("visible");
    }
    else{
        $(".machine").val("2");
    }

    $(".developer .question").each(function () {
        if ($(this).is(":checked")) {
            var id = $(this).attr("id");
            var value = $(this).val();

            var photosDivID = "#Photo_" + $(this).attr("id");

            if (value === "0") {
                $(photosDivID).show();
            }
            else {
                $(photosDivID).hide();
            }

            ChangeValue(id, value);
        }
    });

    $(".question-machines").change(function () {
        if ($(this).val() === "1")
        {
            //$('.question[value="0"]').attr('checked', false);
            //$('.question[value="1"]').attr('checked', false);
            $('.question[value="2"]').attr("checked", false);
            $(".machines-photo").show();
            $(".machine").removeClass("hid").addClass("visible");
        }
        else
        {
            //$('.question[value="0"]').attr('checked', false);
            //$('.question[value="1"]').attr('checked', false);
            $('.question[value="2"]').attr("checked", true);
            $(".machines-photo").hide();
            $(".machine").removeClass("visible").addClass("hid");
        }
    });

    $("#developer-close-task-form").on("submit", function () {

    });
});

$(document).on("change", "#unit-sink-task-img", function () {
    var formData = new FormData();
    var totalFiles = this.files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = this.files[i];

        formData.append("task-img", file);
    }

    formData.append("prefix", "interior_av_karmar_pa_ytan");

    $.ajax({
        type: "POST",
        url: "/Projects/UploadImage",
        data: formData,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.result === "true") {
                toastr.success("Zdjęcie zostało dodane!");
                $("#unit-sink-photos-names").append(
                    '<div class="form-group unit-sink-task-photos col-md-6 photo">' +
                    '<img class="img-responsive" src="/dist/Task_Images/' + response.newFileName + '" />' +
                    '<button type="button" class="btn btn-danger delete-task-photo" style="margin-top: -60px;">Usuń</button>' +
                    '<label class="label-control">' + response.fileName + "</label>" +
                    '<input type="text" class="form-control" placeholder="Opis" name="photosDesc[]" />' +
                    '<input type="hidden" name="photos[]" value="' + response.newFileName + '" />' +
                    "</div>");
            }
            else {
                toastr.error(response.message);
            }
        },
        error: function (error) {
            toastr.error(error);
        }
    });
});

$(document).on("change", "#unit-fan-task-img", function () {
    var formData = new FormData();
    var totalFiles = this.files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = this.files[i];

        formData.append("task-img", file);
    }

    formData.append("prefix", "interiorkabineter_pa_ogon");

    $.ajax({
        type: "POST",
        url: "/Projects/UploadImage",
        data: formData,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.result === "true") {
                toastr.success("Zdjęcie zostało dodane!");
                $("#unit-fan-photos-names").append(
                    '<div class="form-group unit-fan-task-photos col-md-6 photo">' +
                    '<img class="img-responsive" src="/dist/Task_Images/' + response.newFileName + '" />' +
                    '<button type="button" class="btn btn-danger delete-task-photo" style="margin-top: -60px;">Usuń</button>' +
                    '<label class="label-control">' + response.fileName + "</label>" +
                    '<input type="text" class="form-control" placeholder="Opis" name="photosDesc[]" />' +
                    '<input type="hidden" name="photos[]" value="' + response.newFileName + '" />' +
                    "</div>");
            }
            else {
                toastr.error(response.message);
            }
        },
        error: function (error) {
            toastr.error(error);
        }
    });
});

$(document).on("change", "#in-drawer-task-img", function () {
    var formData = new FormData();
    var totalFiles = this.files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = this.files[i];

        formData.append("task-img", file);
    }

    formData.append("prefix", "inuti_skapet");

    $.ajax({
        type: "POST",
        url: "/Projects/UploadImage",
        data: formData,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.result === "true") {
                toastr.success("Zdjęcie zostało dodane!");
                $("#in-drawer-photos-names").append(
                    '<div class="form-group in-drawer-task-photos col-md-6 photo">' +
                    '<img class="img-responsive" src="/dist/Task_Images/' + response.newFileName + '" />' +
                    '<button type="button" class="btn btn-danger delete-task-photo" style="margin-top: -60px;">Usuń</button>' +
                    '<label class="label-control">' + response.fileName + "</label>" +
                    '<input type="text" class="form-control" placeholder="Opis" name="photosDesc[]" />' +
                    '<input type="hidden" name="photos[]" value="' + response.newFileName + '" />' +
                    "</div>");
            }
            else {
                toastr.error(response.message);
            }
        },
        error: function (error) {
            toastr.error(error);
        }
    });
});

$(document).on("change", "#other-task-img", function () {
    var formData = new FormData();
    var totalFiles = this.files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = this.files[i];

        formData.append("task-img", file);
    }

    formData.append("prefix", "andra");

    $.ajax({
        type: "POST",
        url: "/Projects/UploadImage",
        data: formData,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.result === "true") {
                toastr.success("Zdjęcie zostało dodane!");
                                            $("#other-photos-names").append(
                                            '<div class="form-group other-task-photos col-md-6 photo">' +
                                                '<img class="img-responsive" src="/dist/Task_Images/' + response.newFileName + '" />' +
                                                '<button type="button" class="btn btn-danger delete-task-photo" style="margin-top: -60px;">Usuń</button>' +
                                                '<label class="label-control">' + response.fileName + "</label>" +
                                                '<input type="text" class="form-control" placeholder="Opis" name="photosDesc[]" />' +
                                                '<input type="hidden" name="photos[]" value="' + response.newFileName + '" />' +
                                            "</div>");
            }
            else {
                toastr.error(response.message);
            }
        },
        error: function (error) {
            toastr.error(error);
        }
    });
});

$(document).on("change", "#task-document", function () {
    var taskID = $(this).data("task-id");

    var formData = new FormData();
    var totalFiles = this.files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = this.files[i];

        formData.append("task-img", file);
    }

    formData.append("TaskID", taskID);

    $.ajax({
        type: "POST",
        url: "/Projects/UploadTaskDocument",
        data: formData,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.result === "true") {
                toastr.success("Dokument został dodany!");

                var docRow = "<tr>";
                docRow += "<td>" + response.fileName + "</td>";
                docRow += '<td class="text-center"><a href="/Monter/dist/Documents/' + response.newFileName + '" class="btn btn-sm btn-primary text-center" style="color: white;">Pobierz</a></td>';
                docRow += "</tr>";

                $(".documents-table").append(docRow);
            }
            else {
                toastr.error("Wystąpił błąd podczas dodawania dokumentu.");
            }
        },
        error: function () {
            toastr.error("Wystąpił błąd podczas dodawania dokumentu.");
        }
    });
});

$(document).on("click", ".delete-task-photo", function () {
    $(this).parent("div").remove();
});

$(document).on("change", ".developer #question-img", function () {
    var question = $(this).data("question");

    var formData = new FormData();
    var totalFiles = this.files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = this.files[i];

        formData.append("task-img", file);
    }

    formData.append("prefix", question);
    formData.append("destination", "Question_Images");

    $.ajax({
        type: "POST",
        url: "/Projects/UploadImage",
        data: formData,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.result === "true") {
                toastr.success("Zdjęcie zostało dodane!");
                $("#Photo_Task_Questions_" + question).append(
                                            '<div class="form-group task-question-photos">' +
                                                '<label class="label-control">' + response.fileName + "</label>" +
                                                '<div class="input-group">' +
                                                '<input type="text" class="form-control photosQuestionDesc" placeholder="Opis" name="photosQuestionDesc[]" />' +
                                                    '<span class="input-group-btn">' +
                                                        '<button type="button" class="btn btn-danger delete-desc-photo"><i class="glyphicon glyphicon-remove"></i></button>' +
                                                    '</span>' +
                                                '</div>' +
                                                '<input type="hidden" name="photosQuestion[]" value="' + response.newFileName + '" class="photosQuestion" />' +
                                                '<input type="hidden" name="question[]" value="' + question + '" class="questionName" />' +
                    "</div>");
                $(".question-id-" + question + ' span').text(response.fileName);
                $(".question-id-" + question).append('<input type="hidden" name="photosQuestion[]" value="' + response.newFileName + '" class="photosQuestion" />' +
                                                     '<input type="hidden" name="question[]" value="' + question + '" class="questionName" />')
            }
            else {
                toastr.error(response.message);
            }
        },
        error: function (error) {
            toastr.error(error);
        }
    });
});

$(document).on("click", ".delete-desc-photo", function () {
    $(this).closest(".form-group").remove();
});

$(document).on("change", "#patch-img", function () {
    var patchID = $(this).data("patch-id");
    var patchTitle = $(this).data("patch-title");

    var formData = new FormData();
    var totalFiles = this.files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = this.files[i];

        formData.append("task-img", file);
        formData.append("patchID", patchID);
    }

    formData.append("prefix", patchTitle);
    formData.append("destination", "Patch_Images");

    $.ajax({
        type: "POST",
        url: "/Projects/UploadImage",
        data: formData,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.result === "true") {
                toastr.success("Zdjęcie zostało dodane!");
                $("#patch-photo-" + patchID).html(
                    '<div class="form-group task-question-photos">' +
                    '<label class="label-control">' + response.fileName + "</label>" +
                    "</div>");
            }
            else {
                toastr.error(response.message);
            }
        },
        error: function (error) {
            toastr.error(error);
        }
    });
});

$(document).on("change", ".developer .question", function () {
    var id = $(this).attr("id");
    var value = $(this).val();

    var photosDivID = "#Photo_" + $(this).attr("id");

    if (value === "0") {
        $(photosDivID).show();
    }
    else {
        $(photosDivID).hide();
    }

    ChangeValue(id, value);
});

$(document).on("click", ".close-modal", function () {
    window.Custombox.modal.close();
});

$(document).on("click", ".developer.finish-task", function () {
    var questionsOk = true;

    $('.developer .question[value="0"]').each(function () {
        if ($(this).is(":checked"))
            questionsOk = false;
    });

    questionsOk = CheckQuestionStatus();

    var workDone = 0;
    var toEnd = parseFloat($(".developer #ToEnd").val());
    var isUnitSinkPhotosSet = $(".unit-sink-task-photos").length;
    var isUnitFanPhotosSet = $(".unit-fan-task-photos").length;
    var isInDrawerPhotosSet = $(".in-drawer-task-photos").length;
    var isOtherPhotosSet = $(".other-task-photos").length;


    $(".developer .done-input").each(function() {
        var val = $(this).val();
        var valFloat = parseFloat(val);

        if (isNaN(valFloat) === false)
            workDone = workDone + valFloat;
    });


    if (questionsOk === false) {
        alert("Nie można zakończyć zadania gdy wszystkie elementy nie zostały wykonane.");
        return;
    }

    if (workDone !== toEnd){
        toastr.error("Wprowadzone ilości nie uzupełniają zadania");
        return;
    }

    if (window.isKitchen && isOtherPhotosSet === 0) {
        alert("Aby zakończyć zadanie, dodaj przynajmniej jedno zdjęcie ogólne.");
        return;
    }

    $(".developer #Status").val("9");
    $("#developer-close-task-form").submit();
});

$(document).on("click", ".service.finish-task", function () {
    var questionsOk = true;

    $(".service .patch-checkbox").each(function () {
        if ($(this).is(":checked") === false)
            questionsOk = false;
    });

    var workDone = 0;
    var toEnd = parseFloat($(".service #ToEnd").val());
    var isUnitSinkPhotosSet = $(".unit-sink-task-photos").length;
    var isUnitFanPhotosSet = $(".unit-fan-task-photos").length;
    var isInDrawerPhotosSet = $(".in-drawer-task-photos").length;
    var isOtherPhotosSet = $(".other-task-photos").length;


    $(".service .done-input").each(function() {
        var val = $(this).val();
        var valFloat = parseFloat(val);

        if (isNaN(valFloat) === false)
            workDone = workDone + valFloat;
    });


    if (questionsOk === false) {
        alert("Nie można zakończyć zadania gdy wszystkie elementy nie zostały wykonane.");
        return;
    }

    if (workDone !== toEnd) {
        toastr.error("Wprowadzone ilości nie uzupełniają zadania");
        return;
    }

    //if (window.isKitchen && isUnitSinkPhotosSet === 0) {
    //    alert("Aby zakończyć zadanie, dodaj zdjęcie wnętrza szafki na zlew.");
    //    return;
    //}

    //if (window.isKitchen && isUnitFanPhotosSet === 0) {
    //    alert("Aby zakończyć zadanie, dodaj zdjęcie wnętrza szafki na okap.");
    //    return;
    //}

    //if (window.isKitchen && isInDrawerPhotosSet === 0) {
    //    alert("Aby zakończyć zadanie, dodaj zdjęcie wnętrza szuflady z zabezpieczeniem.");
    //    return;
    //}

    if (window.isKitchen && isOtherPhotosSet === 0) {
        alert("Aby zakończyć zadanie, dodaj przynajmniej jedno zdjęcie ogólne.");
        return;
    }

    $(".service #Status").val("9");
    $("#service-close-task-form").submit();
});

$(document).on("click", ".developer.stop-task", function () {
    var workDone = 0;
    var toEnd = parseFloat($(".developer #ToEnd").val());

    $(".developer .done-input").each(function() {
        var val = $(this).val();
        var valFloat = parseFloat(val);

        if (isNaN(valFloat) === false)
            workDone = workDone + valFloat;
    });

    if (workDone === toEnd) {
        toastr.error("Nie można zatrzymać zadania gdy zostało w pełni wykonane!");
        return;
    }

    $(".developer #Status").val("2");
    $("#developer-close-task-form").submit();
});

$(document).on("click", ".service.stop-task", function () {
    var workDone = 0;
    var toEnd = parseFloat($(".service #ToEnd").val());

    $(".service .done-input").each(function() {
        var val = $(this).val();
        var valFloat = parseFloat(val);

        if (isNaN(valFloat) === false)
            workDone = workDone + valFloat;
    });

    if (workDone === toEnd) {
        toastr.error("Nie można zatrzymać zadania gdy zostało w pełni wykonane!");
        return;
    }

    $(".service #Status").val("2");
    $("#service-close-task-form").submit();
});

$(document).on("click", ".developer.finish-faults-task", function () {
    var questionOk = true;
    var descriptionsOk = true;
    var photosOk = true;
    var isUnitSinkPhotosSet = $(".unit-sink-task-photos").length;
    var isUnitFanPhotosSet = $(".unit-fan-task-photos").length;
    var isInDrawerPhotosSet = $(".in-drawer-task-photos").length;
    var isOtherPhotosSet = $(".other-task-photos").length;
    var errorType = $("#ErrorType").val();

    $('.developer .question[value="0"]').each(function () {
        if ($(this).is(":checked")) {
            questionOk = false;

            var questionId = $(this).attr("id");
            var description = $(".developer #Photo_" + questionId + " .photosQuestionDesc").val();

            if (description === "" || description === undefined) {
                descriptionsOk = false;
            }
            else {
                descriptionsOk = true;
            }
        }
    });

    $('.answer-select').each(function () {
        var split = $(this).val().split('-');

        if (split.length > 0 && split[0] === 'ER') {
            if ($('.question-id-' + split[1] + ' .questionName').length === 0) {
                photosOk = false;
            }
        }
    })

    questionOk = CheckQuestionStatus();

    var questionLength = $(".developer .question").length;

    if (questionLength === 0) questionOk = false;

    if (questionOk === true) {
        if (errorType !== "1") {
            alert("Nie można zakończyć zadania z błędami, gdy wszystkie elementy zostały wykonane poprawnie.");
            return;
        }
    }

    if (photosOk === false) {
        alert("Nie można zakończyć zadania z błędami, nie dodając zdjęć do błędów.");
        return;
    }

    if (descriptionsOk === false) {
        alert("Nie można zakończyć zadania z błędami, nie podając zdjęć i opisów do niewykonanych elementów.");
        return;
    }

    //if (window.isKitchen && isUnitSinkPhotosSet === 0) {
    //    alert("Aby zakończyć zadanie, dodaj zdjęcie wnętrza szafki na zlew.");
    //    return;
    //}

    //if (window.isKitchen && isUnitFanPhotosSet === 0) {
    //    alert("Aby zakończyć zadanie, dodaj zdjęcie wnętrza szafki na okap.");
    //    return;
    //}

    //if (window.isKitchen && isInDrawerPhotosSet === 0) {
    //    alert("Aby zakończyć zadanie, dodaj zdjęcie wnętrza szuflady z zabezpieczeniem.");
    //    return;
    //}

    if (window.isKitchen && isOtherPhotosSet === 0) {
        alert("Aby zakończyć zadanie, dodaj przynajmniej jedno zdjęcie ogólne.");
        return;
    }

    if (!window.isKitchen) {
        var comment = $("#TaskComment").val();

        if (comment === "" || comment === undefined) {
            alert("Aby zakończyć zadanie wpisz opis błędu w komentarzu.");
        }
    }

    $(".developer #Status").val("7");
    $("#developer-close-task-form").submit();
});

$(document).on("click", ".service.finish-faults-task", function () {
    var questionOk = false;

    $(".service .patch-checkbox").each(function () {
        if ($(this).is(":checked") === false) {
            questionOk = true;
        }
    });

    var checkboxLength = $(".service .patch-checkbox").length;
    //var isUnitSinkPhotosSet = $(".unit-sink-task-photos").length;
    //var isUnitFanPhotosSet = $(".unit-fan-task-photos").length;
    //var isInDrawerPhotosSet = $(".in-drawer-task-photos").length;
    var isOtherPhotosSet = $(".other-task-photos").length;

    if (checkboxLength === 0) questionOk = true;

    if (questionOk === false) {
        alert("Nie można zakończyć zadania z błędami, gdy wszystkie elementy zostały wykonane poprawnie.");
        return;
    }

    //if (window.isKitchen && isUnitSinkPhotosSet === 0) {
    //    alert("Aby zakończyć zadanie, dodaj zdjęcie wnętrza szafki na zlew.");
    //    return;
    //}

    //if (window.isKitchen && isUnitFanPhotosSet === 0) {
    //    alert("Aby zakończyć zadanie, dodaj zdjęcie wnętrza szafki na okap.");
    //    return;
    //}

    //if (window.isKitchen && isInDrawerPhotosSet === 0) {
    //    alert("Aby zakończyć zadanie, dodaj zdjęcie wnętrza szuflady z zabezpieczeniem.");
    //    return;
    //}

    if (window.isKitchen && isOtherPhotosSet === 0) {
        alert("Aby zakończyć zadanie, dodaj przynajmniej jedno zdjęcie ogólne.");
        return;
    }

    if (!window.isKitchen) {
        var comment = $("#TaskComment").val();

        if (comment === "" || comment === undefined) {
            alert("Aby zakończyć zadanie wpisz opis błędu w komentarzu.");
        }
    }

    $(".service #Status").val("7");
    $("#service-close-task-form").submit();
});

$(document).on("click", ".developer.place-not-ready", function () {
    var isOtherPhotosSet = $(".other-task-photos").length;

    if (isOtherPhotosSet === 0) {
        alert("Aby zakończyć zadanie, dodaj przynajmniej jedno zdjęcie ogólne.");
        return;
    }

    $(".developer #Status").val("3");
    $("#developer-close-task-form").submit();
});

$(document).on("click", ".service.place-not-ready", function () {
    var isOtherPhotosSet = $(".other-task-photos").length;

    if (isOtherPhotosSet === 0) {
        alert("Aby zakończyć zadanie, dodaj przynajmniej jedno zdjęcie ogólne.");
        return;
    }

    $(".service #Status").val("3");
    $("#service-close-task-form").submit();
});

$(document).on("click", ".add-fitter", function () {
    new window.Custombox.modal({
        content: {
            effect: "fadein",
            target: "#modal-add-fitter",
            animateFrom: "top",
            animateTo: "bottom",
            positionX: "center",
            positionY: "center",
            fullscreen: false
        },
        overlay: {
            active: 0.5
        }
    }).open();

    window.Custombox.open({
        target: "#modal-add-fitter",
        effect: "slide",
        position: ["center", "center"],
        escKey: true
    });
});

$(document).on("click", ".btn-add-fitter", function () {
    var fitterID = $(".custombox-content #FitterID").val();
    var taskID = $(".custombox-content #TaskID").val();

    $.ajax({
        url: "/Projects/AddFitter",
        data: { FitterID: fitterID, TaskID: taskID },
        type: "GET",
        dataType: "json",
        success: function (data) {
            if (data.success === true)
                location.reload();
            else
                toastr.error("Wystąpił błąd");
        },
        error: function () {
            toastr.error("Wystąpił błąd");
        }
    });
});

$(document).on("click", ".leave-task", function () {
    var workDone = 0;
    var toEnd = parseFloat($("#ToEnd").val());

    $(".done-input").each(function() {
        var val = $(this).val();
        var valFloat = parseFloat(val);

        if (isNaN(valFloat) === false)
            workDone = workDone + valFloat;
    });

    if (workDone > toEnd) {
        alert("Podana wartość jest większa niż brakująca do zakończenia zadania!");
        return;
    }

    $("#leave-task-form").submit();
});

$(document).on("change", ".service .patch-checkbox", function () {

    var id = $(this).data("patch-id");
    var status = $(this).is(":checked");

    var ajaxData = { PatchID: id, PatchStatus: status };

    var success = function (data) {
        if (data.success === true) {
            toastr.success("Element zadania został zaktualizowany pomyślnie");

            if (status === true) {
                $(".task-element-" + id).css({ 'background': "green", 'color': "white" });
                $(".task-element-" + id + ' .patches-photo').show();

                var doneInputCount = $("#service-close-task-form .done-input.not-hidden").length;

                var toAddCount = 1 / doneInputCount;

                $("#service-close-task-form .patch-count-to-add").each(function (index, elem) {
                    var actuallyVal = Number($(this).val());

                    var value = actuallyVal + toAddCount;

                    $(this).val(value).attr("min", value).attr("max", value);
                });
            }
            else {
                $(".task-element-" + id).css({ 'background': "none", 'color': "#333" });
                $(".task-element-" + id + ' .patches-photo').hide();

                doneInputCount = $("#service-close-task-form .done-input.not-hidden").length;

                toAddCount = 1 / doneInputCount;

                $("#service-close-task-form .patch-count-to-add").each(function (index, elem) {
                    var actuallyVal = Number($(this).val());

                    var value = actuallyVal - toAddCount;

                    $(this).val(value).attr("min", value).attr("max", value);
                });
            }
        }
        else {
            toastr.error("Wystąpił błąd podczas aktualizacji elementu zadania!");
        }
    };

    var error = function () {
        toastr.error("Wystąpił błąd podczas aktualizacji elementu zadania!");
    };

    GetAjaxWithFunctions("/Projects/EditTaskPatch", "GET", ajaxData, success, error, this);
});

$(document).on("submit", "#developer-close-task-form, #service-close-task-form, #leave-task-form", function () {
    $(".allow-block").attr("disabled", true);
});

$(document).on("dp.change", "#activity-datetime", function () {
    var val = $(this).val();

    $("#activity-datetime-hidden").val(val);
});

$(document).on("change", ".answer-select", function () {
    if ($(this).val().includes("ER"))
        $(".question-id-" + $(this).data("id")).removeClass("disabled");
    else {
        $(".question-id-" + $(this).data("id")).removeClass("disabled").addClass("disabled");
    }
})

function ChangeValue(id, value) {
    $("#" + id + ".question-hidden").val(value);
}

function CheckQuestionStatus() {
    questionOK = true;

    $(".developer .answer-select").each(function () {
        var value = $(this).val();

        if (value.includes("ER") || value === '-')
            questionOK = false;
    });

    return questionOK;
}