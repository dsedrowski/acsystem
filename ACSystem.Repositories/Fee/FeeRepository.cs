﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public class FeeRepository : BaseRepository<Fees>, IFeeRepository
    {
        public FeeRepository(IACSystemContext db) : base(db)
        {
        }
    }
}
