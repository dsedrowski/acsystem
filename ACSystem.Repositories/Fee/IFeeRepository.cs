﻿using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public interface IFeeRepository : IBaseRepository<Fees>
    {
    }
}
