﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public class ApartmentRepository : BaseRepository<Apartments>, IApartmentRepository
    {
        public ApartmentRepository(IACSystemContext db) : base(db)
        {
        }
    }
}
