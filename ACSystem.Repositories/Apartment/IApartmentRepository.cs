﻿using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public interface IApartmentRepository : IBaseRepository<Apartments>
    {
    }
}
