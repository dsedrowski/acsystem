﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public class Project_ProjectManagerRepository : BaseRepository<Project_ProjectManager>, IProject_ProjectManagerRepository
    {
        public Project_ProjectManagerRepository(IACSystemContext db) : base(db)
        {
        }
    }
}
