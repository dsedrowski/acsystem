﻿using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public interface IProject_ProjectManagerRepository : IBaseRepository<Project_ProjectManager>
    {
    }
}
