﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public class ProjectRepository : BaseRepository<Projects>, IProjectRepository
    {
        public ProjectRepository(IACSystemContext db) : base(db)
        {
        }
    }
}
