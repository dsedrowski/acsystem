﻿using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public interface IProjectRepository : IBaseRepository<Projects>
    {
    }
}
