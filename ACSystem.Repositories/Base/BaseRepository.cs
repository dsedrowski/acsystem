﻿using ACSystem.Core;
using ACSystem.Core.Extensions;
using ACSystem.Core.IRepo;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ACSystem.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T>, IDisposable where T : class
    {
        private readonly IACSystemContext _db;

        public BaseRepository(IACSystemContext db)
        {
            _db = db;
        }

        #region Synchronous

        public virtual IQueryable<T> All() =>
            _db.Set<T>();

        public virtual bool Any(Expression<Func<T, bool>> expression) =>
            All().Any(expression);

        public virtual int Count(Expression<Func<T, bool>> expression) =>
            All().Count(expression);

        public virtual IQueryable<U> ExecuteQuery<U>(string query) =>
            _db.Database.SqlQuery<U>(query).AsQueryable();

        public virtual T Find(int entityId) =>
            _db.Set<T>().Find(entityId);

        public virtual IList<T> Find(Expression<Func<T, bool>> expression) =>
            All().Where(expression).ToList();

        public virtual IList<T> Find(Expression<Func<T, bool>> expression, string sortBy, SortDirection sortDirection, int startRow, int size) =>
            ApplySorting(All().Where(expression), sortBy, sortDirection).Skip(startRow).Take(size).ToList();

        public virtual List<T> FindAll() =>
            All().ToList();

        public virtual T First() =>
            All().FirstOrDefault();

        public virtual T First(Expression<Func<T, bool>> expression) =>
            All().FirstOrDefault(expression);

        public virtual int GetTotalNumber() =>
            All().Count();

        public virtual void Remove(T entity) =>
            _db.Entry(entity).State = EntityState.Deleted;

        public virtual void Remove(IList<T> entities) 
        {
            foreach (var entity in entities)
                Remove(entity);
        }

        public virtual void Save(T entity) => 
            _db.Entry(entity).State = entity.GetId() > 0 ? EntityState.Modified : EntityState.Added;

        public virtual void Save(IList<T> entities)
        {
            foreach (var entity in entities)
                Save(entity);
        }

        public virtual void SaveChanges() =>
            _db.SaveChanges();

        #endregion

        #region Asynchronous

        public virtual async Task<bool> AnyAsync(Expression<Func<T, bool>> expression) =>
            await All().AnyAsync(expression);

        public virtual async Task<int> CountAsync(Expression<Func<T, bool>> expression) =>
            await All().CountAsync(expression);

        public virtual async Task<T> FindAsync(int entityId) =>
            await _db.Set<T>().FindAsync(entityId);

        public virtual async Task<IList<T>> FindAsync(Expression<Func<T, bool>> expression) =>
            await All().Where(expression).ToListAsync();

        public virtual async Task<IList<T>> FindAsync(Expression<Func<T, bool>> expression, string sortBy, SortDirection sortDirection, int startRow, int size) =>
            await ApplySorting(All().Where(expression), sortBy, sortDirection).Skip(startRow).Take(size).ToListAsync();

        public virtual async Task<List<T>> FindAllAsync() =>
            await All().ToListAsync();

        public virtual async Task<T> FirstAsync() =>
            await All().FirstOrDefaultAsync();

        public virtual async Task<T> FirstAsync(Expression<Func<T, bool>> expression) =>
            await All().FirstOrDefaultAsync(expression);

        public virtual async Task<int> GetTotalNumberAsync() =>
            await All().CountAsync();

        public virtual async void SaveChangesAsync() =>
            await _db.SaveChangesAsync();

        #endregion

        public virtual void Dispose() =>
            _db.Dispose();

        private IQueryable<T> ApplySorting(IQueryable<T> query, string sortBy, SortDirection sortDirection)
        {
            var param = Expression.Parameter(typeof(T), "item");
            var sortExpression = Expression.Lambda<Func<T, object>>(Expression.Convert(Expression.Property(param, sortBy), typeof(object)), param);

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    query = query.OrderBy(sortExpression);
                    break;
                case SortDirection.Descending:
                    query = query.OrderByDescending(sortExpression);
                    break;
            }

            return query;
        }

        public IACSystemContext GetContext() => _db;
    }
}
