﻿using ACSystem.Core;
using ACSystem.Core.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ACSystem.Repositories
{
    public interface IBaseRepository<T>
    {
        IQueryable<T> All();

        bool Any(Expression<Func<T, bool>> expression);
        Task<bool> AnyAsync(Expression<Func<T, bool>> expression);

        int Count(Expression<Func<T, bool>> expression);
        Task<int> CountAsync(Expression<Func<T, bool>> expression);

        IQueryable<U> ExecuteQuery<U>(string query);

        T Find(int entityId);
        Task<T> FindAsync(int entityId);

        IList<T> Find(Expression<Func<T, bool>> expression);
        Task<IList<T>> FindAsync(Expression<Func<T, bool>> expression);

        IList<T> Find(Expression<Func<T, bool>> expression, string sortBy, SortDirection sortDirection, int startRow, int size);
        Task<IList<T>> FindAsync(Expression<Func<T, bool>> expression, string sortBy, SortDirection sortDirection, int startRow, int size);

        List<T> FindAll();
        Task<List<T>> FindAllAsync();

        T First();
        Task<T> FirstAsync();

        T First(Expression<Func<T, bool>> expression);
        Task<T> FirstAsync(Expression<Func<T, bool>> expression);

        int GetTotalNumber();
        Task<int> GetTotalNumberAsync();

        void Remove(T entity);

        void Remove(IList<T> entities);

        void Save(T entity);

        void Save(IList<T> entities);

        void SaveChanges();
        void SaveChangesAsync();

        IACSystemContext GetContext();
    }
}
