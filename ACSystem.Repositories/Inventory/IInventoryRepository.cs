﻿using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public interface IInventoryRepository : IBaseRepository<Inventory>
    {
    }
}
