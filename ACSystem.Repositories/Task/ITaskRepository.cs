﻿using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public interface ITaskRepository : IBaseRepository<Tasks>
    {
    }
}
