﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACSystem.Core.IRepo;
using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public class TaskRepository : BaseRepository<Tasks>, ITaskRepository
    {
        public TaskRepository(IACSystemContext db) : base(db)
        {
        }
    }
}
