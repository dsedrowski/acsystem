﻿using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public interface IInvoiceRepository : IBaseRepository<Invoice>
    {
    }
}
