﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public class InvoiceRepository : BaseRepository<Invoice>, IInvoiceRepository
    {
        public InvoiceRepository(IACSystemContext db) : base(db)
        {
        }
    }
}
