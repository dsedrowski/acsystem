﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public class EventRepository : BaseRepository<Events>, IEventRepository
    {
        public EventRepository(IACSystemContext db) : base(db)
        {
        }
    }
}
