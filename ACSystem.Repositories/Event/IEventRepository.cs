﻿using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public interface IEventRepository : IBaseRepository<Events>
    {
    }
}
