﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public class SettingsRepository : BaseRepository<Settings>, ISettingsRepository
    {
        public SettingsRepository(IACSystemContext db) : base(db)
        {
        }
    }
}
