﻿using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public interface ISettingsRepository : IBaseRepository<Settings>
    {
    }
}
