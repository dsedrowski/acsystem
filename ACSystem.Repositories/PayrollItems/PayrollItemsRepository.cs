﻿using ACSystem.Core.IRepo;
using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public class PayrollItemsRepository : BaseRepository<PayrollItems>, IPayrollItemsRepository
    {
        public PayrollItemsRepository(IACSystemContext db) : base(db)
        {
        }
    }
}
