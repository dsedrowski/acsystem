﻿using ACSystem.Core.Models;

namespace ACSystem.Repositories
{
    public interface IPayrollItemsRepository : IBaseRepository<PayrollItems>
    {
    }
}
