﻿using ACSystem.Fortnox;
using FortnoxAPILibrary;
using System;
using System.Collections.Generic;
using System.Text;

namespace ACSystem.Testy
{
    public class Fortnox
    {
        private static CustomersManage customers;

        public static void CustomerTest()
        {
            ConsoleOp.WriteHeader("Testowanie połączenia klientów");

            customers = new CustomersManage();

            Get();
            GetList();
            Update();
            Create();
            GetList();
        }

        private static void Get()
        {
            ConsoleOp.WritePoint("1. Pobieranie klienta");

            try
            {
                var customer = customers.Get("1");

                Console.WriteLine(customer.Name);
            }
            catch (Exception ex)
            {
                ConsoleOp.WriteError(ex.Message);

                if (ex.InnerException != null)
                    ConsoleOp.WriteError(ex.InnerException.Message);
            }
        }

        private static void GetList()
        {
            ConsoleOp.WritePoint("2. Pobieranie listy klientów");

            try
            {
                var customersList = customers.List(Filter.Customer.Active, "", "");

                Console.WriteLine(customersList.Count);
            }
            catch (Exception ex)
            {
                ConsoleOp.WriteError(ex.Message);

                if (ex.InnerException != null)
                    ConsoleOp.WriteError(ex.InnerException.Message);
            }
        }

        private static void Update()
        {
            ConsoleOp.WritePoint("3. Aktualizacja klienta");

            try
            {
                var customer1 = customers.Get("1");
                Console.WriteLine("Przed: " + customer1.Name);

                customer1.Name = "TestAC";

                customers.Update(customer1);

                var customer2 = customers.Get("1");
                Console.WriteLine("Po: " + customer2.Name);

            }
            catch (Exception ex)
            {
                ConsoleOp.WriteError(ex.Message);

                if (ex.InnerException != null)
                    ConsoleOp.WriteError(ex.InnerException.Message);
            }
        }

        private static void Create()
        {
            ConsoleOp.WritePoint("4. Dodawanie klienta");

            try
            {
                var customer = customers.Create("DamianDS", "Hołdu 2", "75-607", "Koszalin", "1231231233", "test@test.pl");

                Console.WriteLine("OK!");
            }
            catch (Exception ex)
            {
                ConsoleOp.WriteError(ex.Message);

                if (ex.InnerException != null)
                    ConsoleOp.WriteError(ex.InnerException.Message);
            }


        }
    }
}
