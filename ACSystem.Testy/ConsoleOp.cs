﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACSystem.Testy
{
    public class ConsoleOp
    {
        public static void WriteHeader(string TEXT)
        {
            WriteHashLine();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(TEXT);

            WriteHashLine();

            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void WritePoint(string TEXT)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(TEXT);

            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void WriteError(string TEXT)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("!!!!! " + TEXT + " !!!!!");

            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void WriteHashLine()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("################################################");

        }
    }
}
